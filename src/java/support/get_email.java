/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */

package support;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Properties;
import java.util.UUID;
import javax.mail.Address;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class get_email 
{
    private static Logger logger = LogManager.getLogger();
    
    public static ArrayList <String[]> all(Connection con, String context_dir, String send_email_address, String send_subject, String send_body)
    {
        String results[] = {"","",""}; // success/fail    ResultText     #MessagesRetrived
        ArrayList <String[]> return_arraylist = new ArrayList();
        String status = "fail";
        String result = "";
        //get config
        try
        {     
            // create properties field
            Properties properties = new Properties();
            String receive_email_protocol = db.get_configuration_properties.by_name(con, "receive_email_protocol").trim();
            properties.put("mail.store.protocol", receive_email_protocol);
            
            String receive_email_host = db.get_configuration_properties.by_name(con, "receive_email_host").trim();
            properties.put("mail.pop3.host", receive_email_host);
            
            
            properties.put("mail.pop3.port", "995");
            properties.put("mail.pop3.starttls.enable", "true");
            Session emailSession = Session.getDefaultInstance(properties);
            // emailSession.setDebug(true);

                // create the POP3 store object and connect with the pop server
                Store store = emailSession.getStore("pop3s");

                //store.connect(pop3Host, user, password);

                // create the folder object and open it
                Folder emailFolder = store.getFolder("INBOX");
                emailFolder.open(Folder.READ_ONLY);

                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

                // retrieve the messages from the folder in an array and print it
                Message[] messages = emailFolder.getMessages();
                System.out.println("messages.length---" + messages.length);

                for (int i = 0; i < messages.length; i++) 
                {
                    Message message = messages[i];
                    UUID uuid = UUID.randomUUID(); //Generates random UUID  

                    File file = new File("c:/temp/email/" + uuid + ".txt");
                    // creates the file
                    file.createNewFile();
                    // creates a FileWriter Object
                    FileWriter writer = new FileWriter(file,true); 

                    writer.write("UUID: " + uuid + System.lineSeparator());

                    // FROM
                    Address[] addresses;
                    if ((addresses = message.getFrom()) != null) 
                    {
                        for (int j = 0; j < addresses.length; j++) 
                        {
                            System.out.println("FROM: " + addresses[j].toString());
                            writer.write("FROM: " + addresses[j].toString() + System.lineSeparator());
                        }
                    }
                    // TO
                    if ((addresses = message.getRecipients(Message.RecipientType.TO)) != null) 
                    {
                        for (int j = 0; j < addresses.length; j++) 
                        {
                            System.out.println("TO: " + addresses[j].toString());
                            writer.write("TO: " + addresses[j].toString() + System.lineSeparator());
                        }
                    }
                    // SUBJECT
                    if (message.getSubject() != null) 
                    {
                        System.out.println("SUBJECT: " + message.getSubject()+ System.lineSeparator());
                        writer.write("SUBJECT: " + message.getSubject()+ System.lineSeparator());
                    }

                    //writer.write("Date Sent: " + simpleDateFormat.format(message.getSentDate()) + System.lineSeparator());


                    System.out.println("---------------------------------");

                    //writePart(writer, uuid, message);
                }
                // close the store and folder objects
                emailFolder.close(false);
                store.close();
            } 
            catch (NoSuchProviderException e) 
            {
                e.printStackTrace();
            } 
            catch (MessagingException e) 
            {
                e.printStackTrace();
            } 
            catch (IOException e) 
            {
                e.printStackTrace();
            } 
            catch (Exception e) 
            {
                e.printStackTrace();
            }
            
            
            
            
            
            //System.out.println("sending email to:" + send_email_address);
            //System.out.println("sending email sub:" + send_subject);
            //System.out.println("sending email body:" + send_body);
            
            //String send_email_host = db.get_configuration_properties.by_name(con, "send_email_host").trim();
            //System.out.println("email_host=" + email_host);
            //options SSL,TLS,PLAIN
            //String send_email_encryption = db.get_configuration_properties.by_name(con, "send_email_encryption").trim();
            //String send_email_smtp_port = db.get_configuration_properties.by_name(con, "send_email_smtp_port").trim();
            //String send_email_user = db.get_configuration_properties.by_name(con, "send_email_user").trim();
            //String send_email_password = db.get_configuration_properties.by_name(con, "send_email_password").trim();   
            //send_email_password = support.encrypt_utils.decrypt(context_dir, send_email_password);
            //if(send_email_encryption.equalsIgnoreCase("SSL"))
            //{
            //    result = send_ssl(send_email_host, send_email_smtp_port, send_email_user, send_email_password, send_email_address, send_subject, send_body);
            //    if(result.equalsIgnoreCase("Message sent successfully..."))
            //    {
            //        status = "success";
            //    }
            //    else
            //    {
            //        status = "fail";
            //    }
                //return_hashmap.put("status", status);
                //return_hashmap.put("result", result);
            //}
            //else
            //{
            //    if(send_email_encryption.equalsIgnoreCase("TLS"))
            //    {
            //        result = send_tls(send_email_host,send_email_smtp_port, send_email_user, send_email_password, send_email_address, send_subject, send_body);
            //        if(result.equalsIgnoreCase("Message sent successfully..."))
            //        {
            //            status = "success";
            //        }
            //        else
            //        {
            //            status = "fail";
            //        }
            //        return_hashmap.put("status", status);
            //        return_hashmap.put("result", result);
            //    }
            //    else
            //    {
            //        if(send_email_encryption.equalsIgnoreCase("PLAIN"))
            //        {
            //            result = send_plain(send_email_host, send_email_smtp_port, send_email_user, send_email_password, send_email_address, send_subject, send_body);
            //            if(result.equalsIgnoreCase("Message sent successfully..."))
            //            {
            //                status = "success";
            //            }
            //            else
            //            {
            //                status = "fail";
            //            }
            //            return_hashmap.put("status", status);
            //            return_hashmap.put("result", result);
            //        }
            //    }
            //}            
        //}
        //catch(IOException | SQLException e)
        //{
        //    logger.error("ERROR Exception send_email.now:=" + e);
        //    result = "Exception in the email configuration:=" + e.toString();
        //}               
        return return_arraylist;
    }   
}

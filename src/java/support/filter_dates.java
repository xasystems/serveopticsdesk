/*
 * Copyright 2019 Real Data Technologies, Inc. , All rights reserved.
 */
package support;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

/**
 *
 * @author Ralph
 */
public class filter_dates 
{
    public static String past_30_days() 
    {
        SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a");    
        //01/12/2019 12:00 AM - 01/12/2019 11:59 PM
        /*SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a");    
        String return_string = "";
        LocalDate start_date = LocalDate.now().minusDays(30);
        LocalDate stop_date = LocalDate.now();
        String start_time = filter_format.format(java.sql.Date.valueOf(start_date));
        String stop_time = filter_format.format(java.sql.Date.valueOf(stop_date));
        return_string = start_time + " - " + stop_time;
        //System.out.println("return_string=" + return_string);
        return return_string;
        */
             
        String start_time = "";
        String stop_time = "";
        
        String return_string = "";
        GregorianCalendar cal = (GregorianCalendar) GregorianCalendar.getInstance();
        stop_time = filter_format.format(cal.getTime());
        cal.add(cal.MONTH, -1); // this doesn't decrement 30 days exactly. should it be cal.add(cal.DAY_OF_MONTH, -30);?  
        start_time = filter_format.format(cal.getTime());
        return_string = start_time + " - " + stop_time;
        //System.out.println("return_string=" + return_string);
        return return_string;
        
    }    
}

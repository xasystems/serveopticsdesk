/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */

package support;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 *
 * @author rcampbell
 */
public class string_utils 
{
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
    
    public static String convert_timestamp_to_timestamp_string(Timestamp in_timestamp)
    {
        String return_string = "";        
        try
        {
            return_string = timestamp_format.format(in_timestamp); 
        }
        catch(Exception e)
        {
            return_string = "";
        }
        return return_string;
    }
    public static String timestamp_to_date_time_picker_string(Timestamp in_timestamp)
    {
        String return_string = "";        
        try
        {
            return_string = timestamp_format.format(in_timestamp); 
        }
        catch(Exception e)
        {
            return_string = "";
        }
        return return_string;
    }
    public static String check_for_null(String value)
    {
        String return_value = "";
        
        if(value != null && !value.isEmpty())
        {
            return_value = value;
        }
        else
        {
            return_value = "";
        }
        return return_value;
    } 
    public static String clean_up_email_address(String in_email_address)
    {
        String return_string = ""; 
        //Microsoft Outlook <MicrosoftExchange329e71ec88ae4615bbc36ab6ce41109e@xasystems1.onmicrosoft.com>
        if(in_email_address.contains("<"))
        {
            String temp1[] = in_email_address.split("<");
            String temp2[] = temp1[1].split(">");
            return_string = temp2[0];                    
        } 
        else
        {
            return_string = in_email_address; 
        }
        return return_string;
    }
}

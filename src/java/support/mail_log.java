/*
 * Copyright 2019 Real Data Technologies, Inc. , All rights reserved.
 */
package support;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class mail_log 
{
    private static Logger logger = LogManager.getLogger();
    
    public static void insert(Connection con, String status, String type,  String short_message, String message) 
    {
        try 
        {
            PreparedStatement stmt = con.prepareStatement("INSERT INTO mail_log (operation_status,operation_type,short_message,message) VALUES (?,?,?,?)");
            stmt.setString(1,status);
            stmt.setString(2,type);
            stmt.setString(3,short_message);
            stmt.setString(4,message);
            stmt.executeUpdate();
            stmt.close();
            //con.close();
        }
        catch (Exception e) 
        {
            logger.error("ERROR Exception in support.mail_log.insert:=" + e);
        }        
    }
    
}

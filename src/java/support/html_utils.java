/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */

package support;

/**
 *
 * @author dsultanr
 */
public class html_utils 
{
    
    public static String generate_select_options(String[] values, String selected)
    {
        String return_string = "";        
        try
        {
            for (String value : values)
            {
                return_string += "<option " + (value.equals(selected)?"selected":"") + ">" + value + "</option>";
            }
        }
        catch(Exception e)
        {
            return_string = "";
        }
        return return_string;
    }
    
   
}

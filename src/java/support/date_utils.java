/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */

package support;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.TimeZone;

/**
 *
 * @author ralph
 */
public class date_utils 
{
    public static ZonedDateTime user_tz_to_utc(String user_tz_name, String timestamp)
    {
        //This code transfers dates from one timezone to another. 
        //Care was taken not to call any functions other than time zone accurate calendar and ZonedDateTime
        //to avoid accidental convertion to local time zone. 
        //Database connections timezone is set to UTC
        //Database my.ini is set to UTC
        //parse in string into cal parts  can not user date functions due to TZ convertions
        int year = Integer.parseInt(timestamp.substring(0, 4));
        int month = Integer.parseInt(timestamp.substring(4, 6));
        int date = Integer.parseInt(timestamp.substring(6, 8));
        int hour = Integer.parseInt(timestamp.substring(8, 10));
        int minute = Integer.parseInt(timestamp.substring(10, 12));
        int second = Integer.parseInt(timestamp.substring(12, 14));
        
        //create the calendar in the users timezone
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(user_tz_name)); //"America/New_York"
        //set the date and time in the users tz
        cal.set(cal.YEAR, year);
        cal.set(cal.MONTH, month - 1); //cal month starts at 0
        cal.set(cal.DAY_OF_MONTH, date);
        cal.set(cal.HOUR_OF_DAY, hour);
        cal.set(cal.MINUTE, minute);
        cal.set(cal.SECOND, second);
        
        String now_time = cal.get(cal.YEAR) + String.format("%02d", (cal.get(cal.MONTH) + 1))  + String.format("%02d", cal.get(cal.DAY_OF_MONTH))  + String.format("%02d", cal.get(cal.HOUR_OF_DAY))  +  String.format("%02d", cal.get(cal.MINUTE)) +  String.format("%02d", cal.get(cal.SECOND));
        //create a ZonedDateTime for the users tz
        //System.out.println("now=" + now_time);
        ZonedDateTime user_tz_zdt = ZonedDateTime.parse(now_time,DateTimeFormatter.ofPattern("yyyyMMddHHmmss").withZone(ZoneId.of(user_tz_name)));
        //System.out.println("now userTZ=" + user_tz_zdt);
        //create a ZonedDateTime for UTZ
        ZonedDateTime userInUTC = user_tz_zdt.withZoneSameInstant(ZoneId.of("UTC"));
        //System.out.println("now utcTZ=" + userInUTC);
        //return the ZonedDateTime
        return userInUTC;
    }
    public static ZonedDateTime utc_to_user_tz(String user_tz_name, String timestamp)
    {
        //This code transfers dates from one timezone to another. 
        //Care was taken not to call any functions other than time zone accurate calendar and ZonedDateTime
        //to avoid accidental convertion to local time zone. 
        //Database connections timezone is set to UTC
        //Database my.ini is set to UTC
        if (timestamp == null || timestamp.equals("")) {
            java.util.Date now = new java.util.Date();
            java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            timestamp = timestamp_format.format(now);
        }
        //parse in string into cal parts  can not user date functions due to TZ convertions
        int year = Integer.parseInt(timestamp.substring(0, 4));
        int month = Integer.parseInt(timestamp.substring(4, 6));
        int date = Integer.parseInt(timestamp.substring(6, 8));
        int hour = Integer.parseInt(timestamp.substring(8, 10));
        int minute = Integer.parseInt(timestamp.substring(10, 12));
        int second = Integer.parseInt(timestamp.substring(12, 14));
        
        //create the calendar in the users timezone
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC")); //"UTC"
        //set the date and time in the users tz
        cal.set(cal.YEAR, year);
        cal.set(cal.MONTH, month - 1); //cal month starts at 0
        cal.set(cal.DATE, date);
        cal.set(cal.HOUR_OF_DAY, hour);
        cal.set(cal.MINUTE, minute);
        cal.set(cal.SECOND, second);
        
        String now_time = cal.get(cal.YEAR) + String.format("%02d", (cal.get(cal.MONTH) + 1))  + String.format("%02d", cal.get(cal.DATE))  + String.format("%02d", cal.get(cal.HOUR_OF_DAY))  +  String.format("%02d", cal.get(cal.MINUTE)) +  String.format("%02d", cal.get(cal.SECOND));
        //create a ZonedDateTime for UTC
        ZonedDateTime utc_tz_zdt = ZonedDateTime.parse(now_time,DateTimeFormatter.ofPattern("yyyyMMddHHmmss").withZone(ZoneId.of("UTC")));
        //create a ZonedDateTime for users tz
        ZoneId user_zone_id;
        ZonedDateTime utc_in_user_tz;
        try {
            user_zone_id = ZoneId.of(user_tz_name);
            utc_in_user_tz = utc_tz_zdt.withZoneSameInstant(ZoneId.of(user_tz_name));
        } catch (Exception e) {
            utc_in_user_tz = utc_tz_zdt;
        }
        //return the ZonedDateTime
        return utc_in_user_tz;
    }
    public static ZonedDateTime now_in_utc()
    {
        //This code transfers dates from one timezone to another. 
        //Care was taken not to call any functions other than time zone accurate calendar and ZonedDateTime
        //to avoid accidental convertion to local time zone. 
        //Database connections timezone is set to UTC
        //Database my.ini is set to UTC
        //String return_string = "";
        //create the calendar in the users timezone
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC")); //"UTC"        
        String now_time = cal.get(cal.YEAR) + String.format("%02d", (cal.get(cal.MONTH) + 1))  + String.format("%02d", cal.get(cal.DATE))  + String.format("%02d", cal.get(cal.HOUR_OF_DAY))  +  String.format("%02d", cal.get(cal.MINUTE)) +  String.format("%02d", cal.get(cal.SECOND));
        //create a ZonedDateTime for UTC  //yeah yeah unnecessary string conversion, but that same pattern as the other tz conversions
        ZonedDateTime utc_tz_zdt = ZonedDateTime.parse(now_time,DateTimeFormatter.ofPattern("yyyyMMddHHmmss").withZone(ZoneId.of("UTC")));
        return utc_tz_zdt;
    }

}

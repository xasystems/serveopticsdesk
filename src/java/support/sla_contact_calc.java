/*
 * Copyright 2019 Real Data Technologies, Inc. , All rights reserved.
 */
package support;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class sla_contact_calc 
{
    private static Logger logger = LogManager.getLogger();
    
    public static double for_sla_id(Connection con, String contact_sla_id,  java.util.Date start, java.util.Date end) throws Exception, SQLException
    {
        //System.out.println("starting sla_contact_calc");
        double return_value = 0;
        String sla_type = "";
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        ArrayList channels = new <String>ArrayList();
        ArrayList contacts = new <String[]> ArrayList();
        
        //get sla definition
        String sla_info[] = db.get_sla.contact_by_id(con, contact_sla_id);
        sla_type = sla_info[4];
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(Exception e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id target definition:=" + e);
            //System.out.println("Exception in sla_contact_calc.for_sla_id target definition:=" + e);
        }
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(Exception e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id sla_success_threshold_value definition:=" + e);
            //System.out.println("Exception in sla_contact_calc.for_sla_id sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //get the channel from the JSON
        String json_additional_parameters = sla_info[24];
        //parse the json and put it in a arraylist
        try
        {
            //this is the possible JSON if more parameters are to be added
            //{"parameters":[{"channels":[{"name":"Service Desk"},{"name":"Business Systems"}]},{"other":[{"name":"test"},{"name":"test1"}]}]}
            //System.out.println("json_additional_parameters=" + json_additional_parameters);
            JsonReader reader = Json.createReader(new StringReader(json_additional_parameters));
            JsonObject resultObject = reader.readObject();
            
            JsonArray parameters = (JsonArray) resultObject.getJsonArray("parameters");
            //System.out.println("parameters length=" + parameters.size());
            
            JsonObject this_channel_record = parameters.getJsonObject(0); //a single jsonarray
            JsonArray j_channels = (JsonArray) this_channel_record.getJsonArray("channels");
            //System.out.println("j_channels length=" + j_channels.size());
            
            for(int a = 0; a < j_channels.size(); a++) //for each channel
            {
                JsonObject this_record = j_channels.getJsonObject(a); //a single channel
                channels.add(this_record.getString("name"));                 
            }
        }
        catch(Exception e)
        {
            logger.error("ERROR sla_contact_calc.for_sla_id JSON Parse:=" + e);
        }
        
        //get the contacts for this sla and timeframe //possible multiple channels
        //load up the array list with arrays of all contacts
        for(int a = 0; a < channels.size();a++)
        {
            String channel_contacts[][] = db.get_contact.all_contacts_for_channel_timeframe(con, channels.get(a).toString(), start, end);
            for(int b = 0; b < channel_contacts.length;b++)
            {
                
                String temp_array[] = new String[15]; 
                temp_array[0] = channel_contacts[b][0];
                temp_array[1] = channel_contacts[b][1];
                temp_array[2] = channel_contacts[b][2];
                temp_array[3] = channel_contacts[b][3];
                temp_array[4] = channel_contacts[b][4];
                temp_array[5] = channel_contacts[b][5];
                temp_array[6] = channel_contacts[b][6];
                temp_array[7] = channel_contacts[b][7];
                temp_array[8] = channel_contacts[b][8];
                temp_array[9] = channel_contacts[b][9];
                temp_array[10] = channel_contacts[b][10];
                temp_array[11] = channel_contacts[b][11];
                temp_array[12] = channel_contacts[b][12];
                temp_array[13] = channel_contacts[b][13];
                temp_array[14] = channel_contacts[b][14];
                contacts.add(temp_array);
            }
        }        
        //do the calculation
        //{"ABANDONED_CONTACT_RATE", "ABANDONED_CONTACT_COUNT", "AVERAGE_POST_PROCESS_TIME", "AVERAGE_SPEED_TO_ANSWER", "AVERAGE_TOTAL_TIME", "AVERAGE_ABANDONED_TIME", "AVERAGE_HANDLE_TIME", "AVERAGE_PROCESSING_TIME", "AVERAGE_TIME_IN_QUEUE"};
        switch(sla_type) 
        { 
            case "ABANDONED_CONTACT_RATE": 
                return_value = calc_abandoned_contact_rate(sla_info,contacts);
                break; 
            case "ABANDONED_CONTACT_COUNT": 
                return_value = calc_abandoned_contact_count(sla_info,contacts);
                break; 
            case "AVERAGE_SPEED_TO_ANSWER": 
                return_value = calc_average_speed_to_answer(sla_info, contacts);
                break; 
            case "AVERAGE_POST_PROCESS_TIME": 
                return_value = calc_average_post_process_time(sla_info, contacts);
                break;             
            case "AVERAGE_HANDLE_TIME": 
                return_value = calc_average_handle_time(sla_info, contacts);
                break; 
            case "AVERAGE_PROCESSING_TIME": 
                return_value = calc_average_process_time(sla_info, contacts);
                break; 
            default: 
                //System.out.println("no match"); 
        }         
        return return_value;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static double calc_abandoned_contact_rate(String sla_info[], ArrayList <String[]>contacts) throws Exception
    {
        double return_double = 0;
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_contacts = 0;
        int total_abandoned_contacts = 0;
        int total_contacts_meeting_sla = 0;
        int total_contacts_failing_sla = 0;
        double abandoned_threshold = 60;
        
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_rate target definition:=" + e);
        }
        try
        {
            abandoned_threshold = Double.parseDouble(sla_info[14]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_rate abandoned_threshold definition:=" + e);
        }
        
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_rate sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        
        try
        {
            for(int a = 0; a < contacts.size(); a++)
            {
                String contact[] = contacts.get(a);
                total_contacts++;
                if(contact[6] != null) //has abandoned time set
                {
                    try
                    {
                        int abandoned_time_seconds = Integer.parseInt(contact[6]);
                        if(abandoned_time_seconds > abandoned_threshold)
                        {
                            //is abandoned
                            total_abandoned_contacts++;
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_rate calc sla value:=" + e);
                    }
                }
            }
            double percent_abandoned = 0.00;
            try
            {
                percent_abandoned = ((double)total_abandoned_contacts / (double)total_contacts) * 100;
            }
            catch(Exception e)
            {
                percent_abandoned = 0.00;
            }
            switch(sla_operator) 
            { 
                case "greater_than": 
                    if(percent_abandoned > sla_value)
                    {
                        return_double = 100.00;
                    }
                    else
                    {
                        return_double = 0.00;
                    }
                    break; 
                case "greater_than_or_equal":
                    if(percent_abandoned >= sla_value)
                    {
                        return_double = 100.00;
                    }
                    else
                    {
                        return_double = 0.00;
                    }
                    break; 
                case "equal_to": 
                    if(percent_abandoned == sla_value)
                    {
                        return_double = 100.00;
                    }
                    else
                    {
                        return_double = 0.00;
                    }
                    break; 
                case "not_equal_to": 
                    if(percent_abandoned != sla_value)
                    {
                        return_double = 100.00;
                    }
                    else
                    {
                        return_double = 0.00;
                    }
                    break; 
                case "less_than_or_equal": 
                    if(percent_abandoned <= sla_value)
                    {
                        return_double = 100.00;
                    }
                    else
                    {
                        return_double = 0.00;
                    }
                    break; 
                case "less_than": 
                    if(percent_abandoned < sla_value)
                    {
                        return_double = 100.00;
                    }
                    else
                    {
                        return_double = 0.00;
                    }
                    break; 
                default: 
                    //System.out.println("no match");     
            }
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_contact_calc.calc_abandoned_contact_rate calc loop=" + e);
        }
        return return_double;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static double calc_abandoned_contact_count(String sla_info[], ArrayList <String[]>contacts) throws Exception
    {
        double return_double = 0;
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_contacts = 0;
        int total_abandoned_contacts = 0;
        int total_contacts_meeting_sla = 0;
        int total_contacts_failing_sla = 0;
        double abandoned_threshold = 60;
        
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_count target definition:=" + e);
        }
        try
        {
            abandoned_threshold = Double.parseDouble(sla_info[14]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_count abandoned_threshold definition:=" + e);
        }
        
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_count sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        
        try
        {
            for(int a = 0; a < contacts.size(); a++)
            {
                String contact[] = contacts.get(a);
                total_contacts++;
                if(contact[6] != null) //has abandoned time set
                {
                    try
                    {
                        int abandoned_time_seconds = Integer.parseInt(contact[6]);
                        if(abandoned_time_seconds > abandoned_threshold)
                        {
                            //is abandoned
                            total_abandoned_contacts++;
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_count calc sla value:=" + e);
                    }
                }
            }
            switch(sla_operator) 
            { 
                case "greater_than": 
                    if(total_abandoned_contacts > sla_value)
                    {
                        return_double = 100.00;
                    }
                    else
                    {
                        return_double = 0.00;
                    }
                    break; 
                case "greater_than_or_equal":
                    if(total_abandoned_contacts >= sla_value)
                    {
                        return_double = 100.00;
                    }
                    else
                    {
                        return_double = 0.00;
                    }
                    break; 
                case "equal_to": 
                    if(total_abandoned_contacts == sla_value)
                    {
                        return_double = 100.00;
                    }
                    else
                    {
                        return_double = 0.00;
                    }
                    break; 
                case "not_equal_to": 
                    if(total_abandoned_contacts != sla_value)
                    {
                        return_double = 100.00;
                    }
                    else
                    {
                        return_double = 0.00;
                    }
                    break; 
                case "less_than_or_equal": 
                    if(total_abandoned_contacts <= sla_value)
                    {
                        return_double = 100.00;
                    }
                    else
                    {
                        return_double = 0.00;
                    }
                    break; 
                case "less_than": 
                    if(total_abandoned_contacts < sla_value)
                    {
                        return_double = 100.00;
                    }
                    else
                    {
                        return_double = 0.00;
                    }
                    break; 
                default: 
                    //System.out.println("no match");     
            }
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_contact_calc.calc_abandoned_contact_count calc loop=" + e);
        }
        return return_double;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static double calc_average_speed_to_answer(String sla_info[], ArrayList <String[]>contacts) throws Exception
    {
        double return_double = 0;
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_answered_calls = 0;
        int total_contacts_meeting_sla = 0;
        int total_contacts_failing_sla = 0;
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_speed_to_answer target definition:=" + e);
        }
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_speed_to_answer sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        //for each contact        
            //get the "accept_time_seconds"            
            //use operator to determine good or bad
            //increment good/bad counts
        //end for
        try
        {
            for(int a = 0; a < contacts.size(); a++)
            {
                String contact[] = contacts.get(a);
                if(contact[4] != null) //was it answered?
                {
                    int accept_time_seconds = 0;
                    try
                    {
                        accept_time_seconds = Integer.parseInt(contact[4]);
                        total_answered_calls++;
                        //{"greater_than", "greater_than_or_equal", "equal_to", "not_equal_to", "less_than_or_equal", "less_than"}
                        switch(sla_operator) 
                        { 
                            case "greater_than": 
                                if(accept_time_seconds > sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "greater_than_or_equal":
                                if(accept_time_seconds >= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "equal_to": 
                                if(accept_time_seconds == sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "not_equal_to": 
                                if(accept_time_seconds != sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "less_than_or_equal": 
                                if(accept_time_seconds <= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "less_than": 
                                if(accept_time_seconds < sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            default: 
                                //System.out.println("no match");     
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_speed_to_answer calc sla value:=" + e);
                    }                
                }
            } 
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_contact_calc.calc_average_speed_to_answer calc loop=" + e);
        }
        System.out.println("total_contacts_meeting_sla=" + total_contacts_meeting_sla + "  total_answered_calls=" + total_answered_calls);
        
        try
        {
            return_double = ((double)total_contacts_meeting_sla / (double)total_answered_calls ) * 100;
        }
        catch(Exception e)
        {
            logger.error("Exception in sla_contact_calc.calc_average_speed_to_answer final calc:=" + e);
        }
        return return_double;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static double calc_average_post_process_time(String sla_info[], ArrayList <String[]>contacts) throws Exception
    {
        double return_double = 0;
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_answered_calls = 0;
        int total_contacts_meeting_sla = 0;
        int total_contacts_failing_sla = 0;
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_port_processing_time target definition:=" + e);
        }
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_port_processing_time sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        //for each contact        
            //get the "accept_time_seconds"            
            //use operator to determine good or bad
            //increment good/bad counts
        //end for
        try
        {
            for(int a = 0; a < contacts.size(); a++)
            {
                String contact[] = contacts.get(a);
                if(contact[4] != null) //was it answered?
                {
                    int post_process_time = 0;
                    try
                    {
                        post_process_time = Integer.parseInt(contact[11]);
                        total_answered_calls++;
                        //{"greater_than", "greater_than_or_equal", "equal_to", "not_equal_to", "less_than_or_equal", "less_than"}
                        switch(sla_operator) 
                        { 
                            case "greater_than": 
                                if(post_process_time > sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "greater_than_or_equal":
                                if(post_process_time >= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "equal_to": 
                                if(post_process_time == sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "not_equal_to": 
                                if(post_process_time != sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "less_than_or_equal": 
                                if(post_process_time <= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "less_than": 
                                if(post_process_time < sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            default: 
                                //System.out.println("no match");     
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_port_processing_time calc sla value:=" + e);
                    }                
                }
            } 
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_contact_calc.calc_average_port_processing_time calc loop=" + e);
        }
        try
        {
            return_double = ((double)total_contacts_meeting_sla / (double)total_answered_calls ) * 100;
        }
        catch(Exception e)
        {
            logger.error("Exception in sla_contact_calc.calc_average_port_processing_time final calc:=" + e);
        }
        return return_double;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static double calc_average_handle_time(String sla_info[], ArrayList <String[]>contacts) throws Exception
    {
        //this is total_time in DB
        
        double return_double = 0;
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_answered_calls = 0;
        int total_contacts_meeting_sla = 0;
        int total_contacts_failing_sla = 0;
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_total_time target definition:=" + e);
        }
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_total_time sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        //for each contact        
            //get the "accept_time_seconds"            
            //use operator to determine good or bad
            //increment good/bad counts
        //end for
        try
        {
            for(int a = 0; a < contacts.size(); a++)
            {
                String contact[] = contacts.get(a);
                if(contact[4] != null) //was it answered?
                {
                    int total_time = 0;
                    try
                    {
                        total_time = Integer.parseInt(contact[13]);
                        total_answered_calls++;
                        //{"greater_than", "greater_than_or_equal", "equal_to", "not_equal_to", "less_than_or_equal", "less_than"}
                        switch(sla_operator) 
                        { 
                            case "greater_than": 
                                if(total_time > sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "greater_than_or_equal":
                                if(total_time >= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "equal_to": 
                                if(total_time == sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "not_equal_to": 
                                if(total_time != sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "less_than_or_equal": 
                                if(total_time <= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "less_than": 
                                if(total_time < sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            default: 
                                //System.out.println("no match");     
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_total_time calc sla value:=" + e);
                    }                
                }
            } 
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_contact_calc.calc_average_total_time calc loop=" + e);
        }
        try
        {
            return_double = ((double)total_contacts_meeting_sla / (double)total_answered_calls ) * 100;
        }
        catch(Exception e)
        {
            logger.error("Exception in sla_contact_calc.calc_average_total_time final calc:=" + e);
        }
        return return_double;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static double calc_average_process_time(String sla_info[], ArrayList <String[]>contacts) throws Exception
    {
        //this is total_time in DB
        
        double return_double = 0;
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_answered_calls = 0;
        int total_contacts_meeting_sla = 0;
        int total_contacts_failing_sla = 0;
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_process_time target definition:=" + e);
        }
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_process_time sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        //for each contact        
            //get the "accept_time_seconds"            
            //use operator to determine good or bad
            //increment good/bad counts
        //end for
        try
        {
            for(int a = 0; a < contacts.size(); a++)
            {
                String contact[] = contacts.get(a);
                if(contact[4] != null) //was it answered?
                {
                    int process_time = 0;
                    try
                    {
                        process_time = Integer.parseInt(contact[12]);
                        total_answered_calls++;
                        //{"greater_than", "greater_than_or_equal", "equal_to", "not_equal_to", "less_than_or_equal", "less_than"}
                        switch(sla_operator) 
                        { 
                            case "greater_than": 
                                if(process_time > sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "greater_than_or_equal":
                                if(process_time >= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "equal_to": 
                                if(process_time == sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "not_equal_to": 
                                if(process_time != sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "less_than_or_equal": 
                                if(process_time <= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            case "less_than": 
                                if(process_time < sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                }
                                break; 
                            default: 
                                //System.out.println("no match");     
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_process_time calc sla value:=" + e);
                    }                
                }
            } 
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_contact_calc.calc_average_process_time calc loop=" + e);
        }
        try
        {
            return_double = ((double)total_contacts_meeting_sla / (double)total_answered_calls ) * 100;
        }
        catch(Exception e)
        {
            logger.error("Exception in sla_contact_calc.calc_average_process_time final calc:=" + e);
        }
        return return_double;
    }
    
    
    public static ArrayList<String[]> get_drilldown_data_for_sla_id(Connection con, String contact_sla_id,  java.util.Date start, java.util.Date end) throws Exception, SQLException
    {
        //System.out.println("starting sla_contact_calc");
        ArrayList<String[]> return_value = new ArrayList();
        String sla_type = "";
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        ArrayList channels = new <String>ArrayList();
        ArrayList contacts = new <String[]> ArrayList();
        
        //get sla definition
        String sla_info[] = db.get_sla.contact_by_id(con, contact_sla_id);
        sla_type = sla_info[4];
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(Exception e)
        {
            logger.error("Exception in sla_contact_calc.get_non_compliant_list_for_sla_id target definition:=" + e);
            //System.out.println("Exception in sla_contact_calc.for_sla_id target definition:=" + e);
        }
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(Exception e)
        {
            logger.error("Exception in sla_contact_calc.get_non_compliant_list_for_sla_id sla_success_threshold_value definition:=" + e);
            //System.out.println("Exception in sla_contact_calc.for_sla_id sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //get the channel from the JSON
        String json_additional_parameters = sla_info[24];
        //parse the json and put it in a arraylist
        try
        {
            //this is the possible JSON if more parameters are to be added
            //{"parameters":[{"channels":[{"name":"Service Desk"},{"name":"Business Systems"}]},{"other":[{"name":"test"},{"name":"test1"}]}]}
            //System.out.println("json_additional_parameters=" + json_additional_parameters);
            JsonReader reader = Json.createReader(new StringReader(json_additional_parameters));
            JsonObject resultObject = reader.readObject();
            
            JsonArray parameters = (JsonArray) resultObject.getJsonArray("parameters");
            //System.out.println("parameters length=" + parameters.size());
            
            JsonObject this_channel_record = parameters.getJsonObject(0); //a single jsonarray
            JsonArray j_channels = (JsonArray) this_channel_record.getJsonArray("channels");
            //System.out.println("j_channels length=" + j_channels.size());
            
            for(int a = 0; a < j_channels.size(); a++) //for each channel
            {
                JsonObject this_record = j_channels.getJsonObject(a); //a single channel
                channels.add(this_record.getString("name"));                 
            }
        }
        catch(Exception e)
        {
            logger.error("ERROR sla_contact_calc.get_non_compliant_list_for_sla_id JSON Parse:=" + e);
        }
        
        //get the contacts for this sla and timeframe //possible multiple channels
        //load up the array list with arrays of all contacts
        for(int a = 0; a < channels.size();a++)
        {
            String channel_contacts[][] = db.get_contact.all_contacts_for_channel_timeframe(con, channels.get(a).toString(), start, end);
            for(int b = 0; b < channel_contacts.length;b++)
            {
                
                String temp_array[] = new String[15]; 
                temp_array[0] = channel_contacts[b][0];
                temp_array[1] = channel_contacts[b][1];
                temp_array[2] = channel_contacts[b][2];
                temp_array[3] = channel_contacts[b][3];
                temp_array[4] = channel_contacts[b][4];
                temp_array[5] = channel_contacts[b][5];
                temp_array[6] = channel_contacts[b][6];
                temp_array[7] = channel_contacts[b][7];
                temp_array[8] = channel_contacts[b][8];
                temp_array[9] = channel_contacts[b][9];
                temp_array[10] = channel_contacts[b][10];
                temp_array[11] = channel_contacts[b][11];
                temp_array[12] = channel_contacts[b][12];
                temp_array[13] = channel_contacts[b][13];
                temp_array[14] = channel_contacts[b][14];
                contacts.add(temp_array);
            }
        }        
        //do the calculation
        //{"ABANDONED_CONTACT_RATE", "ABANDONED_CONTACT_COUNT", "AVERAGE_POST_PROCESS_TIME", "AVERAGE_SPEED_TO_ANSWER", "AVERAGE_TOTAL_TIME", "AVERAGE_ABANDONED_TIME", "AVERAGE_HANDLE_TIME", "AVERAGE_PROCESSING_TIME", "AVERAGE_TIME_IN_QUEUE"};
        switch(sla_type) 
        { 
            case "ABANDONED_CONTACT_RATE": 
                return_value = get_non_compliant_list_calc_abandoned_contact_rate(sla_info,contacts);
                break; 
            case "ABANDONED_CONTACT_COUNT": 
                return_value = get_non_compliant_list_calc_abandoned_contact_count(sla_info,contacts);
                break; 
            case "AVERAGE_SPEED_TO_ANSWER": 
                return_value = get_non_compliant_list_calc_average_speed_to_answer(sla_info, contacts);
                break; 
            case "AVERAGE_POST_PROCESS_TIME": 
                return_value = get_non_compliant_list_calc_average_post_process_time(sla_info, contacts);
                break;             
            case "AVERAGE_HANDLE_TIME": 
                return_value = get_non_compliant_list_calc_average_handle_time(sla_info, contacts);
                break; 
            case "AVERAGE_PROCESSING_TIME": 
                return_value = get_non_compliant_list_calc_average_process_time(sla_info, contacts);
                break; 
            default: 
                //System.out.println("no match"); 
        }         
        return return_value;
    }
    public static ArrayList<ArrayList<String[]>> get_all_drilldown_data_for_sla_id(Connection con, String contact_sla_id,  java.util.Date start, java.util.Date end) throws Exception, SQLException
    {
        //System.out.println("starting sla_contact_calc");
        ArrayList<ArrayList<String[]>> return_value = new ArrayList<ArrayList<String[]>>();
        String sla_type = "";
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        ArrayList channels = new <String>ArrayList();
        ArrayList contacts = new <String[]> ArrayList();
        
        //get sla definition
        String sla_info[] = db.get_sla.contact_by_id(con, contact_sla_id);
        sla_type = sla_info[4];
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(Exception e)
        {
            logger.error("Exception in sla_contact_calc.get_non_compliant_list_for_sla_id target definition:=" + e);
            //System.out.println("Exception in sla_contact_calc.for_sla_id target definition:=" + e);
        }
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(Exception e)
        {
            logger.error("Exception in sla_contact_calc.get_non_compliant_list_for_sla_id sla_success_threshold_value definition:=" + e);
            //System.out.println("Exception in sla_contact_calc.for_sla_id sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //get the channel from the JSON
        String json_additional_parameters = sla_info[24];
        //parse the json and put it in a arraylist
        try
        {
            //this is the possible JSON if more parameters are to be added
            //{"parameters":[{"channels":[{"name":"Service Desk"},{"name":"Business Systems"}]},{"other":[{"name":"test"},{"name":"test1"}]}]}
            //System.out.println("json_additional_parameters=" + json_additional_parameters);
            JsonReader reader = Json.createReader(new StringReader(json_additional_parameters));
            JsonObject resultObject = reader.readObject();
            
            JsonArray parameters = (JsonArray) resultObject.getJsonArray("parameters");
            //System.out.println("parameters length=" + parameters.size());
            
            JsonObject this_channel_record = parameters.getJsonObject(0); //a single jsonarray
            JsonArray j_channels = (JsonArray) this_channel_record.getJsonArray("channels");
            //System.out.println("j_channels length=" + j_channels.size());
            
            for(int a = 0; a < j_channels.size(); a++) //for each channel
            {
                JsonObject this_record = j_channels.getJsonObject(a); //a single channel
                channels.add(this_record.getString("name"));                 
            }
        }
        catch(Exception e)
        {
            logger.error("ERROR sla_contact_calc.get_non_compliant_list_for_sla_id JSON Parse:=" + e);
        }
        
        //get the contacts for this sla and timeframe //possible multiple channels
        //load up the array list with arrays of all contacts
        for(int a = 0; a < channels.size();a++)
        {
            String channel_contacts[][] = db.get_contact.all_contacts_for_channel_timeframe(con, channels.get(a).toString(), start, end);
            for(int b = 0; b < channel_contacts.length;b++)
            {
                
                String temp_array[] = new String[15]; 
                temp_array[0] = channel_contacts[b][0];
                temp_array[1] = channel_contacts[b][1];
                temp_array[2] = channel_contacts[b][2];
                temp_array[3] = channel_contacts[b][3];
                temp_array[4] = channel_contacts[b][4];
                temp_array[5] = channel_contacts[b][5];
                temp_array[6] = channel_contacts[b][6];
                temp_array[7] = channel_contacts[b][7];
                temp_array[8] = channel_contacts[b][8];
                temp_array[9] = channel_contacts[b][9];
                temp_array[10] = channel_contacts[b][10];
                temp_array[11] = channel_contacts[b][11];
                temp_array[12] = channel_contacts[b][12];
                temp_array[13] = channel_contacts[b][13];
                temp_array[14] = channel_contacts[b][14];
                contacts.add(temp_array);
            }
        }        
        //do the calculation
        //{"ABANDONED_CONTACT_RATE", "ABANDONED_CONTACT_COUNT", "AVERAGE_POST_PROCESS_TIME", "AVERAGE_SPEED_TO_ANSWER", "AVERAGE_TOTAL_TIME", "AVERAGE_ABANDONED_TIME", "AVERAGE_HANDLE_TIME", "AVERAGE_PROCESSING_TIME", "AVERAGE_TIME_IN_QUEUE"};
        switch(sla_type) 
        { 
            case "ABANDONED_CONTACT_RATE": 
                return_value = get_all_list_calc_abandoned_contact_rate(sla_info,contacts);
                break; 
            case "ABANDONED_CONTACT_COUNT": 
                return_value = get_all_list_calc_abandoned_contact_count(sla_info,contacts);
                break; 
            case "AVERAGE_SPEED_TO_ANSWER": 
                return_value = get_all_list_calc_average_speed_to_answer(sla_info, contacts);
                break; 
            case "AVERAGE_POST_PROCESS_TIME": 
                return_value = get_all_list_calc_average_post_process_time(sla_info, contacts);
                break;             
            case "AVERAGE_HANDLE_TIME": 
                return_value = get_all_list_calc_average_handle_time(sla_info, contacts);
                break; 
            case "AVERAGE_PROCESSING_TIME": 
                return_value = get_all_list_calc_average_process_time(sla_info, contacts);
                break; 
            default: 
                //System.out.println("no match"); 
        }         
        return return_value;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static ArrayList<String[]> get_non_compliant_list_calc_abandoned_contact_rate(String sla_info[], ArrayList <String[]>contacts) throws Exception
    {
        ArrayList non_compliant_contacts = new <String[]> ArrayList();
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_contacts = 0;
        int total_abandoned_contacts = 0;
        int total_contacts_meeting_sla = 0;
        int total_contacts_failing_sla = 0;
        double abandoned_threshold = 60;
        
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_rate target definition:=" + e);
        }
        try
        {
            abandoned_threshold = Double.parseDouble(sla_info[14]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_rate abandoned_threshold definition:=" + e);
        }
        
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_rate sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        
        try
        {
            for(int a = 0; a < contacts.size(); a++)
            {
                String contact[] = contacts.get(a);
                total_contacts++;
                if(contact[6] != null) //has abandoned time set
                {
                    try
                    {
                        int abandoned_time_seconds = Integer.parseInt(contact[6]);
                        if(abandoned_time_seconds > abandoned_threshold)
                        {
                            //is abandoned
                            total_abandoned_contacts++;
                            total_contacts_failing_sla++;
                            non_compliant_contacts.add(contact);
                        }
                        else
                        {
                            total_contacts_meeting_sla++;
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_rate calc sla value:=" + e);
                    }
                }
            }
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_contact_calc.calc_abandoned_contact_rate calc loop=" + e);
        }
        //add the metrics to the end of the arraylist
        String metrics[] = {String.valueOf(total_contacts),String.valueOf(total_abandoned_contacts),String.valueOf(total_contacts_meeting_sla),String.valueOf(total_contacts_failing_sla)};
        non_compliant_contacts.add(metrics);
        System.out.println("total_contacts_meeting_sla=" + total_contacts_meeting_sla);
        return non_compliant_contacts;
    }  
    public static ArrayList<ArrayList<String[]>> get_all_list_calc_abandoned_contact_rate(String sla_info[], ArrayList <String[]>contacts) throws Exception
    {
        ArrayList compliant_contacts = new <String[]> ArrayList();
        ArrayList non_compliant_contacts = new <String[]> ArrayList();
        ArrayList all_contacts = new ArrayList<ArrayList<String[]>>();
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_contacts = 0;
        int total_abandoned_contacts = 0;
        int total_contacts_meeting_sla = 0;
        int total_contacts_failing_sla = 0;
        double abandoned_threshold = 60;
        
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_rate target definition:=" + e);
        }
        try
        {
            abandoned_threshold = Double.parseDouble(sla_info[14]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_rate abandoned_threshold definition:=" + e);
        }
        
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_rate sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        
        try
        {
            for(int a = 0; a < contacts.size(); a++)
            {
                String contact[] = contacts.get(a);
                total_contacts++;
                if(contact[6] != null) //has abandoned time set
                {
                    try
                    {
                        int abandoned_time_seconds = Integer.parseInt(contact[6]);
                        if(abandoned_time_seconds > abandoned_threshold)
                        {
                            //is abandoned
                            total_abandoned_contacts++;
                            total_contacts_failing_sla++;
                            non_compliant_contacts.add(contact);
                        }
                        else
                        {
                            compliant_contacts.add(contact);
                            total_contacts_meeting_sla++;
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_rate calc sla value:=" + e);
                    }
                }
            }
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_contact_calc.calc_abandoned_contact_rate calc loop=" + e);
        }
        //add the metrics to the end of the arraylist
        String[] metrics = {String.valueOf(total_contacts),String.valueOf(total_abandoned_contacts),String.valueOf(total_contacts_meeting_sla),String.valueOf(total_contacts_failing_sla)};
        System.out.println("total_contacts_meeting_sla=" + total_contacts_meeting_sla);
        all_contacts.add(compliant_contacts);
        all_contacts.add(non_compliant_contacts);
        all_contacts.add(metrics);
        return all_contacts;
    }  
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static ArrayList<String[]> get_non_compliant_list_calc_abandoned_contact_count(String sla_info[], ArrayList <String[]>contacts) throws Exception
    {
        ArrayList non_compliant_contacts = new <String[]> ArrayList();
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_contacts = 0;
        int total_abandoned_contacts = 0;
        int total_contacts_meeting_sla = 0;
        int total_contacts_failing_sla = 0;
        double abandoned_threshold = 60;
        
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_count target definition:=" + e);
        }
        try
        {
            abandoned_threshold = Double.parseDouble(sla_info[14]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_count abandoned_threshold definition:=" + e);
        }
        
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_count sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        
        try
        {
            for(int a = 0; a < contacts.size(); a++)
            {
                String contact[] = contacts.get(a);
                total_contacts++;
                if(contact[6] != null) //has abandoned time set
                {
                    try
                    {
                        int abandoned_time_seconds = Integer.parseInt(contact[6]);
                        if(abandoned_time_seconds > abandoned_threshold)
                        {
                            //is abandoned
                            total_abandoned_contacts++;
                            total_contacts_failing_sla++;
                            non_compliant_contacts.add(contact);
                        }
                        else
                        {
                            total_contacts_meeting_sla++;
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_count calc sla value:=" + e);
                    }
                }
            }
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_contact_calc.calc_abandoned_contact_count calc loop=" + e);
        }
        //add the metrics to the end of the arraylist
        String metrics[] = {String.valueOf(total_contacts),String.valueOf(total_abandoned_contacts),String.valueOf(total_contacts_meeting_sla),String.valueOf(total_contacts_failing_sla)};
        non_compliant_contacts.add(metrics);
        return non_compliant_contacts;
    }
    public static ArrayList<ArrayList<String[]>> get_all_list_calc_abandoned_contact_count(String sla_info[], ArrayList <String[]>contacts) throws Exception
    {
        ArrayList compliant_contacts = new <String[]> ArrayList();
        ArrayList non_compliant_contacts = new <String[]> ArrayList();
        ArrayList all_contacts = new ArrayList<ArrayList<String[]>>();
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_contacts = 0;
        int total_abandoned_contacts = 0;
        int total_contacts_meeting_sla = 0;
        int total_contacts_failing_sla = 0;
        double abandoned_threshold = 60;
        
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_count target definition:=" + e);
        }
        try
        {
            abandoned_threshold = Double.parseDouble(sla_info[14]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_count abandoned_threshold definition:=" + e);
        }
        
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_count sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        
        try
        {
            for(int a = 0; a < contacts.size(); a++)
            {
                String contact[] = contacts.get(a);
                total_contacts++;
                if(contact[6] != null) //has abandoned time set
                {
                    try
                    {
                        int abandoned_time_seconds = Integer.parseInt(contact[6]);
                        if(abandoned_time_seconds > abandoned_threshold)
                        {
                            //is abandoned
                            total_abandoned_contacts++;
                            total_contacts_failing_sla++;
                            non_compliant_contacts.add(contact);
                        }
                        else
                        {
                            compliant_contacts.add(contact);
                            total_contacts_meeting_sla++;
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception in sla_contact_calc.for_sla_id.calc_abandoned_contact_count calc sla value:=" + e);
                    }
                }
            }
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_contact_calc.calc_abandoned_contact_count calc loop=" + e);
        }
        //add the metrics to the end of the arraylist
        String metrics[] = {String.valueOf(total_contacts),String.valueOf(total_abandoned_contacts),String.valueOf(total_contacts_meeting_sla),String.valueOf(total_contacts_failing_sla)};
        all_contacts.add(compliant_contacts);
        all_contacts.add(non_compliant_contacts);
        all_contacts.add(metrics);
        return all_contacts;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static ArrayList<String[]>  get_non_compliant_list_calc_average_speed_to_answer(String sla_info[], ArrayList <String[]>contacts) throws Exception
    {
        ArrayList non_compliant_contacts = new <String[]> ArrayList();
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_contacts = 0;
        int total_answered_calls = 0;
        int total_abandoned_contacts = 0;
        int total_contacts_meeting_sla = 0;
        int total_contacts_failing_sla = 0;
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_speed_to_answer target definition:=" + e);
        }
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_speed_to_answer sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        //for each contact        
            //get the "accept_time_seconds"            
            //use operator to determine good or bad
            //increment good/bad counts
        //end for
        try
        {
            for(int a = 0; a < contacts.size(); a++)
            {
                String contact[] = contacts.get(a);
                total_contacts++;
                if(contact[4] != null) //was it answered?
                {
                    int accept_time_seconds = 0;
                    try
                    {
                        accept_time_seconds = Integer.parseInt(contact[4]);
                        total_answered_calls++;
                        //{"greater_than", "greater_than_or_equal", "equal_to", "not_equal_to", "less_than_or_equal", "less_than"}
                        switch(sla_operator) 
                        { 
                            case "greater_than": 
                                if(accept_time_seconds > sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "greater_than_or_equal":
                                if(accept_time_seconds >= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "equal_to": 
                                if(accept_time_seconds == sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "not_equal_to": 
                                if(accept_time_seconds != sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "less_than_or_equal": 
                                if(accept_time_seconds <= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "less_than": 
                                if(accept_time_seconds < sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            default: 
                                //System.out.println("no match");     
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception in sla_contact_calc.for_sla_id.get_non_compliant_list_calc_average_speed_to_answer calc sla value:=" + e);
                    }                
                }
                else
                {
                    total_abandoned_contacts++;
                }
            } 
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_contact_calc.get_non_compliant_list_calc_average_speed_to_answer calc loop=" + e);
        }
        //add the metrics to the end of the arraylist
        String metrics[] = {String.valueOf(total_contacts),String.valueOf(total_abandoned_contacts),String.valueOf(total_contacts_meeting_sla),String.valueOf(total_contacts_failing_sla)};
        non_compliant_contacts.add(metrics);
        return non_compliant_contacts;
    }
    public static ArrayList<ArrayList<String[]>>  get_all_list_calc_average_speed_to_answer(String sla_info[], ArrayList <String[]>contacts) throws Exception
    {
        ArrayList non_compliant_contacts = new <String[]> ArrayList();
        ArrayList compliant_contacts = new <String[]> ArrayList();
        ArrayList all_contacts = new ArrayList<ArrayList<String[]>>();
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_contacts = 0;
        int total_answered_calls = 0;
        int total_abandoned_contacts = 0;
        int total_contacts_meeting_sla = 0;
        int total_contacts_failing_sla = 0;
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_speed_to_answer target definition:=" + e);
        }
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_speed_to_answer sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        //for each contact        
            //get the "accept_time_seconds"            
            //use operator to determine good or bad
            //increment good/bad counts
        //end for
        try
        {
            for(int a = 0; a < contacts.size(); a++)
            {
                String contact[] = contacts.get(a);
                total_contacts++;
                if(contact[4] != null) //was it answered?
                {
                    int accept_time_seconds = 0;
                    try
                    {
                        accept_time_seconds = Integer.parseInt(contact[4]);
                        total_answered_calls++;
                        //{"greater_than", "greater_than_or_equal", "equal_to", "not_equal_to", "less_than_or_equal", "less_than"}
                        switch(sla_operator) 
                        { 
                            case "greater_than": 
                                if(accept_time_seconds > sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "greater_than_or_equal":
                                if(accept_time_seconds >= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "equal_to": 
                                if(accept_time_seconds == sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "not_equal_to": 
                                if(accept_time_seconds != sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "less_than_or_equal": 
                                if(accept_time_seconds <= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "less_than": 
                                if(accept_time_seconds < sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            default: 
                                //System.out.println("no match");     
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception in sla_contact_calc.for_sla_id.get_non_compliant_list_calc_average_speed_to_answer calc sla value:=" + e);
                    }                
                }
                else
                {
                    total_abandoned_contacts++;
                }
            } 
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_contact_calc.get_non_compliant_list_calc_average_speed_to_answer calc loop=" + e);
        }
        //add the metrics to the end of the arraylist
        String metrics[] = {String.valueOf(total_contacts),String.valueOf(total_abandoned_contacts),String.valueOf(total_contacts_meeting_sla),String.valueOf(total_contacts_failing_sla)};
        all_contacts.add(compliant_contacts);
        all_contacts.add(non_compliant_contacts);
        all_contacts.add(metrics);
        return all_contacts;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static ArrayList<String[]> get_non_compliant_list_calc_average_post_process_time(String sla_info[], ArrayList <String[]>contacts) throws Exception
    {
        ArrayList non_compliant_contacts = new <String[]> ArrayList();
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_contacts = 0;
        int total_answered_calls = 0;
        int total_abandoned_contacts = 0;
        int total_contacts_meeting_sla = 0;
        int total_contacts_failing_sla = 0;
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_port_processing_time target definition:=" + e);
        }
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_port_processing_time sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        //for each contact        
            //get the "accept_time_seconds"            
            //use operator to determine good or bad
            //increment good/bad counts
        //end for
        try
        {
            for(int a = 0; a < contacts.size(); a++)
            {
                String contact[] = contacts.get(a);
                total_contacts++;
                if(contact[4] != null) //was it answered?
                {
                    int post_process_time = 0;
                    try
                    {
                        post_process_time = Integer.parseInt(contact[11]);
                        total_answered_calls++;
                        //{"greater_than", "greater_than_or_equal", "equal_to", "not_equal_to", "less_than_or_equal", "less_than"}
                        switch(sla_operator) 
                        { 
                            case "greater_than": 
                                if(post_process_time > sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "greater_than_or_equal":
                                if(post_process_time >= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "equal_to": 
                                if(post_process_time == sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "not_equal_to": 
                                if(post_process_time != sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "less_than_or_equal": 
                                if(post_process_time <= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "less_than": 
                                if(post_process_time < sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            default: 
                                //System.out.println("no match");     
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception in sla_contact_calc.for_sla_id.get_non_compliant_list_calc_average_post_process_time calc sla value:=" + e);
                    }                
                }
                else
                {
                    total_abandoned_contacts++;
                }
            } 
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_contact_calc.get_non_compliant_list_calc_average_post_process_time calc loop=" + e);
        }
        //add the metrics to the end of the arraylist
        String metrics[] = {String.valueOf(total_contacts),String.valueOf(total_abandoned_contacts),String.valueOf(total_contacts_meeting_sla),String.valueOf(total_contacts_failing_sla)};
        non_compliant_contacts.add(metrics);
        return non_compliant_contacts;
    }
    public static ArrayList<String[]> get_all_list_calc_average_post_process_time(String sla_info[], ArrayList <String[]>contacts) throws Exception
    {
        ArrayList non_compliant_contacts = new <String[]> ArrayList();
        ArrayList compliant_contacts = new <String[]> ArrayList();
        ArrayList all_contacts = new ArrayList<ArrayList<String[]>>();
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_contacts = 0;
        int total_answered_calls = 0;
        int total_abandoned_contacts = 0;
        int total_contacts_meeting_sla = 0;
        int total_contacts_failing_sla = 0;
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_port_processing_time target definition:=" + e);
        }
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.calc_average_port_processing_time sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        //for each contact        
            //get the "accept_time_seconds"            
            //use operator to determine good or bad
            //increment good/bad counts
        //end for
        try
        {
            for(int a = 0; a < contacts.size(); a++)
            {
                String contact[] = contacts.get(a);
                total_contacts++;
                if(contact[4] != null) //was it answered?
                {
                    int post_process_time = 0;
                    try
                    {
                        post_process_time = Integer.parseInt(contact[11]);
                        total_answered_calls++;
                        //{"greater_than", "greater_than_or_equal", "equal_to", "not_equal_to", "less_than_or_equal", "less_than"}
                        switch(sla_operator) 
                        { 
                            case "greater_than": 
                                if(post_process_time > sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "greater_than_or_equal":
                                if(post_process_time >= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "equal_to": 
                                if(post_process_time == sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "not_equal_to": 
                                if(post_process_time != sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "less_than_or_equal": 
                                if(post_process_time <= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "less_than": 
                                if(post_process_time < sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            default: 
                                //System.out.println("no match");     
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception in sla_contact_calc.for_sla_id.get_non_compliant_list_calc_average_post_process_time calc sla value:=" + e);
                    }                
                }
                else
                {
                    total_abandoned_contacts++;
                }
            } 
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_contact_calc.get_non_compliant_list_calc_average_post_process_time calc loop=" + e);
        }
        //add the metrics to the end of the arraylist
        String metrics[] = {String.valueOf(total_contacts),String.valueOf(total_abandoned_contacts),String.valueOf(total_contacts_meeting_sla),String.valueOf(total_contacts_failing_sla)};
        all_contacts.add(compliant_contacts);
        all_contacts.add(non_compliant_contacts);
        all_contacts.add(metrics);
        return all_contacts;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static ArrayList<String[]> get_non_compliant_list_calc_average_handle_time(String sla_info[], ArrayList <String[]>contacts) throws Exception
    {
        ArrayList non_compliant_contacts = new <String[]> ArrayList();
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_contacts = 0;
        int total_answered_calls = 0;
        int total_abandoned_contacts = 0;
        int total_contacts_meeting_sla = 0;
        int total_contacts_failing_sla = 0;
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.get_non_compliant_list_calc_average_handle_time target definition:=" + e);
        }
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.get_non_compliant_list_calc_average_handle_time sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        //for each contact        
            //get the "accept_time_seconds"            
            //use operator to determine good or bad
            //increment good/bad counts
        //end for
        try
        {
            for(int a = 0; a < contacts.size(); a++)
            {
                total_contacts++;
                String contact[] = contacts.get(a);
                if(contact[4] != null) //was it answered?
                {
                    int total_time = 0;
                    try
                    {
                        total_time = Integer.parseInt(contact[13]);
                        total_answered_calls++;
                        //{"greater_than", "greater_than_or_equal", "equal_to", "not_equal_to", "less_than_or_equal", "less_than"}
                        switch(sla_operator) 
                        { 
                            case "greater_than": 
                                if(total_time > sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "greater_than_or_equal":
                                if(total_time >= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "equal_to": 
                                if(total_time == sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "not_equal_to": 
                                if(total_time != sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "less_than_or_equal": 
                                if(total_time <= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "less_than": 
                                if(total_time < sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            default: 
                                //System.out.println("no match");     
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception in sla_contact_calc.for_sla_id.get_non_compliant_list_calc_average_handle_time calc sla value:=" + e);
                    }                
                }
                else
                {
                    total_abandoned_contacts++;
                }
            } 
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_contact_calc.get_non_compliant_list_calc_average_handle_time calc loop=" + e);
        }
        //add the metrics to the end of the arraylist
        String metrics[] = {String.valueOf(total_contacts),String.valueOf(total_abandoned_contacts),String.valueOf(total_contacts_meeting_sla),String.valueOf(total_contacts_failing_sla)};
        non_compliant_contacts.add(metrics);
        return non_compliant_contacts;
    }
    public static ArrayList<ArrayList<String[]>> get_all_list_calc_average_handle_time(String sla_info[], ArrayList <String[]>contacts) throws Exception
    {
        ArrayList non_compliant_contacts = new <String[]> ArrayList();
        ArrayList compliant_contacts = new <String[]> ArrayList();
        ArrayList all_contacts = new ArrayList<ArrayList<String[]>>();
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_contacts = 0;
        int total_answered_calls = 0;
        int total_abandoned_contacts = 0;
        int total_contacts_meeting_sla = 0;
        int total_contacts_failing_sla = 0;
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.get_non_compliant_list_calc_average_handle_time target definition:=" + e);
        }
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.get_non_compliant_list_calc_average_handle_time sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        //for each contact        
            //get the "accept_time_seconds"            
            //use operator to determine good or bad
            //increment good/bad counts
        //end for
        try
        {
            for(int a = 0; a < contacts.size(); a++)
            {
                total_contacts++;
                String contact[] = contacts.get(a);
                if(contact[4] != null) //was it answered?
                {
                    int total_time = 0;
                    try
                    {
                        total_time = Integer.parseInt(contact[13]);
                        total_answered_calls++;
                        //{"greater_than", "greater_than_or_equal", "equal_to", "not_equal_to", "less_than_or_equal", "less_than"}
                        switch(sla_operator) 
                        { 
                            case "greater_than": 
                                if(total_time > sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "greater_than_or_equal":
                                if(total_time >= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "equal_to": 
                                if(total_time == sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "not_equal_to": 
                                if(total_time != sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "less_than_or_equal": 
                                if(total_time <= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "less_than": 
                                if(total_time < sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            default: 
                                //System.out.println("no match");     
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception in sla_contact_calc.for_sla_id.get_non_compliant_list_calc_average_handle_time calc sla value:=" + e);
                    }                
                }
                else
                {
                    total_abandoned_contacts++;
                }
            } 
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_contact_calc.get_non_compliant_list_calc_average_handle_time calc loop=" + e);
        }
        //add the metrics to the end of the arraylist
        String metrics[] = {String.valueOf(total_contacts),String.valueOf(total_abandoned_contacts),String.valueOf(total_contacts_meeting_sla),String.valueOf(total_contacts_failing_sla)};
        all_contacts.add(compliant_contacts);
        all_contacts.add(non_compliant_contacts);
        all_contacts.add(metrics);
        return all_contacts;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static ArrayList<String[]> get_non_compliant_list_calc_average_process_time(String sla_info[], ArrayList <String[]>contacts) throws Exception
    {
        ArrayList non_compliant_contacts = new <String[]> ArrayList();
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_contacts = 0;
        int total_answered_calls = 0;
        int total_abandoned_contacts = 0;
        int total_contacts_meeting_sla = 0;
        int total_contacts_failing_sla = 0;
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.get_non_compliant_list_calc_average_process_time target definition:=" + e);
        }
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.get_non_compliant_list_calc_average_process_time sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        //for each contact        
            //get the "accept_time_seconds"            
            //use operator to determine good or bad
            //increment good/bad counts
        //end for
        try
        {
            for(int a = 0; a < contacts.size(); a++)
            {
                total_contacts++;
                String contact[] = contacts.get(a);
                if(contact[4] != null) //was it answered?
                {
                    int process_time = 0;
                    try
                    {
                        process_time = Integer.parseInt(contact[12]);
                        total_answered_calls++;
                        //{"greater_than", "greater_than_or_equal", "equal_to", "not_equal_to", "less_than_or_equal", "less_than"}
                        switch(sla_operator) 
                        { 
                            case "greater_than": 
                                if(process_time > sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "greater_than_or_equal":
                                if(process_time >= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "equal_to": 
                                if(process_time == sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "not_equal_to": 
                                if(process_time != sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "less_than_or_equal": 
                                if(process_time <= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "less_than": 
                                if(process_time < sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            default: 
                                //System.out.println("no match");     
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception in sla_contact_calc.for_sla_id.get_non_compliant_list_calc_average_process_time calc sla value:=" + e);
                    }                
                }
                else
                {
                    total_abandoned_contacts++;
                }
            } 
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_contact_calc.get_non_compliant_list_calc_average_process_time calc loop=" + e);
        }
        //add the metrics to the end of the arraylist
        String metrics[] = {String.valueOf(total_contacts),String.valueOf(total_abandoned_contacts),String.valueOf(total_contacts_meeting_sla),String.valueOf(total_contacts_failing_sla)};
        non_compliant_contacts.add(metrics);
        return non_compliant_contacts;
    }
    public static ArrayList<ArrayList<String[]>> get_all_list_calc_average_process_time(String sla_info[], ArrayList <String[]>contacts) throws Exception
    {
        ArrayList non_compliant_contacts = new <String[]> ArrayList();
        ArrayList compliant_contacts = new <String[]> ArrayList();
        ArrayList all_contacts = new ArrayList<ArrayList<String[]>>();
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_contacts = 0;
        int total_answered_calls = 0;
        int total_abandoned_contacts = 0;
        int total_contacts_meeting_sla = 0;
        int total_contacts_failing_sla = 0;
        try
        {
            target = Double.parseDouble(sla_info[5]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.get_non_compliant_list_calc_average_process_time target definition:=" + e);
        }
        sla_operator = sla_info[15];
        try
        {
            sla_value = Double.parseDouble(sla_info[16]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_contact_calc.for_sla_id.get_non_compliant_list_calc_average_process_time sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[17];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        //for each contact        
            //get the "accept_time_seconds"            
            //use operator to determine good or bad
            //increment good/bad counts
        //end for
        try
        {
            for(int a = 0; a < contacts.size(); a++)
            {
                total_contacts++;
                String contact[] = contacts.get(a);
                if(contact[4] != null) //was it answered?
                {
                    int process_time = 0;
                    try
                    {
                        process_time = Integer.parseInt(contact[12]);
                        total_answered_calls++;
                        //{"greater_than", "greater_than_or_equal", "equal_to", "not_equal_to", "less_than_or_equal", "less_than"}
                        switch(sla_operator) 
                        { 
                            case "greater_than": 
                                if(process_time > sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "greater_than_or_equal":
                                if(process_time >= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "equal_to": 
                                if(process_time == sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "not_equal_to": 
                                if(process_time != sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "less_than_or_equal": 
                                if(process_time <= sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            case "less_than": 
                                if(process_time < sla_value)
                                {
                                    total_contacts_meeting_sla++;
                                    compliant_contacts.add(contact);
                                }
                                else
                                {
                                    total_contacts_failing_sla++;
                                    non_compliant_contacts.add(contact);
                                }
                                break; 
                            default: 
                                //System.out.println("no match");     
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception in sla_contact_calc.for_sla_id.get_non_compliant_list_calc_average_process_time calc sla value:=" + e);
                    }                
                }
                else
                {
                    total_abandoned_contacts++;
                }
            } 
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_contact_calc.get_non_compliant_list_calc_average_process_time calc loop=" + e);
        }
        //add the metrics to the end of the arraylist
        String metrics[] = {String.valueOf(total_contacts),String.valueOf(total_abandoned_contacts),String.valueOf(total_contacts_meeting_sla),String.valueOf(total_contacts_failing_sla)};
        all_contacts.add(compliant_contacts);
        all_contacts.add(non_compliant_contacts);
        all_contacts.add(metrics);
        return all_contacts;
    }
}

/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package support;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class data_check 
{
    private static Logger logger = LogManager.getLogger();
    
    public static String null_string(String value) throws Exception
    {
        String return_value = "";
        
        try
        {
            if(value == null || value.equalsIgnoreCase("null"))
            {
                return_value = "";
            }
            else
            {
                return_value = value;
            }
        }
        catch(Exception e)
        {
            return_value = "";
        }        
        return return_value;
    }
    
}

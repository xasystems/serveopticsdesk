package support;



/*
 * Copyright 2019 Real Data Technologies, Inc. , All rights reserved.
 */


import javax.servlet.http.HttpSessionListener;
import javax.servlet.http.HttpSessionEvent;
/**
 *
 * @author Ralph
 */
public class session_listener implements HttpSessionListener
{
    private static int activeSessions = 0;
    
    public void sessionCreated(HttpSessionEvent se) 
    {
        activeSessions++;
    }
    public void sessionDestroyed(HttpSessionEvent se) 
    {
        if(activeSessions > 0)
            activeSessions--;
    }
    public static int getActiveSessions() 
    {
        return activeSessions;
    }
}


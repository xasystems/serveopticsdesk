/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package support;

import db.get_users;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class sla_request_calc 
{
    private static final Logger logger = LogManager.getLogger(sla_request_calc.class.getName());
    private static SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
    private static SimpleDateFormat timestamp2 = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");  
    
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    public static int get_day_of_week(java.util.Date date) 
    {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.get(cal.DAY_OF_WEEK);
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    private static Long actual_get_time_to_close(java.util.Date start, java.util.Date end)
    {
        long return_long = 0;
        System.out.println("start=" + start + " end=" + end);
        return_long = end.getTime() - start.getTime();
        return return_long;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    private static Long get_max_allowed_time_msec(int number, String units)
    {
        long return_long = 0;
        if(units.equalsIgnoreCase("minutes"))
        {
            return_long = TimeUnit.MINUTES.toMillis(number);
        }
        else if(units.equalsIgnoreCase("hours"))
        {
            return_long = TimeUnit.HOURS.toMillis(number);
        }
        else if(units.equalsIgnoreCase("days"))
        {
            return_long = TimeUnit.DAYS.toMillis(number);
        } 
        return return_long;
    }    
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    private static int[] schedule_time_to_int_array(String time)
    {
        int return_array[] = {0,0,0};
        try
        {
            //System.out.println("time=" + time);
            String temp[] = time.split(":");
            int hour = Integer.parseInt(temp[0]);
            int minute = Integer.parseInt(temp[1]);
            int second = Integer.parseInt(temp[2]);
            return_array[0] = hour;
            return_array[1] = minute;
            return_array[2] = second;
        }
        catch(Exception e)
        {
            System.out.println("Exception in sla_request_calc.schedule_time_to_int_array=" + e);
        }        
        return return_array;
    }   
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    private static int[] schedule_day_to_int_array(String schedule[])
    {
        int return_array[] = {0,0,0,0,0,0,0,0};
        try
        {
            if(schedule[7].equalsIgnoreCase("0"))
            {
                return_array[1] = 0;
            }
            else
            {
                return_array[1] = 1;
            }
            if(schedule[8].equalsIgnoreCase("0"))
            {
                return_array[2] = 0;
            }
            else
            {
                return_array[2] = 1;
            }
            if(schedule[9].equalsIgnoreCase("0"))
            {
                return_array[3] = 0;
            }
            else
            {
                return_array[3] = 1;
            }
            if(schedule[10].equalsIgnoreCase("0"))
            {
                return_array[4] = 0;
            }
            else
            {
                return_array[4] = 1;
            }
            if(schedule[11].equalsIgnoreCase("0"))
            {
                return_array[5] = 0;
            }
            else
            {
                return_array[5] = 1;
            }
            if(schedule[12].equalsIgnoreCase("0"))
            {
                return_array[6] = 0;
            }
            else
            {
                return_array[6] = 1;
            }
            if(schedule[13].equalsIgnoreCase("0"))
            {
                return_array[7] = 0;
            }
            else
            {
                return_array[7] = 1;
            }
        }
        catch(Exception e)
        {
            System.out.println("Exception in sla_request_calc.schedule_day_to_int_array=" + e);
        }        
        return return_array;
    } 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    private static List<Integer> load_not_business_days(String schedule[])
    {
        List<Integer> NON_BUSINESS_DAYS = new ArrayList<Integer> ();     
        try
        {
            if(schedule[7].equalsIgnoreCase("0"))  //sunday
            {
                NON_BUSINESS_DAYS.add(1);
            }
            if(schedule[8].equalsIgnoreCase("0")) //mon
            {
                NON_BUSINESS_DAYS.add(2);
            }
            if(schedule[9].equalsIgnoreCase("0")) //tue
            {
                NON_BUSINESS_DAYS.add(3);
            }
            if(schedule[10].equalsIgnoreCase("0")) //wed
            {
                NON_BUSINESS_DAYS.add(4);
            }
            if(schedule[11].equalsIgnoreCase("0")) //thu
            {
                NON_BUSINESS_DAYS.add(5);
            }
            if(schedule[12].equalsIgnoreCase("0")) //fri
            {
                NON_BUSINESS_DAYS.add(6);
            }
            if(schedule[13].equalsIgnoreCase("0")) //sat
            {
                NON_BUSINESS_DAYS.add(7);
            }
        }
        catch(Exception e)
        {
            System.out.println("Exception in sla_request_calc.load_not_business_days=" + e);
        } 
        //for(int a = 0; a < NON_BUSINESS_DAYS.size(); a++)
        //{
        //    System.out.println("not business day=" + NON_BUSINESS_DAYS.get(a));
        //}
        return NON_BUSINESS_DAYS;
    } 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    private static java.util.Date schedule_time_to_date(int schedule_time[], java.util.Date date)
    {
        java.util.Date return_date = new java.util.Date();
        try
        {
            GregorianCalendar schedule_cal = new GregorianCalendar();
            schedule_cal.setTime(date); //this sets day/Month/Year
            schedule_cal.set(schedule_cal.HOUR_OF_DAY,schedule_time[0]);
            schedule_cal.set(schedule_cal.MINUTE,schedule_time[1]);
            schedule_cal.set(schedule_cal.SECOND,schedule_time[2]);
            return_date.setTime(schedule_cal.getTimeInMillis());            
        }
        catch(Exception e)
        {
            System.out.println("Exception in sla_request_calc.schedule_time_to_date=" + e);
        }        
        return return_date;
    }  
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////      
    public static boolean for_request_id(Connection con, String[] request) throws Exception, SQLException
    {
        boolean pass = false;
        String sc_item_info[] = db.get_service_catalog.service_catalog_items_by_id(con, request[4]);
        java.util.Date req_start_date = timestamp.parse(request[6]);
        java.util.Date req_closed_date = timestamp.parse(request[9]);
        System.out.println("RequestID=" + request[0] + " Priority=" + request[21] + " Req Date=" + request[6] + " Closed Date=" + request[9] + " Diff=" + (req_closed_date.getTime() - req_start_date.getTime()));
        long max_allowed_time_msec = 00;
        if(request[21].equalsIgnoreCase("critical"))//if priority = critical
        {
            max_allowed_time_msec = get_max_allowed_time_msec(Integer.parseInt(sc_item_info[10]), sc_item_info[11]);
        }
        else if(request[21].equalsIgnoreCase("high"))//if priority = critical
        {
            max_allowed_time_msec = get_max_allowed_time_msec(Integer.parseInt(sc_item_info[17]), sc_item_info[18]);
        }
        else if(request[21].equalsIgnoreCase("medium"))//if priority = critical
        {
            max_allowed_time_msec = get_max_allowed_time_msec(Integer.parseInt(sc_item_info[24]), sc_item_info[25]);
        }
        else if(request[21].equalsIgnoreCase("low"))//if priority = critical
        {
            max_allowed_time_msec = get_max_allowed_time_msec(Integer.parseInt(sc_item_info[31]), sc_item_info[32]);
        }            
        System.out.println("max_allowed_time_msec=" + max_allowed_time_msec);
            
        //calc actual_total time to close
        long actual_time_to_close_without_schedule_removed = actual_get_time_to_close(req_start_date, req_closed_date);  
        long actual_time_to_close_with_schedule_removed = 00;
        System.out.println("actual_time_to_close_without_schedule_removed=" + actual_time_to_close_without_schedule_removed);
        //if actual < max_allowed dont bother removing schedules from actual time
        if(actual_time_to_close_without_schedule_removed < max_allowed_time_msec) //quick check before removing schedule
        {
            System.out.println("QUICK CHECK SLA PASS  actual_time_to_close_without_schedule_removed < max_allowed_time_msec=" +  (max_allowed_time_msec - actual_time_to_close_without_schedule_removed));
            pass = true;
        }
        else
        {
            System.out.println("FAILED QUICK CHECK START FULL SLA CHECK");
            //get schedule
            String schedule[] = db.get_schedules.by_id(con, sc_item_info[12]);
            System.out.println("Schedule=" + schedule[1]);
            int schedule_start[] = schedule_time_to_int_array(schedule[3]);
            int schedule_end[] = schedule_time_to_int_array(schedule[4]);
            //build the schedule array //0 not used  //1=Sunday   //2=Monday  //3=Tuesday  //4=Wednesday   //5=Thursday  //6=Friday //7=Saturday
            int schedule_week[] = schedule_day_to_int_array(schedule);
            //get the number of days from request_date to closeLocalDateTime oldDate = LocalDateTime.of(1982, Month.AUGUST, 31, 10, 20, 55);
            LocalDate start_localdate = LocalDate.parse( new SimpleDateFormat("yyyy-MM-dd").format(req_start_date) );
            LocalDate end_localdate = LocalDate.parse( new SimpleDateFormat("yyyy-MM-dd").format(req_closed_date) );
            long days = ChronoUnit.DAYS.between(start_localdate, end_localdate);
            System.out.println(days + " days");
            
            //create a array of start and stop times by the day
            java.util.Date req_days[][] = new java.util.Date[0][0];
            if(days == 0)
            {
                req_days = new java.util.Date[1][2];
                req_days[0][0] = req_start_date;
                req_days[0][1] = req_closed_date;
                java.util.Date schedule_start_date = schedule_time_to_date(schedule_start,req_start_date);
                java.util.Date schedule_end_date = schedule_time_to_date(schedule_end,req_closed_date);
                if(schedule_week[get_day_of_week(req_days[0][0])] == 1)
                {
                    //case1 starts before, ends after
                    if((req_start_date.getTime() < schedule_start_date.getTime()) && (req_closed_date.getTime() > schedule_end_date.getTime()))
                    {
                        System.out.println("case 1");
                        req_days[0][0] = schedule_start_date;
                        req_days[0][1] = schedule_end_date;
                    }
                    //case2 starts during, end after
                    else if((req_start_date.getTime() > schedule_start_date.getTime() && (req_start_date.getTime() < schedule_end_date.getTime())) && (req_closed_date.getTime() > schedule_end_date.getTime()))
                    {
                        System.out.println("case 2");
                        req_days[0][0] = req_start_date;
                        req_days[0][1] = schedule_end_date;
                    }
                    //case3 starts after, ends after
                    else if(req_start_date.getTime() > schedule_end_date.getTime())
                    {
                        System.out.println("case 3");
                        req_days[0][0] = req_start_date;
                        req_days[0][1] = req_start_date;
                    }                
                    //case4 starts before, ends before
                    else if(req_closed_date.getTime() < schedule_start_date.getTime())
                    {
                        System.out.println("case 4");
                        req_days[0][0] = req_start_date;
                        req_days[0][1] = req_start_date;
                    }
                    //case5 starts during, ends during
                    else if((req_start_date.getTime() > schedule_start_date.getTime() && req_start_date.getTime() < schedule_end_date.getTime()) && (req_closed_date.getTime() < schedule_end_date.getTime()))
                    {
                        System.out.println("case 5");
                        req_days[0][0] = req_start_date;
                        req_days[0][1] = req_closed_date;
                    }  
                    //case6 starts before, ends during
                    if((req_start_date.getTime() < schedule_start_date.getTime() && (req_closed_date.getTime() > schedule_start_date.getTime()) && req_closed_date.getTime() < schedule_end_date.getTime()))
                    {
                        System.out.println("case 6");
                        req_days[0][0] = schedule_start_date;
                        req_days[0][1] = req_closed_date;
                    }
                }
                else
                {
                    //set start and stop to the smae date so the diff is 0
                    req_days[0][1] = req_days[0][0];
                }
            }
            else
            {
                System.out.println(days + " days greater than 0");
                req_days = new java.util.Date[(int)days + 1][2];
                java.util.Date this_start = req_start_date;
                java.util.Date this_end = req_closed_date;   
                //do first day
                req_days[0][0] = this_start;
                GregorianCalendar cal = new GregorianCalendar();
                cal.setTime(this_start);
                cal.set(cal.HOUR_OF_DAY, 23);
                cal.set(cal.MINUTE, 59);
                cal.set(cal.SECOND, 59);
                req_days[0][1] = cal.getTime();
                //do the other days
                for(int b = 1; b < ((int)days); b++) //skip the first and last record
                {
                    System.out.println("Filling in day=" + b);
                    cal.add(cal.SECOND, 1);
                    req_days[b][0] = cal.getTime();                                
                    cal.set(cal.HOUR_OF_DAY, 23);
                    cal.set(cal.MINUTE, 59);
                    cal.set(cal.SECOND, 59);
                    req_days[b][1] = cal.getTime();  
                } 
                //do last day    
                System.out.println("last day=");
                cal = new GregorianCalendar();
                cal.setTime(this_end);
                cal.set(cal.HOUR_OF_DAY, 0);
                cal.set(cal.MINUTE, 0);
                cal.set(cal.SECOND, 0);
                req_days[(int)days][0] = cal.getTime();
                req_days[(int)days][1] = this_end;
                System.out.println("end last day=");

            }  
            System.out.println("req_days.length=" + req_days.length);
            for(int c = 0; c < req_days.length; c++)
            {
                java.util.Date schedule_start_date = schedule_time_to_date(schedule_start,req_days[c][0]);
                java.util.Date schedule_end_date = schedule_time_to_date(schedule_end,req_days[c][1]);
                
                System.out.println("      FOR start=" + req_days[c][0] + " start dayofweek=" + get_day_of_week(req_days[c][0]) + " Stop=" + req_days[c][1] + " Diff=" + (req_days[c][1].getTime() - req_days[c][0].getTime()));
                //subtract schedule from total time

                //Does this day get counted?
                if(schedule_week[get_day_of_week(req_days[c][0])] == 1)
                {
                    //case1 starts before, ends after
                    if((req_start_date.getTime() < schedule_start_date.getTime()) && (req_closed_date.getTime() > schedule_end_date.getTime()))
                    {
                        System.out.println("case 1");
                        req_days[c][0] = schedule_start_date;
                        req_days[c][1] = schedule_end_date;
                    }
                    //case2 starts during, end after
                    else if((req_start_date.getTime() > schedule_start_date.getTime() && (req_start_date.getTime() < schedule_end_date.getTime())) && (req_closed_date.getTime() > schedule_end_date.getTime()))
                    {
                        System.out.println("case 2");
                        req_days[c][0] = req_start_date;
                        req_days[c][1] = schedule_end_date;
                    }
                    //case3 starts after, ends after
                    else if(req_start_date.getTime() > schedule_end_date.getTime())
                    {
                        System.out.println("case 3");
                        req_days[c][0] = req_start_date;
                        req_days[c][1] = req_start_date;
                    }                
                    //case4 starts before, ends before
                    else if(req_closed_date.getTime() < schedule_start_date.getTime())
                    {
                        System.out.println("case 4");
                        req_days[c][0] = req_start_date;
                        req_days[c][1] = req_start_date;
                    }
                    //case5 starts during, ends during
                    else if((req_start_date.getTime() > schedule_start_date.getTime() && req_start_date.getTime() < schedule_end_date.getTime()) && (req_closed_date.getTime() < schedule_end_date.getTime()))
                    {
                        System.out.println("case 5");
                        req_days[c][0] = req_start_date;
                        req_days[c][1] = req_closed_date;
                    }  
                    //case6 starts before, ends during
                    if((req_start_date.getTime() < schedule_start_date.getTime() && (req_closed_date.getTime() > schedule_start_date.getTime()) && req_closed_date.getTime() < schedule_end_date.getTime()))
                    {
                        System.out.println("case 6");
                        req_days[c][0] = schedule_start_date;
                        req_days[c][1] = req_closed_date;
                    }
                }
                else
                {
                    //set start and stop to the smae date so the diff is 0
                    System.out.println("case 0");
                    req_days[c][1] = req_days[c][0];
                }
                System.out.println("AFTER FOR start=" + req_days[c][0] + " start dayofweek=" + get_day_of_week(req_days[c][0]) + " Stop=" + req_days[c][1] + " Diff=" + (req_days[c][1].getTime() - req_days[c][0].getTime()));
                System.out.println("-----------------------------------------------------------------------------------------------------------------------------");
            }
            for(int c = 0; c < req_days.length; c++)
            {
                long date_dif = req_days[c][1].getTime() - req_days[c][0].getTime();                    
                actual_time_to_close_with_schedule_removed = actual_time_to_close_with_schedule_removed + date_dif;
            }
        }            
        if(actual_time_to_close_with_schedule_removed < max_allowed_time_msec) //quick check before removing schedule
        {
            System.out.println("FULL CHECK SLA PASS  actual_time_to_close_with_schedule_removed < max_allowed_time_msec=" +  (max_allowed_time_msec - actual_time_to_close_with_schedule_removed));
            pass = true;
        }
        else
        {
            System.out.println("FULL CHECK SLA FAIL  actual_time_to_close_with_schedule_removed < max_allowed_time_msec=" +  max_allowed_time_msec + " actual=" + actual_time_to_close_with_schedule_removed + " diff=" + (actual_time_to_close_with_schedule_removed - max_allowed_time_msec));
            pass = false;
        }
        return pass;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    private static int convert_unit_to_cal_int(String unit)
    {
        int return_int = 0;
        try
        {
            if(unit.equalsIgnoreCase("days"))
            {
                return_int = Calendar.DATE;
                //System.out.println("days=" + return_int);
            }
            else if(unit.equalsIgnoreCase("hours"))
            {
                return_int = Calendar.HOUR_OF_DAY;
                //System.out.println("hours=" + return_int);
            }
            else if(unit.equalsIgnoreCase("minutes"))
            {
                return_int = Calendar.MINUTE;
                //System.out.println("minutes=" + return_int);
            }                
        }
        catch(Exception e)
        {
            System.out.println("Exception in sla_request_calc.convert_unit_to_cal_int=" + e);
        }        
        return return_int;
    } 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    private static boolean is_this_a_business_day(List<Integer> NON_BUSINESS_DAYS, java.util.Date in_date)
    {
        boolean return_boolean = false;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(in_date);
        
        if (!NON_BUSINESS_DAYS.contains(calendar.get(Calendar.DAY_OF_WEEK)))
        {
            //System.out.println("in_date=" + in_date + " calendar.get(Calendar.DAY_OF_WEEK)=" + calendar.get(Calendar.DAY_OF_WEEK) + " not in list");
            return_boolean = true;
        }
        return return_boolean;
    } 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    private static boolean is_this_before_business_hours(String schedule[], java.util.Date in_date)
    {
        boolean return_boolean = false;
        int schedule_start[] = schedule_time_to_int_array(schedule[3]);
        java.util.Date scheduled_start_date = schedule_time_to_date(schedule_start,in_date);
        if(in_date.getTime() < scheduled_start_date.getTime())
        {
            return_boolean = true;
        }                
        return return_boolean;
    } 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    private static boolean is_this_after_business_hours(String schedule[], java.util.Date in_date)
    {
        boolean return_boolean = false;
        int schedule_end[] = schedule_time_to_int_array(schedule[4]);
        java.util.Date scheduled_end_date = schedule_time_to_date(schedule_end,in_date);
        if(in_date.getTime() > scheduled_end_date.getTime())
        {
            return_boolean = true;
        }                
        return return_boolean;
    } 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    private static boolean is_this_during_business_hours(String schedule[], java.util.Date in_date)
    {
        boolean return_boolean = false;
        int schedule_start[] = schedule_time_to_int_array(schedule[3]);
        int schedule_end[] = schedule_time_to_int_array(schedule[4]);
        java.util.Date scheduled_start_date = schedule_time_to_date(schedule_start,in_date);
        java.util.Date scheduled_end_date = schedule_time_to_date(schedule_end,in_date);
        if(in_date.getTime() >= scheduled_start_date.getTime() && in_date.getTime() <= scheduled_end_date.getTime())
        {
            return_boolean = true;
        }                
        return return_boolean;
    } 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////      
    public static String due_date_for_request_id(Connection con, String service_catalog_item_id, String priority, ZonedDateTime request_date) throws Exception, SQLException
    {
        //System.out.println("start due_date_for_request_id");
        boolean pass = false;
        String sc_item_info[] = db.get_service_catalog.service_catalog_items_by_id(con, service_catalog_item_id);
        String schedule[] = db.get_schedules.for_catalog_item_priority(con, priority, sc_item_info);
        //System.out.println("Schdule=" + schedule[1]);
        List<Integer> NON_BUSINESS_DAYS = load_not_business_days(schedule);
        int schedule_start[] = schedule_time_to_int_array(schedule[3]);
        int schedule_end[] = schedule_time_to_int_array(schedule[4]);
        load_not_business_days(schedule);
        long raw_max_delivery_time_msec = 00;
        GregorianCalendar req_date = GregorianCalendar.from(request_date);        
        GregorianCalendar new_req_date = GregorianCalendar.from(request_date);             
        GregorianCalendar due_date = GregorianCalendar.from(request_date);        
        String unit = "";
        int time = 0;
        if(priority.equalsIgnoreCase("critical"))//if priority = critical
        {
            unit = sc_item_info[11];
            time = Integer.parseInt(sc_item_info[10]);            
        }
        else if(priority.equalsIgnoreCase("high"))//if priority = critical
        {
            unit = sc_item_info[18];
            time = Integer.parseInt(sc_item_info[17]);
        }
        else if(priority.equalsIgnoreCase("medium"))//if priority = critical
        {
            unit = sc_item_info[25];
            time = Integer.parseInt(sc_item_info[24]);
        }
        else if(priority.equalsIgnoreCase("low"))//if priority = critical
        {
            unit = sc_item_info[32];
            time = Integer.parseInt(sc_item_info[31]);
        }     
        //System.out.println("Priority=" + priority + " Req_Date=" + timestamp2.format(new_req_date.getTime()));
        int count = 0;
        boolean start_date_done = false;
        while(!start_date_done) //adjust due date for non business hour request start
        {
            if(is_this_a_business_day(NON_BUSINESS_DAYS, new_req_date.getTime()))
            {
                //System.out.println("Req_Date This is a business day");
                if(is_this_before_business_hours(schedule,new_req_date.getTime()))
                {
                    //System.out.println("Req_Date This is a before business hours");
                    //adjust hours to start of business + 1 second
                    GregorianCalendar temp_cal = new GregorianCalendar();
                    temp_cal.setTimeInMillis(new_req_date.getTimeInMillis());
                    temp_cal.set(temp_cal.HOUR_OF_DAY,schedule_start[0]);
                    temp_cal.set(temp_cal.MINUTE,schedule_start[1]);
                    temp_cal.set(temp_cal.SECOND,schedule_start[2]);         
                    new_req_date.setTimeInMillis(temp_cal.getTimeInMillis());
                    //System.out.println("Req_Date This is a before business hours Fixed date=" + new_req_date.getTime());
                    start_date_done = true; //break out we have a valid new start date
                }
                else if(is_this_after_business_hours(schedule,new_req_date.getTime()))
                {
                    //System.out.println("This is a after business hours");
                    //adjust hours to start of the NEXT business + 1 second And start the while loop again
                    GregorianCalendar temp_cal = new GregorianCalendar();
                    temp_cal.setTimeInMillis(new_req_date.getTimeInMillis());
                    temp_cal.set(temp_cal.HOUR_OF_DAY,schedule_start[0]);
                    temp_cal.set(temp_cal.MINUTE,schedule_start[1]);
                    temp_cal.set(temp_cal.SECOND,schedule_start[2]);  
                    temp_cal.add(temp_cal.DATE, 1);
                    new_req_date.setTimeInMillis(temp_cal.getTimeInMillis());
                    //System.out.println("Req_Date This is a after business hours Fixed date=" + new_req_date.getTime());
                }
                else if(is_this_during_business_hours(schedule,new_req_date.getTime()))
                {
                    //System.out.println("Req_Date This is a during business hours");
                    //this is business day and business hour
                    start_date_done = true; //break out we have a valid new start date
                }
            }
            else
            {
                //System.out.println("Req_Date This is NOT a business day");
                //not a business day  Add 1 day to cal
                GregorianCalendar temp_cal = new GregorianCalendar();
                temp_cal.setTimeInMillis(new_req_date.getTimeInMillis());
                temp_cal.set(temp_cal.HOUR_OF_DAY,schedule_start[0]);
                temp_cal.set(temp_cal.MINUTE,schedule_start[1]);
                temp_cal.set(temp_cal.SECOND,schedule_start[2]); 
                temp_cal.add(temp_cal.DATE, 1);                
                new_req_date.setTimeInMillis(temp_cal.getTimeInMillis());
                //System.out.println("Req_Date This is NOT a business day Adjuested date=" + new_req_date.getTime());
            }
            count++;
            if(count > 200)
            {
                //System.out.println("breaking");
                break;
            }
        }//end while start date
        //System.out.println("Req_Date Final new_req_date=" + new_req_date.getTime());
            
        //with the fixed new_req_date, calc due date
        //create due cal  set cal to new_req_date
        
        due_date.setTime(new_req_date.getTime()); 
        //add units and value to cal
        due_date.add(convert_unit_to_cal_int(unit), time);
        //System.out.println("initial due_date=" + due_date.getTime() + "  new_req_date=" + new_req_date.getTime() + " + " + time + " " + unit);
        //break the cal into array of dates to remove non business days and add the non business days to the end of the array
        
        LocalDate start_localdate = LocalDate.parse( new SimpleDateFormat("yyyy-MM-dd").format(new_req_date.getTime()));
        //System.out.println("test");
        LocalDate end_localdate = LocalDate.parse( new SimpleDateFormat("yyyy-MM-dd").format(due_date.getTime()));
        
        long days = ChronoUnit.DAYS.between(start_localdate, end_localdate);
        //System.out.println(days + " days between new_req_date and due_date");        
        if(days == 0)
        {
            boolean due_date_done = false;
            while(!due_date_done) //adjust due date for non business hour request start
            {
                if(is_this_a_business_day(NON_BUSINESS_DAYS, due_date.getTime()))
                {
                    //System.out.println("due_date This is a business day");
                    if(is_this_before_business_hours(schedule,due_date.getTime()))
                    {
                        //System.out.println("due_date This is a before business hours");
                        //adjust hours to start of business + 1 second
                        GregorianCalendar temp_cal = new GregorianCalendar();
                        temp_cal.setTimeInMillis(new_req_date.getTimeInMillis());
                        temp_cal.set(temp_cal.HOUR_OF_DAY,schedule_start[0]);
                        temp_cal.set(temp_cal.MINUTE,schedule_start[1]);
                        temp_cal.set(temp_cal.SECOND,schedule_start[2]);         
                        due_date.setTimeInMillis(temp_cal.getTimeInMillis());
                        //System.out.println("due_date This is a before business hours Fixed date=" + due_date.getTime());
                        due_date_done = true; //break out we have a valid new start date
                    }
                    else if(is_this_after_business_hours(schedule,due_date.getTime()))
                    {
                        //System.out.println("due_date This is a after business hours");
                        //adjust hours to start of the NEXT business + 1 second And start the while loop again
                        GregorianCalendar temp_cal = new GregorianCalendar();
                        temp_cal.setTimeInMillis(due_date.getTimeInMillis());
                        temp_cal.set(temp_cal.HOUR_OF_DAY,schedule_start[0]);
                        temp_cal.set(temp_cal.MINUTE,schedule_start[1]);
                        temp_cal.set(temp_cal.SECOND,schedule_start[2]);  
                        temp_cal.add(temp_cal.DATE, 1);
                        due_date.setTimeInMillis(temp_cal.getTimeInMillis());
                        //System.out.println("due_date This is a after business hours Fixed date=" + due_date.getTime());
                    }
                    else if(is_this_during_business_hours(schedule,due_date.getTime()))
                    {
                        //System.out.println("due_date This is a during business hours");
                        //this is business day and business hour
                        due_date_done = true; //break out we have a valid new start date
                    }
                }
                else
                {
                    //System.out.println("due_date This is NOT a business day");
                    //not a business day  Add 1 day to cal
                    GregorianCalendar temp_cal = new GregorianCalendar();
                    temp_cal.setTimeInMillis(due_date.getTimeInMillis());
                    temp_cal.set(temp_cal.HOUR_OF_DAY,schedule_start[0]);
                    temp_cal.set(temp_cal.MINUTE,schedule_start[1]);
                    temp_cal.set(temp_cal.SECOND,schedule_start[2]); 
                    temp_cal.add(temp_cal.DATE, 1);                
                    due_date.setTimeInMillis(temp_cal.getTimeInMillis());
                    //System.out.println("due_date This is NOT a business day Adjuested date=" + due_date.getTime());
                }
                count++;
                if(count > 200)
                {
                    //System.out.println("breaking due_date");
                    break;
                }
            }//end while due_date
            //System.out.println("Due Date Final due_dat for 0 days=" + due_date.getTime());
        }
        else
        {
            //more than 1 day
            //create an arraylist of dates
            ArrayList <java.util.Date> working_days = new ArrayList();
            GregorianCalendar working_cal = new GregorianCalendar();
            working_cal.setTimeInMillis(new_req_date.getTimeInMillis());
            while(working_days.size() <= days)
            {
                working_days.add(working_cal.getTime());
                //System.out.println("Due Date more than 0 days adding =" + working_cal.getTime());
                working_cal.add(working_cal.DATE, 1);
            }
            boolean done_with_due_date = false;
            int current_pointer = 0;
            //System.out.println("Due Date more than 0 ----------------------------------------------");
            while(!done_with_due_date)
            {
                java.util.Date current_pointer_date = working_days.get(current_pointer);
                if(is_this_a_business_day(NON_BUSINESS_DAYS, current_pointer_date))
                {
                    //System.out.println("is business day=" + current_pointer_date + " current_pointer=" + current_pointer);
                }
                else
                {
                    //System.out.println("is NOT business day=" + current_pointer_date + " current_pointer=" + current_pointer);
                    //add another date to the end of the list
                    //get the last date in list
                    java.util.Date last_date = working_days.get(working_days.size() -1);
                    working_cal.setTime(last_date);
                    working_cal.add(working_cal.DATE, 1);
                    //System.out.println("Due Date more than 0 days adding for non business day =" + working_cal.getTime());
                    working_days.add(working_cal.getTime());
                }
                current_pointer++;
                if(current_pointer >= working_days.size())
                {
                    done_with_due_date = true;
                }
            }
            due_date.setTime(working_days.get(working_days.size() -1));
        }
        //System.out.println("Due Date Final due_date=" + due_date.getTime());
        return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(due_date.toZonedDateTime());
        
    }    
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    public static double for_sla_id(Connection con, String sla_id,  java.util.Date start, java.util.Date end) throws Exception, SQLException
    {
        //System.out.println("starting sla_contact_calc");
        double return_value = 0;
        String sla_type = "";
        String request_priority =  "";
        String start_field = "";
        String end_field = "";
        String stop_state = "";
        //System.out.println("incident_sla_id=" + incident_sla_id);
        ArrayList requests = new <String[]> ArrayList();
        //get sla definition
        String sla_info[] = db.get_sla.request_by_id(con, sla_id);
        sla_type = sla_info[3];
        request_priority = sla_info[5];
        PreparedStatement p_stmt;
        Statement stmt = con.createStatement();    
        
        //get the start state
        start_field = sla_info[7];        
        //get the end state
        end_field = sla_info[9];
        //get stop_state
        stop_state = sla_info[10];
        //get the parameters from the JSON
        String json_additional_parameters = sla_info[27];
        //parse the json and put it in a arraylist
        try
        {
            ArrayList<String[]> users = get_users.all(con);
            ArrayList<String[]> groups = db.get_groups.all(con);
            //start the query build
            //String request_query = "SELECT * FROM requests WHERE request_priority = '" + request_priority + "' AND state = '"+ end_field +"' AND (state_date >= '" + timestamp.format(start) + "' AND state_date <= '" + timestamp.format(end) + "')"; // AND (contact_method = 'Chat' OR contact_method = 'Phone' OR contact_method = 'Walk in')";
            //String request_query = "SELECT * FROM request WHERE (" + end_field + " >= '" + timestamp.format(start) + "' AND " + end_field + " <= '" + timestamp.format(end) + "') AND priority = '" + request_priority + "' AND state = '" + stop_state + "'";
            String request_query = "SELECT id,"
            + "rev,"
            + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date,"  // + "CONVERT_TZ(rev_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_rev_date`, "
            + "rev_by_user_id,"
            + "service_catalog_id," 
            + "service_catalog_item_id,"
            + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "  //+ "CONVERT_TZ(create_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_create_date`, "
            + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
            + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, " 
            + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, " 
            + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "  //+ "CONVERT_TZ(closed_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_closed_date`, "
            + "assigned_group_id," 
            + "assigned_to_id,"
            + "create_by_id,"
            + "requested_for_id," 
            + "requested_for_group_id," 
            + "location," 
            + "department," 
            + "site," 
            + "company," 
            + "impact," 
            + "urgency,"
            + "priority,"
            + "state," 
            + "contact_method,"
            + "approval,"
            + "price," 
            + "notes," 
            + "desk_notes," 
            + "description," 
            + "closed_reason,"
            + "required_info," 
            + "required_info_response,"
            + "optional_info," 
            + "optional_info_response " 
            + "FROM request WHERE (" + end_field + " >= '" + timestamp.format(start) + "' AND " + end_field + " <= '" + timestamp.format(end) + "') AND priority = '" + request_priority + "' AND state = '" + stop_state + "'";
            //System.out.println("json_additional_parameters=" + json_additional_parameters);
            JsonReader reader = Json.createReader(new StringReader(json_additional_parameters));
            JsonObject resultObject = reader.readObject();
            
            JsonArray parameters = (JsonArray) resultObject.getJsonArray("parameters");
            //System.out.println("parameters length=" + parameters.size());
            for(int a = 0; a < parameters.size(); a++)
            {
                JsonObject this_record = parameters.getJsonObject(a); //a single jsonrecord
                //System.out.println("json=" + this_record);
                String field_name = this_record.getJsonString("field_name").toString().replace("\"","");
                String operator = this_record.getJsonString("operator").toString().replace("\"","");
                JsonArray values = (JsonArray) this_record.getJsonArray("value");
                String field_query = "";
                for(int b = 0; b < values.size(); b++)
                {
                    String op = "";
                    if(operator.equals("equal")){op = "=";}else{op = "!=";}
                    //AND (contact_method = 'Chat' OR contact_method = 'Phone' OR contact_method = 'Walk in')";
                    if(b == 0)
                    {
                        field_query = field_name + " " + op + " '" + values.getString(b) + "'";
                    }
                    else
                    {
                        field_query = field_query + " OR " + field_name + " " + op + " '" + values.getString(b) + "'";
                    }
                }
                request_query = request_query + " AND (" + field_query + ")";                
            }
            
            //System.out.println("request sla q=" + request_query);
            //get incidents that sla applies to. and their stop dates
            ResultSet rs = stmt.executeQuery(request_query);
            while(rs.next())
            {
                String temp[] = new String[57];
                temp[0] = rs.getString("id");//id
                temp[1] = rs.getString("rev");//rev
                temp[2] = support.string_utils.check_for_null(rs.getString("rev_date"));//rev_date
                temp[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                temp[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                temp[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                //temp[6] = support.string_utils.convert_timestamp_to_timestamp_string(rs.getTimestamp("create_date"));//create_date
                temp[6] = support.string_utils.check_for_null(rs.getString("create_date"));//create_date
                temp[7] = support.string_utils.check_for_null(rs.getString("request_date"));//request_date
                temp[8] = support.string_utils.check_for_null(rs.getString("due_date"));//due_date
                temp[9] = support.string_utils.check_for_null(rs.getString("resolve_date"));//resolve_date
                temp[10] = support.string_utils.check_for_null(rs.getString("closed_date"));//closed_date
                temp[11] = rs.getString("assigned_group_id");//assigned_group_id
                temp[12] = rs.getString("assigned_to_id");//assigned_to_id
                temp[13] = rs.getString("create_by_id");//create_by_id
                temp[14] = rs.getString("requested_for_id");//requested_for_id
                temp[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                temp[16] = rs.getString("location");//location
                temp[17] = rs.getString("department");//department
                temp[18] = rs.getString("site");//site
                temp[19] = rs.getString("company");//company
                temp[20] = rs.getString("impact");//impact
                temp[21] = rs.getString("urgency");//urgency
                temp[22] = rs.getString("priority");//urgency
                temp[23] = rs.getString("state");//state
                temp[24] = rs.getString("contact_method");//contact_method
                temp[25] = rs.getString("approval");//approval
                temp[26] = rs.getString("price");//price
                temp[27] = rs.getString("notes");//notes
                temp[28] = rs.getString("desk_notes");//desk_notes
                temp[29] = rs.getString("description");//description
                temp[30] = rs.getString("closed_reason");//closed_reason
                
                temp[31] = rs.getString("required_info");//required_info
                temp[32] = rs.getString("required_info_response");//desk_notes
                temp[33] = rs.getString("optional_info");//optional_info
                temp[34] = rs.getString("optional_info_response");//optional_info_response


                //rev_by_user_id
                temp[35] = "";
                temp[36] = "";
                temp[37] = "";
                temp[38] = "";
                temp[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        temp[35] = this_user[1]; //username
                        temp[36] = this_user[3]; //first
                        temp[37] = this_user[4]; //MI
                        temp[38] = this_user[5]; //Last
                        temp[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                temp[40] = "";
                for(int a = 0; a < groups.size(); a++)
                {
                    String this_group[] = groups.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        temp[40] = this_group[1];
                        a = groups.size();
                    }
                }
                //assigned_to_id
                temp[41] = "";
                temp[42] = "";
                temp[43] = "";
                temp[44] = "";
                temp[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        temp[41] = this_user[1]; //username
                        temp[42] = this_user[3]; //first
                        temp[43] = this_user[4]; //MI
                        temp[44] = this_user[5]; //Last
                        temp[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                temp[46] = "";
                temp[47] = "";
                temp[48] = "";
                temp[49] = "";
                temp[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        temp[46] = this_user[1]; //username
                        temp[47] = this_user[3]; //first
                        temp[48] = this_user[4]; //MI
                        temp[49] = this_user[5]; //Last
                        temp[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                temp[51] = "";
                temp[52] = "";
                temp[53] = "";
                temp[54] = "";
                temp[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        temp[51] = this_user[1]; //username
                        temp[52] = this_user[3]; //first
                        temp[53] = this_user[4]; //MI
                        temp[54] = this_user[5]; //Last
                        temp[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                temp[56] = "";
                for(int a = 0; a < groups.size(); a++)
                {
                    String this_group[] = groups.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        temp[56] = this_group[1];
                        a = groups.size();
                    }
                }
                
                
                
                /*
                String temp[] = new String[43];
                
                temp[0] = rs.getString("id");//id
                temp[1] = rs.getString("external_id");//external_id
                try
                {
                    temp[2] = timestamp.format(rs.getTimestamp("create_date")); //create_date
                }
                catch(Exception e)
                {
                    temp[2] = "";
                }
                try
                {
                    temp[3] = timestamp.format(rs.getTimestamp("request_date")); //request_date
                }
                catch(Exception e)
                {
                    temp[3] = "";
                }
                try
                {
                    temp[4] = timestamp.format(rs.getTimestamp("resolve_date")); //resolve_date
                }
                catch(Exception e)
                {
                    temp[4] = "";
                }
                try
                {
                    temp[5] = timestamp.format(rs.getTimestamp("closed_date")); //closed_date
                }
                catch(Exception e)
                {
                    temp[5] = "";
                }
                temp[6] = rs.getString("assigned_group_id");//assigned_group_id
                temp[7] = rs.getString("assigned_to_id");//assigned_to_id
                temp[8] = rs.getString("create_by_id");//create_by_id
                temp[9] = rs.getString("requested_for_id");//requested_for_id
                temp[10] = rs.getString("requested_for_group_id");//requested_for_group_id
                temp[11] = rs.getString("location");//location
                temp[12] = rs.getString("department");//department
                temp[13] = rs.getString("site");//site
                temp[14] = rs.getString("company");//company
                temp[15] = rs.getString("impact");//impact
                temp[16] = rs.getString("urgency");//urgency
                temp[17] = rs.getString("priority");//priority
                temp[18] = rs.getString("state");//state
                temp[19] = rs.getString("contact_method");//contact_method
                temp[20] = rs.getString("approval");//approval
                temp[21] = rs.getString("price");//price
                temp[22] = rs.getString("notes");//notes
                temp[23] = rs.getString("desk_notes");//desk_notes
                temp[24] = rs.getString("description");//description
                temp[25] = rs.getString("closed_reason");//closed_reason
                //populate the requested_for_group_id
                temp[26] = "";
                for(int a = 0; a < groups.size(); a++)
                {
                    String this_group[] = groups.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        temp[26] = this_group[1];
                        a = groups.size();
                    }
                }
                //populate the assigned_group_id
                temp[27] = "";
                for(int a = 0; a < groups.size(); a++)
                {
                    String this_group[] = groups.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        temp[27] = this_group[1];
                        a = groups.size();
                    }
                }
                //populate the assigned_to_id
                temp[28] = "";
                temp[29] = "";
                temp[30] = "";
                temp[31] = "";
                temp[32] = "";
                for(int b = 0; b < users.size(); b++)
                {
                    String this_user[] = users.get(b);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        temp[28] = this_user[3];
                        temp[30] = this_user[4];
                        temp[31] = this_user[5];
                        temp[32] = this_user[17];
                        b = users.size();
                    }
                }
                //populate the create_by_id
                temp[33] = "";
                temp[34] = "";
                temp[35] = "";
                temp[36] = "";
                temp[37] = "";
                for(int c = 0; c < users.size(); c++)
                {
                    String this_user[] = users.get(c);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        temp[33] = this_user[1];
                        temp[34] = this_user[3];
                        temp[35] = this_user[4];
                        temp[36] = this_user[5];
                        temp[37] = this_user[17];
                        c = users.size();
                    }
                }
                //populate the requested_for_id
                temp[38] = "";
                temp[39] = "";
                temp[40] = "";
                temp[41] = "";
                temp[42] = "";
                for(int d = 0; d < users.size(); d++)
                {
                    String this_user[] = users.get(d);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        temp[38] = this_user[1];
                        temp[39] = this_user[3];
                        temp[40] = this_user[4];
                        temp[41] = this_user[5];
                        temp[42] = this_user[17];
                        d = users.size();
                    }
                }*/
                requests.add(temp);
            }
        }
        catch(Exception e)
        {
            logger.error("ERROR sla_request_calc.for_sla_id JSON Parse:=" + e);
        }
              
        //do the calculation
        //{"TIME"};
        switch(sla_type) 
        { 
            case "TIME": 
                return_value = calc_time(sla_info,requests);
                //System.out.println("TIME incident sla"); 
                break; 
            default: 
                //System.out.println("no match"); 
        } 
        //System.out.println("number of request sla q=" + requests.size());
        return return_value;
    }
    public static int[] for_sla_id_service_catalog(Connection con, String sla_id,  java.util.Date start, java.util.Date end) throws Exception, SQLException
    {
        //System.out.println("starting sla_contact_calc");
        int return_value[] = {0, 0, 0, 0};
        String sla_type = "";
        String service_catalog_item_id = "";
        String request_priority =  "";
        String start_field = "";
        String end_field = "";
        String stop_state = "";
        //System.out.println("incident_sla_id=" + incident_sla_id);
        ArrayList requests = new <String[]> ArrayList();
        //get sla definition
        String sla_info[] = db.get_sla.request_by_id(con, sla_id);
        sla_type = sla_info[3];
        service_catalog_item_id = sla_info[28];
        request_priority = sla_info[5];
        PreparedStatement p_stmt;
        Statement stmt = con.createStatement();    
        
        //get the start state
        start_field = sla_info[7];        
        //get the end state
        end_field = sla_info[9];
        //get stop_state
        stop_state = sla_info[10];
        //get the parameters from the JSON
        String json_additional_parameters = sla_info[27];
        //parse the json and put it in a arraylist
        try
        {
            ArrayList<String[]> users = get_users.all(con);
            ArrayList<String[]> groups = db.get_groups.all(con);
            //start the query build
            //String request_query = "SELECT * FROM requests WHERE request_priority = '" + request_priority + "' AND state = '"+ end_field +"' AND (state_date >= '" + timestamp.format(start) + "' AND state_date <= '" + timestamp.format(end) + "')"; // AND (contact_method = 'Chat' OR contact_method = 'Phone' OR contact_method = 'Walk in')";
            //String request_query = "SELECT * FROM request WHERE (" + end_field + " >= '" + timestamp.format(start) + "' AND " + end_field + " <= '" + timestamp.format(end) + "') AND priority = '" + request_priority + "' AND state = '" + stop_state + "'";
            String request_query = "SELECT id,"
            + "rev,"
            + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date,"  // + "CONVERT_TZ(rev_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_rev_date`, "
            + "rev_by_user_id,"
            + "service_catalog_id," 
            + "service_catalog_item_id,"
            + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "  //+ "CONVERT_TZ(create_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_create_date`, "
            + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
            + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, " 
            + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, " 
            + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "  //+ "CONVERT_TZ(closed_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_closed_date`, "
            + "assigned_group_id," 
            + "assigned_to_id,"
            + "create_by_id,"
            + "requested_for_id," 
            + "requested_for_group_id," 
            + "location," 
            + "department," 
            + "site," 
            + "company," 
            + "impact," 
            + "urgency,"
            + "priority,"
            + "state," 
            + "contact_method,"
            + "approval,"
            + "price," 
            + "notes," 
            + "desk_notes," 
            + "description," 
            + "closed_reason,"
            + "required_info," 
            + "required_info_response,"
            + "optional_info," 
            + "optional_info_response " 
            + "FROM request WHERE service_catalog_item_id = '" + service_catalog_item_id + "' AND (" + end_field + " >= '" + timestamp.format(start) + "' AND " + end_field + " <= '" + timestamp.format(end) + "') AND priority = '" + request_priority + "' AND state = '" + stop_state + "'";
            //System.out.println("json_additional_parameters=" + json_additional_parameters);
            JsonReader reader = Json.createReader(new StringReader(json_additional_parameters));
            JsonObject resultObject = reader.readObject();
            
            JsonArray parameters = (JsonArray) resultObject.getJsonArray("parameters");
            //System.out.println("parameters length=" + parameters.size());
            for(int a = 0; a < parameters.size(); a++)
            {
                JsonObject this_record = parameters.getJsonObject(a); //a single jsonrecord
                //System.out.println("json=" + this_record);
                String field_name = this_record.getJsonString("field_name").toString().replace("\"","");
                String operator = this_record.getJsonString("operator").toString().replace("\"","");
                JsonArray values = (JsonArray) this_record.getJsonArray("value");
                String field_query = "";
                for(int b = 0; b < values.size(); b++)
                {
                    String op = "";
                    if(operator.equals("equal")){op = "=";}else{op = "!=";}
                    //AND (contact_method = 'Chat' OR contact_method = 'Phone' OR contact_method = 'Walk in')";
                    if(b == 0)
                    {
                        field_query = field_name + " " + op + " '" + values.getString(b) + "'";
                    }
                    else
                    {
                        field_query = field_query + " OR " + field_name + " " + op + " '" + values.getString(b) + "'";
                    }
                }
                request_query = request_query + " AND (" + field_query + ")";                
            }
            
            //System.out.println("request sla q=" + request_query);
            //get incidents that sla applies to. and their stop dates
            ResultSet rs = stmt.executeQuery(request_query);
            while(rs.next())
            {
                String temp[] = new String[57];
                temp[0] = rs.getString("id");//id
                temp[1] = rs.getString("rev");//rev
                temp[2] = support.string_utils.check_for_null(rs.getString("rev_date"));//rev_date
                temp[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                temp[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                temp[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                //temp[6] = support.string_utils.convert_timestamp_to_timestamp_string(rs.getTimestamp("create_date"));//create_date
                temp[6] = support.string_utils.check_for_null(rs.getString("create_date"));//create_date
                temp[7] = support.string_utils.check_for_null(rs.getString("request_date"));//request_date
                temp[8] = support.string_utils.check_for_null(rs.getString("due_date"));//due_date
                temp[9] = support.string_utils.check_for_null(rs.getString("resolve_date"));//resolve_date
                temp[10] = support.string_utils.check_for_null(rs.getString("closed_date"));//closed_date
                temp[11] = rs.getString("assigned_group_id");//assigned_group_id
                temp[12] = rs.getString("assigned_to_id");//assigned_to_id
                temp[13] = rs.getString("create_by_id");//create_by_id
                temp[14] = rs.getString("requested_for_id");//requested_for_id
                temp[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                temp[16] = rs.getString("location");//location
                temp[17] = rs.getString("department");//department
                temp[18] = rs.getString("site");//site
                temp[19] = rs.getString("company");//company
                temp[20] = rs.getString("impact");//impact
                temp[21] = rs.getString("urgency");//urgency
                temp[22] = rs.getString("priority");//urgency
                temp[23] = rs.getString("state");//state
                temp[24] = rs.getString("contact_method");//contact_method
                temp[25] = rs.getString("approval");//approval
                temp[26] = rs.getString("price");//price
                temp[27] = rs.getString("notes");//notes
                temp[28] = rs.getString("desk_notes");//desk_notes
                temp[29] = rs.getString("description");//description
                temp[30] = rs.getString("closed_reason");//closed_reason
                
                temp[31] = rs.getString("required_info");//required_info
                temp[32] = rs.getString("required_info_response");//desk_notes
                temp[33] = rs.getString("optional_info");//optional_info
                temp[34] = rs.getString("optional_info_response");//optional_info_response


                //rev_by_user_id
                temp[35] = "";
                temp[36] = "";
                temp[37] = "";
                temp[38] = "";
                temp[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        temp[35] = this_user[1]; //username
                        temp[36] = this_user[3]; //first
                        temp[37] = this_user[4]; //MI
                        temp[38] = this_user[5]; //Last
                        temp[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                temp[40] = "";
                for(int a = 0; a < groups.size(); a++)
                {
                    String this_group[] = groups.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        temp[40] = this_group[1];
                        a = groups.size();
                    }
                }
                //assigned_to_id
                temp[41] = "";
                temp[42] = "";
                temp[43] = "";
                temp[44] = "";
                temp[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        temp[41] = this_user[1]; //username
                        temp[42] = this_user[3]; //first
                        temp[43] = this_user[4]; //MI
                        temp[44] = this_user[5]; //Last
                        temp[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                temp[46] = "";
                temp[47] = "";
                temp[48] = "";
                temp[49] = "";
                temp[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        temp[46] = this_user[1]; //username
                        temp[47] = this_user[3]; //first
                        temp[48] = this_user[4]; //MI
                        temp[49] = this_user[5]; //Last
                        temp[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                temp[51] = "";
                temp[52] = "";
                temp[53] = "";
                temp[54] = "";
                temp[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        temp[51] = this_user[1]; //username
                        temp[52] = this_user[3]; //first
                        temp[53] = this_user[4]; //MI
                        temp[54] = this_user[5]; //Last
                        temp[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                temp[56] = "";
                for(int a = 0; a < groups.size(); a++)
                {
                    String this_group[] = groups.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        temp[56] = this_group[1];
                        a = groups.size();
                    }
                }
                requests.add(temp);
            }
        }
        catch(Exception e)
        {
            logger.error("ERROR sla_request_calc.for_sla_id JSON Parse:=" + e);
        }
              
        //do the calculation
        //{"TIME"};
        switch(sla_type) 
        { 
            case "TIME": 
                return_value = calc_quantity(sla_info,requests);
                //System.out.println("TIME incident sla"); 
                break; 
            default: 
                //System.out.println("no match"); 
        } 
        //System.out.println("number of request sla q=" + requests.size());
        return return_value;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static int[] calc_quantity(String sla_info[], ArrayList <String[]>requests) throws Exception //arraylist {incident_id, start_date, stop_date}
    {
        int return_array[] = {0, 0, 0, 100};
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_requests = 0;
        int total_requests_meeting_sla = 0;
        int total_requests_failing_sla = 0;
        java.util.Date start = new java.util.Date();
        java.util.Date end = new java.util.Date();
        //get the start state
        String start_field = sla_info[7];        
        //get the end state
        String end_field = sla_info[9];
        //get stop_state
        String stop_state = sla_info[10];  //arraylist index 18
        
        try
        {
            target = Double.parseDouble(sla_info[6]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_request_calc.calc_quantity target definition:=" + e);
        }
        sla_operator = sla_info[18];
        try
        {
            sla_value = Double.parseDouble(sla_info[19]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_request_calc.calc_quantity sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[20];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        //for each incident        
            //get the diff between "start date" and "end date"            
            //use operator to determine good or bad
            //increment good/bad counts
        //end for
        //System.out.println("number of requests=" + requests.size());
        try
        {
            //System.out.println("requests.size()=" + requests.size());
            for(int a = 0; a < requests.size(); a++)
            {
                String request[] = requests.get(a);
                //if end state does not match the sla, then do not count it.
                if(request[18].equalsIgnoreCase(stop_state))
                {
                    //figure out start field and stop
                    if(start_field.equalsIgnoreCase("create_date"))
                    {
                        start = timestamp.parse(request[2]);
                    }
                    else if(start_field.equalsIgnoreCase("request_date"))
                    {
                        start = timestamp.parse(request[3]);
                    }
                    else if(start_field.equalsIgnoreCase("resolve_date"))
                    {
                        start = timestamp.parse(request[4]);
                    }
                    else if(start_field.equalsIgnoreCase("closed_date"))
                    {
                        start = timestamp.parse(request[4]);
                    }
                    /////////////////////////////////////////////////////////////////
                    if(end_field.equalsIgnoreCase("create_date"))
                    {
                        end = timestamp.parse(request[2]);
                    }
                    else if(end_field.equalsIgnoreCase("request_date"))
                    {
                        end = timestamp.parse(request[3]);
                    }
                    else if(end_field.equalsIgnoreCase("resolve_date"))
                    {
                        end = timestamp.parse(request[4]);
                    }
                    else if(end_field.equalsIgnoreCase("closed_date"))
                    {
                        end = timestamp.parse(request[4]);
                    }

                    long diff = ((end.getTime() - start.getTime()) / 1000);
                    //System.out.println("diff=" + diff   + "  sla_value=" + sla_value);
                    total_requests++;
                    //{"greater_than", "greater_than_or_equal", "equal_to", "not_equal_to", "less_than_or_equal", "less_than"}
                    //System.out.println("operator=" + sla_operator);
                    switch(sla_operator) 
                    { 
                        case "greater_than": 
                            if(diff > sla_value)
                            {
                                total_requests_meeting_sla++;
                            }
                            else
                            {
                                total_requests_failing_sla++;
                            }
                            break; 
                        case "greater_than_or_equal":
                            if(diff >= sla_value)
                            {
                                total_requests_meeting_sla++;
                            }
                            else
                            {
                                total_requests_failing_sla++;
                            }
                            break; 
                        case "equal_to": 
                            if(diff == sla_value)
                            {
                                total_requests_meeting_sla++;
                            }
                            else
                            {
                                total_requests_failing_sla++;
                            }
                            break; 
                        case "not_equal_to": 
                            if(diff != sla_value)
                            {
                                total_requests_meeting_sla++;
                            }
                            else
                            {
                                total_requests_failing_sla++;
                            }
                            break; 
                        case "less_than_or_equal": 
                            if(diff <= sla_value)
                            {
                                total_requests_meeting_sla++;
                            }
                            else
                            {
                                total_requests_failing_sla++;
                            }
                            break; 
                        case "less_than": 
                            if(diff < sla_value)
                            {
                                total_requests_meeting_sla++;
                            }
                            else
                            {
                                total_requests_failing_sla++;
                            }
                            break; 
                        default: 
                            //System.out.println("no match");     
                    }
                }
            } 
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_request_calc.calc_quantity calc loop=" + e);
        }
        try
        {
            if(total_requests != 0)
            {
                return_array[0] = total_requests_meeting_sla;
                return_array[1] = total_requests_failing_sla;
                return_array[2] = total_requests;
                return_array[3] = total_requests_meeting_sla * 100 / total_requests;
                //System.out.println("total_requests_meeting_sla=" + total_requests_meeting_sla  + " total_requests=" + total_requests + " =" + return_double);
            }
            
        }
        catch(Exception e)
        {
            logger.error("Exception in sla_request_calc.calc_quantity final calc:=" + e);
        }
        //System.out.println("REQUEST return_double=" + return_double);
        return return_array;
    }  
    public static double calc_time(String sla_info[], ArrayList <String[]>requests) throws Exception //arraylist {incident_id, start_date, stop_date}
    {
        double return_double = 0;
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_requests = 0;
        int total_requests_meeting_sla = 0;
        int total_requests_failing_sla = 0;
        java.util.Date start = new java.util.Date();
        java.util.Date end = new java.util.Date();
        //get the start state
        String start_field = sla_info[7];        
        //get the end state
        String end_field = sla_info[9];
        //get stop_state
        String stop_state = sla_info[10];  //arraylist index 18
        
        try
        {
            target = Double.parseDouble(sla_info[6]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_request_calc.calc_time target definition:=" + e);
        }
        sla_operator = sla_info[18];
        try
        {
            sla_value = Double.parseDouble(sla_info[19]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_request_calc.calc_time sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[20];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        //for each incident        
            //get the diff between "start date" and "end date"            
            //use operator to determine good or bad
            //increment good/bad counts
        //end for
        //System.out.println("number of requests=" + requests.size());
        try
        {
            //System.out.println("requests.size()=" + requests.size());
            for(int a = 0; a < requests.size(); a++)
            {
                String request[] = requests.get(a);
                //if end state does not match the sla, then do not count it.
                if(request[18].equalsIgnoreCase(stop_state))
                {
                    //figure out start field and stop
                    if(start_field.equalsIgnoreCase("create_date"))
                    {
                        start = timestamp.parse(request[2]);
                    }
                    else if(start_field.equalsIgnoreCase("request_date"))
                    {
                        start = timestamp.parse(request[3]);
                    }
                    else if(start_field.equalsIgnoreCase("resolve_date"))
                    {
                        start = timestamp.parse(request[4]);
                    }
                    else if(start_field.equalsIgnoreCase("closed_date"))
                    {
                        start = timestamp.parse(request[4]);
                    }
                    /////////////////////////////////////////////////////////////////
                    if(end_field.equalsIgnoreCase("create_date"))
                    {
                        end = timestamp.parse(request[2]);
                    }
                    else if(end_field.equalsIgnoreCase("request_date"))
                    {
                        end = timestamp.parse(request[3]);
                    }
                    else if(end_field.equalsIgnoreCase("resolve_date"))
                    {
                        end = timestamp.parse(request[4]);
                    }
                    else if(end_field.equalsIgnoreCase("closed_date"))
                    {
                        end = timestamp.parse(request[4]);
                    }

                    long diff = ((end.getTime() - start.getTime()) / 1000);
                    //System.out.println("diff=" + diff   + "  sla_value=" + sla_value);
                    total_requests++;
                    //{"greater_than", "greater_than_or_equal", "equal_to", "not_equal_to", "less_than_or_equal", "less_than"}
                    //System.out.println("operator=" + sla_operator);
                    switch(sla_operator) 
                    { 
                        case "greater_than": 
                            if(diff > sla_value)
                            {
                                total_requests_meeting_sla++;
                            }
                            else
                            {
                                total_requests_failing_sla++;
                            }
                            break; 
                        case "greater_than_or_equal":
                            if(diff >= sla_value)
                            {
                                total_requests_meeting_sla++;
                            }
                            else
                            {
                                total_requests_failing_sla++;
                            }
                            break; 
                        case "equal_to": 
                            if(diff == sla_value)
                            {
                                total_requests_meeting_sla++;
                            }
                            else
                            {
                                total_requests_failing_sla++;
                            }
                            break; 
                        case "not_equal_to": 
                            if(diff != sla_value)
                            {
                                total_requests_meeting_sla++;
                            }
                            else
                            {
                                total_requests_failing_sla++;
                            }
                            break; 
                        case "less_than_or_equal": 
                            if(diff <= sla_value)
                            {
                                total_requests_meeting_sla++;
                            }
                            else
                            {
                                total_requests_failing_sla++;
                            }
                            break; 
                        case "less_than": 
                            if(diff < sla_value)
                            {
                                total_requests_meeting_sla++;
                            }
                            else
                            {
                                total_requests_failing_sla++;
                            }
                            break; 
                        default: 
                            //System.out.println("no match");     
                    }
                }
            } 
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_request_calc.calc_time calc loop=" + e);
        }
        try
        {
            if(total_requests == 0)
            {
                return_double = 100;
            }
            else
            {
                return_double = ((double)total_requests_meeting_sla / (double)total_requests ) * 100;
                //System.out.println("total_requests_meeting_sla=" + total_requests_meeting_sla  + " total_requests=" + total_requests + " =" + return_double);
            }
            
        }
        catch(Exception e)
        {
            logger.error("Exception in sla_request_calc.calc_time final calc:=" + e);
        }
        //System.out.println("REQUEST return_double=" + return_double);
        return return_double;
    }  
    public static ArrayList<String[]> get_noncompliant_requests_for_sla_id(Connection con, String request_sla_id,  java.util.Date start, java.util.Date end) throws Exception, SQLException
    {
        //System.out.println("starting sla_contact_calc");
        ArrayList return_value = new ArrayList<String[]>();
        String sla_type = "";
        String request_priority =  "";
        String start_field = "";
        String end_field = "";
        String stop_state = "";
        //System.out.println("incident_sla_id=" + incident_sla_id);
        ArrayList requests = new <String[]> ArrayList();
        //get sla definition
        String sla_info[] = db.get_sla.request_by_id(con, request_sla_id);
        sla_type = sla_info[3];
        request_priority = sla_info[5];
        PreparedStatement p_stmt;
        Statement stmt = con.createStatement();    
        
        //get the start state
        start_field = sla_info[7];        
        //get the end state
        end_field = sla_info[9];
        //get stop_state
        stop_state = sla_info[10];
        //get the parameters from the JSON
        String json_additional_parameters = sla_info[27];
        //parse the json and put it in a arraylist
        try
        {
            ArrayList<String[]> users = get_users.all(con);
            ArrayList<String[]> groups = db.get_groups.all(con);
            //start the query build
            //String request_query = "SELECT * FROM requests WHERE request_priority = '" + request_priority + "' AND state = '"+ end_field +"' AND (state_date >= '" + timestamp.format(start) + "' AND state_date <= '" + timestamp.format(end) + "')"; // AND (contact_method = 'Chat' OR contact_method = 'Phone' OR contact_method = 'Walk in')";
            String request_query = "SELECT * FROM request WHERE (" + end_field + " >= '" + timestamp.format(start) + "' AND " + end_field + " <= '" + timestamp.format(end) + "') AND priority = '" + request_priority + "' AND state = '" + stop_state + "'";
            
            //System.out.println("json_additional_parameters=" + json_additional_parameters);
            JsonReader reader = Json.createReader(new StringReader(json_additional_parameters));
            JsonObject resultObject = reader.readObject();
            
            JsonArray parameters = (JsonArray) resultObject.getJsonArray("parameters");
            //System.out.println("parameters length=" + parameters.size());
            for(int a = 0; a < parameters.size(); a++)
            {
                JsonObject this_record = parameters.getJsonObject(a); //a single jsonrecord
                //System.out.println("json=" + this_record);
                String field_name = this_record.getJsonString("field_name").toString().replace("\"","");
                String operator = this_record.getJsonString("operator").toString().replace("\"","");
                JsonArray values = (JsonArray) this_record.getJsonArray("value");
                String field_query = "";
                for(int b = 0; b < values.size(); b++)
                {
                    String op = "";
                    if(operator.equals("equal")){op = "=";}else{op = "!=";}
                    //AND (contact_method = 'Chat' OR contact_method = 'Phone' OR contact_method = 'Walk in')";
                    if(b == 0)
                    {
                        field_query = field_name + " " + op + " '" + values.getString(b) + "'";
                    }
                    else
                    {
                        field_query = field_query + " OR " + field_name + " " + op + " '" + values.getString(b) + "'";
                    }
                }
                request_query = request_query + " AND (" + field_query + ")";                
            }
            
            //System.out.println("request sla q=" + request_query);
            //get incidents that sla applies to. and their stop dates
            ResultSet rs = stmt.executeQuery(request_query);
            while(rs.next())
            {
                String temp[] = new String[43];
                
                temp[0] = rs.getString("id");//id
                temp[1] = rs.getString("external_id");//external_id
                try
                {
                    temp[2] = timestamp.format(rs.getTimestamp("create_date")); //create_date
                }
                catch(Exception e)
                {
                    temp[2] = "";
                }
                try
                {
                    temp[3] = timestamp.format(rs.getTimestamp("request_date")); //request_date
                }
                catch(Exception e)
                {
                    temp[3] = "";
                }
                try
                {
                    temp[4] = timestamp.format(rs.getTimestamp("resolve_date")); //resolve_date
                }
                catch(Exception e)
                {
                    temp[4] = "";
                }
                try
                {
                    temp[5] = timestamp.format(rs.getTimestamp("closed_date")); //closed_date
                }
                catch(Exception e)
                {
                    temp[5] = "";
                }
                temp[6] = rs.getString("assigned_group_id");//assigned_group_id
                temp[7] = rs.getString("assigned_to_id");//assigned_to_id
                temp[8] = rs.getString("create_by_id");//create_by_id
                temp[9] = rs.getString("requested_for_id");//requested_for_id
                temp[10] = rs.getString("requested_for_group_id");//requested_for_group_id
                temp[11] = rs.getString("location");//location
                temp[12] = rs.getString("department");//department
                temp[13] = rs.getString("site");//site
                temp[14] = rs.getString("company");//company
                temp[15] = rs.getString("impact");//impact
                temp[16] = rs.getString("urgency");//urgency
                temp[17] = rs.getString("priority");//priority
                temp[18] = rs.getString("state");//state
                temp[19] = rs.getString("contact_method");//contact_method
                temp[20] = rs.getString("approval");//approval
                temp[21] = rs.getString("price");//price
                temp[22] = rs.getString("notes");//notes
                temp[23] = rs.getString("desk_notes");//desk_notes
                temp[24] = rs.getString("description");//description
                temp[25] = rs.getString("closed_reason");//closed_reason
                //populate the requested_for_group_id
                temp[26] = "";
                for(int a = 0; a < groups.size(); a++)
                {
                    String this_group[] = groups.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        temp[26] = this_group[1];
                        a = groups.size();
                    }
                }
                //populate the assigned_group_id
                temp[27] = "";
                for(int a = 0; a < groups.size(); a++)
                {
                    String this_group[] = groups.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        temp[27] = this_group[1];
                        a = groups.size();
                    }
                }
                //populate the assigned_to_id
                temp[28] = "";
                temp[29] = "";
                temp[30] = "";
                temp[31] = "";
                temp[32] = "";
                for(int b = 0; b < users.size(); b++)
                {
                    String this_user[] = users.get(b);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        temp[28] = this_user[1];
                        temp[29] = this_user[3];
                        temp[30] = this_user[4];
                        temp[31] = this_user[5];
                        temp[32] = this_user[17];
                        b = users.size();
                    }
                }
                //populate the create_by_id
                temp[33] = "";
                temp[34] = "";
                temp[35] = "";
                temp[36] = "";
                temp[37] = "";
                for(int c = 0; c < users.size(); c++)
                {
                    String this_user[] = users.get(c);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        temp[33] = this_user[1];
                        temp[34] = this_user[3];
                        temp[35] = this_user[4];
                        temp[36] = this_user[5];
                        temp[37] = this_user[17];
                        c = users.size();
                    }
                }
                //populate the requested_for_id
                temp[38] = "";
                temp[39] = "";
                temp[40] = "";
                temp[41] = "";
                temp[42] = "";
                for(int d = 0; d < users.size(); d++)
                {
                    String this_user[] = users.get(d);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        temp[38] = this_user[1];
                        temp[39] = this_user[3];
                        temp[40] = this_user[4];
                        temp[41] = this_user[5];
                        temp[42] = this_user[17];
                        d = users.size();
                    }
                }
                requests.add(temp);
            }
        }
        catch(Exception e)
        {
            logger.error("ERROR sla_request_calc.get_noncompliant_requests_for_sla_id JSON Parse:=" + e);
        }
              
        //do the calculation
        //{"TIME"};
        switch(sla_type) 
        { 
            case "TIME": 
                return_value = get_noncompliant_requests_calc_time(sla_info,requests);
                //System.out.println("TIME incident sla"); 
                break; 
            default: 
                //System.out.println("no match"); 
        }
        return return_value;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static ArrayList<String[]> get_noncompliant_requests_calc_time(String sla_info[], ArrayList <String[]>requests) throws Exception //arraylist {incident_id, start_date, stop_date}
    {
        ArrayList return_requests = new ArrayList<String[]>();
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_requests = 0;
        int total_requests_meeting_sla = 0;
        int total_requests_failing_sla = 0;
        java.util.Date start = new java.util.Date();
        java.util.Date end = new java.util.Date();
        //get the start state
        String start_field = sla_info[7];        
        //get the end state
        String end_field = sla_info[9];
        //get stop_state
        String stop_state = sla_info[10];
        
        try
        {
            target = Double.parseDouble(sla_info[6]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_request_calc.get_noncompliant_requests_calc_time target definition:=" + e);
        }
        sla_operator = sla_info[18];
        try
        {
            sla_value = Double.parseDouble(sla_info[19]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_request_calc.get_noncompliant_requests_calc_time sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[20];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        //for each incident        
            //get the diff between "start date" and "end date"            
            //use operator to determine good or bad
            //increment good/bad counts
        //end for
        //System.out.println("number of requests=" + requests.size());
        try
        {
            //System.out.println("requests.size()=" + requests.size());
            for(int a = 0; a < requests.size(); a++)
            {
                String request[] = requests.get(a);
                if(request[18].equalsIgnoreCase(stop_state))
                {
                    //figure out start field and stop
                    if(start_field.equalsIgnoreCase("create_date"))
                    {
                        start = timestamp.parse(request[2]);
                    }
                    else if(start_field.equalsIgnoreCase("request_date"))
                    {
                        start = timestamp.parse(request[3]);
                    }
                    else if(start_field.equalsIgnoreCase("resolve_date"))
                    {
                        start = timestamp.parse(request[4]);
                    }
                    else if(start_field.equalsIgnoreCase("closed_date"))
                    {
                        start = timestamp.parse(request[4]);
                    }
                    /////////////////////////////////////////////////////////////////
                    if(end_field.equalsIgnoreCase("create_date"))
                    {
                        end = timestamp.parse(request[2]);
                    }
                    else if(end_field.equalsIgnoreCase("request_date"))
                    {
                        end = timestamp.parse(request[3]);
                    }
                    else if(end_field.equalsIgnoreCase("resolve_date"))
                    {
                        end = timestamp.parse(request[4]);
                    }
                    else if(end_field.equalsIgnoreCase("closed_date"))
                    {
                        end = timestamp.parse(request[4]);
                    }

                    long diff = ((end.getTime() - start.getTime()) / 1000);
                    //System.out.println("diff=" + diff   + "  sla_value=" + sla_value);
                    total_requests++;
                    //{"greater_than", "greater_than_or_equal", "equal_to", "not_equal_to", "less_than_or_equal", "less_than"}
                    //System.out.println("operator=" + sla_operator);

                    //if the end state is not the sla defined end state then ignore it
                    switch(sla_operator) 
                    { 
                        case "greater_than": 
                            if(diff > sla_value)
                            {
                                total_requests_meeting_sla++;
                            }
                            else
                            {
                                total_requests_failing_sla++;
                                return_requests.add(request);
                            }
                            break; 
                        case "greater_than_or_equal":
                            if(diff >= sla_value)
                            {
                                total_requests_meeting_sla++;
                            }
                            else
                            {
                                total_requests_failing_sla++;
                                return_requests.add(request);
                            }
                            break; 
                        case "equal_to": 
                            if(diff == sla_value)
                            {
                                total_requests_meeting_sla++;
                            }
                            else
                            {
                                total_requests_failing_sla++;
                                return_requests.add(request);
                            }
                            break; 
                        case "not_equal_to": 
                            if(diff != sla_value)
                            {
                                total_requests_meeting_sla++;
                            }
                            else
                            {
                                total_requests_failing_sla++;
                                return_requests.add(request);
                            }
                            break; 
                        case "less_than_or_equal": 
                            if(diff <= sla_value)
                            {
                                total_requests_meeting_sla++;
                            }
                            else
                            {
                                total_requests_failing_sla++;
                                return_requests.add(request);
                            }
                            break; 
                        case "less_than": 
                            if(diff < sla_value)
                            {
                                total_requests_meeting_sla++;
                            }
                            else
                            {
                                total_requests_failing_sla++;
                                return_requests.add(request);
                            }
                            break; 
                        default: 
                            //System.out.println("no match");     
                    }
                }
            } 
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_request_calc.get_noncompliant_requests_calc_time calc loop=" + e);
        }
        
        //System.out.println("REQUEST return_double=" + return_double);
        return return_requests;
    }  
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    public static double[] for_service_catalog_item_id(Connection con, String service_catalog_item_id,  java.util.Date start, java.util.Date end) throws Exception, SQLException
    {
        //System.out.println("starting sla_contact_calc");
        double return_value[] = {Double.parseDouble(service_catalog_item_id), 0.0, 0.0 , 0.0 , 0.0};  //sc_item_id, times_requested, PassSLA, FailSLA, SLA_score
        int record_count = 0;
        PreparedStatement stmt;
        //get the requests that are closed_complete for this time period
        String query = "SELECT * FROM `request` WHERE service_catalog_item_id=? AND state='Closed_Complete' AND due_date >= ? AND due_date <= ?";
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, service_catalog_item_id);
            stmt.setTimestamp(2, new Timestamp(start.getTime()));
            stmt.setTimestamp(3, new Timestamp(end.getTime()));
            //System.out.println("query=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                //for each request
                return_value[1] = return_value[1] + 1;
                //get date_closed  and due date
                long closed_date = rs.getTimestamp("closed_date").getTime();
                long due_date = rs.getTimestamp("due_date").getTime();
                //is date_closed <= due_date?
                if(closed_date <= due_date)
                {
                    //yes GOOD
                    //add 1 to return_value[0]
                    return_value[2] = return_value[2] + 1;
                }
                else
                {
                //else
                    //no Failed
                    //add 1 to return_value[1]
                    return_value[3] = return_value[3] + 1;
                }            
                record_count++;
                //end for each request
            }
            //calc sla
            if(record_count == 0)
            {
                return_value[4] = 100.00;
            }
            else
            {
                if(return_value[2] == 0)
                {
                    return_value[4] = 00.00;
                }
                else
                {
                    //(#good / total#) * 100 = score
                    double score = (return_value[2] / record_count) * 100;
                    //return_value[2] = score
                    return_value[4] = score;
                }
            }
            stmt.close();
            
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException sla_request_calc.for_service_catalog_item_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception sla_request_calc.for_service_catalog_item_id:=" + exc);
	}
        return return_value;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    public static double[][] for_service_catalog_item_id_by_priority(Connection con, String service_catalog_item_id,  java.util.Date start, java.util.Date end) throws Exception, SQLException
    {
        //System.out.println("starting sla_contact_calc");
        String priority = "";
        double return_value[][] = {{0.0 , 0.0 , 0.0},{0.0 , 0.0 , 0.0},{0.0 , 0.0 , 0.0},{0.0 , 0.0 , 0.0}};
        PreparedStatement stmt;
        //get the requests that are closed_complete for this time period
        String query = "SELECT * FROM `request` WHERE state='closed_complete' AND closed_date >= ? AND closed_date <= ?";
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setTimestamp(1, new Timestamp(start.getTime()));
            stmt.setTimestamp(2, new Timestamp(end.getTime()));
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                priority = rs.getString("priority");
                long closed_date = rs.getTimestamp("closed_date").getTime();
                long due_date = rs.getTimestamp("due_date").getTime();
                if(priority.equalsIgnoreCase("critical"))
                {
                    if(closed_date <= due_date)
                    {
                        return_value[0][0] = return_value[0][0] + 1;
                    }
                    else
                    {
                        return_value[0][1] = return_value[0][1] + 1;
                    }
                }
                else if(priority.equalsIgnoreCase("high"))
                {
                    if(closed_date <= due_date)
                    {
                        return_value[1][0] = return_value[1][0] + 1;
                    }
                    else
                    {
                        return_value[1][1] = return_value[1][1] + 1;
                    }
                }
                else if(priority.equalsIgnoreCase("medium"))
                {
                    if(closed_date <= due_date)
                    {
                        return_value[2][0] = return_value[2][0] + 1;
                    }
                    else
                    {
                        return_value[2][1] = return_value[2][1] + 1;
                    }
                }
                else if(priority.equalsIgnoreCase("low"))
                {
                    if(closed_date <= due_date)
                    {
                        return_value[3][0] = return_value[3][0] + 1;
                    }
                    else
                    {
                        return_value[3][1] = return_value[3][1] + 1;
                    }
                }
                //end for each request
            }
            
            //calc sla for each priority
            //critical
            double critical_good = return_value[0][0];
            double critical_bad = return_value[0][1];
            double critical_total = critical_good + critical_bad;
            double critical_score = 100;
            if(critical_total == 0)
            {
                critical_score = 100.00;
            }
            else
            {
                if(critical_good == 0)
                {
                    critical_score = 0.00;
                }
                else
                {
                    critical_score = (critical_good / critical_total) * 100;
                    return_value[0][2] = critical_score;
                }
            }
            //high
            double high_good = return_value[1][0];
            double high_bad = return_value[1][1];
            double high_total = high_good + high_bad;
            double high_score = 100;
            if(high_total == 0)
            {
                high_score = 100.00;
            }
            else
            {
                if(high_good == 0)
                {
                    high_score = 0.00;
                }
                else
                {
                    high_score = (high_good / high_total) * 100;
                    return_value[1][2] = high_score;
                }
            }
            //medium
            double medium_good = return_value[2][0];
            double medium_bad = return_value[2][1];
            double medium_total = medium_good + medium_bad;
            double medium_score = 100;
            if(medium_total == 0)
            {
                medium_score = 100.00;
            }
            else
            {
                if(medium_good == 0)
                {
                    medium_score = 0.00;
                }
                else
                {
                    medium_score = (medium_good / medium_total) * 100;
                    return_value[2][2] = medium_score;
                }
            }
            //low
            double low_good = return_value[3][0];
            double low_bad = return_value[3][1];
            double low_total = low_good + low_bad;
            double low_score = 100;
            if(low_total == 0)
            {
                low_score = 100.00;
            }
            else
            {
                if(low_good == 0)
                {
                    low_score = 0.00;
                }
                else
                {
                    low_score = (low_good / low_total) * 100;
                    return_value[3][2] = low_score;
                }
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException sla_request_calc.for_service_catalog_item_id_by_priority:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception sla_request_calc.for_service_catalog_item_id_by_priority:=" + exc);
	}
        return return_value;
    }
}

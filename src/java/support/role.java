/*
 * Copyright 2019 Real Data Technologies, Inc. , All rights reserved.
 */
package support;

import javax.servlet.http.HttpSession;

/**
 *
 * @author Ralph
 */
public class role 
{
    public static boolean authorized(HttpSession Session, String DataObject, String CRUD) 
    {
        boolean return_boolean = false;
        
        if(DataObject.equalsIgnoreCase("FINANCIAL"))
        {
            String session_financial = Session.getAttribute("financial").toString();
            int users_financial_crud = get_crud_value(session_financial);
            int object_crud = get_crud_value(CRUD);
            if(users_financial_crud >= object_crud)
            {
                return_boolean = true;
            }
        }
        else if(DataObject.equalsIgnoreCase("PROBLEM"))
        {
            String session_problem = Session.getAttribute("problem").toString();
            int users_problem_crud = get_crud_value(session_problem);
            int object_crud = get_crud_value(CRUD);
            if(users_problem_crud >= object_crud)
            {
                return_boolean = true;
            }
        }
        else if(DataObject.equalsIgnoreCase("INCIDENT"))
        {
            String session_incident = Session.getAttribute("incident").toString();
            int users_incident_crud = get_crud_value(session_incident);
            int object_crud = get_crud_value(CRUD);
            if(users_incident_crud >= object_crud)
            {
                return_boolean = true;
            }
        }
        else if(DataObject.equalsIgnoreCase("REQUEST"))
        {
            String session_request = Session.getAttribute("request").toString();
            int users_request_crud = get_crud_value(session_request);
            int object_crud = get_crud_value(CRUD);
            if(users_request_crud >= object_crud)
            {
                return_boolean = true;
            }
        }
        else if(DataObject.equalsIgnoreCase("CONTACT"))
        {
            String session_contact = Session.getAttribute("contact").toString();
            int users_contact_crud = get_crud_value(session_contact);
            int object_crud = get_crud_value(CRUD);
            if(users_contact_crud >= object_crud)
            {
                return_boolean = true;
            }
        }
        else if(DataObject.equalsIgnoreCase("CX"))
        {
            String session_cx = Session.getAttribute("cx").toString();
            int users_cx_crud = get_crud_value(session_cx);
            int object_crud = get_crud_value(CRUD);
            if(users_cx_crud >= object_crud)
            {
                return_boolean = true;
            }
        }
        else if(DataObject.equalsIgnoreCase("SURVEY"))
        {
            String session_survey = Session.getAttribute("survey").toString();
            int users_survey_crud = get_crud_value(session_survey);
            int object_crud = get_crud_value(CRUD);
            if(users_survey_crud >= object_crud)
            {
                return_boolean = true;
            }
        }
        else if(DataObject.equalsIgnoreCase("SLA"))
        {
            String session_sla = Session.getAttribute("sla").toString();
            int users_sla_crud = get_crud_value(session_sla);
            int object_crud = get_crud_value(CRUD);
            if(users_sla_crud >= object_crud)
            {
                return_boolean = true;
            }
        }
        else if(DataObject.equalsIgnoreCase("PROJECT"))
        {
            String session_project = Session.getAttribute("project").toString();
            int users_project_crud = get_crud_value(session_project);
            int object_crud = get_crud_value(CRUD);
            if(users_project_crud >= object_crud)
            {
                return_boolean = true;
            }
        }
        else if(DataObject.equalsIgnoreCase("JOB"))
        {
            String session_job = Session.getAttribute("job").toString();
            int users_job_crud = get_crud_value(session_job);
            int object_crud = get_crud_value(CRUD);
            if(users_job_crud >= object_crud)
            {
                return_boolean = true;
            }
        } 
        else if(DataObject.equalsIgnoreCase("METRIC"))
        {
            String session_job = Session.getAttribute("metric").toString();
            int users_job_crud = get_crud_value(session_job);
            int object_crud = get_crud_value(CRUD);
            if(users_job_crud >= object_crud)
            {
                return_boolean = true;
            }
        } 
        else if(DataObject.equalsIgnoreCase("ASSET"))
        {
            String session_job = Session.getAttribute("asset").toString();
            int users_job_crud = get_crud_value(session_job);
            int object_crud = get_crud_value(CRUD);
            if(users_job_crud >= object_crud)
            {
                return_boolean = true;
            }
        } 
        else if(DataObject.equalsIgnoreCase("USER"))
        {
            String session_user = Session.getAttribute("user").toString();
            int users_user_crud = get_crud_value(session_user);
            int object_crud = get_crud_value(CRUD);
            if(users_user_crud >= object_crud)
            {
                return_boolean = true;
            }
        } 
        else if(DataObject.equalsIgnoreCase("SELF_SERVICE"))
        {
            String session_self_service = Session.getAttribute("self_service").toString();
            int users_self_service_crud = get_crud_value(session_self_service);
            int object_crud = get_crud_value(CRUD);
            if(users_self_service_crud >= object_crud)
            {
                return_boolean = true;
            }
        } 
        else if(DataObject.equalsIgnoreCase("ADMINISTRATION"))
        {
            if(Session.getAttribute("administration").toString().equalsIgnoreCase("true"))
            {
                return_boolean = true;
            }            
        }
        else if(DataObject.equalsIgnoreCase("MANAGER"))
        {
            if(Session.getAttribute("manager").toString().equalsIgnoreCase("true"))
            {
                return_boolean = true;
            }            
        }
        //override for admin and managers
        if(Session.getAttribute("administration").toString().equalsIgnoreCase("true"))
        {
            return_boolean = true;
        }
        return return_boolean;
    }
    public static int get_crud_value(String CRUD_VALUE)
    {
        int return_int = 0;
        if(CRUD_VALUE.equalsIgnoreCase("none"))
        {
            return_int = 0;
        }
        else if(CRUD_VALUE.equalsIgnoreCase("read"))
        {
            return_int = 1;
        }
        else if(CRUD_VALUE.equalsIgnoreCase("create"))
        {
            return_int = 2;
        }
        else if(CRUD_VALUE.equalsIgnoreCase("update"))
        {
            return_int = 3;
        }
        else if(CRUD_VALUE.equalsIgnoreCase("delete"))
        {
            return_int = 4;
        }
        return return_int;
    }
    
}

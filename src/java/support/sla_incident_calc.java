/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package support;

import db.get_users;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class sla_incident_calc 
{
    private static final Logger logger = LogManager.getLogger(sla_incident_calc.class.getName());
    private static SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
    
    private static long OLD_number_of_days_between(java.util.Date one, java.util.Date two) 
    { 
        long difference = (one.getTime()-two.getTime())/86400000; 
        return Math.abs(difference); 
    }    
    public static boolean OLD_has_start_and_stop(String[][] incident_states, String start_state, String stop_state)
    {
        boolean return_boolean = false;
        boolean start_state_found = false;
        boolean stop_state_found = false;
        
        for(int a = 0; a < incident_states.length;a++)
        {
            if(incident_states[a][0].equalsIgnoreCase(start_state))
            {
                start_state_found = true;
            }
            else
            {
                if(incident_states[a][0].equalsIgnoreCase(stop_state))
                {
                    stop_state_found = true;
                }
            }
        }
        if(start_state_found && stop_state_found)
        {
            return_boolean = true;
        }
        return return_boolean;
    }
    public static ArrayList<long[]> OLD_get_pause_times(String[][] incident_states,String start_state, String pause_state, String stop_state)
    {
        long start_date = 0;
        long stop_date = 0;
        boolean start_date_set = false;
        boolean stop_date_set = false;
        boolean sla_start_set = false;
        boolean sla_stop_set = false;
        
        ArrayList<long[]> return_arraylist = new ArrayList<long[]>();
        
        //return_string[count][21] = rs.getString("state");
        //return_string[count][22] = timestamp_format.format(rs.getTimestamp("state_date"));
        for(int a = 0; a < incident_states.length;a++)
        {
            if(incident_states[a][21].equalsIgnoreCase(start_state))
            {
                sla_start_set = true;
            }
            else
            {
                if(incident_states[a][21].equalsIgnoreCase(stop_state))
                {
                    sla_stop_set = true;
                }
            }
            
            if(sla_start_set && !sla_stop_set && incident_states[a][21].equalsIgnoreCase(pause_state))
            {
                //get pause start date
                start_date = Long.parseLong(incident_states[a][22]);
                start_date_set = true;
            }
            
            if(sla_start_set && !sla_stop_set && start_date_set && !incident_states[a][21].equalsIgnoreCase(pause_state))
            {
                //get pause start date
                stop_date = Long.parseLong(incident_states[a][22]);
                if(start_date_set)
                {
                    long times[] = {start_date,stop_date};
                    return_arraylist.add(times);
                    start_date_set = false;
                    stop_date_set = false;
                    start_date = 0;
                    stop_date = 0;
                }
            }
        }
        return return_arraylist;
    }
    public static ArrayList<long[]> OLD_get_schedule_pause_times(Connection con, String schedule_id, java.util.Date start, java.util.Date end)
    {
        ArrayList<long[]> return_arraylist = new ArrayList<long[]>();
        long one_day_in_milliseconds = 24*60*60*1000;
        try
        {
            String schedule_info[] = db.get_schedules.by_id(con, schedule_id);
            /*returnString[0] = rs.getString("id");
            returnString[1] = rs.getString("name");
            returnString[2] = rs.getString("description");
            returnString[3] = rs.getString("start_time");
            returnString[4] = rs.getString("end_time");
            returnString[5] = rs.getString("is_all_day");
            returnString[6] = rs.getString("include_holidays");     
            returnString[7] = rs.getString("sunday");     
            returnString[8] = rs.getString("monday");     
            returnString[9] = rs.getString("tuesday");     
            returnString[10] = rs.getString("wednesday");     
            returnString[11] = rs.getString("thursday");     
            returnString[12] = rs.getString("friday");     
            returnString[13] = rs.getString("saturday");*/
            //Getting the day of week for a given date
            int start_hour = 0;
            int start_minute = 0;
            int start_second = 0;
            try
            {
                String temp_start[] = schedule_info[3].split(":");
                start_hour = Integer.parseInt(temp_start[0]);
                start_minute = Integer.parseInt(temp_start[1]);
                start_second = Integer.parseInt(temp_start[2]);                
            }
            catch(Exception f)
            {
                logger.error("ERROR in sla_incident_calc.get_schedule_pause_times converter db schedule start:=" + f);
                start_hour = 0;
                start_minute = 0;
                start_second = 0;
            }
            
            int end_hour = 0;
            int end_minute = 0;
            int end_second = 0;
            String temp_end[] = schedule_info[4].split(":");
            try
            {
                String temp_start[] = schedule_info[3].split(":");
                end_hour = Integer.parseInt(temp_start[0]);
                end_minute = Integer.parseInt(temp_start[1]);
                end_second = Integer.parseInt(temp_start[2]);                
            }
            catch(Exception f)
            {
                logger.error("ERROR in sla_incident_calc.get_schedule_pause_times converter db schedule end:=" + f);
                end_hour = 0;
                end_minute = 0;
                end_second = 0;
            }
            
            //MONDAY(int value=1), TUESDAY(2), WEDNESDAY(3)… till SUNDAY(7).
            while(start.getTime() < end.getTime())
            {
                LocalDate start_local_date = start.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                
                java.time.DayOfWeek day_of_week = start_local_date.getDayOfWeek();
                switch (day_of_week) 
                {
                    case SUNDAY: 
                        if(schedule_info[7].equalsIgnoreCase("0")) //does not include sunday
                        {
                            //add a start/end to return_arraylist
                            Calendar start_cal = Calendar.getInstance();
                            Calendar end_cal = Calendar.getInstance();
                            start_cal.setTime(start);
                            start_cal.set(start_cal.HOUR, 0);
                            start_cal.set(start_cal.MINUTE, 0);
                            start_cal.set(start_cal.SECOND, 0);
                            long t_start = start_cal.getTimeInMillis();
                            end_cal.setTime(start);
                            end_cal.set(start_cal.HOUR, 23);
                            end_cal.set(start_cal.MINUTE, 59);
                            end_cal.set(start_cal.SECOND, 29);
                            long t_end = start_cal.getTimeInMillis();
                            long t_array[] = {t_start,t_end};
                            return_arraylist.add(t_array);
                        }
                        else
                        {
                            //if the start is 00:00:00 then the day must be one split
                            if(start_hour == 0 && start_minute == 0 && start_second ==0)
                            {
                                //single array entry
                                Calendar start_cal = Calendar.getInstance();
                                Calendar end_cal = Calendar.getInstance();
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, end_hour);
                                start_cal.set(start_cal.MINUTE, end_minute);
                                start_cal.set(start_cal.SECOND, end_second);
                                long t_start = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, 23);
                                end_cal.set(start_cal.MINUTE, 59);
                                end_cal.set(start_cal.SECOND, 29);
                                long t_end = start_cal.getTimeInMillis();
                                long t_array[] = {t_start,t_end};
                                return_arraylist.add(t_array); 
                            }
                            else
                            {
                                //double array entry
                                Calendar start_cal = Calendar.getInstance();
                                Calendar end_cal = Calendar.getInstance();
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, 0);
                                start_cal.set(start_cal.MINUTE, 0);
                                start_cal.set(start_cal.SECOND, 0);
                                long t_start = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, start_hour);
                                end_cal.set(start_cal.MINUTE, start_minute);
                                end_cal.set(start_cal.SECOND, start_second);
                                long t_end = start_cal.getTimeInMillis();
                                long t_array[] = {t_start,t_end};
                                return_arraylist.add(t_array); 
                                
                                //second half                                
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, end_hour);
                                start_cal.set(start_cal.MINUTE, end_minute);
                                start_cal.set(start_cal.SECOND, end_second);
                                long t_start1 = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, 23);
                                end_cal.set(start_cal.MINUTE, 59);
                                end_cal.set(start_cal.SECOND, 29);
                                long t_end1 = start_cal.getTimeInMillis();
                                long t_array1[] = {t_start,t_end};
                                return_arraylist.add(t_array1); 
                                
                            }
                        }
                        break;
                    case MONDAY: 
                        if(schedule_info[8].equalsIgnoreCase("0")) //does not include monday
                        {
                            //add a start/end to return_arraylist
                            Calendar start_cal = Calendar.getInstance();
                            Calendar end_cal = Calendar.getInstance();
                            start_cal.setTime(start);
                            start_cal.set(start_cal.HOUR, 0);
                            start_cal.set(start_cal.MINUTE, 0);
                            start_cal.set(start_cal.SECOND, 0);
                            long t_start = start_cal.getTimeInMillis();
                            end_cal.setTime(start);
                            end_cal.set(start_cal.HOUR, 23);
                            end_cal.set(start_cal.MINUTE, 59);
                            end_cal.set(start_cal.SECOND, 29);
                            long t_end = start_cal.getTimeInMillis();
                            long t_array[] = {t_start,t_end};
                            return_arraylist.add(t_array);
                        }
                        else
                        {
                            //if the start is 00:00:00 then the day must be one split
                            if(start_hour == 0 && start_minute == 0 && start_second ==0)
                            {
                                //single array entry
                                Calendar start_cal = Calendar.getInstance();
                                Calendar end_cal = Calendar.getInstance();
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, end_hour);
                                start_cal.set(start_cal.MINUTE, end_minute);
                                start_cal.set(start_cal.SECOND, end_second);
                                long t_start = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, 23);
                                end_cal.set(start_cal.MINUTE, 59);
                                end_cal.set(start_cal.SECOND, 29);
                                long t_end = start_cal.getTimeInMillis();
                                long t_array[] = {t_start,t_end};
                                return_arraylist.add(t_array); 
                            }
                            else
                            {
                                //double array entry
                                Calendar start_cal = Calendar.getInstance();
                                Calendar end_cal = Calendar.getInstance();
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, 0);
                                start_cal.set(start_cal.MINUTE, 0);
                                start_cal.set(start_cal.SECOND, 0);
                                long t_start = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, start_hour);
                                end_cal.set(start_cal.MINUTE, start_minute);
                                end_cal.set(start_cal.SECOND, start_second);
                                long t_end = start_cal.getTimeInMillis();
                                long t_array[] = {t_start,t_end};
                                return_arraylist.add(t_array); 
                                
                                //second half                                
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, end_hour);
                                start_cal.set(start_cal.MINUTE, end_minute);
                                start_cal.set(start_cal.SECOND, end_second);
                                long t_start1 = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, 23);
                                end_cal.set(start_cal.MINUTE, 59);
                                end_cal.set(start_cal.SECOND, 29);
                                long t_end1 = start_cal.getTimeInMillis();
                                long t_array1[] = {t_start,t_end};
                                return_arraylist.add(t_array1); 
                                
                            }
                        }
                        break;
                    case TUESDAY: 
                        if(schedule_info[9].equalsIgnoreCase("0")) //does not include tues
                        {
                            //add a start/end to return_arraylist
                            Calendar start_cal = Calendar.getInstance();
                            Calendar end_cal = Calendar.getInstance();
                            start_cal.setTime(start);
                            start_cal.set(start_cal.HOUR, 0);
                            start_cal.set(start_cal.MINUTE, 0);
                            start_cal.set(start_cal.SECOND, 0);
                            long t_start = start_cal.getTimeInMillis();
                            end_cal.setTime(start);
                            end_cal.set(start_cal.HOUR, 23);
                            end_cal.set(start_cal.MINUTE, 59);
                            end_cal.set(start_cal.SECOND, 29);
                            long t_end = start_cal.getTimeInMillis();
                            long t_array[] = {t_start,t_end};
                            return_arraylist.add(t_array);
                        }
                        else
                        {
                            //if the start is 00:00:00 then the day must be one split
                            if(start_hour == 0 && start_minute == 0 && start_second ==0)
                            {
                                //single array entry
                                Calendar start_cal = Calendar.getInstance();
                                Calendar end_cal = Calendar.getInstance();
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, end_hour);
                                start_cal.set(start_cal.MINUTE, end_minute);
                                start_cal.set(start_cal.SECOND, end_second);
                                long t_start = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, 23);
                                end_cal.set(start_cal.MINUTE, 59);
                                end_cal.set(start_cal.SECOND, 29);
                                long t_end = start_cal.getTimeInMillis();
                                long t_array[] = {t_start,t_end};
                                return_arraylist.add(t_array); 
                            }
                            else
                            {
                                //double array entry
                                Calendar start_cal = Calendar.getInstance();
                                Calendar end_cal = Calendar.getInstance();
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, 0);
                                start_cal.set(start_cal.MINUTE, 0);
                                start_cal.set(start_cal.SECOND, 0);
                                long t_start = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, start_hour);
                                end_cal.set(start_cal.MINUTE, start_minute);
                                end_cal.set(start_cal.SECOND, start_second);
                                long t_end = start_cal.getTimeInMillis();
                                long t_array[] = {t_start,t_end};
                                return_arraylist.add(t_array); 
                                
                                //second half                                
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, end_hour);
                                start_cal.set(start_cal.MINUTE, end_minute);
                                start_cal.set(start_cal.SECOND, end_second);
                                long t_start1 = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, 23);
                                end_cal.set(start_cal.MINUTE, 59);
                                end_cal.set(start_cal.SECOND, 29);
                                long t_end1 = start_cal.getTimeInMillis();
                                long t_array1[] = {t_start,t_end};
                                return_arraylist.add(t_array1); 
                                
                            }
                        }
                        break;
                    case WEDNESDAY: 
                        if(schedule_info[10].equalsIgnoreCase("0")) //does not include weds
                        {
                            //add a start/end to return_arraylist
                            Calendar start_cal = Calendar.getInstance();
                            Calendar end_cal = Calendar.getInstance();
                            start_cal.setTime(start);
                            start_cal.set(start_cal.HOUR, 0);
                            start_cal.set(start_cal.MINUTE, 0);
                            start_cal.set(start_cal.SECOND, 0);
                            long t_start = start_cal.getTimeInMillis();
                            end_cal.setTime(start);
                            end_cal.set(start_cal.HOUR, 23);
                            end_cal.set(start_cal.MINUTE, 59);
                            end_cal.set(start_cal.SECOND, 29);
                            long t_end = start_cal.getTimeInMillis();
                            long t_array[] = {t_start,t_end};
                            return_arraylist.add(t_array);
                        }
                        else
                        {
                            //if the start is 00:00:00 then the day must be one split
                            if(start_hour == 0 && start_minute == 0 && start_second ==0)
                            {
                                //single array entry
                                Calendar start_cal = Calendar.getInstance();
                                Calendar end_cal = Calendar.getInstance();
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, end_hour);
                                start_cal.set(start_cal.MINUTE, end_minute);
                                start_cal.set(start_cal.SECOND, end_second);
                                long t_start = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, 23);
                                end_cal.set(start_cal.MINUTE, 59);
                                end_cal.set(start_cal.SECOND, 29);
                                long t_end = start_cal.getTimeInMillis();
                                long t_array[] = {t_start,t_end};
                                return_arraylist.add(t_array); 
                            }
                            else
                            {
                                //double array entry
                                Calendar start_cal = Calendar.getInstance();
                                Calendar end_cal = Calendar.getInstance();
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, 0);
                                start_cal.set(start_cal.MINUTE, 0);
                                start_cal.set(start_cal.SECOND, 0);
                                long t_start = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, start_hour);
                                end_cal.set(start_cal.MINUTE, start_minute);
                                end_cal.set(start_cal.SECOND, start_second);
                                long t_end = start_cal.getTimeInMillis();
                                long t_array[] = {t_start,t_end};
                                return_arraylist.add(t_array); 
                                
                                //second half                                
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, end_hour);
                                start_cal.set(start_cal.MINUTE, end_minute);
                                start_cal.set(start_cal.SECOND, end_second);
                                long t_start1 = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, 23);
                                end_cal.set(start_cal.MINUTE, 59);
                                end_cal.set(start_cal.SECOND, 29);
                                long t_end1 = start_cal.getTimeInMillis();
                                long t_array1[] = {t_start,t_end};
                                return_arraylist.add(t_array1); 
                                
                            }
                        }
                        break;
                    case THURSDAY: 
                        if(schedule_info[11].equalsIgnoreCase("0")) //does not include thurs
                        {
                            //add a start/end to return_arraylist
                            Calendar start_cal = Calendar.getInstance();
                            Calendar end_cal = Calendar.getInstance();
                            start_cal.setTime(start);
                            start_cal.set(start_cal.HOUR, 0);
                            start_cal.set(start_cal.MINUTE, 0);
                            start_cal.set(start_cal.SECOND, 0);
                            long t_start = start_cal.getTimeInMillis();
                            end_cal.setTime(start);
                            end_cal.set(start_cal.HOUR, 23);
                            end_cal.set(start_cal.MINUTE, 59);
                            end_cal.set(start_cal.SECOND, 29);
                            long t_end = start_cal.getTimeInMillis();
                            long t_array[] = {t_start,t_end};
                            return_arraylist.add(t_array);
                        }
                        else
                        {
                            //if the start is 00:00:00 then the day must be one split
                            if(start_hour == 0 && start_minute == 0 && start_second ==0)
                            {
                                //single array entry
                                Calendar start_cal = Calendar.getInstance();
                                Calendar end_cal = Calendar.getInstance();
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, end_hour);
                                start_cal.set(start_cal.MINUTE, end_minute);
                                start_cal.set(start_cal.SECOND, end_second);
                                long t_start = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, 23);
                                end_cal.set(start_cal.MINUTE, 59);
                                end_cal.set(start_cal.SECOND, 29);
                                long t_end = start_cal.getTimeInMillis();
                                long t_array[] = {t_start,t_end};
                                return_arraylist.add(t_array); 
                            }
                            else
                            {
                                //double array entry
                                Calendar start_cal = Calendar.getInstance();
                                Calendar end_cal = Calendar.getInstance();
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, 0);
                                start_cal.set(start_cal.MINUTE, 0);
                                start_cal.set(start_cal.SECOND, 0);
                                long t_start = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, start_hour);
                                end_cal.set(start_cal.MINUTE, start_minute);
                                end_cal.set(start_cal.SECOND, start_second);
                                long t_end = start_cal.getTimeInMillis();
                                long t_array[] = {t_start,t_end};
                                return_arraylist.add(t_array); 
                                
                                //second half                                
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, end_hour);
                                start_cal.set(start_cal.MINUTE, end_minute);
                                start_cal.set(start_cal.SECOND, end_second);
                                long t_start1 = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, 23);
                                end_cal.set(start_cal.MINUTE, 59);
                                end_cal.set(start_cal.SECOND, 29);
                                long t_end1 = start_cal.getTimeInMillis();
                                long t_array1[] = {t_start,t_end};
                                return_arraylist.add(t_array1); 
                                
                            }
                        }
                        break;
                    case FRIDAY: 
                        if(schedule_info[12].equalsIgnoreCase("0")) //does not include friday
                        {
                            //add a start/end to return_arraylist
                            Calendar start_cal = Calendar.getInstance();
                            Calendar end_cal = Calendar.getInstance();
                            start_cal.setTime(start);
                            start_cal.set(start_cal.HOUR, 0);
                            start_cal.set(start_cal.MINUTE, 0);
                            start_cal.set(start_cal.SECOND, 0);
                            long t_start = start_cal.getTimeInMillis();
                            end_cal.setTime(start);
                            end_cal.set(start_cal.HOUR, 23);
                            end_cal.set(start_cal.MINUTE, 59);
                            end_cal.set(start_cal.SECOND, 29);
                            long t_end = start_cal.getTimeInMillis();
                            long t_array[] = {t_start,t_end};
                            return_arraylist.add(t_array);
                        }
                        else
                        {
                            //if the start is 00:00:00 then the day must be one split
                            if(start_hour == 0 && start_minute == 0 && start_second ==0)
                            {
                                //single array entry
                                Calendar start_cal = Calendar.getInstance();
                                Calendar end_cal = Calendar.getInstance();
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, end_hour);
                                start_cal.set(start_cal.MINUTE, end_minute);
                                start_cal.set(start_cal.SECOND, end_second);
                                long t_start = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, 23);
                                end_cal.set(start_cal.MINUTE, 59);
                                end_cal.set(start_cal.SECOND, 29);
                                long t_end = start_cal.getTimeInMillis();
                                long t_array[] = {t_start,t_end};
                                return_arraylist.add(t_array); 
                            }
                            else
                            {
                                //double array entry
                                Calendar start_cal = Calendar.getInstance();
                                Calendar end_cal = Calendar.getInstance();
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, 0);
                                start_cal.set(start_cal.MINUTE, 0);
                                start_cal.set(start_cal.SECOND, 0);
                                long t_start = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, start_hour);
                                end_cal.set(start_cal.MINUTE, start_minute);
                                end_cal.set(start_cal.SECOND, start_second);
                                long t_end = start_cal.getTimeInMillis();
                                long t_array[] = {t_start,t_end};
                                return_arraylist.add(t_array); 
                                
                                //second half                                
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, end_hour);
                                start_cal.set(start_cal.MINUTE, end_minute);
                                start_cal.set(start_cal.SECOND, end_second);
                                long t_start1 = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, 23);
                                end_cal.set(start_cal.MINUTE, 59);
                                end_cal.set(start_cal.SECOND, 29);
                                long t_end1 = start_cal.getTimeInMillis();
                                long t_array1[] = {t_start,t_end};
                                return_arraylist.add(t_array1); 
                                
                            }
                        }
                        break;
                    case SATURDAY: 
                        if(schedule_info[13].equalsIgnoreCase("0")) //does not include thurs
                        {
                            //add a start/end to return_arraylist
                            Calendar start_cal = Calendar.getInstance();
                            Calendar end_cal = Calendar.getInstance();
                            start_cal.setTime(start);
                            start_cal.set(start_cal.HOUR, 0);
                            start_cal.set(start_cal.MINUTE, 0);
                            start_cal.set(start_cal.SECOND, 0);
                            long t_start = start_cal.getTimeInMillis();
                            end_cal.setTime(start);
                            end_cal.set(start_cal.HOUR, 23);
                            end_cal.set(start_cal.MINUTE, 59);
                            end_cal.set(start_cal.SECOND, 29);
                            long t_end = start_cal.getTimeInMillis();
                            long t_array[] = {t_start,t_end};
                            return_arraylist.add(t_array);
                        }
                        else
                        {
                            //if the start is 00:00:00 then the day must be one split
                            if(start_hour == 0 && start_minute == 0 && start_second ==0)
                            {
                                //single array entry
                                Calendar start_cal = Calendar.getInstance();
                                Calendar end_cal = Calendar.getInstance();
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, end_hour);
                                start_cal.set(start_cal.MINUTE, end_minute);
                                start_cal.set(start_cal.SECOND, end_second);
                                long t_start = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, 23);
                                end_cal.set(start_cal.MINUTE, 59);
                                end_cal.set(start_cal.SECOND, 29);
                                long t_end = start_cal.getTimeInMillis();
                                long t_array[] = {t_start,t_end};
                                return_arraylist.add(t_array); 
                            }
                            else
                            {
                                //double array entry
                                Calendar start_cal = Calendar.getInstance();
                                Calendar end_cal = Calendar.getInstance();
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, 0);
                                start_cal.set(start_cal.MINUTE, 0);
                                start_cal.set(start_cal.SECOND, 0);
                                long t_start = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, start_hour);
                                end_cal.set(start_cal.MINUTE, start_minute);
                                end_cal.set(start_cal.SECOND, start_second);
                                long t_end = start_cal.getTimeInMillis();
                                long t_array[] = {t_start,t_end};
                                return_arraylist.add(t_array); 
                                
                                //second half                                
                                start_cal.setTime(start);
                                start_cal.set(start_cal.HOUR, end_hour);
                                start_cal.set(start_cal.MINUTE, end_minute);
                                start_cal.set(start_cal.SECOND, end_second);
                                long t_start1 = start_cal.getTimeInMillis();
                                end_cal.setTime(start);
                                end_cal.set(start_cal.HOUR, 23);
                                end_cal.set(start_cal.MINUTE, 59);
                                end_cal.set(start_cal.SECOND, 29);
                                long t_end1 = start_cal.getTimeInMillis();
                                long t_array1[] = {t_start,t_end};
                                return_arraylist.add(t_array1); 
                                
                            }
                        }
                        break;                    
                }
                //add 1 day
                start.setTime(start.getTime() + one_day_in_milliseconds);
            }//end while
        }
        catch(Exception e)
        {
            logger.error("ERROR in sla_incident_calc.get_schedule_pause_times:=" + e);
        }
        return return_arraylist;
    }    
    public static double OLD_calc(String context_dir, java.util.Date start, java.util.Date end, String sla_id, String incident_id) 
    {
        double return_value = 0;
        long incident_segment[];
        long incident_segment_start = 0;
        long incident_segment_end = 0;
        long pending_segment[];
        long pending_segment_start = 0;
        long pending_segment_end = 0;
                
        //get sla_info
        
        //get incident_history
        
        //determine incident sla start and end from sla definition
        
        //1546300800000 Tuesday, January 1, 2019 12:00:00 AM 
        
        //1546992000000 Wednesday, January 9, 2019 12:00:00 AM 
        
        long temp[] = new long[2];
        temp[0] = 1546300800000L;
        temp[1] = 1546992000000L;
        long diff = 1546992000000L - 1546300800000L;
        
        
        //create an array of dates for each day in the sla start and end
        ArrayList<long[]> incident_segments = new ArrayList();
        
        incident_segments.add(temp);
        //System.out.println("initial total diff = " + diff + "  total segments=" + incident_segments.size());
        
        //recreate the incident_segments with the pending times removed
        ArrayList<long[]> new_incident_segments = new ArrayList();
        
        //create an array of the pending dates in long
        ArrayList<long[]> pending_segments = new ArrayList();
        //long pending_dates[][] = new long[2][2];
        
        //remove pending from the incident_days array
        for(int a = 0; a < pending_segments.size(); a++)
        {
            pending_segment = pending_segments.get(a);
            pending_segment_start = pending_segment[0];
            pending_segment_end = pending_segment[1];
            int start_segment_index = 0;
            int end_segment_index = 0;
            //loop thru the incident segments to find which segment is the start and which is the stop
            for(int b = 0; b < incident_segments.size(); b++)
            {
                incident_segment = incident_segments.get(b);
                incident_segment_start = incident_segment[0];
                incident_segment_end = incident_segment[1];  
                //does the pending start in this segment?
                if((pending_segment_start >= incident_segment_start) && (pending_segment_start <= incident_segment_end))
                {
                    //yes starts in this segment?
                    start_segment_index = b;                    
                }
                if((pending_segment_end >= incident_segment_start) && (pending_segment_end <= incident_segment_end))
                {
                    //yes ends in this segment?
                    end_segment_index = b;
                }
            }//end incident_segments.size loop
            long new_segment1[] = new long[2];
            long new_segment2[] = new long[2];
            if(end_segment_index == start_segment_index)
            {
                //start and end in the same segement, so split it in two if the incident_segment_end > pending_segment_end
                //loop thru the incident_segments and create the new_incident_segments
                for(int x = 0; x < incident_segments.size(); x++)
                {
                    incident_segment = incident_segments.get(x);
                    incident_segment_start = incident_segment[0];
                    incident_segment_end = incident_segment[1];
                    
                    if(x == start_segment_index) //this is the start that needs editted
                    {
                        new_segment1[0] = incident_segment_start;
                        new_segment1[1] = pending_segment_start;
                        new_segment2[0] = pending_segment_end;
                        new_segment2[1] = incident_segment_end;
                        new_incident_segments.add(new_segment1);
                        new_incident_segments.add(new_segment2);
                    }
                    else
                    {
                        //just rewrite it to the new_incident_segments 
                        new_segment1[0] = incident_segment_start;
                        new_segment1[1] = incident_segment_end;
                        new_incident_segments.add(new_segment1);
                    }
                }                
            }//end if pause is in same segment
            else
            {
                //end_segment_index != start_segment_index (start and end are different segments, so segment(s) will be skipped)
                for(int y = 0; y < incident_segments.size(); y++)
                {
                    incident_segment = incident_segments.get(y);
                    incident_segment_start = incident_segment[0];
                    incident_segment_end = incident_segment[1];
                    if(y == start_segment_index)
                    {
                        new_segment1[0] = incident_segment_start;
                        new_segment1[1] = pending_segment_start;
                        new_incident_segments.add(new_segment1);
                    }
                    else
                    {
                        if(y == end_segment_index)
                        {
                            new_segment1[0] = pending_segment_end;
                            new_segment1[1] = incident_segment_end;
                            new_incident_segments.add(new_segment1);
                        }
                    }
                    if(y < start_segment_index || y > end_segment_index)
                    {
                        //rewrite the segment
                        new_segment1[0] = incident_segment_start;
                        new_segment1[1] = incident_segment_end;
                        new_incident_segments.add(new_segment1);
                    }
                }
            }            
        }
        
        
        return return_value;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    public static double for_sla_id(Connection con, String incident_sla_id,  java.util.Date start, java.util.Date end) throws Exception, SQLException
    {
        //System.out.println("starting sla_contact_calc");
        double return_value = 0;
        String sla_type = "";
        String priority =  "";
        String start_state = "";
        String end_state = "";
        //System.out.println("incident_sla_id=" + incident_sla_id);
        ArrayList incidents = new <String[]> ArrayList();
        String incident_history_query = "";
        //get sla definition
        String sla_info[] = db.get_sla.incident_by_id(con, incident_sla_id);
        sla_type = sla_info[3];
        priority = sla_info[5];
        PreparedStatement p_stmt;
        Statement stmt = con.createStatement();    
        
        //get the start state
        start_state = sla_info[11];        
        //get the end state
        end_state = sla_info[13];
        
        //get the channel from the JSON
        String json_additional_parameters = sla_info[30];
        //parse the json and put it in a arraylist
        try
        {
            //start the query build
            incident_history_query = "SELECT DISTINCT(incident_id),state_date FROM incident_history WHERE priority = '" + priority + "' AND state = '"+ end_state +"' AND (state_date >= '" + timestamp.format(start) + "' AND state_date <= '" + timestamp.format(end) + "')"; // AND (contact_method = 'Chat' OR contact_method = 'Phone' OR contact_method = 'Walk in')";
            
            //System.out.println("json_additional_parameters=" + json_additional_parameters);
            JsonReader reader = Json.createReader(new StringReader(json_additional_parameters));
            JsonObject resultObject = reader.readObject();
            
            JsonArray parameters = (JsonArray) resultObject.getJsonArray("parameters");
            //System.out.println("parameters length=" + parameters.size());
            for(int a = 0; a < parameters.size(); a++)
            {
                JsonObject this_record = parameters.getJsonObject(a); //a single jsonrecord
                //System.out.println("json=" + this_record);
                String field_name = this_record.getJsonString("field_name").toString().replace("\"","");
                String operator = this_record.getJsonString("operator").toString().replace("\"","");
                JsonArray values = (JsonArray) this_record.getJsonArray("value");
                String field_query = "";
                for(int b = 0; b < values.size(); b++)
                {
                    String op = "";
                    if(operator.equals("equal")){op = "=";}else{op = "!=";}
                    //AND (contact_method = 'Chat' OR contact_method = 'Phone' OR contact_method = 'Walk in')";
                    if(b == 0)
                    {
                        field_query = field_name + " " + op + " '" + values.getString(b) + "'";
                    }
                    else
                    {
                        field_query = field_query + " OR " + field_name + " " + op + " '" + values.getString(b) + "'";
                    }
                }
                incident_history_query = incident_history_query + " AND (" + field_query + ")";                
            }
            //load an arraylist with  incident_id, start_date, stop_date
            String ps_query_string = "SELECT state_date FROM incident_history WHERE incident_id=? AND state=?";
            p_stmt = con.prepareStatement(ps_query_string);
            
            //System.out.println("incident sla q=" + incident_history_query);
            //get incidents that sla applies to. and their stop dates
            ResultSet rs = stmt.executeQuery(incident_history_query);
            while(rs.next())
            {
                String temp[] = new String[3];
                temp[0] = rs.getString("incident_id");
                temp[2] = timestamp.format(rs.getTimestamp("state_date"));
                p_stmt.setString(1, rs.getString("incident_id"));
                p_stmt.setString(2, start_state);
                //System.out.println("incident sla q2=" + p_stmt);
                ResultSet rs2 = p_stmt.executeQuery();
                while (rs2.next())
                {
                    temp[1] = timestamp.format(rs2.getTimestamp("state_date"));
                }
                incidents.add(temp);
                //System.out.println("inc="+ temp[0] + " start=" + temp[1] + " end=" + temp[2]);
            }
        }
        catch(Exception e)
        {
            logger.error("ERROR sla_incident_calc.for_sla_id JSON Parse:=" + e);
        }
              
        //do the calculation
        //{"TIME"};
        switch(sla_type) 
        { 
            case "TIME": 
                return_value = calc_time(sla_info,incidents);
                //System.out.println("TIME incident sla"); 
                break; 
            default: 
                //System.out.println("no match"); 
        }         
        return return_value;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static double calc_time(String sla_info[], ArrayList <String[]>incidents) throws Exception //arraylist {incident_id, start_date, stop_date}
    {
        double return_double = 0;
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        int total_incidents = 0;
        int total_incidents_meeting_sla = 0;
        int total_incidents_failing_sla = 0;
        java.util.Date start = new java.util.Date();
        java.util.Date end = new java.util.Date();
        
        try
        {
            target = Double.parseDouble(sla_info[6]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_incident_calc.calc_time target definition:=" + e);
        }
        sla_operator = sla_info[21];
        try
        {
            sla_value = Double.parseDouble(sla_info[22]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_incident_calc.calc_time sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[23];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        //for each incident        
            //get the diff between "start date" and "end date"            
            //use operator to determine good or bad
            //increment good/bad counts
        //end for
        try
        {
            //System.out.println("incidents.size()=" + incidents.size());
            for(int a = 0; a < incidents.size(); a++)
            {
                String incident[] = incidents.get(a);
                start = timestamp.parse(incident[1]);
                end = timestamp.parse(incident[2]);
                
                long diff = ((end.getTime() - start.getTime()) / 1000);
                //System.out.println("diff=" + diff   + "  sla_value=" + sla_value);
                total_incidents++;
                //{"greater_than", "greater_than_or_equal", "equal_to", "not_equal_to", "less_than_or_equal", "less_than"}
                switch(sla_operator) 
                { 
                    case "greater_than": 
                        if(diff > sla_value)
                        {
                            total_incidents_meeting_sla++;
                        }
                        else
                        {
                            total_incidents_failing_sla++;
                        }
                        break; 
                    case "greater_than_or_equal":
                        if(diff >= sla_value)
                        {
                            total_incidents_meeting_sla++;
                        }
                        else
                        {
                            total_incidents_failing_sla++;
                        }
                        break; 
                    case "equal_to": 
                        if(diff == sla_value)
                        {
                            total_incidents_meeting_sla++;
                        }
                        else
                        {
                            total_incidents_failing_sla++;
                        }
                        break; 
                    case "not_equal_to": 
                        if(diff != sla_value)
                        {
                            total_incidents_meeting_sla++;
                        }
                        else
                        {
                            total_incidents_failing_sla++;
                        }
                        break; 
                    case "less_than_or_equal": 
                        if(diff <= sla_value)
                        {
                            total_incidents_meeting_sla++;
                        }
                        else
                        {
                            total_incidents_failing_sla++;
                        }
                        break; 
                    case "less_than": 
                        if(diff < sla_value)
                        {
                            total_incidents_meeting_sla++;
                        }
                        else
                        {
                            total_incidents_failing_sla++;
                        }
                        break; 
                    default: 
                        //System.out.println("no match");     
                }
            } 
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_incident_calc.calc_time calc loop=" + e);
        }
        try
        {
            if(total_incidents == 0)
            {
                return_double = 100;
            }
            else
            {
                return_double = ((double)total_incidents_meeting_sla / (double)total_incidents ) * 100;
                //System.out.println("total_incidents_meeting_sla=" + total_incidents_meeting_sla  + " total_incidents=" + total_incidents + " =" + return_double);
            }
            
        }
        catch(Exception e)
        {
            logger.error("Exception in sla_incident_calc.calc_time final calc:=" + e);
        }
        //System.out.println("return_double=" + return_double);
        return return_double;
    }  
    public static ArrayList<ArrayList<String[]>> get_incidents_for_sla_id(
        Connection con, 
        String incident_sla_id,  
        java.util.Date start, 
        java.util.Date end
    ) throws Exception, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
//        System.out.println("starting sla_contact_calc " + incident_sla_id + " " + start + " " + end);
        ArrayList<ArrayList<String[]>> return_array_list = new ArrayList<ArrayList<String[]>>();
        ArrayList<String[]> return_value = new ArrayList();
        String sla_type = "";
        String priority =  "";
        String start_state = "";
        String end_state = "";
        ArrayList incidents = new <String[]> ArrayList();
        String incident_history_end_state_query = "";
        //get sla definition
        String sla_info[] = db.get_sla.incident_by_id(con, incident_sla_id);
        sla_type = sla_info[3];
        priority = sla_info[5];
        PreparedStatement p_stmt;
        Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);    
        ArrayList<String[]> users = get_users.all(con);        
        ArrayList<String[]> groups = db.get_groups.all(con);
        //get the start state
        start_state = sla_info[11];        
        //get the end state
        end_state = sla_info[13];
        
        //get the channel from the JSON
        String json_additional_parameters = sla_info[30];
        //parse the json and put it in a arraylist
        try
        {
            //start the query build
            incident_history_end_state_query = "SELECT DISTINCT(incident_id) FROM incident_history WHERE priority = '" + priority + "' AND state = '"+ end_state +"' AND (state_date >= '" + timestamp.format(start) + "' AND state_date <= '" + timestamp.format(end) + "')"; // AND (contact_method = 'Chat' OR contact_method = 'Phone' OR contact_method = 'Walk in')";
            
            //load an arraylist with  incident_id, start_date, stop_date
            String ps_query_string = "SELECT * FROM incident_history WHERE incident_id=? AND state=?";
            
            p_stmt = con.prepareStatement(ps_query_string);
            
            //System.out.println("json_additional_parameters=" + json_additional_parameters);
            JsonReader reader = Json.createReader(new StringReader(json_additional_parameters));
            JsonObject resultObject = reader.readObject();
            
            JsonArray parameters = (JsonArray) resultObject.getJsonArray("parameters");
            //System.out.println("parameters length=" + parameters.size());
            for(int a = 0; a < parameters.size(); a++)
            {
                JsonObject this_record = parameters.getJsonObject(a); //a single jsonrecord
                //System.out.println("json=" + this_record);
                String field_name = this_record.getJsonString("field_name").toString().replace("\"","");
                String operator = this_record.getJsonString("operator").toString().replace("\"","");
                JsonArray values = (JsonArray) this_record.getJsonArray("value");
                String field_query = "";
                for(int b = 0; b < values.size(); b++)
                {
                    String op = "";
                    if(operator.equals("equal")){op = "=";}else{op = "!=";}
                    //AND (contact_method = 'Chat' OR contact_method = 'Phone' OR contact_method = 'Walk in')";
                    if(b == 0)
                    {
                        field_query = field_name + " " + op + " '" + values.getString(b) + "'";
                    }
                    else
                    {
                        field_query = field_query + " OR " + field_name + " " + op + " '" + values.getString(b) + "'";
                    }
                }
                incident_history_end_state_query = incident_history_end_state_query + " AND (" + field_query + ")";                
            }
            //get all incidents that end state is in timeframe
//            System.out.println("get_incidents_for_sla_id query " + incident_history_end_state_query);
            ResultSet rs = stmt.executeQuery(incident_history_end_state_query); //end state
            String incident_id = "";
            while(rs.next())
            {
                String temp[] = new String[52]; //last element is start state date
                incident_id = rs.getString("incident_id");  
                
                //load the arraylist with the end state history record
                p_stmt.setString(1, incident_id);
                p_stmt.setString(2, end_state);
                ResultSet rs2 = p_stmt.executeQuery();
                while (rs2.next())
                {
                    //last element is start state date
                    temp[0] = rs2.getString("id");
                    try
                    {
                        temp[1] = timestamp_format.format(rs2.getTimestamp("change_time"));
                    }
                    catch(Exception e)
                    {
                        temp[1] = "";
                    }
                    temp[2] = rs2.getString("incident_id");
                    temp[3] = rs2.getString("incident_type");
                    try
                    {
                        temp[4] = timestamp_format.format(rs2.getTimestamp("incident_time"));
                    }
                    catch(Exception e)
                    {
                        temp[4] = "";
                    }
                    temp[5] = rs2.getString("caller_id");
                    temp[6] = rs2.getString("caller_group_id");                
                    temp[7] = rs2.getString("location");
                    temp[8] = rs2.getString("department");
                    temp[9] = rs2.getString("site");
                    temp[10] = rs2.getString("company");

                    temp[11] = rs2.getString("category");
                    temp[12] = rs2.getString("subcategory");
                    temp[13] = rs2.getString("ci_id");
                    temp[14] = rs2.getString("impact");
                    temp[15] = rs2.getString("urgency");
                    temp[16] = rs2.getString("priority");
                    temp[17] = rs2.getString("description");
                    try
                    {
                        temp[18] = timestamp_format.format(rs2.getTimestamp("create_date"));
                    }
                    catch(Exception e)
                    {
                        temp[18] = "";
                    }

                    temp[19] = rs2.getString("create_by_id");
                    temp[20] = rs2.getString("contact_method");
                    temp[21] = rs2.getString("state");
                    try
                    {
                        temp[22] = timestamp_format.format(rs2.getTimestamp("state_date"));
                    }
                    catch(Exception e)
                    {
                        temp[22] = "";
                    }
                    temp[23] = rs2.getString("assigned_group_id");
                    temp[24] = rs2.getString("assigned_to_id");
                    temp[25] = rs2.getString("notes");
                    temp[26] = rs2.getString("desk_notes");
                    temp[27] = rs2.getString("related");
                    temp[28] = rs2.getString("first_contact_resolution");
                    try
                    {
                        temp[29] = timestamp_format.format(rs2.getTimestamp("closed_date"));
                    }
                    catch(Exception e)
                    {
                        temp[29] = "";
                    }
                    temp[30] = rs2.getString("closed_reason");
                    try
                    {
                        temp[31] = timestamp_format.format(rs2.getTimestamp("pending_date"));
                    }
                    catch(Exception e)
                    {
                        temp[31] = "";
                    }
                    temp[32] = rs2.getString("pending_reason");
                    temp[33] = rs2.getString("external_id");


                    //LOOKUP DATA populate the user info
                    temp[34] = "";
                    temp[35] = "";
                    temp[36] = "";
                    temp[37] = "";
                    temp[38] = "";
                    for(int a = 0; a < users.size(); a++)
                    {
                        String this_user[] = users.get(a);
                        if(this_user[0].equalsIgnoreCase(rs2.getString("caller_id")))
                        {
                            temp[34] = this_user[1];
                            temp[35] = this_user[3];
                            temp[36] = this_user[4];
                            temp[37] = this_user[5];
                            temp[38] = this_user[17];
                            a = users.size();
                        }
                    }
                    temp[39] = "";
                    for(int b = 0; b < groups.size(); b++)
                    {
                        String this_group[] = groups.get(b);
                        if(this_group[0].equalsIgnoreCase(rs2.getString("caller_group_id")))
                        {
                            temp[39] = this_group[1];
                            b = groups.size();
                        }
                    }
                    temp[40] = "";
                    for(int b = 0; b < groups.size(); b++)
                    {
                        String this_group[] = groups.get(b);
                        if(this_group[0].equalsIgnoreCase(rs2.getString("assigned_group_id")))
                        {
                            temp[40] = this_group[1];
                            b = groups.size();
                        }
                    }
                    temp[41] = "";
                    temp[42] = "";
                    temp[43] = "";
                    temp[44] = "";
                    temp[45] = "";
                    for(int c = 0; c < users.size(); c++)
                    {
                        String this_user[] = users.get(c);
                        if(this_user[0].equalsIgnoreCase(rs2.getString("assigned_to_id")))
                        {
                            temp[41] = this_user[1];
                            temp[42] = this_user[3];
                            temp[43] = this_user[4];
                            temp[44] = this_user[5];
                            temp[45] = this_user[17];
                            c = users.size();
                        }
                    }
                    temp[46] = "";
                    temp[47] = "";
                    temp[48] = "";
                    temp[49] = "";
                    temp[50] = "";
                    for(int d = 0; d < users.size(); d++)
                    {
                        String this_user[] = users.get(d);
                        if(this_user[0].equalsIgnoreCase(rs2.getString("create_by_id")))
                        {
                            temp[46] = this_user[1];
                            temp[47] = this_user[3];
                            temp[48] = this_user[4];
                            temp[49] = this_user[5];
                            temp[50] = this_user[17];
                            d = users.size();
                        }
                    }
                }
                p_stmt.setString(1, incident_id);
                p_stmt.setString(2, start_state);
                ResultSet rs_start_date = p_stmt.executeQuery();
                while (rs_start_date.next())
                {
                    temp[51] = timestamp.format(rs_start_date.getTimestamp("state_date")); //start_date
                }
                return_value.add(temp);
            }
        }
        catch(Exception e)
        {
            logger.error("ERROR sla_incident_calc.get_noncompliant_incidents_for_sla_id JSON Parse:=" + e);
        }
              
        //do the calculation
        //{"TIME"};
        logger.info("get_incidents_for_sla_id sla_type {} compliance {}", sla_type);
        switch(sla_type) 
        { 
            case "TIME": 
                return_array_list.add(get_compliant_incidents_calc_time(sla_info,return_value));
                return_array_list.add(get_noncompliant_incidents_calc_time(sla_info,return_value));
                //System.out.println("TIME incident sla"); 
                break; 
            default: 
                //System.out.println("no match"); 
        }         
        return return_array_list;
    }
    public static ArrayList<String[]> get_noncompliant_incidents_for_sla_id(Connection con, String incident_sla_id,  java.util.Date start, java.util.Date end) throws Exception, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        System.out.println("starting sla_contact_calc " + incident_sla_id + " " + start + " " + end);
        ArrayList<String[]> return_value = new ArrayList();
        String sla_type = "";
        String priority =  "";
        String start_state = "";
        String end_state = "";
        ArrayList incidents = new <String[]> ArrayList();
        String incident_history_end_state_query = "";
        //get sla definition
        String sla_info[] = db.get_sla.incident_by_id(con, incident_sla_id);
        sla_type = sla_info[3];
        priority = sla_info[5];
        PreparedStatement p_stmt;
        Statement stmt = con.createStatement();    
        ArrayList<String[]> users = get_users.all(con);        
        ArrayList<String[]> groups = db.get_groups.all(con);
        //get the start state
        start_state = sla_info[11];        
        //get the end state
        end_state = sla_info[13];
        
        //get the channel from the JSON
        String json_additional_parameters = sla_info[30];
        //parse the json and put it in a arraylist
        try
        {
            //start the query build
            incident_history_end_state_query = "SELECT DISTINCT(incident_id) FROM incident_history WHERE priority = '" + priority + "' AND state = '"+ end_state +"' AND (state_date >= '" + timestamp.format(start) + "' AND state_date <= '" + timestamp.format(end) + "')"; // AND (contact_method = 'Chat' OR contact_method = 'Phone' OR contact_method = 'Walk in')";
            
            //load an arraylist with  incident_id, start_date, stop_date
            String ps_query_string = "SELECT * FROM incident_history WHERE incident_id=? AND state=?";
            p_stmt = con.prepareStatement(ps_query_string);
            
            //System.out.println("json_additional_parameters=" + json_additional_parameters);
            JsonReader reader = Json.createReader(new StringReader(json_additional_parameters));
            JsonObject resultObject = reader.readObject();
            
            JsonArray parameters = (JsonArray) resultObject.getJsonArray("parameters");
            //System.out.println("parameters length=" + parameters.size());
            for(int a = 0; a < parameters.size(); a++)
            {
                JsonObject this_record = parameters.getJsonObject(a); //a single jsonrecord
                //System.out.println("json=" + this_record);
                String field_name = this_record.getJsonString("field_name").toString().replace("\"","");
                String operator = this_record.getJsonString("operator").toString().replace("\"","");
                JsonArray values = (JsonArray) this_record.getJsonArray("value");
                String field_query = "";
                for(int b = 0; b < values.size(); b++)
                {
                    String op = "";
                    if(operator.equals("equal")){op = "=";}else{op = "!=";}
                    //AND (contact_method = 'Chat' OR contact_method = 'Phone' OR contact_method = 'Walk in')";
                    if(b == 0)
                    {
                        field_query = field_name + " " + op + " '" + values.getString(b) + "'";
                    }
                    else
                    {
                        field_query = field_query + " OR " + field_name + " " + op + " '" + values.getString(b) + "'";
                    }
                }
                incident_history_end_state_query = incident_history_end_state_query + " AND (" + field_query + ")";                
            }
            //get all incidents that end state is in timeframe
            ResultSet rs = stmt.executeQuery(incident_history_end_state_query); //end state
            String incident_id = "";
            while(rs.next())
            {
                String temp[] = new String[52]; //last element is start state date
                incident_id = rs.getString("incident_id");  
                
                //load the arraylist with the end state history record
                p_stmt.setString(1, incident_id);
                p_stmt.setString(2, end_state);
                ResultSet rs2 = p_stmt.executeQuery();
                while (rs2.next())
                {
                    //last element is start state date
                    temp[0] = rs2.getString("id");
                    try
                    {
                        temp[1] = timestamp_format.format(rs2.getTimestamp("change_time"));
                    }
                    catch(Exception e)
                    {
                        temp[1] = "";
                    }
                    temp[2] = rs2.getString("incident_id");
                    temp[3] = rs2.getString("incident_type");
                    try
                    {
                        temp[4] = timestamp_format.format(rs2.getTimestamp("incident_time"));
                    }
                    catch(Exception e)
                    {
                        temp[4] = "";
                    }
                    temp[5] = rs2.getString("caller_id");
                    temp[6] = rs2.getString("caller_group_id");                
                    temp[7] = rs2.getString("location");
                    temp[8] = rs2.getString("department");
                    temp[9] = rs2.getString("site");
                    temp[10] = rs2.getString("company");

                    temp[11] = rs2.getString("category");
                    temp[12] = rs2.getString("subcategory");
                    temp[13] = rs2.getString("ci_id");
                    temp[14] = rs2.getString("impact");
                    temp[15] = rs2.getString("urgency");
                    temp[16] = rs2.getString("priority");
                    temp[17] = rs2.getString("description");
                    try
                    {
                        temp[18] = timestamp_format.format(rs2.getTimestamp("create_date"));
                    }
                    catch(Exception e)
                    {
                        temp[18] = "";
                    }

                    temp[19] = rs2.getString("create_by_id");
                    temp[20] = rs2.getString("contact_method");
                    temp[21] = rs2.getString("state");
                    try
                    {
                        temp[22] = timestamp_format.format(rs2.getTimestamp("state_date"));
                    }
                    catch(Exception e)
                    {
                        temp[22] = "";
                    }
                    temp[23] = rs2.getString("assigned_group_id");
                    temp[24] = rs2.getString("assigned_to_id");
                    temp[25] = rs2.getString("notes");
                    temp[26] = rs2.getString("desk_notes");
                    temp[27] = rs2.getString("related");
                    temp[28] = rs2.getString("first_contact_resolution");
                    try
                    {
                        temp[29] = timestamp_format.format(rs2.getTimestamp("closed_date"));
                    }
                    catch(Exception e)
                    {
                        temp[29] = "";
                    }
                    temp[30] = rs2.getString("closed_reason");
                    try
                    {
                        temp[31] = timestamp_format.format(rs2.getTimestamp("pending_date"));
                    }
                    catch(Exception e)
                    {
                        temp[31] = "";
                    }
                    temp[32] = rs2.getString("pending_reason");
                    temp[33] = rs2.getString("external_id");


                    //LOOKUP DATA populate the user info
                    temp[34] = "";
                    temp[35] = "";
                    temp[36] = "";
                    temp[37] = "";
                    temp[38] = "";
                    for(int a = 0; a < users.size(); a++)
                    {
                        String this_user[] = users.get(a);
                        if(this_user[0].equalsIgnoreCase(rs2.getString("caller_id")))
                        {
                            temp[34] = this_user[1];
                            temp[35] = this_user[3];
                            temp[36] = this_user[4];
                            temp[37] = this_user[5];
                            temp[38] = this_user[17];
                            a = users.size();
                        }
                    }
                    temp[39] = "";
                    for(int b = 0; b < groups.size(); b++)
                    {
                        String this_group[] = groups.get(b);
                        if(this_group[0].equalsIgnoreCase(rs2.getString("caller_group_id")))
                        {
                            temp[39] = this_group[1];
                            b = groups.size();
                        }
                    }
                    temp[40] = "";
                    for(int b = 0; b < groups.size(); b++)
                    {
                        String this_group[] = groups.get(b);
                        if(this_group[0].equalsIgnoreCase(rs2.getString("assigned_group_id")))
                        {
                            temp[40] = this_group[1];
                            b = groups.size();
                        }
                    }
                    temp[41] = "";
                    temp[42] = "";
                    temp[43] = "";
                    temp[44] = "";
                    temp[45] = "";
                    for(int c = 0; c < users.size(); c++)
                    {
                        String this_user[] = users.get(c);
                        if(this_user[0].equalsIgnoreCase(rs2.getString("assigned_to_id")))
                        {
                            temp[41] = this_user[1];
                            temp[42] = this_user[3];
                            temp[43] = this_user[4];
                            temp[44] = this_user[5];
                            temp[45] = this_user[17];
                            c = users.size();
                        }
                    }
                    temp[46] = "";
                    temp[47] = "";
                    temp[48] = "";
                    temp[49] = "";
                    temp[50] = "";
                    for(int d = 0; d < users.size(); d++)
                    {
                        String this_user[] = users.get(d);
                        if(this_user[0].equalsIgnoreCase(rs2.getString("create_by_id")))
                        {
                            temp[46] = this_user[1];
                            temp[47] = this_user[3];
                            temp[48] = this_user[4];
                            temp[49] = this_user[5];
                            temp[50] = this_user[17];
                            d = users.size();
                        }
                    }
                }
                p_stmt.setString(1, incident_id);
                p_stmt.setString(2, start_state);
                ResultSet rs_start_date = p_stmt.executeQuery();
                while (rs_start_date.next())
                {
                    temp[51] = timestamp.format(rs_start_date.getTimestamp("state_date")); //start_date
                }
                return_value.add(temp);
            }
        }
        catch(Exception e)
        {
            logger.error("ERROR sla_incident_calc.get_noncompliant_incidents_for_sla_id JSON Parse:=" + e);
        }
              
        //do the calculation
        //{"TIME"};
        switch(sla_type) 
        { 
            case "TIME": 
                return_value = get_noncompliant_incidents_calc_time(sla_info,return_value);
                //System.out.println("TIME incident sla"); 
                break; 
            default: 
                //System.out.println("no match"); 
        }         
        return return_value;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static ArrayList<String[]> get_noncompliant_incidents_calc_time(String sla_info[], ArrayList <String[]>incidents) throws Exception //arraylist {incident_id, start_date, stop_date}
    {
        ArrayList <String[]> return_values = new ArrayList();
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        java.util.Date start = new java.util.Date();
        java.util.Date end = new java.util.Date();
        
        try
        {
            target = Double.parseDouble(sla_info[6]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_incident_calc.get_noncompliant_incidents_calc_time target definition:=" + e);
        }
        sla_operator = sla_info[21];
        try
        {
            sla_value = Double.parseDouble(sla_info[22]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_incident_calc.get_noncompliant_incidents_calc_time sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[23];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        //for each incident        
            //get the diff between "start date" and "end date"            
            //use operator to determine good or bad
            //increment good/bad counts
        //end for
        try
        {
            //System.out.println("incidents.size()=" + incidents.size());
            for(int a = 0; a < incidents.size(); a++)
            {
                String incident[] = incidents.get(a);
                start = timestamp.parse(incident[51]);
                end = timestamp.parse(incident[22]);
                //System.out.println("Start=" + timestamp.format(start) + " End=" + timestamp.format(end));
                
                
                
                long diff = ((end.getTime() - start.getTime()) / 1000);
                //System.out.println("diff=" + diff   + "  sla_value=" + sla_value);
                //{"greater_than", "greater_than_or_equal", "equal_to", "not_equal_to", "less_than_or_equal", "less_than"}
                switch(sla_operator) 
                { 
                    case "greater_than": 
                        if(diff > sla_value)
                        {
                            //do nothing
                        }
                        else
                        {
                            return_values.add(incident);
                        }
                        break; 
                    case "greater_than_or_equal":
                        if(diff >= sla_value)
                        {
                            //do nothing
                        }
                        else
                        {
                            return_values.add(incident);
                        }
                        break; 
                    case "equal_to": 
                        if(diff == sla_value)
                        {
                            //do nothing
                        }
                        else
                        {
                            return_values.add(incident);
                        }
                        break; 
                    case "not_equal_to": 
                        if(diff != sla_value)
                        {
                            //do nothing
                        }
                        else
                        {
                            return_values.add(incident);
                        }
                        break; 
                    case "less_than_or_equal": 
                        if(diff <= sla_value)
                        {
                            //do nothing
                        }
                        else
                        {
                            return_values.add(incident);
                        }
                        break; 
                    case "less_than": 
                        if(diff < sla_value)
                        {
                            //do nothing
                        }
                        else
                        {
                            return_values.add(incident);
                        }
                        break; 
                    default: 
                        //System.out.println("no match");     
                }
            } 
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_incident_calc.get_noncompliant_incidents_calc_time calc loop=" + e);
        }
        
        //System.out.println("return_values=" + return_values.size());
        return return_values;
    }      
    public static ArrayList<String[]> get_compliant_incidents_calc_time(String sla_info[], ArrayList <String[]>incidents) throws Exception //arraylist {incident_id, start_date, stop_date}
    {
        ArrayList <String[]> return_values = new ArrayList();
        double target = 0;
        String sla_operator = "";
        double sla_value = 0;
        String sla_unit = "";
        java.util.Date start = new java.util.Date();
        java.util.Date end = new java.util.Date();
        
        try
        {
            target = Double.parseDouble(sla_info[6]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_incident_calc.get_noncompliant_incidents_calc_time target definition:=" + e);
        }
        sla_operator = sla_info[21];
        try
        {
            sla_value = Double.parseDouble(sla_info[22]);
        }
        catch(NumberFormatException e)
        {
            logger.error("Exception in sla_incident_calc.get_noncompliant_incidents_calc_time sla_success_threshold_value definition:=" + e);
        }
        sla_unit = sla_info[23];
        //convert sla_value into seconds //{"seconds", "minutes", "hours", "days"};
        if(sla_unit.equalsIgnoreCase("seconds"))
        {
            sla_value = sla_value; //yep I did that
        }
        else if(sla_unit.equalsIgnoreCase("minutes"))
        {
            sla_value = sla_value * 60;
        } 
        else if(sla_unit.equalsIgnoreCase("hours"))
        {
            sla_value = sla_value * 60 *60;
        } 
        else if(sla_unit.equalsIgnoreCase("days"))
        {
            sla_value = sla_value * 24 * 60 * 60;
        } 
        //for each incident        
            //get the diff between "start date" and "end date"            
            //use operator to determine good or bad
            //increment good/bad counts
        //end for
        try
        {
            //System.out.println("incidents.size()=" + incidents.size());
            for(int a = 0; a < incidents.size(); a++)
            {
                String incident[] = incidents.get(a);
                start = timestamp.parse(incident[51]);
                end = timestamp.parse(incident[22]);
                //System.out.println("Start=" + timestamp.format(start) + " End=" + timestamp.format(end));
                
                
                
                long diff = ((end.getTime() - start.getTime()) / 1000);
                //System.out.println("diff=" + diff   + "  sla_value=" + sla_value);
                //{"greater_than", "greater_than_or_equal", "equal_to", "not_equal_to", "less_than_or_equal", "less_than"}
                switch(sla_operator) 
                { 
                    case "greater_than": 
                        if(diff > sla_value)
                        {
                            return_values.add(incident);
                        }
                        else
                        {
                            //do nothing
                        }
                        break; 
                    case "greater_than_or_equal":
                        if(diff >= sla_value)
                        {
                            return_values.add(incident);
                        }
                        else
                        {
                            //do nothing
                        }
                        break; 
                    case "equal_to": 
                        if(diff == sla_value)
                        {
                            return_values.add(incident);
                        }
                        else
                        {
                            //do nothing
                        }
                        break; 
                    case "not_equal_to": 
                        if(diff != sla_value)
                        {
                            return_values.add(incident);
                        }
                        else
                        {
                            //do nothing
                        }
                        break; 
                    case "less_than_or_equal": 
                        if(diff <= sla_value)
                        {
                            return_values.add(incident);
                        }
                        else
                        {
                            //do nothing
                        }
                        break; 
                    case "less_than": 
                        if(diff < sla_value)
                        {
                            return_values.add(incident);
                        }
                        else
                        {
                            //do nothing
                        }
                        break; 
                    default: 
                        //System.out.println("no match");     
                }
            } 
        }
        catch(Exception e)
        {
            logger.error("Exception in  sla_incident_calc.get_noncompliant_incidents_calc_time calc loop=" + e);
        }
        
        //System.out.println("return_values=" + return_values.size());
        return return_values;
    }      
}

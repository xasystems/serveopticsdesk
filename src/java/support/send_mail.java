/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package support;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class send_mail 
{
    private static Logger logger = LogManager.getLogger();
    
    public static boolean is_valid(String email_address)
    {
        String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        Boolean b = email_address.matches(EMAIL_REGEX);
        return b;
    }
    
    public static HashMap now(Connection con, String context_dir, String send_email_address, String send_subject, String send_body)
    {
        HashMap return_hashmap = new HashMap();
        String status = "fail";
        String result = "";
        //get config
        try
        {     
            //System.out.println("sending email to:" + send_email_address);
            //System.out.println("sending email sub:" + send_subject);
            //System.out.println("sending email body:" + send_body);
            
            String send_email_host = db.get_configuration_properties.by_name(con, "send_email_host").trim();
            //System.out.println("email_host=" + email_host);
            //options SSL,TLS,PLAIN
            String send_email_encryption = db.get_configuration_properties.by_name(con, "send_email_encryption").trim();
            String send_email_smtp_port = db.get_configuration_properties.by_name(con, "send_email_smtp_port").trim();
            String send_email_user = db.get_configuration_properties.by_name(con, "send_email_user").trim();
            String send_email_password = db.get_configuration_properties.by_name(con, "send_email_password").trim();   
            send_email_password = support.encrypt_utils.decrypt(context_dir, send_email_password);
            if(send_email_encryption.equalsIgnoreCase("SSL"))
            {
                result = send_ssl(send_email_host, send_email_smtp_port, send_email_user, send_email_password, send_email_address, send_subject, send_body);
                if(result.equalsIgnoreCase("Message sent successfully..."))
                {
                    status = "success";
                }
                else
                {
                    status = "fail";
                }
                return_hashmap.put("status", status);
                return_hashmap.put("result", result);
            }
            else
            {
                if(send_email_encryption.equalsIgnoreCase("TLS"))
                {
                    result = send_tls(send_email_host,send_email_smtp_port, send_email_user, send_email_password, send_email_address, send_subject, send_body);
                    if(result.equalsIgnoreCase("Message sent successfully..."))
                    {
                        status = "success";
                    }
                    else
                    {
                        status = "fail";
                    }
                    return_hashmap.put("status", status);
                    return_hashmap.put("result", result);
                }
                else
                {
                    if(send_email_encryption.equalsIgnoreCase("PLAIN"))
                    {
                        result = send_plain(send_email_host, send_email_smtp_port, send_email_user, send_email_password, send_email_address, send_subject, send_body);
                        if(result.equalsIgnoreCase("Message sent successfully..."))
                        {
                            status = "success";
                        }
                        else
                        {
                            status = "fail";
                        }
                        return_hashmap.put("status", status);
                        return_hashmap.put("result", result);
                    }
                }
            }            
        }
        catch(IOException | SQLException e)
        {
            logger.error("ERROR Exception send_email.now:=" + e);
            result = "Exception in the email configuration:=" + e.toString();
        }               
        return return_hashmap;
    }   
    /////////////////////////////////////////////////////////////////////////////
    public static String send_ssl(String mail_host, String email_smtp_port, String from_user_name, String from_user_password, String to_user, String message_subject, String message_text)
    {
        final String user = from_user_name;
        final String password = from_user_password;
        String results = "";
        Properties props = new Properties();  
        props.put("mail.smtp.host", mail_host);  
        props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");  
        props.put("mail.smtp.auth", "true"); 
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtps.starttls.enable","true");
        props.put("mail.smtp.port", email_smtp_port);  
        
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() 
            {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() 
                {
                    return new PasswordAuthentication(user,password);
                }
            }
        );
        try 
        {
            MimeMessage message = new MimeMessage(session);
            
            message.setFrom(new InternetAddress(user));
            message.addRecipient(Message.RecipientType.TO,new InternetAddress(to_user));
            message.setSubject(message_subject);
            //message.setText(message_text);
            message.setContent(message_text, "text/html; charset=utf-8");
            //send the message
            Transport.send(message);
            results = "Message sent successfully...";
            
        } 
        catch (Exception e) 
        {
            System.out.println("send_ssl exception=" + e);
            e.printStackTrace();
            results = e.toString();
        }        
        return results;
    }
    /////////////////////////////////////////////////////////////////////////////
    public static String send_tls(String mail_host, String email_smtp_port, String from_user_name, String from_user_password, String to_user, String message_subject, String message_text)
    {
        //System.out.println("start send_tls");
        final String user = from_user_name;
        final String password = from_user_password;
        String results = "";
        
        Properties props = new Properties();
        props.put("mail.smtp.host", mail_host); 
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.port", email_smtp_port);
        props.put("mail.smtps.starttls.enable","true");
        props.put("mail.smtp.ssl.trust", "*");
        
        Session session; 
        session = Session.getInstance(props, new javax.mail.Authenticator() 
        {
            @Override
            protected PasswordAuthentication getPasswordAuthentication()
            {
                return new PasswordAuthentication(user,password);
            }
        }
        );
        try 
        {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(user));
            message.addRecipient(Message.RecipientType.TO,new InternetAddress(to_user));
            message.setSubject(message_subject);
            //message.setText(message_text);
            message.setContent(message_text, "text/html; charset=utf-8");

            //send the message
            Transport.send(message);
            results = "Message sent successfully...";
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
            results = e.toString();
        }        
        return results;
    }
    /////////////////////////////////////////////////////////////////////////////
    public static String send_plain(String mail_host, String email_smtp_port, String from_user_name, String from_user_password, String to_user, String message_subject, String message_text)
    {
        String results = "";        
        
        Properties properties = System.getProperties(); 
        properties.setProperty("mail.smtp.host", mail_host); 
        Session session = Session.getDefaultInstance(properties); 
        // default session 
        try 
        { 
            MimeMessage message = new MimeMessage(session);
            // email message 
            message.setFrom(new InternetAddress(from_user_name)); 
            // setting header fields 
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to_user)); 
            message.setSubject(message_subject); 
            // subject line 
            // actual mail body 
            //message.setText(message_text);
            message.setContent(message_text, "text/html; charset=utf-8");
            // Send message 
            Transport.send(message); 
            results = "Email Sent successfully...."; 
        } 
        catch (MessagingException mex) 
        { 
            mex.printStackTrace(); 
            results = mex.toString();
        } 
        return results;
    }    
}


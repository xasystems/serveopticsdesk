/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package support;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.UUID;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class email 
{
    private static Logger logger = LogManager.getLogger();
    static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMssHHmmss");
    
    public static boolean is_valid(String email_address)
    {
        String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        Boolean b = email_address.matches(EMAIL_REGEX);
        return b;
    }    
    public static HashMap send(Connection con, String context_dir, String send_email_address, String send_subject, String send_body)
    {
        HashMap return_hashmap = new HashMap();
        String status = "fail";
        String result = "";
        //get config
        try
        {     
            //System.out.println("sending email to:" + send_email_address);
            //System.out.println("sending email sub:" + send_subject);
            //System.out.println("sending email body:" + send_body);
            
            String send_email_host = db.get_configuration_properties.by_name(con, "send_email_host").trim();
            //System.out.println("email_host=" + email_host);
            //options SSL,TLS,PLAIN
            String send_email_encryption = db.get_configuration_properties.by_name(con, "send_email_encryption").trim();
            String send_email_smtp_port = db.get_configuration_properties.by_name(con, "send_email_smtp_port").trim();
            String send_email_user = db.get_configuration_properties.by_name(con, "send_email_user").trim();
            String send_email_password = db.get_configuration_properties.by_name(con, "send_email_password").trim();   
            send_email_password = support.encrypt_utils.decrypt(context_dir, send_email_password);
            if(send_email_encryption.equalsIgnoreCase("SSL"))
            {
                result = send_ssl(send_email_host, send_email_smtp_port, send_email_user, send_email_password, send_email_address, send_subject, send_body);
                if(result.equalsIgnoreCase("Message sent successfully..."))
                {
                    status = "success";
                }
                else
                {
                    status = "fail";
                }
                return_hashmap.put("status", status);
                return_hashmap.put("result", result);
            }
            else
            {
                if(send_email_encryption.equalsIgnoreCase("TLS"))
                {
                    result = send_tls(send_email_host,send_email_smtp_port, send_email_user, send_email_password, send_email_address, send_subject, send_body);
                    if(result.equalsIgnoreCase("Message sent successfully..."))
                    {
                        status = "success";
                    }
                    else
                    {
                        status = "fail";
                    }
                    return_hashmap.put("status", status);
                    return_hashmap.put("result", result);
                }
                else
                {
                    if(send_email_encryption.equalsIgnoreCase("PLAIN"))
                    {
                        result = send_plain(send_email_host, send_email_smtp_port, send_email_user, send_email_password, send_email_address, send_subject, send_body);
                        if(result.equalsIgnoreCase("Message sent successfully..."))
                        {
                            status = "success";
                        }
                        else
                        {
                            status = "fail";
                        }
                        return_hashmap.put("status", status);
                        return_hashmap.put("result", result);
                    }
                }
            }            
        }
        catch(IOException | SQLException e)
        {
            logger.error("ERROR Exception send_email.now:=" + e);
            result = "Exception in the email configuration:=" + e.toString();
        }               
        return return_hashmap;
    }   
    /////////////////////////////////////////////////////////////////////////////
    public static String send_ssl(String mail_host, String email_smtp_port, String from_user_name, String from_user_password, String to_user, String message_subject, String message_text)
    {
        final String user = from_user_name;
        final String password = from_user_password;
        String results = "";
        Properties props = new Properties();  
        props.put("mail.smtp.host", mail_host);  
        props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");  
        props.put("mail.smtp.auth", "true"); 
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtps.starttls.enable","true");
        props.put("mail.smtp.port", email_smtp_port);  
        
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() 
            {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() 
                {
                    return new PasswordAuthentication(user,password);
                }
            }
        );
        try 
        {
            MimeMessage message = new MimeMessage(session);
            
            message.setFrom(new InternetAddress(user));
            message.addRecipient(Message.RecipientType.TO,new InternetAddress(to_user));
            message.setSubject(message_subject);
            //message.setText(message_text);
            message.setContent(message_text, "text/html; charset=utf-8");
            //send the message
            Transport.send(message);
            results = "Message sent successfully...";
            
        } 
        catch (Exception e) 
        {
            System.out.println("send_ssl exception=" + e);
            e.printStackTrace();
            results = e.toString();
        }        
        return results;
    }
    /////////////////////////////////////////////////////////////////////////////
    public static String send_tls(String mail_host, String email_smtp_port, String from_user_name, String from_user_password, String to_user, String message_subject, String message_text)
    {
        //System.out.println("start send_tls");
        final String user = from_user_name;
        final String password = from_user_password;
        String results = "";
        
        Properties props = new Properties();
        props.put("mail.smtp.host", mail_host); 
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.port", email_smtp_port);
        props.put("mail.smtps.starttls.enable","true");
        
        Session session; 
        session = Session.getDefaultInstance(props, new javax.mail.Authenticator() 
        {
            @Override
            protected PasswordAuthentication getPasswordAuthentication()
            {
                return new PasswordAuthentication(user,password);
            }
        }
        );
        try 
        {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(user));
            message.addRecipient(Message.RecipientType.TO,new InternetAddress(to_user));
            message.setSubject(message_subject);
            //message.setText(message_text);
            message.setContent(message_text, "text/html; charset=utf-8");

            //send the message
            Transport.send(message);
            results = "Message sent successfully...";
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
            results = e.toString();
        }        
        return results;
    }
    /////////////////////////////////////////////////////////////////////////////
    public static String send_plain(String mail_host, String email_smtp_port, String from_user_name, String from_user_password, String to_user, String message_subject, String message_text)
    {
        String results = "";        
        
        Properties properties = System.getProperties(); 
        properties.setProperty("mail.smtp.host", mail_host); 
        Session session = Session.getDefaultInstance(properties); 
        // default session 
        try 
        { 
            MimeMessage message = new MimeMessage(session);
            // email message 
            message.setFrom(new InternetAddress(from_user_name)); 
            // setting header fields 
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to_user)); 
            message.setSubject(message_subject); 
            // subject line 
            // actual mail body 
            //message.setText(message_text);
            message.setContent(message_text, "text/html; charset=utf-8");
            // Send message 
            Transport.send(message); 
            results = "Email Sent successfully...."; 
        } 
        catch (MessagingException mex) 
        { 
            mex.printStackTrace(); 
            results = mex.toString();
        } 
        return results;
    }   
    public static ArrayList<String[]> get_email_config_for_protocol(Connection con, String protocol)
    {
        ArrayList<String[]> parameters = new ArrayList();
        try
        {
            String query = "";
            if(protocol.equalsIgnoreCase("imap"))
            {
                protocol = "mail_imap_";
            }
            else if(protocol.equalsIgnoreCase("pop3"))
            {
                protocol = "mail_pop3_";
            }                
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM configuration_properties WHERE `name` LIKE'%" + protocol + "%'");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = new String[2];
                temp[0] = rs.getString("name");
                temp[1] = rs.getString("value");
                parameters.add(temp);
            }            
        }
        catch(Exception e)
        {
            System.out.println("Exception in email.get_email_config_for_protocol=" + e);
        }
        return parameters;
    }
    public static void process_email(UUID uuid, Part p) throws Exception 
    {
        File writter_file = new File("c:/temp/email/" + uuid + ".txt");
        // creates the file
        writter_file.createNewFile();
        // creates a FileWriter Object
        FileWriter writer = new FileWriter(writter_file,true); 
        writer.write("UUID: " + uuid + System.lineSeparator());



        System.out.println("---------------------------------");
                
        
        
        
        
        
        
        System.out.println("----------------------------");
        System.out.println("UUID=" + uuid + " EMAIL MESSAGE CONTENT-TYPE: " + p.getContentType());
        writer.write("EMAIL MESSAGE CONTENT-TYPE: " + p.getContentType() + System.lineSeparator());

        //check if the content is plain text
        if (p.isMimeType("text/plain")) 
        {
            System.out.println("This is plain text");
            System.out.println("---------------------------");
            System.out.println((String) p.getContent());
            writer.write("Message is plain text" + (String) p.getContent() + System.lineSeparator());
        }
        if (p.isMimeType("text/html")) 
        {
            System.out.println("This is HTML");
            System.out.println("---------------------------");
            System.out.println((String) p.getContent());
            writer.write("Message is HTML" + (String) p.getContent() + System.lineSeparator());
        } //check if the content has attachment
        else if (p.isMimeType("multipart/*")) 
        {
            // content may contain attachments
            Multipart multiPart = (Multipart) p.getContent();
            int numberOfParts = multiPart.getCount();
            writer.write("multipart/* number of parts: " + numberOfParts + " Actual mime type=" + p.getContentType() + System.lineSeparator());
            for (int partCount = 0; partCount < numberOfParts; partCount++) 
            {
                MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
                BodyPart bodyPart = multiPart.getBodyPart(partCount);
                
                String disposition = part.getDisposition();
                
                String file=part.getFileName();
                
                writer.write("multipart/* part number=" + partCount + " "  + part.getContentType() + System.lineSeparator());
                
                
                
                //External attachments
                if (disposition != null && Part.ATTACHMENT.equalsIgnoreCase(disposition)) 
                {
                    // this part is attachment
                    String fileName = uuid + "_"+ part.getFileName().replaceAll("[^a-zA-Z0-9\\._]+", "_"); //To make attachment name uniq we are adding current datatime before name.
                    part.saveFile(new File("c:/temp/email/Attachments/" + fileName));   //To save the attachment file at specific location.
                }
                //Inline Attachments
                else if (disposition != null && Part.INLINE.equalsIgnoreCase(disposition)) 
                {
                    // this part is attachment
                    String fileName = uuid + "_"+ part.getFileName().replaceAll("[^a-zA-Z0-9\\._]+", "_"); //To make attachment name uniq we are adding current datatime before name.
                    //  attachFiles += fileName + ","; //concrete all attachment's name with comma separated.                  
                    part.saveFile(new File("c:/temp/email/Inline/" + fileName));   //To save the attachment file at specific location.
                    //LOG.info("\n\t Path :- " +request.getSession().getServletContext().getRealPath("/WEB-INF/attechments/" + fileName));
                }
                //Inline icons and smileys
                else if(file != null && disposition==null)
                {
                    String fileName = uuid + "_"+ part.getFileName().replaceAll("[^a-zA-Z0-9\\._]+", "_");
                    //attachFiles += fileName + ","; //concrete all attachment's name with comma separated.
                    part.saveFile(new File("c:/temp/email/Icons/" + fileName));   //To save the attachment file at specific location.
                }
                else if(part.isMimeType("text/plain"))
                {
                    Object o = part.getContent();
                    writer.write("text/plain Message Content: " + (String) o + System.lineSeparator());
                }
                else if(part.isMimeType("text/html"))
                {
                    Object o = part.getContent();
                    writer.write("text/html Message Content: " + (String) o + System.lineSeparator());
                }
                
                else if (bodyPart.isMimeType("text/html")) 
                {
                    writer.write("text/html Message Content: " + (String) bodyPart.getContent() + System.lineSeparator());
                } 
                else if (bodyPart.isMimeType("text/plain")) 
                {
                    writer.write("text/plain Message Content: " + (String) bodyPart.getContent() + System.lineSeparator());
                }
            } 
            
        } //check if the content is a nested message
        else if (p.isMimeType("message/rfc822")) 
        {
            System.out.println("This is a Nested Message");
            System.out.println("---------------------------");
            process_email(uuid,(Part) p.getContent());
        } //check if the content is an inline image
        else if (p.isMimeType("image/jpeg")) 
        {
            System.out.println("--------> image/jpeg");
            int i = 0;
            Object o = p.getContent();

            InputStream x = (InputStream) o;
            byte[] bArray = new byte[x.available()];

            // Construct the required byte array
            System.out.println("x.length = " + x.available());
            while ((i = (int) ((InputStream) x).available()) > 0) 
            {
                int result = (int) (((InputStream) x).read(bArray));
                if (result == -1) {
                    break;
                }
            }
            FileOutputStream f2 = new FileOutputStream("c:/temp/email/" + uuid + "_image.jpg");
            f2.write(bArray);
        } 
        else if (p.getContentType().contains("image/")) 
        {
            System.out.println("content type" + p.getContentType());
            File f = new File("c:/temp/email/image" + uuid + ".jpg");
            DataOutputStream output = new DataOutputStream(
                    new BufferedOutputStream(new FileOutputStream(f)));
            com.sun.mail.util.BASE64DecoderStream test = (com.sun.mail.util.BASE64DecoderStream) p.getContent();
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = test.read(buffer)) != -1) 
            {
                output.write(buffer, 0, bytesRead);
            }
        } 
        else 
        {
            Object o = p.getContent();
            if (o instanceof String) 
            {
                System.out.println("This is a string");
                System.out.println("---------------------------");
                writer.write("Message Content: " + (String) o + System.lineSeparator());
                System.out.println((String) o);
            } 
            else if (o instanceof InputStream) 
            {
                System.out.println("This is just an input stream");
                System.out.println("---------------------------");
                writer.write("Message Stream: " + System.lineSeparator());
                InputStream is = (InputStream) o;
                is = (InputStream) o;
                int c;
                while ((c = is.read()) != -1) 
                {
                    System.out.write(c);
                    writer.write(c);
                }
            } 
            else 
            {
                System.out.println("This is an unknown type");
                System.out.println("---------------------------");
                writer.write("Message Content Unknown: " + System.lineSeparator());
                writer.write(o.toString());
                System.out.println(o.toString());
            }
        }
        // Writes the content to the file
        //writer.write("This\n is\n an\n example\n"); 
        writer.flush();
        writer.close();
    }
    public static ArrayList<String[]> get_all_for_test(Connection con, String context_dir)
    {
        boolean good_email_config = false;
        ArrayList<String[]> messages = new ArrayList();
        String get_email_host = "";
        String get_email_store_protocol = "";
        String get_email_user = "";
        String get_email_password = "";
        String date_sent = "";
        try
        {
            Properties properties = new Properties();
            get_email_host = db.get_configuration_properties.by_name(con, "get_email_host").trim();
            get_email_user = db.get_configuration_properties.by_name(con, "get_email_user").trim();
            get_email_password = db.get_configuration_properties.by_name(con, "get_email_password").trim();
            System.out.println("get_email_password=" + get_email_password);
            get_email_password = support.encrypt_utils.decrypt(context_dir, get_email_password);
            System.out.println("decrypted get_email_password=" + get_email_password);
            
            System.out.println("get_email_host=" + get_email_host + " get_email_user=" + get_email_user + " get_email_password=" + get_email_password);
            
            get_email_store_protocol = db.get_configuration_properties.by_name(con, "get_email_store_protocol").trim();
            System.out.println("get_email_store_protocol=" + get_email_store_protocol);
            
            if(get_email_store_protocol.equalsIgnoreCase("pop3"))
            {
                //get all the pop3 parameters
                properties.put("mail.store.protocol", "pop3");
                //properties.put("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
                //properties.put("mail.pop3.host", get_email_host);
                //properties.put("mail.pop3.port", "995");
                ArrayList<String[]> pop3_parameters = get_email_config_for_protocol(con,"pop3");
                for(int a = 0; a < pop3_parameters.size(); a++)
                {
                    String parameter[] = pop3_parameters.get(a);
                    System.out.println("name=" + parameter[0] + " value=" + parameter[1]);
                    properties.put(parameter[0], parameter[1]);
                }
            }
            else if(get_email_store_protocol.equalsIgnoreCase("imap"))
            {
                //get all the imap parameters
                //properties.put("mail.store.protocol", get_mail_store_protocol);
                ArrayList<String[]> imap_parameters = get_email_config_for_protocol(con,"imap");
                for(int a = 0; a < imap_parameters.size(); a++)
                {
                    String parameter[] = imap_parameters.get(a);
                    System.out.println("name=" + parameter[0] + " value=" + parameter[1]);
                    properties.put(parameter[0], parameter[1]);
                }
                
            }
            
            /*properties.put("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            properties.put("mail.imap.host", "outlook.office365.com");            
            properties.put("mail.imap.socketFactory.fallback", "true");
            properties.put("mail.imap.port", "993");
            properties.put("mail.imap.socketFactory.port", "993");
            properties.put("mail.imap.starttls.enable", "true");
            properties.put("mail.imap.ssl.enable", "true");*/
            
            System.out.println("get_mail_store_protocol=" + get_email_store_protocol);
            Session emailSession = Session.getDefaultInstance(properties);
            Store store = emailSession.getStore(get_email_store_protocol);
            store.connect(get_email_host, get_email_user, get_email_password);
            // create the folder object and open it
            Folder emailFolder = store.getFolder("INBOX");
            emailFolder.open(Folder.READ_ONLY);

            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

            // retrieve the messages from the folder in an array and print it
            Message[] emails = emailFolder.getMessages();
            System.out.println("emails.length---" + emails.length);
            for (int i = 0; i < emails.length; i++) 
            {
                String temp[] = {"","","",""}; //from, subject, to , date_sent
                UUID uuid = UUID.randomUUID(); //Generates random UUID  
                Message email = emails[i];   
                // FROM
                String from = "";
                String to = "";
                String subject = "";
                
                Address[] addresses;
                if ((addresses = email.getFrom()) != null) 
                {
                    for (int a = 0; a < addresses.length; a++) 
                    {
                        
                        from = from + " " + addresses[a].toString();
                    }
                }
                // TO
                if ((addresses = email.getRecipients(Message.RecipientType.TO)) != null) 
                {
                    for (int j = 0; j < addresses.length; j++) 
                    {
                        to = to + " " + addresses[j].toString();
                    }
                }
                // SUBJECT
                if (email.getSubject() != null) 
                {
                    subject = email.getSubject();
                }
                date_sent = timestamp_format.format(email.getSentDate());
                temp[0] = from;
                temp[1] = subject;
                temp[2] = to;
                temp[3] = date_sent;
                messages.add(temp);
                //process_email(uuid, email);
                if(i >= 9)
                {
                    break;
                }
            }
            // close the store and folder objects
            emailFolder.close(false);
            store.close();
        }
        catch(Exception e)
        {
            System.out.println("Exception in email.get_all_for_test=" + e);
            String temp[] = {"","","",""};
            temp[0] = "Error";
            temp[1] = e.getMessage();
            temp[2] = "Error";
            temp[3] = "";
            messages.add(temp);
        }
        
        return messages;
    }
}

/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */

package support;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

/**
 *
 * @author rcampbell
 */
public class format_email 
{
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
    private static SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
    
    public static String text(Connection con, String type, String id, String format, String user_tz_time) throws IOException, SQLException
    {
        String incident_format_vars[] = {"<$incident_id$>","<$incident_time$>","<$caller$>","<$caller_group$>","<$location$>","<$department$>","<$site$>","<$company$>","<$category$>","<$subcategory$>","<$priority$>","<$description$>","<$create_date$>","<$create_by$>","<$contact_method$>","<$state$>","<$state_date$>","<$assigned_group$>","<$assigned_to$>","<$notes$>","<$desk_notes$>","<$closed_date$>","<$closed_reason$>","<$pending_date$>","<$pending_reason$>"}; 
        
        String incident_field_index[] = {"id","incident_time","caller_id_username","caller_group_id_name","location","department","site","company","category","subcategory","priority","description","create_date","create_by_id_username","contact_method","state","state_date","assigned_group_id_name","assigned_to_id_username","notes","desk_notes","closed_date","closed_reason","pending_date","pending_reason"}; 
        
        String request_format_vars[] = {"<$request_id$>","<$rev$>","<$rev_date$>","<$rev_by_user_id$>","<$service_catalog$>","<$service_catalog_item$>","<$create_date$>","<$request_date$>","<$due_date$>","<$resolve_date$>","<$closed_date$>","<$assigned_group$>","<$assigned_to$>","<$create_by$>","<$requested_for_id$>","<$requested_for_group$>","<$location$>","<$department$>","<$site$>","<$company$>","<$impact$>","<$urgency$>","<$priority$>","<$state$>","<$contact_method$>","<$approval$>","<$price$>","<$notes$>","<$desk_notes$>","<$description$>","<$closed_reason$>","<$required_info$>","<$required_info_response$>","<$optional_info$>","<$optional_info_response$>"};
        
        String request_field_index[] = {"id","rev","rev_date","rev_by_user_id_username","service_catalog_id_name","service_catalog_item_id_name","create_date","request_date","due_date","resolve_date","closed_date","assigned_group_id_name","assigned_to_id_username","create_by_id_username","requested_for_id_username","requested_for_group_id_name","location","department","site","company","impact","urgency","priority","state","contact_method","approval","price","notes","desk_notes","description","closed_reason","required_info","required_info_response","optional_info","optional_info_response"};
        
        String task_format_vars[] = {"<$task_id$>","<$project_id_name$>","<$name$>","<$description$>","<$assigned_group_id_name$>","<$assigned_to_id_username$>","<$priority$>","<$status$>","<$scheduled_start_date$>","<$actual_start_date$>","<$scheduled_end_date$>","<$actual_end_date$>","<$estimated_duration$>","<$actual_duration$>","<$notes$>"};
        String task_field_index[] = {"id","project_id_name","name","description","assigned_group_id_name","assigned_to_id_username","priority","status","scheduled_start_date","actual_start_date","scheduled_end_date","actual_end_date","estimated_duration","actual_duration","notes"};
        
        String job_format_vars[] = {"<$job_id$>","<$name$>","<$description$>","<$assigned_group_id_name$>","<$assigned_to_id_username$>","<$priority$>","<$status$>","<$scheduled_start_date$>","<$actual_start_date$>","<$scheduled_end_date$>","<$actual_end_date$>","<$estimated_duration$>","<actual_duration>","<$notes$>"};     
        String job_field_index[] = {"id","name","description","assigned_group_id_name","assigned_to_id_username","priority","status","scheduled_start_date","actual_start_date","scheduled_end_date","actual_end_date","estimated_duration","actual_duration","notes"};     
                                                            
        
        if(type.equalsIgnoreCase("incident"))
        {
            //System.out.println("incident format=" + format);
            HashMap<String,String> incident_info = db.get_incidents.incident_hash_by_id(con, id);
            for(int a = 0; a < incident_format_vars.length; a++)
            {
                try
                {
                    format = format.replace(incident_format_vars[a], incident_info.get(incident_field_index[a]));
                }
                catch(Exception e)
                {
                    //System.out.println("Exception getting incident info for index " + a + " Find=" + incident_format_vars[a] + " Replace with " + incident_info.get(incident_field_index[a]));
                }
            }
            //System.out.println("after incident format=" + format);
        }
        else if(type.equalsIgnoreCase("request"))
        {
            HashMap<String,String> request_info = db.get_requests.request_hash_by_id(con, id, user_tz_time);
            for(int a = 0; a < request_format_vars.length; a++)
            {
                try
                {
                    format = format.replace(request_format_vars[a], request_info.get(request_field_index[a]));
                }
                catch(Exception e)
                {
                    //System.out.println("Exception getting request info for index " + a + " Find=" + request_format_vars[a] + " Replace with " + request_info.get(request_field_index[a]));
                }
            }
        }
        else if(type.equalsIgnoreCase("task"))
        {
            HashMap<String,String> task_info = db.get_projects.project_task_full_info(con, id);
            for(int a = 0; a < task_format_vars.length; a++)
            {
                try
                {
                    format = format.replace(task_format_vars[a], task_info.get(task_field_index[a]));
                }
                catch(Exception e)
                {
                    System.out.println("Exception getting task info for index " + a + " Find=" + task_format_vars[a] + " Replace with " + task_info.get(task_field_index[a]));
                }
            }
        }
        else if(type.equalsIgnoreCase("job"))
        {
            HashMap<String,String> job_info = db.get_jobs.job_by_id_full(con, id);
            for(int a = 0; a < job_format_vars.length; a++)
            {
                try
                {
                    format = format.replace(job_format_vars[a], job_info.get(job_field_index[a]));
                }
                catch(Exception e)
                {
                    System.out.println("Exception getting job info for index " + a + " Find=" + job_format_vars[a] + " Replace with " + job_info.get(job_field_index[a]));
                }
            }
        }
        return format;
    }
}

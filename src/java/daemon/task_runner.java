/*
* Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
*/
package daemon;

import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class task_runner implements Runnable 
{
    private PreparedStatement stmt = null;
    private String context_dir;
    private SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss"); 
    private LinkedHashMap props = new LinkedHashMap();
    private Logger logger = null;
    
    public task_runner(String context_dir, LinkedHashMap props, Logger logger) 
    {
       this.context_dir = context_dir;
       this.props = props;
       this.logger = logger;
    }
    
    @Override
    public void run() 
    {
        //do surveys
        try
        {
            //System.out.println("start survey_processor.run_now");
            survey_processor.run_now(context_dir, props, logger);
            //System.out.println("end survey_processor.run_now");
        }
        catch(Exception e)
        {
            System.out.println("Exception in daemon.task_runner.run -> survey_processor.run_now");
        }
        
        //do email processing
        try
        {
            //System.out.println("start email_processor.run_now");
            email_processor.run_now(context_dir, props, logger);
            //System.out.println("end email_processor.run_now");
        }
        catch(Exception e)
        {
            System.out.println("Exception in daemon.task_runner.run -> email_processor.run_now");
        }
    }
    
}

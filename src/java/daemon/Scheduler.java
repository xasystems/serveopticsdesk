/*
* Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
*/

package daemon;

import java.util.LinkedHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@WebListener
public class Scheduler implements ServletContextListener 
{
    private ScheduledExecutorService run_task_runner;
    
    @Override
    public void contextInitialized(ServletContextEvent event) 
    {
        String context_dir = event.getServletContext().getRealPath("");        
        LinkedHashMap props = support.config.get_config(context_dir); 
        Logger logger = LogManager.getLogger();
        ServletContext sc = event.getServletContext();
        try
        {
            run_task_runner = Executors.newSingleThreadScheduledExecutor();
            run_task_runner.scheduleAtFixedRate(new task_runner(context_dir, props, logger), 0, 15, TimeUnit.SECONDS);    
        }
        catch(Exception e)
        {
            System.out.println("ERROR Exception Scheduler.contextInitialized:=" + e);
        }
    }
    @Override
    public void contextDestroyed(ServletContextEvent event) 
    {
        run_task_runner.shutdownNow();
    }
        
}

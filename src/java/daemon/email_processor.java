/*
* Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
*/
package daemon;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class email_processor 
{
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss"); 
    
    public static void run_now(String context_dir, LinkedHashMap sys_props, Logger logger) throws IOException, SQLException
    {
        String config_message_attachment_store = sys_props.get("message_attachment_store").toString();
        java.util.Date now = new java.util.Date();
        //get all active SO instances from the management database
        HashMap<String,String> active_so_instances = db.get_so_instance.active_instances(context_dir, sys_props);    //management connections    
        //for each instance
        for (Map.Entry<String, String> entry : active_so_instances.entrySet()) 
        {
            try
            {
                String customer_db = entry.getValue();            
                //get db connection info for the contract
                Connection contract_con = db.db_util.get_contract_connection_by_contract_id(sys_props, customer_db, context_dir);   
                String query = "INSERT INTO message (" 
                        + "`uuid`,"
                        + "`source`,"
                        + "`date_sent`,"
                        + "`from`,"
                        + "`subject`,"
                        + "`to`,"
                        + "`body`,"
                        + "`mime_type`,"
                        + "`priority`,"
                        + "`status`,"
                        + "`status_date`,"
                        + "`status_by_user_id`,"
                        + "`result`," 
                        + "`has_attachment`,"
                        + "`description`,"
                        + "`log`,"
                        + "`related_to`,"
                        + "`related_to_id`,"
                        + "`assigned_to_id`,"
                        + "`date_assigned`) "
                        + "VALUES "
                        + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";  
                PreparedStatement stmt = contract_con.prepareStatement(query);                
                File this_customer_message_attachements = new File(config_message_attachment_store + "/" + customer_db);
                if(!this_customer_message_attachements.exists()) //if the customer attachment dir not exist ctreate it
                {   
                    this_customer_message_attachements.mkdirs(); 
                }
                
                //get email schedule
                String sys_sch[] = db.system_schedules.get_by_name(contract_con, "get_email");
                String last_run_interval_db_value = sys_sch[1];
                String last_run_db_value = sys_sch[2];
                //is it time to run?
                //System.out.println("get_email for instance " + entry.getValue() + " is " + sys_sch[1] + " lastrun=" + sys_sch[2]);
                boolean time_to_run = false;
                
                //is email get enabled?
                HashMap <String,String> get_email_server = db.get_configuration_properties.get_email_server(contract_con, context_dir);                
                
                
                String get_email_enable = get_email_server.get("get_email_enable"); 
                if(get_email_enable.equalsIgnoreCase("true"))
                {
                    if(last_run_db_value.equalsIgnoreCase(""))
                    {
                        time_to_run = true;
                        //System.out.println("time to run because never ran");                    
                    }
                    else
                    {
                        try
                        {
                            java.util.Date last_run = timestamp_format.parse(last_run_db_value);
                            GregorianCalendar last_run_cal = new GregorianCalendar();
                            last_run_cal.setTimeInMillis(last_run.getTime());
                            last_run_cal.add(last_run_cal.MINUTE, Integer.parseInt(last_run_interval_db_value));
                            //java.util.Date now = new java.util.Date();
                            if(now.getTime() > last_run_cal.getTimeInMillis())
                            {
                                time_to_run = true;
                                //System.out.println("time to run because its time");
                            }
                            else
                            {
                                //System.out.println("NOT time to run because its time");
                            }
                        }
                        catch(Exception e)
                        {
                            time_to_run = true;
                            System.out.println("time to run because error in time to run=" + e);
                        }                    
                    }
                    //if time to run 
                    if(time_to_run)
                    {
                        //get the emails
                        try 
                        {
                            HashMap <String,String> server_params = db.get_configuration_properties.get_email_server(contract_con, context_dir);
                            Iterator ita = server_params.entrySet().iterator();
                            //System.out.println("server_params size=" + server_params.size());
                            //while (ita.hasNext()) 
                            //{
                            //    Map.Entry pair = (Map.Entry)ita.next();
                            //    
                            //    System.out.println("373 email_scheduler=" + pair.getKey() + " = " + pair.getValue());
                            //}
                            String get_email_host = server_params.get("get_email_host");
                            //System.out.println("370 get_email_host=" + get_email_host);
                            
                            String get_email_user = server_params.get("get_email_user");
                            String get_email_password = server_params.get("get_email_password");
                            String get_email_store_protocol = get_email_server.get("get_email_store_protocol"); 
                            //System.out.println("get_email_store_protocol=" + get_email_store_protocol);
                            final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
                            //System.out.println("Inside MailReader()...");
                            Properties properties = new Properties();
                            Store store = null;
                            Session emailSession = null;
                            //get specific
                            // Set manual Properties
                            if(get_email_store_protocol.equalsIgnoreCase("pop3"))
                            {
                                //get pop3 parameters
                                HashMap <String,String> params = db.get_configuration_properties.get_email_server_properties(contract_con, "pop3");
                                Iterator it = params.entrySet().iterator();
                                while (it.hasNext()) 
                                {
                                    Map.Entry pair = (Map.Entry)it.next();
                                    properties.put(pair.getKey(),pair.getValue());
                                    //System.out.println("email_scheduler=" + pair.getKey() + " = " + pair.getValue());
                                }
                                emailSession = Session.getDefaultInstance(properties);
                                store = emailSession.getStore("pop3");                            
                            }
                            else if(get_email_store_protocol.equalsIgnoreCase("pop3s"))
                            {
                                //get pop3 parameters
                                HashMap <String,String> params = db.get_configuration_properties.get_email_server_properties(contract_con, "pop3");
                                Iterator it = params.entrySet().iterator();
                                while (it.hasNext()) 
                                {
                                    Map.Entry pair = (Map.Entry)it.next();
                                    properties.put(pair.getKey(),pair.getValue());
                                    //System.out.println("email_scheduler=" + pair.getKey() + " = " + pair.getValue());
                                }
                                emailSession = Session.getDefaultInstance(properties);
                                store = emailSession.getStore("pop3s");                            
                            }
                            else if(get_email_store_protocol.equalsIgnoreCase("imap"))
                            {
                                HashMap <String,String> params = db.get_configuration_properties.get_email_server_properties(contract_con, "imap");
                                Iterator it = params.entrySet().iterator();
                                while (it.hasNext()) 
                                {
                                    Map.Entry pair = (Map.Entry)it.next();
                                    properties.put(pair.getKey(),pair.getValue());
                                    //System.out.println(pair.getKey() + " = " + pair.getValue());
                                }
                                emailSession = Session.getDefaultInstance(properties);
                                store = emailSession.getStore("imap");
                            }
                            //System.out.println("get_email_host=" + get_email_host + " get_email_user=" + get_email_user + " get_email_password=" + get_email_password);
                            store.connect(get_email_host, get_email_user, get_email_password);
                            // create the folder object and open it
                            Folder emailFolder = store.getFolder("INBOX");
                            emailFolder.open(Folder.READ_WRITE);
                            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                            // retrieve the messages from the folder in an array and print it
                            Message[] messages = emailFolder.getMessages();
                            //System.out.println("messages.length---" + messages.length);
                            //GET THE EMAILS/////////////////////////////////////////////////////////////////////////////////////
                            for (int i = 0; i < messages.length; i++) 
                            {
                                Message message = messages[i];
                                UUID Uuid = UUID.randomUUID(); //Generates random UUID                                  
                                String uuid = Uuid.toString();
                                String source = "email";
                                String date_sent = null;
                                try
                                {
                                    date_sent = timestamp_format.format(message.getSentDate());
                                }
                                catch(Exception e)
                                {
                                    date_sent = null;
                                }
                                String from = "";
                                Address[] addresses;
                                if ((addresses = message.getFrom()) != null) 
                                {
                                    for (int j = 0; j < addresses.length; j++) 
                                    {
                                        //System.out.println("FROM: " + addresses[j].toString());
                                        if(j > 0)
                                        {
                                            from = from + "," + addresses[j].toString();
                                        }
                                        else
                                        {
                                            from = addresses[j].toString();
                                        }
                                        //writer.write("FROM: " + addresses[j].toString() + System.lineSeparator());
                                    }
                                }
                                String subject = "";
                                if (message.getSubject() != null) 
                                {
                                    //System.out.println("SUBJECT: " + message.getSubject()+ System.lineSeparator());
                                    subject = message.getSubject();
                                    //writer.write("SUBJECT: " + message.getSubject()+ System.lineSeparator());
                                }
                                String to = "";
                                // TO
                                if ((addresses = message.getRecipients(Message.RecipientType.TO)) != null) 
                                {
                                    for (int j = 0; j < addresses.length; j++) 
                                    {
                                        //System.out.println("TO: " + addresses[j].toString());
                                        if(j > 0)
                                        {
                                            to = to + "," + addresses[j].toString();
                                        }
                                        else
                                        {
                                            to = addresses[j].toString();
                                        }
                                        //writer.write("TO: " + addresses[j].toString() + System.lineSeparator());
                                    }
                                }
                                String body = daemon.email_support.getTextFromMessage(message);
                                String mime_type = "";
                                String priority = "";
                                String status = "New";
                                String status_date = timestamp_format.format(now);
                                String status_by_user_id = "0";
                                String result = "";
                                String has_attachment = "";
                                String description = "";
                                String log = "";
                                String related_to = "";
                                String related_to_id = null;
                                String assigned_to_id = null;
                                String date_assigned = null;
                                  
                                
                                //save the to email basics then process the rest of the parts
                                stmt.setString(1, uuid);
                                stmt.setString(2, source);
                                stmt.setString(3, date_sent);
                                stmt.setString(4, from);
                                stmt.setString(5, subject);
                                stmt.setString(6, to);
                                stmt.setString(7, body);
                                stmt.setString(8, mime_type);
                                stmt.setString(9, priority);
                                stmt.setString(10, status);
                                stmt.setString(11, status_date);
                                stmt.setString(12, status_by_user_id);
                                stmt.setString(13, result); 
                                stmt.setString(14, has_attachment);
                                stmt.setString(15, description);
                                stmt.setString(16, log);
                                stmt.setString(17, related_to);
                                stmt.setString(18, related_to_id);
                                stmt.setString(19, assigned_to_id);
                                stmt.setString(20, date_assigned);                                
                                stmt.execute();                                
                                //process the rest of the email parts
                                daemon.email_support.writePart(contract_con, this_customer_message_attachements, Uuid, message);  //customer_con
                                //delete the email went done
                                //System.out.println("delete message=" + message.getSubject());
                                message.setFlag(Flags.Flag.DELETED, true);
                                
                                //save the email in calls
                                //# id, media_type, create_time, accept_time, accept_time_seconds, abandon_time, abandon_time_seconds, call_duration_seconds, channel, direction, agent_name, post_process_time, process_time, total_time, interaction_id
                                //'10016', 'Email', '2017-08-08 14:05:22', '2017-08-08 14:07:33', '131', NULL, NULL, '557', '1-800-555-1234', '0', 'Alan King (Agent3)', '195', '362', '557', '10016'
                                String media_type = "Email";
                                String create_time = date_sent;
                                String accept_time = null;
                                String accept_time_seconds = null;
                                String abandon_time = null;
                                String abandon_time_seconds = null;
                                String call_duration_seconds = null;
                                String channel = "Email";
                                String direction = "0";
                                String agent_name = null;
                                String post_process_time = null;
                                String process_time = null;
                                String total_time = null;
                                String interaction_id = uuid;
                                //insert emails into calls too for contact sla/surveys!
                                String calls_insert = "INSERT INTO calls(" 
                                        + "media_type,"
                                        + "create_time,"
                                        + "accept_time,"
                                        + "accept_time_seconds,"
                                        + "abandon_time," 
                                        + "abandon_time_seconds,"
                                        + "call_duration_seconds," 
                                        + "channel,"  
                                        + "direction,"
                                        + "agent_name,"
                                        + "post_process_time," 
                                        + "process_time," 
                                        + "total_time,"
                                        + "interaction_id) "
                                        + " VALUE "
                                        + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                PreparedStatement stmt2 = contract_con.prepareStatement(calls_insert);       
                                stmt2.setString(1, media_type);
                                stmt2.setString(2, create_time);
                                stmt2.setString(3, accept_time);
                                stmt2.setString(4, accept_time_seconds);
                                stmt2.setString(5, abandon_time);
                                stmt2.setString(6, abandon_time_seconds);
                                stmt2.setString(7, call_duration_seconds);
                                stmt2.setString(8, channel);
                                stmt2.setString(9, direction);
                                stmt2.setString(10, agent_name);
                                stmt2.setString(11, post_process_time);
                                stmt2.setString(12, process_time);
                                stmt2.setString(13, total_time);
                                stmt2.setString(14, interaction_id);
                                System.out.println("insert into call=" + stmt2);
                                stmt2.execute();   
                            }//end for each email
                            // close the store and folder objects
                            emailFolder.close(true);
                            store.close();
                        } 
                        catch (NoSuchProviderException e) 
                        {
                            e.printStackTrace();
                        } 
                        catch (MessagingException e) 
                        {
                            e.printStackTrace();
                        } 
                        catch (IOException e) 
                        {
                            e.printStackTrace();
                        } 
                        catch (Exception e) 
                        {
                            System.out.println("Exception in email_processor=" + e);
                        }
                    }
                    //System.out.println("END get_email is enabled");
                }
                else
                {
                    //System.out.println("get_email is NOT enabled");
                }
                contract_con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in email_scheduler.run=" + e);
            }
        }//end for each so instance
    }
}

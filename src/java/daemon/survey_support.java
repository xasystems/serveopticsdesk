/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daemon;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.UUID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class survey_support 
{
    private static Logger logger = LogManager.getLogger();
    private static PreparedStatement stmt = null;
    private static String context_dir;
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss"); 
    private static LinkedHashMap props = new LinkedHashMap();
    
    public static ArrayList<String[]> get_sent_surveys(Connection thread_con, String survey_id, java.util.Date start_date, java.util.Date end_date)
    {
        ArrayList<String[]> return_list = new ArrayList();
        PreparedStatement stmt = null;
        try 
        {
            String query = "SELECT * FROM survey_results WHERE date_sent >= ? AND date_sent <= ? AND survey_id = ?";
            stmt = thread_con.prepareStatement(query);
            stmt.setTimestamp(1,new Timestamp(start_date.getTime()));
            stmt.setTimestamp(2,new Timestamp(end_date.getTime())); 
            stmt.setString(3,survey_id); 
               
            //System.out.println("query=" + stmt);
            // Create a Statement Object
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                String add_array[] = new String[11];
                add_array[0] = rs.getString("id");
                add_array[1] = rs.getString("survey_id");
                add_array[2] = rs.getString("user_id");
                add_array[3] = rs.getString("trigger_object");
                add_array[4] = rs.getString("trigger_id");
                add_array[5] = "";
                try
                {
                    add_array[5] = timestamp_format.format(rs.getDate("date_sent"));
                }
                catch(Exception e)
                {
                    add_array[5] = "";
                }
                add_array[6] = "";
                try
                {
                    add_array[6] = timestamp_format.format(rs.getDate("date_reminder_sent"));
                }
                catch(Exception e)
                {
                    add_array[6] = "";
                }
                add_array[7] = "";
                try
                {
                    add_array[7] = timestamp_format.format(rs.getDate("date_expire"));
                }
                catch(Exception e)
                {
                    add_array[7] = "";
                }
                add_array[8] = "";
                try
                {
                    add_array[8] = timestamp_format.format(rs.getDate("date_completed"));
                }
                catch(Exception e)
                {
                    add_array[8] = "";
                }
                add_array[9] = rs.getString("answers_json");
                add_array[10] = rs.getString("survey_key");
                return_list.add(add_array);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException send_surveys.get_sent_surveys:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception send_surveys.get_sent_surveys:="+ exc);
	}        
        return return_list;
    }
    public static int get_surveys_sent_to_user_id(ArrayList<String[]> sent_surveys, String user_id)
    {
        int count = 0;
        for(int a = 0; a < sent_surveys.size();a++)
        {
            String this_sent_survey[] = sent_surveys.get(a);
            if(this_sent_survey[2].equalsIgnoreCase(user_id))
            {
                count++;
            }
        }
        return count;
    }
    public static String get_survey_key()
    {
        return(UUID.randomUUID().toString());
    }
    public static void insert_survey_results(Connection thread_con, String survey_id, String user_id, String trigger_object,String trigger_id,java.util.Date date_sent,java.util.Date date_expire,java.util.Date date_completed,String answers_json,String survey_key) throws IOException, SQLException
    {
        PreparedStatement stmt = null;
        try 
        {
            String query = "INSERT INTO survey_results(survey_id,user_id,trigger_object,trigger_id,date_sent,date_expire,date_completed,answers_json,survey_key) VALUES(?,?,?,?,?,?,?,?,?)";
            stmt = thread_con.prepareStatement(query);
            stmt.setString(1,survey_id);
            stmt.setString(2,user_id);
            stmt.setString(3,trigger_object);
            stmt.setString(4,trigger_id);
            stmt.setTimestamp(5,new Timestamp(date_sent.getTime()));
            stmt.setTimestamp(6,new Timestamp(date_expire.getTime()));
            stmt.setTimestamp(7,null);
            stmt.setString(8,null);
            stmt.setString(9,survey_key);
            stmt.execute();            
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException send_surveys.insert_survey_results:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception send_surveys.insert_survey_results:="+ exc);
	}        
    } 
    public static void update_survey_results(Connection thread_con, java.util.Date now,String survey_key) throws IOException, SQLException
    {
        PreparedStatement stmt = null;
        try 
        {
            String query = "UPDATE survey_results SET date_reminder_sent=? WHERE survey_key=?";
            stmt = thread_con.prepareStatement(query);
            stmt.setTimestamp(1,new Timestamp(now.getTime()));
            stmt.setString(2,survey_key);
            stmt.execute();            
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException send_surveys.update_survey_results:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception send_surveys.update_survey_results:="+ exc);
	}        
    } 
}

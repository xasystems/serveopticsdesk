/*
* Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
*/
package daemon;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.UUID;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import org.apache.commons.text.StringEscapeUtils;
/**
 *
 * @author ralph
 */
public class email_support 
{
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss"); 
    private static SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
    
    public static String getTextFromMessage(Message message) throws MessagingException, IOException 
    {
        String result = "";
        if (message.isMimeType("text/plain")) 
        {
            result = message.getContent().toString();
        } 
        else if (message.isMimeType("multipart/*")) 
        {
            MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
            result = getTextFromMimeMultipart(mimeMultipart);
        }
        return result;
    }

    public static String getTextFromMimeMultipart (MimeMultipart mimeMultipart)  throws MessagingException, IOException
    {
        String result = "";
        int count = mimeMultipart.getCount();
        for (int i = 0; i < count; i++) 
        {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if (bodyPart.isMimeType("text/plain")) 
            {
                result = result + "\n" + bodyPart.getContent();
                break; // without break same text appears twice in my tests
            } 
            else if (bodyPart.isMimeType("text/html")) 
            {
                String html = (String) bodyPart.getContent();
                result = result + "\n" + StringEscapeUtils.escapeHtml4(html);
            } 
            else if (bodyPart.getContent() instanceof MimeMultipart)
            {
                result = result + getTextFromMimeMultipart((MimeMultipart)bodyPart.getContent());
            }
        }
        return result;
    }

    public static void writePart(Connection customer_con, File this_customer_email_attachements, UUID uuid, Part p) throws Exception 
    {
        java.util.Date now = new java.util.Date();
        //check if the content is plain text       
        
        //the email table
        String query = "UPDATE message SET mime_type=?,status=?,status_date=?,status_by_user_id=?,has_attachment=?,log=? WHERE uuid=?";
        PreparedStatement stmt = customer_con.prepareStatement(query);
        String mime_type = "";
        String body_mime_type ="";
        String status = "New";
        String status_date = timestamp_format.format(now);
        String status_by_user_id = "0";
        String has_attachment = "false";
        String log = "Email processes on " + status_date + System.lineSeparator();
        
        //the email_attachement table
        String query_attachment = "INSERT INTO message_attachements (`uuid`,`name_on_disk`,`file_name`,`file_date`,`file_size`) VALUES (?,?,?,?,?)";
        PreparedStatement stmt_attachment = customer_con.prepareStatement(query_attachment);
        String name_on_disk = "";
        String file_name = "";
        Timestamp file_date = new Timestamp(now.getTime());
        int file_size = 0;
        if (p.isMimeType("text/plain")) 
        {
            mime_type = "text/plain";
            body_mime_type = "text/plain";
        }
        if (p.isMimeType("text/html")) 
        {
            body_mime_type = "text/html";
            mime_type = "text/html";
        } //check if the content has attachment
        else if (p.isMimeType("multipart/*")) 
        {
            mime_type = "multipart";
            body_mime_type = "multipart";
            // content may contain attachments
            Multipart multiPart = (Multipart) p.getContent();
            int numberOfParts = multiPart.getCount();
            //System.out.println("Multipart number of parts=" + numberOfParts);
            for (int partCount = 0; partCount < numberOfParts; partCount++) 
            {
                MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
                BodyPart bodyPart = multiPart.getBodyPart(partCount);                
                String disposition = part.getDisposition();                
                String file=part.getFileName();  
                //External attachments
                if (disposition != null && Part.ATTACHMENT.equalsIgnoreCase(disposition)) 
                {
                    try
                    {
                        has_attachment = "true";
                        // this part is attachment
                        name_on_disk = uuid + "_"+ part.getFileName().replaceAll("[^a-zA-Z0-9\\._]+", "_"); //To make attachment name uniq we are adding current datatime before name.
                        file_name = part.getFileName().replaceAll("[^a-zA-Z0-9\\._]+", "_");
                        file_size = part.getSize();
                        part.saveFile(new File(this_customer_email_attachements + "/" + name_on_disk));   //To save the attachment file at specific location.
                        stmt_attachment.setString(1, uuid.toString());
                        stmt_attachment.setString(2, name_on_disk);
                        stmt_attachment.setString(3, file_name);
                        stmt_attachment.setTimestamp(4, file_date);
                        stmt_attachment.setInt(5, file_size);
                        //System.out.println("email_support 94 stmt=" + stmt);
                        stmt_attachment.execute();
                    }
                    catch(Exception e)
                    {
                        System.out.println("Exception getting email attachment=" + e);
                    }
                }
                //Inline Attachments
                else if (disposition != null && Part.INLINE.equalsIgnoreCase(disposition)) 
                {
                    try
                    {
                        has_attachment = "true";
                        // this part is attachment
                        name_on_disk = uuid + "_"+ part.getFileName().replaceAll("[^a-zA-Z0-9\\._]+", "_"); //To make attachment name uniq we are adding current datatime before name.
                        file_name = part.getFileName().replaceAll("[^a-zA-Z0-9\\._]+", "_");
                        file_size = part.getSize();
                        part.saveFile(new File(this_customer_email_attachements + "/" + name_on_disk));   //To save the attachment file at specific location.
                        stmt_attachment.setString(1, uuid.toString());
                        stmt_attachment.setString(2, name_on_disk);
                        stmt_attachment.setString(3, file_name);
                        stmt_attachment.setTimestamp(4, file_date);
                        stmt_attachment.setInt(5, file_size);
                        stmt_attachment.execute();
                    }
                    catch(Exception e)
                    {
                        System.out.println("Exception getting email INLINE attachment=" + e);
                    }
                }
                //Inline icons and smileys
                else if(file != null && disposition==null)
                {
                    try
                    {
                        has_attachment = "true";
                        // this part is attachment
                        name_on_disk = uuid + "_"+ part.getFileName().replaceAll("[^a-zA-Z0-9\\._]+", "_"); //To make attachment name uniq we are adding current datatime before name.
                        file_name = part.getFileName().replaceAll("[^a-zA-Z0-9\\._]+", "_");
                        file_size = part.getSize();
                        part.saveFile(new File(this_customer_email_attachements + "/" + name_on_disk));   //To save the attachment file at specific location.
                        stmt_attachment.setString(1, uuid.toString());
                        stmt_attachment.setString(2, name_on_disk);
                        stmt_attachment.setString(3, file_name);
                        stmt_attachment.setTimestamp(4, file_date);
                        stmt_attachment.setInt(5, file_size);
                        stmt_attachment.execute();
                    }
                    catch(Exception e)
                    {
                        System.out.println("Exception getting email ICON(S) attachment=" + e);
                    }
                }
                else if(part.isMimeType("text/plain"))
                {
                    body_mime_type = "text/plain";
                    //Object o = part.getContent();
                    //body = body + (String) o + System.lineSeparator();
                }
                else if (bodyPart.isMimeType("text/html")) 
                {
                    body_mime_type = "text/html";
                    //body = body + (String) bodyPart.getContent() + System.lineSeparator() + "----------------------------------" + System.lineSeparator();
                } 
            } 
        } //check if the content is a nested message
        else if (p.isMimeType("message/rfc822")) 
        {
            writePart(customer_con, this_customer_email_attachements, uuid,(Part) p.getContent());
        } 
        //check if the content is an inline image
        else if (p.isMimeType("image/jpeg")) 
        {
            System.out.println("--------> image/jpeg");
            int i = 0;
            Object o = p.getContent();

            InputStream x = (InputStream) o;
            byte[] bArray = new byte[x.available()];

            // Construct the required byte array
            System.out.println("x.length = " + x.available());
            while ((i = (int) ((InputStream) x).available()) > 0) 
            {
                int result = (int) (((InputStream) x).read(bArray));
                if (result == -1) 
                {
                    break;
                }
            }
            has_attachment = "true";
            file_name = p.getFileName().replaceAll("[^a-zA-Z0-9\\._]+", "_");
            file_size = p.getSize();
            name_on_disk = uuid + "_"+ file_name.replaceAll("[^a-zA-Z0-9\\._]+", "_");
            stmt_attachment.setString(1, uuid.toString());
            stmt_attachment.setString(2, name_on_disk);
            stmt_attachment.setString(3, file_name);
            stmt_attachment.setTimestamp(4, file_date);
            stmt_attachment.setInt(5, file_size);
            stmt_attachment.execute();
            FileOutputStream f2 = new FileOutputStream(this_customer_email_attachements + "/attachments/" + name_on_disk);
            f2.write(bArray);
        } 
        else if (p.getContentType().contains("image/")) 
        {
            System.out.println("content type" + p.getContentType());
            File f = new File("c:/temp/email/image" + uuid + ".jpg");
            DataOutputStream output = new DataOutputStream(
                    new BufferedOutputStream(new FileOutputStream(f)));
            com.sun.mail.util.BASE64DecoderStream test = (com.sun.mail.util.BASE64DecoderStream) p.getContent();
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = test.read(buffer)) != -1) 
            {
                output.write(buffer, 0, bytesRead);
            }
        } 
        else 
        {
            /*Object o = p.getContent();
            if (o instanceof String) 
            {
                body = body + (String) o + System.lineSeparator() + "----------------------------------" + System.lineSeparator();
            } 
            else if (o instanceof InputStream) 
            {
                System.out.println("This is just an input stream");
                System.out.println("---------------------------");
                //writer.write("Message Stream: " + System.lineSeparator());
                InputStream is = (InputStream) o;
                is = (InputStream) o;
                int c;
                while ((c = is.read()) != -1) 
                {
                    System.out.write(c);
                    //writer.write(c);
                }
            } 
            else 
            {
                System.out.println("This is an unknown type");
                System.out.println("---------------------------");
                //writer.write("Message Content Unknown: " + System.lineSeparator());
                //writer.write(o.toString());
                System.out.println(o.toString());
            }*/
        }
        //save the body and the mime_type
        //System.out.println("body_mime_type=" + body_mime_type);
        
        //if html content encap it
        //import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;
        //if(body_mime_type.equalsIgnoreCase("text/html"))
        //{
        //    body = StringEscapeUtils.escapeHtml4(body);
        //}
        //String source = "The less than sign (<) and ampersand (&) must be escaped before using them in HTML";
        try
        {
            stmt.setString(1,body_mime_type);
            stmt.setString(2,"New");
            stmt.setString(3,timestamp_format.format(now.getTime()));
            stmt.setString(4,"0");
            stmt.setString(5,has_attachment);            
            stmt.setString(6,"Email Received at: " + display_format.format(now) + " Status: New" + System.lineSeparator());
            stmt.setString(7, uuid.toString());
            //System.out.println("?=" + stmt);
            stmt.execute();
        }
        catch(Exception e)
        {
            System.out.println("Exception in email_support.writePart=" + e);
        }        
    }
}

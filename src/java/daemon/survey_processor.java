/*
* Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
*/

package daemon;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class survey_processor 
{
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss"); 
    private static PreparedStatement stmt = null;
    
    public static void run_now(String context_dir, LinkedHashMap sys_props, Logger logger) throws IOException, SQLException
    {
        HashMap active_instances = db.get_so_instance.active_instances(context_dir, sys_props);
        //System.out.println("active_instances=" + active_instances.size());
        //for each instance process surveys
        Map<String, String> map = active_instances;
        for (Map.Entry<String, String> entry : map.entrySet()) 
        {
            String contract_id = entry.getValue();
            //System.out.println("start send_surveys for contract_id=" + contract_id);
            //run_logger.debug("start send_surveys for contract_id=" + contract_id);    
            java.util.Date now = new java.util.Date();
            java.util.Date last_run = new java.util.Date();
            java.util.Date expire = new java.util.Date();
            java.util.Date thirty_days_back = new java.util.Date();
            java.util.Date start_date = new java.util.Date();
            String serveoptics_url = ""; 
            try
            {
                //Connection this_instance_con = db.db_util.get_instance_connection(context_dir, contract_id);
                Connection this_instance_con = db.db_util.get_contract_connection_by_contract_id(sys_props, contract_id, context_dir);
                serveoptics_url = db.get_configuration_properties.by_name(this_instance_con, "serveoptics_url");
                GregorianCalendar cal = new GregorianCalendar();
                cal.setTimeInMillis(now.getTime());
                long time = cal.getTimeInMillis();
                
                Long lobject = new Long(2592000000L); 
                  // Very large number will produce compile errors 
                long nl = lobject.longValue(); 
                //System.out.println("The Value of nl as long is = " + nl); 
                time = time - nl;
                thirty_days_back.setTime(time);            
                //System.out.println("30 back call=" + thirty_days_back.toString());                
                String survey_schedule_info[] = db.system_schedules.get_by_name(this_instance_con, "send_surveys");
                try
                {
                    last_run = timestamp_format.parse(survey_schedule_info[2]);
                }
                catch (ParseException e)
                {
                    last_run = new java.util.Date();
                    last_run.setTime(0);
                }
                int survey_schedule = 1;
                try
                {
                    survey_schedule = Integer.parseInt(survey_schedule_info[1]);
                }
                catch(NumberFormatException d)
                {
                    survey_schedule = 1;
                }
                //run_logger.debug("start send_surveys for contract_id=" + contract_id + "; survey_schedule=" + survey_schedule);
                //is it time to run
                long schedule_in_milliseconds = survey_schedule * 60000;
                if((last_run.getTime() + schedule_in_milliseconds) <= now.getTime())
                {
                    //run_logger.debug("send_surveys time to run");
                    //time to run
                    //get all active surveys
                    String all_active_csat_surveys[][] = db.get_surveys.all_active_csat(this_instance_con);
                    //for each active survey
                    int triggered_survey_count = 0;
                    for (String[] all_active_csat_survey : all_active_csat_surveys) 
                    { 
                        //get the survey info
                        String survey_id = all_active_csat_survey[0];
                        //System.out.println("working survey=" + all_active_csat_survey[2]);
                        ArrayList<String[]> sent_surveys;
                        sent_surveys = daemon.survey_support.get_sent_surveys(this_instance_con, survey_id,thirty_days_back, now);
                        triggered_survey_count = 0;
                        //System.out.println("create_date=\"" + all_active_csat_survey[1] + "\"");
                        java.util.Date create_date = timestamp_format.parse(all_active_csat_survey[1]);
                        //System.out.println("create_date PARSED =" + create_date.toString());
                        
                        //figure out how far back to look at incidents/requests
                        if(create_date.getTime() >  thirty_days_back.getTime())
                        {
                            start_date.setTime(create_date.getTime());
                            //System.out.println("using survey create date because its less than 30 days old startdate is now=" + start_date.toString());
                        }
                        else
                        {
                            start_date.setTime(thirty_days_back.getTime());
                            //System.out.println("using 30 days back date because its greater than 30 days old startdate is now=" + start_date.toString());
                        }
                        String name = all_active_csat_survey[2];
                        String description = all_active_csat_survey[3];
                        String version = all_active_csat_survey[4];
                        String status = all_active_csat_survey[5];
                        String type = all_active_csat_survey[6];
                        String contact_method = all_active_csat_survey[7];
                        String trigger_object = all_active_csat_survey[8];
                        String trigger_state = all_active_csat_survey[9];
                        String survey_trigger = all_active_csat_survey[10];
                        int trigger_interval = 0;
                        try 
                        {
                            trigger_interval = Integer.parseInt(all_active_csat_survey[11]);
                        }
                        catch(NumberFormatException e)
                        {
                            trigger_interval = 0;
                        }
                        int max_per_person = 0;
                        try 
                        {
                            max_per_person = Integer.parseInt(all_active_csat_survey[12]);
                        }
                        catch(NumberFormatException e)
                        {
                            max_per_person = 0;
                        }
                        int lifespan = 72;
                        try 
                        {
                            lifespan = Integer.parseInt(all_active_csat_survey[13]);
                        }
                        catch(NumberFormatException e)
                        {
                            lifespan = 72;
                        }
                        String email_subject = all_active_csat_survey[14];
                        String email_body = all_active_csat_survey[15];
                        String survey_link = serveoptics_url + "/survey.jsp?survey="; //http://localhost:8084
                        int email_reminder_schedule = 0;
                        try 
                        {
                            email_reminder_schedule = Integer.parseInt(all_active_csat_survey[16]);
                        }
                        catch(NumberFormatException e)
                        {
                            email_reminder_schedule = 0;
                        }
                        String email_reminder_subject = all_active_csat_survey[17];
                        String email_reminder_body = all_active_csat_survey[18];
                        email_reminder_body = email_reminder_body + "&#13;&#10;" + survey_link;
                        String survey_json = all_active_csat_survey[19];
                        String uuid = "";
                        //run_logger.debug("send_surveys start processing survey_id=" + survey_id);
                        //System.out.println("trigger_object=" + trigger_object);
                        //get all incidents or requests that the survey is defined for
                        if(trigger_object.equalsIgnoreCase("incident"))
                        {
                            //System.out.println("This is a incident survey");
                            //select * from incident/request where state=? and state_date >= ? and state_date <= ?
                            //if state is Create then any state but not resolved or closed
                            String query = "";
                            if(trigger_state.equalsIgnoreCase("create"))
                            {
                                //anything but resolved/closed
                                query = "SELECT * FROM incidents WHERE (state<>'Resolved' OR state<>'Closed') AND (state_date >=? AND state_date <= ?)";
                                stmt = this_instance_con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
                                stmt.setString(1, timestamp_format.format(start_date));
                                stmt.setString(2, timestamp_format.format(now)); 
                            }
                            else
                            {
                                //state = trigger_state
                                query = "SELECT * FROM incidents WHERE state=? AND (state_date >=? AND state_date <= ?)";
                                stmt = this_instance_con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
                                stmt.setString(1,trigger_state);
                                stmt.setString(2, timestamp_format.format(start_date));
                                stmt.setString(3, timestamp_format.format(now)); 
                            }
                            //System.out.println("survey_scheduler incident q=" + stmt.toString());
                            ResultSet rs = stmt.executeQuery();
                            //int count = 0;
                            //while(rs.next())
                            //{
                            //    count++;
                            //}
                            //rs.beforeFirst();
                            //System.out.println("number of incidents for this survey=" + count);
                            while(rs.next())
                            {
                                String incident_id = rs.getString("id");
                                //System.out.println("349 incident_id for survey=" + incident_id);
                                boolean survey_has_been_sent = false;
                                boolean survey_expired = false;
                                boolean survey_completed = false;
                                boolean survey_send_reminder = false;
                                boolean time_to_send_reminder = false;                            
                                String incident_user_id = rs.getString("caller_id");

                                for(int b = 0; b < sent_surveys.size(); b++)
                                {
                                    String sent_survey[] = sent_surveys.get(b);
                                    //System.out.println("472 sent_survey[]=:id=" + sent_survey[0] + " length=" + sent_survey.length);
                                    //add_array[0] = rs.getString("id");
                                    //add_array[1] = rs.getString("survey_id");
                                    //add_array[2] = rs.getString("user_id");
                                    //add_array[3] = rs.getString("trigger_object");
                                    //add_array[4] = rs.getString("trigger_id");
                                    //add_array[5] = timestamp_format.format(rs.getDate("date_sent"));
                                    //add_array[6] = timestamp_format.format(rs.getDate("date_reminder_sent"));
                                    //add_array[7] = timestamp_format.format(rs.getDate("date_expire"));
                                    //add_array[8] = timestamp_format.format(rs.getDate("date_completed"));
                                    //add_array[9] = rs.getString("answers_json");
                                    //add_array[10] = rs.getString("survey_key");
                                    uuid = sent_survey[10];
                                    String sent_survey_trigger_id = sent_survey[4];

                                    if(incident_id.equalsIgnoreCase(sent_survey_trigger_id))//has this request/incident been paged?
                                    {
                                        //System.out.println("incident has been surveyed=:" + incident_id);
                                        //yes
                                        survey_has_been_sent = true; //found it
                                        b = sent_surveys.size(); //break the for loop
                                        //has it been completed?
                                        java.util.Date date_completed = null;
                                        try 
                                        {
                                            date_completed = timestamp_format.parse(sent_survey[8]);
                                        }
                                        catch (Exception e)
                                        {
                                            date_completed = null;
                                        }
                                        if(date_completed == null)
                                        {
                                            //System.out.println("survey has been sent and NOT completed");
                                            survey_completed = false;
                                            //has expired
                                            java.util.Date date_expired = null;
                                            try 
                                            {
                                                date_expired = timestamp_format.parse(sent_survey[7]);
                                                if(date_expired.getTime() < now.getTime())
                                                {
                                                    survey_expired = true;
                                                    //System.out.println("survey has been sent, NOT completed, and IS EXPRIRED");
                                                }
                                                else
                                                {
                                                    survey_expired = false;
                                                    if(email_reminder_schedule == 0) //repage = yes
                                                    {
                                                        survey_send_reminder = false;
                                                    }
                                                    else
                                                    {
                                                        survey_send_reminder = true;
                                                        //is it time to send a reminder

                                                        //get time survey was sent
                                                        java.util.Date date_sent = null;
                                                        try {date_sent = timestamp_format.parse(sent_survey[5]);}catch (Exception e){date_sent = null;}
                                                        //get time reminder was sent
                                                        java.util.Date date_reminder_sent = null;
                                                        try {date_reminder_sent = timestamp_format.parse(sent_survey[6]);}catch (Exception e){date_reminder_sent = null;}
                                                        long last_sent_date = 0;
                                                        //which is larger? init send or reminder
                                                        if(date_reminder_sent == null)
                                                        {
                                                            last_sent_date = date_sent.getTime();
                                                        }
                                                        else
                                                        {
                                                            last_sent_date = date_reminder_sent.getTime();
                                                        }
                                                        long reminder_interval_in_msecs = email_reminder_schedule * 3600000;
                                                        if((now.getTime() - last_sent_date) > reminder_interval_in_msecs)
                                                        {
                                                            //yes send reminder
                                                            time_to_send_reminder = true;
                                                        } 
                                                    }
                                                }
                                            }
                                            catch (Exception e)
                                            {
                                                date_expired = null;
                                            }
                                        }//has not been completed
                                        else
                                        {
                                            //has been completed
                                            survey_completed = true;
                                        } 
                                    }
                                }//end for sent_surveys.size

                                //should i send a survey?
                                if(!survey_has_been_sent)
                                {
                                    //System.out.println("456 survey has NOT been sent");
                                    //trigger
                                    if(trigger_interval != 0)
                                    {  
                                        if(trigger_interval < triggered_survey_count)
                                        {
                                            //Dont send
                                            triggered_survey_count = 0;   
                                            //System.out.println("464 trigger count EXCEEDED");
                                        }
                                        else
                                        {
                                            //System.out.println("468 trigger count NOT EXCEEDED");
                                            //send initial survey                                        
                                            int number_sent_to_user = daemon.survey_support.get_surveys_sent_to_user_id(sent_surveys,incident_user_id);
                                            //System.out.println("544 < number_sent_to_user=" + number_sent_to_user);
                                            if((number_sent_to_user <= max_per_person) || (max_per_person == 0))
                                            {
                                                //System.out.println("474 < max/person");

                                                //send
                                                //System.out.println("survey incident_user_id=" + incident_user_id);
                                                String user_info[] = db.get_users.by_id(this_instance_con, incident_user_id);
                                                String users_email_address = user_info[15];
                                                //System.out.println("survey email=" + users_email_address);
                                                if(support.send_mail.is_valid(users_email_address))
                                                {
                                                    //System.out.println("481 email_address is valid");

                                                    GregorianCalendar eCal = new GregorianCalendar();
                                                    eCal.add(eCal.HOUR, lifespan);
                                                    expire.setTime(eCal.getTimeInMillis());  
                                                    String new_uuid = daemon.survey_support.get_survey_key();
                                                    daemon.survey_support.insert_survey_results(this_instance_con,survey_id,incident_user_id,"incident",incident_id,now,expire,null,null,new_uuid);   
                                                    //add this send survey to the survey_results list gotten earlier
                                                    String new_result_array[] = new String[11];
                                                    new_result_array[0] = "0";
                                                    new_result_array[1] = survey_id;
                                                    new_result_array[2] = incident_user_id;
                                                    new_result_array[3] = "incident";
                                                    new_result_array[4] = incident_id;
                                                    new_result_array[5] = timestamp_format.format(now);
                                                    new_result_array[6] = "";
                                                    new_result_array[7] = timestamp_format.format(expire);
                                                    new_result_array[8] = "";
                                                    new_result_array[9] = "";
                                                    new_result_array[10] = new_uuid;

                                                    //add_array[0] = rs.getString("id");
                                                    //add_array[1] = rs.getString("survey_id");
                                                    //add_array[2] = rs.getString("user_id");
                                                    //add_array[3] = rs.getString("trigger_object");
                                                    //add_array[4] = rs.getString("trigger_id");
                                                    //add_array[5] = timestamp_format.format(rs.getDate("date_sent"));
                                                    //add_array[6] = timestamp_format.format(rs.getDate("date_reminder_sent"));
                                                    //add_array[7] = timestamp_format.format(rs.getDate("date_expire"));
                                                    //add_array[8] = timestamp_format.format(rs.getDate("date_completed"));
                                                    //add_array[9] = rs.getString("answers_json");
                                                    //add_array[10] = rs.getString("survey_key");
                                                    
                                                    
                                                    sent_surveys.add(new_result_array); 
                                                    HashMap send_email_results = support.send_mail.now(this_instance_con, context_dir,users_email_address, email_subject, email_body +  "&#13;&#10;<a href=\"" + survey_link + new_uuid + "&id=" + contract_id + "\">Take the Survey</a>");
                                                    //System.out.println("567 send_email_results=" + send_email_results.get("status") + " result=" + send_email_results.get("result"));
                                                    triggered_survey_count++;
                                                }
                                            }
                                        }  
                                    }
                                }
                                else
                                {
                                    if(time_to_send_reminder && (trigger_interval != 0) && !survey_expired)
                                    {
                                        //send reminder
                                        daemon.survey_support.update_survey_results(this_instance_con,now,uuid);  
                                        String user_info[] = db.get_users.by_id(this_instance_con, incident_user_id);
                                        String users_email_address = user_info[15];
                                        HashMap send_email_results = support.send_mail.now(this_instance_con, context_dir,users_email_address, email_reminder_subject, email_reminder_body + "<a href=\"" + survey_link + uuid + "&id=" + contract_id + "\">Take the Survey</a>");
                                        //System.out.println("591 send_email_status=" + send_email_results.get("status") + " result=" + send_email_results.get("result"));
                                    }
                                }
                            }//end for each incident
                        } 
                        else if(trigger_object.equalsIgnoreCase("request"))
                        {
                            //System.out.println("request survey");
                            //select * from incident/request where state=? and state_date >= ? and state_date <= ?
                            //trigger states= create, resolved, closed
                            //db_values[] = {"pending_approval","approved","closed_complete","closed_incomplete","closed_cancelled","closed_Rejected"};
                            String query = "";
                            if(trigger_state.equalsIgnoreCase("create"))
                            {
                                //create
                                query = "SELECT * FROM request WHERE (state='pending_approval' OR state='approved')  AND (create_date >=? AND create_date <= ?)"; //create_date, resolved_date, closed_date
                            }
                            else if(trigger_state.equalsIgnoreCase("resolved") || trigger_state.equalsIgnoreCase("closed"))
                            {
                                //closed
                                query = "SELECT * FROM request WHERE (state='closed_complete' OR state='closed_incomplete' OR state='closed_cancelled' OR state='closed_rejected')  AND (closed_date >=? AND closed_date <= ?)";
                            }

                            stmt = this_instance_con.prepareStatement(query);
                            stmt.setString(1, timestamp_format.format(start_date));
                            stmt.setString(2, timestamp_format.format(now)); 
                            //System.out.println("REQUEST SURVEY q=" + stmt.toString());
                            ResultSet rs = stmt.executeQuery();
                            //int count = 0;
                            //while(rs.next())
                            //{
                            //    count++;
                            //}
                            //rs.beforeFirst();
                            //System.out.println("number of requests for this survey=" + count);
                            while(rs.next())
                            {
                                String request_id = rs.getString("id");
                                //System.out.println("589 request_id=" + request_id);
                                boolean survey_has_been_sent = false;
                                boolean survey_expired = false;
                                boolean survey_completed = false;
                                boolean survey_send_reminder = false;
                                boolean time_to_send_reminder = false;                            
                                String requested_for_user_id = rs.getString("requested_for_id");

                                for(int b = 0; b < sent_surveys.size(); b++)
                                {
                                    String sent_survey[] = sent_surveys.get(b);
                                    //add_array[0] = rs.getString("id");
                                    //add_array[1] = rs.getString("survey_id");
                                    //add_array[2] = rs.getString("user_id");
                                    //add_array[3] = rs.getString("trigger_object");
                                    //add_array[4] = rs.getString("trigger_id");
                                    //add_array[5] = timestamp_format.format(rs.getDate("date_sent"));
                                    //add_array[6] = timestamp_format.format(rs.getDate("date_reminder_sent"));
                                    //add_array[7] = timestamp_format.format(rs.getDate("date_expire"));
                                    //add_array[8] = timestamp_format.format(rs.getDate("date_completed"));
                                    //add_array[9] = rs.getString("answers_json");
                                    //add_array[10] = rs.getString("survey_key");
                                    uuid = sent_survey[10];
                                    String sent_survey_trigger_id = sent_survey[4];

                                    if(request_id.equalsIgnoreCase(sent_survey_trigger_id))//has this request/incident been paged?
                                    {
                                        //System.out.println("request has been paged=:" + request_id);
                                        //yes
                                        survey_has_been_sent = true; //found it
                                        b = sent_surveys.size(); //break the for loop
                                        //has it been completed?
                                        java.util.Date date_completed = null;
                                        try 
                                        {
                                            date_completed = timestamp_format.parse(sent_survey[8]);
                                        }
                                        catch (Exception e)
                                        {
                                            date_completed = null;
                                        }
                                        if(date_completed == null)
                                        {
                                            //System.out.println("Request survey has been sent and NOT completed");
                                            survey_completed = false;
                                            //has expired
                                            java.util.Date date_expired = null;
                                            try 
                                            {
                                                date_expired = timestamp_format.parse(sent_survey[7]);
                                                if(date_expired.getTime() < now.getTime())
                                                {
                                                    survey_expired = true;
                                                    //System.out.println("Request survey has been sent, NOT completed, and IS EXPRIRED");
                                                }
                                                else
                                                {
                                                    survey_expired = false;
                                                    if(email_reminder_schedule == 0) //repage = yes
                                                    {
                                                        survey_send_reminder = false;
                                                    }
                                                    else
                                                    {
                                                        survey_send_reminder = true;
                                                        //is it time to send a reminder
                                                        //System.out.println("Request repage is ON");
                                                        //get time survey was sent
                                                        java.util.Date date_sent = null;
                                                        try {date_sent = timestamp_format.parse(sent_survey[5]);}catch (Exception e){date_sent = null;}
                                                        //get time reminder was sent
                                                        java.util.Date date_reminder_sent = null;
                                                        try {date_reminder_sent = timestamp_format.parse(sent_survey[6]);}catch (Exception e){date_reminder_sent = null;}
                                                        long last_sent_date = 0;
                                                        //which is larger? init send or reminder
                                                        if(date_reminder_sent == null)
                                                        {
                                                            last_sent_date = date_sent.getTime();
                                                        }
                                                        else
                                                        {
                                                            last_sent_date = date_reminder_sent.getTime();
                                                        }
                                                        long reminder_interval_in_msecs = email_reminder_schedule * 3600000;
                                                        if((now.getTime() - last_sent_date) > reminder_interval_in_msecs)
                                                        {
                                                            //yes send reminder
                                                            time_to_send_reminder = true;
                                                        } 
                                                    }
                                                }
                                            }
                                            catch (Exception e)
                                            {
                                                date_expired = null;
                                            }

                                        }//has not been completed
                                        else
                                        {
                                            //has been completed
                                            survey_completed = true;
                                        } 
                                    }
                                }//end for sent_surveys.size

                                //should i send a survey?
                                if(!survey_has_been_sent)
                                {
                                    //System.out.println("Request 698 survey has not been sent");
                                    //trigger
                                    if(trigger_interval != 0)
                                    {  
                                        if(trigger_interval < triggered_survey_count)
                                        {
                                            //Dont send
                                            triggered_survey_count = 0;   
                                            //System.out.println("706 request trigger count EXCEEDED");
                                        }
                                        else
                                        {
                                            //System.out.println("710 request trigger count NOT EXCEEDED");
                                            //send initial survey                                        
                                            int number_sent_to_user = daemon.survey_support.get_surveys_sent_to_user_id(sent_surveys,requested_for_user_id);
                                            //System.out.println("778 < number_sent_to_user=" + number_sent_to_user);
                                            if((number_sent_to_user <= max_per_person) || (max_per_person == 0))
                                            {
                                                //System.out.println("781 < max/person");

                                                //send
                                                String user_info[] = db.get_users.by_id(this_instance_con, requested_for_user_id);
                                                String users_email_address = user_info[15];
                                                
                                                if(support.send_mail.is_valid(users_email_address))
                                                {
                                                    //System.out.println("Request 723 email_address is valid");

                                                    GregorianCalendar eCal = new GregorianCalendar();
                                                    eCal.add(eCal.HOUR, lifespan);
                                                    expire.setTime(eCal.getTimeInMillis());  
                                                    String new_uuid = daemon.survey_support.get_survey_key();
                                                    daemon.survey_support.insert_survey_results(this_instance_con,survey_id,requested_for_user_id,"request",request_id,now,expire,null,null,new_uuid);   
                                                    //add this send survey to the survey_results list gotten earlier
                                                    String new_result_array[] = new String[11];
                                                    new_result_array[0] = "0";
                                                    new_result_array[1] = survey_id;
                                                    new_result_array[2] = requested_for_user_id;
                                                    new_result_array[3] = "request";
                                                    new_result_array[4] = request_id;
                                                    new_result_array[5] = timestamp_format.format(now);
                                                    new_result_array[6] = "";
                                                    new_result_array[7] = timestamp_format.format(expire);
                                                    new_result_array[8] = "";
                                                    new_result_array[9] = "";
                                                    new_result_array[10] = new_uuid;

                                                    //add_array[0] = rs.getString("id");
                                                    //add_array[1] = rs.getString("survey_id");
                                                    //add_array[2] = rs.getString("user_id");
                                                    //add_array[3] = rs.getString("trigger_object");
                                                    //add_array[4] = rs.getString("trigger_id");
                                                    //add_array[5] = timestamp_format.format(rs.getDate("date_sent"));
                                                    //add_array[6] = timestamp_format.format(rs.getDate("date_reminder_sent"));
                                                    //add_array[7] = timestamp_format.format(rs.getDate("date_expire"));
                                                    //add_array[8] = timestamp_format.format(rs.getDate("date_completed"));
                                                    //add_array[9] = rs.getString("answers_json");
                                                    //add_array[10] = rs.getString("survey_key");
                                                    
                                                    sent_surveys.add(new_result_array); 
                                                    
                                                    HashMap send_email_results = support.send_mail.now(this_instance_con, context_dir,users_email_address, email_subject, email_body + "<a href=\"" + survey_link + new_uuid + "&id=" + contract_id + "\">Take the Survey</a>");
                                                    //System.out.println("760 REQUEST send_email_status=" + send_email_results.get("status") + " result=" + send_email_results.get("result"));
                                                    
                                                    
                                                    //String send_email_results = support.send_mail.now(thread_con, context_dir,users_email_address, email_subject, email_body +  "&#13;&#10;<a href=\"" + survey_link + new_uuid + "\">Take the Survey</a>");
                                                    //System.out.println("809 send_email_results=" + send_email_results);
                                                    triggered_survey_count++;
                                                }
                                                else
                                                {
                                                    //System.out.println("Request 765 email_address is NOT valid");
                                                }
                                            }
                                        }  
                                    }
                                }
                                else
                                {
                                    if(time_to_send_reminder && (trigger_interval != 0) && !survey_expired)
                                    {
                                        //send reminder
                                        daemon.survey_support.update_survey_results(this_instance_con,now,uuid);  
                                        String user_info[] = db.get_users.by_id(this_instance_con, requested_for_user_id);
                                        String users_email_address = user_info[15];
                                        HashMap send_email_results = support.send_mail.now(this_instance_con, context_dir,users_email_address, email_reminder_subject, email_reminder_body + "<a href=\"" + survey_link + uuid + "&id=" + contract_id + "\">Take the Survey</a>");
                                        //System.out.println("784 send_reminder email_results=" + send_email_results.get("status") + " result=" + send_email_results.get("result"));
                                    }
                                }
                            }//end for each request
                        }
                        else if(trigger_object.equalsIgnoreCase("contact"))
                        {
                            //System.out.println("contact survey");
                            //select * from incident/request where state=? and state_date >= ? and state_date <= ?
                            //trigger states= create, resolved, closed
                            //db_values[] = {"pending_approval","approved","closed_complete","closed_incomplete","closed_cancelled","closed_Rejected"};
                            String query = "";
                            if(trigger_state.equalsIgnoreCase("complete"))
                            {
                                //create
                                query = "SELECT * FROM calls WHERE (  AND (create_date >=? AND create_date <= ?)"; //create_date, resolved_date, closed_date
                            }
                            
                            stmt = this_instance_con.prepareStatement(query);
                            stmt.setString(1, timestamp_format.format(start_date));
                            stmt.setString(2, timestamp_format.format(now)); 
                            //System.out.println("REQUEST SURVEY q=" + stmt.toString());
                            ResultSet rs = stmt.executeQuery();
                            //int count = 0;
                            //while(rs.next())
                            //{
                            //    count++;
                            //}
                            //rs.beforeFirst();
                            //System.out.println("number of requests for this survey=" + count);
                            while(rs.next())
                            {
                                String request_id = rs.getString("id");
                                //System.out.println("589 request_id=" + request_id);
                                boolean survey_has_been_sent = false;
                                boolean survey_expired = false;
                                boolean survey_completed = false;
                                boolean survey_send_reminder = false;
                                boolean time_to_send_reminder = false;                            
                                String requested_for_user_id = rs.getString("requested_for_id");

                                for(int b = 0; b < sent_surveys.size(); b++)
                                {
                                    String sent_survey[] = sent_surveys.get(b);
                                    //add_array[0] = rs.getString("id");
                                    //add_array[1] = rs.getString("survey_id");
                                    //add_array[2] = rs.getString("user_id");
                                    //add_array[3] = rs.getString("trigger_object");
                                    //add_array[4] = rs.getString("trigger_id");
                                    //add_array[5] = timestamp_format.format(rs.getDate("date_sent"));
                                    //add_array[6] = timestamp_format.format(rs.getDate("date_reminder_sent"));
                                    //add_array[7] = timestamp_format.format(rs.getDate("date_expire"));
                                    //add_array[8] = timestamp_format.format(rs.getDate("date_completed"));
                                    //add_array[9] = rs.getString("answers_json");
                                    //add_array[10] = rs.getString("survey_key");
                                    uuid = sent_survey[10];
                                    String sent_survey_trigger_id = sent_survey[4];

                                    if(request_id.equalsIgnoreCase(sent_survey_trigger_id))//has this request/incident been paged?
                                    {
                                        //System.out.println("request has been paged=:" + request_id);
                                        //yes
                                        survey_has_been_sent = true; //found it
                                        b = sent_surveys.size(); //break the for loop
                                        //has it been completed?
                                        java.util.Date date_completed = null;
                                        try 
                                        {
                                            date_completed = timestamp_format.parse(sent_survey[8]);
                                        }
                                        catch (Exception e)
                                        {
                                            date_completed = null;
                                        }
                                        if(date_completed == null)
                                        {
                                            //System.out.println("Request survey has been sent and NOT completed");
                                            survey_completed = false;
                                            //has expired
                                            java.util.Date date_expired = null;
                                            try 
                                            {
                                                date_expired = timestamp_format.parse(sent_survey[7]);
                                                if(date_expired.getTime() < now.getTime())
                                                {
                                                    survey_expired = true;
                                                    //System.out.println("Request survey has been sent, NOT completed, and IS EXPRIRED");
                                                }
                                                else
                                                {
                                                    survey_expired = false;
                                                    if(email_reminder_schedule == 0) //repage = yes
                                                    {
                                                        survey_send_reminder = false;
                                                    }
                                                    else
                                                    {
                                                        survey_send_reminder = true;
                                                        //is it time to send a reminder
                                                        //System.out.println("Request repage is ON");
                                                        //get time survey was sent
                                                        java.util.Date date_sent = null;
                                                        try {date_sent = timestamp_format.parse(sent_survey[5]);}catch (Exception e){date_sent = null;}
                                                        //get time reminder was sent
                                                        java.util.Date date_reminder_sent = null;
                                                        try {date_reminder_sent = timestamp_format.parse(sent_survey[6]);}catch (Exception e){date_reminder_sent = null;}
                                                        long last_sent_date = 0;
                                                        //which is larger? init send or reminder
                                                        if(date_reminder_sent == null)
                                                        {
                                                            last_sent_date = date_sent.getTime();
                                                        }
                                                        else
                                                        {
                                                            last_sent_date = date_reminder_sent.getTime();
                                                        }
                                                        long reminder_interval_in_msecs = email_reminder_schedule * 3600000;
                                                        if((now.getTime() - last_sent_date) > reminder_interval_in_msecs)
                                                        {
                                                            //yes send reminder
                                                            time_to_send_reminder = true;
                                                        } 
                                                    }
                                                }
                                            }
                                            catch (Exception e)
                                            {
                                                date_expired = null;
                                            }

                                        }//has not been completed
                                        else
                                        {
                                            //has been completed
                                            survey_completed = true;
                                        } 
                                    }
                                }//end for sent_surveys.size

                                //should i send a survey?
                                if(!survey_has_been_sent)
                                {
                                    //System.out.println("Request 698 survey has not been sent");
                                    //trigger
                                    if(trigger_interval != 0)
                                    {  
                                        if(trigger_interval < triggered_survey_count)
                                        {
                                            //Dont send
                                            triggered_survey_count = 0;   
                                            //System.out.println("706 request trigger count EXCEEDED");
                                        }
                                        else
                                        {
                                            //System.out.println("710 request trigger count NOT EXCEEDED");
                                            //send initial survey                                        
                                            int number_sent_to_user = daemon.survey_support.get_surveys_sent_to_user_id(sent_surveys,requested_for_user_id);
                                            //System.out.println("778 < number_sent_to_user=" + number_sent_to_user);
                                            if((number_sent_to_user <= max_per_person) || (max_per_person == 0))
                                            {
                                                //System.out.println("781 < max/person");

                                                //send
                                                String user_info[] = db.get_users.by_id(this_instance_con, requested_for_user_id);
                                                String users_email_address = user_info[15];
                                                
                                                if(support.send_mail.is_valid(users_email_address))
                                                {
                                                    //System.out.println("Request 723 email_address is valid");

                                                    GregorianCalendar eCal = new GregorianCalendar();
                                                    eCal.add(eCal.HOUR, lifespan);
                                                    expire.setTime(eCal.getTimeInMillis());  
                                                    String new_uuid = daemon.survey_support.get_survey_key();
                                                    daemon.survey_support.insert_survey_results(this_instance_con,survey_id,requested_for_user_id,"request",request_id,now,expire,null,null,new_uuid);   
                                                    //add this send survey to the survey_results list gotten earlier
                                                    String new_result_array[] = new String[11];
                                                    new_result_array[0] = "0";
                                                    new_result_array[1] = survey_id;
                                                    new_result_array[2] = requested_for_user_id;
                                                    new_result_array[3] = "request";
                                                    new_result_array[4] = request_id;
                                                    new_result_array[5] = timestamp_format.format(now);
                                                    new_result_array[6] = "";
                                                    new_result_array[7] = timestamp_format.format(expire);
                                                    new_result_array[8] = "";
                                                    new_result_array[9] = "";
                                                    new_result_array[10] = new_uuid;

                                                    //add_array[0] = rs.getString("id");
                                                    //add_array[1] = rs.getString("survey_id");
                                                    //add_array[2] = rs.getString("user_id");
                                                    //add_array[3] = rs.getString("trigger_object");
                                                    //add_array[4] = rs.getString("trigger_id");
                                                    //add_array[5] = timestamp_format.format(rs.getDate("date_sent"));
                                                    //add_array[6] = timestamp_format.format(rs.getDate("date_reminder_sent"));
                                                    //add_array[7] = timestamp_format.format(rs.getDate("date_expire"));
                                                    //add_array[8] = timestamp_format.format(rs.getDate("date_completed"));
                                                    //add_array[9] = rs.getString("answers_json");
                                                    //add_array[10] = rs.getString("survey_key");
                                                    
                                                    sent_surveys.add(new_result_array); 
                                                    
                                                    HashMap send_email_results = support.send_mail.now(this_instance_con, context_dir,users_email_address, email_subject, email_body + "<a href=\"" + survey_link + new_uuid + "&id=" + contract_id + "\">Take the Survey</a>");
                                                    //System.out.println("760 REQUEST send_email_status=" + send_email_results.get("status") + " result=" + send_email_results.get("result"));
                                                    
                                                    
                                                    //String send_email_results = support.send_mail.now(thread_con, context_dir,users_email_address, email_subject, email_body +  "&#13;&#10;<a href=\"" + survey_link + new_uuid + "\">Take the Survey</a>");
                                                    //System.out.println("809 send_email_results=" + send_email_results);
                                                    triggered_survey_count++;
                                                }
                                                else
                                                {
                                                    //System.out.println("Request 765 email_address is NOT valid");
                                                }
                                            }
                                        }  
                                    }
                                }
                                else
                                {
                                    if(time_to_send_reminder && (trigger_interval != 0) && !survey_expired)
                                    {
                                        //send reminder
                                        daemon.survey_support.update_survey_results(this_instance_con,now,uuid);  
                                        String user_info[] = db.get_users.by_id(this_instance_con, requested_for_user_id);
                                        String users_email_address = user_info[15];
                                        HashMap send_email_results = support.send_mail.now(this_instance_con, context_dir,users_email_address, email_reminder_subject, email_reminder_body + "<a href=\"" + survey_link + uuid + "&id=" + contract_id + "\">Take the Survey</a>");
                                        //System.out.println("784 send_reminder email_results=" + send_email_results.get("status") + " result=" + send_email_results.get("result"));
                                    }
                                }
                            }//end for each contact
                        }
                        
                    } //end for each survey
                    //update last run
                    //run_logger.debug("Update last Run db.system_schedules.set_last_run");
                    db.system_schedules.set_last_run(this_instance_con,now,"send_surveys");
                }
                else
                {
                    //not time to run
                }
            }
            catch(Exception e)
            {
                logger.error("ERROR send_surveys.run:=" + e);
            }
        }
    }    
}

/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 * 
 * 
 */
package servlets;

import com.google.gson.JsonObject;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.commons.codec.binary.Hex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.Tika;
import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;

@MultipartConfig()
/**
 *
 * @author server-xc6701
 */
public class admin_users_edit extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String installation_type = props.get("installation_type").toString().trim();
        String contract_id = session.getAttribute("db").toString();
        CouchDbClient dbClient = null;
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            try 
            {
                boolean has_login_role = false;
                boolean needs_login = false;
                String id = request.getParameter("id");
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                String first = request.getParameter("first");
                String mi = request.getParameter("mi");
                String last = request.getParameter("last");
                String address_1 = request.getParameter("address_1");
                String address_2 = request.getParameter("address_2");
                String city = request.getParameter("city");
                String state = request.getParameter("state");
                String zip = request.getParameter("zip");
                String location = request.getParameter("location");
                String department = request.getParameter("department");
                String site = request.getParameter("site");
                String company = request.getParameter("company");
                String email = request.getParameter("email");
                String phone_office = request.getParameter("phone_office");
                String phone_mobile = request.getParameter("phone_mobile");
                String tz[] = support.data_check.null_string(request.getParameter("tz")).split(",");
                String tz_name = tz[0];
                String tz_time = tz[1];
                String notes = request.getParameter("notes");                
                String vip = "false";
                ArrayList<byte[]> salt_and_hash = new ArrayList(Arrays.asList(null, null));

                if(request.getParameter("vip") == null)
                {
                    //checkbox not checked
                    vip = "false";
                }
                else
                {
                    //checkbox checked
                    vip = "true";
                }
                String is_admin = "false";
                if(request.getParameter("is_admin") == null)
                {
                    //checkbox not checked
                    is_admin = "false";
                }
                else
                {
                    //checkbox checked
                    is_admin = "true";
                }                
                String external_id = request.getParameter("external_id");

                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement("UPDATE users SET username=?,first=?,mi=?,last=?,address_1=?,address_2=?,city=?,state=?,zip=?,location=?,department=?,site=?,company=?,email=?,phone_office=?,phone_mobile=?,notes=?,vip=?,is_admin=?,tz_name=?,tz_time=?,external_id=? WHERE id=?"); 
                stmt.setString(1,username);
//                stmt.setString(2,password);
                stmt.setString(2,first);
                stmt.setString(3,mi);
                stmt.setString(4,last);
                stmt.setString(5,address_1);
                stmt.setString(6,address_2);
                stmt.setString(7,city);
                stmt.setString(8,state);
                stmt.setString(9,zip);
                stmt.setString(10,location);
                stmt.setString(11,department);
                stmt.setString(12,site);
                stmt.setString(13,company);
                stmt.setString(14,email);
                stmt.setString(15,phone_office);
                stmt.setString(16,phone_mobile);
                stmt.setString(17,notes);
                stmt.setString(18,vip);
                stmt.setString(19,is_admin);
                stmt.setString(20,tz_name);
                stmt.setString(21,tz_time);
                stmt.setString(22,external_id);
                stmt.setString(23,id);                
                stmt.executeUpdate();

//                password = support.encrypt_utils.encrypt(context_dir, password);
                if (!password.equals("")) {
                    salt_and_hash = support.encrypt_utils.get_hash(password);
                    stmt = con.prepareStatement("UPDATE users SET password_salt = ?, password_hash = ?  WHERE id=?");
                    stmt.setBytes(1,salt_and_hash.get(0));
                    stmt.setBytes(2,salt_and_hash.get(1));
                    stmt.setString(3,id);
                    stmt.executeUpdate();
                }

                //clear existing roles
                stmt = con.prepareStatement("DELETE FROM user_roles WHERE user_id=?");
                stmt.setString(1,id);
                stmt.executeUpdate();
                    
                //start get the file attachment if there is one
                try
                {
                    Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
                    if (filePart != null)
                    {
                        try
                        {
                            dbClient = db.couchdb.get_avatars_client(props);
                            if (dbClient == null)
                            {
                                throw new Exception("dbClient is null");
                            }
                            String file_name;
                            Tika tika = new Tika();

        //                    logger.info("filepart:" + filePart);
                            String fileName = filePart.getSubmittedFileName();
                            if (fileName != null && !"".equals(fileName)) 
                            {
                                long file_size = filePart.getSize();
                                String mimeType = tika.detect(filePart.getInputStream());
                                file_name = "avatar";

                                String user_info[] = db.get_users.by_id(con, id);
                                String uuid;
                                String rev;
                                int new_uuid = 0;
                                Response dbclient_response;
                                java.util.Date now = new java.util.Date();
                                String user_id = session.getAttribute("user_id").toString();

                                if (user_info[24] != null && !user_info[24].equals(""))
                                {
                                    uuid = user_info[24];
                                    JsonObject json = new JsonObject();
                                    json = dbClient.find(JsonObject.class, uuid);
                                    Map<String, Object> map = new HashMap<>(); 
                                    map.put("_id", uuid);
                                    map.put("Filename", file_name);
                                    map.put("MIME_type", mimeType);
                                    map.put("FileSize", file_size);
                                    map.put("DateUploaded", now);
                                    map.put("UploadedBy", user_id);
                                    dbclient_response = dbClient.update(map);
                                    rev = dbclient_response.getRev();

//                                    rev = json.get("_rev").getAsString();
                                } else {
                                    UUID Uuid = UUID.randomUUID(); //Generates random UUID                                  
                                    uuid = Uuid.toString();
                                    new_uuid = 1;
                                    Map<String, Object> map = new HashMap<>(); 

                                    map.put("_id", uuid);
                                    map.put("Filename", file_name);
                                    map.put("MIME_type", mimeType);
                                    map.put("FileSize", file_size);
                                    map.put("DateUploaded", now);
                                    map.put("UploadedBy", user_id);
                                    dbclient_response = dbClient.save(map);
                                    rev = dbclient_response.getRev();
                                }

                                dbclient_response = dbClient.saveAttachment(filePart.getInputStream(), file_name, mimeType, uuid, rev);

                                if (dbclient_response.getError() != null)
                                {
                                    throw new Exception("CouchDB attachment upload error: " + dbclient_response.getError());
                                }

                                if (new_uuid > 0)
                                {
                                    String query_avatar = "UPDATE users SET avatar = ? WHERE id = ?";
                                    PreparedStatement stmt_attachment = con.prepareStatement(query_avatar);
                                    stmt_attachment.setString(1, uuid);
                                    stmt_attachment.setString(2, id);
                                    stmt_attachment.execute();

                                    int rows_affected = stmt_attachment.executeUpdate();
                                    if (rows_affected <= 0)
                                    {
                                        throw new Exception("Avatar field is somehow not updated in the users table");
                                    }
                                }
                            }
                        } catch (Exception e)
                        {
                            logger.error("ERROR attachments process in servlet admin_users_edit:=" + e);
                        }

                    }
                }
                catch(Exception e)
                {
                    logger.error("ERROR Exception avatar saving in servlet admin_users_add:=" + e);
                }                

                String[] attachments_delete_ids = request.getParameterValues("attachment_delete_id");
                if (attachments_delete_ids != null && attachments_delete_ids.length > 0)
                {
                    try
                    {                     
                        String query_avatar = "UPDATE users SET avatar = null WHERE id = ?";
                        PreparedStatement stmt_attachment = con.prepareStatement(query_avatar);
                        stmt_attachment.setString(1, id);
                        stmt_attachment.execute();

                        dbClient = db.couchdb.get_avatars_client(props);
                        if (dbClient == null)
                        {
                            throw new Exception("dbClient is null");
                        }
                        String uuid = attachments_delete_ids[0];
                        String rev;

                        JsonObject json = new JsonObject();
                        json = dbClient.find(JsonObject.class, uuid);        
                        rev = json.get("_rev").getAsString();
                        dbClient.remove(uuid, rev);

                    } catch (Exception e)
                    {
                        logger.error("ERROR avatar deletion in servlet admin_users_edit:=" + e);
                    }
                }

                
                //do roles now
                ArrayList <String> role_ids = new ArrayList();
                Set<String> paramNames = request.getParameterMap().keySet();
                // iterating over parameter names and get its value
                for (String name : paramNames) 
                {
                    String value = request.getParameter(name);
                    if(name.startsWith("role_id_"))
                    {
                        if(value.equalsIgnoreCase("0"))
                        {
                            has_login_role = true;
                        }
                        role_ids.add(value);
                    }
                }
                stmt = con.prepareStatement("INSERT INTO user_roles (user_id,role_id) VALUES (?,?)");
                
                for(int a = 0; a < role_ids.size(); a++)
                {
                    String role_info[] = db.get_roles.by_role_id(con, role_ids.get(a));
                    if(!role_info[19].equalsIgnoreCase("none"))
                    {
                        needs_login = true;
                    }                    
                    
                    stmt.setString(1,id);
                    stmt.setString(2,role_ids.get(a));
                    stmt.executeUpdate();
                }
                //delete all entries for this user.
                stmt = con.prepareStatement("DELETE FROM users_groups WHERE user_id=?");
                stmt.setString(1,id);
                stmt.executeUpdate();
                //add new group assignments
                //do group membership
                String groups[] = request.getParameterValues("group_ids");
                if(groups != null)
                {
                    stmt = con.prepareStatement("INSERT INTO users_groups (user_id,group_id,role,external_id) VALUES (?,?,?,?)");
                    for(int a = 0; a < groups.length; a++)
                    {
                        stmt.setString(1,id);
                        stmt.setString(2,groups[a]);
                        stmt.setString(3,"");
                        stmt.setString(4,"");                
                        stmt.executeUpdate();
                    } 
                }     
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                
                //if a shared installation then update the user table on the management server                
                if(installation_type.equalsIgnoreCase("shared"))
                {
                    if(needs_login)
                    {
                        //call the custom REST API
                        String query = "REPLACE INTO `users`" 
                                + "(`id`,"
                                + "`contract_id`," 
                                + "`username`," 
                                + "`password`," 
                                + "`first`," 
                                + "`mi`," 
                                + "`last`," 
                                + "`address_1`," 
                                + "`address_2`,"
                                + "`city`," 
                                + "`state`," 
                                + "`zip`," 
                                + "`email`," 
                                + "`phone_office`," 
                                + "`phone_mobile`," 
                                + "`notes`) " 
                                + "VALUES " 
                                + "('" + id + "'," 
                                + "'" + contract_id + "'," 
                                + "'" + username + "'," 
                                + "''," 
                                + "'" + first + "'," 
                                + "'" + mi + "'," 
                                + "'" + last + "'," 
                                + "'" + address_1 + "'," 
                                + "'" + address_2 + "'," 
                                + "'" + city + "'," 
                                + "'" + state + "'," 
                                + "'" + zip + "'," 
                                + "'" + email + "'," 
                                + "'" + phone_office + "'," 
                                + "'" + phone_mobile + "',"
                                + "'" + notes + "')";
                        boolean replace_success = db.get_management_server.run_insert(context_dir, query);
                        System.out.println("replace_success=" + replace_success);               
// seems password_hash and salt doesn't need to be sync to the management server
//                        if (replace_success && !password.equals("")) {
//                            query = "UPDATE `users` SET password_salt = UNHEX('" + Hex.encodeHexString(salt_and_hash.get(0)) +"'), password_hash = UNHEX('" + Hex.encodeHexString(salt_and_hash.get(1)) + "') "
//                                    + "WHERE `id` = '" + id + "'"; 
//                            boolean update_success = db.get_management_server.run_insert(context_dir, query);
//                            System.out.println("password_hash_update_success=" + update_success);               
//                        }

                    }//end if needs_login
                    else
                    {
                        //delete because login is not required
                        String query = "DELETE FROM `users` " 
                                + "WHERE `id`='" + id + "' AND `contract_id`='" + contract_id + "'";

                                
                        boolean delete_success = db.get_management_server.run_insert(context_dir, query);
                        System.out.println("delete_success=" + delete_success);      
                    }
                }
                stmt.close();
                con.close();
                session.setAttribute("alert", "User successfully updated");
                session.setAttribute("alert-class", "alert-success");
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_users_edit:=" + e);
                session.setAttribute("alert", "User updating error");
                session.setAttribute("alert-class", "alert-dangerNew");

            }

//            response.sendRedirect("admin_users.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

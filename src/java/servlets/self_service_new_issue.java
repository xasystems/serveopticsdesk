/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */

package servlets;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@MultipartConfig()
/**
 *
 * @author ralph
 */
public class self_service_new_issue extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
    private static SimpleDateFormat form_format = new SimpleDateFormat("HH:mm / dd-MMM-YY"); //10:42 / 24-Jan-20
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   
    private static SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();  
        Connection con = null;
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap sys_props = support.config.get_config(context_dir);
            //do work here
            String user_tz_name = "UTC";
            String user_tz_time = "00:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "UTC";
                    user_tz_time = "00:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "UTC";
                user_tz_time = "00:00";
            }  
            java.util.Date now = new java.util.Date();
            java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
            String users_tz_time = DateTimeFormatter.ofPattern("HH:mm MM/dd/yyyy").format(support.date_utils.utc_to_user_tz(user_tz_name, timestamp_format.format(now)));
            String now_utc = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
            String now_utc_date_time_picker = DateTimeFormatter.ofPattern("HH:mm MM/dd/yyyy").format(support.date_utils.now_in_utc());
            String user_id = session.getAttribute("user_id").toString();
            try
            {      
                con = db.db_util.get_contract_connection(context_dir, session);  
                String uuid = UUID.randomUUID().toString(); //Generates random UUID  
                String source = "self_service";
                String date_sent = now_utc;
                String from = "";                
                String subject = request.getParameter("subject");
                String to = "Service Desk";  
                String body = request.getParameter("body"); 
                String mime_type = "";  
                String priority = "";  
                String status = "New";  
                String status_date = now_utc;  
                String status_by_user_id = "0";  
                String result = "";  
                String has_attachment = "false";  
                String description = "";  
                String log = "";  
                String related_to = null;     
                String related_to_id = null;  
                String assigned_to_id = null;     
                String date_assigned = null;     
                String creator_email = request.getParameter("creator_email");
                String creator_phone = request.getParameter("creator_phone");
                String submitted_for_email = "";
                String submitting_for_phone = "";
                //start get the file attachment if there is one
                try
                {
                    String config_message_attachment_store = sys_props.get("message_attachment_store").toString();        
                    String customer_db = session.getAttribute("db").toString();
                    String save_attachement_path = config_message_attachment_store + "/" + customer_db;
                    File this_customer_message_attachements = new File(save_attachement_path);

                    Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
                    String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
                    long file_size = filePart.getSize();
                    String fixed_file_name = fileName.replaceAll("[^a-zA-Z0-9\\._]+", "_");
                    InputStream fileContent = filePart.getInputStream();
                    File uploadDir = new File(save_attachement_path);
                    if (!uploadDir.exists()) 
                    {
                        uploadDir.mkdir();
                    }        
                    filePart.write(save_attachement_path + File.separator + uuid);
                    has_attachment = "true"; 
                    //save attachment info to database
                    String query_attachment = "INSERT INTO message_attachements (`uuid`,`name_on_disk`,`file_name`,`file_date`,`file_size`) VALUES (?,?,?,?,?)";
                    PreparedStatement stmt_attachment = con.prepareStatement(query_attachment);
                    stmt_attachment.setString(1, uuid);
                    stmt_attachment.setString(2, uuid);
                    stmt_attachment.setString(3, fixed_file_name);                    
                    stmt_attachment.setString(4, now_utc);
                    stmt_attachment.setString(5, String.valueOf(file_size));
                    stmt_attachment.execute();                    
                }
                catch(Exception e)
                {
                    has_attachment = "false"; 
                }
                //end get the file attachment if there is one
                
                boolean for_someone_else = false; //checkbox
                if(request.getParameter("for_someone_else") == null)
                {
                    for_someone_else = false;
                }
                else
                {
                    for_someone_else = true;
                    submitted_for_email = request.getParameter("submitted_for_email");
                    submitting_for_phone = request.getParameter("submitting_for_phone");
                }  
                
                if(for_someone_else)  
                {
                    from = submitted_for_email;
                    subject =  subject + ". (Self Service Portal New Issue.) Submitted By Email: " + creator_email + " Phone:" +  creator_phone + " For Email:" + submitted_for_email + " Phone:" + submitting_for_phone + ".";
                    body = body + "   Self Service Portal New Issue. Submitted By Email: " + creator_email + " Phone:" +  creator_phone + " For Email:" + submitted_for_email + " Phone:" + submitting_for_phone + ".";
                    log = "Date/Time:" + users_tz_time + "<br>Status:" + status + "<br>Source:" + source + "<br>Subject:" + subject + "<br>Body:" + body + "<br>-------------------------------------------------------------------------";
                }
                else
                {
                    from = creator_email;
                    subject = subject + " (Self Service Portal New Issue.)";
                    body = body;
                    log = "Date/Time:" + users_tz_time  + "<br>Status:" + status + "<br>Source:" + source + "<br>Subject:" + subject + "<br>Body:" + body + "<br>-------------------------------------------------------------------------";
                }
                
                String query = "";
                //query = "INSERT INTO message (`uuid`,`source`,`date_sent`,`from`,`subject`,`to`,`body`,`mime_type`,`status`,`status_date`,`status_by_user_id`,`log`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
                query = "INSERT INTO message (`uuid`,`source`,`date_sent`,`from`,`subject`,`to`,`body`,`mime_type`,`priority`,`status`,`status_date`,`status_by_user_id`,`result`,`has_attachment`,`description`,`log`,`related_to`,`related_to_id`,`assigned_to_id`,`date_assigned`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                PreparedStatement stmt = con.prepareStatement(query);
                stmt.setString(1, uuid);
                stmt.setString(2, source);
                stmt.setString(3, date_sent);
                stmt.setString(4, from);
                stmt.setString(5, subject);
                stmt.setString(6, to);
                stmt.setString(7, body);
                stmt.setString(8, mime_type);
                stmt.setString(9, priority);
                stmt.setString(10, status);
                stmt.setString(11, status_date);
                stmt.setString(12, status_by_user_id);
                stmt.setString(13, result);
                stmt.setString(14, has_attachment);
                stmt.setString(15, description);
                stmt.setString(16, StringEscapeUtils.escapeHtml4(log));
                stmt.setString(17, related_to);
                stmt.setString(18, related_to_id);
                stmt.setString(19, assigned_to_id);
                stmt.setString(20, date_assigned);
                //System.out.println("q=" + stmt);
                stmt.execute();                
                con.close();
                String RedirectURL = "home_self_service.jsp?result=Success";
                response.sendRedirect(RedirectURL);                  
            }
            catch(Exception e)
            {
                logger.error("ERROR Exception in servlet self_service_follow:=" + e);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

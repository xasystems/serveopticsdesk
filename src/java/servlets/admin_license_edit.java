/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class admin_license_edit extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        int is_valid = -1;
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        try 
        {
            String license = request.getParameter("license");
            is_valid = support.util.validate_license(context_dir, license);
            if(is_valid != -1)
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String delete = "DELETE FROM system_license";
                PreparedStatement stmt = con.prepareStatement(delete);
                stmt.executeUpdate();

                String insert = "INSERT INTO system_license (license) VALUES (?)";
                stmt = con.prepareStatement(insert);
                stmt.setString(1, license);
                stmt.executeUpdate();
                stmt.close();
                con.close();
            }
        }
        catch (Exception e) 
        {
            logger.error("ERROR Exception in servlet admin_license_add:=" + e);
        }
        if(is_valid != -1)
        {
            response.sendRedirect("admin_license_edit.jsp?message=License is valid.");
        }
        else
        {
            response.sendRedirect("admin_license_edit.jsp?error=License is INVALID.");
        }  
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

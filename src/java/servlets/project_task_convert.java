/*
 * Copyright 2019 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class project_task_convert extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
    private static SimpleDateFormat form_format = new SimpleDateFormat("HH:mm / dd-MMM-YY"); //10:42 / 24-Jan-20
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            java.util.Date now = new java.util.Date();
            java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
            try
            {                
                String context_dir = request.getServletContext().getRealPath("");
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String job_id = request.getParameter("job_id");        
                int project_id = 0;
                try{project_id = Integer.parseInt(request.getParameter("project_id"));}catch (Exception e){project_id=0;}
                int id = db.get_projects.next_project_task_id_for_project(con, project_id);
                String name = "";
                try{name = request.getParameter("name");}catch (Exception e){name="";}
                String description = "";
                try{description = request.getParameter("description");}catch (Exception e){description="";}
                int assigned_group_id = 0;
                try{assigned_group_id = Integer.parseInt(request.getParameter("assigned_group_id"));}catch (Exception e){assigned_group_id=0;}
                int assigned_to_id = 0;
                try{assigned_to_id = Integer.parseInt(request.getParameter("assigned_to_id"));}catch (Exception e){assigned_to_id=0;}
                String priority = "";
                try{priority = request.getParameter("priority");}catch (Exception e){priority="";}
                String status = "";
                try{status = request.getParameter("status");}catch (Exception e){status="";}
                java.sql.Timestamp scheduled_start_date = null;
                try
                {
                    scheduled_start_date = new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("scheduled_start_date")).getTime());
                }
                catch(Exception e)
                {
                    scheduled_start_date = null;
                }
                java.sql.Timestamp actual_start_date = null;
                try
                {
                    actual_start_date = new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("actual_start_date")).getTime());
                }
                catch(Exception e)
                {
                    actual_start_date = null;
                }
                java.sql.Timestamp scheduled_end_date = null;
                try
                {
                    scheduled_end_date = new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("scheduled_end_date")).getTime());
                }
                catch(Exception e)
                {
                    scheduled_end_date = null;
                }
                java.sql.Timestamp actual_end_date = null;
                try
                {
                    actual_end_date = new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("actual_end_date")).getTime());
                }
                catch(Exception e)
                {
                    actual_end_date = null;
                }
                int estimated_duration = 0;
                try{estimated_duration = Integer.parseInt(request.getParameter("estimated_duration"));}catch (Exception e){estimated_duration=0;}
                int actual_duration = 0;
                try{actual_duration = Integer.parseInt(request.getParameter("actual_duration"));}catch (Exception e){actual_duration=0;}
                String notes = "";
                try{notes = request.getParameter("notes");}catch (Exception e){notes="";}
                
                String insert_project_tasks_table = "INSERT INTO project_tasks (id,project_id,name,description,assigned_group_id,assigned_to_id,priority,status,scheduled_start_date,actual_start_date,scheduled_end_date,actual_end_date,estimated_duration,actual_duration,notes) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                PreparedStatement stmt = con.prepareStatement(insert_project_tasks_table);
                stmt.setInt(1, id); //id
                stmt.setInt(2, project_id); //project_id
                stmt.setString(3,name); //name
                stmt.setString(4,description); //description
                stmt.setInt(5, assigned_group_id); //assigned_group_id
                stmt.setInt(6, assigned_to_id); //assigned_to_id
                stmt.setString(7,priority); //priority
                stmt.setString(8,status); //status
                stmt.setTimestamp(9,scheduled_start_date); //scheduled_start_date
                stmt.setTimestamp(10,actual_start_date); //actual_start_date
                stmt.setTimestamp(11,scheduled_end_date); //scheduled_end_date
                stmt.setTimestamp(12,actual_end_date); //actual_end_date
                stmt.setInt(13, estimated_duration); //estimated_duration
                stmt.setInt(14, actual_duration); //actual_duration
                stmt.setString(15,notes); //notes 
                stmt.execute();
                
                //delete the old job
                String delete_job_query = "DELETE FROM jobs WHERE id=?";
                stmt = con.prepareStatement(delete_job_query);
                stmt.setString(1, job_id); //id
                stmt.execute();                
                
                String RedirectURL = "project.jsp?project_id=" + project_id ;
                response.sendRedirect(RedirectURL);
            }
            catch(Exception e)
            {
                logger.error("ERROR Exception in servlet project_task_convert:=" + e);
            } 
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

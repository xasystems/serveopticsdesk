/*
 * Copyright 2019 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class admin_groups_add extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                int group_id = db.get_groups.next_id(con);
                String name = request.getParameter("name");
                String description = request.getParameter("description");
                String poc_id = request.getParameter("poc_id");                
                String external_id = "";
                PreparedStatement stmt = con.prepareStatement("INSERT INTO `groups` (id,name,description,poc_id,external_id) VALUES (?,?,?,?,?)");

                stmt.setInt(1,group_id);
                stmt.setString(2,name);
                stmt.setString(3,description);
                stmt.setString(4,poc_id);
                stmt.setString(5,external_id);
                stmt.execute();
                
                //get group members
                String user_ids[] = request.getParameterValues("user_ids");
                stmt = con.prepareStatement("INSERT INTO users_groups (user_id,group_id,role,external_id) VALUES (?,?,?,?)");
                for(int a = 0; a < user_ids.length; a++)
                {
                    stmt.setString(1,user_ids[a]);
                    stmt.setInt(2,group_id);
                    stmt.setString(3,"");
                    stmt.setString(4,"");                    
                    //System.out.println("stmt=" + stmt);
                    stmt.executeUpdate();
                }  
                stmt.close();
                con.close();
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_groups_add:=" + e);
            }
            response.sendRedirect("admin_groups.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.reflect.Array.set;
import java.sql.Connection;
import java.util.HashSet;
import java.util.Set;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
@WebServlet("/ajax_lookup_chat_room")
public class ajax_lookup_chat_room extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
//        String user_id = request.getParameter("user_id");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

//        String user_id = request.getParameter("user_id");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        JsonObjectBuilder json_builder = Json.createObjectBuilder();
        JsonObject json;
        
        boolean first_record = true;
        boolean has_data = false;
        try (PrintWriter out = response.getWriter()) 
        {

            try
            {

                if (session.getAttribute("authenticated") == null) 
                {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                    return;
                }
                logger.info("context dir {} ", context_dir);
                Connection con = db.db_util.get_contract_connection(context_dir, session);

                String user_id = session.getAttribute("user_id").toString();
                String user_tz_name = session.getAttribute("tz_name").toString();
                int session_timeout = NumberUtils.toInt(session.getAttribute("session_timeout").toString(), 0);
                String object_type = StringUtils.defaultString(request.getParameter("object_type"));
                String parent_object_type = StringUtils.defaultString(request.getParameter("parent_object_type"));
                String object_id = StringUtils.defaultString(request.getParameter("object_id"));
                String parent_object_id = StringUtils.defaultString(request.getParameter("parent_object_id"));
                
                boolean update_session_table = db.get_user_sessions.update(con, user_id);

                String[] room_object;
                String room_name = "";
//                String[] room_users = {};
                String[] chat_room_users;

                chat_room_users = db.get_chat_messages.chat_room_users(con, object_type, object_id, parent_object_type, parent_object_id);
                Set<String> room_users = new HashSet<String>();

                switch (object_type) {
                    case "project":
                        room_object = db.get_projects.project_by_id(con, object_id);
                        room_name = room_object[1];
                        String[] project_users = new String[]{room_object[4], room_object[5], room_object[6]}; // owner_id, manager_id, tech_lead_id
                        for (String project_user : project_users)
                        {
                            if (NumberUtils.toInt(project_user, 0) > 0)
                            {
                                room_users.add(project_user);
                            }
                        }
                        break;
                    case "task":
                        room_object = db.get_projects.project_task_info(con, parent_object_id, object_id);
                        room_name = room_object[2];
                        break;
                    case "incident":
                        room_object = db.get_incidents.incident_by_id(con, object_id);
                        room_name = room_object[15];
                        break;
                    case "problem":
                        room_object = db.get_problems.problem_by_id(con, object_id);
                        room_name = room_object[2];
                        break;
                    case "article":
                        room_object = db.get_kb.article_info_for_id(con, object_id);
                        room_name = room_object[2];
                        break;
                    default:
                        break;
                }
                room_name = StringUtils.capitalize(object_type) + (room_name.equals("") ? "" : ": " + room_name);
                JsonArrayBuilder room_users_json_array = Json.createArrayBuilder();
                JsonArrayBuilder menu_actions_json_array = Json.createArrayBuilder();
                for (String room_user_id : room_users)
                {
                    if (room_user_id != null && !room_user_id.equals(""))
                    {
                        String[] room_user = db.get_users.by_id(con, room_user_id);
                        int room_user_active = db.get_user_sessions.active_for_user_id(con, session_timeout, room_user_id);
                        String user_status = (room_user_active > 0 ? "Online" : "Offline");
                        String room_user_last_active = db.get_user_sessions.last_active(con, room_user_id, user_tz_name);

                        room_users_json_array
                            .add(Json.createObjectBuilder()
                        //        _id: 4321,
                        //        username: 'John Snow',
                        //        avatar: 'assets/imgs/snow.png',
                        //        status: {
                        //          state: 'offline',
                        //          lastChanged: '14 July, 20:00'
                        //        }
                                .add("_id", Integer.parseInt(room_user_id))
                                .add("username", room_user[3] + " " + room_user[5])
                                .add("avatar", (room_user[24] != null && !room_user[24].equals("") ? "get_avatar?uuid=" + room_user[24] : ""))
                                .add("status", Json.createObjectBuilder()
                                    .add("state", user_status)
                                    .add("lastChanged", room_user_last_active)
                                )
                        );
                        menu_actions_json_array
                            .add(Json.createObjectBuilder()
                                .add("name", Integer.parseInt(room_user_id))
                                .add("title", "<div class=\"vac-avatar\""
                                    + (user_status.equals("Offline") && !room_user_last_active.equals("") ? " title=\"since " + room_user_last_active + "\"": "")
                                    + (room_user[24] != null && !room_user[24].equals("") ? " style=\"background-image: url(&quot;get_avatar?uuid=" + room_user[24] + "&quot;);\"" : "") 
                                    + "></div>" + room_user[3] + " " + room_user[5] + " - " + user_status)
                        );
                    }
                }
                JsonArrayBuilder rooms_array_builder = Json.createArrayBuilder();
                rooms_array_builder
                    .add(Json.createObjectBuilder()
                        .add("roomId", object_type + "/" + object_id+ "/" + parent_object_type + "/" + parent_object_id)
                        .add("roomName", room_name)
                        .add("avatar", "")
                        .add("unreadCount", 0)
                        .add("index", 0)
                        .add("lastMessage", Json.createObjectBuilder()
//                            .add("content", "Last message received")
//                            .add("senderId", 1234)
//                            .add("username", "John Doe")
//                            .add("timestamp", "2019-02-03T06:48:07")
//                            .add("saved", true)
//                            .add("distributed", false)
//                            .add("seen", false)
//                            .add("new", true)
                        )
                        .add("users", room_users_json_array)
                        .add("menuActions", menu_actions_json_array)
                        .add("typingUsers", Json.createArrayBuilder()
//                            .add(1)
                        )
                    );

                JsonArray result = rooms_array_builder.build();

                con.close();
                JsonWriter writer = Json.createWriter(out);
                writer.writeArray(result);
                writer.close();
                //send the JSON data

                // users
//                JsonArrayBuilder users_array_builder = Json.createArrayBuilder();
//                users_array_builder
//                    .add(Json.createObjectBuilder()
//                        .add("_id", room_object[4])
//                        .add("username", support.string_utils.check_for_null(attachment[1]))
//                        .add("avatar", support.string_utils.check_for_null(attachment[2]))
//                        .add("status", Json.createObjectBuilder()
//                            .add("state", "online")
//                            .add("lastChanged", "2013-10-21T13:28:06.419Z")
//                        )
//                        .add("href", project_task_attachments_folder_symlink + "/" + support.string_utils.check_for_null(attachment[4]))
//                        .add("file_name", support.string_utils.check_for_null(attachment[5]))
//                        .add("file_date", support.string_utils.check_for_null(attachment[6]))
//                        .add("file_size", support.string_utils.check_for_null(attachment[7]))
//                    );


                //[
                //  {
                //    "roomId": "project/1/task/1",
                //    "roomName": "Room 1",
                //    "avatar": "assets/imgs/people.png",
                //    "unreadCount": 0,
                //    "index": 0,
                //    "lastMessage": {
                //      "content": "Last message received",
                //      "senderId": 1234,
                //      "username": "John Doe",
                //      "timestamp": "10:20",
                //      "saved": true,
                //      "distributed": false,
                //      "seen": false,
                //      "new": true
                //    },
                //    "users": [
                //      {
                //        "_id": 1234,
                //        "username": "John Doe",
                //        "avatar": "assets/imgs/doe.png",
                //        "status": {
                //          "state": "online",
                //          "lastChanged": "today, 14:30"
                //        }
                //      },
                //      {
                //        "_id": 4321,
                //        "username": "John Snow",
                //        "avatar": "assets/imgs/snow.png",
                //        "status": {
                //          "state": "offline",
                //          "lastChanged": "14 July, 20:00"
                //        }
                //      }
                //    ],
                //    "typingUsers": [
                //    ]
                //  }
                //]

            }
            catch(Exception e)
            {
                e.printStackTrace();
                logger.error("ERROR Exception in servlet ajax_lookup_project_task_rooms:=" + e);
                stringBuilder.setLength(0);
                stringBuilder.append("Error in retrieving tasks list");
                out.print(stringBuilder.toString());

            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

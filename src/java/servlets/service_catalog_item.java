/*
<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;
import org.apache.commons.text.StringEscapeUtils;
/**
 *
 * @author rcampbell
 */
public class service_catalog_item extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
    private static SimpleDateFormat form_format = new SimpleDateFormat("HH:mm / dd-MMM-YY"); //10:42 / 24-Jan-20
    
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        timestamp_format.setTimeZone(TimeZone.getTimeZone("UTC"));
        HttpSession session = request.getSession();
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            String user_tz_name = "US/Eastern";
            String user_tz_time = "-05:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "US/Eastern";
                    user_tz_time = "-05:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "US/Eastern";
                user_tz_time = "-05:00";
            }  
            String user_id = session.getAttribute("user_id").toString();
            String rev_by_user_id = user_id;
            String create_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(support.date_utils.now_in_utc());
            String request_date = create_date;
            String rev_date = create_date;
            int rev = 0;    
            try
            {                
                String context_dir = request.getServletContext().getRealPath("");
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                //get all the parameters
                int request_id = db.get_requests.next_request_id(con);
                String service_catalog_item_id = request.getParameter("catalog_item_id");
                //get requested_item info
                String requested_item_info[] = db.get_service_catalog.service_catalog_items_by_id(con, service_catalog_item_id);  
                String service_catalog_id = requested_item_info[1];
                String priority = request.getParameter("priority");
                //cal due date
                //////////////////////////////////////////////////////////////////////////////////////
                //Get due date
                String due_date = support.sla_request_calc.due_date_for_request_id(con, service_catalog_item_id, priority, support.date_utils.now_in_utc());
                
                int amount = 0;
                String unit = "";
                String assigned_group_id = "0";
                Double price = 0.00;
                if(priority.equalsIgnoreCase("Critical"))
                {
                    assigned_group_id = requested_item_info[13];
                    price = Double.parseDouble(requested_item_info[7]);
                }
                else if(priority.equalsIgnoreCase("High"))
                {
                    assigned_group_id = requested_item_info[20];
                    price = Double.parseDouble(requested_item_info[14]);
                }
                else if(priority.equalsIgnoreCase("Medium"))
                {
                    assigned_group_id = requested_item_info[27];
                    price = Double.parseDouble(requested_item_info[21]);
                }
                else if(priority.equalsIgnoreCase("Low"))
                {
                    assigned_group_id = requested_item_info[34];
                    price = Double.parseDouble(requested_item_info[28]);
                }
                String resolve_date = null;
                String closed_date = null;
                String assigned_to_id = "0";
                String create_by_id = user_id;
                //get requested_for_id
                String requested_for_id = "0";
                if(request.getParameter("requested_for_id").equalsIgnoreCase(""))
                {
                    requested_for_id = create_by_id;
                }
                else
                {
                    requested_for_id = request.getParameter("requested_for_id");
                }
                String requested_for_group_id = null;
                //get requested_for user info to get location and such
                String requested_for_user_info[] = db.get_users.by_id(con, requested_for_id);
                String location = requested_for_user_info[12];
                String department = requested_for_user_info[11];
                String site = "";
                String company = "";
                String impact = null;
                String urgency = null;
                //get approval state
                String state = "Pending_Approval";
                String approval = "Pending";
                try
                {
                    JsonReader reader = Json.createReader(new StringReader(requested_item_info[6]));
                    //System.out.println("service_catalog_item_info[5]=" + service_catalog_item_info[5]);
                    JsonObject jsonObject = reader.readObject();
                    //System.out.println("jsonObject=" + jsonObject);                                            
                    JsonObject approversObject = jsonObject.getJsonObject("approvers");
                    //System.out.println("approversObject=" + approversObject);   
                    JsonArray approver_array = (JsonArray) approversObject.getJsonArray("approver");
                    //System.out.println("approver_array=" + approver_array); 
                    for(int a = 0; a < approver_array.size(); a++)
                    {
                        JsonObject this_record = approver_array.getJsonObject(a); //a single record
                        String this_record_id = this_record.getString("id");
                        String this_record_type = this_record.getString("type");
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
                        //AUTO
                        if(this_record_type.equalsIgnoreCase("auto"))
                        {
                            state = "Approved";
                            approval = "Approved";
                        }
                    }
                }
                catch(Exception e)
                {
                    System.out.println("Exception ub service_catalog_item servlet:=" + e);
                }
                String contact_method = "Self-Service"; 
                String notes = "";
                String desk_notes = "";
                String description = requested_item_info[2] + " -- " + requested_item_info[3];
                String closed_reason = null;
                String required_info = requested_item_info[31]; //escaped
                String required_info_response = request.getParameter("required_info_response");
                String optional_info = requested_item_info[32]; //escaped
                String optional_info_response = request.getParameter("optional_info_response");   
//////////////////////////////////////////////////////////////////////////////////////
                String query = "INSERT INTO request (id,rev,rev_date,rev_by_user_id,service_catalog_id,service_catalog_item_id,create_date,request_date,due_date,resolve_date,closed_date,assigned_group_id,assigned_to_id,create_by_id,requested_for_id,requested_for_group_id,location,department,site,company,impact,urgency,priority,state,contact_method,approval,price,notes,desk_notes,description,closed_reason,required_info,required_info_response,optional_info,optional_info_response) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                PreparedStatement stmt = con.prepareStatement(query);
                stmt.setInt(1, request_id); //id
                stmt.setInt(2, rev); //rev
                stmt.setString(3, rev_date);
                stmt.setString(4, rev_by_user_id); //rev_by_user_id
                stmt.setString(5, service_catalog_id); //service_catalog_item_id
                stmt.setString(6, service_catalog_item_id); //service_catalog_item_id
                stmt.setString(7, create_date); //create_date
                stmt.setString(8, request_date); //request_date
                stmt.setString(9, due_date); //due_date
                stmt.setString(10, resolve_date); //resolve_date
                stmt.setString(11, closed_date); //closed_date
                stmt.setString(12, assigned_group_id); //assigned_group_id
                stmt.setString(13, assigned_to_id); //assigned_to_id
                stmt.setString(14, create_by_id); //create_by_id
                stmt.setString(15, requested_for_id); //requested_for_id
                stmt.setString(16, requested_for_group_id); //requested_for_group_id
                stmt.setString(17, location); //location
                stmt.setString(18, department); //department
                stmt.setString(19, site); //site
                stmt.setString(20, company); //company
                stmt.setString(21, impact); //impact
                stmt.setString(22, urgency); //urgency
                stmt.setString(23, priority); //priority
                stmt.setString(24, state); //state
                stmt.setString(25, contact_method); //contact_method
                stmt.setString(26, approval); //approval
                stmt.setDouble(27, price); //price
                stmt.setString(28, notes); //notes
                stmt.setString(29, desk_notes); //desk_notes
                stmt.setString(30, description); //description
                stmt.setString(31, closed_reason); //closed_reason
                stmt.setString(32, required_info); //required_info
                stmt.setString(33, required_info_response); //required_info_response
                stmt.setString(34, optional_info); //required_info
                stmt.setString(35, optional_info_response); //required_info_response
                stmt.execute();
                //save the request to the history table
                query = "INSERT INTO request_history (id,rev,rev_date,rev_by_user_id,service_catalog_id,service_catalog_item_id,create_date,request_date,due_date,resolve_date,closed_date,assigned_group_id,assigned_to_id,create_by_id,requested_for_id,requested_for_group_id,location,department,site,company,impact,urgency,priority,state,contact_method,approval,price,notes,desk_notes,description,closed_reason,required_info,required_info_response,optional_info,optional_info_response) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                stmt = con.prepareStatement(query);
                stmt.setInt(1, request_id); //id
                stmt.setInt(2, rev); //rev
                stmt.setString(3, rev_date);
                stmt.setString(4, rev_by_user_id); //rev_by_user_id
                stmt.setString(5, service_catalog_id); //service_catalog_item_id
                stmt.setString(6, service_catalog_item_id); //service_catalog_item_id
                stmt.setString(7, create_date); //create_date
                stmt.setString(8, request_date); //request_date
                stmt.setString(9, due_date); //due_date
                stmt.setString(10, resolve_date); //resolve_date
                stmt.setString(11, closed_date); //closed_date
                stmt.setString(12, assigned_group_id); //assigned_group_id
                stmt.setString(13, assigned_to_id); //assigned_to_id
                stmt.setString(14, create_by_id); //create_by_id
                stmt.setString(15, requested_for_id); //requested_for_id
                stmt.setString(16, requested_for_group_id); //requested_for_group_id
                stmt.setString(17, location); //location
                stmt.setString(18, department); //department
                stmt.setString(19, site); //site
                stmt.setString(20, company); //company
                stmt.setString(21, impact); //impact
                stmt.setString(22, urgency); //urgency
                stmt.setString(23, priority); //priority
                stmt.setString(24, state); //state
                stmt.setString(25, contact_method); //contact_method
                stmt.setString(26, approval); //approval
                stmt.setDouble(27, price); //price
                stmt.setString(28, notes); //notes
                stmt.setString(29, desk_notes); //desk_notes
                stmt.setString(30, description); //description
                stmt.setString(31, closed_reason); //closed_reason
                stmt.setString(32, required_info); //required_info
                stmt.setString(33, required_info_response); //required_info_response
                stmt.setString(34, optional_info); //required_info
                stmt.setString(35, optional_info_response); //required_info_response
                stmt.execute();
                
                //do global notification
                //request_created_by_create
                String created_by_request_create[] = db.get_notifications.specific_global(con, "request_created_by_create");
                if(created_by_request_create[2].equalsIgnoreCase("true"))
                {
                    //get subject
                    String request_subject_format = StringEscapeUtils.unescapeHtml4(created_by_request_create[3]);
                    String subject = support.format_email.text(con, "request", String.valueOf(request_id), request_subject_format,user_tz_time);
                    //get body
                    String request_body_format = StringEscapeUtils.unescapeHtml4(created_by_request_create[4]);
                    String body = support.format_email.text(con, "request", String.valueOf(request_id), request_body_format, user_tz_time);

                    String user_info[] = db.get_users.by_id(con, String.valueOf(create_by_id));
                    String email = user_info[15];
                    if(!email.equalsIgnoreCase(""))
                    {
                        //System.out.println("Sending to emailer SUBJECT=" + subject + " BODY=" + body);
                        boolean email_status = db.save_pending_email.now(con, email, subject, body);
                    }
                }            
                //request_requester_create
                String requester_request_create[] = db.get_notifications.specific_global(con, "request_requester_create");
                //System.out.println("requester_request_create length=" + requester_request_create.length);
                if(requester_request_create[2].equalsIgnoreCase("true"))
                {
                    //System.out.println("Request Create Requester START");
                    //get subject
                    String request_subject_format = StringEscapeUtils.unescapeHtml4(requester_request_create[3]);
                    String subject = support.format_email.text(con, "request", String.valueOf(request_id), request_subject_format, user_tz_time);
                    //get body
                    String request_body_format = StringEscapeUtils.unescapeHtml4(requester_request_create[4]);
                    String body = support.format_email.text(con, "request", String.valueOf(request_id), request_body_format, user_tz_time);

                    String user_info[] = db.get_users.by_id(con, String.valueOf(requested_for_id));
                    String email = user_info[15];
                    if(!email.equalsIgnoreCase(""))
                    {
                        //System.out.println("Sending to emailer SUBJECT=" + subject + " BODY=" + body);
                        boolean email_status = db.save_pending_email.now(con, email, subject, body);
                    }
                    //System.out.println("Request Create Requester END");
                } 
                String RedirectURL = "request_list.jsp";
                response.sendRedirect(RedirectURL);                
            }
            catch(Exception e)
            {
                logger.error("ERROR Exception in servlet service_catalog_item:=" + e);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

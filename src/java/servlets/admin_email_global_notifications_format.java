/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */

package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author rcampbell
 */
public class admin_email_global_notifications_format extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
    

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            String incident_create_subject = StringEscapeUtils.escapeHtml4(request.getParameter("incident_create_subject"));
            String incident_create_body = StringEscapeUtils.escapeHtml4(request.getParameter("incident_create_body"));
            String incident_update_subject = StringEscapeUtils.escapeHtml4(request.getParameter("incident_update_subject"));
            String incident_update_body = StringEscapeUtils.escapeHtml4(request.getParameter("incident_update_body"));
            String incident_resolve_subject = StringEscapeUtils.escapeHtml4(request.getParameter("incident_resolve_subject"));
            String incident_resolve_body = StringEscapeUtils.escapeHtml4(request.getParameter("incident_resolve_body"));
            String incident_close_subject = StringEscapeUtils.escapeHtml4(request.getParameter("incident_close_subject"));
            String incident_close_body = StringEscapeUtils.escapeHtml4(request.getParameter("incident_close_body"));
            String request_create_subject = StringEscapeUtils.escapeHtml4(request.getParameter("request_create_subject"));
            String request_create_body = StringEscapeUtils.escapeHtml4(request.getParameter("request_create_body"));
            String request_update_subject = StringEscapeUtils.escapeHtml4(request.getParameter("request_update_subject"));
            String request_update_body = StringEscapeUtils.escapeHtml4(request.getParameter("request_update_body"));
            String request_resolve_subject = StringEscapeUtils.escapeHtml4(request.getParameter("request_resolve_subject"));
            String request_resolve_body = StringEscapeUtils.escapeHtml4(request.getParameter("request_resolve_body"));
            String request_close_subject = StringEscapeUtils.escapeHtml4(request.getParameter("request_close_subject"));
            String request_close_body = StringEscapeUtils.escapeHtml4(request.getParameter("request_close_body"));
            
            String query = "UPDATE notifications_global SET subject=?,body=? WHERE notify_on=?";
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement(query);
                
                stmt.setString(1, incident_create_subject);
                stmt.setString(2, incident_create_body);
                stmt.setString(3, "caller_incident_create");
                System.out.println("caller_incident_create=" + stmt);
                stmt.execute();
                
                stmt.setString(1, incident_update_subject);
                stmt.setString(2, incident_update_body);
                stmt.setString(3, "caller_incident_update");
                stmt.execute();
                
                stmt.setString(1, incident_resolve_subject);
                stmt.setString(2, incident_resolve_body);
                stmt.setString(3, "caller_incident_resolve");
                stmt.execute();
                
                stmt.setString(1, incident_close_subject);
                stmt.setString(2, incident_close_body);
                stmt.setString(3, "caller_incident_close");
                stmt.execute();
                
                stmt.setString(1, request_create_subject);
                stmt.setString(2, request_create_body);
                stmt.setString(3, "request_created_by_create");
                stmt.execute();
                
                stmt.setString(1, request_update_subject);
                stmt.setString(2, request_update_body);
                stmt.setString(3, "request_created_by_update");
                stmt.execute();
                
                stmt.setString(1, request_resolve_subject);
                stmt.setString(2, request_resolve_body);
                stmt.setString(3, "request_created_by_resolve");
                stmt.execute();
                
                stmt.setString(1, request_close_subject);
                stmt.setString(2, request_close_body);
                stmt.setString(3, "request_created_by_close");
                stmt.execute();
                
                stmt.setString(1, request_create_subject);
                stmt.setString(2, request_create_body);
                stmt.setString(3, "request_requester_create");
                stmt.execute();
                
                stmt.setString(1, request_update_subject);
                stmt.setString(2, request_update_body);
                stmt.setString(3, "request_requester_update");
                stmt.execute();
                
                stmt.setString(1, request_resolve_subject);
                stmt.setString(2, request_resolve_body);
                stmt.setString(3, "request_requester_resolve");
                stmt.execute();
                
                stmt.setString(1, request_close_subject);
                stmt.setString(2, request_close_body);
                stmt.setString(3, "request_requester_close");
                stmt.execute();
                
                stmt.close();
                con.close();
            }
            catch (Exception e) 
            {
                System.out.println("ERROR Exception in servlet admin_email_notifications:=" + e);
            }
        }
        response.sendRedirect("admin_email.jsp?success=true");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

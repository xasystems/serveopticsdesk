/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebServlet("/ajax_lookup_messages")
/**
 *
 * @author ralph
 */
public class ajax_lookup_messages extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
//        String user_id = request.getParameter("user_id");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

//        String user_id = request.getParameter("user_id");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        JsonObjectBuilder json_builder = Json.createObjectBuilder();
        JsonObject json;
        
        boolean first_record = true;
        boolean has_data = false;
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {

                if (session.getAttribute("authenticated") == null) 
                {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }

                Connection con = db.db_util.get_contract_connection(context_dir, session);
                int starting_record = 0;
                int previous_record = 0;
                int next_record = 0;
                int fetch_size = 50;
                
                try
                {
                    starting_record  = Integer.parseInt(request.getParameter("start"));
                    fetch_size  = Integer.parseInt(request.getParameter("length"));
                    next_record = starting_record + fetch_size;
                    previous_record = starting_record - fetch_size;
                    if(previous_record <= 0)
                    {
                        previous_record = 0;
                    }
                }
                catch(Exception e)
                {
                    starting_record = 0;
                    previous_record = 0;
                    next_record = starting_record + fetch_size;
                }

                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
//                String all_users_info[][] = db.get_users.all_limited(con,starting_record,fetch_size);
                String user_tz_name = "UTC";
                String user_tz_time = "+00:00";
                try
                {
                    user_tz_name = session.getAttribute("tz_name").toString();
                    user_tz_time = session.getAttribute("tz_time").toString();
                    if(user_tz_name == null || user_tz_time == null )
                    {
                        user_tz_name = "UTC";
                        user_tz_time = "+00:00";
                    }

                }
                catch(Exception e)
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }
                String home_page = session.getAttribute("home_page").toString();
                SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
                SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
                SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020            
                SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
                String timeframe = request.getParameter("timeframe");
                GregorianCalendar start_cal = new GregorianCalendar();
                GregorianCalendar end_cal = new GregorianCalendar();
                ArrayList<String[]> messages = new ArrayList();
                String status = StringUtils.defaultString(request.getParameter("status"));

                if(timeframe == null)
                {
//                    timeframe = "1Days";
                    timeframe = "all_time";
                }
                int total_filtered_count = 0;
                if(timeframe.equalsIgnoreCase("1Days"))
                {
                    start_cal.add(start_cal.DATE, -1);
                }
                else if(timeframe.equalsIgnoreCase("7Days"))
                {
                    start_cal.add(start_cal.DATE, -7);
                }
                else if(timeframe.equalsIgnoreCase("30Days"))
                {
                    start_cal.add(start_cal.DATE, -30);
                }
                else if(timeframe.equalsIgnoreCase("60Days"))
                {
                    start_cal.add(start_cal.DATE, -60);
                }
                else if(timeframe.equalsIgnoreCase("90Days"))
                {
                    start_cal.add(start_cal.DATE, -90);
                }
                else if(timeframe.equalsIgnoreCase("365Days"))
                {
                    start_cal.add(start_cal.DATE, -365);
                }

                String filter = "";
                if(!timeframe.equalsIgnoreCase("all_time"))
                {
                    String start = timestamp_format.format(start_cal.getTime());
                    String end = timestamp_format.format(end_cal.getTime());
                    if (!filter.equals("")) {
                        filter += " AND ";
                    }
                    filter += " (date_sent > " + start + " AND date_sent < " + end + ")";
                }
                total_filtered_count = db.get_message.message_count_for_filter(con, filter);
                int number_of_new_messages;
                int number_of_open_messages;
                int number_of_all_messages;
                number_of_new_messages = db.get_message.message_count_for_filter(con, filter + (!filter.equals("") ? " AND " : "") + " status = 'New'");
                number_of_open_messages = db.get_message.message_count_for_filter(con, filter + (!filter.equals("") ? " AND " : "") + " (status='Open' OR status='INW' OR status='Assigned')");
                number_of_all_messages = db.get_message.message_count_for_filter(con, filter);
                
                if (!status.equals("all")) {
                    if (!filter.equals("")) {
                        filter += " AND ";
                    }
                    if (status.equals("new")) {
                        filter += " status = 'New'";
                    } else if (status.equals("open")) {
                        filter += " (status='Open' OR status='INW' OR status='Assigned')";
                    }
                }
                String order_column_num = StringUtils.defaultString(request.getParameter("order[0][column]"));
                String order_dir = StringUtils.defaultString(request.getParameter("order[0][dir]"));
                String order_column_name = "";                
                String[] permitted_sort_fields = {"date_sent", "from", "subject", "body", "has_attachment"};
                if (!order_column_num.equals(""))
                {
                    order_column_name = StringUtils.defaultString(request.getParameter("columns[" + order_column_num + "][name]"));
                    if (!Arrays.stream(permitted_sort_fields).anyMatch(order_column_name::equals))
                    {
                        order_column_name = "";
                    } else {
                        switch (order_dir) {
                            case "asc":
                                break;
                            case "desc":
                                break;
                            default:
                                order_dir = "";
                        }
                    }
                    order_column_name = "`" + order_column_name + "`";
                    
                }

                messages = db.get_message.all_messages_filtered_limited(con, starting_record, fetch_size, filter, order_column_name, order_dir);
                total_filtered_count = db.get_message.message_count_for_filter(con, filter);
                
                String roles = "";
                JsonArrayBuilder json_array_builder = Json.createArrayBuilder();
                for(int a = 0; a < messages.size();a++)
                {

//                message[0] = rs.getString("id");
//                message[1] = rs.getString("uuid");
//                message[2] = support.string_utils.check_for_null(rs.getString("source"));
//                message[3] = support.string_utils.check_for_null(rs.getString("date_sent"));
//                message[4] = support.string_utils.check_for_null(rs.getString("from"));                
//                message[5] = support.string_utils.check_for_null(rs.getString("subject"));  
//                message[6] = support.string_utils.check_for_null(rs.getString("to"));  
//                message[7] = support.string_utils.check_for_null(rs.getString("body"));  
//                message[8] = support.string_utils.check_for_null(rs.getString("mime_type"));  
//                message[9] = support.string_utils.check_for_null(rs.getString("priority"));  
//                message[10] = support.string_utils.check_for_null(rs.getString("status"));  
//                message[11] = support.string_utils.check_for_null(rs.getString("status_date"));  
//                message[12] = support.string_utils.check_for_null(rs.getString("status_by_user_id"));  
//                message[13] = support.string_utils.check_for_null(rs.getString("result"));  
//                message[14] = support.string_utils.check_for_null(rs.getString("has_attachment"));  
//                message[15] = support.string_utils.check_for_null(rs.getString("description"));  
//                message[16] = support.string_utils.check_for_null(rs.getString("log"));  
//                message[17] = support.string_utils.check_for_null(rs.getString("related_to"));     
//                message[18] = support.string_utils.check_for_null(rs.getString("related_to_id"));  
//                message[19] = support.string_utils.check_for_null(rs.getString("assigned_to_id"));     
//                message[20] = support.string_utils.check_for_null(rs.getString("date_assigned"));     
//                message[21] = support.string_utils.check_for_null(rs.getString("date_closed"));     


                    String message_record[] = messages.get(a);
                    ZonedDateTime zdt_temp_date = support.date_utils.utc_to_user_tz(user_tz_name, message_record[3]);
                    String date_sent = DateTimeFormatter.ofPattern("HH:mm MM/dd/yyyy").format(zdt_temp_date);                                                    
                    json_array_builder
                            .add(Json.createObjectBuilder()
                                    .add("id", message_record[0]) 
                                    .add("status", message_record[10]) 
                                    .add("date_sent", date_sent) 
                                    .add("from", message_record[4]) 
                                    .add("subject", message_record[5])
                                    .add("body", StringUtils.substring(message_record[7], 0, 50))
                                    .add("has_attachment", message_record[14])
                            );
                }
                json_builder.add("recordsTotal", total_filtered_count);
                json_builder.add("recordsFiltered", total_filtered_count);
                json_builder.add("recordsFilteredNew", number_of_new_messages);
                json_builder.add("recordsFilteredOpen", number_of_open_messages);
                json_builder.add("recordsFilteredAll", number_of_all_messages);
                json_builder.add("draw", StringUtils.defaultString(request.getParameter("draw")));
                json_builder.add("data", json_array_builder);
                json = json_builder.build();

                con.close();
                //send the JSON data
                out.print(json.toString());
            }
            catch(Exception e)
            {
                logger.error("ERROR Exception in servlet ajax_lookup_messages:=" + e);
                stringBuilder.setLength(0);
                stringBuilder.append("Error in retrieving messages list");
                out.print(stringBuilder.toString());

            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

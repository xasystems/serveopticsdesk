/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */

package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class assets_type_subtype_add_subtype extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String query = "";
        String type_id = "";
        try
        {
            HttpSession session = request.getSession();
            String context_dir = request.getServletContext().getRealPath("");
            if (session.getAttribute("authenticated") == null) 
            {
                response.sendRedirect("index.jsp");
            }
            else
            {
                //do work here
                type_id = request.getParameter("type_id");
                String subtype_active = "false";
                try
                {
                    subtype_active = request.getParameter("subtype_active");
                    if(subtype_active == null)
                    {
                        subtype_active = "false";
                    }
                    else
                    {
                        subtype_active = "true";
                    }
                }
                catch(Exception e)
                {
                    subtype_active = "false";
                }
                String subtype_name = request.getParameter("subtype_name");
                
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                //delete all the current settings
                query = "INSERT INTO asset_subtype (asset_type_id,name,active) VALUES (?,?,?)";
                PreparedStatement stmt = con.prepareStatement(query);
                stmt.setString(1,type_id);
                stmt.setString(2,subtype_name);
                stmt.setString(3,subtype_active);
                stmt.executeUpdate();                
                //stmt.close();
                con.close();
            }            
        }
        catch (Exception e) 
        {
            logger.error("ERROR Exception in servlet assets_type_subtype_add_subtype:=" + e);
        }
        response.sendRedirect("assets_type_subtype_edit.jsp?type_id=" + type_id);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

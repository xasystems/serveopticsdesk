/*
 * Copyright 2019 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class admin_category_subcategory_delete_subcategory extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String query = "";
        String category_id = "";
        String subcategory_id = "";
        try
        {
            HttpSession session = request.getSession();
            String context_dir = request.getServletContext().getRealPath("");
            if (session.getAttribute("authenticated") == null) 
            {
                response.sendRedirect("index.jsp");
            }
            else
            {
                //do work here
                subcategory_id = request.getParameter("subcategory_id");
                category_id = request.getParameter("category_id");
                
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                //delete all the current settings
                query = "DELETE FROM subcategory WHERE id=?";
                PreparedStatement stmt = con.prepareStatement(query);
                stmt.setString(1,subcategory_id);
                stmt.executeUpdate();                
                //stmt.close();
                con.close();
            }            
        }
        catch (Exception e) 
        {
            logger.error("ERROR Exception in servlet admin_category_subcategory_delete_subcategory:=" + e);
        }
        response.sendRedirect("admin_category_subcategory_edit.jsp?category_id=" + category_id);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class assets_type_subtype_edit extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String query = "";
        String type_id = "";
        String type_active = "";
        String type_name = "";
        try
        {
            HttpSession session = request.getSession();
            String context_dir = request.getServletContext().getRealPath("");
            type_id = request.getParameter("type_id");
            if (session.getAttribute("authenticated") == null) 
            {
                response.sendRedirect("index.jsp");
            }
            else
            {
                //do work here
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                //update the category/active
                type_active = request.getParameter("type_active");
                if(type_active == null || type_active.equalsIgnoreCase(""))
                {
                    type_active = "false";
                }
                else
                {
                    type_active = "true";
                }
                type_name = request.getParameter("type_name");
                query = "UPDATE `asset_type` SET `name`='" + type_name + "',`active`='" + type_active + "' WHERE `id`='" + type_id + "'";                

                PreparedStatement stmt = con.prepareStatement(query);
                //stmt.setString(1,category_name);
                //stmt.setString(2,category_active);
                //stmt.setString(3,category_id);
                //System.out.println("stmt=" + stmt);
                stmt.executeUpdate();                   
                
                //delete all the old subcategories
                query = "DELETE FROM asset_subtype WHERE asset_type_id=?";
                stmt = con.prepareStatement(query);
                stmt.setString(1,type_id);                
                stmt.executeUpdate(); 
                //System.out.println("DELETE subs");
                //
                //add all the subcategories
                query = "INSERT INTO asset_subtype (asset_type_id,name,active) VALUES (?,?,?)"; 
                stmt = con.prepareStatement(query);
                //get all the request parameters
                Map params = request.getParameterMap();
                Iterator i = params.keySet().iterator();
                while (i.hasNext())
                {
                    String name = (String) i.next();
                    String value = ((String[]) params.get( name ))[ 0 ];
                    //System.out.println("name=" + name + " value=" + value);
                    if(name.startsWith("subtype_id_"))
                    {
                        //get name
                        String subtype_name = request.getParameter("subtype_name_" + value);
                        //get active
                        String subtype_active = request.getParameter("subtype_active_" + value);
                        try
                        {
                            subtype_active = request.getParameter("subtype_active_" + value);
                            if(subtype_active == null || subtype_active.equalsIgnoreCase(""))
                            {
                                subtype_active = "false";
                            }
                            else
                            {
                                subtype_active = "true";
                            }
                        }
                        catch(Exception e)
                        {
                            subtype_active = "false";
                        }
                        //insert the subcategory
                        stmt.setString(1,type_id);
                        stmt.setString(2,subtype_name);
                        stmt.setString(3,subtype_active);
                        stmt.executeUpdate();   
                    }
                }         
                //stmt.close();
                con.close();
            }            
        }
        catch (Exception e) 
        {
            logger.error("ERROR Exception in servlet assets_type_subtype_edit:=" + e);
        }
        response.sendRedirect("assets_type_subtype.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedHashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lightcouch.CouchDbClient;

/**
 *
 * @author ralph
 */
@WebServlet("/get_avatar")
public class get_avatar extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        CouchDbClient dbClient = null;
        try (OutputStream output = response.getOutputStream()) 
        {
            String uuid = StringUtils.defaultString(request.getParameter("uuid"));
            String name = "avatar";
            logger.info("uuid, name, request" + uuid + name + request.getContextPath());
            if (uuid.equals(""))
            {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
            } else {
                try
                {
                    if (session.getAttribute("authenticated") == null) 
                    {
                        response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                    } else {
                        try
                        {
                            LinkedHashMap props = support.config.get_config(context_dir);

                            dbClient = db.couchdb.get_avatars_client(props);
                            if (dbClient == null)
                            {
                                throw new Exception("dbClient is null");
                            }

                            byte[] buffer = new byte[10240];
                            JsonObject json;
                            try
                            {
                                json = dbClient.find(JsonObject.class, uuid);
                                logger.info(dbClient.getDBUri() + uuid);
                                String content_type = json.get("MIME_type").getAsString();
                                int content_length = json.get("FileSize").getAsInt();
                                response.setContentType(content_type);
                                response.setContentLength(content_length);
                            } catch (Exception e)
                            {
                                e.printStackTrace();
                                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                                return;
                            }

                            try (
                                InputStream input = dbClient.find(uuid + "/" + name);
                            ) {
                                for (int length = 0; (length = input.read(buffer)) > 0;) {
                                    output.write(buffer, 0, length);
                                }
                            }
                        }
                        catch(Exception e)
                        {
                            e.printStackTrace();
                            logger.error("ERROR Exception in servlet get_avatar:=" + e);
                        } finally 
                        {
                            if (dbClient != null )
                            {
                                dbClient.shutdown();
                            }
                        }
                    }
                }   catch(Exception e)
                {
                    e.printStackTrace();
                    logger.error("ERROR Exception in servlet get_avatar:=" + e);
                    response.sendError(HttpServletResponse.SC_NOT_FOUND);
                }

            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

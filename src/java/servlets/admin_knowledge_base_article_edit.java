/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class admin_knowledge_base_article_edit extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            try 
            {
                SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
                java.util.Date cdate = new java.util.Date();
                java.util.Date vdate = new java.util.Date();
                
                String id = request.getParameter("id");
                String knowledge_base_id = request.getParameter("knowledge_base_id");
                String article_name = request.getParameter("article_name");
                String article_description = request.getParameter("article_description");
                String status = request.getParameter("status");
                String article_text = request.getParameter("article_text");
                String keywords = request.getParameter("keywords");
                String created_by_id = request.getParameter("created_by_id");
                if(created_by_id.equalsIgnoreCase(""))
                {
                    created_by_id = null;
                }
                Timestamp created_date = null;
                try
                {
                    created_date = new Timestamp(date_time_picker_format.parse(request.getParameter("created_date")).getTime());
                }
                catch(Exception e)
                {
                    created_date = null;
                }
                //approved_by
                String approved_by_id = request.getParameter("approved_by_id");
                if(approved_by_id.equalsIgnoreCase(""))
                {
                    approved_by_id = null;
                }
                //approved_date
                Timestamp approved_date = null;
                try
                {
                    approved_date = new Timestamp(date_time_picker_format.parse(request.getParameter("approved_date")).getTime());
                }
                catch(Exception e)
                {
                    approved_date = null;
                }   
                Timestamp valid_till = null;
                try
                {
                    valid_till = new Timestamp(date_time_picker_format.parse(request.getParameter("valid_till")).getTime());
                }
                catch(Exception e)
                {
                    valid_till = null;
                }
                
                String escapedHTML = StringEscapeUtils.escapeHtml4(article_text);
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement("UPDATE knowledge_articles SET knowledge_base_id=?,article_name=?,article_description=?,status=?,article_text=?,keywords=?,created_by_id=?,created_date=?,approved_by_id=?,approved_date=?,valid_till=? WHERE id=?");
                
                stmt.setString(1,knowledge_base_id);
                stmt.setString(2,article_name);
                stmt.setString(3,article_description);
                stmt.setString(4,status);
                stmt.setString(5,article_text);
                stmt.setString(6,keywords);
                stmt.setString(7,created_by_id);
                stmt.setTimestamp(8,created_date);
                stmt.setString(9,approved_by_id);
                stmt.setTimestamp(10,approved_date);
                stmt.setTimestamp(11,valid_till);
                stmt.setString(12,id);
                stmt.executeUpdate();
                stmt.close();
                con.close();
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_knowledge_base_article_edit:=" + e);
            }
            response.sendRedirect("admin_knowledge_base.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

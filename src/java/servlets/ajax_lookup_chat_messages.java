/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
@WebServlet("/ajax_lookup_chat_messages")
public class ajax_lookup_chat_messages extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
//        String user_id = request.getParameter("user_id");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

//        String user_id = request.getParameter("user_id");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        JsonObjectBuilder json_builder = Json.createObjectBuilder();
        JsonObject json;
        Integer start;
        Integer length;
        try
        {
            start  = Integer.parseInt(request.getParameter("start"));
        } catch (Exception e)
        {
            start = 0;
        }
        try
        {
            length  = Integer.parseInt(request.getParameter("length"));
        } catch (Exception e)
        {
            length = 20;
        }
        
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {

                if (session.getAttribute("authenticated") == null) 
                {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }

                Connection con = db.db_util.get_contract_connection(context_dir, session);
                LinkedHashMap props = support.config.get_config(context_dir);

                String user_id = session.getAttribute("user_id").toString();
                String object_type = StringUtils.defaultString(request.getParameter("object_type"));
                String parent_object_type = StringUtils.defaultString(request.getParameter("parent_object_type"));
                String object_id = StringUtils.defaultString(request.getParameter("object_id"));
                String parent_object_id = StringUtils.defaultString(request.getParameter("parent_object_id"));
                
                boolean update_session_table = db.get_user_sessions.update(con, user_id);

                String[] room_object;
//                
//                if (task_id.equals("0"))
//                {
//                    room_object = db.get_projects.project_by_id(con, project_id);
//                } else {
//                    room_object = db.get_projects.project_task_info(con, project_id, task_id);
//                }

                JsonArrayBuilder messages_array_builder = Json.createArrayBuilder();
                ArrayList<String[]> messages = db.get_chat_messages.get_messages(con, object_type, object_id, parent_object_type, parent_object_id, start, length);
                JsonArrayBuilder mentions_builder = Json.createArrayBuilder();
                final String regex = "<usertag>(\\d+)</usertag>";
                
                for (int i = 0; i < messages.size(); i++)
                {
                    String[] message = messages.get(i);
                    final Matcher m = Pattern.compile(regex).matcher(StringUtils.defaultString(message[7]));

                    final List<String> matches = new ArrayList<>();
                    while (m.find()) {
//                        matches.add(m.group(0));
                        String mentioned_user_id = m.group(1);
                        String[] mentioned_user = db.get_users.by_id(con, mentioned_user_id);
                        mentions_builder
                            .add(Json.createObjectBuilder()
                                .add("_id", Integer.parseInt(mentioned_user_id))
                                .add("username", StringUtils.defaultString(mentioned_user[3] + " " + mentioned_user[5]))
                            );
                    }
                    JsonArrayBuilder attachments_builder = Json.createArrayBuilder();
                    try
                    { // processing attachments
                        ArrayList<String[]> attachments = new ArrayList();
                        attachments = db.get_chat_messages.get_attachments(con, Long.parseLong(message[0]));
//                        logger.info("INFO ajax_lookup_project_tasks attachments.size :=" + attachments.size());
                        if (attachments.size() > 0)
                        {   
                            for (int k = 0; k < attachments.size(); k++)
                            {
                                String[] attachment = attachments.get(k);
                                attachments_builder
                                    .add(Json.createObjectBuilder()
//                                        .add("id", attachment[0])
//                                        .add("uuid", attachment[1])
//                                        .add("name_on_disk", attachment[2])
//                                        .add("file_name", attachment[3])
//                                        .add("file_date", attachment[4])
//                                        .add("file_size", attachment[5])
					.add("name", attachment[3])
					.add("size", attachment[5])
					.add("type", attachment[6])
					.add("extension", attachment[6])
//					.add("url", props.get("couchdb.attachments_host") + "/" + props.get("couchdb.name") + "/" + attachment[2])
					.add("url", "get_attachment?uuid=" + attachment[1] + "&name=" + attachment[3])
                                    );
                            }
                        }
                    } catch (Exception e)
                    {
                        logger.error("ERROR processing attachments Exception in servlet ajax_lookup_chat_messages:=" + e);
                    }

                    JsonObjectBuilder message_object_builder = Json.createObjectBuilder();
                    message_object_builder
                        .add("id", Long.parseLong(message[0]))
//                        .add("indexId", "12092")
                        .add("sender_id", Integer.parseInt(message[5]))
                        .add("username", message[6])
                        .add("content", StringUtils.defaultString(message[7]))
                        .add("avatar", "assets/imgs/doe.png")
//                        .add("date", "13 November")
                        .add("timestamp", message[8]) //"2019-02-03T06:48:07+0000"
                        .add("system", BooleanUtils.toBoolean(message[9]))
                        .add("saved", BooleanUtils.toBoolean(message[10]))
                        .add("distributed", BooleanUtils.toBoolean(message[11]))
                        .add("seen", BooleanUtils.toBoolean(message[12]))
                        .add("deleted", BooleanUtils.toBoolean(message[13]))
                        .add("disableActions", false)
                        .add("disableReactions", false)
                        .add("files", Json.createArrayBuilder())
                        .add("reactions", Json.createObjectBuilder())
                        .add("files", attachments_builder)
                        .add("mentions", mentions_builder);
                    if (NumberUtils.toInt(message[14], 0) > 0) {
                        message_object_builder.add("replyMessage", Json.createObjectBuilder()
//                            .add("senderId", 1)
                            .add("content", message[15])
                        );
                    }
                    messages_array_builder.add(message_object_builder);
                }
                JsonArray result = messages_array_builder.build();

                con.close();
                JsonWriter writer = Json.createWriter(out);
                writer.writeArray(result);
                writer.close();
                //send the JSON data

                // users
//                JsonArrayBuilder users_array_builder = Json.createArrayBuilder();
//                users_array_builder
//                    .add(Json.createObjectBuilder()
//                        .add("_id", room_object[4])
//                        .add("username", support.string_utils.check_for_null(attachment[1]))
//                        .add("avatar", support.string_utils.check_for_null(attachment[2]))
//                        .add("status", Json.createObjectBuilder()
//                            .add("state", "online")
//                            .add("lastChanged", "2013-10-21T13:28:06.419Z")
//                        )
//                        .add("href", project_task_attachments_folder_symlink + "/" + support.string_utils.check_for_null(attachment[4]))
//                        .add("file_name", support.string_utils.check_for_null(attachment[5]))
//                        .add("file_date", support.string_utils.check_for_null(attachment[6]))
//                        .add("file_size", support.string_utils.check_for_null(attachment[7]))
//                    );


//[
//  {
//    "_id": 7890,
//    "indexId": 12092,
//    "content": "Message 1",
//    "senderId": 1234,
//    "username": "John Doe",
//    "avatar": "assets/imgs/doe.png",
//    "date": "13 November",
//    "timestamp": "10:20",
//    "system": false,
//    "saved": true,
//    "distributed": true,
//    "seen": true,
//    "deleted": false,
//    "disableActions": false,
//    "disableReactions": false,
//    "files": [
//    ],
//    "reactions": {
//      "😁": [
//        1234,
//        4321
//      ],
//      "🥰": [
//        1234
//      ]
//    },
//    "replyMessage": {
//      "content": "Reply Message",
//      "senderId": 4321,
//      "files": [
//      ]
//    }
//  }
//]
            }
            catch(Exception e)
            {
                e.printStackTrace();
                logger.error("ERROR Exception in servlet ajax_lookup_chat_messages:=" + e);
                stringBuilder.setLength(0);
                stringBuilder.append("Error in retrieving tasks list");
                out.print(stringBuilder.toString());

            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class kb_feedback extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String user_id = "";
        String id = "";
        String helpful = "";
        String comment = "";
        String context_dir = request.getServletContext().getRealPath("");
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            try 
            {
                user_id = session.getAttribute("user_id").toString();
                id = request.getParameter("id");
                helpful = request.getParameter("helpful");
                comment = request.getParameter("comment");
                java.util.Date now = new java.util.Date();                  
                Timestamp created_date = null;
                try
                {
                    created_date = new Timestamp(now.getTime());
                }
                catch(Exception e)
                {
                    created_date = null;
                }                
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement("INSERT INTO knowledge_article_feedback (user_id,article_id,helpful,comment,comment_date) VALUES (?,?,?,?,?)");
                stmt.setString(1,user_id);
                stmt.setString(2,id);
                stmt.setString(3,helpful);
                stmt.setString(4,comment);
                stmt.setTimestamp(5,created_date);
                stmt.executeUpdate();
                stmt.close();
                con.close();
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet kb_feedback:=" + e);
            }
            response.sendRedirect("kb_view.jsp?id=" + id);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

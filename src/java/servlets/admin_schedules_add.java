/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class admin_schedules_add extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        String temp = "";
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            try 
            {
                String name = request.getParameter("name");
                String description = request.getParameter("description");
                String start_time = request.getParameter("start_time");
                String end_time = request.getParameter("end_time");
                int is_all_day = 0;
                int include_holidays = 0;
                int sunday = 0; 
                int monday = 0; 
                int tuesday = 0; 
                int wednesday = 0; 
                int thursday = 0; 
                int friday = 0; 
                int saturday = 0; 
                try{temp = request.getParameter("is_all_day"); is_all_day = 1; }catch (Exception e){is_all_day = 0;}
                try{temp = request.getParameter("include_holidays"); include_holidays = 1; }catch (Exception e){include_holidays = 0;}
                try{temp = request.getParameter("sunday"); sunday = 1; }catch (Exception e){sunday = 0;}
                try{temp = request.getParameter("monday"); monday = 1; }catch (Exception e){monday = 0;}
                try{temp = request.getParameter("tuesday"); tuesday = 1; }catch (Exception e){tuesday = 0;}
                try{temp = request.getParameter("wednesday"); wednesday = 1; }catch (Exception e){wednesday = 0;}
                try{temp = request.getParameter("thursday"); thursday = 1; }catch (Exception e){thursday = 0;}
                try{temp = request.getParameter("friday"); friday = 1; }catch (Exception e){friday = 0;}
                try{temp = request.getParameter("saturday"); saturday = 1; }catch (Exception e){saturday = 0;}
                
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement("INSERT INTO schedules (name,description,start_time,end_time,is_all_day,include_holidays,sunday,monday,tuesday,wednesday,thursday,friday,saturday) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
                
                stmt.setString(1,name);
                stmt.setString(2,description);
                stmt.setString(3,start_time);
                stmt.setString(4,end_time);
                stmt.setInt(5,is_all_day);
                stmt.setInt(6,include_holidays);
                stmt.setInt(7,sunday);
                stmt.setInt(8,monday);
                stmt.setInt(9,tuesday);
                stmt.setInt(10,wednesday);
                stmt.setInt(11,thursday);
                stmt.setInt(12,friday);
                stmt.setInt(13,saturday);
                stmt.executeUpdate();
                stmt.close();
                con.close();
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_schedule_add:=" + e);
            }
            response.sendRedirect("admin_schedules.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

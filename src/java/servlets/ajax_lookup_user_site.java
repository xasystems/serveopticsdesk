/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class ajax_lookup_user_site extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
    protected String check_for_null(String value)
    {
        String return_string = "";
        if(value == null || value.equalsIgnoreCase("null"))
        {
            return_string = "";
        }
        else 
        {
            return_string = value;
        }        
        return return_string;
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
        String search = request.getParameter("search");
        response.setContentType("text/html;charset=UTF-8");
        PreparedStatement stmt;
        boolean first_record = true;
        boolean has_data = false;
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                stringBuilder.append("[");
                stmt = con.prepareStatement("SELECT DISTINCT(site) FROM users WHERE site like '%" + search + "%' LIMIT 10");
                ResultSet rs = stmt.executeQuery();
                while(rs.next())
                {
                    has_data = true;
                    
                    if(first_record)
                    {
                        stringBuilder.append("{");
                        stringBuilder.append("\"value\": \"" + check_for_null(rs.getString("site")) + "\",");
                        stringBuilder.append("\"label\": \"" + check_for_null(rs.getString("site")) + "\"");
                        
                        stringBuilder.append("}");
                    }
                    else
                    {
                        stringBuilder.append(",{");
                        stringBuilder.append("\"value\": \"" + check_for_null(rs.getString("site")) + "\",");
                        stringBuilder.append("\"label\": \"" + check_for_null(rs.getString("site")) + "\"");
                        stringBuilder.append("}");
                    }
                    first_record = false;
                }
                stringBuilder.append("]");                
                if(!has_data)
                {
                    stringBuilder.setLength(0);
                    stringBuilder.append("[{\"value\":\"\",\"label\":\"\"}]");
                }                
                stmt.close();
                con.close();
                //send the JSON data
                out.print(stringBuilder.toString());
            }
            catch(Exception e)
            {
                logger.error("ERROR Exception in servlet ajax_lookup_user_site:=" + e);
            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

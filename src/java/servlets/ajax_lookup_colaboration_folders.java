/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebServlet("/ajax_lookup_collaboration_folders")
/**
 *
 * @author ralph
 */
public class ajax_lookup_colaboration_folders extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
//        String user_id = request.getParameter("user_id");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

//        String user_id = request.getParameter("user_id");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        JsonObjectBuilder json_builder = Json.createObjectBuilder();
        JsonObject json;
        
        boolean first_record = true;
        boolean has_data = false;
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {

                if (session.getAttribute("authenticated") == null) 
                {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }

                Connection con = db.db_util.get_contract_connection(context_dir, session);
                int starting_record = 0;
                int previous_record = 0;
                int next_record = 0;
                int fetch_size = 50;
                
                try
                {
                    starting_record  = Integer.parseInt(request.getParameter("start"));
                    fetch_size  = Integer.parseInt(request.getParameter("length"));
                    next_record = starting_record + fetch_size;
                    previous_record = starting_record - fetch_size;
                    if(previous_record <= 0)
                    {
                        previous_record = 0;
                    }
                }
                catch(Exception e)
                {
                    starting_record = 0;
                    previous_record = 0;
                    next_record = starting_record + fetch_size;
                }

                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String view = StringUtils.defaultString(request.getParameter("view"));
                String filter_user_id = "";
                if (view.equals("my")) {
                    filter_user_id = user_id;
                }
                Map<String, String> incidents = db.get_incidents.incidents_count_by_states(con, filter_user_id);
                JsonObjectBuilder incidents_states_object_builder = Json.createObjectBuilder();
                incidents_states_object_builder.add("All", Json.createObjectBuilder()
                    .add("txt", "All")
                    .add("count", incidents.get("all"))
                );                    

                incidents.entrySet().stream().filter(entry -> (!entry.getKey().equals("all"))).forEachOrdered(entry ->
                {
                    incidents_states_object_builder.add(entry.getKey(), Json.createObjectBuilder()
                            .add("txt", entry.getKey())
                            .add("count", entry.getValue())
                        );
                });

                Map<String, String> requests = db.get_requests.requests_count_by_states(con, filter_user_id);
                JsonObjectBuilder requests_states_object_builder = Json.createObjectBuilder();
                requests_states_object_builder.add("All", Json.createObjectBuilder()
                    .add("txt", "All")
                    .add("count", requests.get("all"))
                );                    

                requests.entrySet().stream().filter(entry -> (!entry.getKey().equals("all"))).forEachOrdered(entry ->
                {
                    requests_states_object_builder.add(entry.getKey(), Json.createObjectBuilder()
                            .add("txt", entry.getKey())
                            .add("count", entry.getValue())
                        );
                });

                JsonObjectBuilder folders_object_builder = Json.createObjectBuilder();
                folders_object_builder
                    .add("Incidents", Json.createObjectBuilder()
                        .add("icon", "assets/images/svg/sub-menu/new/incidents-icon.svg")
                        .add("txt", "Incidents")
                        .add("objectType", "Incident")
                        .add("count", incidents.get("all"))
                        .add("subfolders", incidents_states_object_builder))
                    .add("Requests", Json.createObjectBuilder()
                        .add("icon", "assets/images/svg/sub-menu/new/requests-icon.svg")
                        .add("txt", "Requests")
                        .add("objectType", "Request")
                        .add("count", requests.get("all"))
                        .add("subfolders", requests_states_object_builder));

//                "Incidents": {
//                    icon: "assets/images/svg/sub-menu/new/incidents-icon.svg",
//                    txt: "Incidents",
//                    count: "1,117",
//                    items: {
//                        "New": {
//                            txt: "New",
//                            count: "12",
//                        },
//                        "Assigned": {
//                            txt: "Assigned",
//                            count: "105",
//                        }                            
//                    }                     
//                },
//                "Requests": {
//                    icon: "assets/images/svg/sub-menu/new/requests-icon.svg",
//                    txt: "Requests",
//                    count: "41"                        
//                },
//                "Mentions": {
//                    icon: "assets/images/svg/sub-menu/new/requests-icon.svg",
//                    txt: "@Mentions",
//                    count: "17"                        
//                }

                json = folders_object_builder.build();

                con.close();
                //send the JSON data
                out.print(json.toString());
            }
            catch(Exception e)
            {
                e.printStackTrace();
                logger.error("ERROR Exception in servlet ajax_lookup_collaboration_folders:=" + e);
                stringBuilder.setLength(0);
                stringBuilder.append("Error in retrieving folders list");
                out.print(stringBuilder.toString());

            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

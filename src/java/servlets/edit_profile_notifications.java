/*
//Copyright 2020 XaSystems, Inc. , All rights reserved.
 */

package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author rcampbell
 */
public class edit_profile_notifications extends HttpServlet {

    private static Logger logger = LogManager.getLogger();    

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            String user_id = session.getAttribute("user_id").toString();
            
            String incident_create_assigned_to_me = "false";
            try
            {
                incident_create_assigned_to_me = request.getParameter("incident_create_assigned_to_me");
                if(incident_create_assigned_to_me == null)
                {
                    incident_create_assigned_to_me = "false";
                }
            }
            catch(Exception e)
            {
                incident_create_assigned_to_me = "false";
            }            
            
            String incident_create_assigned_to_group = "false";            
            try
            {
                incident_create_assigned_to_group = request.getParameter("incident_create_assigned_to_group");
                if(incident_create_assigned_to_group == null)
                {
                    incident_create_assigned_to_group = "false";
                }
            }
            catch(Exception e)
            {
                incident_create_assigned_to_group = "false";
            }    
            
            String incident_create_assigned_to_group_select[] = request.getParameterValues("incident_create_assigned_to_group_select");
            if(incident_create_assigned_to_group_select == null)
            {
                incident_create_assigned_to_group_select = new String[0];
            }
                    
            String incident_update_assigned_to_me = "false";
            try
            {
                incident_update_assigned_to_me = request.getParameter("incident_update_assigned_to_me");
                if(incident_update_assigned_to_me == null)
                {
                    incident_update_assigned_to_me = "false";
                }
            }
            catch(Exception e)
            {
                incident_update_assigned_to_me = "false";
            } 
            
            String incident_update_assigned_to_group = "false";            
            try
            {
                incident_update_assigned_to_group = request.getParameter("incident_update_assigned_to_group");
                if(incident_update_assigned_to_group == null)
                {
                    incident_update_assigned_to_group = "false";
                }
            }
            catch(Exception e)
            {
                incident_update_assigned_to_group = "false";
            }
            
            String incident_update_assigned_to_group_select[] = request.getParameterValues("incident_update_assigned_to_group_select");
            if(incident_update_assigned_to_group_select == null)
            {
                incident_update_assigned_to_group_select = new String[0];
            }
                    
            String incident_resolve_assigned_to_me = "false";
            try
            {
                incident_resolve_assigned_to_me = request.getParameter("incident_resolve_assigned_to_me");
                if(incident_resolve_assigned_to_me == null)
                {
                    incident_resolve_assigned_to_me = "false";
                }
            }
            catch(Exception e)
            {
                incident_resolve_assigned_to_me = "false";
            }
            
            String incident_resolve_assigned_to_group = "false";
            try
            {
                incident_resolve_assigned_to_group = request.getParameter("incident_resolve_assigned_to_group");
                if(incident_resolve_assigned_to_group == null)
                {
                    incident_resolve_assigned_to_group = "false";
                }
            }
            catch(Exception e)
            {
                incident_resolve_assigned_to_group = "false";
            }
            
            String incident_resolve_assigned_to_group_select[] = request.getParameterValues("incident_resolve_assigned_to_group_select");
            if(incident_resolve_assigned_to_group_select == null)
            {
                incident_resolve_assigned_to_group_select = new String[0];
            }
                    
            String incident_close_assigned_to_me = "false";
            try
            {
                incident_close_assigned_to_me = request.getParameter("incident_close_assigned_to_me");
                if(incident_close_assigned_to_me == null)
                {
                    incident_close_assigned_to_me = "false";
                }
            }
            catch(Exception e)
            {
                incident_close_assigned_to_me = "false";
            }
            
            String incident_close_assigned_to_group = "false";
            try
            {
                incident_close_assigned_to_group = request.getParameter("incident_close_assigned_to_group");
                if(incident_close_assigned_to_group == null)
                {
                    incident_close_assigned_to_group = "false";
                }
            }
            catch(Exception e)
            {
                incident_close_assigned_to_group = "false";
            }
            
            String incident_close_assigned_to_group_select[] = request.getParameterValues("incident_close_assigned_to_group_select");
            if(incident_close_assigned_to_group_select == null)
            {
                incident_close_assigned_to_group_select = new String[0];
            }
                        
            String request_create_assigned_to_me = "false";
            try
            {
                request_create_assigned_to_me = request.getParameter("request_create_assigned_to_me");
                if(request_create_assigned_to_me == null)
                {
                    request_create_assigned_to_me = "false";
                }
            }
            catch(Exception e)
            {
                request_create_assigned_to_me = "false";
            }
            
            String request_create_assigned_to_group = "false";
            try
            {
                request_create_assigned_to_group = request.getParameter("request_create_assigned_to_group");
                if(request_create_assigned_to_group == null)
                {
                    request_create_assigned_to_group = "false";
                }
            }
            catch(Exception e)
            {
                request_create_assigned_to_group = "false";
            }
            
            String request_create_assigned_to_group_select[] = request.getParameterValues("request_create_assigned_to_group_select");
            if(request_create_assigned_to_group_select == null)
            {
                request_create_assigned_to_group_select = new String[0];
            }            
             
            String request_update_assigned_to_me = "false";
            try
            {
                request_update_assigned_to_me = request.getParameter("request_update_assigned_to_me");
                if(request_update_assigned_to_me == null)
                {
                    request_update_assigned_to_me = "false";
                }
            }
            catch(Exception e)
            {
                request_update_assigned_to_me = "false";
            }
                        
            String request_update_assigned_to_group = "false";
            try
            {
                request_update_assigned_to_group = request.getParameter("request_update_assigned_to_group");
                if(request_update_assigned_to_group == null)
                {
                    request_update_assigned_to_group = "false";
                }
            }
            catch(Exception e)
            {
                request_update_assigned_to_group = "false";
            }
            
            String request_update_assigned_to_group_select[] = request.getParameterValues("request_update_assigned_to_group_select");
            if(request_update_assigned_to_group_select == null)
            {
                request_update_assigned_to_group_select = new String[0];
            }  
            
            String request_resolve_assigned_to_me = "false";
            try
            {
                request_resolve_assigned_to_me = request.getParameter("request_resolve_assigned_to_me");
                if(request_resolve_assigned_to_me == null)
                {
                    request_resolve_assigned_to_me = "false";
                }
            }
            catch(Exception e)
            {
                request_resolve_assigned_to_me = "false";
            }
            
            String request_resolve_assigned_to_group = "false";
            try
            {
                request_resolve_assigned_to_group = request.getParameter("request_resolve_assigned_to_group");
                if(request_resolve_assigned_to_group == null)
                {
                    request_resolve_assigned_to_group = "false";
                }
            }
            catch(Exception e)
            {
                request_resolve_assigned_to_group = "false";
            }
            String request_resolve_assigned_to_group_select[] = request.getParameterValues("request_resolve_assigned_to_group_select");
            if(request_resolve_assigned_to_group_select == null)
            {
                request_resolve_assigned_to_group_select = new String[0];
            }  
            
            String request_close_assigned_to_me = "false";
            try
            {
                request_close_assigned_to_me = request.getParameter("request_close_assigned_to_me");
                if(request_close_assigned_to_me == null)
                {
                    request_close_assigned_to_me = "false";
                }
            }
            catch(Exception e)
            {
                request_close_assigned_to_me = "false";
            }
            String request_close_assigned_to_group = "false";
            try
            {
                request_close_assigned_to_group = request.getParameter("request_close_assigned_to_group");
                if(request_close_assigned_to_group == null)
                {
                    request_close_assigned_to_group = "false";
                }
            }
            catch(Exception e)
            {
                request_close_assigned_to_group = "false";
            }
            
            String request_close_assigned_to_group_select[] = request.getParameterValues("request_close_assigned_to_group_select");
            if(request_close_assigned_to_group_select == null)
            {
                request_close_assigned_to_group_select = new String[0];
            }  
            
            
            String job_create_assigned_to_me = "false";
            try
            {
                job_create_assigned_to_me = request.getParameter("job_create_assigned_to_me");
                if(job_create_assigned_to_me == null)
                {
                    job_create_assigned_to_me = "false";
                }
            }
            catch(Exception e)
            {
                job_create_assigned_to_me = "false";
            }
            
            String job_create_assigned_to_group = "false";
            try
            {
                job_create_assigned_to_group = request.getParameter("job_create_assigned_to_group");
                if(job_create_assigned_to_group == null)
                {
                    job_create_assigned_to_group = "false";
                }
            }
            catch(Exception e)
            {
                job_create_assigned_to_group = "false";
            }
            String job_create_assigned_to_group_select[] = request.getParameterValues("job_create_assigned_to_group_select");
            if(job_create_assigned_to_group_select == null)
            {
                job_create_assigned_to_group_select = new String[0];
            }  
                
            String job_update_assigned_to_me = "false";
            try
            {
                job_update_assigned_to_me = request.getParameter("job_update_assigned_to_me");
                if(job_update_assigned_to_me == null)
                {
                    job_update_assigned_to_me = "false";
                }
            }
            catch(Exception e)
            {
                job_update_assigned_to_me = "false";
            }
            String job_update_assigned_to_group = "false";
            try
            {
                job_update_assigned_to_group = request.getParameter("job_update_assigned_to_group");
                if(job_update_assigned_to_group == null)
                {
                    job_update_assigned_to_group = "false";
                }
            }
            catch(Exception e)
            {
                job_update_assigned_to_group = "false";
            }
            String job_update_assigned_to_group_select[] = request.getParameterValues("job_update_assigned_to_group_select");
            if(job_update_assigned_to_group_select == null)
            {
                job_update_assigned_to_group_select = new String[0];
            } 
            
            String job_close_assigned_to_me = "false";
            try
            {
                job_close_assigned_to_me = request.getParameter("job_close_assigned_to_me");
                if(job_close_assigned_to_me == null)
                {
                    job_close_assigned_to_me = "false";
                }
            }
            catch(Exception e)
            {
                job_close_assigned_to_me = "false";
            }
            String job_close_assigned_to_group = "false";
            try
            {
                job_close_assigned_to_group = request.getParameter("job_close_assigned_to_group");
                if(job_close_assigned_to_group == null)
                {
                    job_close_assigned_to_group = "false";
                }
            }
            catch(Exception e)
            {
                job_close_assigned_to_group = "false";
            }
            String job_close_assigned_to_group_select[] = request.getParameterValues("job_close_assigned_to_group_select");
            if(job_close_assigned_to_group_select == null)
            {
                job_close_assigned_to_group_select = new String[0];
            } 
                    
            String task_create_assigned_to_me = "false";
            try
            {
                task_create_assigned_to_me = request.getParameter("task_create_assigned_to_me");
                if(task_create_assigned_to_me == null)
                {
                    task_create_assigned_to_me = "false";
                }
            }
            catch(Exception e)
            {
                task_create_assigned_to_me = "false";
            }
            String task_create_assigned_to_group = "false";
            try
            {
                task_create_assigned_to_group = request.getParameter("task_create_assigned_to_group");
                if(task_create_assigned_to_group == null)
                {
                    task_create_assigned_to_group = "false";
                }
            }
            catch(Exception e)
            {
                task_create_assigned_to_group = "false";
            }
            String task_create_assigned_to_group_select[] = request.getParameterValues("task_create_assigned_to_group_select");
            if(task_create_assigned_to_group_select == null)
            {
                task_create_assigned_to_group_select = new String[0];
            }        
            String task_update_assigned_to_me = "false";
            try
            {
                task_update_assigned_to_me = request.getParameter("task_update_assigned_to_me");
                if(task_update_assigned_to_me == null)
                {
                    task_update_assigned_to_me = "false";
                }
            }
            catch(Exception e)
            {
                task_update_assigned_to_me = "false";
            }
            String task_update_assigned_to_group = "false";
            try
            {
                task_update_assigned_to_group = request.getParameter("task_update_assigned_to_group");
                if(task_update_assigned_to_group == null)
                {
                    task_update_assigned_to_group = "false";
                }
            }
            catch(Exception e)
            {
                task_update_assigned_to_group = "false";
            }
            String task_update_assigned_to_group_select[] = request.getParameterValues("task_update_assigned_to_group_select");
            if(task_update_assigned_to_group_select == null)
            {
                task_update_assigned_to_group_select = new String[0];
            }  
            String task_close_assigned_to_me = "false";
            try
            {
                task_close_assigned_to_me = request.getParameter("task_close_assigned_to_me");
                if(task_close_assigned_to_me == null)
                {
                    task_close_assigned_to_me = "false";
                }
            }
            catch(Exception e)
            {
                task_close_assigned_to_me = "false";
            }
            String task_close_assigned_to_group = "false";
            try
            {
                task_close_assigned_to_group = request.getParameter("task_close_assigned_to_group");
                if(task_close_assigned_to_group == null)
                {
                    task_close_assigned_to_group = "false";
                }
            }
            catch(Exception e)
            {
                task_close_assigned_to_group = "false";
            }
            
            String task_close_assigned_to_group_select[] = request.getParameterValues("task_close_assigned_to_group_select");
            if(task_close_assigned_to_group_select == null)
            {
                task_close_assigned_to_group_select = new String[0];
            }  
            
            
            String query_notifications_user = "UPDATE `notifications_user` SET active=? WHERE user_id=? AND notify_on=?";   
            String query_notifications = "INSERT INTO `notifications` (type,user_id,entity_id) VALUES (?,?,?)";
            String truncate_notifications = "DELETE FROM `notifications` WHERE user_id=?";
            
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt_notifications_user = con.prepareStatement(query_notifications_user);
                
                PreparedStatement stmt_notifications = con.prepareStatement(query_notifications);
                //clear out the current groups in notifications
                PreparedStatement stmt_truncate_notifications = con.prepareStatement(truncate_notifications);
                stmt_truncate_notifications.setString(1, user_id);
                stmt_truncate_notifications.executeUpdate();
                
                System.out.println("line 519");
                
                //update the users notifications
                stmt_notifications_user.setString(1, incident_create_assigned_to_me);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "incident_create_assigned_to_me");
                stmt_notifications_user.executeUpdate();                
                
                stmt_notifications_user.setString(1, incident_create_assigned_to_group);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "incident_create_assigned_to_group");
                stmt_notifications_user.executeUpdate();
                
                for(int a = 0; a < incident_create_assigned_to_group_select.length;a++)
                {
                    stmt_notifications.setString(1, "incident_create_assigned_to_group");
                    stmt_notifications.setString(2, user_id);
                    stmt_notifications.setString(3, incident_create_assigned_to_group_select[a]);
                    stmt_notifications.execute();
                }
                
                ///////////////////////////////////////////////////////////////////////////////////////////////////////
                stmt_notifications_user.setString(1, incident_update_assigned_to_me);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "incident_update_assigned_to_me");
                stmt_notifications_user.execute();
                
                stmt_notifications_user.setString(1, incident_update_assigned_to_group);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "incident_update_assigned_to_group");
                stmt_notifications_user.execute();
                
                for(int a = 0; a < incident_update_assigned_to_group_select.length;a++)
                {
                    stmt_notifications.setString(1, "incident_update_assigned_to_group");
                    stmt_notifications.setString(2, user_id);
                    stmt_notifications.setString(3, incident_update_assigned_to_group_select[a]);
                    stmt_notifications.execute();
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////
                stmt_notifications_user.setString(1, incident_resolve_assigned_to_me);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "incident_resolve_assigned_to_me");
                stmt_notifications_user.execute();
                
                stmt_notifications_user.setString(1, incident_resolve_assigned_to_group);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "incident_resolve_assigned_to_group");
                stmt_notifications_user.execute();
                
                for(int a = 0; a < incident_resolve_assigned_to_group_select.length;a++)
                {
                    stmt_notifications.setString(1, "incident_resolve_assigned_to_group");
                    stmt_notifications.setString(2, user_id);
                    stmt_notifications.setString(3, incident_resolve_assigned_to_group_select[a]);
                    stmt_notifications.execute();
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////
                stmt_notifications_user.setString(1, incident_close_assigned_to_me);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "incident_close_assigned_to_me");
                stmt_notifications_user.execute();
                
                stmt_notifications_user.setString(1,incident_close_assigned_to_group );
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "incident_close_assigned_to_group");
                stmt_notifications_user.execute();
                
                for(int a = 0; a < incident_close_assigned_to_group_select.length;a++)
                {
                    stmt_notifications.setString(1, "incident_close_assigned_to_group");
                    stmt_notifications.setString(2, user_id);
                    stmt_notifications.setString(3, incident_close_assigned_to_group_select[a]);
                    stmt_notifications.execute();
                }
                //////////////////////////////////////////////////////////////////////////////////////////
                stmt_notifications_user.setString(1, request_create_assigned_to_me);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "request_create_assigned_to_me");
                stmt_notifications_user.execute();
                
                stmt_notifications_user.setString(1, request_create_assigned_to_group);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "request_create_assigned_to_group");
                stmt_notifications_user.execute();
                
                for(int a = 0; a < request_create_assigned_to_group_select.length;a++)
                {
                    stmt_notifications.setString(1, "request_create_assigned_to_group");
                    stmt_notifications.setString(2, user_id);
                    stmt_notifications.setString(3, request_create_assigned_to_group_select[a]);
                    stmt_notifications.execute();
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////
                stmt_notifications_user.setString(1, request_update_assigned_to_me);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "request_update_assigned_to_me");
                stmt_notifications_user.execute();
                
                stmt_notifications_user.setString(1, request_update_assigned_to_group);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "request_update_assigned_to_group");
                stmt_notifications_user.execute();
                
                for(int a = 0; a < request_update_assigned_to_group_select.length;a++)
                {
                    stmt_notifications.setString(1, "request_update_assigned_to_group");
                    stmt_notifications.setString(2, user_id);
                    stmt_notifications.setString(3, request_update_assigned_to_group_select[a]);
                    stmt_notifications.execute();
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////
                stmt_notifications_user.setString(1, request_resolve_assigned_to_me);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "request_resolve_assigned_to_me");
                stmt_notifications_user.execute();
                
                stmt_notifications_user.setString(1, request_resolve_assigned_to_group);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "request_resolve_assigned_to_group");
                stmt_notifications_user.execute();
                
                for(int a = 0; a < request_resolve_assigned_to_group_select.length;a++)
                {
                    stmt_notifications.setString(1, "request_resolve_assigned_to_group");
                    stmt_notifications.setString(2, user_id);
                    stmt_notifications.setString(3, request_resolve_assigned_to_group_select[a]);
                    stmt_notifications.execute();
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////
                stmt_notifications_user.setString(1, request_close_assigned_to_me);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "request_close_assigned_to_me");
                stmt_notifications_user.execute();
                
                stmt_notifications_user.setString(1, request_close_assigned_to_group);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "request_close_assigned_to_group");
                stmt_notifications_user.execute();
                
                for(int a = 0; a < request_close_assigned_to_group_select.length;a++)
                {
                    stmt_notifications.setString(1, "request_close_assigned_to_group");
                    stmt_notifications.setString(2, user_id);
                    stmt_notifications.setString(3, request_close_assigned_to_group_select[a]);
                    stmt_notifications.execute();
                }
                //////////////////////////////////////////////////////////////////////////////////////////
                stmt_notifications_user.setString(1, job_create_assigned_to_me);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "job_create_assigned_to_me");
                stmt_notifications_user.execute();
                
                stmt_notifications_user.setString(1, job_create_assigned_to_group);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "job_create_assigned_to_group");
                stmt_notifications_user.execute();
                
                for(int a = 0; a < job_create_assigned_to_group_select.length;a++)
                {
                    stmt_notifications.setString(1, "job_create_assigned_to_group");
                    stmt_notifications.setString(2, user_id);
                    stmt_notifications.setString(3, job_create_assigned_to_group_select[a]);
                    stmt_notifications.execute();
                }
                //////////////////////////////////////////////////////////////////////////////////////////
                stmt_notifications_user.setString(1, job_update_assigned_to_me);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "job_update_assigned_to_me");
                stmt_notifications_user.execute();
                
                stmt_notifications_user.setString(1, job_update_assigned_to_group);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "job_update_assigned_to_group");
                stmt_notifications_user.execute();
                
                for(int a = 0; a < job_update_assigned_to_group_select.length;a++)
                {
                    stmt_notifications.setString(1, "job_update_assigned_to_group");
                    stmt_notifications.setString(2, user_id);
                    stmt_notifications.setString(3, job_update_assigned_to_group_select[a]);
                    stmt_notifications.execute();
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////
                stmt_notifications_user.setString(1, job_close_assigned_to_me);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "job_close_assigned_to_me");
                stmt_notifications_user.execute();
                
                stmt_notifications_user.setString(1, job_close_assigned_to_group);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "job_close_assigned_to_group");
                stmt_notifications_user.execute();
                
                for(int a = 0; a < job_close_assigned_to_group_select.length;a++)
                {
                    stmt_notifications.setString(1, "job_close_assigned_to_group");
                    stmt_notifications.setString(2, user_id);
                    stmt_notifications.setString(3, job_close_assigned_to_group_select[a]);
                    stmt_notifications.execute();
                }
                //////////////////////////////////////////////////////////////////////////////////////////
                stmt_notifications_user.setString(1, task_create_assigned_to_me);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "task_create_assigned_to_me");
                stmt_notifications_user.execute();
                
                stmt_notifications_user.setString(1, task_create_assigned_to_group);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "task_create_assigned_to_group");
                stmt_notifications_user.execute();
                
                for(int a = 0; a < task_create_assigned_to_group_select.length;a++)
                {
                    stmt_notifications.setString(1, "task_create_assigned_to_group");
                    stmt_notifications.setString(2, user_id);
                    stmt_notifications.setString(3, task_create_assigned_to_group_select[a]);
                    stmt_notifications.execute();
                }
                //////////////////////////////////////////////////////////////////////////////////////////
                stmt_notifications_user.setString(1, task_update_assigned_to_me);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "task_update_assigned_to_me");
                stmt_notifications_user.execute();
                
                stmt_notifications_user.setString(1, task_update_assigned_to_group);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "task_update_assigned_to_group");
                stmt_notifications_user.execute();
                
                for(int a = 0; a < task_update_assigned_to_group_select.length;a++)
                {
                    stmt_notifications.setString(1, "task_update_assigned_to_group");
                    stmt_notifications.setString(2, user_id);
                    stmt_notifications.setString(3, task_update_assigned_to_group_select[a]);
                    stmt_notifications.execute();
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////
                stmt_notifications_user.setString(1, task_close_assigned_to_me);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "task_close_assigned_to_me");
                stmt_notifications_user.execute();
                
                stmt_notifications_user.setString(1, task_close_assigned_to_group);
                stmt_notifications_user.setString(2, user_id);
                stmt_notifications_user.setString(3, "task_close_assigned_to_group");
                stmt_notifications_user.execute();
                
                for(int a = 0; a < task_close_assigned_to_group_select.length;a++)
                {
                    stmt_notifications.setString(1, "task_close_assigned_to_group");
                    stmt_notifications.setString(2, user_id);
                    stmt_notifications.setString(3, task_close_assigned_to_group_select[a]);
                    stmt_notifications.execute();
                }
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet edit_profile_notifications:=" + e);
            }
        }
        response.sendRedirect("edit_profile.jsp?success=true");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

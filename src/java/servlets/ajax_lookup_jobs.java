/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class ajax_lookup_jobs extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
//        String user_id = request.getParameter("user_id");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

//        String user_id = request.getParameter("user_id");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        JsonObjectBuilder json_builder = Json.createObjectBuilder();
        JsonObject json;
        
        boolean first_record = true;
        boolean has_data = false;
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {

                if (session.getAttribute("authenticated") == null) 
                {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }

                Connection con = db.db_util.get_contract_connection(context_dir, session);
                int starting_record = 0;
                int previous_record = 0;
                int next_record = 0;
                int fetch_size = 50;
                
                try
                {
                    starting_record  = Integer.parseInt(request.getParameter("start"));
                    fetch_size  = Integer.parseInt(request.getParameter("length"));
                    next_record = starting_record + fetch_size;
                    previous_record = starting_record - fetch_size;
                    if(previous_record <= 0)
                    {
                        previous_record = 0;
                    }
                }
                catch(Exception e)
                {
                    starting_record = 0;
                    previous_record = 0;
                    next_record = starting_record + fetch_size;
                }

                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);

                ArrayList<String[]> jobs = new ArrayList();

                Map<String, String> filters = new HashMap<String, String>();

                try
                {
                    String request_filters = request.getParameter("filters"); //assigned_to_me,unassigned, created_today, closed_today, open, open_7_14, open_14_30, open_30_plus 
                    if(request_filters != null)
                    {
                        JsonReader reader = Json.createReader(new StringReader(request_filters));

                        JsonObject jsonObject = reader.readObject();
                        Iterator it = jsonObject.entrySet().iterator();

                        while (it.hasNext()) {
                            JsonObject.Entry mapEntry = (JsonObject.Entry)it.next();
                            filters.put(mapEntry.getKey().toString(), ((JsonString)mapEntry.getValue()).getString());
                        }
                    }   
                }
                catch(Exception e)
                {
//                    filter = "all";
                }

                String order_column_num = StringUtils.defaultString(request.getParameter("order[0][column]"));
                String order_dir = StringUtils.defaultString(request.getParameter("order[0][dir]"));
                String order_column_name = "";                
                String[] permitted_sort_fields = {"name", "priority", "scheduled_start_date", "scheduled_end_date"};
                if (!order_column_num.equals(""))
                {
                    order_column_name = StringUtils.defaultString(request.getParameter("columns[" + order_column_num + "][name]"));
                    if (!Arrays.stream(permitted_sort_fields).anyMatch(order_column_name::equals))
                    {
                        order_column_name = "";
                    } else {
                        switch (order_dir) {
                            case "asc":
                                break;
                            case "desc":
                                break;
                            default:
                                order_dir = "";
                        }
                    }
                    
                }

                String predefined = StringUtils.defaultString(request.getParameter("predefined"));
                String add_query = "";
                SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
                java.util.Date now = new java.util.Date();
                String now_string = timestamp_format.format(now);

                if(!predefined.equalsIgnoreCase(""))
                {
                    if(predefined.equalsIgnoreCase("my_open_jobs"))
                    {
                        add_query = "jobs.assigned_to_id = " + user_id + " AND jobs.status NOT LIKE '%complete%' AND jobs.status NOT LIKE '%cancelled%'";
                    }
                    else if(predefined.equalsIgnoreCase("delinquent"))
                    {
                        add_query = "jobs.assigned_to_id= " + user_id + " AND jobs.scheduled_end_date < '" + now_string + "' AND jobs.status NOT LIKE '%complete%' AND  jobs.status NOT LIKE '%cancelled%'";
                    }
                    else if(predefined.equalsIgnoreCase("all_open_jobs"))
                    {
                        add_query = "jobs.status NOT LIKE '%complete%' AND jobs.status NOT LIKE '%cancelled%'";
                    }
                    else if(predefined.equalsIgnoreCase("all_delinquent"))
                    {
                        add_query = "jobs.scheduled_end_date < '" + now_string + "' AND jobs.status NOT LIKE '%complete%' AND  jobs.status NOT LIKE '%cancelled%'";
                    }
                    else if(predefined.equalsIgnoreCase("my_critical"))
                    {
                        add_query = "jobs.assigned_to_id = " + user_id + " AND jobs.status NOT LIKE '%complete%' AND jobs.status NOT LIKE '%cancelled%' AND jobs.priority = 'Critical'" ;
                    }
                    else if(predefined.equalsIgnoreCase("my_high"))
                    {
                        add_query = "jobs.assigned_to_id = " + user_id + " AND jobs.status NOT LIKE '%complete%' AND jobs.status NOT LIKE '%cancelled%' AND jobs.priority = 'High'";
                    }
                    else if(predefined.equalsIgnoreCase("my_medium"))
                    {
                        add_query = "jobs.assigned_to_id = " + user_id + " AND jobs.status NOT LIKE '%complete%' AND jobs.status NOT LIKE '%cancelled%' AND jobs.priority = 'Medium'";
                    }
                    else if(predefined.equalsIgnoreCase("my_low"))
                    {
                        add_query = "jobs.assigned_to_id = " + user_id + " AND jobs.status NOT LIKE '%complete%' AND jobs.status NOT LIKE '%cancelled%' AND jobs.priority = 'Low'";
                    }
                    else if(predefined.equalsIgnoreCase("all_open_critical"))
                    {
                        add_query = "jobs.status NOT LIKE '%complete%' AND jobs.status NOT LIKE '%cancelled%' AND jobs.priority = 'Critical'" ;
                    }
                    else if(predefined.equalsIgnoreCase("all_open_high"))
                    {
                        add_query = "jobs.status NOT LIKE '%complete%' AND jobs.status NOT LIKE '%cancelled%' AND reques.priority = 'High'";
                    }
                    else if(predefined.equalsIgnoreCase("all_open_medium"))
                    {
                        add_query = "jobs.status NOT LIKE '%complete%' AND jobs.status NOT LIKE '%cancelled%' AND jobs.priority = 'Medium'";
                    }
                    else if(predefined.equalsIgnoreCase("all_open_low"))
                    {
                        add_query = "jobs.status NOT LIKE '%complete%' AND jobs.status NOT LIKE '%cancelled%' AND jobs.priority = 'Low'";
                    }
                }

                int total_filtered_count = db.get_jobs.all_filtered_count(con, filters, add_query);
                jobs = db.get_jobs.all_limited_filtered(con, starting_record, fetch_size, filters, order_column_name, order_dir, add_query);

                JsonArrayBuilder json_array_builder = Json.createArrayBuilder();
                for(int a = 0; a < jobs.size();a++)
                {
                    String job_record[] = jobs.get(a);

                    JsonArrayBuilder attachments_builder = Json.createArrayBuilder();                
                    try
                    { // processing attachments
                        ArrayList<String[]> attachments = new ArrayList();
                        attachments = db.get_jobs.get_attachments_by_job_id(con, job_record[0]);
                        logger.info("INFO ajax_lookup_jobs attachments.size :=" + attachments.size());
                        if (attachments.size() > 0)
                        {   
                            for (int i = 0; i < attachments.size(); i++)
                            {
                                String[] attachment = attachments.get(i);
                                attachments_builder
                                    .add(Json.createObjectBuilder()
                                         .add("id", support.string_utils.check_for_null(attachment[0]))
                                         .add("job_id", support.string_utils.check_for_null(attachment[1]))
                                         .add("uuid", support.string_utils.check_for_null(attachment[2]))
                                         .add("name_on_disk", support.string_utils.check_for_null(attachment[3]))
                                         .add("file_name", support.string_utils.check_for_null(attachment[4]))
                                         .add("file_date", support.string_utils.check_for_null(attachment[5]))
                                         .add("file_size", support.string_utils.check_for_null(attachment[6]))
                                         .add("uploaded_by", support.string_utils.check_for_null(attachment[8]))
                                         .add("uploaded_by_name", support.string_utils.check_for_null(attachment[9]))
                                    );
                            }
                        }
                    } catch (Exception e)
                    {
                        logger.error("ERROR processing attachments Exception in servlet ajax_lookup_jobs:=" + e);
                    }

                    json_array_builder
                        .add(Json.createObjectBuilder()
                            .add("id", support.string_utils.check_for_null(job_record[0]))
                            .add("name", support.string_utils.check_for_null(job_record[1]))
                            .add("description", support.string_utils.check_for_null(job_record[2]))
                            .add("assigned_group_id", support.string_utils.check_for_null(job_record[3]))
                            .add("assigned_to_id", support.string_utils.check_for_null(job_record[4]))
                            .add("priority", support.string_utils.check_for_null(job_record[5]))
                            .add("status", support.string_utils.check_for_null(job_record[6]))
                            .add("scheduled_start_date", support.string_utils.check_for_null(job_record[7]))
                            .add("actual_start_date", support.string_utils.check_for_null(job_record[8]))
                            .add("scheduled_end_date", support.string_utils.check_for_null(job_record[9]))
                            .add("actual_end_date", support.string_utils.check_for_null(job_record[10]))
                            .add("estimated_duration", support.string_utils.check_for_null(job_record[11]))
                            .add("actual_duration", support.string_utils.check_for_null(job_record[12]))
                            .add("notes", support.string_utils.check_for_null(job_record[13]))
                            //LOOKUP populate the assigned_to_id info
                            .add("assigned_to_username", support.string_utils.check_for_null(job_record[14]))
                            .add("assigned_to_first", support.string_utils.check_for_null(job_record[15]))
                            .add("assigned_to_mi", support.string_utils.check_for_null(job_record[16]))
                            .add("assigned_to_last", support.string_utils.check_for_null(job_record[17]))
                            .add("assigned_to_phone_mobile", support.string_utils.check_for_null(job_record[18]))
                            .add("assigned_to_avatar", support.string_utils.check_for_null(job_record[22]))
                            //LOOKUP assigned_group info
                            .add("assigned_group_name", support.string_utils.check_for_null(job_record[19]))
                            .add("attachments", attachments_builder)
                            .add("timeline", support.string_utils.check_for_null(job_record[20]))
                            .add("timeline_percent", support.string_utils.check_for_null(job_record[21]))
                        );
                }
                json_builder.add("recordsTotal", total_filtered_count);
                json_builder.add("recordsFiltered", total_filtered_count);
                json_builder.add("draw", request.getParameter("draw"));
                json_builder.add("data", json_array_builder);



                json = json_builder.build();

                con.close();
                //send the JSON data
                out.print(json.toString());
            }
            catch(Exception e)
            {
                e.printStackTrace();
                logger.error("ERROR Exception in servlet ajax_lookup_jobs:=" + e);
                stringBuilder.setLength(0);
                stringBuilder.append("Error in retrieving users list");
                out.print(stringBuilder.toString());

            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

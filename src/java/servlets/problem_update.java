/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import com.google.gson.Gson;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.Tika;
import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;

@WebServlet("/problem_update")
@MultipartConfig()
/**
 *
 * @author Campbellr_2
 */
public class problem_update extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();    
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
    private static SimpleDateFormat form_format = new SimpleDateFormat("HH:mm / dd-MMM-YY"); //10:42 / 24-Jan-20
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            String user_tz_name = "US/Eastern";
            String user_tz_time = "-05:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "US/Eastern";
                    user_tz_time = "-05:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "US/Eastern";
                user_tz_time = "-05:00";
            }
            int id;

            String status = "New";
            String final_status = "";
            int create_by_id = 0;
            String priority = "";

            int assigned_group_id = 0;
            int assigned_to_id = 0;
            String name = "";
            String description = "";
            String notes = "";
            int knowledge_article_id = NumberUtils.toInt(request.getParameter("knowledge_article_id"), 0);
            String close_linked_incidents = StringUtils.defaultString(request.getParameter("close_linked_incidents"));
            String closed_reason = StringUtils.defaultString(request.getParameter("closed_reason"));
            java.util.Date now = new java.util.Date();
            //java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
            String create_date = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
            String status_date = create_date;
            CouchDbClient dbClient = null;

            if (session.getAttribute("authenticated") == null) 
            {
                response.sendRedirect("index.jsp");
            }
            else
            {
                //do work here
                try
                {                
                    String context_dir = request.getServletContext().getRealPath("");
                    Connection con = db.db_util.get_contract_connection(context_dir, session);
                    //get all the parameters
                    id = NumberUtils.toInt(request.getParameter("id"), 0);
                    if (id == 0){
                        throw new Exception("Id is null");
                    }
                    try{status = request.getParameter("status");}catch (Exception e){status = "New";}
                    try{final_status = request.getParameter("final_status");}catch (Exception e){final_status = "";}
                    try{create_by_id = Integer.parseInt(request.getParameter("create_by_id"));}catch (Exception e){create_by_id=0;}
                    //create_by_username:Vera.Mcfarland154 //not needed
                    try{priority = request.getParameter("priority");}catch (Exception e){priority = "";}
                    //create_date
                    //Create date already defined at declaration
                    try{assigned_group_id = Integer.parseInt(request.getParameter("assigned_group_id"));}catch (Exception e){assigned_group_id=0;}
                    //assigned_group_name:Helpdesk
                    try{assigned_to_id = Integer.parseInt(request.getParameter("assigned_to_id"));}catch (Exception e){assigned_to_id=0;}
                    //assigned_to_name:Ralph.Byers2783  //not needed
                    try{name = request.getParameter("name");}catch (Exception e){name="";}
                    try{description = request.getParameter("description");}catch (Exception e){description="";}
                    try{notes = request.getParameter("notes");}catch (Exception e){notes="";}
                    
                    String update_problems_table = "UPDATE problems SET "
                        + "priority = ?,"
                        + "name = ?,"
                        + "description = ?,"
                        + "final_status = ?,"
                        + "status = ?,"
                        + "status_date = ?,"
                        + "assigned_group_id = ?,"
                        + "assigned_to_id = ?,"
                        + "notes = ?,"
                        + "knowledge_article_id = ? "
                        + "WHERE id = ?";

                    PreparedStatement stmt = con.prepareStatement(update_problems_table);
                    stmt.setString(1,priority);
                    stmt.setString(2,name);
                    stmt.setString(3,description);
                    stmt.setString(4,final_status);
                    stmt.setString(5,status);
                    stmt.setString(6,status_date);
                    stmt.setInt(7,assigned_group_id);
                    stmt.setInt(8,assigned_to_id);
                    stmt.setString(9,notes);
                    stmt.setInt(10,knowledge_article_id);
                    stmt.setInt(11,id);
                    int affectedRows = stmt.executeUpdate();
                    if (affectedRows == 0) {
                        throw new SQLException("Update problem failed");
                    }
                    //stmt.close();

                    String insert_problem_history_table = "INSERT INTO problem_history (problem_id,priority,name,description,create_date,create_by_id,final_status,status,status_date,assigned_group_id,assigned_to_id,notes,knowledge_article_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

                    
                    PreparedStatement stmt2 = con.prepareStatement(insert_problem_history_table);
                    stmt2.setInt(1,id);
                    stmt2.setString(2,priority);
                    stmt2.setString(3,name);
                    stmt2.setString(4,description);
                    stmt2.setString(5,create_date);
                    stmt2.setInt(6,create_by_id);
                    stmt2.setString(7,final_status);
                    stmt2.setString(8,status);
                    stmt2.setString(9,status_date);
                    stmt2.setInt(10,assigned_group_id);
                    stmt2.setInt(11,assigned_to_id);
                    stmt2.setString(12,notes);
                    stmt2.setInt(13,knowledge_article_id);
                    stmt2.execute();
                    
                    // do attachments
                    try
                    {
                        LinkedHashMap props = support.config.get_config(context_dir);
                        dbClient = db.couchdb.get_client(props);
                        if (dbClient == null)
                        {
                            throw new Exception("dbClient is null");
                        }
                        String user_id = session.getAttribute("user_id").toString();
                        String name_on_disk;
                        String file_name;
                        Tika tika = new Tika();

                        for (Part filePart : request.getParts()) {
        //                    logger.info("filepart:" + filePart);
                            String fileName = filePart.getSubmittedFileName();
                            if (fileName != null && !"".equals(fileName)) 
                            {
                                long file_size = filePart.getSize();
                                String mimeType = tika.detect(filePart.getInputStream());
                                file_name = fileName.replaceAll("[^a-zA-Z0-9\\._]+", "_");

                                UUID Uuid = UUID.randomUUID(); //Generates random UUID                                  
                                String uuid = Uuid.toString();

                                Map<String, Object> map = new HashMap<>(); 
                                map.put("_id", uuid);
                                map.put("Filename", file_name);
                                map.put("MIME_type", mimeType);
                                map.put("FileSize", file_size);
                                map.put("DateUploaded", now);
                                map.put("UploadedBy", user_id);
                                map.put("AssociatedObject", "Problem");
                                map.put("AssociatedObjectId", id);

                                Response dbclient_response = dbClient.save(map);
                                name_on_disk = uuid + "/"+ file_name; 

                                dbclient_response = dbClient.saveAttachment(filePart.getInputStream(), file_name, mimeType, dbclient_response.getId(), dbclient_response.getRev());

                                if (dbclient_response.getError() == null)
                                {
                                    String query_attachment = "INSERT INTO problem_attachements (`problem_id`, `uuid`,`name_on_disk`,`file_name`,`file_date`,`file_size`,`rev`,`uploaded_by`) VALUES (?,?,?,?,?,?,?,?)";
                                    PreparedStatement stmt_attachment = con.prepareStatement(query_attachment);
                                    stmt_attachment.setInt(1, id);
                                    stmt_attachment.setString(2, uuid);
                                    stmt_attachment.setString(3, name_on_disk);
                                    stmt_attachment.setString(4, file_name);
                                    Timestamp file_date = new Timestamp(now.getTime());
                                    stmt_attachment.setTimestamp(5, file_date);
                                    stmt_attachment.setInt(6, Math.toIntExact(file_size));
                                    stmt_attachment.setString(7, dbclient_response.getRev());
                                    stmt_attachment.setInt(8, Integer.parseInt(user_id));
                                    stmt_attachment.execute();
                                } else {
                                    logger.error("CouchDB attachment upload error: {}", dbclient_response.getError());
                                }

                            }

                        }
                    } catch (Exception e)
                    {
                        logger.error("ERROR attachments process in servlet problem_update:=" + e);
                    }

                    try
                    {
                        String[] attachments_delete_ids = request.getParameterValues("attachment_delete_id");
                        if (attachments_delete_ids != null && attachments_delete_ids.length > 0)
                        {
                            ArrayList<String[]> attachments = db.get_problems.get_attachments_by_problem_id(con, String.valueOf(id));
                            ArrayList <String> deleted_ids = new ArrayList();

                            for (int i = 0; i < attachments.size(); i++)
                            {
                                String[] attachment = attachments.get(i);
                                if (Arrays.stream(attachments_delete_ids).anyMatch(attachment[0]::equals))
                                {
                                    try
                                    {
                                        dbClient.remove(attachment[2], attachment[7]);
                                        deleted_ids.add(attachment[0]);
                                    } catch (Exception e)
                                    {
                                    }
                                }
                            }
                            if (deleted_ids.size() > 0)
                            {
                                String query_attachment = "DELETE FROM problem_attachements WHERE problem_id = ? AND id IN (" + String.join(",", deleted_ids) + ")";
                                PreparedStatement stmt_attachment = con.prepareStatement(query_attachment);

                                stmt_attachment.setInt(1, id);
                                stmt_attachment.execute();
                            }

                        }

                    } catch (Exception e)
                    {
                        logger.error("ERROR Exception in attachment deletion servlet problem_update:=" + e);
                    }

                    // do linked incidents
                    String request_incident_ids = StringUtils.defaultString(request.getParameter("incident_ids"));
                    if (!request_incident_ids.equals("") && !request_incident_ids.equals("[]"))
                    {
                        Gson gson = new Gson();
                        String[] incident_ids = gson.fromJson(request_incident_ids, String[].class); 
                        if (incident_ids.length > 0)
                        {
                            String unlink_incidents_query = "DELETE FROM incident_problem WHERE problem_id = ? AND NOT incident_id IN (";
                            for (int i = 0; i < incident_ids.length; i++)
                            {
                                unlink_incidents_query += "?" + (i != (incident_ids.length-1) ? "," : "");
                            }
                            unlink_incidents_query += ")";
                            PreparedStatement unlink_stmt = con.prepareStatement(unlink_incidents_query);
                            unlink_stmt.setInt(1, id); 
                            for (int i = 0; i < incident_ids.length; i++)
                            {
                                String incident_id = incident_ids[i];
                                unlink_stmt.setString(i + 2, incident_id); 
                            }
                            unlink_stmt.execute();
                        }
//                        logger.info(unlink_stmt.toString());

                        String link_incidents_query = "INSERT IGNORE incident_problem (problem_id, incident_id) VALUES (?, ?)";
                        PreparedStatement link_stmt = con.prepareStatement(link_incidents_query);
                        for (String incident_id : incident_ids)
                        {
                            if (!incident_id.equals("")) 
                            {
                                link_stmt.setInt(1, id); 
                                link_stmt.setString(2, incident_id); 
                                link_stmt.execute();
                            }
                        }

                        // processing linked incidents auto closure
                        if (close_linked_incidents.equalsIgnoreCase("on"))
                        {
                            String state_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(support.date_utils.now_in_utc());

                            for (String incident_id : incident_ids)
                            {
                                if (!incident_id.equals("")) 
                                {

                                    String incident_update_query = "UPDATE incidents SET state = ?, state_date = ?, closed_date = ?, closed_reason = ? WHERE id = ?";
                                    PreparedStatement stmt_incident = con.prepareStatement(incident_update_query);
                                    stmt_incident.setString(1, "Closed"); //id
                                    stmt_incident.setString(2, state_date); //id
                                    stmt_incident.setString(3, state_date); //id
                                    stmt_incident.setString(4, closed_reason); //id
                                    stmt_incident.setString(5, incident_id); //id
                                    stmt_incident.execute();

                                    String[] incident_info = db.get_incidents.incident_by_id(con, incident_id);
                                    //insert into incident_history
                                    String insert_incident_history_table = "INSERT INTO incident_history (incident_id,incident_type,incident_time,caller_id,caller_group_id,location,department,site,company,category,subcategory,ci_id,impact,urgency,priority,description,create_date,create_by_id,contact_method,state,state_date,assigned_group_id,assigned_to_id,notes,desk_notes,related,first_contact_resolution,closed_date,closed_reason,pending_date,pending_reason,external_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                    PreparedStatement stmt_history = con.prepareStatement(insert_incident_history_table);
                                    stmt_history.setString(1, incident_id); //id
                                    stmt_history.setString(2, incident_info[1]); //incident_type
                                    stmt_history.setString(3, incident_info[2]);//incident_time
                                    stmt_history.setString(4, incident_info[3]);//caller_id
                                    stmt_history.setString(5, incident_info[4]);//caller_group_id
                                    stmt_history.setString(6, incident_info[5]); //location
                                    stmt_history.setString(7, incident_info[6]); //department
                                    stmt_history.setString(8, incident_info[7]);//site=8
                                    stmt_history.setString(9, incident_info[8]);//company=9,
                                    stmt_history.setString(10, incident_info[9]);//category=10,
                                    stmt_history.setString(11, incident_info[10]);//subcategory=11,
                                    stmt_history.setString(12, incident_info[11]);//ci_id=12,
                                    stmt_history.setString(13, incident_info[12]);//impact=13,
                                    stmt_history.setString(14, incident_info[13]);//urgency=14,
                                    stmt_history.setString(15, incident_info[14]);//priority=15,
                                    stmt_history.setString(16, incident_info[15]);//description=16,
                                    stmt_history.setString(17, incident_info[16]);//create_date=17,
                                    stmt_history.setString(18, incident_info[17]);//create_by_id=18,
                                    stmt_history.setString(19, incident_info[18]);//contact_method=19,
                                    stmt_history.setString(20, "Closed");//state=20,
                                    stmt_history.setString(21, state_date);//state_date=20,
                                    stmt_history.setString(22, incident_info[21]);//assigned_group_id=22,
                                    stmt_history.setString(23, incident_info[22]);//assigned_to_id=23,
                                    stmt_history.setString(24, incident_info[23]);//notes=24,
                                    stmt_history.setString(25, incident_info[24]);//desk_notes=25,
                                    stmt_history.setString(26, incident_info[25]);//related=26,
                                    stmt_history.setString(27, incident_info[26]);//first_contact_resolution=27,
                                    stmt_history.setString(28, state_date);//closed_date=28,
                                    stmt_history.setString(29, closed_reason);//closed_reason=29,
                                    stmt_history.setString(30, (incident_info[29].equals("") ? null : incident_info[29]));//pending_date=30,
                                    stmt_history.setString(31, incident_info[30]);//pending_reason=31,
                                    stmt_history.setString(32, incident_info[31]);//external_id=32    
                                    stmt_history.execute();


                                    String caller_incident_close[] = db.get_notifications.specific_global(con, "caller_incident_close");
                                    if(caller_incident_close[2].equalsIgnoreCase("true"))
                                    {
                                        //get subject
                                        String incident_subject_format = StringEscapeUtils.unescapeHtml4(caller_incident_close[3]);
                                        String subject = support.format_email.text(con, "incident", String.valueOf(id), incident_subject_format, user_tz_time);
                                        //get body
                                        String incident_body_format = StringEscapeUtils.unescapeHtml4(caller_incident_close[4]);
                                        String body = support.format_email.text(con, "incident", String.valueOf(id), incident_body_format, user_tz_time);

                                        String user_info[] = db.get_users.by_id(con, String.valueOf(incident_info[3]));
                                        String email = user_info[15];
                                        if(!email.equalsIgnoreCase(""))
                                        {
                                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                                        }
                                    } 
                                    //assigned USER
                                    //System.out.println("PRE start personal assigned to me");
                                    String assigned_to_me_incident_close[] = db.get_notifications.user_notifications_for_user_by_notify_on(con, String.valueOf(incident_info[22]), "incident_close_assigned_to_me");
                                    if(assigned_to_me_incident_close[3].equalsIgnoreCase("true"))
                                    {
                                        //get subject
                                        String incident_subject_format = StringEscapeUtils.unescapeHtml4(assigned_to_me_incident_close[4]);
                                        String subject = support.format_email.text(con, "incident", String.valueOf(id), incident_subject_format, user_tz_time);
                                        //get body
                                        String incident_body_format = StringEscapeUtils.unescapeHtml4(assigned_to_me_incident_close[5]);
                                        String body = support.format_email.text(con, "incident", String.valueOf(id), incident_body_format, user_tz_time);

                                        String user_info[] = db.get_users.by_id(con, String.valueOf(incident_info[22]));
                                        String email = user_info[15];
                                        if(!email.equalsIgnoreCase(""))
                                        {
                                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                                        }
                                    }
                                    //assigned to group
                                    ArrayList<String[]> close_assigned_to_group = db.get_notifications.user_group_notifications_for_user_by_notify_on(con, String.valueOf(incident_info[21]), "incident_close_assigned_to_group");
                                    for(int a = 0; a < close_assigned_to_group.size(); a++)
                                    {
                                        String this_assigned_to_group[] = close_assigned_to_group.get(a);
                                        //get subject
                                        String incident_subject_format = StringEscapeUtils.unescapeHtml4(this_assigned_to_group[3]);
                                        String subject = support.format_email.text(con, "incident", String.valueOf(id), incident_subject_format, user_tz_time);
                                        //get body
                                        String incident_body_format = StringEscapeUtils.unescapeHtml4(this_assigned_to_group[4]);
                                        String body = support.format_email.text(con, "incident", String.valueOf(id), incident_body_format, user_tz_time);

                                        String user_info[] = db.get_users.by_id(con, String.valueOf(this_assigned_to_group[1]));
                                        String email = user_info[15];
                                        if(!email.equalsIgnoreCase(""))
                                        {
                                            //System.out.println("Sending to emailer  personal assigned to group resolve SUBJECT=" + subject + " BODY=" + body);
                                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                                        }
                                    }


                                }
                            }


                        }
                    }


                    //do custom fields
                    //get active custom fields
                    ArrayList<String[]> active_custom_fields = db.get_custom_fields.active_for_form(con, "problem"); 
                    //System.out.println("Number of custom problem fields=" + active_custom_fields.size());
                    String replace_data = "REPLACE INTO custom_form_fields_data (`form`,`object_id`,`field_name`,`field_value`) VALUES (?,?,?,?)";
                    PreparedStatement stmt3 = con.prepareStatement(replace_data);
                    String insert_history = "INSERT INTO `custom_form_fields_history` (`date`,`form`,`object_id`,`field_name`,`field_value`) VALUES (?,?,?,?,?)";
                    PreparedStatement stmt4 = con.prepareStatement(insert_history);
                    
                    //get the form values
                    for(int a = 0; a < active_custom_fields.size();a++)
                    {
                        String this_custom_field[] = active_custom_fields.get(a);
                        String this_field_name = this_custom_field[3];
                        String this_field_value = "";
                        String this_field_type = this_custom_field[7];
                        try
                        {
                            if(this_field_type.equalsIgnoreCase("select"))
                            {
                                this_field_value = support.util.check_for_null(request.getParameter(this_field_name));                                
                            }
                            else if(this_field_type.equalsIgnoreCase("text"))
                            {
                                this_field_value = support.util.check_for_null(request.getParameter(this_field_name));                                
                            }
                            else if(this_field_type.equalsIgnoreCase("textarea"))
                            {
                                this_field_value = support.util.check_for_null(request.getParameter(this_field_name));                                
                            }
                            else if(this_field_type.equalsIgnoreCase("checkbox"))
                            {
                                this_field_value = support.util.check_for_null(request.getParameter(this_field_name));    
                                if(this_field_value == null)
                                {
                                    this_field_value = "false";
                                }
                                else
                                {
                                    this_field_value = "true";
                                }
                            }
                            else if(this_field_type.equalsIgnoreCase("people-lookup"))
                            {
                                this_field_value = support.util.check_for_null(request.getParameter(this_field_name));                                
                            }
                            else if(this_field_type.equalsIgnoreCase("group-lookup"))
                            {
                                this_field_value = support.util.check_for_null(request.getParameter(this_field_name));                                
                            }
                            else if(this_field_type.equalsIgnoreCase("date"))
                            {
                                this_field_value = support.util.check_for_null(request.getParameter(this_field_name));  
                                if(this_field_value.equalsIgnoreCase(""))
                                {
                                    this_field_value = null;
                                }
                                try
                                {
                                    String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(this_field_value).getTime()));
                                    ZonedDateTime this_field_time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                                    this_field_value = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(this_field_time_utc);
                                }
                                catch (Exception e)
                                {
                                    this_field_value = null;
                                }
                            }
                        }
                        catch(Exception e)
                        {
                            this_field_value = "";
                        }
                        stmt3.setString(1, "problem"); 
                        stmt3.setInt(2, id); 
                        stmt3.setString(3, this_custom_field[3]); 
                        stmt3.setString(4, this_field_value); 
                        stmt3.execute();
                        
                        stmt4.setString(1, create_date);
                        stmt4.setString(2, "problem");
                        stmt4.setInt(3, id); 
                        stmt4.setString(4, this_custom_field[3]);
                        stmt4.setString(5, this_field_value);
                        stmt4.execute();
                    }
                    //do problem new notification
                    
                    //do global notification

                    //do personal notifications                    
                    //get problem_create_assigned_to_me  FROM notifications_user WHERE notifications_user.user_id = assigned_to_id
                    //System.out.println("PRE start personal assigned to me");
/*
                    String assigned_to_me_problem_create[] = db.get_notifications.user_notifications_for_user_by_notify_on(con, String.valueOf(assigned_to_id), "problem_create_assigned_to_me");
                    if(assigned_to_me_problem_create[3].equalsIgnoreCase("true"))
                    {
                        //get subject
                        String problem_subject_format = StringEscapeUtils.unescapeHtml4(assigned_to_me_problem_create[4]);
                        String subject = support.format_email.text(con, "problem", String.valueOf(id), problem_subject_format,user_tz_time);
                        //get body
                        String problem_body_format = StringEscapeUtils.unescapeHtml4(assigned_to_me_problem_create[5]);
                        String body = support.format_email.text(con, "problem", String.valueOf(id), problem_body_format, user_tz_time);
                        
                        String user_info[] = db.get_users.by_id(con, String.valueOf(assigned_to_id));
                        String email = user_info[15];
                        if(!email.equalsIgnoreCase(""))
                        {
                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                        }
                    }  
                    //do personal group notifications                    
                    //problem_create_assigned_to_group
                    ArrayList<String[]> create_assigned_to_group = db.get_notifications.user_group_notifications_for_user_by_notify_on(con, String.valueOf(assigned_group_id), "problem_create_assigned_to_group");
                    for(int a = 0; a < create_assigned_to_group.size(); a++)
                    {
                        String this_assigned_to_group[] = create_assigned_to_group.get(a);
                        //get subject
                        String problem_subject_format = StringEscapeUtils.unescapeHtml4(this_assigned_to_group[3]);
                        String subject = support.format_email.text(con, "problem", String.valueOf(id), problem_subject_format,user_tz_time);
                        //get body
                        String problem_body_format = StringEscapeUtils.unescapeHtml4(this_assigned_to_group[4]);
                        String body = support.format_email.text(con, "problem", String.valueOf(id), problem_body_format,user_tz_time);
                        
                        String user_info[] = db.get_users.by_id(con, String.valueOf(this_assigned_to_group[1]));
                        String email = user_info[15];
                        if(!email.equalsIgnoreCase(""))
                        {
                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                        }
                    }
                    */  
                    String RedirectURL = "problem_list.jsp?id=" + id ;
                    response.sendRedirect(RedirectURL);

                }
                catch(Exception e)
                {
                    logger.error("ERROR Exception in servlet problem_update:=" + e);
                    java.util.logging.Logger.getLogger(admin_service_catalog_edit.class.getName()).log(Level.SEVERE, null, e);
                } finally {
                    if (dbClient != null )
                    {
                        dbClient.shutdown();
                    }
                }
          
            }
        }
    }
    // <editor-fold defaultstatus="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class ajax_lookup_incident_sla_parameters extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
        String field_name = request.getParameter("field_name");  
        String query = "";
        response.setContentType("text/html;charset=UTF-8");
        PreparedStatement stmt;
        boolean first_record = true;
        boolean has_data = false;
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                
                if(field_name.equalsIgnoreCase("contact_method"))
                {
                    query = "SELECT DISTINCT contact_method FROM incidents";                    
                    stmt = con.prepareStatement(query); 
                    ResultSet rs = stmt.executeQuery();
                    stringBuilder.append("[");
                    while(rs.next())
                    {
                        has_data = true;
                        if(first_record)
                        {
                            stringBuilder.append("{");
                            stringBuilder.append("\"value\": \"" + rs.getString("contact_method") + "\",");
                            stringBuilder.append("\"label\": \"" + rs.getString("contact_method") + "\"");                        
                            stringBuilder.append("}");
                        }
                        else
                        {
                            stringBuilder.append(",{");
                            stringBuilder.append("\"value\": \"" + rs.getString("contact_method") + "\",");
                            stringBuilder.append("\"label\": \"" + rs.getString("contact_method") + "\"");
                            stringBuilder.append("}");
                        }
                        first_record = false;
                    }
                    stringBuilder.append("]");                
                    if(!has_data)
                    {
                        stringBuilder.setLength(0);
                        stringBuilder.append("[{\"value\":\"\",\"label\":\"\"}]");
                    } 
                    stmt.close();
                }
                else if(field_name.equalsIgnoreCase("category"))
                {
                    query = "SELECT DISTINCT category FROM incidents";                    
                    stmt = con.prepareStatement(query); 
                    ResultSet rs = stmt.executeQuery();
                    stringBuilder.append("[");
                    while(rs.next())
                    {
                        has_data = true;
                        if(first_record)
                        {
                            stringBuilder.append("{");
                            stringBuilder.append("\"value\": \"" + rs.getString("category") + "\",");
                            stringBuilder.append("\"label\": \"" + rs.getString("category") + "\"");                        
                            stringBuilder.append("}");
                        }
                        else
                        {
                            stringBuilder.append(",{");
                            stringBuilder.append("\"value\": \"" + rs.getString("category") + "\",");
                            stringBuilder.append("\"label\": \"" + rs.getString("category") + "\"");
                            stringBuilder.append("}");
                        }
                        first_record = false;
                    }
                    stringBuilder.append("]");                
                    if(!has_data)
                    {
                        stringBuilder.setLength(0);
                        stringBuilder.append("[{\"value\":\"\",\"label\":\"\"}]");
                    } 
                    stmt.close();
                }
                else if(field_name.equalsIgnoreCase("subcategory"))
                {
                    query = "SELECT DISTINCT subcategory FROM incidents";                    
                    stmt = con.prepareStatement(query); 
                    ResultSet rs = stmt.executeQuery();
                    stringBuilder.append("[");
                    while(rs.next())
                    {
                        has_data = true;
                        if(first_record)
                        {
                            stringBuilder.append("{");
                            stringBuilder.append("\"value\": \"" + rs.getString("subcategory") + "\",");
                            stringBuilder.append("\"label\": \"" + rs.getString("subcategory") + "\"");                        
                            stringBuilder.append("}");
                        }
                        else
                        {
                            stringBuilder.append(",{");
                            stringBuilder.append("\"value\": \"" + rs.getString("subcategory") + "\",");
                            stringBuilder.append("\"label\": \"" + rs.getString("subcategory") + "\"");
                            stringBuilder.append("}");
                        }
                        first_record = false;
                    }
                    stringBuilder.append("]");                
                    if(!has_data)
                    {
                        stringBuilder.setLength(0);
                        stringBuilder.append("[{\"value\":\"\",\"label\":\"\"}]");
                    } 
                    stmt.close();
                }
                else if(field_name.equalsIgnoreCase("assigned_group_id"))
                {
                    query = "SELECT * FROM `groups`";                    
                    stmt = con.prepareStatement(query); 
                    ResultSet rs = stmt.executeQuery();
                    stringBuilder.append("[");
                    while(rs.next())
                    {
                        has_data = true;
                        if(first_record)
                        {
                            stringBuilder.append("{");
                            stringBuilder.append("\"value\": \"" + rs.getString("id") + "\",");
                            stringBuilder.append("\"label\": \"" + rs.getString("name") + "\"");                        
                            stringBuilder.append("}");
                        }
                        else
                        {
                            stringBuilder.append(",{");
                            stringBuilder.append("\"value\": \"" + rs.getString("id") + "\",");
                            stringBuilder.append("\"label\": \"" + rs.getString("name") + "\"");
                            stringBuilder.append("}");
                        }
                        first_record = false;
                    }
                    stringBuilder.append("]");                
                    if(!has_data)
                    {
                        stringBuilder.setLength(0);
                        stringBuilder.append("[{\"value\":\"\",\"label\":\"\"}]");
                    } 
                    stmt.close();
                }
                else if(field_name.equalsIgnoreCase("caller_group_id"))
                {
                    query = "SELECT * FROM `groups`";                    
                    stmt = con.prepareStatement(query); 
                    ResultSet rs = stmt.executeQuery();
                    stringBuilder.append("[");
                    while(rs.next())
                    {
                        has_data = true;
                        if(first_record)
                        {
                            stringBuilder.append("{");
                            stringBuilder.append("\"value\": \"" + rs.getString("id") + "\",");
                            stringBuilder.append("\"label\": \"" + rs.getString("name") + "\"");                        
                            stringBuilder.append("}");
                        }
                        else
                        {
                            stringBuilder.append(",{");
                            stringBuilder.append("\"value\": \"" + rs.getString("id") + "\",");
                            stringBuilder.append("\"label\": \"" + rs.getString("name") + "\"");
                            stringBuilder.append("}");
                        }
                        first_record = false;
                    }
                    stringBuilder.append("]");                
                    if(!has_data)
                    {
                        stringBuilder.setLength(0);
                        stringBuilder.append("[{\"value\":\"\",\"label\":\"\"}]");
                    } 
                    stmt.close();
                }
                
               /*<option value="contact_method">Contact Method</option>
                <option value="category">Category</option>
                <option value="subcategory">Sub Category</option>
                <option value="assigned_group_id">Assigned Group</option>
                <option value="caller_group_id">Users Group</option>*/ 
                    
                con.close();
                //send the JSON data
                out.print(stringBuilder.toString());
            }
            catch(Exception e)
            {
                logger.error("ERROR Exception in servlet ajax_lookup_incident_company:=" + e);
            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

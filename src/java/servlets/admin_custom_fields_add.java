/*
<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class admin_custom_fields_add extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
    

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            String html_escaped = "";
            String form = request.getParameter("form");  
            String index = request.getParameter("index");  
            String field_name = request.getParameter("field_name");  
            String field_label = request.getParameter("field_label");  
            String default_text = request.getParameter("default_text");  
            String field_type = request.getParameter("field_type");  
            String number_cols = request.getParameter("number_cols");              
            String required = request.getParameter("required");   
            String selected = request.getParameter("selected");
            if(selected == null)
            {
                selected = "";
            }
            try
            {
                if(required == null)
                {
                    required = "false";
                }
                else
                {
                    required = "true";
                }
            }
            catch(Exception e)
            {
                required = "false";
            }
            String active = request.getParameter("active");              
            try
            {
                if(active == null)
                {
                    active = "false";
                }
                else
                {
                    active = "true";
                }
            }
            catch(Exception e)
            {
                active = "false";
            }
            
            ArrayList<String[]> options = new ArrayList();
            //if field_type is select or multi-select then get the options
            if(field_type.equalsIgnoreCase("select"))
            {
                Enumeration paramNames = request.getParameterNames();
                while(paramNames.hasMoreElements()) 
                {
                    String paramName = (String)paramNames.nextElement();
                    String paramValues[] = request.getParameterValues(paramName);
                    String paramValue = paramValues[0]; 
                    if(paramName.startsWith("option_value~"))
                    {
                        String index_array[] =  paramName.split("~");
                        String item_number = index_array[1].replace("~", "");
                        String temp[] = {"","",""}; //value   label selected
                        temp[0] = paramValue;
                        temp[1] = request.getParameter("option_label~" + item_number);  
                        if(selected.equalsIgnoreCase(item_number))
                        {
                            temp[2] = "true";
                        }
                        else
                        {
                            temp[2] = "false";
                        }
                        options.add(temp);
                    }
                }
            }
            
            //build the HTML for trhe Select field type
            if(field_type.equalsIgnoreCase("Select"))
            {
                StringBuilder sb = new StringBuilder();
                sb.append("{"); 
                sb.append("\"elements\": {");
                sb.append("\"element\": [");
                for(int a = 0; a < options.size(); a++)
                {
                    String this_option[] = options.get(a);
                    if(a != 0)
                    {
                        sb.append(",");
                    }
                    
                    sb.append("{");
                    sb.append("\"id\": \"" + a + "\",");
                    sb.append("\"value\": \"" + this_option[0] + "\",");
                    sb.append("\"label\": \"" + this_option[1] + "\",");
                    sb.append("\"selected\": \"" + this_option[2] + "\"");
                    sb.append("}");                    
                }
                sb.append("]"); 
                sb.append("}"); 
                sb.append("}"); 
                
                
                /*{
                    "elements": {
                      "element": [
                        {
                          "id": "1",
                          "value": "Tom",
                          "label": "Cruise",
                          "selected": "false"
                        },
                        {
                          "id": "2",
                          "value": "Tom",
                          "label": "Fairer",
                          "selected": "true"
                        },
                        {
                          "id": "3",
                          "value": "Tom",
                          "label": "Basser",
                          "selected": "false"
                        }
                      ]
                    }
                  }*/
                //html escape the code
                html_escaped = StringEscapeUtils.escapeHtml4(sb.toString());
                
            }
            else if(field_type.equalsIgnoreCase("Text"))
            {
                
            }
            else if(field_type.equalsIgnoreCase("TextArea"))
            {
                
            }
            else if(field_type.equalsIgnoreCase("Checkbox"))
            {
                
            }
            else if(field_type.equalsIgnoreCase("People-Lookup"))
            {
                
            }
            else if(field_type.equalsIgnoreCase("Group-Lookup"))
            {
                
            }
            else if(field_type.equalsIgnoreCase("Date"))
            {
                
            }
            
            //save it to the database
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement("INSERT INTO custom_form_fields (`form`,`index`,`field_name`,`field_label`,`default_text`,`number_cols`,`field_type`,`field_values`,`required`,`active`) VALUES (?,?,?,?,?,?,?,?,?,?)"); 
                stmt.setString(1,form);
                stmt.setString(2,index);
                stmt.setString(3,field_name);
                stmt.setString(4,field_label);
                stmt.setString(5,default_text);
                stmt.setString(6,number_cols);
                stmt.setString(7,field_type);
                stmt.setString(8,html_escaped);
                stmt.setString(9,required);
                stmt.setString(10,active);
                stmt.executeUpdate();
                stmt.close();
                con.close();
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_custom_fields_add:=" + e);
            }
            response.sendRedirect("admin_custom_fields.jsp?success=true");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

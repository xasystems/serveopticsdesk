/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 * 
 * 
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author rcampbell
 */
public class admin_self_service extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            String self_service_enabled = request.getParameter("self_service_enabled");  
            if(self_service_enabled == null)
            {
                self_service_enabled = "false";
            }
            else
            {
                self_service_enabled = "true";
            }
            //String self_service_login_required = request.getParameter("self_service_login_required");  
            //String self_service_enable_captcha = request.getParameter("self_service_enable_captcha");               
            
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement("REPLACE INTO configuration_properties VALUES (?,?)");
                stmt.setString(1,"self_service_enabled");
                stmt.setString(2,self_service_enabled);
                stmt.executeUpdate(); 
                /*stmt.setString(1,"self_service_login_required");
                stmt.setString(2,self_service_login_required);
                stmt.executeUpdate(); 
                stmt.setString(1,"self_service_enable_captcha");
                stmt.setString(2,self_service_enable_captcha);
                stmt.executeUpdate(); */
                
                stmt.close();
                con.close();
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_self_service:=" + e);
            }
            response.sendRedirect("admin_self_service.jsp?success=true");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

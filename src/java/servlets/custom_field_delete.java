/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author rcampbell
 */
public class custom_field_delete extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        PreparedStatement stmt = null;
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String id = request.getParameter("id");
            try 
            {
                stmt = con.prepareStatement("DELETE FROM custom_form_fields WHERE id=?");
                stmt.setString(1,id);
                stmt.executeUpdate();
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet custom_field_delete custom_form_fields:=" + e);
            }
            try 
            {
                //delete the current data
                stmt = con.prepareStatement("DELETE FROM custom_field_data WHERE id=?");
                stmt.setString(1,id);
                stmt.executeUpdate();
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet custom_field_delete custom_field_data:=" + e);
            }
            try 
            {
                //delete the history data
                stmt = con.prepareStatement("DELETE FROM custom_field_history WHERE id=?");
                stmt.setString(1,id);
                stmt.executeUpdate();
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet custom_field_delete custom_field_history:=" + e);
            }
            try 
            {
                stmt.close();
                con.close();
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet custom_field_delete:=" + e);
            }
        }
        response.sendRedirect("admin_custom_fields.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

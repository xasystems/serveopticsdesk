/*
* Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
*/

package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rcampbell
 */
@WebServlet("/generate_password_hash_for_users")
public class generate_password_hash_for_users extends HttpServlet 
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        PreparedStatement stmt;
        PreparedStatement stmt_update;
        Connection con = null;
        //get contract db connection
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {
                if (session.getAttribute("authenticated") == null) 
                {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                    return;
                }
                con = db.db_util.get_contract_connection(context_dir, session);
                //id=0	*username=1	password=2	first=3	mi=4	last=5	address_1=6	Address_2=7	city=8	state=9	zip=10	location=11	department=12	site=13	
                //company=14	email=15	phone_office=16 	phone_mobile=17 	notes=18	vip=19	is_admin=20	tz_name=21	tz_time=22	
                //external_id=23	self_service=24	role=25
                stmt = con.prepareStatement("SELECT * FROM `users` WHERE not(isnull(password)) and (isnull(password_salt) or isnull(password_hash))");
                ResultSet rs = stmt.executeQuery();
                //get the users in an ArrayList            
                while(rs.next())
                {
                    String unencrypted_password = "";
                    String password = "";
                    Integer user_id = rs.getInt("id");
                    try
                    {
                        if(!rs.getString("password").equalsIgnoreCase("null") || rs.getString("password") == null)
                        {
                            unencrypted_password = support.encrypt_utils.decrypt(context_dir, rs.getString("password"));
                        }
                    }
                    catch(Exception e)
                    {
                        unencrypted_password = "";
                    }
                    if (!unencrypted_password.equals(""))
                    {
                        try {
                            ArrayList<byte[]> salt_and_hash = support.encrypt_utils.get_hash(unencrypted_password);
                            stmt_update = con.prepareStatement("UPDATE users SET password = '', password_salt = ?, password_hash = ?  WHERE id=?");
                            stmt_update.setBytes(1,salt_and_hash.get(0));
                            stmt_update.setBytes(2,salt_and_hash.get(1));
                            stmt_update.setInt(3,user_id);
//                            System.out.println("update query=" + stmt_update.toString());
                            stmt_update.executeUpdate();
                        }
                        catch(Exception e)
                        {
                            System.out.println("ERROR Update users Exception in servlet generate_password_hash_for_users:=" + e);
                        }
                    }
                }
            out.println("done");
            
            }
            catch(Exception e)
            {
                System.out.println("ERROR Exception in servlet generate_password_hash_for_users:=" + e);
            }
            
        }        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

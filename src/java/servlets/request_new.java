/*
 * Copyright 2019 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class request_new extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
    private static SimpleDateFormat form_format = new SimpleDateFormat("HH:mm / dd-MMM-YY"); //10:42 / 24-Jan-20
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            java.util.Date now = new java.util.Date();
            java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
            
            int id = 0;
            int external_id = 0;
            java.sql.Timestamp create_date = new java.sql.Timestamp(now.getTime());
            java.sql.Timestamp request_date = null;
            java.sql.Timestamp resolve_date = null;
            java.sql.Timestamp closed_date = null;
            int assigned_group_id = 0;
            int assigned_to_id = 0;
            int create_by_id = 0;
            int requested_for_id = 0;
            int requested_for_group_id = 0;
            String location = "";
            String department = "";
            String site = "";
            String company = ""; 
            String impact = "";
            String urgency = "";
            String priority = "";
            String state = "";
            String contact_method = "";
            String approval = "";
            String price = "";
            String notes = "";
            String desk_notes = "";
            String description = "";
            String closed_reason = "";
            java.sql.Timestamp request_time = null;    
            //do work here
            try
            {                
                String context_dir = request.getServletContext().getRealPath("");
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                //get all the parameters
                id = db.get_requests.next_request_id(con);
                external_id = id;
                //request_date
                try
                {
                    request_date = new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("request_date")).getTime());
                }
                catch(Exception e)
                {
                    request_date = new java.sql.Timestamp(now.getTime());
                }                
                try
                {
                    resolve_date = new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("resolve_date")).getTime());
                }
                catch(Exception e)
                {
                    resolve_date = null;
                }
                try
                {
                    closed_date = new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("closed_date")).getTime());
                }
                catch(Exception e)
                {
                    closed_date = null;
                }
                try{assigned_group_id = Integer.parseInt(request.getParameter("assigned_group_id"));}catch (Exception e){assigned_group_id=0;}
                try{assigned_to_id = Integer.parseInt(request.getParameter("assigned_to_id"));}catch (Exception e){assigned_to_id=0;}
                try{create_by_id = Integer.parseInt(request.getParameter("create_by_id"));}catch (Exception e){create_by_id=0;}
                try{requested_for_id = Integer.parseInt(request.getParameter("requested_for_id"));}catch (Exception e){requested_for_id=0;}
                try{requested_for_group_id = Integer.parseInt(request.getParameter("requested_for_group_id"));}catch (Exception e){requested_for_group_id=0;}
                try{location = request.getParameter("location");}catch (Exception e){location = "";}
                try{department = request.getParameter("department");}catch (Exception e){department = "";}
                try{site = request.getParameter("site");}catch (Exception e){site = "";}
                try{company = request.getParameter("company");}catch (Exception e){company = "";}
                try{priority = request.getParameter("priority");}catch (Exception e){priority = "";}
                try{impact = request.getParameter("impact");}catch (Exception e){impact = "";}
                try{urgency = request.getParameter("urgency");}catch (Exception e){urgency = "";}
                try{state = request.getParameter("state");}catch (Exception e){state = "New";}
                try{contact_method = request.getParameter("contact_method");}catch (Exception e){contact_method = "Phone";}
                try{approval = request.getParameter("approval");}catch (Exception e){approval = null;}
                try{price = request.getParameter("price");}catch (Exception e){price = null;}
                try{notes = request.getParameter("notes");}catch (Exception e){notes = "";}
                try{desk_notes = request.getParameter("desk_notes");}catch (Exception e){desk_notes = "";}
                try{description = request.getParameter("description");}catch (Exception e){description = "";}
                try{closed_reason = request.getParameter("closed_reason");}catch (Exception e){closed_reason = "";}
                System.out.println("requested_for_group_id=" + requested_for_group_id);
                String insert_request_table = "INSERT INTO request (id,external_id,create_date,request_date,resolve_date,closed_date,assigned_group_id,assigned_to_id,create_by_id,requested_for_id,requested_for_group_id,location,department,site,company,impact,urgency,priority,state,contact_method,approval,price,notes,desk_notes,description,closed_reason,request_time) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                PreparedStatement stmt = con.prepareStatement(insert_request_table);
                stmt.setInt(1, id); //id
                stmt.setString(2,String.valueOf(external_id)); //external_id
                stmt.setTimestamp(3,create_date);//create_date
                stmt.setTimestamp(4,request_date);//request_date
                stmt.setTimestamp(5,resolve_date);//resolve_date
                stmt.setTimestamp(6,closed_date);//closed_date
                stmt.setInt(7, assigned_group_id); //assigned_group_id
                stmt.setInt(8, assigned_to_id); //assigned_to_id
                stmt.setInt(9, create_by_id); //create_by_id
                stmt.setInt(10, requested_for_id); //requested_for_id
                stmt.setInt(11, requested_for_group_id); //requested_for_group_id
                stmt.setString(12,location); //location
                stmt.setString(13,department); //department
                stmt.setString(14,site); //site
                stmt.setString(15,company); //company
                stmt.setString(16,impact); //impact
                stmt.setString(17,urgency); //urgency
                stmt.setString(18,priority); //priority
                stmt.setString(19,state); //state
                stmt.setString(20,contact_method); //contact_method
                stmt.setString(21,approval); //approval
                stmt.setString(22,price); //price
                stmt.setString(23,notes); //notes
                stmt.setString(24,desk_notes); //desk_notes
                stmt.setString(25,description); //description
                stmt.setString(26,closed_reason); //closed_reason
                stmt.setTimestamp(27,null);//request_time
                stmt.execute();
                
                
                String RedirectURL = "request_list.jsp";
                response.sendRedirect(RedirectURL);
            }
            catch(Exception e)
            {
                logger.error("ERROR Exception in servlet request_new:=" + e);
            }  
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebServlet("/ajax_lookup_sla_contacts")
/**
 *
 * @author ralph
 */
public class ajax_lookup_sla_contacts extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
//        String user_id = request.getParameter("user_id");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

//        String user_id = request.getParameter("user_id");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        JsonObjectBuilder json_builder = Json.createObjectBuilder();
        JsonObject json;
        
        boolean first_record = true;
        boolean has_data = false;
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {

                if (session.getAttribute("authenticated") == null) 
                {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }

                Connection con = db.db_util.get_contract_connection(context_dir, session);

                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);

                SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
                SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
                SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19

                //SimpleDateFormat filter_format2 = new SimpleDateFormat("yyyy"); //01/12/2019 12:00 AM
                java.util.Date filter_start = new java.util.Date();
                java.util.Date filter_end = new java.util.Date();
                String start = "";
                String end = "";
                //System.out.println("format2=" + filter_format2.parse("2016"));
                //System.out.println("format" + filter_format.parse("01/12/2019 12:00 AM"));

                String sla_id = request.getParameter("sla_id");
                String date_range = "";
                String referer = "";
                try 
                {
                    date_range = request.getParameter("date_range");
                    if (date_range.equalsIgnoreCase("null") || date_range == null) 
                    {
                        date_range = support.filter_dates.past_30_days();
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to past 30 days
                    date_range = support.filter_dates.past_30_days();
                }
                try 
                {
                    referer = request.getParameter("referer");
                    if (referer.equalsIgnoreCase("null") || referer == null) 
                    {
                        referer = "contact_home_support.jsp?date_range=" + date_range;
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to past 30 days
                    referer = "contact_home_support.jsp?date_range=" + date_range;
                }
                try 
                {
                    //01/12/2019 12:00 AM - 01/12/2019 11:59 PM&group_id=all
                    //System.out.println("date_range=" + date_range);
                    String temp[] = date_range.split("-");
                    filter_start = filter_format.parse(temp[0]);
                    //filter_start = filter_format.parse("01/12/2019 12:00 AM");
                    //System.out.println("filter_start=" + temp[0] + " ====" + filter_start);

                    filter_end = filter_format.parse(temp[1]);
                    //System.out.println("filter_end=" + temp[1].trim() + " ====" + filter_end);

                    start = timestamp_format.format(filter_start);
                    end = timestamp_format.format(filter_end);

                    
                } catch (Exception e) {
                    //if not set then default to past 30 days
                    System.out.println("Exception on contact_sla_drilldown.jsp=" + e);
                }
                
//                int total_filtered_count = db.get_contacts.all_filtered_by_query_count(con,query);

                ArrayList<ArrayList<String[]>> all_contacts = support.sla_contact_calc.get_all_drilldown_data_for_sla_id(con, sla_id, filter_start, filter_end);
//                ArrayList <String[]> contacts = support.sla_contact_calc.get_noncompliant_contacts_for_sla_id(con, sla_id, filter_start, filter_end);
//                ArrayList <String[]> all_contacts = db.get_contacts.all_limited_filtered_by_query(con,starting_record,fetch_size, query, order_column_name, order_dir);

                String[] metrics = {"--","--","--","--"};
                try
                {
                    metrics = all_contacts.get(3).get(0);
                }
                catch(Exception e)
                {

                }

                String[] compliances = {"compliant", "non-compliant"};
                for (int i = 0; i < compliances.length; i++)
                {
                    JsonArrayBuilder json_array_builder = Json.createArrayBuilder();
                    ArrayList<String[]> contacts = all_contacts.get(i);
                    for(int a = 0; a < contacts.size();a++)
                    {
                        String contact_record[] = contacts.get(a);
                        String create_time = "";
                        try
                        {
                            //System.out.println("contact_record[2]=" + contact[2]);
                            java.util.Date temp1 = timestamp_format.parse(contact_record[2]);
                            //System.out.println("format=" + display_format.format(temp1));
                            create_time = filter_format.format(temp1);
                        }
                        catch (Exception e)
                        {
                            create_time = "";
                        }
                        String agent = "";
                        if(contact_record[10] == null)
                        {
                            agent = "--";
                        }
                        else
                        {
                            agent = contact_record[10];
                        }
                        String asa = "";
                        if(contact_record[4] == null)
                        {
                            asa = "--";
                        }
                        else
                        {
                            asa = contact_record[4];
                        }
                        String Abandoned = "";
                        if(contact_record[6] == null)
                        {
                            Abandoned = "--";
                        }
                        else
                        {
                            Abandoned = contact_record[6];
                        }
                        String aha = "";
                        if(contact_record[13] == null)
                        {
                            aha = "--";
                        }
                        else
                        {
                            aha = contact_record[13];
                        }
                        String Duration = "";
                        if(contact_record[7] == null)
                        {
                            Duration = "--";
                        }
                        else
                        {
                            Duration = contact_record[7];
                        }
                        String wut = "";
                        if(contact_record[11] == null)
                        {
                            wut = "--";
                        }
                        else
                        {
                            wut = contact_record[11];
                        }

                        json_array_builder
                            .add(Json.createObjectBuilder()
                                .add("channel", contact_record[8])
                                .add("time", create_time)
                                .add("agent", agent)
                                .add("asa", asa)
                                .add("abandoned", Abandoned)
                                .add("handle_time", aha)
                                .add("duration", Duration)
                                .add("wrap_up", wut)
                            );
                    }

                    json_builder.add(compliances[i] + "_count", contacts.size());
                    json_builder.add(compliances[i], json_array_builder);
                    
                }
                JsonArrayBuilder metrics_array_builder = Json.createArrayBuilder();
                for (int i = 0; i < metrics.length; i++)
                {
                    metrics_array_builder.add(metrics[i]);
                }
                json_builder.add("metrics", metrics_array_builder);
                json = json_builder.build();

                con.close();
                //send the JSON data
                out.print(json.toString());
            }
            catch(Exception e)
            {
                e.printStackTrace();
                logger.error("ERROR Exception in servlet ajax_lookup_contacts:=" + e);
                stringBuilder.setLength(0);
                stringBuilder.append("Error in retrieving contacts list");
                out.print(stringBuilder.toString());

            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

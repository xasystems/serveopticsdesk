/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author rcampbell
 */
public class admin_email_global_notifications extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            String notify_on[] = {"caller_incident_create","caller_incident_update","caller_incident_resolve","caller_incident_close","request_created_by_create","request_created_by_update","request_created_by_resolve","request_created_by_close","request_requester_create","request_requester_update","request_requester_resolve","request_requester_close"};
            String active[] = {"false","false","false","false","false","false","false","false","false","false","false","false"};
            String subject[] = {"","","","","","","","","","","",""};
            String body[] = {"","","","","","","","","","","",""};
            String delete_query = "DELETE FROM notifications_global";
            String query = "INSERT INTO notifications_global (notify_on,active,subject,body) VALUES (?,?,?,?)";
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement(delete_query);
                stmt.execute();
                stmt = con.prepareStatement(query);
                
                for(int a = 0; a < notify_on.length; a++)
                {
                    try
                    {
                        String notify_on_value = request.getParameter(notify_on[a]);
                        if(notify_on_value == null)
                        {
                            stmt.setString(1, notify_on[a]);
                            stmt.setString(2, "false");
                            stmt.setString(3, "");
                            stmt.setString(4, "");
                        }
                        else
                        {
                            stmt.setString(1, notify_on[a]);
                            stmt.setString(2, "true");
                            stmt.setString(3, "");
                            stmt.setString(4, "");
                        }
                        stmt.execute();
                    }
                    catch(Exception e)
                    {
                        System.out.println("Exception in admin_email_notifications=" + e);
                    }
                }
                stmt.close();
                con.close();
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_email_notifications:=" + e);
            }
            
        }
        response.sendRedirect("admin_email.jsp?success=true");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

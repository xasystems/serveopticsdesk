/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class ajax_lookup_projects extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
//        String user_id = request.getParameter("user_id");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

//        String user_id = request.getParameter("user_id");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        JsonObjectBuilder json_builder = Json.createObjectBuilder();
        JsonObject json;
        
        boolean first_record = true;
        boolean has_data = false;
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {

                if (session.getAttribute("authenticated") == null) 
                {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }

                Connection con = db.db_util.get_contract_connection(context_dir, session);
                int starting_record = 0;
                int previous_record = 0;
                int next_record = 0;
                int fetch_size = 50;
                
                try
                {
                    starting_record  = Integer.parseInt(request.getParameter("start"));
                    fetch_size  = Integer.parseInt(request.getParameter("length"));
                    next_record = starting_record + fetch_size;
                    previous_record = starting_record - fetch_size;
                    if(previous_record <= 0)
                    {
                        previous_record = 0;
                    }
                }
                catch(Exception e)
                {
                    starting_record = 0;
                    previous_record = 0;
                    next_record = starting_record + fetch_size;
                }

                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String add_query = "";
                Map<String, String> query_filters = new HashMap<String, String>();
                String order_column_num = StringUtils.defaultString(request.getParameter("order[0][column]"));
                String order_dir = StringUtils.defaultString(request.getParameter("order[0][dir]"));
                String order_column_name = "";                

                int project_id = NumberUtils.toInt(request.getParameter("project_id"), 0);

                ArrayList<String[]> projects = new ArrayList();

                if (project_id == 0)
                {

                    try
                    {
                        String filters = request.getParameter("filters"); //assigned_to_me,unassigned, created_today, closed_today, open, open_7_14, open_14_30, open_30_plus 
                        if(filters != null)
                        {
                            JsonReader reader = Json.createReader(new StringReader(filters));

                            JsonObject jsonObject = reader.readObject();
                            Iterator it = jsonObject.entrySet().iterator();

                            while (it.hasNext()) {
                                JsonObject.Entry mapEntry = (JsonObject.Entry)it.next();
                                query_filters.put(mapEntry.getKey().toString(), ((JsonString)mapEntry.getValue()).getString());
                            }
                        }   
                    }
                    catch(Exception e)
                    {
    //                    filter = "all";
                    }

                    String[] permitted_sort_fields = {"name", "priority", "scheduled_start_date", "scheduled_end_date"};
                    if (!order_column_num.equals(""))
                    {
                        order_column_name = StringUtils.defaultString(request.getParameter("columns[" + order_column_num + "][name]"));
                        if (!Arrays.stream(permitted_sort_fields).anyMatch(order_column_name::equals))
                        {
                            order_column_name = "";
                        } else {
                            switch (order_dir) {
                                case "asc":
                                    break;
                                case "desc":
                                    break;
                                default:
                                    order_dir = "";
                            }
                        }

                    }

                    String predefined = StringUtils.defaultString(request.getParameter("predefined"));
                    if(!predefined.equalsIgnoreCase(""))
                    {
                        if(predefined.equalsIgnoreCase("assigned_to_me"))
                        {
                            add_query = "projects.status NOT LIKE '%closed%' AND (projects.owner_id = " + user_id + " OR projects.manager_id = " + user_id + " OR projects.tech_lead_id = " + user_id + ")";
                        }
                        else if(predefined.equalsIgnoreCase("unassigned"))
                        {
                            add_query = "projects.status NOT LIKE '%closed%' AND (projects.owner_id = 0 AND projects.manager_id = 0 AND projects.tech_lead_id = 0)";
                        }
                        else if(predefined.equalsIgnoreCase("open"))
                        {
                            add_query = "projects.status NOT LIKE '%draft%' AND projects.status NOT LIKE '%closed%' AND projects.status NOT LIKE '%cancelled%'";
                        }
                        else if(predefined.equalsIgnoreCase("critical"))
                        {
                            add_query = "projects.status NOT LIKE '%closed%' AND projects.state <> 'Resolved' AND projects.priority = 'Critical'" ;
                        }
                        else if(predefined.equalsIgnoreCase("my_critical"))
                        {
                            add_query = "(projects.owner_id = " + user_id + " OR projects.manager_id = " + user_id + " OR projects.tech_lead_id = " + user_id + ") AND projects.status NOT LIKE '%draft%' AND projects.status NOT LIKE '%closed%' AND projects.status NOT LIKE '%cancelled%' AND projects.priority = 'Critical'" ;
                        }
                        else if(predefined.equalsIgnoreCase("my_high"))
                        {
                            add_query = "(projects.owner_id = " + user_id + " OR projects.manager_id = " + user_id + " OR projects.tech_lead_id = " + user_id + ") AND projects.status NOT LIKE '%draft%' AND projects.status NOT LIKE '%closed%' AND projects.status NOT LIKE '%cancelled%' AND projects.priority = 'High' and projects.assigned_to_id = " + user_id;
                        }
                        else if(predefined.equalsIgnoreCase("my_medium"))
                        {
                            add_query = "(projects.owner_id = " + user_id + " OR projects.manager_id = " + user_id + " OR projects.tech_lead_id = " + user_id + ") AND projects.status NOT LIKE '%draft%' AND projects.status NOT LIKE '%closed%' AND projects.status NOT LIKE '%cancelled%' AND projects.priority = 'Medium' and projects.assigned_to_id = " + user_id;
                        }
                        else if(predefined.equalsIgnoreCase("my_low"))
                        {
                            add_query = "(projects.owner_id = " + user_id + " OR projects.manager_id = " + user_id + " OR projects.tech_lead_id = " + user_id + ") AND projects.status NOT LIKE '%draft%' AND projects.status NOT LIKE '%closed%' AND projects.status NOT LIKE '%cancelled%' AND projects.priority = 'Low' and projects.assigned_to_id = " + user_id;
                        }
                        else if(predefined.equalsIgnoreCase("all_open_critical"))
                        {
                            add_query = "projects.status NOT LIKE '%draft%' AND projects.status NOT LIKE '%closed%' AND projects.status NOT LIKE '%cancelled%' AND projects.priority = 'Critical'" ;
                        }
                        else if(predefined.equalsIgnoreCase("all_open_high"))
                        {
                            add_query = "projects.status NOT LIKE '%draft%' AND projects.status NOT LIKE '%closed%' AND projects.status NOT LIKE '%cancelled%' AND reques.priority = 'High'";
                        }
                        else if(predefined.equalsIgnoreCase("all_open_medium"))
                        {
                            add_query = "projects.status NOT LIKE '%draft%' AND projects.status NOT LIKE '%closed%' AND projects.status NOT LIKE '%cancelled%' AND projects.priority = 'Medium'";
                        }
                        else if(predefined.equalsIgnoreCase("all_open_low"))
                        {
                            add_query = "projects.status NOT LIKE '%draft%' AND projects.status NOT LIKE '%closed%' AND projects.status NOT LIKE '%cancelled%' AND projects.priority = 'Low'";
                        }
                    }

                } else {
                    add_query = "projects.id = " + String.valueOf(project_id);
                }

                int total_filtered_count = db.get_projects.all_filtered_by_query_count(con, query_filters, add_query);
                projects = db.get_projects.all_limited_filtered_by_query(con,starting_record,fetch_size, query_filters, order_column_name, order_dir, add_query);
                final DateTimeFormatter formatter_timestamp = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
                LocalDateTime now = LocalDateTime.now();

                JsonArrayBuilder json_array_builder = Json.createArrayBuilder();
                for(int a = 0; a < projects.size();a++)
                {
                    String project_record[] = projects.get(a);

                    JsonArrayBuilder attachments_builder = Json.createArrayBuilder();                
                    try
                    { // processing attachments
                        ArrayList<String[]> attachments = new ArrayList();
                        attachments = db.get_projects.get_attachments_by_project_id(con, project_record[0]);
//                        logger.info("INFO ajax_lookup_projects attachments.size :=" + attachments.size());
                        if (attachments.size() > 0)
                        {   
                            for (int i = 0; i < attachments.size(); i++)
                            {
                                String[] attachment = attachments.get(i);
                                attachments_builder
                                    .add(Json.createObjectBuilder()
                                         .add("id", support.string_utils.check_for_null(attachment[0]))
                                         .add("project_id", support.string_utils.check_for_null(attachment[1]))
                                         .add("uuid", support.string_utils.check_for_null(attachment[2]))
                                         .add("name_on_disk", support.string_utils.check_for_null(attachment[3]))
                                         .add("file_name", support.string_utils.check_for_null(attachment[4]))
                                         .add("file_date", support.string_utils.check_for_null(attachment[5]))
                                         .add("file_size", support.string_utils.check_for_null(attachment[6]))
                                         .add("uploaded_by", support.string_utils.check_for_null(attachment[8]))
                                         .add("uploaded_by_name", support.string_utils.check_for_null(attachment[9]))
                                    );
                            }
                        }
                    } catch (Exception e)
                    {
                        logger.error("ERROR processing attachments Exception in servlet ajax_lookup_projects:=" + e);
                    }

                    String[] status_filters_completed = {"Closed", "Complete", "Cancelled"};
                    String timeline_overdue = "";
                    ArrayList<String[]> tasks = db.get_projects.project_tasks_for_project(con, project_record[0]);
                    int tasks_completed = 0;
                    int tasks_not_completed = 0;
                    int tasks_all = 0;

                    Boolean task_is_overdue = false;
                    for (int i = 0; i < tasks.size(); i++)
                    {
                        String[] task = tasks.get(i);
                        tasks_all++;
                        if (Arrays.stream(status_filters_completed).anyMatch(task[7]::equals)) { // tasks completed
                            if (task[11] != null && task[10] != null) {
                                try
                                {
                                    final LocalDateTime actual_end_date = LocalDateTime.parse(task[11], formatter_timestamp); // actual_end_date
                                    final LocalDateTime scheduled_end_date = LocalDateTime.parse(task[10], formatter_timestamp); // scheduled_end_date
                                    if (actual_end_date.isAfter(scheduled_end_date)) {
                                        task_is_overdue = true;
                                    }                                    
                                } catch (Exception e)
                                {
                                    // stub
                                }
                            }
                            tasks_completed++;
                        } else { // tasks not completed
                            if (task[10] != null) {
                                try
                                {
                                    final LocalDateTime scheduled_end_date = LocalDateTime.parse(task[10], formatter_timestamp); // scheduled_end_date
                                    if (scheduled_end_date.isBefore(now)) {
                                        task_is_overdue = true;
                                    }                                    
                                } catch (Exception e)
                                {
                                    // stub
                                }
                            }
                            tasks_not_completed++;
                        }
                    }

                    int timeline_percent = 0;
                    if (tasks_all > 0) {
                        timeline_percent = tasks_completed * 100 / tasks_all;
                    }

                    Boolean project_is_overdue = false;
                    if (!Arrays.stream(status_filters_completed).anyMatch(project_record[11]::equals)) { // project not completed
                        try
                        {
                            if (project_record[9] != null) {
                                final LocalDateTime scheduled_end_date = LocalDateTime.parse(project_record[9], formatter_timestamp); // scheduled_end_date
                                if (scheduled_end_date.isBefore(now) && tasks_not_completed > 0) {
                                    timeline_overdue = "Overdue Project";
                                }                                    

                            }
                        } catch (Exception e)
                        {
                            // stub
                        }
                        if (!timeline_overdue.equals("") && task_is_overdue) {
                            timeline_overdue = "Overdue Task";
                        }
                    }

                    logger.info("tasks_all :=" + tasks_all);
                    logger.info("tasks_completed :=" + tasks_completed);
                    logger.info("timeline_percent :=" + timeline_percent);

                    json_array_builder
                        .add(Json.createObjectBuilder()
                             .add("id", support.string_utils.check_for_null(project_record[0]))
                             .add("name", support.string_utils.check_for_null(project_record[1]))
                             .add("description", support.string_utils.check_for_null(project_record[2]))
                             .add("owner_group_id", support.string_utils.check_for_null(project_record[3]))
                             .add("owner_id", support.string_utils.check_for_null(project_record[4]))
                             .add("manager_id", support.string_utils.check_for_null(project_record[5]))
                             .add("tech_lead_id", support.string_utils.check_for_null(project_record[6]))
                             .add("scheduled_start_date", support.string_utils.check_for_null(project_record[7]))
                             .add("actual_start_date", support.string_utils.check_for_null(project_record[8]))
                             .add("scheduled_end_date", support.string_utils.check_for_null(project_record[9]))
                             .add("actual_end_date", support.string_utils.check_for_null(project_record[10]))

                             .add("status", support.string_utils.check_for_null(project_record[11]))
                             .add("priority", support.string_utils.check_for_null(project_record[12]))
                             .add("notes", support.string_utils.check_for_null(project_record[13]))
                             .add("owner_group_name", support.string_utils.check_for_null(project_record[14]))
                            //LOOKUP populate the owner info
                             .add("owner_username", support.string_utils.check_for_null(project_record[15]))
                             .add("owner_first", support.string_utils.check_for_null(project_record[16]))
                             .add("owner_mi", support.string_utils.check_for_null(project_record[17]))
                             .add("owner_last", support.string_utils.check_for_null(project_record[18]))
                             .add("owner_phone_mobile", support.string_utils.check_for_null(project_record[19]))
                            //LOOKUP populate the Manager info
                             .add("manager_username", support.string_utils.check_for_null(project_record[20]))
                             .add("manager_first", support.string_utils.check_for_null(project_record[21]))
                             .add("manager_mi", support.string_utils.check_for_null(project_record[22]))
                             .add("manager_last", support.string_utils.check_for_null(project_record[23]))
                             .add("manager_phone_mobile", support.string_utils.check_for_null(project_record[24]))
                            //LOOKUP populate the tech_lead info
                             .add("tech_lead_username", support.string_utils.check_for_null(project_record[25]))
                             .add("tech_lead_first", support.string_utils.check_for_null(project_record[26]))
                             .add("tech_lead_mi", support.string_utils.check_for_null(project_record[27]))
                             .add("tech_lead_last", support.string_utils.check_for_null(project_record[28]))
                             .add("tech_lead_phone_mobile", support.string_utils.check_for_null(project_record[29]))
//                             .add("timeline", support.string_utils.check_for_null(project_record[30]))
                             .add("timeline_overdue", timeline_overdue)
                             .add("timeline_percent", timeline_percent)
                             .add("attachments", attachments_builder)
                        );
                }
                json_builder.add("recordsTotal", total_filtered_count);
                json_builder.add("recordsFiltered", total_filtered_count);
                json_builder.add("draw", request.getParameter("draw"));
                json_builder.add("data", json_array_builder);



                json = json_builder.build();

                con.close();
                //send the JSON data
                out.print(json.toString());
            }
            catch(Exception e)
            {
                e.printStackTrace();
                logger.error("ERROR Exception in servlet ajax_lookup_projects:=" + e);
                stringBuilder.setLength(0);
                stringBuilder.append("Error in retrieving users list");
                out.print(stringBuilder.toString());

            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.codecs.Codec;
import org.owasp.esapi.codecs.OracleCodec;

@WebServlet("/ajax_lookup_problems")
/**
 *
 * @author ralph
 */
public class ajax_lookup_problems extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
//        String user_id = request.getParameter("user_id");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

//        String user_id = request.getParameter("user_id");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        JsonObjectBuilder json_builder = Json.createObjectBuilder();
        JsonObject json;
        
        boolean first_record = true;
        boolean has_data = false;
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {

                if (session.getAttribute("authenticated") == null) 
                {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }

                Connection con = db.db_util.get_contract_connection(context_dir, session);
                int starting_record = 0;
                int previous_record = 0;
                int next_record = 0;
                int fetch_size = 50;
                
                try
                {
                    starting_record  = Integer.parseInt(request.getParameter("start"));
                    fetch_size  = Integer.parseInt(request.getParameter("length"));
                    next_record = starting_record + fetch_size;
                    previous_record = starting_record - fetch_size;
                    if(previous_record <= 0)
                    {
                        previous_record = 0;
                    }
                }
                catch(Exception e)
                {
                    starting_record = 0;
                    previous_record = 0;
                    next_record = starting_record + fetch_size;
                }

                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
//                String all_users_info[][] = db.get_users.all_limited(con,starting_record,fetch_size);

                //get all the filter parameters
                String problem_time_start = null;
                String problem_time_stop = null;
                String create_date_start =  null;
                String create_date_stop =  null;
                String pending_date_start = null;
                String pending_date_stop = null;
                String state_date_start = null;
                String state_date_stop = null;
                String closed_date_start = null;
                String closed_date_stop = null;                
                String caller_id = "0";
                String caller_info[] = {"","","","","","","","","","","","","","","","","","","","","",""};
                String caller_name = "";
                String assigned_to_id = "0";
                String assigned_to_info[] = {"","","","","","","","","","","","","","","","","","","","","",""};
                String assigned_to_name = "";
                String create_by_id = "0";
                String create_by_info[] = {"","","","","","","","","","","","","","","","","","","","","",""};
                String create_by_name = "";
                String caller_group_id = null;
                String caller_group_info[] = {"","","","",""};
                String caller_group_name = "";
                String assigned_group_id = null;
                String assigned_group_info[] = {"","","","",""};
                String assigned_group_name = "";
                String priority = null;
                String urgency = null;
                String impact = null;
                String category = null;
                String subcategory = null;
                String status = null;
                String final_status = null;
                String number_incidents = null;
                String description = null;
                String location = null;
                String department = null;
                String site = null;
                String company = null;
                int id = NumberUtils.toInt(request.getParameter("id"), 0);
                String query = "";
                String search = StringUtils.defaultString(request.getParameter("search"));
                ESAPI.securityConfiguration().setResourceDirectory(getClass().getResource(".").getPath());
                Codec ORACLE_CODEC = new OracleCodec();

                if (id != 0)
                {
                    query = "problems.id = " + String.valueOf(id);
                } else if (!search.equals(""))
                {
                    String search_sanitized = ESAPI.encoder().encodeForSQL( ORACLE_CODEC, search);
                    query = "problems.id LIKE '%" + search_sanitized + "%' OR problems.description LIKE '%" + search_sanitized + "%'";
                } else {
                    boolean use_problem_time = false;
                    boolean use_create_date =  false;
                    boolean use_pending_date = false;
                    boolean use_state_date = false;
                    boolean use_closed_date = false;               
                    boolean use_caller_id = false;
                    boolean use_assigned_to_id = false;
                    boolean use_create_by_id = false;
                    boolean use_caller_group_id = false;
                    boolean use_assigned_group_id = false;
                    boolean use_priority = false;
                    boolean use_urgency = false;
                    boolean use_impact = false;
                    boolean use_category = false;
                    boolean use_subcategory = false;
                    boolean use_status = false;
                    boolean use_final_status = false;
                    boolean use_number_incidents = false;
                    boolean use_description = false;
                    boolean use_location = false;
                    boolean use_department = false;
                    boolean use_site = false;
                    boolean use_company = false;
                    boolean a_parameter_was_send = false;            

                    String predefined = "all";
                    String start = "";
                    String end = "";
                    String date_range = "";

                    SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
                    SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
                    SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020            
                    SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19

                    String user_tz_name = "UTC";
                    String user_tz_time = "+00:00";
                    try
                    {
                        user_tz_name = session.getAttribute("tz_name").toString();
                        user_tz_time = session.getAttribute("tz_time").toString();
                        if(user_tz_name == null || user_tz_time == null )
                        {
                            user_tz_name = "UTC";
                            user_tz_time = "+00:00";
                        }

                    }
                    catch(Exception e)
                    {
                        user_tz_name = "UTC";
                        user_tz_time = "+00:00";
                    }
                    date_time_picker_format.setTimeZone(TimeZone.getTimeZone(user_tz_name));
                    java.util.Date now = new java.util.Date();
                    String now_picker_time = date_time_picker_format.format(now);

                    GregorianCalendar cal = new GregorianCalendar();
                    cal.set(cal.HOUR_OF_DAY,00);
                    cal.set(cal.MINUTE,00);
                    cal.set(cal.SECOND,00);
                    String today_start_date = timestamp_format.format(cal.getTime());

                    cal.set(cal.HOUR_OF_DAY,23);
                    cal.set(cal.MINUTE,59);
                    cal.set(cal.SECOND,59);
                    String today_end_date = timestamp_format.format(cal.getTime());

                    cal = new GregorianCalendar();
                    cal.add(cal.DATE, -30);
                    cal.set(cal.HOUR_OF_DAY,00);
                    cal.set(cal.MINUTE,00);
                    cal.set(cal.SECOND,00);
                    String today_minus_30 = timestamp_format.format(cal.getTime());

                    cal = new GregorianCalendar();
                    cal.add(cal.DATE, -14);
                    cal.set(cal.HOUR_OF_DAY,00);
                    cal.set(cal.MINUTE,00);
                    cal.set(cal.SECOND,00);
                    String today_minus_14 = timestamp_format.format(cal.getTime());

                    cal = new GregorianCalendar();
                    cal.add(cal.DATE, -7);
                    cal.set(cal.HOUR_OF_DAY,00);
                    cal.set(cal.MINUTE,00);
                    cal.set(cal.SECOND,00);
                    String today_minus_7 = timestamp_format.format(cal.getTime());

                    try
                    {

                        //SimpleDateFormat filter_format2 = new SimpleDateFormat("yyyy"); //01/12/2019 12:00 AM
                        java.util.Date filter_start = new java.util.Date();
                        java.util.Date filter_end = new java.util.Date();

                        date_range = support.filter_dates.past_30_days();

                        try 
                        {
                            //01/12/2019 12:00 AM - 01/12/2019 11:59 PM&group_id=all
                            //System.out.println("date_range=" + date_range);
                            String temp[] = date_range.split("-");
                            filter_start = filter_format.parse(temp[0]);
                            //filter_start = filter_format.parse("01/12/2019 12:00 AM");
                            //System.out.println("filter_start=" + temp[0] + " ====" + filter_start);

                            filter_end = filter_format.parse(temp[1]);
                            //System.out.println("filter_end=" + temp[1].trim() + " ====" + filter_end);

                            start = timestamp_format.format(filter_start);
                            end = timestamp_format.format(filter_end);

                            //get the problems
                            //problem_info = db.get_problems.problems_by_date_range(con, filter_start, filter_end);

                        } 
                        catch (Exception e) 
                        {
                            //if not set then default to past 30 days
                            System.out.println("Exception on ajax_lookup_problems=" + e);
                        }

                        //get filter type

                        try
                        {
                            predefined = request.getParameter("predefined"); //assigned_to_me,unassigned, created_today, closed_today, open, open_7_14, open_14_30, open_30_plus 
                            //System.out.println("filter=" + filter);
                            if(predefined == null || predefined.equalsIgnoreCase("null"))
                            {
                                predefined = "all";
                            }
                        }
                        catch(Exception e)
                        {
                            predefined = "all";
                        }

                        //get parameters
                        try
                        {
                            problem_time_start = request.getParameter("problem_time_start");
                            problem_time_stop = request.getParameter("problem_time_stop");
                            //System.out.println("187 problem_time_stop=" + problem_time_stop);
                            if(problem_time_start != null && !problem_time_start.equalsIgnoreCase("") && problem_time_stop != null && !problem_time_stop.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_problem_time = true;
                            }
                            else
                            {
                                problem_time_start = "";
                                problem_time_stop = "";
                            }
                        }
                        catch(Exception e)
                        {
                            problem_time_start = "";
                            problem_time_stop = "";
                        }   
                        try
                        {
                            create_date_start = request.getParameter("create_date_start");
                            create_date_stop = request.getParameter("create_date_stop");
                            if(create_date_start != null && !create_date_start.equalsIgnoreCase("") && create_date_stop != null && !create_date_stop.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_create_date = true;
                            }
                            else
                            {
                                create_date_start = "";
                                create_date_stop = "";
                            }
                        }
                        catch(Exception e)
                        {
                            create_date_start = "";
                            create_date_stop = "";
                        }     

                        try
                        {
                            pending_date_start = request.getParameter("pending_date_start");
                            pending_date_stop = request.getParameter("pending_date_stop");
                            if(pending_date_start != null && !pending_date_start.equalsIgnoreCase("") && pending_date_stop != null && !pending_date_stop.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_pending_date = true;
                            }
                            else
                            {
                                pending_date_start = "";
                                pending_date_stop = "";
                            }
                        }
                        catch(Exception e)
                        {
                            pending_date_start = "";
                            pending_date_stop = "";
                        }     

                        try
                        {
                            state_date_start = request.getParameter("state_date_start");
                            state_date_stop = request.getParameter("state_date_stop");
                            if(state_date_start != null && !state_date_start.equalsIgnoreCase("") && state_date_stop != null && !state_date_stop.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_state_date = true;
                            }
                            else
                            {
                                state_date_start = "";
                                state_date_stop = "";
                            }
                        }
                        catch(Exception e)
                        {
                            state_date_start = "";
                            state_date_stop = "";
                        } 

                        try
                        {
                            closed_date_start = request.getParameter("closed_date_start");
                            closed_date_stop = request.getParameter("closed_date_stop");
                            if(closed_date_start != null && !closed_date_start.equalsIgnoreCase("") && closed_date_stop != null && !closed_date_stop.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_closed_date = true;
                            }
                            else
                            {
                                closed_date_start = "";
                                closed_date_stop = "";
                            }
                        }
                        catch(Exception e)
                        {
                            closed_date_start = "";
                            closed_date_stop = "";
                        }  
                        //use_caller_id
                        try
                        {
                            caller_id = request.getParameter("caller_id");
                            if(caller_id != null && !caller_id.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_caller_id = true;
                                caller_info = db.get_users.by_id(con, caller_id);
                                caller_name = caller_info[5] + " " + caller_info[3];
                            }
                            else
                            {
                                caller_id = "";
                            }
                        }
                        catch(Exception e)
                        {
                            caller_id = "";
                        }  
                        //use_assigned_to_id
                        try
                        {
                            assigned_to_id = request.getParameter("assigned_to_id");
                            if(assigned_to_id != null && !assigned_to_id.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_assigned_to_id = true;
                                assigned_to_info = db.get_users.by_id(con, assigned_to_id);
                                assigned_to_name = assigned_to_info[5] + " " + assigned_to_info[3];
                            }
                            else
                            {
                                assigned_to_id = "";
                            }
                        }
                        catch(Exception e)
                        {
                            assigned_to_id = "";
                        }  
                        //use_create_by_id
                        try
                        {
                            create_by_id = request.getParameter("create_by_id");
                            if(create_by_id != null && !create_by_id.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_create_by_id = true;
                                create_by_info = db.get_users.by_id(con, create_by_id);
                                create_by_name = create_by_info[5] + " " + create_by_info[3];
                            }
                            else
                            {
                                create_by_id = "";
                            }
                        }
                        catch(Exception e)
                        {
                            create_by_id = "";
                        }  
                        //use_caller_group_id
                        try
                        {
                            caller_group_id = request.getParameter("caller_group_id");
                            if(caller_group_id != null && !caller_group_id.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_caller_group_id = true;
                                caller_group_info = db.get_groups.by_id(con,caller_group_id);
                                caller_group_name = caller_group_info[1];
                            }
                            else
                            {
                                caller_group_id = "";
                            }
                        }
                        catch(Exception e)
                        {
                            caller_group_id = "";
                        }  
                        //use_assigned_group_id
                        try
                        {
                            assigned_group_id = request.getParameter("assigned_group_id");
                            if(assigned_group_id != null && !assigned_group_id.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_assigned_group_id = true;
                                assigned_group_info = db.get_groups.by_id(con,assigned_group_id);
                                assigned_group_name = assigned_group_info[1];
                            }
                            else
                            {
                                assigned_group_id = "";
                            }
                        }
                        catch(Exception e)
                        {
                            assigned_group_id = "";
                        } 
                        //use_priority
                        try
                        {
                            priority = request.getParameter("priority");
                            if(priority != null && !priority.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_priority = true;
                            }
                            else
                            {
                                priority = "";
                            }
                        }
                        catch(Exception e)
                        {
                            priority = "";
                        } 
                        //use_urgency
                        try
                        {
                            urgency = request.getParameter("urgency");
                            if(urgency != null && !urgency.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_urgency = true;
                            }
                            else
                            {
                                urgency = "";
                            }
                        }
                        catch(Exception e)
                        {
                            urgency = "";
                        } 
                        //use_impact
                        try
                        {
                            impact = request.getParameter("impact");
                            if(impact != null && !impact.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_impact = true;
                            }
                            else
                            {
                                impact = "";
                            }
                        }
                        catch(Exception e)
                        {
                            impact = "";
                        } 
                        //use_category
                        try
                        {
                            category = request.getParameter("category");
                            if(category != null && !category.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_category = true;
                            }
                            else
                            {
                                category = "";
                            }
                        }
                        catch(Exception e)
                        {
                            category = "";
                        } 
                        //use_subcategory
                        try
                        {
                            subcategory = request.getParameter("subcategory");
                            if(subcategory != null && !subcategory.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_subcategory = true;
                            }
                            else
                            {
                                subcategory = "";
                            }
                        }
                        catch(Exception e)
                        {
                            subcategory = "";
                        } 
                        //use_status
                        try
                        {
                            status = request.getParameter("status");
                            if(status != null && !status.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_status = true;
                            }
                            else
                            {
                                status = "";
                            }
                        }
                        catch(Exception e)
                        {
                            status = "";
                        } 
                        //use_final_status
                        try
                        {
                            final_status = request.getParameter("final_status");
                            if(final_status != null && !final_status.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_final_status = true;
                            }
                            else
                            {
                                final_status = "";
                            }
                        }
                        catch(Exception e)
                        {
                            final_status = "";
                        } 
                        //use_number_incidents
                        try
                        {
                            number_incidents = request.getParameter("number_incidents");
                            if(number_incidents != null && !number_incidents.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_number_incidents = true;
                            }
                            else
                            {
                                number_incidents = "";
                            }
                        }
                        catch(Exception e)
                        {
                            number_incidents = "";
                        } 
                        //use_description
                        try
                        {
                            description = request.getParameter("description");
                            if(description != null && !description.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_description = true;
                            }
                            else
                            {
                                description = "";
                            }
                        }
                        catch(Exception e)
                        {
                            description = "";
                        } 
                        //use_location
                        try
                        {
                            location = request.getParameter("location");
                            if(location != null && !location.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_location = true;
                            }
                            else
                            {
                                location = "";
                            }
                        }
                        catch(Exception e)
                        {
                            location = "";
                        } 
                        //use_department
                        try
                        {
                            department = request.getParameter("department");
                            if(department != null && !department.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_department = true;
                            }
                            else
                            {
                                department = "";
                            }
                        }
                        catch(Exception e)
                        {
                            department = "";
                        } 
                        //use_site
                        try
                        {
                            site = request.getParameter("site");
                            if(site != null && !site.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_site = true;
                            }
                            else
                            {
                                site = "";
                            }
                        }
                        catch(Exception e)
                        {
                            site = "";
                        } 
                        //use_company
                        try
                        {
                            company = request.getParameter("company");
                            if(company != null && !company.equalsIgnoreCase(""))
                            {
                                a_parameter_was_send = true;
                                use_company = true;
                            }
                            else
                            {
                                company = "";
                            }
                        }
                        catch(Exception e)
                        {
                            company = "";
                        } 
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////                
                        //if any filter parameter is set override the filter parameter
                        if(a_parameter_was_send)
                        {
                            if(use_problem_time)
                            {
                                try
                                {
                                    String Qstart = timestamp_format.format(date_time_picker_format.parse(problem_time_start.trim()));
                                    //convert to UTC for DB query
                                    //System.out.println("Qstart before=" + Qstart);
                                    ZonedDateTime Qstart_utc = support.date_utils.user_tz_to_utc(user_tz_name,Qstart);
                                    Qstart = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(Qstart_utc);
                                    //System.out.println("Qstart  after=" + Qstart);
                                    //System.out.println("599 problem_time_stop=" + problem_time_stop);
                                    String Qend = timestamp_format.format(date_time_picker_format.parse(problem_time_stop.trim()));
                                    //convert to UTC for DB query
                                    //System.out.println("Qend  before=" + Qend);
                                    ZonedDateTime Qend_utc = support.date_utils.user_tz_to_utc(user_tz_name,Qend);
                                    Qend = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(Qend_utc);
                                    //System.out.println("Qend   after=" + Qend);

                                    if(query.equalsIgnoreCase(""))
                                    {
                                        query = "(problem_time >= '" + Qstart + "' AND problem_time <= '" + Qend + "')";
                                    }
                                    else
                                    {
                                        query = query + " AND (problem_time >= '" + Qstart + "' AND problem_time <= '" + Qend + "')";
                                    }
                                }
                                catch(Exception e)
                                {
                                    System.out.println("Exception on ajax_lookup_problems parse problem_time=" + e);
                                }
                            }
                            if(use_create_date)
                            {
                                try
                                {
                                    java.util.Date Qstart = date_time_picker_format.parse(create_date_start.trim());
                                    java.util.Date Qend = date_time_picker_format.parse(create_date_stop.trim());
                                    String start_string = timestamp_format.format(Qstart);
                                    String end_string = timestamp_format.format(Qend);
                                    if(query.equalsIgnoreCase(""))
                                    {
                                        query = "(problems.create_date >= '" + start_string + "' AND problems.create_date <= '" + end_string + "')";
                                    }
                                    else
                                    {
                                        query = query + " AND (problems.create_date >= '" + start_string + "' AND problems.create_date <= '" + end_string + "')";
                                    }
                                }
                                catch(Exception e)
                                {
                                    System.out.println("Exception on ajax_lookup_problems parse create_date=" + e);
                                }
                            }
                            if(use_pending_date)
                            {
                                try
                                {
                                    java.util.Date Qstart = date_time_picker_format.parse(pending_date_start.trim());
                                    java.util.Date Qend = date_time_picker_format.parse(pending_date_stop.trim());
                                    String start_string = timestamp_format.format(Qstart);
                                    String end_string = timestamp_format.format(Qend);
                                    if(query.equalsIgnoreCase(""))
                                    {
                                        query = "(pending_date >= '" + start_string + "' AND pending_date <= '" + end_string + "')";
                                    }
                                    else
                                    {
                                        query = query + " AND (pending_date >= '" + start_string + "' AND pending_date <= '" + end_string + "')";
                                    }
                                }
                                catch(Exception e)
                                {
                                    System.out.println("Exception on ajax_lookup_problems parse pending_date=" + e);
                                }
                            }
                            if(use_state_date)
                            {
                                try
                                {
                                    java.util.Date Qstart = date_time_picker_format.parse(state_date_start.trim());
                                    java.util.Date Qend = date_time_picker_format.parse(state_date_stop.trim());
                                    String start_string = timestamp_format.format(Qstart);
                                    String end_string = timestamp_format.format(Qend);
                                    if(query.equalsIgnoreCase(""))
                                    {
                                        query = "(problems.state_date >= '" + start_string + "' AND problems.state_date <= '" + end_string + "')";
                                    }
                                    else
                                    {
                                        query = query + " AND (problems.state_date >= '" + start_string + "' AND problems.state_date <= '" + end_string + "')";
                                    }
                                }
                                catch(Exception e)
                                {
                                    System.out.println("Exception on ajax_lookup_problems parse state_date=" + e);
                                }
                            }
                            if(use_closed_date)
                            {
                                try
                                {
                                    java.util.Date Qstart = date_time_picker_format.parse(closed_date_start.trim());
                                    java.util.Date Qend = date_time_picker_format.parse(closed_date_stop.trim());
                                    String start_string = timestamp_format.format(Qstart);
                                    String end_string = timestamp_format.format(Qend);
                                    if(query.equalsIgnoreCase(""))
                                    {
                                        query = "(closed_date >= '" + start_string + "' AND closed_date <= '" + end_string + "')";
                                    }
                                    else
                                    {
                                        query = query + " AND (closed_date >= '" + start_string + "' AND closed_date <= '" + end_string + "')";
                                    }
                                }
                                catch(Exception e)
                                {
                                    System.out.println("Exception on ajax_lookup_problems parse closed_date=" + e);
                                }
                            }

                            if(use_caller_id)
                            {
                                try
                                {
                                    if(query.equalsIgnoreCase(""))
                                    {
                                        query = "caller_id = '" + caller_id + "'";
                                    }
                                    else
                                    {
                                        query = query + " AND caller_id = '" + caller_id + "'";
                                    }
                                }
                                catch(Exception e)
                                {
                                    System.out.println("Exception on ajax_lookup_problems parse use_caller_id=" + e);
                                }
                            }
                            if(use_assigned_to_id)
                            {
                                try
                                {
                                    String value_sanitized = ESAPI.encoder().encodeForSQL( ORACLE_CODEC, assigned_to_id);
                                    if(query.equalsIgnoreCase(""))
                                    {
                                        query = "assigned_to_id = '" + value_sanitized + "'";
                                    }
                                    else
                                    {
                                        query = query + " AND assigned_to_id = '" + value_sanitized + "'";
                                    }
                                }
                                catch(Exception e)
                                {
                                    System.out.println("Exception on ajax_lookup_problems parse assigned_to_id=" + e);
                                }
                            }
                            if(use_create_by_id)
                            {
                                try
                                {
                                    if(query.equalsIgnoreCase(""))
                                    {
                                        query = "create_by_id = '" + create_by_id + "'";
                                    }
                                    else
                                    {
                                        query = query + " AND create_by_id = '" + create_by_id + "'";
                                    }
                                }
                                catch(Exception e)
                                {
                                    System.out.println("Exception on ajax_lookup_problems parse use_create_by_id=" + e);
                                }
                            }
                            if(use_caller_group_id)
                            {
                                try
                                {
                                    if(query.equalsIgnoreCase(""))
                                    {
                                        query = "caller_group_id = '" + caller_group_id + "'";
                                    }
                                    else
                                    {
                                        query = query + " AND caller_group_id = '" + caller_group_id + "'";
                                    }
                                }
                                catch(Exception e)
                                {
                                    System.out.println("Exception on ajax_lookup_problems parse caller_group_id=" + e);
                                }
                            }
                            if(use_assigned_group_id)
                            {
                                try
                                {
                                    if(query.equalsIgnoreCase(""))
                                    {
                                        query = "assigned_group_id = '" + assigned_group_id + "'";
                                    }
                                    else
                                    {
                                        query = query + " AND assigned_group_id = '" + assigned_group_id + "'";
                                    }
                                }
                                catch(Exception e)
                                {
                                    System.out.println("Exception on ajax_lookup_problems parse assigned_group_id=" + e);
                                }
                            }
                            if(use_priority)
                            {
                                try
                                {
                                    String value_sanitized = ESAPI.encoder().encodeForSQL( ORACLE_CODEC, priority);
                                    if(query.equalsIgnoreCase(""))
                                    {
                                        query = "priority = '" + value_sanitized + "'";
                                    }
                                    else
                                    {
                                        query = query + " AND priority = '" + value_sanitized + "'";
                                    }
                                }
                                catch(Exception e)
                                {
                                    System.out.println("Exception on ajax_lookup_problems parse priority=" + e);
                                }
                            }
                            if(use_status)
                            {
                                try
                                {
                                    String value_sanitized = ESAPI.encoder().encodeForSQL( ORACLE_CODEC, status);
                                    if(query.equalsIgnoreCase(""))
                                    {
                                        query = "problems.status = '" + value_sanitized + "'";
                                    }
                                    else
                                    {
                                        query = query + " AND problems.status = '" + value_sanitized + "'";
                                    }
                                }
                                catch(Exception e)
                                {
                                    System.out.println("Exception on ajax_lookup_problems parse status=" + e);
                                }
                            }
                            if(use_final_status)
                            {
                                try
                                {
                                    String value_sanitized = ESAPI.encoder().encodeForSQL( ORACLE_CODEC, final_status);
                                    if(query.equalsIgnoreCase(""))
                                    {
                                        query = "problems.final_status = '" + value_sanitized + "'";
                                    }
                                    else
                                    {
                                        query = query + " AND problems.final_status = '" + value_sanitized + "'";
                                    }
                                }
                                catch(Exception e)
                                {
                                    System.out.println("Exception on ajax_lookup_problems parse final_status=" + e);
                                }
                            }
                            if(use_number_incidents)
                            {
                                try
                                {
                                    String value_sanitized = ESAPI.encoder().encodeForSQL( ORACLE_CODEC, number_incidents);
                                    if(query.equalsIgnoreCase(""))
                                    {
                                        query = "incidents_count >= " + value_sanitized + "";
                                    }
                                    else
                                    {
                                        query = query + " AND incidents_count >= " + value_sanitized + "";
                                    }
                                }
                                catch(Exception e)
                                {
                                    System.out.println("Exception on ajax_lookup_problems parse number_incidents=" + e);
                                }
                            }
                            if(use_description)
                            {
                                try
                                {
                                    String sub_query = "";
                                    String temp[] = description.split(" ");
                                    //System.out.println("temp length=" + temp.length);
                                    if(temp.length == 1)
                                    {
                                        sub_query = "description LIKE '%" + description + "%'";
                                    }
                                    else
                                    {
                                        for(int a = 0; a < temp.length; a++)
                                        {
                                            if(a == 0)
                                            {
                                                sub_query = "description LIKE '%" + temp[a] + "%'";
                                            }
                                            else
                                            {
                                                sub_query = sub_query + " AND description LIKE '%" + temp[a] + "%'";
                                            }
                                        }
                                    }

                                    if(query.equalsIgnoreCase(""))
                                    {
                                        query = sub_query;
                                    }
                                    else
                                    {
                                        query = query + " AND " + sub_query;
                                    }
                                }
                                catch(Exception e)
                                {
                                    System.out.println("Exception on ajax_lookup_problems parse description=" + e);
                                }
                            }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                   
                        }    
                        else
                        {

                        }

                        //get the problems
                        if(predefined.equalsIgnoreCase("all"))
                        {
    //                            problem_info = db.get_problems.problems_by_date_range(con, user_tz_name, filter_start, filter_end);
                        }
                        else if(predefined.equalsIgnoreCase("assigned_to_me"))
                        {
                            query = (query.equalsIgnoreCase("") ? "" : query + " AND ") + "problems.state <> 'Closed' AND assigned_to_id = " + user_id;
    //                            problem_info = db.get_problems.problems_by_date_range_and_assigned_to_id(con,user_id);
                        }
                        else if(predefined.equalsIgnoreCase("unassigned"))
                        {
    //                            problem_info = db.get_problems.problems_unassigned(con);
                            query = (query.equalsIgnoreCase("") ? "" : query + " AND ") + "problems.state <> 'Closed' AND assigned_to_id = 0";
                        }
                        else if(predefined.equalsIgnoreCase("created_today"))
                        {
                            query = (query.equalsIgnoreCase("") ? "" : query + " AND ") + "problems.create_date >= " + today_start_date + " AND problems.create_date <= " + today_end_date;
    //                            problem_info = db.get_problems.problems_created_today(con);
                        }
                        else if(predefined.equalsIgnoreCase("closed_today"))
                        {
                            query = (query.equalsIgnoreCase("") ? "" : query + " AND ") + "problems.closed_date >= " + today_start_date + " AND problems.closed_date <= " + today_end_date;
    //                            problem_info = db.get_problems.problems_closed_today(con);
                        }
                        else if(predefined.equalsIgnoreCase("open"))
                        {
                            query = (query.equalsIgnoreCase("") ? "" : query + " AND ") + "problems.state <> 'Closed'";
    //                            problem_info = db.get_problems.problems_open(con);
                        }
                        else if(predefined.equalsIgnoreCase("open_7_14"))
                        {
                            query = (query.equalsIgnoreCase("") ? "" : query + " AND ") + "problems.state <> 'Closed' AND problems.create_date >= " + today_minus_14 + " AND problems.create_date <= " + today_minus_7;
    //                            problem_info = db.get_problems.problems_open_7_14_days(con);
                        }
                        else if(predefined.equalsIgnoreCase("open_14_30"))
                        {
                            query = (query.equalsIgnoreCase("") ? "" : query + " AND ") + "problems.state <> 'Closed' AND problems.create_date >= " + today_minus_30 + " AND problems.create_date <= " + today_minus_14;
    //                            problem_info = db.get_problems.problems_open_14_30_days(con);
                        }
                        else if(predefined.equalsIgnoreCase("open_30_plus"))
                        {
                            query = (query.equalsIgnoreCase("") ? "" : query + " AND ") + "problems.state <> 'Closed' AND problems.create_date <= " + today_minus_30;
    //                            problem_info = db.get_problems.problems_open_30_plus_days(con);
                        }
                        else if(predefined.equalsIgnoreCase("critical"))
                        {
                            query = (query.equalsIgnoreCase("") ? "" : query + " AND ") + "problems.state <> 'Closed' AND problems.state <> 'Resolved' AND problems.priority = 'Critical'" ;
    //                            problem_info = db.get_problems.problems_by_priority(con,"critical");
                        }
                        else if(predefined.equalsIgnoreCase("my_critical"))
                        {
                            query = (query.equalsIgnoreCase("") ? "" : query + " AND ") + "problems.state <> 'Closed' AND problems.state <> 'Resolved' AND problems.priority = 'Critical' and problems.assigned_to_id = " + user_id;
    //                            problem_info = db.get_problems.my_problems_by_priority(con,"Critical",user_id);
                        }
                        else if(predefined.equalsIgnoreCase("my_high"))
                        {
                            query = (query.equalsIgnoreCase("") ? "" : query + " AND ") + "problems.state <> 'Closed' AND problems.state <> 'Resolved' AND problems.priority = 'High' and problems.assigned_to_id = " + user_id;
    //                            problem_info = db.get_problems.my_problems_by_priority(con,"High",user_id);
                        }
                        else if(predefined.equalsIgnoreCase("my_medium"))
                        {
                            query = (query.equalsIgnoreCase("") ? "" : query + " AND ") + "problems.state <> 'Closed' AND problems.state <> 'Resolved' AND problems.priority = 'Medium' and problems.assigned_to_id = " + user_id;
    //                            problem_info = db.get_problems.my_problems_by_priority(con,"Medium",user_id);
                        }
                        else if(predefined.equalsIgnoreCase("my_low"))
                        {
                            query = (query.equalsIgnoreCase("") ? "" : query + " AND ") + "problems.state <> 'Closed' AND problems.state <> 'Resolved' AND problems.priority = 'Low' and problems.assigned_to_id = " + user_id;
    //                            problem_info = db.get_problems.my_problems_by_priority(con,"Low",user_id);
                        }
                        else if(predefined.equalsIgnoreCase("all_open_critical"))
                        {
                            query = (query.equalsIgnoreCase("") ? "" : query + " AND ") + "problems.state <> 'Closed' AND problems.state <> 'Resolved' AND problems.priority = 'Critical'" ;
    //                            problem_info = db.get_problems.problems_by_priority(con,"Critical");
                        }
                        else if(predefined.equalsIgnoreCase("all_open_high"))
                        {
                            query = (query.equalsIgnoreCase("") ? "" : query + " AND ") + "problems.state <> 'Closed' AND problems.state <> 'Resolved' AND problems.priority = 'High'";
    //                            problem_info = db.get_problems.problems_by_priority(con,"High");
                        }
                        else if(predefined.equalsIgnoreCase("all_open_medium"))
                        {
                            query = (query.equalsIgnoreCase("") ? "" : query + " AND ") + "problems.state <> 'Closed' AND problems.state <> 'Resolved' AND problems.priority = 'Medium'";
    //                            problem_info = db.get_problems.problems_by_priority(con,"Medium");
                        }
                        else if(predefined.equalsIgnoreCase("all_open_low"))
                        {
                            query = (query.equalsIgnoreCase("") ? "" : query + " AND ") + "problems.state <> 'Closed' AND problems.state <> 'Resolved' AND problems.priority = 'Low'";
    //                            problem_info = db.get_problems.problems_by_priority(con,"Low");
                        }

    //                    con.close();
                    }
                    catch(Exception e)
                    {
                        System.out.println("Exception on ajax_lookup_problems=" + e);
                    }
                }


//                String[] filter_fields = {
////                        "filter",
//
//                };
//                List<Map<String, String>> filters = new ArrayList<>();
//                for (int i = 0; i < filter_fields.length; i++)
//                {
//                    String filter_field = filter_fields[i];
//                    String filter_value = request.getParameter(filter_field);
//                    if (filter_value != null && !filter_value.isEmpty()) 
//                    {
//                        Map<String, String> map = new HashMap<String, String>();
//                        map.put("key", filter_field);
//                        map.put("value", filter_value);
//                        filters.add(map);
//                    }
//                }
                
//                int total_filtered_count = db.get_problems.all_filtered_count(con,filters);
                int total_filtered_count = db.get_problems.all_filtered_by_query_count(con,query);
//                ArrayList <String[]> all_problems = db.get_problems.all_limited_filtered(con,starting_record,fetch_size,filters);
                String order_column_num = StringUtils.defaultString(request.getParameter("order[0][column]"));
                String order_dir = StringUtils.defaultString(request.getParameter("order[0][dir]"));
                String order_column_name = "";                
                String[] permitted_sort_fields = {"id", "priority", "state", "problem_time", "caller_id", "assigned_to_id"};
                if (!order_column_num.equals(""))
                {
                    order_column_name = StringUtils.defaultString(request.getParameter("columns[" + order_column_num + "][name]"));
                    if (!Arrays.stream(permitted_sort_fields).anyMatch(order_column_name::equals))
                    {
                        order_column_name = "";
                    } else {
                        switch (order_dir) {
                            case "asc":
                                break;
                            case "desc":
                                break;
                            default:
                                order_dir = "";
                        }
                    }
                    
                }
                

                ArrayList <String[]> all_problems = db.get_problems.all_limited_filtered_by_query(con,starting_record,fetch_size, query, order_column_name, order_dir);

                String roles = "";
                JsonArrayBuilder json_array_builder = Json.createArrayBuilder();
                for(int a = 0; a < all_problems.size();a++)
                {
                    String problem_record[] = all_problems.get(a);
//                    logger.info(Arrays.toString(problem_record));
                    json_array_builder
                        .add(Json.createObjectBuilder()
                            .add("id", StringUtils.defaultString(problem_record[0]))
                            .add("priority", StringUtils.defaultString(problem_record[1]))
                            .add("name", StringUtils.defaultString(problem_record[2]))
                            .add("description", StringUtils.defaultString(problem_record[3]))
                            .add("create_date", StringUtils.defaultString(problem_record[4]))
                            .add("create_by_id", StringUtils.defaultString(problem_record[5]))
                            .add("final_status", StringUtils.defaultString(problem_record[22]))
                            .add("status", StringUtils.defaultString(problem_record[6]))
                            .add("status_date", StringUtils.defaultString(problem_record[7]))
                            .add("assigned_group_id", StringUtils.defaultString(problem_record[8]))
                            .add("assigned_to_id", StringUtils.defaultString(problem_record[9]))
                            .add("notes", StringUtils.defaultString(problem_record[10]))
                            .add("assigned_group_name", StringUtils.defaultString(problem_record[11]))
                            .add("assigned_to_username", StringUtils.defaultString(problem_record[12]))
                            .add("assigned_to_first", StringUtils.defaultString(problem_record[13]))
                            .add("assigned_to_mi", StringUtils.defaultString(problem_record[14]))
                            .add("assigned_to_last", StringUtils.defaultString(problem_record[15]))
                            .add("assigned_to_phone_mobile", StringUtils.defaultString(problem_record[16]))
                            .add("created_by_username", StringUtils.defaultString(problem_record[17]))
                            .add("created_by_first", StringUtils.defaultString(problem_record[18]))
                            .add("created_by_mi", StringUtils.defaultString(problem_record[19]))
                            .add("created_by_last", StringUtils.defaultString(problem_record[20]))
                            .add("created_by_phone_mobile", StringUtils.defaultString(problem_record[21]))
                            .add("incidents", NumberUtils.toInt(problem_record[23], 0))
                            .add("knowledge_article_id", NumberUtils.toInt(problem_record[24], 0))
                            .add("knowledge_article_name", StringUtils.defaultString(problem_record[25])) 
                        );
                }
                json_builder.add("recordsTotal", total_filtered_count);
                json_builder.add("recordsFiltered", total_filtered_count);
                json_builder.add("draw", StringUtils.defaultString(request.getParameter("draw")));
                json_builder.add("data", json_array_builder);
                json = json_builder.build();

                con.close();
                //send the JSON data
                out.print(json.toString());
            }
            catch(Exception e)
            {
                e.printStackTrace();
                logger.error("ERROR Exception in servlet ajax_lookup_problems:=" + e);
                stringBuilder.setLength(0);
                stringBuilder.append("Error in retrieving users list");
                out.print(stringBuilder.toString());

            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Copyright 2019 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class admin_system_select_field_edit extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String query = "";
        try
        {
            HttpSession session = request.getSession();
            String context_dir = request.getServletContext().getRealPath("");
            
            if (session.getAttribute("authenticated") == null) 
            {
                response.sendRedirect("index.jsp");
            }
            else
            {
                //do work here
                String table = request.getParameter("table");
                String column = request.getParameter("column");
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                //delete all the current settings
                query = "DELETE FROM system_select_fields WHERE `table`=? AND `column`=?";
                PreparedStatement stmt = con.prepareStatement(query);
                stmt.setString(1,table);
                stmt.setString(2,column);
                stmt.executeUpdate();
                
                query = "INSERT INTO system_select_fields (`table`,`column`,`value`,`label`,`order`,`active`) VALUES (?,?,?,?,?,?)";
                stmt = con.prepareStatement(query);
                

                //get all the request parameters
                Map params = request.getParameterMap();
                Iterator i = params.keySet().iterator();
                while (i.hasNext())
                {
                    String name = (String) i.next();
                    String value = ((String[]) params.get( name ))[ 0 ];
                    //System.out.println("name=" + name + " value=" + value);
                    if(name.startsWith("index_row_"))
                    {
                        String row_index = name.replace("index_row_", "");
                        String order = value;
                        String label = request.getParameter("display_row_" + row_index);
                        String db_value = request.getParameter("db_row_" + row_index);
                        String active = "false";
                        try
                        {
                            active = request.getParameter("active_row_" + row_index);
                            if(active != null || !active.equalsIgnoreCase(""))
                            {
                                active = "true";
                            }
                        }
                        catch(Exception e)
                        {
                            active = "false";
                        }
                        stmt.setString(1,table);
                        stmt.setString(2,column);
                        stmt.setString(3,db_value);
                        stmt.setString(4,label);
                        stmt.setString(5,order);
                        stmt.setString(6,active);
                        stmt.executeUpdate();
                    }
                }
                //stmt.close();
                con.close();
            }
            
        }
        catch (Exception e) 
        {
            logger.error("ERROR Exception in servlet admin_system_select_field_edit:=" + e);
        }
        response.sendRedirect("admin_system_select_fields.jsp");
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.Tika;
import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;

@MultipartConfig()
public class project_task_add extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
    private static SimpleDateFormat form_format = new SimpleDateFormat("HH:mm / dd-MMM-yy"); //10:42 / 24-Jan-20
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            String user_tz_name = "US/Eastern";
            String user_tz_time = "-05:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "US/Eastern";
                    user_tz_time = "-05:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "US/Eastern";
                user_tz_time = "-05:00";
            }
            java.util.Date now = new java.util.Date();
            String currentTimestamp = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(support.date_utils.now_in_utc());
            try
            {                
                String context_dir = request.getServletContext().getRealPath("");
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                        
                int project_id = 0;
                try{project_id = Integer.parseInt(request.getParameter("project_id"));}catch (Exception e){project_id=0;}
                int project_task_id = db.get_projects.next_project_task_id_for_project(con, project_id);
                String name = "";
                try{name = request.getParameter("name");}catch (Exception e){name="";}
                String description = "";
                try{description = request.getParameter("description");}catch (Exception e){description="";}
                int assigned_group_id = 0;
                try{assigned_group_id = Integer.parseInt(request.getParameter("assigned_group_id"));}catch (Exception e){assigned_group_id=0;}
                int assigned_to_id = 0;
                try{assigned_to_id = Integer.parseInt(request.getParameter("assigned_to_id"));}catch (Exception e){assigned_to_id=0;}
                String priority = "";
                try{priority = request.getParameter("priority");}catch (Exception e){priority="";}
                String status = "";
                try{status = request.getParameter("status");}catch (Exception e){status="";}
                String scheduled_start_date = null;
                try
                {
                    String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("scheduled_start_date")).getTime()));
                    ZonedDateTime time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                    scheduled_start_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(time_utc);
                }
                catch(Exception e)
                {
                    scheduled_start_date = null;
                }                
                String actual_start_date = null;
                try
                {
                    String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("actual_start_date")).getTime()));
                    ZonedDateTime time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                    actual_start_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(time_utc);
                }
                catch(Exception e)
                {
                    actual_start_date = null;
                }
                String scheduled_end_date = null;
                try
                {
                    String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("scheduled_end_date")).getTime()));
                    ZonedDateTime time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                    scheduled_end_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(time_utc);
                }
                catch(Exception e)
                {
                    scheduled_end_date = null;
                }
                String actual_end_date = null;
                try
                {
                    String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("actual_end_date")).getTime()));
                    ZonedDateTime time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                    actual_end_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(time_utc);
                }
                catch(Exception e)
                {
                    actual_end_date = null;
                }
                int estimated_duration = 0;
                try{estimated_duration = Integer.parseInt(request.getParameter("estimated_duration"));}catch (Exception e){estimated_duration=0;}
                int actual_duration = 0;
                try{actual_duration = Integer.parseInt(request.getParameter("actual_duration"));}catch (Exception e){actual_duration=0;}
                String notes = "";
                try{notes = request.getParameter("notes");}catch (Exception e){notes="";}
                
                String insert_project_tasks_table = "INSERT INTO project_tasks (id,project_id,name,description,assigned_group_id,assigned_to_id,priority,status,scheduled_start_date,actual_start_date,scheduled_end_date,actual_end_date,estimated_duration,actual_duration,notes) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                PreparedStatement stmt = con.prepareStatement(insert_project_tasks_table);
                stmt.setInt(1, project_task_id); //id
                stmt.setInt(2, project_id); //project_id
                stmt.setString(3,name); //name
                stmt.setString(4,description); //description
                stmt.setInt(5, assigned_group_id); //assigned_group_id
                stmt.setInt(6, assigned_to_id); //assigned_to_id
                stmt.setString(7,priority); //priority
                stmt.setString(8,status); //status
                stmt.setString(9,scheduled_start_date); //scheduled_start_date
                stmt.setString(10,actual_start_date); //actual_start_date
                stmt.setString(11,scheduled_end_date); //scheduled_end_date
                stmt.setString(12,actual_end_date); //actual_end_date
                stmt.setInt(13, estimated_duration); //estimated_duration
                stmt.setInt(14, actual_duration); //actual_duration
                stmt.setString(15,notes); //notes 
                stmt.execute();
                
                CouchDbClient dbClient = null;
                try
                {
                    LinkedHashMap props = support.config.get_config(context_dir);
                    String customer_db = session.getAttribute("db").toString();
                    dbClient = db.couchdb.get_client(props);
                    if (dbClient == null)
                    {
                        throw new Exception("dbClient is null");
                    }
                    String user_id = session.getAttribute("user_id").toString();
                    String name_on_disk;
                    String file_name;
                    Tika tika = new Tika();

                    for (Part filePart : request.getParts()) {
    //                    logger.info("filepart:" + filePart);
                        String fileName = filePart.getSubmittedFileName();
                        if (fileName != null && !"".equals(fileName)) 
                        {
                            long file_size = filePart.getSize();
                            String mimeType = tika.detect(filePart.getInputStream());
                            file_name = fileName.replaceAll("[^a-zA-Z0-9\\._]+", "_");

                            UUID Uuid = UUID.randomUUID(); //Generates random UUID                                  
                            String uuid = Uuid.toString();

                            Map<String, Object> map = new HashMap<>(); 
                            map.put("_id", uuid);
                            map.put("Filename", file_name);
                            map.put("MIME_type", mimeType);
                            map.put("FileSize", file_size);
                            map.put("DateUploaded", now);
                            map.put("UploadedBy", user_id);
                            map.put("AssociatedObject", "Project:Task");
                            map.put("AssociatedObjectId", project_id + ":" + project_task_id);

                            Response dbclient_response = dbClient.save(map);
                            name_on_disk = uuid + "/"+ file_name; 
 
                            dbclient_response = dbClient.saveAttachment(filePart.getInputStream(), file_name, mimeType, dbclient_response.getId(), dbclient_response.getRev());

                            if (dbclient_response.getError() == null)
                            {

                                String query_attachment = "INSERT INTO project_task_attachements (`project_id`, `task_id`, `uuid`,`name_on_disk`,`file_name`,`file_date`,`file_size`,`rev`,`uploaded_by`) VALUES (?,?,?,?,?,?,?,?,?)";
                                PreparedStatement stmt_attachment = con.prepareStatement(query_attachment);
                                stmt_attachment.setInt(1, project_id);
                                stmt_attachment.setInt(2, project_task_id);
                                stmt_attachment.setString(3, uuid);
                                stmt_attachment.setString(4, name_on_disk);
                                stmt_attachment.setString(5, file_name);
                                Timestamp file_date = new Timestamp(now.getTime());
                                stmt_attachment.setTimestamp(6, file_date);
                                stmt_attachment.setInt(7, Math.toIntExact(file_size));
                                stmt_attachment.setString(8, dbclient_response.getRev());
                                stmt_attachment.setInt(9, Integer.parseInt(user_id));
                                //System.out.println("email_support 94 stmt=" + stmt);
                                stmt_attachment.execute();
                            } else {
                                logger.error("CouchDB attachment upload error: {}", dbclient_response.getError());
                            }

                        }

                    }
                } catch (Exception e)
                {
                    logger.error("ERROR attachments process in servlet project_task_add:=" + e);
                } finally {
                    if (dbClient != null )
                    {
                        dbClient.shutdown();
                    }
                }


//                String RedirectURL = "project.jsp?project_id=" + project_id ;
//                response.sendRedirect(RedirectURL);
            }
            catch(Exception e)
            {
                logger.error("ERROR Exception in servlet project_task_add:=" + e);
            } 
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

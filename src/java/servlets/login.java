//Copyright 2021 XaSystems, Inc. , All rights reserved.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;


import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 *
 * @author server-xc6701
 */
public class login extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        logger.info("DEBUG: Start login servlet:");
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        
        boolean login_good = false;
        boolean license_good = true;
        int seats = -1;
        boolean seat_available = false;
        HttpSession session = request.getSession();        
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String home_selection = request.getParameter("home_selection");  //service_desk  or analytics
        String installation_type = props.get("installation_type").toString().trim();
        String login_url = props.get("login_url").toString();
        String RedirectURL = login_url + "?error=Unknown";
        int session_timeout = 30;
        boolean self_service_only = true;
        
        //check user inputs
        if(username == null || password == null || home_selection == null)
        {
            RedirectURL = "index.jsp?error=missing_parameter";
        }
        else
        {
            try
            {
                session.setAttribute("home_page", "home_service_desk.jsp");
                session.setAttribute("installation_type",installation_type);
                session.setAttribute("db",props.get("db").toString());
                session.setAttribute("db_host",props.get("db_host").toString());
                session.setAttribute("db_port",props.get("db_port").toString());
                session.setAttribute("db_user",props.get("db_user").toString());
                session.setAttribute("db_password",props.get("db_password").toString());
                session.setAttribute("db_password_encrypted",props.get("db_password_encrypted").toString());
                session.setAttribute("debug",props.get("debug").toString());
                session.setAttribute("log_level",props.get("log_level").toString());
                session.setAttribute("timezone",props.get("timezone").toString());
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_info[] = db.get_users.by_username(con, username);
                String user_id = user_info[0];
                //get session_timeout
                try
                {
                    session_timeout = Integer.parseInt(db.get_configuration_properties.by_name(con, "session_timeout"));
                }
                catch(Exception e)
                {
                    session_timeout = 30;
                }
                session.setAttribute("session_timeout",String.valueOf(session_timeout));
                
                
                //check license exist and is good
                String license = db.get_license.all(con);
                if(!license.equalsIgnoreCase(""))
                {
                    seats = support.util.validate_license(context_dir, license);

                    //check # sessions
                    int number_of_sessions = db.get_user_sessions.active(con, session_timeout,user_id);
                    //System.out.println("actice session=" + number_of_sessions);
                    System.out.println("seats=" + seats);
                    //DO LICENSE CHECK HERE
                    if(seats == -1)
                    {
                        license_good = false;
                    }
                    else
                    {
                        if(number_of_sessions <= seats)
                        {
                            seat_available = true;
                        }
                    }


                    //license is good a seat available the login
                    if(license_good && seat_available)
                    {
                        String unencrypted_password = support.encrypt_utils.decrypt(context_dir, user_info[2]);
                        //System.out.println("LOGIN unencrypted_password=" + unencrypted_password);
                        if(unencrypted_password.equals(password))
                        {
                            //login good
                            login_good = true;
                            session.setAttribute("authenticated","true");
                            session.setAttribute("username",username);
                            session.setAttribute("user_id",user_info[0]);
                            session.setAttribute("user_first",user_info[3]);
                            session.setAttribute("user_last",user_info[5]);
                            //add up all the roles. highest permission is used from each role for each function
                            ArrayList <String> permission_values = new ArrayList();
                            permission_values.add("none");
                            permission_values.add("read");
                            permission_values.add("create");
                            permission_values.add("update");
                            permission_values.add("delete");

                            int role_level_value[] = {0,0,0,0,0,0,0,0,0,0,0,0,0};
                            //financial=0
                            //incident=1
                            //request=2
                            //contact=3
                            //cx=4
                            //survey=5
                            //sla=6
                            //project=7
                            //job=8
                            //metric=9
                            //asset=10
                            //user=11
                            //self_service=12
                            
                            String admin = "false";
                            String manager = "false";
                            //get all users assigned roles
                            ArrayList <String[]> user_assigned_roles = db.get_roles.roles_for_user_id(con, user_id);
                            for(int a = 0; a < user_assigned_roles.size();a++)
                            {
                                String user_role[] = user_assigned_roles.get(a);
                                if(user_role[6].equalsIgnoreCase("true")) //admin
                                {
                                    self_service_only = false;
                                    admin = "true";
                                    manager = "true";
                                    role_level_value[0] = 4;
                                    role_level_value[1] = 4;
                                    role_level_value[2] = 4;
                                    role_level_value[3] = 4;
                                    role_level_value[4] = 4;
                                    role_level_value[5] = 4;
                                    role_level_value[6] = 4;
                                    role_level_value[7] = 4;
                                    role_level_value[8] = 4;
                                    role_level_value[9] = 4;
                                    role_level_value[10] = 4;
                                    role_level_value[11] = 4;
                                    role_level_value[12] = 4;
                                    a = user_assigned_roles.size();
                                }
                                else if(user_role[7].equalsIgnoreCase("true")) //manager
                                {
                                    self_service_only = false;
                                    manager = "true";
                                    role_level_value[0] = 4;
                                    role_level_value[1] = 4;
                                    role_level_value[2] = 4;
                                    role_level_value[3] = 4;
                                    role_level_value[4] = 4;
                                    role_level_value[5] = 4;
                                    role_level_value[6] = 4;
                                    role_level_value[7] = 4;
                                    role_level_value[8] = 4;
                                    role_level_value[9] = 4;
                                    role_level_value[10] = 4;
                                    role_level_value[11] = 4;
                                    role_level_value[12] = 4;
                                    a = user_assigned_roles.size();
                                }
                                //financial/////////////////////////////////////////////////////////////////////
                                int financial_value = permission_values.indexOf(user_role[8]);
                                if(financial_value > role_level_value[0])
                                {
                                    role_level_value[0] = financial_value;
                                }
                                if(financial_value > 0)
                                {
                                    self_service_only = false;
                                }
                                //incident/////////////////////////////////////////////////////////////////////
                                int incident_value = permission_values.indexOf(user_role[9]);
                                if(incident_value > role_level_value[1])
                                {
                                    role_level_value[1] = incident_value;
                                }
                                if(incident_value > 0)
                                {
                                    self_service_only = false;
                                }
                                //request/////////////////////////////////////////////////////////////////////
                                int request_value = permission_values.indexOf(user_role[10]);
                                if(request_value > role_level_value[2])
                                {
                                    role_level_value[2] = request_value;
                                }
                                if(request_value > 0)
                                {
                                    self_service_only = false;
                                }
                                //contact/////////////////////////////////////////////////////////////////////
                                int contact_value = permission_values.indexOf(user_role[11]);
                                if(contact_value > role_level_value[3])
                                {
                                    role_level_value[3] = contact_value;
                                }
                                if(contact_value > 0)
                                {
                                    self_service_only = false;
                                }
                                //cx/////////////////////////////////////////////////////////////////////
                                int cx_value = permission_values.indexOf(user_role[12]);
                                if(cx_value > role_level_value[4])
                                {
                                    role_level_value[4] = cx_value;
                                }
                                if(cx_value > 0)
                                {
                                    self_service_only = false;
                                }
                                //survey/////////////////////////////////////////////////////////////////////
                                int survey_value = permission_values.indexOf(user_role[13]);
                                if(survey_value > role_level_value[5])
                                {
                                    role_level_value[5] = survey_value;
                                }
                                if(survey_value > 0)
                                {
                                    self_service_only = false;
                                }
                                //sla/////////////////////////////////////////////////////////////////////
                                int sla_value = permission_values.indexOf(user_role[14]);
                                if(sla_value > role_level_value[6])
                                {
                                    role_level_value[6] = sla_value;
                                }
                                if(sla_value > 0)
                                {
                                    self_service_only = false;
                                }
                                //project/////////////////////////////////////////////////////////////////////
                                int project_value = permission_values.indexOf(user_role[15]);
                                if(project_value > role_level_value[7])
                                {
                                    role_level_value[7] = project_value;
                                }
                                if(project_value > 0)
                                {
                                    self_service_only = false;
                                }
                                //job/////////////////////////////////////////////////////////////////////
                                int job_value = permission_values.indexOf(user_role[16]);
                                if(job_value > role_level_value[8])
                                {
                                    role_level_value[8] = job_value;
                                }
                                if(job_value > 0)
                                {
                                    self_service_only = false;
                                }
                                //metric/////////////////////////////////////////////////////////////////////
                                int metric_value = permission_values.indexOf(user_role[17]);
                                if(metric_value > role_level_value[9])
                                {
                                    role_level_value[9] = metric_value;
                                }
                                if(metric_value > 0)
                                {
                                    self_service_only = false;
                                }
                                //asset/////////////////////////////////////////////////////////////////////
                                int asset_value = permission_values.indexOf(user_role[18]);
                                if(asset_value > role_level_value[10])
                                {
                                    role_level_value[10] = asset_value;
                                }
                                if(asset_value > 0)
                                {
                                    self_service_only = false;
                                }
                                //user/////////////////////////////////////////////////////////////////////
                                int user_value = permission_values.indexOf(user_role[19]);
                                if(user_value > role_level_value[11])
                                {
                                    role_level_value[11] = user_value;
                                }
                                if(user_value > 0)
                                {
                                    self_service_only = false;
                                }
                                //self_service/////////////////////////////////////////////////////////////////////
                                int self_service_value = permission_values.indexOf(user_role[20]);
                                if(self_service_value > role_level_value[12])
                                {
                                    role_level_value[12] = self_service_value;
                                }
                            }
                            //set session vars                    
                            session.setAttribute("administration",admin);
                            session.setAttribute("manager",manager);
                            session.setAttribute("financial",permission_values.get(role_level_value[0]));
                            session.setAttribute("incident",permission_values.get(role_level_value[1]));
                            session.setAttribute("request",permission_values.get(role_level_value[2]));
                            session.setAttribute("contact",permission_values.get(role_level_value[3]));
                            session.setAttribute("cx",permission_values.get(role_level_value[4]));
                            session.setAttribute("survey",permission_values.get(role_level_value[5]));
                            session.setAttribute("sla",permission_values.get(role_level_value[6]));
                            session.setAttribute("project",permission_values.get(role_level_value[7]));
                            session.setAttribute("job",permission_values.get(role_level_value[8]));
                            session.setAttribute("metric",permission_values.get(role_level_value[9]));
                            session.setAttribute("asset",permission_values.get(role_level_value[10]));
                            session.setAttribute("user",permission_values.get(role_level_value[11]));
                            session.setAttribute("self_service",permission_values.get(role_level_value[12]));
                            if(self_service_only)
                            {
                                session.setAttribute("self_service_only","true");
                            }
                            else
                            {
                                session.setAttribute("self_service_only","false");
                            }
                            session.setAttribute("tz_name",user_info[21]);
                            session.setAttribute("tz_time",user_info[22]);
                        }
                        else
                        {
                            //password is bad  
                            login_good = false;
                            RedirectURL = login_url + "?error=Username/Password incorrect!";
                        }
                    }
                }//end if license = ""
                else
                {
                    license_good = false;
                }

            }
            catch(Exception ex) 
            {
                System.out.println("Exception in login servlet " + ex); 
                logger.debug("ERROR: Exception in login servlet:" + ex);
            }
            //start redirecting
            if(!license_good)
            {
                RedirectURL = "admin_license_add_only.jsp" ;
            }
            else //license is good
            {
                if(login_good)
                {
                    if(home_selection.equalsIgnoreCase("service_desk"))
                    {
                        RedirectURL = "home_service_desk.jsp" ;
                    }
                    else
                    {
                        RedirectURL = "home_analytics.jsp" ;
                    }
                }
                else //login failed
                {
                    login_url = props.get("login_url").toString();
                    if(!seat_available)
                    {
                        RedirectURL = login_url + "?error=License";
                    }
                    else
                    {
                        RedirectURL = login_url + "?error=Password";
                    }
                }
            }
        }
        //System.out.println("start redirecting=" + RedirectURL);
        response.sendRedirect(RedirectURL);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

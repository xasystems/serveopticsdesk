/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 * 
 * 
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author server-xc6701
 */
public class admin_roles_edit extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            try 
            {
                java.util.Date now_date = new java.util.Date();
                SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");    
                String created = timestamp_format.format(now_date);
                
                String id = request.getParameter("id");
                String name = request.getParameter("name");
                String description = request.getParameter("description");
                String administration = request.getParameter("administration");
                String manager = request.getParameter("manager");
                String financial = request.getParameter("financial");
                String incident = request.getParameter("incident");
                String param_request = request.getParameter("param_request");
                String contact = request.getParameter("contact");
                String cx = request.getParameter("cx");
                String survey = request.getParameter("survey");
                String sla = request.getParameter("sla");
                String project = request.getParameter("project");
                String job = request.getParameter("job");
                String metric = request.getParameter("metric");
                String asset = request.getParameter("asset");
                String user = request.getParameter("user");
                String self_service = request.getParameter("self_service");
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement("UPDATE roles SET name=?,description=?,administration=?,manager=?,financial=?,incident=?,request=?,contact=?,cx=?,survey=?,sla=?, project=?, job=?, metric=?, asset=?, user=?, self_service=? WHERE id=?");
                                
                stmt.setString(1,name);
                stmt.setString(2,description);
                stmt.setString(3,administration);
                stmt.setString(4,manager);
                stmt.setString(5,financial);
                stmt.setString(6,incident);
                stmt.setString(7,param_request);
                stmt.setString(8,contact);
                stmt.setString(9,cx);
                stmt.setString(10,survey);
                stmt.setString(11,sla);
                stmt.setString(12,project);
                stmt.setString(13,job);
                stmt.setString(14,metric);
                stmt.setString(15,asset);
                stmt.setString(16,user);
                stmt.setString(17,self_service);
                stmt.setString(18,id);
                stmt.executeUpdate();
                stmt.close();
                con.close();
                session.setAttribute("alert", "Role successfully updated");
                session.setAttribute("alert-class", "alert-success");
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_roles_edit:=" + e);
                session.setAttribute("alert", "Error updating role");
                session.setAttribute("alert-class", "alert-dangerNew");
            }
            response.sendRedirect("admin_roles.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

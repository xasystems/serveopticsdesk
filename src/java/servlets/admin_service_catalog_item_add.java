/*
<!--Copyright 2020 Real Data Technologies, Inc. , All rights reserved.-->
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.Tika;
import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;

@MultipartConfig()
/**
 *
 * @author ralph
 */
public class admin_service_catalog_item_add extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            int approver_id = 0;
            try 
            {
                String catalog_id = request.getParameter("catalog_id");
                String name = request.getParameter("name");
                String description = request.getParameter("description");
                String category = request.getParameter("category");
                String state = request.getParameter("state");
                
                String critical_fixed_cost = request.getParameter("critical_fixed_cost");
                String critical_recurring_cost = request.getParameter("critical_recurring_cost");
                String critical_recurring_cost_frequency = request.getParameter("critical_recurring_cost_frequency");
                String critical_delivery_time = request.getParameter("critical_delivery_time");
                String critical_delivery_time_unit = request.getParameter("critical_delivery_time_unit");
                String critical_schedule_id = request.getParameter("critical_schedule_id");
                String critical_delivery_group_id = request.getParameter("critical_delivery_group_id");
                
                String high_fixed_cost = request.getParameter("high_fixed_cost");
                String high_recurring_cost = request.getParameter("high_recurring_cost");
                String high_recurring_cost_frequency = request.getParameter("high_recurring_cost_frequency");
                String high_delivery_time = request.getParameter("high_delivery_time");
                String high_delivery_time_unit = request.getParameter("high_delivery_time_unit");
                String high_schedule_id = request.getParameter("high_schedule_id");
                String high_delivery_group_id = request.getParameter("high_delivery_group_id");
                
                String medium_fixed_cost = request.getParameter("medium_fixed_cost");
                String medium_recurring_cost = request.getParameter("medium_recurring_cost");
                String medium_recurring_cost_frequency = request.getParameter("medium_recurring_cost_frequency");
                String medium_delivery_time = request.getParameter("medium_delivery_time");
                String medium_delivery_time_unit = request.getParameter("medium_delivery_time_unit");
                String medium_schedule_id = request.getParameter("medium_schedule_id");
                String medium_delivery_group_id = request.getParameter("medium_delivery_group_id");
                
                String low_fixed_cost = request.getParameter("low_fixed_cost");
                String low_recurring_cost = request.getParameter("low_recurring_cost");
                String low_recurring_cost_frequency = request.getParameter("low_recurring_cost_frequency");
                String low_delivery_time = request.getParameter("low_delivery_time");
                String low_delivery_time_unit = request.getParameter("low_delivery_time_unit");
                String low_schedule_id = request.getParameter("low_schedule_id");
                String low_delivery_group_id = request.getParameter("low_delivery_group_id");
                
                String required_user_input = request.getParameter("required_user_input");
                String escaped_required_user_input = StringEscapeUtils.escapeHtml4(required_user_input);
                
                String optional_user_input = request.getParameter("optional_user_input");
                String escaped_optional_user_input = StringEscapeUtils.escapeHtml4(optional_user_input);      
                
                StringBuilder approvers_list_json = new StringBuilder(); 
                approvers_list_json.append("{\"approvers\": {\"approver\": [");
                ///APPROVER
                String approver_radio = request.getParameter("approver_radio");
                if(approver_radio.equalsIgnoreCase("auto"))
                {
                    approvers_list_json.append("{\"id\": \"" + approver_id + "\",\"type\": \"auto\",\"name\": \"auto\"}");
                    approver_id++;
                }
                else if(approver_radio.equalsIgnoreCase("manual"))
                {
                    approvers_list_json.append("{\"id\": \"" + approver_id + "\",\"type\": \"manual\",\"name\": \"manual\"}");
                    approver_id++;
                }
                else if(approver_radio.equalsIgnoreCase("users_group_poc"))
                {
                    //get group_ids for this user
                    approvers_list_json.append("{\"id\": \"" + approver_id + "\",\"type\": \"group_poc\",\"name\": \"group_poc\"}");
                    approver_id++;
                }
                else if(approver_radio.equalsIgnoreCase("group_anybody"))
                {
                    //get approver_group_ids multi-select
                    String anybody_groups[] = request.getParameterValues("approver_group_ids");
                    approvers_list_json.append("{\"id\":\"" + approver_id + "\",\"type\":\"group_list\",\"approver_list\":[");
                    for(int a = 0; a < anybody_groups.length;a++)
                    {
                        if(a==0)
                        {
                            approvers_list_json.append("{\"group_id\":\"" + anybody_groups[a] + "\"}");
                        }
                        else
                        {
                            approvers_list_json.append(",{\"group_id\":\"" + anybody_groups[a] + "\"}");
                        }
                    }
                    approvers_list_json.append("]}");  
                    approver_id++;
                }
                else if(approver_radio.equalsIgnoreCase("individual"))
                {
                    //get list approver_list multi-select
                    String individuals[] = request.getParameterValues("approver_list");
                    approvers_list_json.append("{\"id\":\"" + approver_id + "\",\"type\":\"individual\",\"approver_list\":[");
                    for(int a = 0; a < individuals.length;a++)
                    {
                        if(a==0)
                        {
                            approvers_list_json.append("{\"user_id\":\"" + individuals[a] + "\"}");
                        }
                        else
                        {
                            approvers_list_json.append(",{\"user_id\":\"" + individuals[a] + "\"}");
                        }
                    }
                    approvers_list_json.append("]}");  
                    approver_id++;
                }
                //close the json
                approvers_list_json.append("]}}");
                /*
                {
                    "approvers": 
                    {
                        "approver": 
                        [
                            {
                                "id": "0",
                                "type": "auto",
                                "name": "auto"
                            },
                            {
                                "id": "1",
                                "type": "position",
                                "name": "group_poc"
                            },
                            {
                                "id": "2",
                                "type": "user_list",
                                "approver_list": 
                                [
                                    {
                                      "user_id": "1234"
                                    },
                                    {
                                      "user_id": "23456"
                                    },
                                    {
                                      "user_id": "46578"
                                    }
                                ]
                            },
                            {
                                "id": "3",
                                "type": "group_list",
                                "approver_list": 
                                [
                                    {
                                      "group_id": "1234"
                                    },
                                    {
                                      "group_id": "23456"
                                    },
                                    {
                                      "group_id": "46578"
                                    }
                                ]
                            }
                        ]
                    }
                }
                */
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement("INSERT INTO service_catalog_item (catalog_id,name,description,category,state,approver_list,critical_fixed_cost,critical_recurring_cost,critical_recurring_cost_frequency,critical_delivery_time,critical_delivery_time_unit,critical_schedule_id,critical_delivery_group_id,high_fixed_cost,high_recurring_cost,high_recurring_cost_frequency,high_delivery_time,high_delivery_time_unit,high_schedule_id,high_delivery_group_id,medium_fixed_cost,medium_recurring_cost,medium_recurring_cost_frequency,medium_delivery_time,medium_delivery_time_unit,medium_schedule_id,medium_delivery_group_id,low_fixed_cost,low_recurring_cost,low_recurring_cost_frequency,low_delivery_time,low_delivery_time_unit,low_schedule_id,low_delivery_group_id,required_user_input,optional_user_input) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
                stmt.setString(1,catalog_id);
                stmt.setString(2,name);
                stmt.setString(3,description);
                stmt.setString(4,category);
                stmt.setString(5,state);
                stmt.setString(6, approvers_list_json.toString());
                stmt.setString(7,critical_fixed_cost);
                stmt.setString(8,critical_recurring_cost);
                stmt.setString(9,critical_recurring_cost_frequency);
                stmt.setString(10,critical_delivery_time);
                stmt.setString(11,critical_delivery_time_unit);
                stmt.setString(12,critical_schedule_id);
                stmt.setString(13,critical_delivery_group_id);
                stmt.setString(14,high_fixed_cost);
                stmt.setString(15,high_recurring_cost);
                stmt.setString(16,high_recurring_cost_frequency);
                stmt.setString(17,high_delivery_time);
                stmt.setString(18,high_delivery_time_unit);
                stmt.setString(19,high_schedule_id);
                stmt.setString(20,high_delivery_group_id);
                stmt.setString(21,medium_fixed_cost);
                stmt.setString(22,medium_recurring_cost);
                stmt.setString(23,medium_recurring_cost_frequency);
                stmt.setString(24,medium_delivery_time);
                stmt.setString(25,medium_delivery_time_unit);
                stmt.setString(26,medium_schedule_id);
                stmt.setString(27,medium_delivery_group_id);
                stmt.setString(28,low_fixed_cost);
                stmt.setString(29,low_recurring_cost);
                stmt.setString(30,low_recurring_cost_frequency);
                stmt.setString(31,low_delivery_time);
                stmt.setString(32,low_delivery_time_unit);
                stmt.setString(33,low_schedule_id);
                stmt.setString(34,low_delivery_group_id);
                stmt.setString(35,escaped_required_user_input);
                stmt.setString(36,escaped_optional_user_input);               
                int affectedRows = stmt.executeUpdate();
                String id = "";
                if (affectedRows == 0) {
                    throw new SQLException("Creating catalog_item failed");
                }
                //stmt.close();

                try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        id = generatedKeys.getString(1);
                    }
                    else {
                        throw new SQLException("Creating catalog_item failed, no ID obtained.");
                    }
                }

                CouchDbClient dbClient;
                LinkedHashMap props = support.config.get_config(context_dir);

                //start get the file attachment if there is one
                try
                {
                    Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
                    if (filePart != null && filePart.getSize() > 0)
                    {
                        try
                        {
                            dbClient = db.couchdb.get_client(props);
                            if (dbClient == null)
                            {
                                throw new Exception("dbClient is null");
                            }
                            String file_name;
                            Tika tika = new Tika();

                            String fileName = filePart.getSubmittedFileName();
//                            if (fileName != null && !"".equals(fileName)) 
                            long file_size = filePart.getSize();
                            String mimeType = tika.detect(filePart.getInputStream());
                            String request_mime_type = filePart.getContentType();
                            if (request_mime_type.equals("image/svg+xml")) {
                                mimeType = request_mime_type;
                            }
                            file_name = "image";

                            String uuid;
                            String rev;
                            int new_uuid = 0;
                            Response dbclient_response;

                            UUID Uuid = UUID.randomUUID(); //Generates random UUID                                  

                            String user_id = session.getAttribute("user_id").toString();
                            java.util.Date now = new java.util.Date();

                            Map<String, Object> map = new HashMap<>(); 
                            map.put("Filename", file_name);
                            map.put("MIME_type", mimeType);
                            map.put("FileSize", file_size);
                            map.put("DateUploaded", now);
                            map.put("UploadedBy", user_id);
                            map.put("AssociatedObject", "Service Catalog Item");
                            map.put("AssociatedObjectId", id);

                            uuid = Uuid.toString();
                            map.put("_id", uuid);
                            dbclient_response = dbClient.save(map);
                            rev = dbclient_response.getRev();

                            dbclient_response = dbClient.saveAttachment(filePart.getInputStream(), file_name, mimeType, uuid, rev);

                            if (dbclient_response.getError() != null)
                            {
                                throw new Exception("CouchDB attachment upload error: " + dbclient_response.getError());
                            }

                            String query_image = "UPDATE service_catalog_item SET image = ? WHERE id = ?";
                            PreparedStatement stmt_attachment = con.prepareStatement(query_image);
                            stmt_attachment.setString(1, uuid);
                            stmt_attachment.setString(2, id);
                            stmt_attachment.execute();

                            int rows_affected = stmt_attachment.executeUpdate();
                            if (rows_affected <= 0)
                            {
                                throw new Exception("Image field is somehow not updated in the service_catalog_item table");
                            }
                        } catch (Exception e)
                        {
                            logger.error("ERROR attachments process in servlet admin_service_catalog_item_add:=" + e);
                        }

                    }
                }
                catch(Exception e)
                {
                    logger.error("ERROR Exception image saving in servlet admin_service_catalog_item_add:=" + e);
                }                


                stmt.close();
                con.close();

            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_service_catalog_item_add:=" + e);
            }
            response.sendRedirect("admin_service_catalog.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class admin_schedules_edit extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            try 
            {
                String temp = "";
                String id = request.getParameter("schedule_id");
                String name = request.getParameter("name");
                String description = request.getParameter("description");
                String start_time = request.getParameter("start_time");
                String end_time = request.getParameter("end_time");
                int is_all_day = 0;
                int include_holidays = 0;
                int sunday = 0; 
                int monday = 0; 
                int tuesday = 0; 
                int wednesday = 0; 
                int thursday = 0; 
                int friday = 0; 
                int saturday = 0; 
                try
                {
                    if(request.getParameter("is_all_day") == null || request.getParameter("is_all_day").equalsIgnoreCase("null"))
                    {
                        is_all_day = 0; 
                    }
                    else
                    {
                        is_all_day = 1; 
                    }
                }
                catch (Exception e)
                {
                    is_all_day = 0;
                }
                try
                {
                    if(request.getParameter("include_holidays") == null || request.getParameter("include_holidays").equalsIgnoreCase("null"))
                    {
                        include_holidays = 0; 
                    }
                    else
                    {
                        include_holidays = 1; 
                    }
                }
                catch (Exception e)
                {
                    include_holidays = 0;
                }
                try
                {
                    if(request.getParameter("sunday") == null || request.getParameter("sunday").equalsIgnoreCase("null"))
                    {
                        sunday = 0; 
                    }
                    else
                    {
                        sunday = 1; 
                    }
                }
                catch (Exception e)
                {
                    sunday = 0;
                }
                try
                {
                    if(request.getParameter("monday") == null || request.getParameter("monday").equalsIgnoreCase("null"))
                    {
                        monday = 0; 
                    }
                    else
                    {
                        monday = 1; 
                    }
                }
                catch (Exception e)
                {
                    sunday = 0;
                }
                try
                {
                    if(request.getParameter("tuesday") == null || request.getParameter("tuesday").equalsIgnoreCase("null"))
                    {
                        tuesday = 0; 
                    }
                    else
                    {
                        tuesday = 1; 
                    }
                }
                catch (Exception e)
                {
                    tuesday = 0;
                }
                try
                {
                    if(request.getParameter("wednesday") == null || request.getParameter("wednesday").equalsIgnoreCase("null"))
                    {
                        wednesday = 0; 
                    }
                    else
                    {
                        wednesday = 1; 
                    }
                }
                catch (Exception e)
                {
                    wednesday = 0;
                }
                try
                {
                    if(request.getParameter("thursday") == null || request.getParameter("thursday").equalsIgnoreCase("null"))
                    {
                        thursday = 0; 
                    }
                    else
                    {
                        thursday = 1; 
                    }
                }
                catch (Exception e)
                {
                    thursday = 0;
                }
                try
                {
                    if(request.getParameter("friday") == null || request.getParameter("friday").equalsIgnoreCase("null"))
                    {
                        friday = 0; 
                    }
                    else
                    {
                        friday = 1; 
                    }
                }
                catch (Exception e)
                {
                    friday = 0;
                }
                try
                {
                    if(request.getParameter("saturday") == null || request.getParameter("saturday").equalsIgnoreCase("null"))
                    {
                        saturday = 0; 
                    }
                    else
                    {
                        saturday = 1; 
                    }
                }
                catch (Exception e)
                {
                    saturday = 0;
                }
                
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement("UPDATE schedules SET name=?,description=?,start_time=?,end_time=?,is_all_day=?,include_holidays=?,sunday=?,monday=?,tuesday=?,wednesday=?,thursday=?,friday=?,saturday=? WHERE id=?");
                
                stmt.setString(1,name);
                stmt.setString(2,description);
                stmt.setString(3,start_time);
                stmt.setString(4,end_time);
                stmt.setInt(5,is_all_day);
                stmt.setInt(6,include_holidays);
                stmt.setInt(7,sunday);
                stmt.setInt(8,monday);
                stmt.setInt(9,tuesday);
                stmt.setInt(10,wednesday);
                stmt.setInt(11,thursday);
                stmt.setInt(12,friday);
                stmt.setInt(13,saturday);
                stmt.setString(14,id);
//                System.out.println("queue stmt=" + stmt.toString());
                stmt.executeUpdate();
                stmt.close();
                con.close();
                
                
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_schedule_edit:=" + e);
            }
            response.sendRedirect("admin_schedules.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

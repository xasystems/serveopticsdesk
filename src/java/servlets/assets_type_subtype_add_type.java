/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class assets_type_subtype_add_type extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String query = "";
        try
        {
            HttpSession session = request.getSession();
            String context_dir = request.getServletContext().getRealPath("");
            
            if (session.getAttribute("authenticated") == null) 
            {
                response.sendRedirect("index.jsp");
            }
            else
            {
                //do work here
                String type_active = "false";
                String assets_type_name = request.getParameter("assets_type_name");
                try
                {
                    type_active = request.getParameter("type_active");
                    if(type_active == null)
                    {
                        type_active = "false";
                    }
                    else
                    {
                        type_active = "true";
                    }
                }
                catch(Exception e)
                {
                    type_active = "false";
                }
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                //delete all the current settings
                query = "INSERT INTO asset_type (name,active) VALUES (?,?)";
                PreparedStatement stmt = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                stmt.setString(1,assets_type_name);
                stmt.setString(2,type_active);

                int affectedRows = stmt.executeUpdate();                

                if (affectedRows == 0) {
                    throw new SQLException("Creating asset type failed, no rows affected.");
                }
                String asset_type_id = "";
                try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        asset_type_id = generatedKeys.getString(1);

                        for(String name : request.getParameterMap().keySet())
                        {
                            if (name.contains("subtype_name"))
                            {
                                String subtype_name = request.getParameter(name);
                                String uniq = name.split("_")[2]; // subtype_name_1635522379098
                                String subtype_active = StringUtils.defaultString(request.getParameter("subtype_active_" + uniq), "false");
                                query = "INSERT INTO asset_subtype (name,active,asset_type_id) VALUES (?,?,?)";
                                stmt = con.prepareStatement(query);
                                stmt.setString(1,subtype_name);
                                stmt.setString(2,subtype_active);
                                stmt.setString(3,asset_type_id);
                                System.out.println("stmt=" + stmt);
                                stmt.executeUpdate();   
                            }
                        }
                    }
                    else {
                        throw new SQLException("Creating asset type failed, no ID obtained.");
                    }
                }

                stmt.close();
                con.close();
            }            
        }
        catch (Exception e) 
        {
            e.printStackTrace();
            logger.error("ERROR Exception in servlet assets_type_subtype_add_type:=" + e);
        }
        response.sendRedirect("assets_type_subtype.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

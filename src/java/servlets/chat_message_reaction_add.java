/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.Tika;
import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;

/**
 *
 * @author ralph
 */
@MultipartConfig()
@WebServlet("/chat_message_reaction_add")
public class chat_message_reaction_add extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
//        String user_id = request.getParameter("user_id");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

//        String user_id = request.getParameter("user_id");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        JsonObjectBuilder json_builder = Json.createObjectBuilder();
        JsonObject json;
        
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {

                if (session.getAttribute("authenticated") == null) 
                {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }

                logger.info(session);
                Connection con = db.db_util.get_contract_connection(context_dir, session);
//                StringBuffer jb = new StringBuffer();
//                String line = null;
//                BufferedReader reader = request.getReader();
//                while ((line = reader.readLine()) != null) {
//                    jb.append(line);
//                }

//                JsonReader jr = Json.createReader(new StringReader(jb.toString()));
//                JsonObject jo = jr.readObject();
                   
                String user_id = session.getAttribute("user_id").toString();
                String message_id = StringUtils.defaultString(request.getParameter("message_id"));
                String reaction = StringUtils.defaultString(request.getParameter("reaction"));
                boolean remove = BooleanUtils.toBoolean(request.getParameter("remove"));
                boolean update_session_table = db.get_user_sessions.update(con, user_id);

                if (!remove) {
                    String query = ""
                        + "INSERT INTO chat_message_reactions "
                        + "(message_id,user_id,reaction) "
                        + "VALUES "
                        + "(?,?,?)";
                    PreparedStatement stmt = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                    stmt.setString(1, message_id); 
                    stmt.setString(2, user_id); 
                    stmt.setString(3, reaction); 
                    int affectedRows = stmt.executeUpdate();
                    if (affectedRows == 0) {
                        throw new SQLException("Creating message reaction failed, no rows affected.");
                    }

                    try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                        if (generatedKeys.next()) {
                            long message_reaction_id = generatedKeys.getLong(1);
                        }
                        else {
                            throw new SQLException("Creating message reaction failed, no ID obtained.");
                        }
                    }
                } else {
                    String query = ""
                        + "DELETE FROM chat_message_reactions WHERE message_id = ? AND user_id = ? AND reaction = ?";
                    PreparedStatement stmt = con.prepareStatement(query);
                    stmt.setString(1, message_id); 
                    stmt.setString(2, user_id); 
                    stmt.setString(3, reaction); 
                    int affectedRows = stmt.executeUpdate();
                    if (affectedRows == 0) {
                        throw new SQLException("Updating message failed, no rows affected.");
                    }
                }

                    boolean success = true;
//                    logger.info("success {}", success);
                    json_builder.add("success", success);

                    json = json_builder.build();
                    con.close();
                    //send the JSON data
                    out.print(json.toString());
            }   catch(Exception e)
            {
                e.printStackTrace();
                logger.error("ERROR Exception in servlet chat_message_reaction_add:=" + e);
                json_builder.add("error", e.getMessage());
                json = json_builder.build();
                //send the JSON data
                out.print(json.toString());
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

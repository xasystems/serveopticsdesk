/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebServlet("/ajax_lookup_problem_incidents")
/**
 *
 * @author ralph
 */
public class ajax_lookup_problem_incidents extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
//        String user_id = request.getParameter("user_id");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

//        String user_id = request.getParameter("user_id");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        JsonObjectBuilder json_builder = Json.createObjectBuilder();
        JsonObject json;
        
        boolean first_record = true;
        boolean has_data = false;
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {

                if (session.getAttribute("authenticated") == null) 
                {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }

                Connection con = db.db_util.get_contract_connection(context_dir, session);
                int starting_record = 0;
                int previous_record = 0;
                int next_record = 0;
                int fetch_size = 50;
                
                try
                {
                    starting_record  = Integer.parseInt(request.getParameter("start"));
                    fetch_size  = Integer.parseInt(request.getParameter("length"));
                    next_record = starting_record + fetch_size;
                    previous_record = starting_record - fetch_size;
                    if(previous_record <= 0)
                    {
                        previous_record = 0;
                    }
                }
                catch(Exception e)
                {
                    starting_record = 0;
                    previous_record = 0;
                    next_record = starting_record + fetch_size;
                }

                String user_id = session.getAttribute("user_id").toString();
                String problem_id = request.getParameter("problem_id");
                
                boolean update_session_table = db.get_user_sessions.update(con, user_id);

                String[] status_filters = request.getParameterValues("status_filters[]");

                ArrayList<String[]> incidents = db.get_problems.problem_incidents_for_problem(con, problem_id, status_filters);

                Map<String, Integer> status_counters = new HashMap<String, Integer>();

                ArrayList<String[]> status_select = db.get_system_select_field.active_for_table_column(con, "incidents", "state");
                for(int a = 0; a < status_select.size(); a++)
                {
                    String select_option[] = status_select.get(a);
                    status_counters.put(select_option[3], 0);
                }

                JsonArrayBuilder json_array_builder = Json.createArrayBuilder();
                for(int a = 0; a < incidents.size();a++)
                {
                    String incident_record[] = incidents.get(a);
                    String incident_status = support.string_utils.check_for_null(incident_record[19]);                    
                    JsonArrayBuilder attachments_builder = Json.createArrayBuilder();                
                    try
                    { // processing attachments
                        ArrayList<String[]> attachments = new ArrayList();
                        attachments = db.get_incidents.get_attachments_by_incident_id(con, incident_record[0]);
                        logger.info("INFO ajax_lookup_problem_incidents attachments.size :=" + attachments.size());
                        if (attachments.size() > 0)
                        {   
                            for (int i = 0; i < attachments.size(); i++)
                            {
                                String[] attachment = attachments.get(i);
                                attachments_builder
                                    .add(Json.createObjectBuilder()
                                        .add("id", support.string_utils.check_for_null(attachment[0]))
                                        .add("incident_id", support.string_utils.check_for_null(attachment[1]))
                                        .add("uuid", support.string_utils.check_for_null(attachment[2]))
                                        .add("name_on_disk", support.string_utils.check_for_null(attachment[3]))
                                        .add("file_name", support.string_utils.check_for_null(attachment[4]))
                                        .add("file_date", support.string_utils.check_for_null(attachment[5]))
                                        .add("file_size", support.string_utils.check_for_null(attachment[6]))
                                        .add("rev", support.string_utils.check_for_null(attachment[7]))
                                        .add("uploaded_by", support.string_utils.check_for_null(attachment[8]))
                                        .add("uploaded_by_name", support.string_utils.check_for_null(attachment[9]))
                                    );
                            }
                        }
                    } catch (Exception e)
                    {
                        logger.error("ERROR processing attachments Exception in servlet ajax_lookup_problem_incidents:=" + e);
                    }


                    json_array_builder
                        .add(Json.createObjectBuilder()
                            .add("id", support.string_utils.check_for_null(incident_record[0]))
                            .add("incident_type", support.string_utils.check_for_null(incident_record[1]))
                            .add("incident_time", support.string_utils.check_for_null(incident_record[2]))
                            .add("caller_id", support.string_utils.check_for_null(incident_record[3]))
                            .add("caller_group_id", support.string_utils.check_for_null(incident_record[4]))
                            .add("location", support.string_utils.check_for_null(incident_record[5]))
                            .add("department", support.string_utils.check_for_null(incident_record[6]))
                            .add("site", support.string_utils.check_for_null(incident_record[7]))
                            .add("company", support.string_utils.check_for_null(incident_record[8]))
                            .add("category", support.string_utils.check_for_null(incident_record[9]))
                            .add("subcategory", support.string_utils.check_for_null(incident_record[10]))
                            .add("ci_id", support.string_utils.check_for_null(incident_record[11]))
                            .add("impact", support.string_utils.check_for_null(incident_record[12]))
                            .add("urgency", support.string_utils.check_for_null(incident_record[13]))
                            .add("priority", support.string_utils.check_for_null(incident_record[14]))
                            .add("description", support.string_utils.check_for_null(incident_record[15]))
                            .add("create_date", support.string_utils.check_for_null(incident_record[16]))
                            .add("create_by_id", support.string_utils.check_for_null(incident_record[17]))
                            .add("contact_method", support.string_utils.check_for_null(incident_record[18]))
                            .add("state", support.string_utils.check_for_null(incident_record[19]))
                            .add("state_date", support.string_utils.check_for_null(incident_record[20]))
                            .add("assigned_group_id", support.string_utils.check_for_null(incident_record[21]))
                            .add("assigned_to_id", support.string_utils.check_for_null(incident_record[22]))
                            .add("notes", support.string_utils.check_for_null(incident_record[23]))
                            .add("desk_notes", support.string_utils.check_for_null(incident_record[24]))
                            .add("related", support.string_utils.check_for_null(incident_record[25]))
                            .add("first_contact_resolution", support.string_utils.check_for_null(incident_record[26]))
                            .add("closed_date", support.string_utils.check_for_null(incident_record[27]))
                            .add("closed_reason", support.string_utils.check_for_null(incident_record[28]))
                            .add("pending_date", support.string_utils.check_for_null(incident_record[29]))
                            .add("pending_reason", support.string_utils.check_for_null(incident_record[30]))
                            .add("external_id", support.string_utils.check_for_null(incident_record[31]))
                            .add("caller_username", support.string_utils.check_for_null(incident_record[32]))
                            .add("caller_first", support.string_utils.check_for_null(incident_record[33]))
                            .add("caller_mi", support.string_utils.check_for_null(incident_record[34]))
                            .add("caller_last", support.string_utils.check_for_null(incident_record[35]))
                            .add("caller_phone_mobile", support.string_utils.check_for_null(incident_record[36]))
                            .add("caller_group_name", support.string_utils.check_for_null(incident_record[37]))
                            .add("assigned_group_name", support.string_utils.check_for_null(incident_record[38]))
                            .add("assigned_to_username", support.string_utils.check_for_null(incident_record[39]))
                            .add("assigned_to_first", support.string_utils.check_for_null(incident_record[40]))
                            .add("assigned_to_mi", support.string_utils.check_for_null(incident_record[41]))
                            .add("assigned_to_last", support.string_utils.check_for_null(incident_record[42]))
                            .add("assigned_to_phone_mobile", support.string_utils.check_for_null(incident_record[43]))
                            .add("created_by_username", support.string_utils.check_for_null(incident_record[44]))
                            .add("created_by_first", support.string_utils.check_for_null(incident_record[45]))
                            .add("created_by_mi", support.string_utils.check_for_null(incident_record[46]))
                            .add("created_by_last", support.string_utils.check_for_null(incident_record[47]))
                            .add("created_by_phone_mobile", support.string_utils.check_for_null(incident_record[48]))
                            .add("attachments", attachments_builder)
                        );
                        status_counters.merge(incident_status, 1, Integer::sum);
//                        map.put("key", );
//                        map.put("value", filter_value);
//                        filters.add(map);
                }
                json_builder.add("recordsTotal", incidents.size());
                json_builder.add("recordsFiltered", incidents.size());
                json_builder.add("draw", request.getParameter("draw"));
                
                JsonObjectBuilder status_counters_object_builder = Json.createObjectBuilder();
                status_counters.forEach(status_counters_object_builder::add);
                JsonObject status_counters_obj = status_counters_object_builder.build();
                json_builder.add("statusCounters", status_counters_obj);

                json_builder.add("data", json_array_builder);
                json = json_builder.build();

                con.close();
                //send the JSON data
                out.print(json.toString());
            }
            catch(Exception e)
            {
                e.printStackTrace();
                logger.error("ERROR Exception in servlet ajax_lookup_problem_incidents:=" + e);
                stringBuilder.setLength(0);
                stringBuilder.append("Error in retrieving incidents list");
                out.print(stringBuilder.toString());

            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

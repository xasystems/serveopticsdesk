/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.binary.Hex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class login_web extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        boolean can_login = true;
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String username = "";
        String password = "";
        String home_selection = "";
        String contract_id = "";
        String instance_name = "";
        String license = "";
        String login_url = props.get("login_url").toString();
        String RedirectURL = login_url + "?error=Unknown";
        boolean license_good = true;//license exist and is good     
        int session_timeout = 30;
        int seats = -1;
        int number_of_sessions = 0;
        boolean login_good = true; //check username and password
        boolean self_service_only = true;
        try
        {
            try
            {
                String q = request.getParameter("q");
                String q_decrypted = support.encrypt_utils.decrypt(context_dir, q);
                //get login parameters
                //username + "sEp,sEp" + password + "sEp,sEp" + home_selection + "sEp,sEp" + contract_id + "sEp,sEp" + license
                String q_split[] = q_decrypted.split("sEp,sEp");
                username = q_split[0];
                password = q_split[1];
                home_selection = q_split[2];  //service_desk  or analytics
                contract_id = q_split[3];
                license = q_split[4];
                instance_name = q_split[5];
                //get user specific config info
                logger.debug("DEBUG: Start login_web servlet:");   
                RedirectURL = login_url + "?error=Unknown";                
                logger.debug("2 DEBUG: Start login_web servlet:");   
            }
            catch(Exception e)
            {
                System.out.println("exception = " + e);
                RedirectURL = login_url + "?error=missing_parameter";
            }
            if(username == null || username.equalsIgnoreCase("") || password == null || password.equalsIgnoreCase("") || home_selection == null || home_selection.equalsIgnoreCase("")|| contract_id == null || contract_id.equalsIgnoreCase("") || license == null || license.equalsIgnoreCase("") || instance_name == null || instance_name.equalsIgnoreCase(""))
            {
                System.out.println("can_login = false");
                can_login = false;
                RedirectURL = login_url + "?error=missing_parameter";
            }
            else
            {
                System.out.println("can_login = false");
                session.setAttribute("db",contract_id);
                session.setAttribute("db_host",props.get("db_host").toString());
                session.setAttribute("db_port",props.get("db_port").toString());
                session.setAttribute("db_user",props.get("db_user").toString());
                session.setAttribute("db_password",props.get("db_password").toString());
                session.setAttribute("db_password_encrypted",props.get("db_password_encrypted").toString());
                session.setAttribute("debug",props.get("debug").toString());
                session.setAttribute("log_level",props.get("log_level").toString());
                session.setAttribute("timezone",props.get("timezone").toString());
                session.setAttribute("license",license);   
                session.setAttribute("instance_name",instance_name); 
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_info[] = db.get_users.by_email(con, username);
                String user_id = user_info[0];
                //get session_timeout
                try
                {
                    session_timeout = Integer.parseInt(db.get_configuration_properties.by_name(con, "session_timeout"));
                }
                catch(Exception e)
                {
                    session_timeout = 30;
                }
                number_of_sessions = db.get_user_sessions.active(con, session_timeout,user_id);
                System.out.println("Number of active sesssions=" + number_of_sessions);
                System.out.println("session_timeout=" + session_timeout);

                
                session.setAttribute("session_timeout",session_timeout);
                //DO LICENSE CHECK HERE             
                //get license count
                seats = support.util.validate_license(context_dir, license);
                if(seats > number_of_sessions)
                {
                    license_good = true;
                    ArrayList<byte[]> salt_and_hash_from_db = db.get_users.salt_and_hash_by_id(con, user_id);
//                    System.out.println("salt_and_hash_from_db: " + salt_and_hash_from_db);
                    byte[] salt_from_db = null;
                    byte[] hash_from_db = null;

                    if (salt_and_hash_from_db != null) 
                    {
                        salt_from_db = salt_and_hash_from_db.get(0);
                        hash_from_db = salt_and_hash_from_db.get(1);
                        if (salt_from_db == null || hash_from_db == null)
                        {
                            System.out.println("Old password encryption. Upgrading to salt/hash");
                            String unencrypted_password = support.encrypt_utils.decrypt(context_dir, user_info[2]);
                            if(unencrypted_password.equals(password))
                            {
                                ArrayList<byte[]> salt_and_hash = support.encrypt_utils.get_hash(unencrypted_password);
                                PreparedStatement stmt_update;
                                stmt_update = con.prepareStatement("UPDATE users SET password = '', password_salt = ?, password_hash = ?  WHERE id=?");
                                stmt_update.setBytes(1,salt_and_hash.get(0));
                                stmt_update.setBytes(2,salt_and_hash.get(1));
                                stmt_update.setString(3,user_id);
    //                            System.out.println("update query=" + stmt_update.toString());
                                stmt_update.executeUpdate();
                                salt_from_db = salt_and_hash.get(0);
                                hash_from_db = salt_and_hash.get(1);
                            }
                        }
                    } else {
                        System.out.println("Error in login_web: missing users password_salt and password_hash fields in DB" );
                        RedirectURL = login_url + "?error=Unknown";
                    }
                    byte[] computed_hash = null;
                    try
                    {
                        computed_hash = support.encrypt_utils.get_hash(password, salt_from_db);
                    } catch (Exception e)
                    {
                        System.out.println("Overall Exception in computing has in login_web=" + e);
                        RedirectURL = login_url + "?error=Unknown";
                    }
//                    System.out.println("password hashes: " + Hex.encodeHexString( computed_hash ) + "---" + Hex.encodeHexString( hash_from_db ));
                    if(Arrays.equals(computed_hash, hash_from_db))
                    {
                        //System.out.println("pasword is good");
                        login_good = true;
                        //login good
                        session.setAttribute("authenticated","true");
                        session.setAttribute("username",user_info[1]);
                        session.setAttribute("user_id",user_info[0]);
                        session.setAttribute("user_first",user_info[3]);
                        session.setAttribute("user_last",user_info[5]);
                        session.setAttribute("contract_id",contract_id);
//                        http://localhost:5984//avatars_1/727106ab-72c4-4ec7-950a-d3873b8663d2/avatar
                        if (user_info[24] != null && !user_info[24].equals(""))
                        {
                            session.setAttribute("avatar",props.get("couchdb.attachments_host") + "/" + props.get("couchdb.avatars_db") + "/" + user_info[24] + "/avatar");
                        }
                        //add up all the roles. highest permission is used from each role for each function
                        ArrayList <String> permission_values = new ArrayList();
                        permission_values.add("none");
                        permission_values.add("read");
                        permission_values.add("create");
                        permission_values.add("update");
                        permission_values.add("delete");

                        int role_level_value[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
                        //financial=0
                        //incident=1
                        //request=2
                        //contact=3
                        //cx=4
                        //survey=5
                        //sla=6
                        //project=7
                        //job=8
                        //metric=9
                        //asset=10
                        //user=11
                        //self_service=12
                        //problem=13
                        String admin = "false";
                        String manager = "false";
                        //get all users assigned roles
                        ArrayList <String[]> user_assigned_roles = db.get_roles.roles_for_user_id(con, user_id);
                        for(int a = 0; a < user_assigned_roles.size();a++)
                        {
                            String user_role[] = user_assigned_roles.get(a);
                            if(user_role[6].equalsIgnoreCase("true")) //admin
                            {
                                self_service_only = false;
                                admin = "true";
                                manager = "true";
                                role_level_value[0] = 4;
                                role_level_value[1] = 4;
                                role_level_value[2] = 4;
                                role_level_value[3] = 4;
                                role_level_value[4] = 4;
                                role_level_value[5] = 4;
                                role_level_value[6] = 4;
                                role_level_value[7] = 4;
                                role_level_value[8] = 4;
                                role_level_value[9] = 4;
                                role_level_value[10] = 4;
                                role_level_value[11] = 4;
                                role_level_value[12] = 4;
                                role_level_value[13] = 4;
                                a = user_assigned_roles.size();
                            }
                            else if(user_role[7].equalsIgnoreCase("true")) //manager
                            {
                                self_service_only = false;
                                manager = "true";
                                role_level_value[0] = 4;
                                role_level_value[1] = 4;
                                role_level_value[2] = 4;
                                role_level_value[3] = 4;
                                role_level_value[4] = 4;
                                role_level_value[5] = 4;
                                role_level_value[6] = 4;
                                role_level_value[7] = 4;
                                role_level_value[8] = 4;
                                role_level_value[9] = 4;
                                role_level_value[10] = 4;
                                role_level_value[11] = 4;
                                role_level_value[12] = 4;
                                role_level_value[13] = 4;
                                a = user_assigned_roles.size();
                            }
                            //financial/////////////////////////////////////////////////////////////////////
                            int financial_value = permission_values.indexOf(user_role[8]);
                            if(financial_value > 0)
                            {
                                self_service_only = false;
                            }
                            if(financial_value > role_level_value[0])
                            {
                                role_level_value[0] = financial_value;
                            }
                            //incident/////////////////////////////////////////////////////////////////////
                            int incident_value = permission_values.indexOf(user_role[9]);
                            if(incident_value > 0)
                            {
                                self_service_only = false;
                            }
                            if(incident_value > role_level_value[1])
                            {
                                role_level_value[1] = incident_value;
                            }
                            //problem/////////////////////////////////////////////////////////////////////
                            int problem_value = permission_values.indexOf(user_role[21]);
                            if(problem_value > 0)
                            {
                                self_service_only = false;
                            }
                            if(problem_value > role_level_value[13])
                            {
                                role_level_value[13] = problem_value;
                            }
                            //request/////////////////////////////////////////////////////////////////////
                            int request_value = permission_values.indexOf(user_role[10]);
                            if(request_value > 0)
                            {
                                self_service_only = false;
                            }
                            if(request_value > role_level_value[2])
                            {
                                role_level_value[2] = request_value;
                            }
                            //contact/////////////////////////////////////////////////////////////////////
                            int contact_value = permission_values.indexOf(user_role[11]);
                            if(contact_value > 0)
                            {
                                self_service_only = false;
                            }
                            if(contact_value > role_level_value[3])
                            {
                                role_level_value[3] = contact_value;
                            }
                            //cx/////////////////////////////////////////////////////////////////////
                            int cx_value = permission_values.indexOf(user_role[12]);
                            if(cx_value > 0)
                            {
                                self_service_only = false;
                            }
                            if(cx_value > role_level_value[4])
                            {
                                role_level_value[4] = cx_value;
                            }
                            //survey/////////////////////////////////////////////////////////////////////
                            int survey_value = permission_values.indexOf(user_role[13]);
                            if(survey_value > 0)
                            {
                                self_service_only = false;
                            }
                            if(survey_value > role_level_value[5])
                            {
                                role_level_value[5] = survey_value;
                            }
                            //sla/////////////////////////////////////////////////////////////////////
                            int sla_value = permission_values.indexOf(user_role[14]);
                            if(sla_value > 0)
                            {
                                self_service_only = false;
                            }
                            if(sla_value > role_level_value[6])
                            {
                                role_level_value[6] = sla_value;
                            }
                            //project/////////////////////////////////////////////////////////////////////
                            int project_value = permission_values.indexOf(user_role[15]);
                            if(project_value > 0)
                            {
                                self_service_only = false;
                            }
                            if(project_value > role_level_value[7])
                            {
                                role_level_value[7] = project_value;
                            }
                            //job/////////////////////////////////////////////////////////////////////
                            int job_value = permission_values.indexOf(user_role[16]);
                            if(job_value > 0)
                            {
                                self_service_only = false;
                            }
                            if(job_value > role_level_value[8])
                            {
                                role_level_value[8] = job_value;
                            }
                            //metric/////////////////////////////////////////////////////////////////////
                            int metric_value = permission_values.indexOf(user_role[17]);
                            if(metric_value > 0)
                            {
                                self_service_only = false;
                            }
                            if(metric_value > role_level_value[9])
                            {
                                role_level_value[9] = metric_value;
                            }
                            //asset/////////////////////////////////////////////////////////////////////
                            int asset_value = permission_values.indexOf(user_role[18]);
                            if(asset_value > 0)
                            {
                                self_service_only = false;
                            }
                            if(asset_value > role_level_value[10])
                            {
                                role_level_value[10] = asset_value;
                            }
                            //user/////////////////////////////////////////////////////////////////////
                            int user_value = permission_values.indexOf(user_role[19]);
                            if(user_value > 0)
                            {
                                self_service_only = false;
                            }
                            if(user_value > role_level_value[11])
                            {
                                role_level_value[11] = user_value;
                            }
                            //self_service/////////////////////////////////////////////////////////////////////
                            int self_service_value = permission_values.indexOf(user_role[20]);
                            if(self_service_value > role_level_value[12])
                            {
                                role_level_value[12] = self_service_value;
                            }
                        }
                        //set session vars       
                        session.setAttribute("home_page", "home_service_desk.jsp");
                        session.setAttribute("administration",admin);
                        session.setAttribute("manager",manager);
                        session.setAttribute("financial",permission_values.get(role_level_value[0]));
                        session.setAttribute("incident",permission_values.get(role_level_value[1]));
                        session.setAttribute("request",permission_values.get(role_level_value[2]));
                        session.setAttribute("contact",permission_values.get(role_level_value[3]));
                        session.setAttribute("cx",permission_values.get(role_level_value[4]));
                        session.setAttribute("survey",permission_values.get(role_level_value[5]));
                        session.setAttribute("sla",permission_values.get(role_level_value[6]));
                        session.setAttribute("project",permission_values.get(role_level_value[7]));
                        session.setAttribute("job",permission_values.get(role_level_value[8]));
                        session.setAttribute("metric",permission_values.get(role_level_value[9]));
                        session.setAttribute("asset",permission_values.get(role_level_value[10]));
                        session.setAttribute("user",permission_values.get(role_level_value[11]));
                        session.setAttribute("self_service",permission_values.get(role_level_value[12]));
                        session.setAttribute("problem",permission_values.get(role_level_value[13]));
                        if(self_service_only)
                        {
                            session.setAttribute("self_service_only","true");
                        }
                        else
                        {
                            session.setAttribute("self_service_only","false");
                        }
                        session.setAttribute("tz_name",user_info[21]);
                        session.setAttribute("tz_time",user_info[22]);
                    }
                    else
                    {
                        //System.out.println("pasword is NOT good");
                        //password is bad
                        login_good = false;
                    }
                }
                else
                {
                    license_good = false;
                }
            }
        }
        catch(Exception e)
        {
            System.out.println("Overall Exception in login_web=" + e);
            RedirectURL = login_url + "?error=Unknown";
        }
        //System.out.println("start redirecting self_service_only=" + self_service_only);
        if(login_good && license_good && self_service_only)
        {
            session.setAttribute("home_page", "home_self_service.jsp");
            RedirectURL = "home_self_service.jsp" ;
        }else if(login_good && license_good && home_selection.equalsIgnoreCase("service_desk"))
        {
            session.setAttribute("home_page", "home_service_desk.jsp");
            RedirectURL = "home_service_desk.jsp";
        }
        else if(login_good && license_good && home_selection.equalsIgnoreCase("analytics"))
        {
            session.setAttribute("home_page", "home_analytics.jsp");
            RedirectURL = "home_analytics.jsp";
        }           
        else if(!login_good)
        {
            login_url = props.get("login_url").toString();
            RedirectURL = login_url + "?error=Password";
        }
        else if(!license_good)
        {
            RedirectURL = "license_issue.jsp" ;
        }
        response.sendRedirect(RedirectURL);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author rcampbell
 */
public class admin_send_email extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
    

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            String send_email_host = request.getParameter("send_email_host");  
            String send_email_smtp_port = request.getParameter("send_email_smtp_port");  
            String send_email_encryption = request.getParameter("send_email_encryption");  
            String send_email_user = request.getParameter("send_email_user");  
            String send_email_password = request.getParameter("send_email_password");              
            
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement("REPLACE INTO configuration_properties VALUES (?,?)");
                stmt.setString(1,"send_email_host");
                stmt.setString(2,send_email_host);
                stmt.executeUpdate(); 
                stmt.setString(1,"send_email_smtp_port");
                stmt.setString(2,send_email_smtp_port);
                stmt.executeUpdate(); 
                stmt.setString(1,"send_email_encryption");
                stmt.setString(2,send_email_encryption);
                stmt.executeUpdate(); 
                stmt.setString(1,"send_email_user");
                stmt.setString(2,send_email_user);
                stmt.executeUpdate(); 
                stmt.setString(1,"send_email_password");                
                String encrypted_password = support.encrypt_utils.encrypt(context_dir,send_email_password);
                stmt.setString(2,encrypted_password);
                stmt.executeUpdate(); 
                
                
                stmt.close();
                con.close();
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_send_email:=" + e);
            }
            response.sendRedirect("admin_email.jsp?success=true");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

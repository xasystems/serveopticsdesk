/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class ajax_lookup_user_last_first extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    protected String check_for_null(String value)
    {
        String return_string = "";
        if(value == null || value.equalsIgnoreCase("null"))
        {
            return_string = "";
        }
        else 
        {
            return_string = value;
        }        
        return return_string;
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
        String search = request.getParameter("search");
        String search_parts[] = search.split(" ");
        String last = "";
        String first = "";
        if(search_parts.length != 2)
        {
            last = search_parts[0];
            first = "";
        }
        else if(search_parts.length == 2)
        {
            last = search_parts[0];
            first = search_parts[1];
        }
        
        response.setContentType("text/html;charset=UTF-8");
        PreparedStatement stmt;
        boolean first_record = true;
        boolean has_data = false;
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                stringBuilder.append("[");
                stmt = con.prepareStatement("SELECT * FROM users WHERE last LIKE '%" + last + "%' AND first LIKE '%" + first + "%' LIMIT 10");
                ResultSet rs = stmt.executeQuery();
                while(rs.next())
                {
                    has_data = true;                    
                    if(first_record)
                    {
                        stringBuilder.append("{");
                        stringBuilder.append("\"value\": \"" + check_for_null(rs.getString("id")) + "\",");
                        stringBuilder.append("\"label\": \"" + check_for_null(rs.getString("username")) + "\",");
                        stringBuilder.append("\"first\": \"" + check_for_null(rs.getString("first")) + "\",");
                        stringBuilder.append("\"last\": \"" + check_for_null(rs.getString("last")) + "\"");                        
                        stringBuilder.append("}");
                    }
                    else
                    {
                        stringBuilder.append(",{");
                        stringBuilder.append("\"value\": \"" + check_for_null(rs.getString("id")) + "\",");
                        stringBuilder.append("\"label\": \"" + check_for_null(rs.getString("username")) + "\",");
                        stringBuilder.append("\"first\": \"" + check_for_null(rs.getString("first")) + "\",");
                        stringBuilder.append("\"last\": \"" + check_for_null(rs.getString("last")) + "\"");
                        stringBuilder.append("}");
                    }
                    
                    
                    first_record = false;
                }
                stringBuilder.append("]");                
                if(!has_data)
                {
                    stringBuilder.setLength(0);
                    stringBuilder.append("[{\"value\":\"\",\"label\":\"\",\"first\":\"\",\"last\":\"\"}]");
                }                
                stmt.close();
                con.close();
                //send the JSON data
                out.print(stringBuilder.toString());
            }
            catch(Exception e)
            {
                logger.error("ERROR Exception in servlet ajax_lookup_user_last_first:=" + e);
            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

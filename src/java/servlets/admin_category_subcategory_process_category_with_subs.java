/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.joining;
import java.util.stream.Stream;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 *
 * @author Ralph
 */
public class admin_category_subcategory_process_category_with_subs extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String query = "";
        try
        {
            HttpSession session = request.getSession();
            String context_dir = request.getServletContext().getRealPath("");
            
            if (session.getAttribute("authenticated") == null) 
            {
                response.sendRedirect("index.jsp");
            }
            else
            {
                //do work here
                String category_id = request.getParameter("category_id");

                String category_active = "false";
                String category_name = request.getParameter("category_name");
                try
                {
                    category_active = request.getParameter("category_active");
                    if(category_active == null)
                    {
                        category_active = "false";
                    }
                    else
                    {
                        category_active = "true";
                    }
                }
                catch(Exception e)
                {
                    category_active = "false";
                }
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt;

                if ("".equals(category_id)) 
                {                    
                    query = "INSERT INTO category (name,active) VALUES (?,?)";
                    stmt = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                    stmt.setString(1,category_name);
                    stmt.setString(2,category_active);
                    int affectedRows = stmt.executeUpdate();

                    if (affectedRows == 0) {
                        throw new SQLException("Creating category failed");
                    }
                    //stmt.close();

                    try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                        if (generatedKeys.next()) {
                            category_id = generatedKeys.getString(1);
                        }
                        else {
                            throw new SQLException("Creating category failed, no ID obtained.");
                        }
                    }
                } else {
                    query = "UPDATE `category` SET name=?,active=? WHERE `id`=?";
                    stmt = con.prepareStatement(query);
                    stmt.setString(1,category_name);
                    stmt.setString(2,category_active);
                    stmt.setString(3,category_id);
                    //System.out.println("stmt=" + stmt);
                    stmt.executeUpdate();
                }

//                logger.info("category id " +category_id);
                // processing subcategories

                ArrayList<String> subcats_ids = new ArrayList();
                ArrayList<String> subcats_actives = new ArrayList();
                ArrayList<String> subcats_names = new ArrayList();
                
                for(String name : request.getParameterMap().keySet())
                {
                    for(String value : request.getParameterValues(name))
                    {
                        if ("subcategory_id[]".equals(name))
                        {
                            subcats_ids.add(value);
                        }
                        if ("subcategory_active[]".equals(name))
                        {
                            subcats_actives.add(value);
                        }
                        if ("subcategory_name[]".equals(name))
                        {
                            subcats_names.add(value);
                        }
                    }
                }
//                logger.info("subcats count" +subcats_names.size());
                
                ArrayList<String> ids_to_keep = (ArrayList<String>)subcats_ids.clone();
                ids_to_keep.removeIf(item -> item == null || "".equals(item));
                if (ids_to_keep.size() > 0) 
                {
                    // deleting subcats that are absent in request
                    query = "DELETE FROM subcategory WHERE category_id=? AND NOT id IN (";
                    
                    StringBuilder parameterBuilder = new StringBuilder();
                    
                    for( int i = 0 ; i < ids_to_keep.size(); i++ ) {
                        parameterBuilder = i < (ids_to_keep.size() - 1) ? parameterBuilder.append("?,") : parameterBuilder.append("?");
                    }
                    
                    parameterBuilder.append(")");

                    stmt = con.prepareStatement(query + parameterBuilder);
                    stmt.setString(1,category_id);
                    
                    int i = 2;
                    for (String field : ids_to_keep)
                    {
                        stmt.setString(i++, field);
                    }
                    System.out.println("stmt=" + stmt);
                    stmt.executeUpdate();   
                } else {
                    // deleting all subcats
                    query = "DELETE FROM subcategory WHERE category_id=?";
                    stmt = con.prepareStatement(query);
                    stmt.setString(1,category_id);
                    stmt.executeUpdate();   
                }
                
                // CREATE OR UPDATE
                for (int i = 0; i < subcats_ids.size(); i++)
                {
                    String subcat_id = subcats_ids.get(i);
                    if ("".equals(subcat_id)) {
                        query = "INSERT INTO subcategory (name,active,category_id) VALUES (?,?,?)";
                        stmt = con.prepareStatement(query);
                        stmt.setString(1,subcats_names.get(i));
                        stmt.setString(2,subcats_actives.get(i));
                        stmt.setString(3,category_id);
                        System.out.println("stmt=" + stmt);
                        stmt.executeUpdate();   
                    } else {
                        query = "UPDATE subcategory SET name=?,active=?,category_id=? WHERE id = ?";
                        stmt = con.prepareStatement(query);
                        stmt.setString(1,subcats_names.get(i));
                        stmt.setString(2,subcats_actives.get(i));
                        stmt.setString(3,category_id);
                        stmt.setString(4,subcat_id);
                        System.out.println("stmt=" + stmt);
                        stmt.executeUpdate();                           
                    }
                }
                con.close();
            }            
        }
        catch (Exception e) 
        {
            logger.error("ERROR Exception in servlet admin_category_subcategory_add_category_with_subs:=" + e);
            e.printStackTrace();
        }
        response.sendRedirect("admin_category_subcategory.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

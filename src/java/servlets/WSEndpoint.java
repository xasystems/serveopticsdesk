/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import javax.ejb.Singleton;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.CloseReason;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author user
 */
@Singleton
@ServerEndpoint(value = "/wsendpoint", configurator = ServletAwareConfig.class)
public class WSEndpoint
{
    private static final Set<Session> peers = Collections.synchronizedSet(new HashSet<Session>());
    private Map<String, List<String>> params;
    private static Logger logger = LogManager.getLogger();
    private final Tags<Session, String> tags = new Tags<>();
    private EndpointConfig config;

    @OnOpen
    public void onOpen (Session session, EndpointConfig config) throws IOException {
        this.config = config;
        HttpSession httpSession = (HttpSession) config.getUserProperties().get("httpSession");
        logger.info("Session ID: {} opened authenticated: {} ", session.getId(), httpSession.getAttribute("authenticated"));

        if (httpSession.getAttribute("authenticated")==null)
        {
            session.close(new CloseReason(CloseReason.CloseCodes.VIOLATED_POLICY,"Unauthorized")); 
        }

//        params = session.getRequestParameterMap();
//        if (params.get("authenticated") != null && params.get("authenticated").get(0).equals("true")) {
//            tags.add(session, session.getId());
//        }

    }

    @OnMessage
    public void onMessage(String message, Session sessionFrom) throws IOException
    {
        HttpSession httpSession = (HttpSession) config.getUserProperties().get("httpSession");
        ServletContext servletContext = httpSession.getServletContext();
        String context_dir = servletContext.getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        logger.info("Message received from session ID: {}, message: {}, authenticated: {}, user_id: {}", sessionFrom.getId(), message, httpSession.getAttribute("authenticated"), httpSession.getAttribute("user_id"));

        try
        {
            Connection con = db.db_util.get_contract_connection(context_dir, httpSession);
            String authenticated = httpSession.getAttribute("authenticated").toString();
            String user_id = httpSession.getAttribute("user_id").toString();

            if(authenticated != null && user_id != null)
            {
                int session_timeout = 30;
                try
                {
                    session_timeout = Integer.parseInt(db.get_configuration_properties.by_name(con, "session_timeout"));
                }
                catch(Exception e)
                {
                    session_timeout = 30;
                }

                int number_of_active_sessions = 0;
                try
                {
                    number_of_active_sessions = db.get_user_sessions.active_for_user_id(con, session_timeout, user_id);
                } catch (SQLException ex)
                {
                    logger.error("Error getting number of active sessions by user_id in websocket.onMessage");
                }
                if (number_of_active_sessions > 0)
                {
                    try (JsonReader jr = Json.createReader(new StringReader(message))) {
                        JsonObject jo = jr.readObject();
                        String toTag = jo.getString("toTag", null);
                        String fromUser = jo.getString("sender_id", null);
                        String content = jo.getString("content", null);
                        String addTag = jo.getString("addTag", null);
                        String removeTag = jo.getString("removeTag", null);
                        if (toTag != null) {
                            sendMessage(fromUser, toTag, content);
                        }
                        if (addTag != null) {
                            logger.error("websocket.onMessage user {} subscribed to {}", sessionFrom.getId(), addTag);
                            addTag(sessionFrom, addTag);
                            tags.getObjectsWith(addTag).parallelStream().forEach(sessionTo -> logger.info("added tag to session {}", sessionTo.getId()));
                        }
                        if (removeTag != null) {
                            removeTag(sessionFrom, removeTag);
                        }
                    }

                } else {
                    logger.error("No active sessions by user_id in websocket.onMessage. Closing");
                    sessionFrom.close(new CloseReason(CloseReason.CloseCodes.VIOLATED_POLICY,"Unauthorized")); 
                }
            } else {
                logger.error("Authenticated == null or user_id == null in websocket.onMessage. Closing");
                sessionFrom.close(new CloseReason(CloseReason.CloseCodes.VIOLATED_POLICY,"Unauthorized")); 
            }
            
        } catch (Exception e)
        {
            logger.error("Error in websocket.onMessage {}", e.getMessage());
            sessionFrom.close(new CloseReason(CloseReason.CloseCodes.VIOLATED_POLICY,"Unauthorized")); 
        }
//        params = sessionFrom.getRequestParameterMap();
//        if (params.get("authenticated") != null && params.get("authenticated").get(0).equals("true")) {
//        } else {
//            sessionFrom.close();
//        }
    }

    private void sendMessage(String fromUser, String toTag, String content) {
        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
        objectBuilder.add("toTag", toTag);
//        objectBuilder.add("fromUser", fromUser);
//        objectBuilder.add("content", content);
        String message = objectBuilder.build().toString();
        tags.getObjectsWith(toTag).parallelStream().forEach(sessionTo -> sessionTo.getAsyncRemote().sendText(message));
        tags.getObjectsWith(toTag).parallelStream().forEach(sessionTo -> logger.info("Message sent to session {}", sessionTo.getId()));
        
    }
    
    private void addTag(Session session, String tag) {
        tags.add(session, tag);
        logger.info("Session ID: {} received tag {}", session.getId(), tag);
    }
    
    private void removeTag(Session session, String tag) {
        tags.removeTagFrom(session, tag);
        logger.info("Session ID: {} removed tag {}", session.getId(), tag);
    }

    @OnClose
    public void onClose (Session session) {
        logger.info("Session closed ID: {}", session.getId());
        tags.removeObject(session);
    }    

    @OnError
    public void onError(Session session, Throwable throwable) {
        logger.error("Session error. Removing session ID: " + session.getId(), throwable);
        try {
            session.close();
        } catch (IOException ex) {
            logger.warn("Error closing session ID: " + session.getId(), ex);
        }
    }

}

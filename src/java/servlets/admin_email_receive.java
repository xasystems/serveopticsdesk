/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author rcampbell
 */
public class admin_email_receive extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();    

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            ArrayList<String[]>values = new ArrayList<String[]>();
            
            String temp[] = new String[2];
            String get_email_host = "";
            try{get_email_host = support.data_check.null_string(request.getParameter("get_email_host"));}catch(Exception e){get_email_host = "";}
            temp[0] = "get_email_host";
            temp[1] = get_email_host;
            values.add(temp);
            
            String get_email_user = "";
            try{get_email_user = support.data_check.null_string(request.getParameter("get_email_user"));}catch(Exception e){get_email_user = "";}
            temp = new String[2];
            temp[0] = "get_email_user";
            temp[1] = get_email_user;
            values.add(temp);
            
            String get_email_password = ""; 
            try{get_email_password = support.data_check.null_string(request.getParameter("get_email_password"));}catch(Exception e){get_email_password = "";}
            get_email_password = support.encrypt_utils.encrypt(context_dir, get_email_password);
            temp = new String[2];
            temp[0] = "get_email_password";
            temp[1] = get_email_password;
            values.add(temp);
            
            String get_mail_store_protocol = "";  
            try{get_mail_store_protocol = support.data_check.null_string(request.getParameter("get_email_store_protocol"));}catch(Exception e){get_mail_store_protocol = "";}
            temp = new String[2];
            temp[0] = "get_email_store_protocol";
            temp[1] = get_mail_store_protocol;
            values.add(temp);
            
            String mail_pop3_port = ""; 
            try{mail_pop3_port = support.data_check.null_string(request.getParameter("mail_pop3_port"));}catch(Exception e){mail_pop3_port = "";}
            temp = new String[2];
            temp[0] = "mail.pop3.port";
            temp[1] = mail_pop3_port;
            values.add(temp);
            String get_email_enable = "false"; 
            try
            {
                if(request.getParameter("get_email_enable") == null)
                {
                    get_email_enable = "false";
                }
                else
                {
                    get_email_enable = "true";
                }
            }
            catch(Exception e)
            {
                get_email_enable = "false";
            }
            temp = new String[2];
            temp[0] = "get_email_enable";
            temp[1] = get_email_enable;
            values.add(temp);
            
            String mail_pop3_starttls_enable = "false"; 
            try
            {
                if(request.getParameter("mail_pop3_starttls_enable") == null)
                {
                    mail_pop3_starttls_enable = "false";
                }
                else
                {
                    mail_pop3_starttls_enable = "true";
                }
            }
            catch(Exception e)
            {
                mail_pop3_starttls_enable = "false";
            }
            temp = new String[2];
            temp[0] = "mail.pop3.starttls.enable";
            temp[1] = mail_pop3_starttls_enable;
            values.add(temp);
            
            String mail_pop3_ssl_enable = "false"; 
            try
            {
                if(request.getParameter("mail_pop3_ssl_enable") == null)
                {
                    mail_pop3_ssl_enable = "false";
                }
                else
                {
                    mail_pop3_ssl_enable = "true";
                }
            }
            catch(Exception e)
            {
                mail_pop3_ssl_enable = "false";
            }            
            temp = new String[2];
            temp[0] = "mail.pop3.ssl.enable";
            temp[1] = mail_pop3_ssl_enable;
            values.add(temp);
            
            String mail_pop3_socketFactory_fallback = "false"; 
            try
            {
                if(request.getParameter("mail_pop3_socketFactory_fallback") == null)
                {
                    mail_pop3_socketFactory_fallback = "false";
                }
                else
                {
                    mail_pop3_socketFactory_fallback = "true";
                }
            }
            catch(Exception e)
            {
                mail_pop3_socketFactory_fallback = "false";
            }   
            temp = new String[2];
            temp[0] = "mail.pop3.socketFactory.fallback";
            temp[1] = mail_pop3_socketFactory_fallback;
            values.add(temp);
            
            
            String mail_imap_port = request.getParameter("mail_imap_port"); 
            String mail_imap_starttls_enable = "false"; 
            try
            {
                if(request.getParameter("mail_imap_starttls_enable") == null)
                {
                    mail_imap_starttls_enable = "false";
                }
                else
                {
                    mail_imap_starttls_enable = "true";
                }
            }
            catch(Exception e)
            {
                mail_imap_starttls_enable = "false";
            }   
            temp = new String[2];
            temp[0] = "mail.imap.starttls.enable";
            temp[1] = mail_imap_starttls_enable;
            values.add(temp);
            
            
            String mail_imap_ssl_enable = "false"; 
            try
            {
                if(request.getParameter("mail_imap_ssl_enable") == null)
                {
                    mail_imap_ssl_enable = "false";
                }
                else
                {
                    mail_imap_ssl_enable = "true";
                }
            }
            catch(Exception e)
            {
                mail_imap_ssl_enable = "false";
            }  
            temp = new String[2];
            temp[0] = "mail.imap.ssl.enable";
            temp[1] = mail_imap_ssl_enable;
            values.add(temp);
            
            String mail_imap_socketFactory_fallback = "false";    
            try
            {
                if(request.getParameter("mail_imap_socketFactory_fallback") == null)
                {
                    mail_imap_socketFactory_fallback = "false";
                }
                else
                {
                    mail_imap_socketFactory_fallback = "true";
                }
            }
            catch(Exception e)
            {
                mail_imap_socketFactory_fallback = "false";
            }                                                 
            temp = new String[2];
            temp[0] = "mail.imap.socketFactory.fallback";
            temp[1] = mail_imap_socketFactory_fallback;
            values.add(temp);
            
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement("REPLACE INTO configuration_properties VALUES (?,?)");
                for(int a = 0; a < values.size(); a++)
                {
                    String v[] = values.get(a);
                    stmt.setString(1,v[0]);
                    stmt.setString(2,v[1]);
                    stmt.executeUpdate(); 
                }
                stmt.close();
                con.close();
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_email_receive:=" + e);
            }
            response.sendRedirect("admin_email.jsp?success=true");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

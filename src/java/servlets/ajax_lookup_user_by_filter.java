/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class ajax_lookup_user_by_filter extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    protected String check_for_null(String value)
    {
        String return_string = "";
        if(value == null || value.equalsIgnoreCase("null"))
        {
            return_string = "";
        }
        else 
        {
            return_string = value;
        }        
        return return_string;
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
        String search = request.getParameter("search");
        String request_field = request.getParameter("field");
        String sql_field;
        
        if (request_field != null) 
        {
            switch (request_field) 
            {
                case "first":
                    sql_field = "first";
                    break;
                case "last":
                    sql_field = "last";
                    break;
                case "company":
                    sql_field = "company";
                    break;
                case "department":
                    sql_field = "department";
                    break;
                default:
                    sql_field = "username";
                    break;
            }            
        } else {
            sql_field = "username";
        }
        
        response.setContentType("text/html;charset=UTF-8");
        PreparedStatement stmt;
        boolean first_record = true;
        boolean has_data = false;
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {
                //System.out.println("search=" + search);
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                stringBuilder.append("[");
                stmt = con.prepareStatement("SELECT DISTINCT (" + sql_field + ") FROM users WHERE " + sql_field + "  like ? ORDER BY " + sql_field + " LIMIT 10 ");
                stmt.setString(1, "%" + search + "%");
                System.out.println("query=" + stmt.toString());
                ResultSet rs = stmt.executeQuery();
                while(rs.next())
                {
                    has_data = true;
                    
                    if(first_record)
                    {
                        stringBuilder.append("{");
                        stringBuilder.append("\"label\": \"" + check_for_null(rs.getString(sql_field)) + "\"");
                        stringBuilder.append("}");
                    }
                    else
                    {
                        stringBuilder.append(",{");
                        stringBuilder.append("\"label\": \"" + check_for_null(rs.getString(sql_field)) + "\"");
                        stringBuilder.append("}");
                    }
                    first_record = false;
                }
                stringBuilder.append("]");                
                if(!has_data)
                {
                    stringBuilder.setLength(0);
                    stringBuilder.append("[{\"value\":\"\",\"label\":\"\",\"vip\":\"\"}]");
                }                
                stmt.close();
                con.close();
                //send the JSON data
                out.print(stringBuilder.toString());
            }
            catch(Exception e)
            {
                logger.error("ERROR Exception in servlet ajax_lookup_user:=" + e);
            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

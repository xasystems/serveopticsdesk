/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class admin_knowledge_base_article_delete extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String url = "admin_knowledge_base.jsp";
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            try 
            {
                String id = request.getParameter("id");
                String source = request.getParameter("source");
                if(source.equalsIgnoreCase("search"))
                {
                    url = "kb_search.jsp";
                }
                else
                {
                    url = "admin_knowledge_base.jsp";
                }
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                //delete the article
                PreparedStatement stmt = con.prepareStatement("DELETE FROM knowledge_articles WHERE id=?");
                stmt.setString(1,id);
                System.out.println("stmt=" + stmt.toString());
                stmt.executeUpdate();
                //delete usage 
                stmt = con.prepareStatement("DELETE FROM knowledge_article_usages WHERE article_id=?");
                stmt.setString(1,id);
                stmt.executeUpdate();
                //delete feedback 
                stmt = con.prepareStatement("DELETE FROM knowledge_article_feedback WHERE article_id=?");
                stmt.setString(1,id);
                stmt.executeUpdate();
                
                
                stmt.close();
                con.close();
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_knowledge_base_article_delete:=" + e);
            }
        }
        response.sendRedirect(url);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

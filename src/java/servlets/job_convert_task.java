/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class job_convert_task extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
    private static SimpleDateFormat form_format = new SimpleDateFormat("HH:mm / dd-MMM-YY"); //10:42 / 24-Jan-20
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            String user_tz_name = "US/Eastern";
            String user_tz_time = "-05:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "US/Eastern";
                    user_tz_time = "-05:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "US/Eastern";
                user_tz_time = "-05:00";
            }
            String project_id = request.getParameter("project_id");
            String task_id = request.getParameter("task_id");
            
            java.util.Date now = new java.util.Date();
            java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
            
            //id
            int id = 0;
            //name
            String name = "";
            try{name = request.getParameter("name");}catch (Exception e){name="";}
            //assigned_group_id
            int assigned_group_id = 0;
            try{assigned_group_id = Integer.parseInt(request.getParameter("assigned_group_id"));}catch (Exception e){assigned_group_id=0;}
            //assigned_to_id
            int assigned_to_id = 0;
            try{assigned_to_id = Integer.parseInt(request.getParameter("assigned_to_id"));}catch (Exception e){assigned_to_id=0;}
            //description
            String description = "";
            try{description = request.getParameter("description");}catch (Exception e){description="";}
           
            //scheduled_start_date
            String scheduled_start_date = null;
            try
            {
                String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("scheduled_start_date")).getTime()));
                ZonedDateTime time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                scheduled_start_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(time_utc);
            }
            catch(Exception e)
            {
                scheduled_start_date = null;
            }
            //actual_start_date
            String actual_start_date = null;
            try
            {
                String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("actual_start_date")).getTime()));
                ZonedDateTime time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                actual_start_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(time_utc);
            }
            catch(Exception e)
            {
                actual_start_date = null;
            }
            
            //scheduled_end_date
            String scheduled_end_date = null;
            try
            {
                String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("scheduled_end_date")).getTime()));
                ZonedDateTime time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                scheduled_end_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(time_utc);
            }
            catch(Exception e)
            {
                scheduled_end_date = null;
            }
            //actual_end_date
            String actual_end_date = null;
            try
            {
                String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("actual_end_date")).getTime()));
                ZonedDateTime time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                actual_end_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(time_utc);
            }
            catch(Exception e)
            {
                actual_end_date = null;
            }
            //estimated_duration
            int estimated_duration = 0;
            try{estimated_duration = Integer.parseInt(request.getParameter("estimated_duration"));}catch (Exception e){estimated_duration=0;}
            //actual_duration
            int actual_duration = 0;
            try{actual_duration = Integer.parseInt(request.getParameter("actual_duration"));}catch (Exception e){actual_duration=0;}            
            
            //priority
            String priority = "";
            try{priority = request.getParameter("priority");}catch (Exception e){priority="";}
            //status
            String status = "";
            try{status = request.getParameter("status");}catch (Exception e){status="";}
            //notes
            String notes = "";
            try{notes = request.getParameter("notes");}catch (Exception e){notes="";}
            //do work here
            try
            {                
                String context_dir = request.getServletContext().getRealPath("");
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                //get all the parameters
                id = db.get_jobs.next_job_id(con);
                
                String insert_projects_table = "INSERT INTO jobs (id,name,description,assigned_group_id,assigned_to_id,priority,status,scheduled_start_date,actual_start_date,scheduled_end_date,actual_end_date,estimated_duration,actual_duration,notes) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                PreparedStatement stmt = con.prepareStatement(insert_projects_table);
                stmt.setInt(1, id); //id
                stmt.setString(2,name); //name
                stmt.setString(3,description);//description
                stmt.setInt(4,assigned_group_id);//owner_group_id
                stmt.setInt(5,assigned_to_id);//owner_id
                stmt.setString(6,priority); //priority
                stmt.setString(7,status); //status
                stmt.setString(8, scheduled_start_date); //scheduled_start_date
                stmt.setString(9, actual_start_date); //actual_start_date
                stmt.setString(10, scheduled_end_date); //scheduled_end_date
                stmt.setString(11, actual_end_date); //actual_end_date
                stmt.setInt(12,estimated_duration);//estimated_duration
                stmt.setInt(13,actual_duration);//actual_duration       
                stmt.setString(14,notes); //notes                
                stmt.execute();                
                
                //delete the old task db entry
                String query = "DELETE FROM project_tasks WHERE id=? AND project_id=?";
                stmt = con.prepareStatement(query);
                stmt.setString(1, task_id); 
                stmt.setString(2,project_id); 
                stmt.execute();
                String RedirectURL = "job.jsp?job_id=" + id ;
                response.sendRedirect(RedirectURL);
            }
            catch(Exception e)
            {
                logger.error("ERROR Exception in servlet job_convert_task:=" + e);
            }  
        }
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

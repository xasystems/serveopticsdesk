/*
<!--Copyright 2020 Real Data Technologies, Inc. , All rights reserved.-->
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.Tika;
import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;

@MultipartConfig()
/**
 *
 * @author ralph
 */
public class admin_service_catalog_add extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            try 
            {
                String name = request.getParameter("name");
                String description = request.getParameter("description");
                String state = request.getParameter("state");
                String catalog_type = request.getParameter("catalog_type");
                String icon = request.getParameter("icon");
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement("INSERT INTO service_catalog (name,description,state,catalog_type,icon) VALUES (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);

                stmt.setString(1,name);
                stmt.setString(2,description);
                stmt.setString(3,state);
                stmt.setString(4,catalog_type);
                stmt.setString(5,icon);

                int affectedRows = stmt.executeUpdate();
                String id = "";
                if (affectedRows == 0) {
                    throw new SQLException("Creating catalog failed");
                }
                //stmt.close();

                try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        id = generatedKeys.getString(1);
                    }
                    else {
                        throw new SQLException("Creating catalog failed, no ID obtained.");
                    }
                }

                CouchDbClient dbClient = null;
                LinkedHashMap props = support.config.get_config(context_dir);

                //start get the file attachment if there is one
                try
                {
                    Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
                    if (filePart != null)
                    {
                        try
                        {
                            dbClient = db.couchdb.get_client(props);
                            if (dbClient == null)
                            {
                                throw new Exception("dbClient is null");
                            }
                            String file_name;
                            Tika tika = new Tika();

                            String fileName = filePart.getSubmittedFileName();
                            logger.info("filename:" + fileName);
                            logger.info("content-type:" + filePart.getContentType());
                            logger.info("content-size:" + filePart.getSize());
//                            if (fileName != null && !"".equals(fileName)) 
                            long file_size = filePart.getSize();
                            if (file_size > 0) 
                            {
                                String mimeType;
                                String request_mime_type = filePart.getContentType();
                                if (request_mime_type.equals("image/svg+xml")) {
                                    mimeType = request_mime_type;
                                } else {
                                    mimeType = tika.detect(filePart.getInputStream());
                                }
                                file_name = "image";

                                String uuid;
                                String rev;
                                int new_uuid = 0;
                                Response dbclient_response;

                                UUID Uuid = UUID.randomUUID(); //Generates random UUID                                  
                                
                                String user_id = session.getAttribute("user_id").toString();
                                java.util.Date now = new java.util.Date();

                                Map<String, Object> map = new HashMap<>(); 
                                map.put("Filename", file_name);
                                map.put("MIME_type", mimeType);
                                map.put("FileSize", file_size);
                                map.put("DateUploaded", now);
                                map.put("UploadedBy", user_id);
                                map.put("AssociatedObject", "Service Catalog");
                                map.put("AssociatedObjectId", id);

                                uuid = Uuid.toString();
                                map.put("_id", uuid);
                                dbclient_response = dbClient.save(map);
                                rev = dbclient_response.getRev();

                                dbclient_response = dbClient.saveAttachment(filePart.getInputStream(), file_name, mimeType, uuid, rev);

                                if (dbclient_response.getError() != null)
                                {
                                    throw new Exception("CouchDB attachment upload error: " + dbclient_response.getError());
                                }

                                String query_image = "UPDATE service_catalog SET image = ? WHERE id = ?";
                                PreparedStatement stmt_attachment = con.prepareStatement(query_image);
                                stmt_attachment.setString(1, uuid);
                                stmt_attachment.setString(2, id);
                                stmt_attachment.execute();

                                int rows_affected = stmt_attachment.executeUpdate();
                                if (rows_affected <= 0)
                                {
                                    throw new Exception("Image field is somehow not updated in the service_catalog_table");
                                }
                            }
                        } catch (Exception e)
                        {
                            logger.error("ERROR attachments process in servlet admin_service_catalog_add:=" + e);
                        }

                    }
                }
                catch(Exception e)
                {
                    logger.error("ERROR Exception image saving in servlet admin_service_catalog_add:=" + e);
                }                


                stmt.close();
                con.close();
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_service_catalog_add:=" + e);
            }
            response.sendRedirect("admin_service_catalog.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

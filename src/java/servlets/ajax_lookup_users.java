/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonString;

/**
 *
 * @author Ralph
 */
public class ajax_lookup_users extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    protected String check_for_null(String value)
    {
        String return_string = "";
        if(value == null || value.equalsIgnoreCase("null"))
        {
            return_string = "";
        }
        else 
        {
            return_string = value;
        }        
        return return_string;
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
//        String user_id = request.getParameter("user_id");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

//        String user_id = request.getParameter("user_id");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        JsonObjectBuilder json_builder = Json.createObjectBuilder();
        JsonObject json;
        
        boolean first_record = true;
        boolean has_data = false;
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {

                if (session.getAttribute("authenticated") == null) 
                {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }
                //System.out.println("search=" + search);
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                int starting_record = 0;
                int previous_record = 0;
                int next_record = 0;
                int fetch_size = 50;
                
                try
                {
                    starting_record  = Integer.parseInt(request.getParameter("start"));
                    next_record = starting_record + fetch_size;
                    previous_record = starting_record - fetch_size;
                    if(previous_record <= 0)
                    {
                        previous_record = 0;
                    }
                }
                catch(Exception e)
                {
                    starting_record = 0;
                    previous_record = 0;
                    next_record = starting_record + fetch_size;
                }

                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
//                String all_users_info[][] = db.get_users.all_limited(con,starting_record,fetch_size);
                
                String filter_username = request.getParameter("filter_username");
                String filter_firstname = request.getParameter("filter_firstname");
                String filter_lastname = request.getParameter("filter_lastname");
                String filter_company = request.getParameter("filter_company");
                String filter_department = request.getParameter("filter_department");
                
                
                List<Map<String, String>> filters = new ArrayList<>();
                int role_id = -1;

                try
                {
                    String[] filter_fields = {
                             "username",
                             "first",
                             "last",
                             "role",
                             "company",
                             "department"
                    };

                    String request_filters = request.getParameter("filters"); //assigned_to_me,unassigned, created_today, closed_today, open, open_7_14, open_14_30, open_30_plus 
                    if(request_filters != null)
                    {
                        JsonReader reader = Json.createReader(new StringReader(request_filters));

                        JsonObject jsonObject = reader.readObject();
                        Iterator it = jsonObject.entrySet().iterator();

                        while (it.hasNext()) {
                            JsonObject.Entry mapEntry = (JsonObject.Entry)it.next();
//                            filters.put(mapEntry.getKey().toString(), ((JsonString)mapEntry.getValue()).getString());
                            String filter_key = mapEntry.getKey().toString();
                            if (filter_key.equals("role"))
                            {
                                role_id = Integer.parseInt(((JsonString)mapEntry.getValue()).getString());
                            } else {
                                if (Arrays.stream(filter_fields).anyMatch(filter_key::equals))
                                {
                                    Map<String, String> map = new HashMap<String, String>();
                                    map.put("key", filter_key);
                                    map.put("value", ((JsonString)mapEntry.getValue()).getString());
                                    filters.add(map);
                                }
                            }

                        }
                    }   
                }
                catch(Exception e)
                {
                }
                
                int total_filtered_count = db.get_users.all_filtered_count(con,filters,role_id);
                String all_users_info[][] = db.get_users.all_limited_filtered(con,starting_record,fetch_size,filters,role_id);

//                ArrayList<String[]> users_assigned_roles = db.get_roles.roles_for_user_id(con, user_id);
//                for (int i = 0; i < user_info.length; i++) {
//                    String string = user_info[i];
//                    json_builder.add(String.valueOf(i), user_info[i]);
//                    json_builder
//                      .add("employees", Json.createArrayBuilder()
//                        .add(Json.createObjectBuilder()
//                          .add("firstName", "John")
//                          .add("lastName", "Doe")));
//                }
                
                String roles = "";
                JsonArrayBuilder json_array_builder = Json.createArrayBuilder();
                for(int a = 0; a < all_users_info.length;a++)
                {
                    roles = db.get_roles.all_roles_for_user_id(con, all_users_info[a][0]); 
                    json_array_builder
                            .add(Json.createObjectBuilder()
                                    .add("id", all_users_info[a][0]) 
                                    .add("username", all_users_info[a][1]) 
                                    .add("first", all_users_info[a][3])
                                    .add("last", all_users_info[a][5])
                                    .add("email", all_users_info[a][15])
                                    .add("roles", roles)
                            );
                    
                                       
//                    stringBuilder.append(
//                        "<tr>" +
//                                "<td><i class=\"la la-edit info\"  style=\"cursor: pointer\"></i>&nbsp;<i class=\"la la-user-times danger\"  style=\"cursor: pointer\"></i>&nbsp;&nbsp;" + all_users_info[a][1] + "</td>" +
//                                "<td>" + all_users_info[a][3] + "</td>" +
//                                "<td>" + all_users_info[a][5] + "</td>" +
//                                "<td>" + all_users_info[a][15] + "</td>" +
//                                "<td>" + roles + "</td>" +
//                                "<td>" +
//                                    "<a onclick=\"editUser('" + all_users_info[a][0] + "')\" href=\"#\" class=\"mr-5\"><img src=\"assets/images/svg/edit-icon.svg\" alt=\"\"></a>" +
//                                    "<a onclick=\"javascript:location.href='admin_users_delete.jsp?id=" + all_users_info[a][0] + "'\" href=\"#\" class=\"mr-5\"><img src=\"assets/images/svg/trash-icon-blk.svg\" alt=\"\"></a>" +
//                                "</td>" +
//                        "</tr>"         
//                    );

                }
                con.close();

                json_builder.add("recordsTotal", total_filtered_count);
                json_builder.add("recordsFiltered", total_filtered_count);
                json_builder.add("draw", request.getParameter("draw"));
                json_builder.add("data", json_array_builder);
//
//                json_builder.add("total_filtered_count", total_filtered_count);
//                json_builder.add("users", json_array_builder);
                json = json_builder.build();

                //send the JSON data
                out.print(json.toString());
            }
            catch(Exception e)
            {
                logger.error("ERROR Exception in servlet ajax_lookup_users_list:=" + e);
                stringBuilder.setLength(0);
                stringBuilder.append("Error in retrieving users list");
                out.print(stringBuilder.toString());

            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

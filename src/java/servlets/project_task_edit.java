/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.Tika;
import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;

@MultipartConfig()
/**
 *
 * @author Ralph
 */
public class project_task_edit extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
    private static SimpleDateFormat form_format = new SimpleDateFormat("HH:mm / dd-MMM-YY"); //10:42 / 24-Jan-20
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            java.util.Date now = new java.util.Date();
            java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
            CouchDbClient dbClient = null;

            try
            {                
                String context_dir = request.getServletContext().getRealPath("");
                Connection con = db.db_util.get_contract_connection(context_dir, session);

                int project_id = 0;
                try{project_id = Integer.parseInt(request.getParameter("project_id"));}catch (Exception e){project_id=0;}
                int task_id = 0;
                try{task_id = Integer.parseInt(request.getParameter("task_id"));}catch (Exception e){task_id=0;}

                String name = "";
                try{name = request.getParameter("name");}catch (Exception e){name="";}
                String description = "";
                try{description = request.getParameter("description");}catch (Exception e){description="";}
                int assigned_group_id = 0;
                try{assigned_group_id = Integer.parseInt(request.getParameter("assigned_group_id"));}catch (Exception e){assigned_group_id=0;}
                int assigned_to_id = 0;
                try{assigned_to_id = Integer.parseInt(request.getParameter("assigned_to_id"));}catch (Exception e){assigned_to_id=0;}
                String priority = "";
                try{priority = request.getParameter("priority");}catch (Exception e){priority="";}
                String status = "";
                try{status = request.getParameter("status");}catch (Exception e){status="";}
                java.sql.Timestamp scheduled_start_date = null;
                try
                {
                    scheduled_start_date = new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("scheduled_start_date")).getTime());
                }
                catch(Exception e)
                {
                    scheduled_start_date = null;
                }
                java.sql.Timestamp actual_start_date = null;
                try
                {
                    actual_start_date = new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("actual_start_date")).getTime());
                }
                catch(Exception e)
                {
                    actual_start_date = null;
                }
                java.sql.Timestamp scheduled_end_date = null;
                try
                {
                    scheduled_end_date = new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("scheduled_end_date")).getTime());
                }
                catch(Exception e)
                {
                    scheduled_end_date = null;
                }
                java.sql.Timestamp actual_end_date = null;
                try
                {
                    actual_end_date = new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("actual_end_date")).getTime());
                }
                catch(Exception e)
                {
                    actual_end_date = null;
                }
                int estimated_duration = 0;
                try{estimated_duration = Integer.parseInt(request.getParameter("estimated_duration"));}catch (Exception e){estimated_duration=0;}
                int actual_duration = 0;
                try{actual_duration = Integer.parseInt(request.getParameter("actual_duration"));}catch (Exception e){actual_duration=0;}
                String notes = "";
                try{notes = request.getParameter("notes");}catch (Exception e){notes="";}

                String insert_project_tasks_table = "UPDATE project_tasks SET name=?,description=?,assigned_group_id=?,assigned_to_id=?,priority=?,status=?,scheduled_start_date=?,actual_start_date=?,scheduled_end_date=?,actual_end_date=?,estimated_duration=?,actual_duration=?,notes=? WHERE id=? AND project_id=?";
                
                PreparedStatement stmt = con.prepareStatement(insert_project_tasks_table);
                
                stmt.setString(1,name); //name
                stmt.setString(2,description); //description
                stmt.setInt(3, assigned_group_id); //assigned_group_id
                stmt.setInt(4, assigned_to_id); //assigned_to_id
                stmt.setString(5,priority); //priority
                stmt.setString(6,status); //status
                stmt.setTimestamp(7,scheduled_start_date); //scheduled_start_date
                stmt.setTimestamp(8,actual_start_date); //actual_start_date
                stmt.setTimestamp(9,scheduled_end_date); //scheduled_end_date
                stmt.setTimestamp(10,actual_end_date); //actual_end_date
                stmt.setInt(11, estimated_duration); //estimated_duration
                stmt.setInt(12, actual_duration); //actual_duration
                stmt.setString(13,notes); //notes 
                stmt.setInt(14, task_id); //id
                stmt.setInt(15, project_id); //project_id
                //System.out.println("q=" + stmt);
                stmt.execute();

                try
                {
                    LinkedHashMap props = support.config.get_config(context_dir);
                    dbClient = db.couchdb.get_client(props);
                    if (dbClient == null)
                    {
                        throw new Exception("dbClient is null");
                    }
                    String user_id = session.getAttribute("user_id").toString();
                    String name_on_disk;
                    String file_name;
                    Tika tika = new Tika();

                    for (Part filePart : request.getParts()) {
    //                    logger.info("filepart:" + filePart);
                        String fileName = filePart.getSubmittedFileName();
                        if (fileName != null && !"".equals(fileName)) 
                        {
                            long file_size = filePart.getSize();
                            String mimeType = tika.detect(filePart.getInputStream());
                            file_name = fileName.replaceAll("[^a-zA-Z0-9\\._]+", "_");

                            UUID Uuid = UUID.randomUUID(); //Generates random UUID                                  
                            String uuid = Uuid.toString();

                            Map<String, Object> map = new HashMap<>(); 
                            map.put("_id", uuid);
                            map.put("Filename", file_name);
                            map.put("MIME_type", mimeType);
                            map.put("FileSize", file_size);
                            map.put("DateUploaded", now);
                            map.put("UploadedBy", user_id);
                            map.put("AssociatedObject", "Project:Task");
                            map.put("AssociatedObjectId", project_id + ":" + task_id);

                            Response dbclient_response = dbClient.save(map);
                            name_on_disk = uuid + "/"+ file_name; 
 
                            dbclient_response = dbClient.saveAttachment(filePart.getInputStream(), file_name, mimeType, dbclient_response.getId(), dbclient_response.getRev());

                            if (dbclient_response.getError() == null)
                            {

                                String query_attachment = "INSERT INTO project_task_attachements (`project_id`, `task_id`, `uuid`,`name_on_disk`,`file_name`,`file_date`,`file_size`,`rev`,`uploaded_by`) VALUES (?,?,?,?,?,?,?,?,?)";
                                PreparedStatement stmt_attachment = con.prepareStatement(query_attachment);
                                stmt_attachment.setInt(1, project_id);
                                stmt_attachment.setInt(2, task_id);
                                stmt_attachment.setString(3, uuid);
                                stmt_attachment.setString(4, name_on_disk);
                                stmt_attachment.setString(5, file_name);
                                Timestamp file_date = new Timestamp(now.getTime());
                                stmt_attachment.setTimestamp(6, file_date);
                                stmt_attachment.setInt(7, Math.toIntExact(file_size));
                                stmt_attachment.setString(8, dbclient_response.getRev());
                                stmt_attachment.setInt(9, Integer.parseInt(user_id));
                                //System.out.println("email_support 94 stmt=" + stmt);
                                stmt_attachment.execute();
                            } else {
                                logger.error("CouchDB attachment upload error: {}", dbclient_response.getError());
                            }

                        }

                    }
                } catch (Exception e)
                {
//                    e.printStackTrace();
                    logger.error("ERROR attachments process in servlet project_task_edit:=" + e);
                }

                try
                { // processing attachment deletions
                    String[] attachments_delete_ids = request.getParameterValues("attachment_delete_id");
                    if (attachments_delete_ids != null)
                    {
                        ArrayList<String[]> attachments = db.get_projects.get_attachments_by_project_task_id(con, String.valueOf(project_id), String.valueOf(task_id));
                        ArrayList <String> deleted_ids = new ArrayList();

                        for (int i = 0; i < attachments.size(); i++)
                        {
                            String[] attachment = attachments.get(i);
                            if (Arrays.stream(attachments_delete_ids).anyMatch(attachment[0]::equals))
                            {
                                try
                                {
                                    dbClient.remove(attachment[3], attachment[8]);
                                    deleted_ids.add(attachment[0]);
                                } catch (Exception e)
                                {
                                    logger.error("ERROR attachments delete in servlet project_task_edit:=" + e);
                                }
                            }
                        }
                        if (deleted_ids.size() > 0)
                        {
                            String query_attachment = "DELETE FROM project_task_attachements WHERE project_id = ? AND task_id = ? AND id IN (" + String.join(",", deleted_ids) + ")";
                            PreparedStatement stmt_attachment = con.prepareStatement(query_attachment);

                            stmt_attachment.setInt(1, project_id);
                            stmt_attachment.setInt(2, task_id);
                            stmt_attachment.execute();
                        }
                    }
                    
                } catch (Exception e)
                {
                    e.printStackTrace();
                    logger.error("ERROR Exception in attachment deletion servlet project_task_edit:=" + e);
                }


//                String RedirectURL = "project.jsp?project_id=" + project_id ;
//                response.sendRedirect(RedirectURL);
            }
            catch(Exception e)
            {
                logger.error("ERROR Exception in servlet project_task_edit:=" + e);
            } finally {
                if (dbClient != null )
                {
                    dbClient.shutdown();
                }
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

@MultipartConfig()
/**
 *
 * @author rcampbell
 */
public class message_process extends HttpServlet 
{
    static private SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss"); 
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String session_user_id = session.getAttribute("user_id").toString();
        String session_username = session.getAttribute("username").toString();
        //get message_id
        String message_id = request.getParameter("message_id");
        
        //get action taken
        String action = request.getParameter("action");
        //"none","new_incident","new_request","follow-up_incident","follow-up_request","spam","update"
        //if action is 
        if(action.equalsIgnoreCase("none"))
        {
            do_none(request, response, message_id);
        }
        else if(action.equalsIgnoreCase("new_incident"))
        {
            do_new_incident(request, response, message_id);
        }
        else if(action.equalsIgnoreCase("new_request"))
        {
            do_new_request(request, response, message_id);
        }
        else if(action.equalsIgnoreCase("follow-up_incident"))
        {
            do_follow_up_incident(request, response, message_id);
        }
        else if(action.equalsIgnoreCase("follow-up_request"))
        {
            do_follow_up_request(request, response, message_id, action);
        }
        else if(action.equalsIgnoreCase("close"))
        {
            if (message_id != null && !message_id.equals("")) {
                do_close(request, response, message_id, true);
            }
            JsonObjectBuilder json_builder = Json.createObjectBuilder();
            JsonObject json;
            PrintWriter out;
            try {
                String[] message_ids = request.getParameterValues("message_ids[]");
                for (String message_id_from_ids : message_ids)
                {
                    do_close(request, response, message_id_from_ids, false);
                }
                out = response.getWriter();
                json_builder.add("error", "");
                json = json_builder.build();
                out.print(json.toString());
            }
            catch(Exception e)
            {
                out = response.getWriter();
                System.out.println("ERROR Exception in servlet message_process:=" + e.getMessage());
                json_builder.add("error", e.getMessage());
                json = json_builder.build();
                out.print(json.toString());
            }
            
        }
        else if(action.equalsIgnoreCase("spam"))
        {
            if (message_id != null && !message_id.equals("")) {
                do_spam(request, response, message_id, true);
            }
            JsonObjectBuilder json_builder = Json.createObjectBuilder();
            JsonObject json;
            PrintWriter out;
            try {
                String[] message_ids = request.getParameterValues("message_ids[]");
                for (String message_id_from_ids : message_ids)
                {
                    do_spam(request, response, message_id_from_ids, false);
                }
                out = response.getWriter();
                json_builder.add("error", "");
                json = json_builder.build();
                out.print(json.toString());
            }
            catch(Exception e)
            {
                out = response.getWriter();
                System.out.println("ERROR Exception in servlet message_process:=" + e.getMessage());
                json_builder.add("error", e.getMessage());
                json = json_builder.build();
                out.print(json.toString());
            }
            
        }
        else if(action.equalsIgnoreCase("update"))
        {
            do_update(request, response, message_id);
        }
    }
    
    protected String do_encode(String inString)
    {
        String return_string = "";
        try 
        {
            return_string = URLEncoder.encode(inString, StandardCharsets.UTF_8.toString());
        } 
        catch (UnsupportedEncodingException ex) 
        {
            throw new RuntimeException(ex.getCause());
        }
        return return_string;
    }
    protected void do_none(HttpServletRequest request, HttpServletResponse response,String message_id)
    {
        try
        {
            String RedirectURL = "message_list.jsp?status=new";
            response.sendRedirect(RedirectURL);
        }
        catch(Exception e)
        {
            System.out.println("Error in message_process servlet redirect on none action=" + e);
        }
    }
    protected void do_new_incident(HttpServletRequest request, HttpServletResponse response,String message_id)
    {
        HttpSession session = request.getSession();   
        try
        {
            String context_dir = request.getServletContext().getRealPath("");
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            //get the message
            String message[] = db.get_message.message_by_id(con, message_id);
            //get message log, why the only field that can be edited         
            String message_log = request.getParameter("log");
            //get the from users email
            String from = request.getParameter("from");
            String referer = request.getParameter("referer");
            String action = request.getParameter("action");
            
            //process the call
            String message_info[] = db.get_message.message_by_id(con, message_id);
            String call_info[] = db.get_call.for_interaction_id(con, message_info[1]);
            String start_working_on_message_time = request.getParameter("start_working_on_message_time");  //timestamp format
            try
            {
                //# id	media_type	create_time	     accept_time	accept_time_seconds	abandon_time	abandon_time_seconds	call_duration_seconds	channel	direction	agent_name	post_process_time	process_time	total_time	interaction_id
                //22526	Email	        2021-05-01 21:54:42						                                                                Email	0					                         2e55a5dd-421f-4a74-8d38-74ecd31331f3
                //field needing to update
                //accept_time
                String accept_time = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
                //accept_time_seconds  (accept_time - create_time) /1000 <Sec to Seconds
                java.util.Date aTime = timestamp_format.parse(accept_time);
                java.util.Date cTime = timestamp_format.parse(call_info[2]);
                String accept_time_seconds = String.valueOf((aTime.getTime() - cTime.getTime()) / 1000);
                //call_duration_seconds  
                java.util.Date now_in_utc = Date.from(java.time.ZonedDateTime.now().toInstant());
                String call_duration_seconds = String.valueOf((now_in_utc.getTime() - cTime.getTime()) / 1000);
                //agent_name
                String agent_name = session.getAttribute("username").toString();
                //post_process_time = 0
                String post_process_time = "0";
                //process_time   //now - start working on time / 1000
                java.util.Date sTime = timestamp_format.parse(start_working_on_message_time);
                String process_time = String.valueOf((now_in_utc.getTime() - sTime.getTime()) / 1000);
                //total_time
                String total_time = String.valueOf((now_in_utc.getTime() - cTime.getTime()) / 1000);
                //System.out.println("create Time=" + cTime);
                //System.out.println("accept Time=" + aTime);
                //System.out.println("Now    Time=" + now_in_utc);
                //System.out.println("start wTime=" + sTime);
                String update_call = "UPDATE `calls` SET "
                        + "`accept_time`=?,"
                        + "`accept_time_seconds` = ?,"
                        + "`call_duration_seconds` = ?,"
                        + "`agent_name` = ?,"
                        + "`post_process_time` = ?," 
                        + "`process_time` = ?," 
                        + "`total_time` = ?" 
                        + " WHERE `interaction_id` = ?";                
                PreparedStatement stmt = con.prepareStatement(update_call);
                stmt.setString(1, accept_time);
                stmt.setString(2, accept_time_seconds);
                stmt.setString(3, call_duration_seconds);                
                stmt.setString(4, agent_name); 
                stmt.setString(5, post_process_time);
                stmt.setString(6, process_time);
                stmt.setString(7, total_time); 
                stmt.setString(8, message_info[1]);  //message_uuid
                //System.out.println("stmt=" + stmt);
                stmt.executeUpdate(); 
            }
            catch(Exception e)
            {
                System.out.println("ERROR Exception in servlet message_process do_new_incident update call record:=" + e);
            }
            
            //does this email exist in the user database?
            boolean users_email_exists = db.get_users.email_exists(con, from);
            
            //System.out.println("from=" + from + " Exists=" + users_email_exists);
            if(!users_email_exists)
            {
                //send to create user with referer,action,message_id,message_log 
                String RedirectURL = "users_add_lite.jsp?" + 
                        "from=" + do_encode(from) + 
                        "&referer=" + do_encode(referer) + 
                        "&action=" + do_encode(action) + 
                        "&message_id=" + message_id + 
                        "&message_log=" + do_encode(message_log);
                
                response.sendRedirect(RedirectURL);
            }
            else
            {
                String caller_info[] = db.get_users.by_email(con, from);        
                //get next incident_id
                int incident_id = db.get_incidents.next_incident_id(con);
                System.out.println("incident_id=" + incident_id);
                //insert new incident stub
                //create fields/values for save_incident_stub
                ArrayList<String[]> field_values = new ArrayList();
                Properties incident_props = new Properties();
                incident_props.put("incident_id", String.valueOf(incident_id));
                incident_props.put("incident_time", DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc()));
                incident_props.put("caller_id", caller_info[0]);
                incident_props.put("description", message_info[5]);
                incident_props.put("create_date", DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc()));
                incident_props.put("create_by_id", session.getAttribute("user_id").toString());
                incident_props.put("contact_method", "Self-Service");
                incident_props.put("state", "New");
                incident_props.put("state_date", DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc()));
                incident_props.put("notes", StringEscapeUtils.escapeHtml4(message_log) + "Subject:" + StringEscapeUtils.escapeHtml4(message_info[5]) + " Body:" + StringEscapeUtils.escapeHtml4(message_info[7]));
                incident_props.put("external_id", String.valueOf(incident_id));
                
                boolean saved_incident = db.save_incident_stub.new_with_history(con, incident_props);     
                   
                //update the message status=closed status_date=utc_now  status_by_user_id  related_to=incident  related_to_id=incident_id
                String query = "UPDATE message SET `status`=?,`status_date`=?, `status_by_user_id`=?,`log`=?,`related_to`=?,`related_to_id`=?, `assigned_to_id`=?,`date_assigned`=?,`date_closed`=? WHERE `id`=?";
                
                String assigned_to_id = "";
                try
                {
                    assigned_to_id = request.getParameter("assigned_to_id");
                    if(assigned_to_id == null || assigned_to_id.equalsIgnoreCase(""))
                    {
                        assigned_to_id = session.getAttribute("user_id").toString();
                    }
                }
                catch(Exception e)
                {
                    assigned_to_id = session.getAttribute("user_id").toString();
                }
                String date_assigned = "";
                java.util.Date temp_date_assigned = new java.util.Date();
                try
                {
                    date_assigned = request.getParameter("date_assigned");
                    if(date_assigned != null)
                    {
                        temp_date_assigned = date_time_picker_format.parse(date_assigned);
                        date_assigned = timestamp_format.format(temp_date_assigned);
                    }
                }
                catch(Exception e)
                {
                    date_assigned = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
                }
                String date_closed = "";
                java.util.Date temp_date_closed = new java.util.Date();
                try
                {
                    date_closed = request.getParameter("date_closed");
                    if(date_closed != null)
                    {
                        temp_date_closed = date_time_picker_format.parse(date_closed);
                        date_closed = timestamp_format.format(temp_date_closed);
                    }
                }
                catch(Exception e)
                {
                    date_closed = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
                }
                try
                {
                    PreparedStatement stmt = con.prepareStatement(query);
                    stmt.setString(1, "Closed");
                    stmt.setString(2, DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc()));
                    stmt.setString(3, session.getAttribute("user_id").toString());
                    message_log = message_log + " Message Closed at " + DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc()) + ".<br>";
                    message_log = message_log + " Incident:" + incident_id + " was created.<br>";
                    stmt.setString(4, StringEscapeUtils.escapeHtml4(message_log)); 
                    stmt.setString(5, "incident");
                    stmt.setString(6, String.valueOf(incident_id));
                    stmt.setString(7, assigned_to_id);
                    stmt.setString(8, date_assigned);
                    stmt.setString(9, date_closed);
                    stmt.setString(10, message_id); 
                    System.out.println("q=" + stmt);
                    stmt.executeUpdate(); 
                }
                catch(Exception e)
                {
                    System.out.println("ERROR Exception in servlet message_process create new incident and close:=" + e);
                }
                //redirect to incident.jsp?id=INCIDENT_ID
                response.sendRedirect("incident_list.jsp?id=" + incident_id);
            }   
        }
        catch(Exception e)
        {
            System.out.println("Error in message_process servlet redirect on new_incident action=" + e);
        }
    }
    protected void do_new_request(HttpServletRequest request, HttpServletResponse response,String message_id)
    {
        HttpSession session = request.getSession();   
        try
        {
            String context_dir = request.getServletContext().getRealPath("");
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            //get the message
            String message[] = db.get_message.message_by_id(con, message_id);
            //get message log, why the only field that can be edited         
            String message_log = request.getParameter("log");
            //get the from users email
            String from = request.getParameter("from");
            String referer = request.getParameter("referer");
            String action = request.getParameter("action");
            
            //process the call
            String message_info[] = db.get_message.message_by_id(con, message_id);
            String call_info[] = db.get_call.for_interaction_id(con, message_info[1]);
            String start_working_on_message_time = request.getParameter("start_working_on_message_time");  //timestamp format
            try
            {
                //# id	media_type	create_time	     accept_time	accept_time_seconds	abandon_time	abandon_time_seconds	call_duration_seconds	channel	direction	agent_name	post_process_time	process_time	total_time	interaction_id
                //22526	Email	        2021-05-01 21:54:42						                                                                Email	0					                         2e55a5dd-421f-4a74-8d38-74ecd31331f3
                //field needing to update
                //accept_time
                String accept_time = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
                //accept_time_seconds  (accept_time - create_time) /1000 <Sec to Seconds
                java.util.Date aTime = timestamp_format.parse(accept_time);
                java.util.Date cTime = timestamp_format.parse(call_info[2]);
                String accept_time_seconds = String.valueOf((aTime.getTime() - cTime.getTime()) / 1000);
                //call_duration_seconds  
                java.util.Date now_in_utc = Date.from(java.time.ZonedDateTime.now().toInstant());
                String call_duration_seconds = String.valueOf((now_in_utc.getTime() - cTime.getTime()) / 1000);
                //agent_name
                String agent_name = session.getAttribute("username").toString();
                //post_process_time = 0
                String post_process_time = "0";
                //process_time   //now - start working on time / 1000
                java.util.Date sTime = timestamp_format.parse(start_working_on_message_time);
                String process_time = String.valueOf((now_in_utc.getTime() - sTime.getTime()) / 1000);
                //total_time
                String total_time = String.valueOf((now_in_utc.getTime() - cTime.getTime()) / 1000);
                //System.out.println("create Time=" + cTime);
                //System.out.println("accept Time=" + aTime);
                //System.out.println("Now    Time=" + now_in_utc);
                //System.out.println("start wTime=" + sTime);
                String update_call = "UPDATE `calls` SET "
                        + "`accept_time`=?,"
                        + "`accept_time_seconds` = ?,"
                        + "`call_duration_seconds` = ?,"
                        + "`agent_name` = ?,"
                        + "`post_process_time` = ?," 
                        + "`process_time` = ?," 
                        + "`total_time` = ?" 
                        + " WHERE `interaction_id` = ?";                
                PreparedStatement stmt = con.prepareStatement(update_call);
                
                stmt.setString(1, accept_time);
                stmt.setString(2, accept_time_seconds);
                stmt.setString(3, call_duration_seconds);                
                stmt.setString(4, agent_name); 
                stmt.setString(5, post_process_time);
                stmt.setString(6, process_time);
                stmt.setString(7, total_time); 
                stmt.setString(8, message_info[1]);  //message_uuid
                //System.out.println("stmt=" + stmt);
                stmt.executeUpdate(); 
            }
            catch(Exception e)
            {
                System.out.println("ERROR Exception in servlet message_process do_new_request update call record:=" + e);
            }
            
            //does this email exist in the user database?
            boolean users_email_exists = db.get_users.email_exists(con, from);
            
            
            if(!users_email_exists)
            {
                //send to create user with referer,action,message_id,message_log 
                String RedirectURL = "users_add_lite.jsp?" + 
                        "from=" + do_encode(from) + 
                        "&referer=" + do_encode(referer) + 
                        "&action=" + do_encode(action) + 
                        "&message_id=" + message_id + 
                        "&message_log=" + do_encode(message_log);
                
                response.sendRedirect(RedirectURL);
            }
            else
            {
                String caller_info[] = db.get_users.by_email(con, from);        
                //update the message status=closed status_date=utc_now  status_by_user_id  related_to=incident  related_to_id=request_id
                //String query = "UPDATE message SET `status`=?,`status_date`=?, `status_by_user_id`=?,`log`=?,`related_to`=?,`related_to_id`=? WHERE `id`=?";
                String query = "UPDATE message SET `status`=?,`status_date`=?, `status_by_user_id`=?,`log`=?,`related_to`=?,`related_to_id`=?, `assigned_to_id`=?,`date_assigned`=?,`date_closed`=? WHERE `id`=?";
                String assigned_to_id = "";
                try
                {
                    assigned_to_id = request.getParameter("assigned_to_id");
                    if(assigned_to_id == null || assigned_to_id.equalsIgnoreCase(""))
                    {
                        assigned_to_id = session.getAttribute("user_id").toString();
                    }
                }
                catch(Exception e)
                {
                    assigned_to_id = session.getAttribute("user_id").toString();
                }
                String date_assigned = "";
                java.util.Date temp_date_assigned = new java.util.Date();
                try
                {
                    date_assigned = request.getParameter("date_assigned");
                    if(date_assigned != null)
                    {
                        temp_date_assigned = date_time_picker_format.parse(date_assigned);
                        date_assigned = timestamp_format.format(temp_date_assigned);
                    }
                }
                catch(Exception e)
                {
                    date_assigned = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
                }
                String date_closed = "";
                java.util.Date temp_date_closed = new java.util.Date();
                try
                {
                    date_closed = request.getParameter("date_closed");
                    if(date_closed != null)
                    {
                        temp_date_closed = date_time_picker_format.parse(date_closed);
                        date_closed = timestamp_format.format(temp_date_closed);
                    }
                }
                catch(Exception e)
                {
                    date_closed = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
                }
                try
                {
                    PreparedStatement stmt = con.prepareStatement(query);
                    stmt.setString(1, "Closed");
                    stmt.setString(2, DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc()));
                    stmt.setString(3, session.getAttribute("user_id").toString());
                    message_log = message_log + " Message Closed at " + DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc()) + ".<br>";
                    message_log = message_log + " Request was created.<br>";
                    stmt.setString(4, StringEscapeUtils.escapeHtml4(message_log)); 
                    stmt.setString(5, "request");
                    stmt.setString(6, null);
                    stmt.setString(7, assigned_to_id);
                    stmt.setString(8, date_assigned);
                    stmt.setString(9, date_closed);
                    stmt.setString(10, message_id); 
                    System.out.println("q=" + stmt);
                    stmt.executeUpdate(); 
                }
                catch(Exception e)
                {
                    System.out.println("ERROR Exception in servlet message_process create new request and close:=" + e);
                }
                /*try
                {
                    PreparedStatement stmt = con.prepareStatement(query);
                    stmt.setString(1, "Closed");
                    stmt.setString(2, DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc()));
                    stmt.setString(3, session.getAttribute("user_id").toString());
                    message_log = message_log + " Message Closed at " + DateTimeFormatter.ofPattern("HH:mm MM/dd/yyyy").format(support.date_utils.now_in_utc()) + ".";
                    message_log = message_log + " Request was created.";
                    stmt.setString(4, message_log); 
                    stmt.setString(5, "request");
                    stmt.setString(6, null);
                    stmt.setString(7, message_id); 
                    System.out.println("q=" + stmt);
                    stmt.executeUpdate(); 
                }
                catch(Exception e)
                {
                    System.out.println("ERROR Exception in servlet message_process update message table:=" + e);
                }*/
                //redirect to service_catalog.jsp
                response.sendRedirect("home_service_catalog.jsp?message_id=" + message_id + "&user_id=" + caller_info[0]);
            }   
        }
        catch(Exception e)
        {
            System.out.println("Error in message_process servlet redirect on new_request action=" + e);
        }
    }
    protected void do_follow_up_incident(HttpServletRequest request, HttpServletResponse response,String message_id)
    {
        String redirect = "message_process.jsp?id=" + message_id;
        java.util.Date now = new java.util.Date();
        String now_string = date_time_picker_format.format(now);
        HttpSession session = request.getSession();   
        try
        {
            String context_dir = request.getServletContext().getRealPath("");
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            //get the message
            String message[] = db.get_message.message_by_id(con, message_id);
            String message_source = message[2];
            String message_subject = message[5];
            String message_body = message[7];
            //get message log, why the only field that can be edited         
            String message_log = request.getParameter("log");
            //get the from users email
            String from = request.getParameter("from");
            //String referer = request.getParameter("referer");
            //String action = request.getParameter("action");
            //does this email exist in the user database?
            //process the call
            String message_info[] = db.get_message.message_by_id(con, message_id);
            String call_info[] = db.get_call.for_interaction_id(con, message_info[1]);
            String start_working_on_message_time = request.getParameter("start_working_on_message_time");  //timestamp format
            try
            {
                //# id	media_type	create_time	     accept_time	accept_time_seconds	abandon_time	abandon_time_seconds	call_duration_seconds	channel	direction	agent_name	post_process_time	process_time	total_time	interaction_id
                //22526	Email	        2021-05-01 21:54:42						                                                                Email	0					                         2e55a5dd-421f-4a74-8d38-74ecd31331f3
                //field needing to update
                //accept_time
                String accept_time = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
                //accept_time_seconds  (accept_time - create_time) /1000 <Sec to Seconds
                java.util.Date aTime = timestamp_format.parse(accept_time);
                java.util.Date cTime = timestamp_format.parse(call_info[2]);
                String accept_time_seconds = String.valueOf((aTime.getTime() - cTime.getTime()) / 1000);
                //call_duration_seconds  
                java.util.Date now_in_utc = Date.from(java.time.ZonedDateTime.now().toInstant());
                String call_duration_seconds = String.valueOf((now_in_utc.getTime() - cTime.getTime()) / 1000);
                //agent_name
                String agent_name = session.getAttribute("username").toString();
                //post_process_time = 0
                String post_process_time = "0";
                //process_time   //now - start working on time / 1000
                java.util.Date sTime = timestamp_format.parse(start_working_on_message_time);
                String process_time = String.valueOf((now_in_utc.getTime() - sTime.getTime()) / 1000);
                //total_time
                String total_time = String.valueOf((now_in_utc.getTime() - cTime.getTime()) / 1000);
                //System.out.println("create Time=" + cTime);
                //System.out.println("accept Time=" + aTime);
                //System.out.println("Now    Time=" + now_in_utc);
                //System.out.println("start wTime=" + sTime);
                String update_call = "UPDATE `calls` SET "
                        + "`accept_time`=?,"
                        + "`accept_time_seconds` = ?,"
                        + "`call_duration_seconds` = ?,"
                        + "`agent_name` = ?,"
                        + "`post_process_time` = ?," 
                        + "`process_time` = ?," 
                        + "`total_time` = ?" 
                        + " WHERE `interaction_id` = ?";                
                PreparedStatement stmt = con.prepareStatement(update_call);
                stmt.setString(1, accept_time);
                stmt.setString(2, accept_time_seconds);
                stmt.setString(3, call_duration_seconds);                
                stmt.setString(4, agent_name); 
                stmt.setString(5, post_process_time);
                stmt.setString(6, process_time);
                stmt.setString(7, total_time); 
                stmt.setString(8, message_info[1]);  //message_uuid
                //System.out.println("stmt=" + stmt);
                stmt.executeUpdate(); 
            }
            catch(Exception e)
            {
                System.out.println("ERROR Exception in servlet message_process do_follow_up_incident update call record:=" + e);
            }
            
            boolean users_email_exists = db.get_users.email_exists(con, from);
            //System.out.println("from=" + from + " Exists=" + users_email_exists);
            //link_to_item
            String link_to_item = request.getParameter("link_to_item");
            
            try
            {
                if(link_to_item != null)
                {
                    if(link_to_item.startsWith("INC"))  //INC114974 or REQ18
                    {
                        String incident_id = link_to_item.replace("INC", "");
                        String incident_info[] = db.get_incidents.incident_by_id(con, incident_id);
                        String unescaped_notes = StringEscapeUtils.unescapeHtml4(incident_info[23]); //turn it back to html
                                
                        String addon_notes = "<br>---------------------------------------------------------------------------------<br>"
                                + "Message received:" + now_string
                                + " <br>Message Source:" + message_source
                                + " <br>Message Subject:" + message_subject
                                + " <br>Message Body:" + message_body
                                + " <br>Activity Log:" + message_log;
                        String new_notes = unescaped_notes + " " + addon_notes;
                        String escaped_notes = StringEscapeUtils.escapeHtml4(new_notes); //turn it back to escaped html
                        //update the incident
                        String query = "UPDATE incidents SET `notes`=? WHERE `id`=?";
                        try
                        {
                            PreparedStatement stmt = con.prepareStatement(query);
                            stmt.setString(1, escaped_notes);
                            stmt.setString(2, incident_id);
                            System.out.println("q=" + stmt);
                            stmt.executeUpdate(); 
                        }
                        catch(Exception e)
                        {
                            System.out.println("ERROR Exception in servlet message_process do_follow_up_incident:=" + e);
                        }
                        //update the message status=closed status_date=utc_now  status_by_user_id  related_to=incident  related_to_id=incident_id
                        query = "UPDATE message SET `status`=?,`status_date`=?, `status_by_user_id`=?,`log`=?,`related_to`=?,`related_to_id`=?, `assigned_to_id`=?,`date_assigned`=?,`date_closed`=? WHERE `id`=?";

                        String assigned_to_id = "";
                        try
                        {
                            assigned_to_id = request.getParameter("assigned_to_id");
                            if(assigned_to_id == null || assigned_to_id.equalsIgnoreCase(""))
                            {
                                assigned_to_id = session.getAttribute("user_id").toString();
                            }
                        }
                        catch(Exception e)
                        {
                            assigned_to_id = session.getAttribute("user_id").toString();
                        }
                        String date_assigned = "";
                        java.util.Date temp_date_assigned = new java.util.Date();
                        try
                        {
                            date_assigned = request.getParameter("date_assigned");
                            if(date_assigned != null)
                            {
                                temp_date_assigned = date_time_picker_format.parse(date_assigned);
                                date_assigned = timestamp_format.format(temp_date_assigned);
                            }
                        }
                        catch(Exception e)
                        {
                            date_assigned = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
                        }
                        String date_closed = "";
                        java.util.Date temp_date_closed = new java.util.Date();
                        try
                        {
                            date_closed = request.getParameter("date_closed");
                            if(date_closed != null)
                            {
                                temp_date_closed = date_time_picker_format.parse(date_closed);
                                date_closed = timestamp_format.format(temp_date_closed);
                            }
                        }
                        catch(Exception e)
                        {
                            date_closed = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
                        }
                        try
                        {
                            PreparedStatement stmt = con.prepareStatement(query);
                            stmt.setString(1, "Closed");
                            stmt.setString(2, DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc()));
                            stmt.setString(3, session.getAttribute("user_id").toString());
                            message_log = message_log + " Message Closed at " + DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc()) + ".<br>";
                            message_log = message_log + " Incident:" + incident_id + " was created.<br>";
                            stmt.setString(4, StringEscapeUtils.escapeHtml4(message_log)); 
                            stmt.setString(5, "incident");
                            stmt.setString(6, String.valueOf(incident_id));
                            stmt.setString(7, assigned_to_id);
                            stmt.setString(8, date_assigned);
                            stmt.setString(9, date_closed);
                            stmt.setString(10, message_id); 
                            System.out.println("q=" + stmt);
                            stmt.executeUpdate(); 
                        }
                        catch(Exception e)
                        {
                            System.out.println("ERROR Exception in servlet message_process create new incident and close:=" + e);
                        }
                        //redirect to incident.jsp?id=INCIDENT_ID
                        redirect = "incident_list.jsp?id=" + incident_id;
                    }   
                }
            }
            catch(Exception e)
            {
                System.out.println("Exception in message_process servlet do_follow_up_incident=" + e);
            } 
        }
        catch(Exception e)
        {
            System.out.println("Error in message_process servlet redirect on do_follow_up_incident action=" + e);
        }
        finally
        {
            try
            {
                response.sendRedirect(redirect);
            }
            catch(Exception e)
            {
                
            }
        }
    }        
    protected void do_follow_up_request(HttpServletRequest request, HttpServletResponse response,String message_id, String action)
    {
        HttpSession session = request.getSession();   
        String link_to_item = request.getParameter("link_to_item");
        link_to_item = link_to_item.replace("REQ", "");
        //update the message status=closed status_date=utc_now  status_by_user_id  related_to=incident  related_to_id=incident_id
        String query = "UPDATE message SET `status`=?,`status_date`=?, `status_by_user_id`=?,`log`=?,`related_to`=?,`related_to_id`=?, `assigned_to_id`=?,`date_assigned`=?,`date_closed`=? WHERE `id`=?";
        String assigned_to_id = "";
        String message_log = request.getParameter("log");
        try
        {
            assigned_to_id = request.getParameter("assigned_to_id");
            if(assigned_to_id == null || assigned_to_id.equalsIgnoreCase(""))
            {
                assigned_to_id = session.getAttribute("user_id").toString();
            }
        }
        catch(Exception e)
        {
            assigned_to_id = session.getAttribute("user_id").toString();
        }
        String date_assigned = "";
        java.util.Date temp_date_assigned = new java.util.Date();
        try
        {
            date_assigned = request.getParameter("date_assigned");
            if(date_assigned != null)
            {
                temp_date_assigned = date_time_picker_format.parse(date_assigned);
                date_assigned = timestamp_format.format(temp_date_assigned);
            }
        }
        catch(Exception e)
        {
            date_assigned = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
        }
        String date_closed = "";
        java.util.Date temp_date_closed = new java.util.Date();
        try
        {
            date_closed = request.getParameter("date_closed");
            if(date_closed != null)
            {
                temp_date_closed = date_time_picker_format.parse(date_closed);
                date_closed = timestamp_format.format(temp_date_closed);
            }
        }
        catch(Exception e)
        {
            date_closed = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
        }
        try
        {
            String context_dir = request.getServletContext().getRealPath("");
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, "Closed");
            stmt.setString(2, DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc()));
            stmt.setString(3, session.getAttribute("user_id").toString());
            message_log = message_log + " Message Closed at " + DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc()) + ".<br>";
            message_log = message_log + " Request was created.<br>";
            stmt.setString(4, StringEscapeUtils.escapeHtml4(message_log)); 
            stmt.setString(5, "request");
            stmt.setString(6, null);
            stmt.setString(7, assigned_to_id);
            stmt.setString(8, date_assigned);
            stmt.setString(9, date_closed);
            stmt.setString(10, message_id); 
            System.out.println("q=" + stmt);
            stmt.executeUpdate(); 
        }
        catch(Exception e)
        {
            System.out.println("ERROR Exception in servlet message_process create new request and close:=" + e);
        }
        
        
        
        
        
        try
        {
            String context_dir = request.getServletContext().getRealPath("");
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            //process the call
            String message_info[] = db.get_message.message_by_id(con, message_id);
            String call_info[] = db.get_call.for_interaction_id(con, message_info[1]);
            String start_working_on_message_time = request.getParameter("start_working_on_message_time");  //timestamp format
            try
            {
                //# id	media_type	create_time	     accept_time	accept_time_seconds	abandon_time	abandon_time_seconds	call_duration_seconds	channel	direction	agent_name	post_process_time	process_time	total_time	interaction_id
                //22526	Email	        2021-05-01 21:54:42						                                                                Email	0					                         2e55a5dd-421f-4a74-8d38-74ecd31331f3
                //field needing to update
                //accept_time
                String accept_time = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
                //accept_time_seconds  (accept_time - create_time) /1000 <Sec to Seconds
                java.util.Date aTime = timestamp_format.parse(accept_time);
                java.util.Date cTime = timestamp_format.parse(call_info[2]);
                String accept_time_seconds = String.valueOf((aTime.getTime() - cTime.getTime()) / 1000);
                //call_duration_seconds  
                java.util.Date now_in_utc = Date.from(java.time.ZonedDateTime.now().toInstant());
                String call_duration_seconds = String.valueOf((now_in_utc.getTime() - cTime.getTime()) / 1000);
                //agent_name
                String agent_name = session.getAttribute("username").toString();
                //post_process_time = 0
                String post_process_time = "0";
                //process_time   //now - start working on time / 1000
                java.util.Date sTime = timestamp_format.parse(start_working_on_message_time);
                String process_time = String.valueOf((now_in_utc.getTime() - sTime.getTime()) / 1000);
                //total_time
                String total_time = String.valueOf((now_in_utc.getTime() - cTime.getTime()) / 1000);
                //System.out.println("create Time=" + cTime);
                //System.out.println("accept Time=" + aTime);
                //System.out.println("Now    Time=" + now_in_utc);
                //System.out.println("start wTime=" + sTime);
                String update_call = "UPDATE `calls` SET "
                        + "`accept_time`=?,"
                        + "`accept_time_seconds` = ?,"
                        + "`call_duration_seconds` = ?,"
                        + "`agent_name` = ?,"
                        + "`post_process_time` = ?," 
                        + "`process_time` = ?," 
                        + "`total_time` = ?" 
                        + " WHERE `interaction_id` = ?";                
                PreparedStatement stmt2 = con.prepareStatement(update_call);
                stmt2.setString(1, accept_time);
                stmt2.setString(2, accept_time_seconds);
                stmt2.setString(3, call_duration_seconds);                
                stmt2.setString(4, agent_name); 
                stmt2.setString(5, post_process_time);
                stmt2.setString(6, process_time);
                stmt2.setString(7, total_time); 
                stmt2.setString(8, message_info[1]);  //message_uuid
                //System.out.println("stmt=" + stmt);
                stmt2.executeUpdate(); 
            }
            catch(Exception e)
            {
                System.out.println("ERROR Exception in servlet message_process do_follow_up_request update call record:=" + e);
            }            
        }
        catch(Exception e)
        {
            System.out.println("ERROR Exception in servlet message_process do_follow_up_request:=" + e);
        }
        try
        {            
            response.sendRedirect("request.jsp?id=" + link_to_item + "&message_id=" + message_id + "&action=" + action);
        }
        catch(Exception e)
        {
            
        }
    } 
    protected void do_close(HttpServletRequest request, HttpServletResponse response,String message_id, boolean send_redirect)
    {
        HttpSession session = request.getSession();   
        try
        {
            String start_working_on_message_time = request.getParameter("start_working_on_message_time");  //timestamp format
            if (start_working_on_message_time == null) {
                start_working_on_message_time = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
            }
            String context_dir = request.getServletContext().getRealPath("");
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String message_log = request.getParameter("log");
            //update the message status=closed status_date=utc_now  status_by_user_id  related_to=incident  related_to_id=incident_id
            
            //process the message
            try
            {
                String query = "UPDATE message SET `status`=?,`status_date`=?, `status_by_user_id`=?,`log`=?,`related_to`=?,`related_to_id`=? WHERE `id`=?";
                PreparedStatement stmt = con.prepareStatement(query);
                stmt.setString(1, "Closed");
                stmt.setString(2, DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc()));
                stmt.setString(3, session.getAttribute("user_id").toString());
                message_log = message_log + " Message Closed at " + DateTimeFormatter.ofPattern("HH:mm MM/dd/yyyy").format(support.date_utils.now_in_utc()) + ".";
                message_log = message_log + " Closed By:" + session.getAttribute("username").toString();
                stmt.setString(4, message_log); 
                stmt.setString(5, "close");
                stmt.setString(6, null);
                stmt.setString(7, message_id); 
                stmt.executeUpdate(); 
            }
            catch(Exception e)
            {
                System.out.println("ERROR Exception in servlet message_process do_close:=" + e);
            }
            //process the call
            String message_info[] = db.get_message.message_by_id(con, message_id);
            String call_info[] = db.get_call.for_interaction_id(con, message_info[1]);
            try
            {
                //# id	media_type	create_time	     accept_time	accept_time_seconds	abandon_time	abandon_time_seconds	call_duration_seconds	channel	direction	agent_name	post_process_time	process_time	total_time	interaction_id
                //22526	Email	        2021-05-01 21:54:42						                                                                Email	0					                         2e55a5dd-421f-4a74-8d38-74ecd31331f3

                //field needing to update
                //accept_time
                String accept_time = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
                //accept_time_seconds  (accept_time - create_time) /1000 <Sec to Seconds
                java.util.Date aTime = timestamp_format.parse(accept_time);
                java.util.Date cTime = timestamp_format.parse(call_info[2]);
                String accept_time_seconds = String.valueOf((aTime.getTime() - cTime.getTime()) / 1000);
                //call_duration_seconds  
                java.util.Date now_in_utc = Date.from(java.time.ZonedDateTime.now().toInstant());
                String call_duration_seconds = String.valueOf((now_in_utc.getTime() - cTime.getTime()) / 1000);
                //agent_name
                String agent_name = session.getAttribute("username").toString();
                //post_process_time = 0
                String post_process_time = "0";
                //process_time   //now - start working on time / 1000
                java.util.Date sTime = timestamp_format.parse(start_working_on_message_time);
                String process_time = String.valueOf((now_in_utc.getTime() - sTime.getTime()) / 1000);
                //total_time
                String total_time = String.valueOf((now_in_utc.getTime() - cTime.getTime()) / 1000);
                //System.out.println("create Time=" + cTime);
                //System.out.println("accept Time=" + aTime);
                //System.out.println("Now    Time=" + now_in_utc);
                //System.out.println("start wTime=" + sTime);
                String update_call = "UPDATE `calls` SET "
                        + "`accept_time`=?,"
                        + "`accept_time_seconds` = ?,"
                        + "`call_duration_seconds` = ?,"
                        + "`agent_name` = ?,"
                        + "`post_process_time` = ?," 
                        + "`process_time` = ?," 
                        + "`total_time` = ?" 
                        + " WHERE `interaction_id` = ?";                
                PreparedStatement stmt = con.prepareStatement(update_call);
                stmt.setString(1, accept_time);
                stmt.setString(2, accept_time_seconds);
                stmt.setString(3, call_duration_seconds);                
                stmt.setString(4, agent_name); 
                stmt.setString(5, post_process_time);
                stmt.setString(6, process_time);
                stmt.setString(7, total_time); 
                stmt.setString(8, message_info[1]);  //message_uuid
                //System.out.println("stmt=" + stmt);
                stmt.executeUpdate(); 
            }
            catch(Exception e)
            {
                e.printStackTrace();
                System.out.println("ERROR Exception in servlet message_process do_close update call record:=" + e);
            }
            
            if (send_redirect) {
                response.sendRedirect("message_list.jsp?status=new");
            }
        }
        catch(Exception e)
        {
            System.out.println("Error in message_process servlet redirect on do_close action=" + e);
        }        
    } 
    protected void do_spam(HttpServletRequest request, HttpServletResponse response, String message_id, boolean send_redirect)
    {
        HttpSession session = request.getSession();   
        try
        {
            if (message_id == null || message_id.equals("")) {
                throw new Exception("message_id is empty");
            }
            String start_working_on_message_time = request.getParameter("start_working_on_message_time");  //timestamp format
            if (start_working_on_message_time == null) {
                start_working_on_message_time = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
            }
            String context_dir = request.getServletContext().getRealPath("");
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String message_log = request.getParameter("log");
            //update the message status=closed status_date=utc_now  status_by_user_id  related_to=incident  related_to_id=incident_id
            
            //process the message
            try
            {
                String query = "UPDATE message SET `status`=?,`status_date`=?, `status_by_user_id`=?,`log`=?,`related_to`=?,`related_to_id`=? WHERE `id`=?";
                PreparedStatement stmt = con.prepareStatement(query);
                stmt.setString(1, "Closed");
                stmt.setString(2, DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc()));
                stmt.setString(3, session.getAttribute("user_id").toString());
                message_log = message_log + " Message Closed at " + DateTimeFormatter.ofPattern("HH:mm MM/dd/yyyy").format(support.date_utils.now_in_utc()) + ".";
                message_log = message_log + " Closed By:" + session.getAttribute("username").toString();
                message_log = message_log + " Marked as Spam.";
                stmt.setString(4, message_log); 
                stmt.setString(5, "spam");
                stmt.setString(6, null);
                stmt.setString(7, message_id); 
                stmt.executeUpdate(); 
            }
            catch(Exception e)
            {
                System.out.println("ERROR Exception in servlet message_process do_spam:=" + e);
            }
            //process the call
            String message_info[] = db.get_message.message_by_id(con, message_id);
            String call_info[] = db.get_call.for_interaction_id(con, message_info[1]);
            try
            {
                //# id	media_type	create_time	     accept_time	accept_time_seconds	abandon_time	abandon_time_seconds	call_duration_seconds	channel	direction	agent_name	post_process_time	process_time	total_time	interaction_id
                //22526	Email	        2021-05-01 21:54:42						                                                                Email	0					                         2e55a5dd-421f-4a74-8d38-74ecd31331f3

                //field needing to update
                //accept_time
                String accept_time = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
                //accept_time_seconds  (accept_time - create_time) /1000 <Sec to Seconds
                java.util.Date aTime = timestamp_format.parse(accept_time);
                java.util.Date cTime = timestamp_format.parse(call_info[2]);
                String accept_time_seconds = String.valueOf((aTime.getTime() - cTime.getTime()) / 1000);
                //call_duration_seconds  
                java.util.Date now_in_utc = Date.from(java.time.ZonedDateTime.now().toInstant());
                String call_duration_seconds = String.valueOf((now_in_utc.getTime() - cTime.getTime()) / 1000);
                //agent_name
                String agent_name = session.getAttribute("username").toString();
                //post_process_time = 0
                String post_process_time = "0";
                //process_time   //now - start working on time / 1000
                java.util.Date sTime = timestamp_format.parse(start_working_on_message_time);
                String process_time = String.valueOf((now_in_utc.getTime() - sTime.getTime()) / 1000);
                //total_time
                String total_time = String.valueOf((now_in_utc.getTime() - cTime.getTime()) / 1000);
                //System.out.println("create Time=" + cTime);
                //System.out.println("accept Time=" + aTime);
                //System.out.println("Now    Time=" + now_in_utc);
                //System.out.println("start wTime=" + sTime);
                String update_call = "UPDATE `calls` SET "
                        + "`accept_time`=?,"
                        + "`accept_time_seconds` = ?,"
                        + "`call_duration_seconds` = ?,"
                        + "`agent_name` = ?,"
                        + "`post_process_time` = ?," 
                        + "`process_time` = ?," 
                        + "`total_time` = ?" 
                        + " WHERE `interaction_id` = ?";                
                PreparedStatement stmt = con.prepareStatement(update_call);
                stmt.setString(1, accept_time);
                stmt.setString(2, accept_time_seconds);
                stmt.setString(3, call_duration_seconds);                
                stmt.setString(4, agent_name); 
                stmt.setString(5, post_process_time);
                stmt.setString(6, process_time);
                stmt.setString(7, total_time); 
                stmt.setString(8, message_info[1]);  //message_uuid
                //System.out.println("stmt=" + stmt);
                stmt.executeUpdate(); 
            }
            catch(Exception e)
            {
                System.out.println("ERROR Exception in servlet message_process do_spam update call record:=" + e);
            }
            
            if (send_redirect) {
                response.sendRedirect("message_list.jsp?status=new");
            }
            
        }
        catch(Exception e)
        {
            System.out.println("Error in message_process servlet redirect on do_spam action=" + e);
        }        
    } 
    protected void do_update(HttpServletRequest request, HttpServletResponse response,String message_id)
    {
        HttpSession session = request.getSession();   
        try
        {
            String start_working_on_message_time = request.getParameter("start_working_on_message_time");
            String context_dir = request.getServletContext().getRealPath("");
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String status = request.getParameter("status");
            
            String message_log = request.getParameter("log");
            message_log = StringEscapeUtils.escapeHtml4(message_log); //turn it back to html
            
            //update the message status=closed status_date=utc_now  status_by_user_id  related_to=incident  related_to_id=incident_id
            //String query = "UPDATE message SET `status`=?,`status_date`=?, `status_by_user_id`=?,`log`=? WHERE `id`=?";
            String query = "UPDATE message SET `status`=?,`status_date`=?, `status_by_user_id`=?,`log`=?,`assigned_to_id`=?,`date_assigned`=?,`date_closed`=? WHERE `id`=?";
            String assigned_to_id = null;
            try
            {
                assigned_to_id = request.getParameter("assigned_to_id");
                if(assigned_to_id.equalsIgnoreCase(""))
                {
                    assigned_to_id = null;
                }
            }
            catch(Exception e)
            {
                assigned_to_id = null;
            }
            String date_assigned = null;
            java.util.Date temp_date_assigned = new java.util.Date();
            try
            {
                date_assigned = request.getParameter("date_assigned");
                if(date_assigned != null)
                {
                    temp_date_assigned = date_time_picker_format.parse(date_assigned);
                    date_assigned = timestamp_format.format(temp_date_assigned);
                }
            }
            catch(Exception e)
            {
                date_assigned = null;
            }
            String date_closed = null;
            java.util.Date temp_date_closed = new java.util.Date();
            try
            {
                date_closed = request.getParameter("date_closed");
                if(date_closed != null)
                {
                    temp_date_closed = date_time_picker_format.parse(date_closed);
                    date_closed = timestamp_format.format(temp_date_closed);
                }
            }
            catch(Exception e)
            {
                date_closed = null;
            }
            try
            {
                PreparedStatement stmt = con.prepareStatement(query);
                stmt.setString(1, status);
                stmt.setString(2, DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc()));
                stmt.setString(3, session.getAttribute("user_id").toString());
                message_log = message_log + " Message Updated at " + DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc()) + ".<br>";
                message_log = message_log + " Updated By:" + session.getAttribute("username").toString() + "<br>";
                stmt.setString(4, StringEscapeUtils.escapeHtml4(message_log)); 
                stmt.setString(5, assigned_to_id);
                stmt.setString(6, date_assigned);
                stmt.setString(7, date_closed);
                stmt.setString(8, message_id); 
                System.out.println("q=" + stmt);
                stmt.executeUpdate(); 
            }
            catch(Exception e)
            {
                System.out.println("ERROR Exception in servlet message_process update message:=" + e);
            }
            //process the call
            String message_info[] = db.get_message.message_by_id(con, message_id);
            String call_info[] = db.get_call.for_interaction_id(con, message_info[1]);
            try
            {
                //# id	media_type	create_time	     accept_time	accept_time_seconds	abandon_time	abandon_time_seconds	call_duration_seconds	channel	direction	agent_name	post_process_time	process_time	total_time	interaction_id
                //22526	Email	        2021-05-01 21:54:42						                                                                Email	0					                         2e55a5dd-421f-4a74-8d38-74ecd31331f3

                //field needing to update
                //accept_time
                String accept_time = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
                //accept_time_seconds  (accept_time - create_time) /1000 <Sec to Seconds
                java.util.Date aTime = timestamp_format.parse(accept_time);
                java.util.Date cTime = timestamp_format.parse(call_info[2]);
                String accept_time_seconds = String.valueOf((aTime.getTime() - cTime.getTime()) / 1000);
                //call_duration_seconds  
                java.util.Date now_in_utc = Date.from(java.time.ZonedDateTime.now().toInstant());
                String call_duration_seconds = String.valueOf((now_in_utc.getTime() - cTime.getTime()) / 1000);
                //agent_name
                String agent_name = session.getAttribute("username").toString();
                //post_process_time = 0
                String post_process_time = "0";
                //process_time   //now - start working on time / 1000
                java.util.Date sTime = timestamp_format.parse(start_working_on_message_time);
                String process_time = String.valueOf((now_in_utc.getTime() - sTime.getTime()) / 1000);
                //total_time
                String total_time = String.valueOf((now_in_utc.getTime() - cTime.getTime()) / 1000);
                //System.out.println("create Time=" + cTime);
                //System.out.println("accept Time=" + aTime);
                //System.out.println("Now    Time=" + now_in_utc);
                //System.out.println("start wTime=" + sTime);
                String update_call = "UPDATE `calls` SET "
                        + "`accept_time`=?,"
                        + "`accept_time_seconds` = ?,"
                        + "`call_duration_seconds` = ?,"
                        + "`agent_name` = ?,"
                        + "`post_process_time` = ?," 
                        + "`process_time` = ?," 
                        + "`total_time` = ?" 
                        + " WHERE `interaction_id` = ?";                
                PreparedStatement stmt = con.prepareStatement(update_call);
                stmt.setString(1, accept_time);
                stmt.setString(2, accept_time_seconds);
                stmt.setString(3, call_duration_seconds);                
                stmt.setString(4, agent_name); 
                stmt.setString(5, post_process_time);
                stmt.setString(6, process_time);
                stmt.setString(7, total_time); 
                stmt.setString(8, message_info[1]);  //message_uuid
                //System.out.println("stmt=" + stmt);
                stmt.executeUpdate(); 
            }
            catch(Exception e)
            {
                System.out.println("ERROR Exception in servlet message_process update call:=" + e);
            }
            response.sendRedirect("message_list.jsp?status=new");
        }
        catch(Exception e)
        {
            System.out.println("Error in message_process servlet redirect on do_update action=" + e);
        }
    } 
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

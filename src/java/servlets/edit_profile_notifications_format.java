/*
//Copyright 2020 XaSystems, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author rcampbell
 */
public class edit_profile_notifications_format extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            String user_id = session.getAttribute("user_id").toString();
            
            String incident_create_subject = StringEscapeUtils.escapeHtml4(request.getParameter("incident_create_subject"));
            String incident_create_body = StringEscapeUtils.escapeHtml4(request.getParameter("incident_create_body"));
            String incident_update_subject = StringEscapeUtils.escapeHtml4(request.getParameter("incident_update_subject"));
            String incident_update_body = StringEscapeUtils.escapeHtml4(request.getParameter("incident_update_body"));
            String incident_resolve_subject = StringEscapeUtils.escapeHtml4(request.getParameter("incident_resolve_subject"));
            String incident_resolve_body = StringEscapeUtils.escapeHtml4(request.getParameter("incident_resolve_body"));
            String incident_close_subject = StringEscapeUtils.escapeHtml4(request.getParameter("incident_close_subject"));
            String incident_close_body = StringEscapeUtils.escapeHtml4(request.getParameter("incident_close_body"));
            String request_create_subject = StringEscapeUtils.escapeHtml4(request.getParameter("request_create_subject"));
            String request_create_body = StringEscapeUtils.escapeHtml4(request.getParameter("request_create_body"));
            String request_update_subject = StringEscapeUtils.escapeHtml4(request.getParameter("request_update_subject"));
            String request_update_body = StringEscapeUtils.escapeHtml4(request.getParameter("request_update_body"));
            String request_resolve_subject = StringEscapeUtils.escapeHtml4(request.getParameter("request_resolve_subject"));
            String request_resolve_body = StringEscapeUtils.escapeHtml4(request.getParameter("request_resolve_body"));
            String request_close_subject = StringEscapeUtils.escapeHtml4(request.getParameter("request_close_subject"));
            String request_close_body = StringEscapeUtils.escapeHtml4(request.getParameter("request_close_body"));
            String task_create_subject = StringEscapeUtils.escapeHtml4(request.getParameter("task_create_subject"));
            String task_create_body = StringEscapeUtils.escapeHtml4(request.getParameter("task_create_body"));
            String task_update_subject = StringEscapeUtils.escapeHtml4(request.getParameter("task_update_subject"));
            String task_update_body = StringEscapeUtils.escapeHtml4(request.getParameter("task_update_body"));
            String task_resolve_subject = StringEscapeUtils.escapeHtml4(request.getParameter("task_resolve_subject"));
            String task_resolve_body = StringEscapeUtils.escapeHtml4(request.getParameter("task_resolve_body"));
            String task_close_subject = StringEscapeUtils.escapeHtml4(request.getParameter("task_close_subject"));
            String task_close_body = StringEscapeUtils.escapeHtml4(request.getParameter("task_close_body"));
            String job_create_subject = StringEscapeUtils.escapeHtml4(request.getParameter("job_create_subject"));
            String job_create_body = StringEscapeUtils.escapeHtml4(request.getParameter("job_create_body"));
            String job_update_subject = StringEscapeUtils.escapeHtml4(request.getParameter("job_update_subject"));
            String job_update_body = StringEscapeUtils.escapeHtml4(request.getParameter("job_update_body"));
            String job_resolve_subject = StringEscapeUtils.escapeHtml4(request.getParameter("job_resolve_subject"));
            String job_resolve_body = StringEscapeUtils.escapeHtml4(request.getParameter("job_resolve_body"));
            String job_close_subject = StringEscapeUtils.escapeHtml4(request.getParameter("job_close_subject"));
            String job_close_body = StringEscapeUtils.escapeHtml4(request.getParameter("job_close_body"));
            
            String query = "UPDATE notifications_user SET subject=?,body=? WHERE user_id=? AND notify_on=?";
            
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement(query);
                
                stmt.setString(1, incident_create_subject);
                stmt.setString(2, incident_create_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "incident_create_assigned_to_me");
                stmt.execute();
                stmt.setString(1, incident_create_subject);
                stmt.setString(2, incident_create_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "incident_create_assigned_to_group");
                stmt.execute();
                
                stmt.setString(1, incident_update_subject);
                stmt.setString(2, incident_update_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "incident_update_assigned_to_me");
                stmt.execute();
                stmt.setString(1, incident_update_subject);
                stmt.setString(2, incident_update_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "incident_update_assigned_to_group");
                stmt.execute();
                
                stmt.setString(1, incident_resolve_subject);
                stmt.setString(2, incident_resolve_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "incident_resolve_assigned_to_me");
                stmt.execute();
                stmt.setString(1, incident_resolve_subject);
                stmt.setString(2, incident_resolve_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "incident_resolve_assigned_to_group");
                stmt.execute();
                
                stmt.setString(1, incident_close_subject);
                stmt.setString(2, incident_close_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "incident_close_assigned_to_me");
                stmt.execute();
                stmt.setString(1, incident_resolve_subject);
                stmt.setString(2, incident_resolve_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "incident_close_assigned_to_group");
                stmt.execute();
                ////////////////////////////////////////////////////////////////////
                stmt.setString(1, request_create_subject);
                stmt.setString(2, request_create_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "request_create_assigned_to_me");
                stmt.execute();
                stmt.setString(1, request_create_subject);
                stmt.setString(2, request_create_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "request_create_assigned_to_group");
                stmt.execute();
                
                stmt.setString(1, request_update_subject);
                stmt.setString(2, request_update_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "request_update_assigned_to_me");
                stmt.execute();
                stmt.setString(1, request_update_subject);
                stmt.setString(2, request_update_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "request_update_assigned_to_group");
                stmt.execute();
                
                stmt.setString(1, request_resolve_subject);
                stmt.setString(2, request_resolve_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "request_resolve_assigned_to_me");
                stmt.execute();
                stmt.setString(1, request_resolve_subject);
                stmt.setString(2, request_resolve_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "request_resolve_assigned_to_group");
                stmt.execute();
                
                stmt.setString(1, request_close_subject);
                stmt.setString(2, request_close_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "request_close_assigned_to_me");
                stmt.execute();
                stmt.setString(1, request_resolve_subject);
                stmt.setString(2, request_resolve_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "request_close_assigned_to_group");
                stmt.execute();
                ////////////////////////////////////////////////////////////////////
                stmt.setString(1, job_create_subject);
                stmt.setString(2, job_create_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "job_create_assigned_to_me");
                stmt.execute();
                stmt.setString(1, job_create_subject);
                stmt.setString(2, job_create_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "job_create_assigned_to_group");
                stmt.execute();
                
                stmt.setString(1, job_update_subject);
                stmt.setString(2, job_update_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "job_update_assigned_to_me");
                stmt.execute();
                stmt.setString(1, job_update_subject);
                stmt.setString(2, job_update_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "job_update_assigned_to_group");
                stmt.execute();
                
                stmt.setString(1, job_close_subject);
                stmt.setString(2, job_close_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "job_close_assigned_to_me");
                stmt.execute();
                stmt.setString(1, job_resolve_subject);
                stmt.setString(2, job_resolve_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "job_close_assigned_to_group");
                stmt.execute();
                ////////////////////////////////////////////////////////////////////
                stmt.setString(1, task_create_subject);
                stmt.setString(2, task_create_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "task_create_assigned_to_me");
                stmt.execute();
                stmt.setString(1, task_create_subject);
                stmt.setString(2, task_create_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "task_create_assigned_to_group");
                stmt.execute();
                
                stmt.setString(1, task_update_subject);
                stmt.setString(2, task_update_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "task_update_assigned_to_me");
                stmt.execute();
                stmt.setString(1, task_update_subject);
                stmt.setString(2, task_update_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "task_update_assigned_to_group");
                stmt.execute();
                
                stmt.setString(1, task_close_subject);
                stmt.setString(2, task_close_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "task_close_assigned_to_me");
                stmt.execute();
                stmt.setString(1, task_resolve_subject);
                stmt.setString(2, task_resolve_body);
                stmt.setString(3, user_id);
                stmt.setString(4, "task_close_assigned_to_group");
                stmt.execute();
                
                stmt.close();
                con.close();
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet edit_profile_notifications_format:=" + e);
            }
        }
        response.sendRedirect("edit_profile.jsp?success=true");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

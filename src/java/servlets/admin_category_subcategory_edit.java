/*
 * Copyright 2019 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class admin_category_subcategory_edit extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String query = "";
        String category_id = "";
        String category_active = "";
        String category_name = "";
        try
        {
            HttpSession session = request.getSession();
            String context_dir = request.getServletContext().getRealPath("");
            category_id = request.getParameter("category_id");
            if (session.getAttribute("authenticated") == null) 
            {
                response.sendRedirect("index.jsp");
            }
            else
            {
                //do work here
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                //update the category/active
                category_active = request.getParameter("category_active");
                if(category_active == null || category_active.equalsIgnoreCase(""))
                {
                    category_active = "false";
                }
                else
                {
                    category_active = "true";
                }
                category_name = request.getParameter("category_name");
                query = "UPDATE `category` SET `name`='" + category_name + "',`active`='" + category_active + "' WHERE `id`='" + category_id + "'";
                

                PreparedStatement stmt = con.prepareStatement(query);
                //stmt.setString(1,category_name);
                //stmt.setString(2,category_active);
                //stmt.setString(3,category_id);
                //System.out.println("stmt=" + stmt);
                stmt.executeUpdate();                   
                
                //delete all the old subcategories
                query = "DELETE FROM subcategory WHERE category_id=?";
                stmt = con.prepareStatement(query);
                stmt.setString(1,category_id);                
                stmt.executeUpdate(); 
                //System.out.println("DELETE subs");
                //
                //add all the subcategories
                query = "INSERT INTO subcategory (category_id,name,active) VALUES (?,?,?)"; 
                stmt = con.prepareStatement(query);
                //get all the request parameters
                Map params = request.getParameterMap();
                Iterator i = params.keySet().iterator();
                while (i.hasNext())
                {
                    String name = (String) i.next();
                    String value = ((String[]) params.get( name ))[ 0 ];
                    //System.out.println("name=" + name + " value=" + value);
                    if(name.startsWith("subcategory_id_"))
                    {
                        //get name
                        String subcategory_name = request.getParameter("subcategory_name_" + value);
                        //get active
                        String subcategory_active = request.getParameter("subcategory_active_" + value);
                        try
                        {
                            subcategory_active = request.getParameter("subcategory_active_" + value);
                            if(subcategory_active == null || subcategory_active.equalsIgnoreCase(""))
                            {
                                subcategory_active = "false";
                            }
                            else
                            {
                                subcategory_active = "true";
                            }
                        }
                        catch(Exception e)
                        {
                            subcategory_active = "false";
                        }
                        //insert the subcategory
                        stmt.setString(1,category_id);
                        stmt.setString(2,subcategory_name);
                        stmt.setString(3,subcategory_active);
                        stmt.executeUpdate();   
                    }
                }         
                //stmt.close();
                con.close();
            }            
        }
        catch (Exception e) 
        {
            logger.error("ERROR Exception in servlet admin_category_subcategory_add_subcategory:=" + e);
        }
        response.sendRedirect("admin_category_subcategory_edit.jsp?category_id=" + category_id);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

//Copyright 2021 XaSystems, Inc. , All rights reserved.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.Tika;
import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;

@MultipartConfig()
/**
 *
 * @author server-xc6701
 */
public class admin_users_add extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        CouchDbClient dbClient = null;
        
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            String contract_id = session.getAttribute("db").toString();
            String installation_type = props.get("installation_type").toString().trim();
            JsonObjectBuilder json_builder = Json.createObjectBuilder();
            JsonObject json;

            try (PrintWriter out = response.getWriter()) {

                //do work here
                try 
                {
                    /*returnString[0] = rs.getString("id");
                    returnString[1] = rs.getString("username");
                    returnString[2] = rs.getString("password");
                    returnString[3] = check_for_null(rs.getString("first"));
                    returnString[4] = check_for_null(rs.getString("mi"));
                    returnString[5] = check_for_null(rs.getString("last"));
                    returnString[6] = check_for_null(rs.getString("address_1"));
                    returnString[7] = check_for_null(rs.getString("address_2"));
                    returnString[8] = check_for_null(rs.getString("city"));
                    returnString[9] = check_for_null(rs.getString("state"));
                    returnString[10] = check_for_null(rs.getString("zip"));
                    returnString[11] = check_for_null(rs.getString("location"));
                    returnString[12] = check_for_null(rs.getString("department"));
                    returnString[13] = check_for_null(rs.getString("site"));
                    returnString[14] = check_for_null(rs.getString("company"));
                    returnString[15] = check_for_null(rs.getString("email"));
                    returnString[16] = check_for_null(rs.getString("phone_office"));
                    returnString[17] = check_for_null(rs.getString("phone_mobile"));
                    returnString[18] = check_for_null(rs.getString("notes"));
                    returnString[19] = check_for_null(rs.getString("vip"));
                    returnString[20] = check_for_null(rs.getString("is_admin"));
                    returnString[21] = check_for_null(rs.getString("tz_name"));
                    returnString[22] = check_for_null(rs.getString("tz_time"));
                    returnString[23] = check_for_null(rs.getString("external_id"));*/                

                    String username = support.data_check.null_string(request.getParameter("username"));
                    String password = support.data_check.null_string(request.getParameter("password"));
                    String first = support.data_check.null_string(request.getParameter("first"));
                    String mi = StringUtils.defaultString(request.getParameter("mi"));
                    String last = support.data_check.null_string(request.getParameter("last"));
                    String address_1 = support.data_check.null_string(request.getParameter("address_1"));
                    String address_2 = support.data_check.null_string(request.getParameter("address_2"));
                    String city = support.data_check.null_string(request.getParameter("city"));
                    String state = support.data_check.null_string(request.getParameter("state"));
                    String zip = support.data_check.null_string(request.getParameter("zip"));
                    String location = support.data_check.null_string(request.getParameter("location"));
                    String department = support.data_check.null_string(request.getParameter("department"));
                    String site = support.data_check.null_string(request.getParameter("site"));
                    String company = support.data_check.null_string(request.getParameter("company"));
                    String email = support.data_check.null_string(request.getParameter("email"));
                    String phone_office = support.data_check.null_string(request.getParameter("phone_office"));
                    String phone_mobile = support.data_check.null_string(request.getParameter("phone_mobile"));
                    String tz[] = support.data_check.null_string(request.getParameter("tz")).split(",");
                    String tz_name = tz[0];
                    String tz_time = tz[1];
                    String notes = support.data_check.null_string(request.getParameter("notes"));                
                    String vip = "false";
                    String new_user_id = "";
                    ArrayList<byte[]> salt_and_hash = new ArrayList(Arrays.asList(null, null));

                    if(request.getParameter("vip") == null)
                    {
                        //checkbox not checked
                        vip = "false";
                    }
                    else
                    {
                        //checkbox checked
                        vip = "true";
                    }
                    String is_admin = "false";
                    if(request.getParameter("is_admin") == null)
                    {
                        //checkbox not checked
                        is_admin = "false";
                    }
                    else
                    {
                        //checkbox checked
                        is_admin = "true";
                    }
                    String external_id = request.getParameter("external_id");                
    //                password = support.encrypt_utils.encrypt(context_dir, password); 

                    if (!password.equals("")) {
                        salt_and_hash = support.encrypt_utils.get_hash(password);
                    }




                    Connection con = db.db_util.get_contract_connection(context_dir, session);                
                    //does this username exist
                    boolean user_exists = true;
                    PreparedStatement stmt = con.prepareStatement("SELECT COUNT(*) AS count FROM users WHERE username=?");
                    try
                    {
                        //if username exists
                        stmt.setString(1, username);
                        ResultSet rs = stmt.executeQuery();
                        while(rs.next())
                        {
                            int count = rs.getInt("count");
                            if(count > 0)
                            {
                                user_exists = true;
                            }
                            else
                            {
                                user_exists = false;
                            }
                        }
                    }
                    catch(Exception e)
                    {
                        user_exists = true;
                    }

                    if(!user_exists)
                    {
                        stmt = con.prepareStatement("INSERT INTO users (username,password,first,mi,last,address_1,address_2,city,state,zip,location,department,site,company,email,phone_office,phone_mobile,notes,vip,is_admin,tz_name,tz_time,external_id,password_salt,password_hash) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                        stmt.setString(1,username);
                        stmt.setString(2,password);
                        stmt.setString(3,first);
                        stmt.setString(4, (mi.length() > 0 ? mi.substring(0, 1) : ""));
                        stmt.setString(5,last);
                        stmt.setString(6,address_1);
                        stmt.setString(7,address_2);
                        stmt.setString(8,city);
                        stmt.setString(9,state);
                        stmt.setString(10,zip);
                        stmt.setString(11,location);
                        stmt.setString(12,department);
                        stmt.setString(13,site);
                        stmt.setString(14,company);
                        stmt.setString(15,email);
                        stmt.setString(16,phone_office);
                        stmt.setString(17,phone_mobile);
                        stmt.setString(18,notes);
                        stmt.setString(19,vip);
                        stmt.setString(20,is_admin);
                        stmt.setString(21,tz_name);
                        stmt.setString(22,tz_time);
                        stmt.setString(23,external_id);
                        stmt.setBytes(24,salt_and_hash.get(0));
                        stmt.setBytes(25,salt_and_hash.get(1));
                        stmt.executeUpdate();
                        //get the user id for this new user
                        String new_user_info[] = db.get_users.by_username(con, username);
                        new_user_id = new_user_info[0];

                        //start get the file attachment if there is one
                        Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
                        if (filePart != null)
                        {
                            try
                            {
                                dbClient = db.couchdb.get_avatars_client(props);
                                if (dbClient == null)
                                {
                                    throw new Exception("dbClient is null");
                                }
                                String file_name;
                                Tika tika = new Tika();

            //                    logger.info("filepart:" + filePart);
                                String fileName = filePart.getSubmittedFileName();
                                if (fileName != null && !"".equals(fileName)) 
                                {
                                    long file_size = filePart.getSize();
                                    String mimeType = tika.detect(filePart.getInputStream());
                                    file_name = "avatar";

                                    UUID Uuid = UUID.randomUUID(); //Generates random UUID                                  
                                    String uuid = Uuid.toString();

                                    Map<String, Object> map = new HashMap<>(); 
                                    java.util.Date now = new java.util.Date();
                                    String user_id = session.getAttribute("user_id").toString();

                                    map.put("_id", uuid);
                                    map.put("Filename", file_name);
                                    map.put("MIME_type", mimeType);
                                    map.put("FileSize", file_size);
                                    map.put("DateUploaded", now);
                                    map.put("UploadedBy", user_id);

                                    Response dbclient_response = dbClient.save(map);

                                    dbclient_response = dbClient.saveAttachment(filePart.getInputStream(), file_name, mimeType, dbclient_response.getId(), dbclient_response.getRev());

                                    if (dbclient_response.getError() == null)
                                    {
                                        Statement stmt_attachment  = con.createStatement();
                                        int rows_affected = stmt_attachment.executeUpdate("UPDATE users SET avatar = '" + uuid + "' WHERE id = '" + new_user_id + "'");
                                        if (rows_affected <= 0)
                                        {
                                            throw new Exception("Avatar field is somehow not updated in the users table");
                                        }
                                    } else {
                                        logger.error("CouchDB attachment upload error: {}", dbclient_response.getError());
                                    }


                                }
                            } catch (Exception e)
                            {
                                logger.error("ERROR attachments process in servlet admin_users_add:=" + e);
                            }

                        }
                        //do roles now
                        ArrayList <String> role_ids = new ArrayList();
                        Set<String> paramNames = request.getParameterMap().keySet();
                        // iterating over parameter names and get its value
                        for (String name : paramNames) 
                        {
                            String value = request.getParameter(name);
                            if(name.startsWith("role_id_"))
                            {
                                role_ids.add(value);
                            }
                        }
                        boolean shared = false;
                        if(installation_type.equalsIgnoreCase("shared"))
                        {
                            shared = true;
                        }
                        String shared_key = props.get("shared_key").toString().trim(); 
                        String shared_user = props.get("shared_user").toString().trim(); 
                        boolean needs_login = false;
                        stmt = con.prepareStatement("INSERT INTO user_roles (user_id,role_id) VALUES (?,?)");
                        for(int a = 0; a < role_ids.size(); a++)
                        {
                            //if the role is 0-3 or contains self_service
                            if(role_ids.get(a).equalsIgnoreCase("0") || role_ids.get(a).equalsIgnoreCase("1") || role_ids.get(a).equalsIgnoreCase("2") || role_ids.get(a).equalsIgnoreCase("3"))
                            {
                                needs_login = true;
                            }
                            else
                            {
                                String role_info[] = db.get_roles.by_role_id(con, role_ids.get(a));
                                //if the role has any permission other than none they need a login
                                if(!role_info[19].equalsIgnoreCase("none"))
                                {
                                    needs_login = true;
                                }
                            }
                            stmt.setString(1,new_user_id);
                            stmt.setString(2,role_ids.get(a));
                            stmt.executeUpdate();
                        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                
                        //if a shared installation then update the user table on he management server
                        if(shared)
                        {
                            if(needs_login)
                            {
                                //call the custom REST API
                                String query = "INSERT INTO `users`" 
                                        + "(`id`,"
                                        + "`contract_id`," 
                                        + "`username`," 
                                        + "`password`," 
                                        + "`first`," 
                                        + "`mi`," 
                                        + "`last`," 
                                        + "`address_1`," 
                                        + "`address_2`,"
                                        + "`city`," 
                                        + "`state`," 
                                        + "`zip`," 
                                        + "`email`," 
                                        + "`phone_office`," 
                                        + "`phone_mobile`," 
                                        + "`notes`"
                                        + ") " 
                                        + "VALUES " 
                                        + "('" + new_user_id + "'," 
                                        + "'" + contract_id + "'," 
                                        + "'" + username + "'," 
                                        + "''," 
                                        + "'" + first + "'," 
                                        + "'" + mi + "'," 
                                        + "'" + last + "'," 
                                        + "'" + address_1 + "'," 
                                        + "'" + address_2 + "'," 
                                        + "'" + city + "'," 
                                        + "'" + state + "'," 
                                        + "'" + zip + "'," 
                                        + "'" + email + "'," 
                                        + "'" + phone_office + "'," 
                                        + "'" + phone_mobile + "',"
                                        + "'" + notes + "'"
                                        + ")";
                                boolean insert_success = db.get_management_server.run_insert(context_dir, query);
    // seems password_hash and salt doesn't need to be sync to the management server
    //                            if (insert_success && !password.equals("")) {
    //                                query = "UPDATE `users` SET password_salt = UNHEX('" + Hex.encodeHexString(salt_and_hash.get(0)) +"'), password_hash = UNHEX('" + Hex.encodeHexString(salt_and_hash.get(1)) + "') "
    //                                        + "WHERE `id` = '" + new_user_id + "'"; 
    //                                boolean update_success = db.get_management_server.run_insert(context_dir, query);
    //                            }

                            } 
                        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                 
                        //do group membership
                        try
                        {
                            String groups[] = request.getParameterValues("group_ids");
                            if(groups != null)
                            {
                                stmt = con.prepareStatement("INSERT INTO users_groups (user_id,group_id,role,external_id) VALUES (?,?,?,?)");
                                for(int a = 0; a < groups.length; a++)
                                {
                                    stmt.setString(1,new_user_id);
                                    stmt.setString(2,groups[a]);
                                    stmt.setString(3,"");
                                    stmt.setString(4,"");                    
                                    stmt.executeUpdate();
                                }    
                            }
                        }
                        catch(Exception e)
                        {
                            System.out.println("ERROR Exception in servlet admin_users_add do group membership:=" + e);
                        }
                        //create all the notification table stuff
                        String notify_on[] = {"incident_create_assigned_to_me",
                            "incident_create_assigned_to_group",
                            "incident_update_assigned_to_me",
                            "incident_update_assigned_to_group",
                            "incident_resolve_assigned_to_me",
                            "incident_resolve_assigned_to_group",
                            "incident_close_assigned_to_me",
                            "incident_close_assigned_to_group",
                            "request_create_assigned_to_me",
                            "request_create_assigned_to_group",
                            "request_update_assigned_to_me",
                            "request_update_assigned_to_group",
                            "request_resolve_assigned_to_me",
                            "request_resolve_assigned_to_group",
                            "request_close_assigned_to_me",
                            "request_close_assigned_to_group",
                            "job_create_assigned_to_me",
                            "job_create_assigned_to_group",
                            "job_update_assigned_to_me",
                            "job_update_assigned_to_group",
                            "job_close_assigned_to_me",
                            "job_close_assigned_to_group",
                            "task_create_assigned_to_me",
                            "task_create_assigned_to_group",
                            "task_update_assigned_to_me",
                            "task_update_assigned_to_group",
                            "task_close_assigned_to_me",
                            "task_close_assigned_to_group"                   
                        };
                        try
                        {
                            stmt = con.prepareStatement("INSERT INTO notifications_user (user_id,notify_on,active,subject,body) VALUES (?,?,?,?,?)");
                            for(int a = 0; a < notify_on.length; a++)
                            {
                                stmt.setString(1,new_user_id);
                                stmt.setString(2,notify_on[a]);
                                stmt.setString(3,"false");
                                stmt.setString(4,""); 
                                stmt.setString(5,"");
                                stmt.executeUpdate();
                            }  
                        }
                        catch(Exception e)
                        {
                            System.out.println("ERROR Exception in servlet admin_users_add do notifications:=" + e);
                        }
                        stmt.close();
                        con.close();
                        session.setAttribute("alert", "User successfully created");
                        session.setAttribute("alert-class", "alert-success");
                        json_builder.add("error", "");
                        json_builder.add("user_id", new_user_id);
                        json_builder.add("user_name", username);
                        json = json_builder.build();
                        out.print(json.toString());
//                        System.out.println(json.toString());
//                        response.sendRedirect("admin_users.jsp");
                    }
                    else
                    {
                        //user exists so send the back to admin_users_add
                        Map params = request.getParameterMap();
                        Iterator i = params.keySet().iterator();

                        while ( i.hasNext() )
                        {
                          String key = (String) i.next();
                          String value = ((String[]) params.get( key ))[ 0 ];
                        }


    //                    response.sendRedirect("admin_users_add.jsp?error=user_exists&username=" + username);
                        session.setAttribute("alert", "User already exists");
                        session.setAttribute("alert-class", "alert-dangerNew");
    //                    response.sendRedirect("admin_users.jsp");
    //                    logger.info("success {}", success);
                        json_builder.add("error", "User already exists");
                        json_builder.add("user_id", "");
                        json = json_builder.build();
                        out.print(json.toString());

                    }


                }
                catch (Exception e) 
                {
                    logger.error("ERROR Exception in servlet admin_users_add:=" + e);
    //                e.printStackTrace();
                    session.setAttribute("alert", "Error while creating user");
                    session.setAttribute("alert-class", "alert-dangerNew");
                    json_builder.add("error", e.getMessage());
                    json_builder.add("user_id", "");
                    json = json_builder.build();
                    //send the JSON data
                    out.print(json.toString());

    //                response.sendRedirect("admin_users.jsp");
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

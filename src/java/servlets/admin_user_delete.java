/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 * 
 * 
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author server-xc6701
 */
public class admin_user_delete extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            LinkedHashMap props = support.config.get_config(context_dir);
            String installation_type = props.get("installation_type").toString().trim();
            String contract_id = session.getAttribute("db").toString();
            String id = request.getParameter("id");
            try 
            {
                ArrayList <String[]> user_roles = db.get_roles.roles_for_user_id(con, id);
                boolean has_login = db.get_roles.has_login(user_roles);
                
                //delete user from login server if has_login
                if(has_login && installation_type.equalsIgnoreCase("shared"))
                {
                    String query = "DELETE FROM `users` " 
                                + "WHERE `id`='" + id + "' AND `contract_id`='" + contract_id + "'";
                    boolean delete_success = db.get_management_server.run_insert(context_dir, query);
                    System.out.println("delete_success=" + delete_success); 
                }                
                PreparedStatement stmt = con.prepareStatement("DELETE FROM users WHERE id=?");
                stmt.setString(1,id);
                stmt.executeUpdate();
                
                //delete the users group assignments
                stmt = con.prepareStatement("DELETE FROM user_roles WHERE user_id=?");
                stmt.setString(1,id);
                stmt.executeUpdate();
                
                //delete the users notifications
                stmt = con.prepareStatement("DELETE FROM notifications_user WHERE user_id=?");
                stmt.setString(1,id);
                stmt.executeUpdate();
                
                stmt = con.prepareStatement("DELETE FROM notifications WHERE user_id=?");
                stmt.setString(1,id);
                stmt.executeUpdate();
                
                stmt.close();
                con.close();
                session.setAttribute("alert", "User successfully deleted");
                session.setAttribute("alert-class", "alert-success");
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_user_delete:=" + e);
                session.setAttribute("alert", "User deletion error");
                session.setAttribute("alert-class", "alert-dangerNew");
                            }
        }
        response.sendRedirect("admin_users.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

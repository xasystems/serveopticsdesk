/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class ajax_lookup_assets extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
//        String user_id = request.getParameter("user_id");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

//        String user_id = request.getParameter("user_id");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        JsonObjectBuilder json_builder = Json.createObjectBuilder();
        JsonObject json;
        
        boolean first_record = true;
        boolean has_data = false;
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {

                if (session.getAttribute("authenticated") == null) 
                {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }

                Connection con = db.db_util.get_contract_connection(context_dir, session);
                int starting_record = 0;
                int previous_record = 0;
                int next_record = 0;
                int fetch_size = 50;
                
                try
                {
                    starting_record  = Integer.parseInt(request.getParameter("start"));
                    fetch_size  = Integer.parseInt(request.getParameter("length"));
                    next_record = starting_record + fetch_size;
                    previous_record = starting_record - fetch_size;
                    if(previous_record <= 0)
                    {
                        previous_record = 0;
                    }
                }
                catch(Exception e)
                {
                    starting_record = 0;
                    previous_record = 0;
                    next_record = starting_record + fetch_size;
                }

                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
//                String all_users_info[][] = db.get_users.all_limited(con,starting_record,fetch_size);

                String[] filter_fields = {
                        "serial_number",
                        "asset_tag",
                        "manufacturer",
                        "model",
                        "asset_type",
                        "asset_subtype",
                        "purchase_order",
                        "warranty_expire",
                        "state",
                        "state_date",
                        "assigned_to_id",
                        "assigned_group_id",
                        "department",
                        "location",
                        "site",
                        "company",
                        "ip",
                        "notes"
                };
                List<Map<String, String>> filters = new ArrayList<>();
                for (int i = 0; i < filter_fields.length; i++)
                {
                    String filter_field = filter_fields[i];
                    String filter_value = request.getParameter(filter_field);
                    if (filter_value != null && !filter_value.isEmpty()) 
                    {
                        Map<String, String> map = new HashMap<String, String>();
                        map.put("key", filter_field);
                        map.put("value", filter_value);
                        filters.add(map);
                    }
                }
                
                int total_filtered_count = db.get_assets.all_filtered_count(con,filters);
                ArrayList <String[]> all_assets = db.get_assets.all_limited_filtered(con,starting_record,fetch_size,filters);

                String roles = "";
                JsonArrayBuilder json_array_builder = Json.createArrayBuilder();
                for(int a = 0; a < all_assets.size();a++)
                {

//                this_record[0] = rs.getString("id");
//                this_record[1] = rs.getString("serial_number");
//                this_record[2] = rs.getString("asset_tag");
//                this_record[3] = rs.getString("manufacturer");
//                this_record[4] = rs.getString("model");
//                this_record[5] = rs.getString("asset_type");
//                this_record[6] = rs.getString("asset_subtype");
//                this_record[7] = rs.getString("purchase_order");
//                this_record[8] = rs.getString("warranty_expire");
//                this_record[9] = rs.getString("state");
//                this_record[10] = rs.getString("state_date");
//                this_record[11] = rs.getString("assigned_to_id");
//                this_record[12] = rs.getString("assigned_group_id");
//                this_record[13] = rs.getString("department");
//                this_record[14] = rs.getString("location");
//                this_record[15] = rs.getString("site");
//                this_record[16] = rs.getString("company");
//                this_record[17] = rs.getString("ip");
//                this_record[18] = rs.getString("notes");                
                    String asset_record[] = all_assets.get(a);
                    json_array_builder
                            .add(Json.createObjectBuilder()
                                    .add("id", asset_record[0]) 
                                    .add("serial_number", asset_record[1]) 
                                    .add("asset_tag", asset_record[2]) 
                                    .add("vendor", asset_record[3])
                                    .add("model", asset_record[4])
                                    .add("asset_type", asset_record[5])
                                    .add("asset_subtype", asset_record[6])
                            );
                }
                json_builder.add("recordsTotal", total_filtered_count);
                json_builder.add("recordsFiltered", total_filtered_count);
                json_builder.add("draw", StringUtils.defaultString(request.getParameter("draw")));
                json_builder.add("data", json_array_builder);
                json = json_builder.build();

                con.close();
                //send the JSON data
                out.print(json.toString());
            }
            catch(Exception e)
            {
                logger.error("ERROR Exception in servlet ajax_lookup_assets:=" + e);
                stringBuilder.setLength(0);
                stringBuilder.append("Error in retrieving users list");
                out.print(stringBuilder.toString());

            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

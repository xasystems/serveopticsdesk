/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.Tika;
import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;

@MultipartConfig()
/**
 *
 * @author Ralph
 */
public class job_edit extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
    private static SimpleDateFormat form_format = new SimpleDateFormat("HH:mm / dd-MMM-YY"); //10:42 / 24-Jan-20
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            java.util.Date now = new java.util.Date();
            java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
            
            //job_id
            String job_id = "0";
            try{job_id = request.getParameter("job_id");}catch (Exception e){job_id="0";}            
            //name
            String name = "";
            try{name = request.getParameter("name");}catch (Exception e){name="";}
            //assigned_group_id
            int assigned_group_id = 0;
            try{assigned_group_id = Integer.parseInt(request.getParameter("assigned_group_id"));}catch (Exception e){assigned_group_id=0;}
            //assigned_to_id
            int assigned_to_id = 0;
            try{assigned_to_id = Integer.parseInt(request.getParameter("assigned_to_id"));}catch (Exception e){assigned_to_id=0;}
            //description
            String description = "";
            try{description = request.getParameter("description");}catch (Exception e){description="";}
           
            //scheduled_start_date
            java.sql.Timestamp scheduled_start_date = null;
            try
            {
                scheduled_start_date = new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("scheduled_start_date")).getTime());
            }
            catch(Exception e)
            {
                scheduled_start_date = null;
            }
            //actual_start_date
            java.sql.Timestamp actual_start_date = null;
            try
            {
                actual_start_date = new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("actual_start_date")).getTime());
            }
            catch(Exception e)
            {
                actual_start_date = null;
            }
            //scheduled_end_date
            java.sql.Timestamp scheduled_end_date = null;
            try
            {
                scheduled_end_date = new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("scheduled_end_date")).getTime());
            }
            catch(Exception e)
            {
                scheduled_end_date = null;
            }
            //actual_end_date
            java.sql.Timestamp actual_end_date = null;
            try
            {
                actual_end_date = new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("actual_end_date")).getTime());
            }
            catch(Exception e)
            {
                actual_end_date = null;
            }
            //estimated_duration
            int estimated_duration = 0;
            try{estimated_duration = Integer.parseInt(request.getParameter("estimated_duration"));}catch (Exception e){estimated_duration=0;}
            //actual_duration
            int actual_duration = 0;
            try{actual_duration = Integer.parseInt(request.getParameter("actual_duration"));}catch (Exception e){actual_duration=0;}            
            
            //priority
            String priority = "";
            try{priority = request.getParameter("priority");}catch (Exception e){priority="";}
            //status
            String status = "";
            try{status = request.getParameter("status");}catch (Exception e){status="";}
            //notes
            String notes = "";
            try{notes = request.getParameter("notes");}catch (Exception e){notes="";}
            //do work here
            CouchDbClient dbClient = null;

            try
            {                
                String context_dir = request.getServletContext().getRealPath("");
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                LinkedHashMap props = support.config.get_config(context_dir);
                
                String insert_jobs_table = "REPLACE INTO jobs (id,name,description,assigned_group_id,assigned_to_id,priority,status,scheduled_start_date,actual_start_date,scheduled_end_date,actual_end_date,estimated_duration,actual_duration,notes) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                PreparedStatement stmt = con.prepareStatement(insert_jobs_table);
                stmt.setString(1, job_id); //id
                stmt.setString(2,name); //name
                stmt.setString(3,description);//description
                stmt.setInt(4,assigned_group_id);//owner_group_id
                stmt.setInt(5,assigned_to_id);//owner_id
                stmt.setString(6,priority); //priority
                stmt.setString(7,status); //status
                stmt.setTimestamp(8, scheduled_start_date); //scheduled_start_date
                stmt.setTimestamp(9, actual_start_date); //actual_start_date
                stmt.setTimestamp(10, scheduled_end_date); //scheduled_end_date
                stmt.setTimestamp(11, actual_end_date); //actual_end_date
                stmt.setInt(12,estimated_duration);//estimated_duration
                stmt.setInt(13,actual_duration);//actual_duration       
                stmt.setString(14,notes); //notes                
                stmt.execute();

                try
                {
                    dbClient = db.couchdb.get_client(props);
                    if (dbClient == null)
                    {
                        throw new Exception("dbClient is null");
                    }
                    String user_id = session.getAttribute("user_id").toString();
//                    String config_message_attachment_store = props.get("job_attachment_store").toString();        
//                    String customer_db = session.getAttribute("db").toString();
//                    customer_save_path = config_message_attachment_store + "/" + customer_db;
                    String name_on_disk;
                    String file_name;
//
//                    File customerUploadDir = new File(customer_save_path);
//                    if (!customerUploadDir.exists()) //if the customer attachment dir not exist ctreate it
//                    {
//                        customerUploadDir.mkdir();
//                    }
                    Tika tika = new Tika();

                    
//                    JsonObject couchdb_document = dbClient.find(JsonObject.class, "jobs:" + job_id);
//                    logger.info("couchdb document {}", couchdb_document);

                    for (Part filePart : request.getParts()) {
    //                    logger.info("filepart:" + filePart);
                        String fileName = filePart.getSubmittedFileName();
                        if (fileName != null && !"".equals(fileName)) 
                        {
    //                        logger.info("filename:" + fileName);
                            long file_size = filePart.getSize();
                            String mimeType = tika.detect(filePart.getInputStream());
                            file_name = fileName.replaceAll("[^a-zA-Z0-9\\._]+", "_");
    //                        logger.info("file size" + file_size);
    //                    filePart.write(fileName);
    //                    out.println("... uploaded to: /uploads/" + fileName);

                            UUID Uuid = UUID.randomUUID(); //Generates random UUID                                  
                            String uuid = Uuid.toString();

                            Map<String, Object> map = new HashMap<>(); 
                            map.put("_id", uuid);
                            map.put("Filename", file_name);
                            map.put("MIME_type", mimeType);
                            map.put("FileSize", file_size);
                            map.put("DateUploaded", now);
                            map.put("UploadedBy", user_id);
                            map.put("AssociatedObject", "Job");
                            map.put("AssociatedObjectId", job_id);

                            Response dbclient_response = dbClient.save(map);
                            name_on_disk = uuid + "/"+ file_name; 
 
                            dbclient_response = dbClient.saveAttachment(filePart.getInputStream(), file_name, mimeType, dbclient_response.getId(), dbclient_response.getRev());

                            if (dbclient_response.getError() == null)
                            {
                                String query_attachment = "INSERT INTO job_attachements (`job_id`, `uuid`, `name_on_disk`,`file_name`,`file_date`,`file_size`,`rev`,`uploaded_by`) VALUES (?,?,?,?,?,?,?,?)";
                                PreparedStatement stmt_attachment = con.prepareStatement(query_attachment);
                                stmt_attachment.setInt(1, Integer.parseInt(job_id));
                                stmt_attachment.setString(2, uuid);
                                stmt_attachment.setString(3, name_on_disk);
                                stmt_attachment.setString(4, file_name);
                                Timestamp file_date = new Timestamp(now.getTime());
                                stmt_attachment.setTimestamp(5, file_date);
                                stmt_attachment.setInt(6, Math.toIntExact(file_size));
                                stmt_attachment.setString(7, dbclient_response.getRev());
                                stmt_attachment.setInt(8, Integer.parseInt(user_id));
                                stmt_attachment.execute();
                            } else {
                                logger.error("CouchDB attachment upload error: {}", dbclient_response.getError());
                            }

                        }

                    }
                } catch (Exception e)
                {
                    logger.error("ERROR attachments process in servlet job_update:=" + e);
                }

                try
                {
                    String[] attachments_delete_ids = request.getParameterValues("attachment_delete_id");
                    if (attachments_delete_ids != null && attachments_delete_ids.length > 0)
                    {
                        ArrayList<String[]> attachments = db.get_jobs.get_attachments_by_job_id(con, String.valueOf(job_id));
                        ArrayList <String> deleted_ids = new ArrayList();
                    
                        for (int i = 0; i < attachments.size(); i++)
                        {
                            String[] attachment = attachments.get(i);
                            if (Arrays.stream(attachments_delete_ids).anyMatch(attachment[0]::equals))
                            {
                                try
                                {
                                    dbClient.remove(attachment[2], attachment[7]);
                                    deleted_ids.add(attachment[0]);
                                } catch (Exception e)
                                {
                                }
                            }
                        }
                        if (deleted_ids.size() > 0)
                        {
                            String query_attachment = "DELETE FROM job_attachements WHERE job_id = ? AND id IN (" + String.join(",", deleted_ids) + ")";
                            PreparedStatement stmt_attachment = con.prepareStatement(query_attachment);

                            stmt_attachment.setInt(1, Integer.parseInt(job_id));
                            stmt_attachment.execute();
                        }

                    }
                    
                } catch (Exception e)
                {
                    e.printStackTrace();
                    logger.error("ERROR Exception in attachment deletion servlet job_update:=" + e);
                }



            }
            catch(Exception e)
            {
                logger.error("ERROR Exception in servlet job_edit:=" + e);
            } finally {
                if (dbClient != null )
                {
                    dbClient.shutdown();
                }
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

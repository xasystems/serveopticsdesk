/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.codecs.Codec;
import org.owasp.esapi.codecs.OracleCodec;

@WebServlet("/ajax_lookup_kb")
/**
 *
 * @author ralph
 */
public class ajax_lookup_kb extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
//        String user_id = request.getParameter("user_id");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

//        String user_id = request.getParameter("user_id");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        JsonObjectBuilder json_builder = Json.createObjectBuilder();
        JsonObject json;
        
        boolean first_record = true;
        boolean has_data = false;
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {

                if (session.getAttribute("authenticated") == null) 
                {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }

                Connection con = db.db_util.get_contract_connection(context_dir, session);
                int starting_record = 0;
                int previous_record = 0;
                int next_record = 0;
                int fetch_size = 50;
                
                try
                {
                    starting_record  = Integer.parseInt(request.getParameter("start"));
                    fetch_size  = Integer.parseInt(request.getParameter("length"));
                    next_record = starting_record + fetch_size;
                    previous_record = starting_record - fetch_size;
                    if(previous_record <= 0)
                    {
                        previous_record = 0;
                    }
                }
                catch(Exception e)
                {
                    starting_record = 0;
                    previous_record = 0;
                    next_record = starting_record + fetch_size;
                }

                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
//                String all_users_info[][] = db.get_users.all_limited(con,starting_record,fetch_size);

                String query = "";
                String search = StringUtils.defaultString(request.getParameter("search"));
                String category = StringUtils.defaultString(request.getParameter("category"));
                ESAPI.securityConfiguration().setResourceDirectory(getClass().getResource(".").getPath());
                Codec ORACLE_CODEC = new OracleCodec();

                if (!search.equals(""))
                {
//                    logger.info("SEARCH " + search + " CONTAINS COMMA " + search.contains(","));
                    if (search.contains(",")) {
                        String[] parts = search.split(",");
                        for (String part : parts)
                        {
                            String part_sanitized = ESAPI.encoder().encodeForSQL( ORACLE_CODEC, part);
                            query += (query.equals("") ? "" : " AND ") 
                            + " (knowledge_articles.article_name LIKE '%" + part_sanitized + "%' OR knowledge_articles.article_description LIKE '%" + part_sanitized + "%' OR knowledge_articles.article_text LIKE '%" + part_sanitized + "%') ";
                        }
                    } else {
                        String search_sanitized = ESAPI.encoder().encodeForSQL( ORACLE_CODEC, search);
                        query = " (knowledge_articles.article_name LIKE '%" + search_sanitized + "%' OR knowledge_articles.article_description LIKE '%" + search_sanitized + "%' OR knowledge_articles.article_text LIKE '%" + search_sanitized + "%') ";
                    }
                }

                Map<Integer, String> categories = new HashMap<Integer, String>();
                Map<Integer, Integer> articles_count = new HashMap<Integer, Integer>();
                ArrayList <String[]> all_categories = db.get_kb.categories_filtered(con, query);
                Integer total_counter = 0;
                JsonArrayBuilder json_categories_array_builder = Json.createArrayBuilder();
                categories.put(0, "All");
                for(int a = 0; a < all_categories.size();a++)
                {
                    String category_record[] = all_categories.get(a);
//                    logger.info(Arrays.toString(article_record));
                    int kb_id = NumberUtils.toInt(category_record[0]);
                    total_counter++;
                    if (!articles_count.containsKey(kb_id)) {
                        articles_count.put(kb_id, 1);
                    } else {
                        articles_count.put(kb_id, articles_count.get(kb_id) + 1);
                    }
                    if (!categories.containsKey(kb_id)) {
                        categories.put(kb_id, StringUtils.defaultString(category_record[1]));
                    }                    
                }
                articles_count.put(0, total_counter);
                if (!category.equals(""))
                {
                    query += (!query.equals("") ? " AND " : "") + " knowledge_base_id = " + ESAPI.encoder().encodeForSQL( ORACLE_CODEC, category);
                }
                int total_filtered_count = db.get_kb.all_articles_filtered_count(con,query);

                String order_column_num = StringUtils.defaultString(request.getParameter("order[0][column]"));
                String order_dir = StringUtils.defaultString(request.getParameter("order[0][dir]"));
                String order_column_name = "";                
                String[] permitted_sort_fields = {"id", "priority", "state", "problem_time", "caller_id", "assigned_to_id"};
                if (!order_column_num.equals(""))
                {
                    order_column_name = StringUtils.defaultString(request.getParameter("columns[" + order_column_num + "][name]"));
                    if (!Arrays.stream(permitted_sort_fields).anyMatch(order_column_name::equals))
                    {
                        order_column_name = "";
                    } else {
                        switch (order_dir) {
                            case "asc":
                                break;
                            case "desc":
                                break;
                            default:
                                order_dir = "";
                        }
                    }
                    
                }

                ArrayList <String[]> all_articles = db.get_kb.all_articles_filtered(con,starting_record,fetch_size, query, order_column_name, order_dir);

                JsonArrayBuilder json_array_builder = Json.createArrayBuilder();

                JsonArrayBuilder categories_builder = Json.createArrayBuilder();
                for(int a = 0; a < all_articles.size();a++)
                {
                    String article_record[] = all_articles.get(a);
//                    logger.info(Arrays.toString(article_record));
                    json_array_builder
                        .add(Json.createObjectBuilder()
                            .add("id", StringUtils.defaultString(article_record[0]))
                            .add("knowledge_base_id", article_record[1])
                            .add("article_name", StringUtils.defaultString(article_record[2]))
                            .add("article_description", StringUtils.defaultString(article_record[3]))
                            .add("status", StringUtils.defaultString(article_record[4]))
                            .add("article_text", StringUtils.defaultString(article_record[5]))
                            .add("kb_name", StringUtils.defaultString(article_record[12]))
                        );
                }
                json_builder.add("recordsTotal", total_filtered_count);
                json_builder.add("recordsFiltered", total_filtered_count);
                json_builder.add("draw", StringUtils.defaultString(request.getParameter("draw")));
                json_builder.add("data", json_array_builder);

                categories.entrySet().forEach(entry ->
                {
                    logger.error("key" + entry.getKey());
                    logger.error("count" + articles_count.get(entry.getKey()));
                    json_categories_array_builder
                        .add(Json.createObjectBuilder()
                            .add("id", entry.getKey())
                            .add("name", entry.getValue())
                            .add("count", articles_count.get(entry.getKey()))
                        );
                });

                json_builder.add("categories", json_categories_array_builder);

                json = json_builder.build();

                con.close();
                //send the JSON data
                out.print(json.toString());
            }
            catch(Exception e)
            {
                e.printStackTrace();
                logger.error("ERROR Exception in servlet ajax_lookup_problems:=" + e);
                stringBuilder.setLength(0);
                stringBuilder.append("Error in retrieving users list");
                out.print(stringBuilder.toString());

            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

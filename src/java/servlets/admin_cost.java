/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class admin_cost extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
    
    protected String get_month(String mmm)
    {
        String return_string = "";
        String a_months[] = {"jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"};
        String n_months[] = {"01","02","03","04","05","06","07","08","09","10","11","12"};
        for(int a = 0; a < a_months.length; a++)
        {
            if(mmm.equalsIgnoreCase(a_months[a]))
            {
                return_string = n_months[a];
                a = a_months.length;
            }
        }
        return return_string;
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        String year = request.getParameter("year");  
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here            
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement("REPLACE INTO cost (group_id,date,cost) VALUES (?,?,?)");
                

                SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
                Enumeration enumeration = request.getParameterNames();
                while (enumeration.hasMoreElements()) 
                {
                    String name = (String) enumeration.nextElement();
                    String value = request.getParameter(name);
                    value = value.replace("$","");
                    value = value.replace(",","");
                    //inc_jan_group_id
                    if(name.startsWith("inc_"))
                    {
                        String split_name[] = name.split("_");
                        String group_id = "";
                        String MM = "";
                        String mmm = "";
                        mmm = split_name[1];
                        group_id = split_name[2];                    
                        MM = get_month(mmm);
                        java.util.Date date = new java.util.Date();
                        date = timestamp_format.parse(year + MM + "01000000");

                        stmt.setString(1,group_id);
                        stmt.setTimestamp(2,new Timestamp(date.getTime()));
                        stmt.setDouble(3,Double.parseDouble(value));
                        stmt.executeUpdate(); 
                    }
                }
                stmt.close();
                con.close();
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_cost:=" + e);
            }
            response.sendRedirect("admin_cost.jsp?requested_year=" + year);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

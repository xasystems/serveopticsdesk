/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */

package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class self_service_follow extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
    private static SimpleDateFormat form_format = new SimpleDateFormat("HH:mm / dd-MMM-YY"); //10:42 / 24-Jan-20
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   
    private static SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        Connection con = null;
        String context_dir = request.getServletContext().getRealPath("");
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            String user_tz_name = "UTC";
            String user_tz_time = "00:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "UTC";
                    user_tz_time = "00:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "UTC";
                user_tz_time = "00:00";
            }  
            java.util.Date now = new java.util.Date();
            java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
            String user_id = session.getAttribute("user_id").toString();
            try
            {      
                
                con = db.db_util.get_contract_connection(context_dir, session); 
                boolean has_related_item = false;
                String user_info[] = db.get_users.by_id(con, user_id);
                String uuid = UUID.randomUUID().toString(); //Generates random UUID  
                String source = "self_service";
                java.util.Date date_sent = new java.util.Date();
                String from = ""; 
                String subject = "";
                String to = "Service Desk"; 
                String body = "";
                String mime_type = "text/plain"; 
                String status = "New"; 
                java.util.Date status_date = new java.util.Date();
                String status_by_user_id = "0";
                String log = "Service Service Follow-up Received at: " + display_format.format(now) + " Status: New" + System.lineSeparator();
                String related_to = "";
                String related_to_id = "";
                String query = "";
                //get all the parameters
                String follow_up_item = "";
                String follow_up_question = "";
                try
                {
                    follow_up_item = request.getParameter("follow_up_item");
                    has_related_item = true;
                    subject = "Self-Service follow-up on " + follow_up_item;
                    //INC114911
                    if(follow_up_item.startsWith("INC"))
                    {
                        related_to = "incident";
                        related_to_id = follow_up_item.replace("INC", "");
                    }
                    else if(follow_up_item.startsWith("REQ"))
                    {
                        related_to = "request";
                        related_to_id = follow_up_item.replace("REQ", "");
                    }
                }
                catch(Exception e)
                {
                    follow_up_item = "None Selected";
                    subject = "Self-Service follow-up on NOTHING SELECTED BY THE USER";
                }
                try
                {
                    follow_up_question = request.getParameter("follow_up_question");
                    body = follow_up_question;
                }
                catch(Exception e)
                {
                    follow_up_question = "Nothing Entered";
                    body = "Nothing Entered";
                }
                if(has_related_item)     
                {
                    query = "INSERT INTO message (`uuid`,`source`,`date_sent`,`from`,`subject`,`to`,`body`,`mime_type`,`status`,`status_date`,`status_by_user_id`,`log`,`related_to`,`related_to_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    PreparedStatement stmt = con.prepareStatement(query);
                    stmt.setString(1, uuid); //uuid
                    stmt.setString(2, source); //source
                    stmt.setTimestamp(3,new Timestamp(now.getTime()));
                    stmt.setString(4, user_info[15]); //user email
                    stmt.setString(5, subject); //subject
                    stmt.setString(6, to); //to
                    stmt.setString(7, body); //body
                    stmt.setString(8, mime_type); //mime_type
                    stmt.setString(9, status); //status
                    stmt.setTimestamp(10,new Timestamp(now.getTime())); //status_date
                    stmt.setString(11, status_by_user_id); //status_by_user_id
                    stmt.setString(12, log); //log
                    stmt.setString(13, related_to); //related_to
                    stmt.setString(14, related_to_id); //related_to_id
                    stmt.execute();
                }
                else
                {
                    query = "INSERT INTO message (`uuid`,`source`,`date_sent`,`from`,`subject`,`to`,`body`,`mime_type`,`status`,`status_date`,`status_by_user_id`,`log`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
                    PreparedStatement stmt = con.prepareStatement(query);
                    stmt.setString(1, uuid); //uuid
                    stmt.setString(2, source); //source
                    stmt.setTimestamp(3,new Timestamp(now.getTime()));
                    stmt.setString(4, user_info[15]); //user email
                    stmt.setString(5, subject); //subject
                    stmt.setString(6, to); //to
                    stmt.setString(7, body); //body
                    stmt.setString(8, mime_type); //mime_type
                    stmt.setString(9, status); //status
                    stmt.setTimestamp(10,new Timestamp(now.getTime())); //status_date
                    stmt.setString(11, status_by_user_id); //status_by_user_id
                    stmt.setString(12, log); //log
                    stmt.execute();
                }
                //create a call for CSAT/SLA
                String calls_insert = "INSERT INTO calls(" 
                        + "media_type,"
                        + "create_time,"
                        + "accept_time,"
                        + "accept_time_seconds,"
                        + "abandon_time," 
                        + "abandon_time_seconds,"
                        + "call_duration_seconds," 
                        + "channel,"  
                        + "direction,"
                        + "agent_name,"
                        + "post_process_time," 
                        + "process_time," 
                        + "total_time,"
                        + "interaction_id) "
                        + " VALUE "
                        + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                PreparedStatement stmt = con.prepareStatement(calls_insert);
                //save the email in calls
                //# id, media_type, create_time, accept_time, accept_time_seconds, abandon_time, abandon_time_seconds, call_duration_seconds, channel, direction, agent_name, post_process_time, process_time, total_time, interaction_id
                //'10016', 'Email', '2017-08-08 14:05:22', '2017-08-08 14:07:33', '131', NULL, NULL, '557', '1-800-555-1234', '0', 'Alan King (Agent3)', '195', '362', '557', '10016'
                String media_type = "Email";
                String create_time = timestamp_format.format(date_sent);
                String accept_time = null;
                String accept_time_seconds = null;
                String abandon_time = null;
                String abandon_time_seconds = null;
                String call_duration_seconds = null;
                String channel = "Self Service";
                String direction = "0";
                String agent_name = null;
                String post_process_time = null;
                String process_time = null;
                String total_time = null;
                String interaction_id = uuid;

                stmt = con.prepareStatement(calls_insert);    
                stmt.setString(1, media_type);
                stmt.setString(2, create_time);
                stmt.setString(3, accept_time);
                stmt.setString(4, accept_time_seconds);
                stmt.setString(5, abandon_time);
                stmt.setString(6, abandon_time_seconds);
                stmt.setString(7, call_duration_seconds);
                stmt.setString(8, channel);
                stmt.setString(9, direction);
                stmt.setString(10, agent_name);
                stmt.setString(11, post_process_time);
                stmt.setString(12, process_time);
                stmt.setString(13, total_time);
                stmt.setString(14, interaction_id);
                stmt.execute();   
                con.close();
                
                String RedirectURL = "home_self_service.jsp?result=Success";
                response.sendRedirect(RedirectURL);  
                
            }
            catch(Exception e)
            {
                logger.error("ERROR Exception in servlet self_service_follow:=" + e);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

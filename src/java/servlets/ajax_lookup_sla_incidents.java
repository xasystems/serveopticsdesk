/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebServlet("/ajax_lookup_sla_incidents")
/**
 *
 * @author ralph
 */
public class ajax_lookup_sla_incidents extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
//        String user_id = request.getParameter("user_id");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

//        String user_id = request.getParameter("user_id");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        JsonObjectBuilder json_builder = Json.createObjectBuilder();
        JsonObject json;
        
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {

                if (session.getAttribute("authenticated") == null) 
                {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }

                Connection con = db.db_util.get_contract_connection(context_dir, session);
                
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
                SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
                SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19

                //SimpleDateFormat filter_format2 = new SimpleDateFormat("yyyy"); //01/12/2019 12:00 AM
                java.util.Date filter_start = new java.util.Date();
                java.util.Date filter_end = new java.util.Date();
                String start = "";
                String end = "";
                //System.out.println("format2=" + filter_format2.parse("2016"));
                //System.out.println("format" + filter_format.parse("01/12/2019 12:00 AM"));

                String sla_id = request.getParameter("sla_id");
                String date_range = "";
                String referer = "";
                try 
                {
                    date_range = request.getParameter("date_range");
                    if (date_range.equalsIgnoreCase("null") || date_range == null) 
                    {
                        date_range = support.filter_dates.past_30_days();
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to past 30 days
                    date_range = support.filter_dates.past_30_days();
                }
                try 
                {
                    referer = request.getParameter("referer");
                    if (referer.equalsIgnoreCase("null") || referer == null) 
                    {
                        referer = "incident_home_support.jsp?date_range=" + date_range;
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to past 30 days
                    referer = "incident_home_support.jsp?date_range=" + date_range;
                }
                try 
                {
                    //01/12/2019 12:00 AM - 01/12/2019 11:59 PM&group_id=all
                    //System.out.println("date_range=" + date_range);
                    String temp[] = date_range.split("-");
                    filter_start = filter_format.parse(temp[0]);
                    //filter_start = filter_format.parse("01/12/2019 12:00 AM");
                    //System.out.println("filter_start=" + temp[0] + " ====" + filter_start);

                    filter_end = filter_format.parse(temp[1]);
                    //System.out.println("filter_end=" + temp[1].trim() + " ====" + filter_end);

                    start = timestamp_format.format(filter_start);
                    end = timestamp_format.format(filter_end);

                    
                } catch (Exception e) {
                    //if not set then default to past 30 days
                    System.out.println("Exception on incident_sla_drilldown.jsp=" + e);
                }
                
//                int total_filtered_count = db.get_incidents.all_filtered_by_query_count(con,query);

                ArrayList<ArrayList<String[]>> all_incidents = support.sla_incident_calc.get_incidents_for_sla_id(con, sla_id, filter_start, filter_end);
//                ArrayList <String[]> incidents = support.sla_incident_calc.get_noncompliant_incidents_for_sla_id(con, sla_id, filter_start, filter_end);
//                ArrayList <String[]> all_incidents = db.get_incidents.all_limited_filtered_by_query(con,starting_record,fetch_size, query, order_column_name, order_dir);

                String[] compliances = {"compliant", "non-compliant"};
                for (int i = 0; i < compliances.length; i++)
                {
                    JsonArrayBuilder json_array_builder = Json.createArrayBuilder();
                    ArrayList<String[]> incidents = all_incidents.get(i);
                    for(int a = 0; a < incidents.size();a++)
                    {
                        String incident_record[] = incidents.get(a);
                        String incident_time = "";
                        try
                        {
                            incident_time = filter_format.format(timestamp_format.parse(incident_record[4]));
                        }
                        catch (Exception e)
                        {
                            incident_time = "";
                        }
                        String state_date = "";
                        try
                        {
                            state_date = filter_format.format(timestamp_format.parse(incident_record[22]));
                        }
                        catch (Exception e)
                        {
                            state_date = "";
                        }

                        json_array_builder
                            .add(Json.createObjectBuilder()
                                .add("history_id", support.string_utils.check_for_null(incident_record[0]))
                                .add("change_time", support.string_utils.check_for_null(incident_record[1]))
                                .add("incident_id", support.string_utils.check_for_null(incident_record[2]))
                                .add("incident_type", support.string_utils.check_for_null(incident_record[3]))
                                .add("incident_time", incident_time)
                                .add("caller_id", support.string_utils.check_for_null(incident_record[5]))
                                .add("caller_group_id", support.string_utils.check_for_null(incident_record[6]))
                                .add("location", support.string_utils.check_for_null(incident_record[7]))
                                .add("department", support.string_utils.check_for_null(incident_record[8]))
                                .add("site", support.string_utils.check_for_null(incident_record[9]))
                                .add("company", support.string_utils.check_for_null(incident_record[10]))
                                .add("category", support.string_utils.check_for_null(incident_record[11]))
                                .add("subcategory", support.string_utils.check_for_null(incident_record[12]))
                                .add("ci_id", support.string_utils.check_for_null(incident_record[13]))
                                .add("impact", support.string_utils.check_for_null(incident_record[14]))
                                .add("urgency", support.string_utils.check_for_null(incident_record[15]))
                                .add("priority", support.string_utils.check_for_null(incident_record[16]))
                                .add("description", support.string_utils.check_for_null(incident_record[17]))
                                .add("create_date", support.string_utils.check_for_null(incident_record[18]))
                                .add("create_by_id", support.string_utils.check_for_null(incident_record[19]))
                                .add("contact_method", support.string_utils.check_for_null(incident_record[20]))
                                .add("state", support.string_utils.check_for_null(incident_record[21]))
                                .add("state_date", state_date)
                                .add("assigned_group_id", support.string_utils.check_for_null(incident_record[23]))
                                .add("assigned_to_id", support.string_utils.check_for_null(incident_record[24]))
                                .add("notes", support.string_utils.check_for_null(incident_record[25]))
                                .add("desk_notes", support.string_utils.check_for_null(incident_record[26]))
                                .add("related", support.string_utils.check_for_null(incident_record[27]))
                                .add("first_contact_resolution", support.string_utils.check_for_null(incident_record[28]))
                                .add("closed_date", support.string_utils.check_for_null(incident_record[29]))
                                .add("closed_reason", support.string_utils.check_for_null(incident_record[30]))
                                .add("pending_date", support.string_utils.check_for_null(incident_record[31]))
                                .add("pending_reason", support.string_utils.check_for_null(incident_record[32]))
                                .add("external_id", support.string_utils.check_for_null(incident_record[33]))
                                .add("caller_username", support.string_utils.check_for_null(incident_record[34]))
                                .add("caller_first", support.string_utils.check_for_null(incident_record[35]))
                                .add("caller_mi", support.string_utils.check_for_null(incident_record[36]))
                                .add("caller_last", support.string_utils.check_for_null(incident_record[37]))
                                .add("caller_phone_mobile", support.string_utils.check_for_null(incident_record[38]))
                                .add("caller_group_name", support.string_utils.check_for_null(incident_record[39]))
                                .add("assigned_group_name", support.string_utils.check_for_null(incident_record[40]))
                                .add("assigned_to_username", support.string_utils.check_for_null(incident_record[41]))
                                .add("assigned_to_first", support.string_utils.check_for_null(incident_record[42]))
                                .add("assigned_to_mi", support.string_utils.check_for_null(incident_record[43]))
                                .add("assigned_to_last", support.string_utils.check_for_null(incident_record[44]))
                                .add("assigned_to_phone_mobile", support.string_utils.check_for_null(incident_record[45]))
                                .add("created_by_username", support.string_utils.check_for_null(incident_record[46]))
                                .add("created_by_first", support.string_utils.check_for_null(incident_record[47]))
                                .add("created_by_mi", support.string_utils.check_for_null(incident_record[48]))
                                .add("created_by_last", support.string_utils.check_for_null(incident_record[49]))
                                .add("created_by_phone_mobile", support.string_utils.check_for_null(incident_record[50]))
                            );
                    }

                    json_builder.add(compliances[i] + "_count", incidents.size());
                    json_builder.add(compliances[i], json_array_builder);
                    
                }
                json = json_builder.build();

                con.close();
                //send the JSON data
                out.print(json.toString());
            }
            catch(Exception e)
            {
                e.printStackTrace();
                logger.error("ERROR Exception in servlet ajax_lookup_incidents:=" + e);
                stringBuilder.setLength(0);
                stringBuilder.append("Error in retrieving incidents list");
                out.print(stringBuilder.toString());

            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

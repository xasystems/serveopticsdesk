/*
* Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
*/

package servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.UUID;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rcampbell
 */
public class export_users extends HttpServlet 
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20  
        java.util.Date now = new java.util.Date();
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        PreparedStatement stmt;
        Connection con = null;
        ArrayList<String[]> records = new ArrayList();
        //get contract db connection
        try
        {
            con = db.db_util.get_contract_connection(context_dir, session);
            //id=0	*username=1	password=2	first=3	mi=4	last=5	address_1=6	Address_2=7	city=8	state=9	zip=10	location=11	department=12	site=13	
            //company=14	email=15	phone_office=16 	phone_mobile=17 	notes=18	vip=19	is_admin=20	tz_name=21	tz_time=22	
            //external_id=23	self_service=24	role=25
            stmt = con.prepareStatement("SELECT * FROM `users` WHERE id > 1 ORDER BY username");
            ResultSet rs = stmt.executeQuery();
            //get the users in an ArrayList            
            while(rs.next())
            {
                String record[] = {"","","","","","","","","","","","","","","","","","","","","","","","","",""};
                String role_list = "";
                record[0] = rs.getString("id");
                record[1] = rs.getString("username");
                String unencrypted_password = "";
                String password = "";
                try
                {
                    if(!rs.getString("password").equalsIgnoreCase("null") || rs.getString("password") == null)
                    {
                        unencrypted_password = support.encrypt_utils.decrypt(context_dir, rs.getString("password"));
                    }
                }
                catch(Exception e)
                {
                    unencrypted_password = "";
                }
                record[2] = unencrypted_password;
                record[3] = rs.getString("first");
                record[4] = rs.getString("mi");
                record[5] = rs.getString("last");
                record[6] = rs.getString("address_1");
                record[7] = rs.getString("Address_2");
                record[8] = rs.getString("city");
                record[9] = rs.getString("state");
                record[10] = rs.getString("zip");
                record[11] = rs.getString("location");
                record[12] = rs.getString("department");
                record[13] = rs.getString("site");
                record[14] = rs.getString("company");
                record[15] = rs.getString("email");
                record[16] = rs.getString("phone_office");
                record[17] = rs.getString("phone_mobile");
                record[18] = rs.getString("notes");
                record[19] = rs.getString("vip");
                record[20] = "false"; //is_admin
                record[21] = rs.getString("tz_name");
                record[22] = rs.getString("tz_time");
                record[23] = rs.getString("external_id");
                record[24] = "false";
                //do role
                ArrayList<String[]> roles = db.get_roles.roles_for_user_id(con,rs.getString("id"));
                for(int a = 0; a < roles.size(); a++)
                {
                    String role[] = roles.get(a);
                    if(role[1].equalsIgnoreCase("3"))//self service
                    {
                        record[24] = "true";
                    }
                    if(role_list.equalsIgnoreCase(""))
                    {
                        role_list =  role[2];
                    }
                    else
                    {
                        role_list = role_list + ";" + role[2];
                    }               
                }
                record[25] = role_list;
                records.add(record);
            }
            //get a UUID file in context_dir/WEB-INF
            //here
            //write the arraylist to a csv file
            UUID uuid = UUID.randomUUID();
            String filePath = context_dir + "/WEB-INF/" + uuid;
            File downloadFile = new File(filePath);
            //write arraylist to file
            FileWriter fw = new FileWriter(downloadFile);
            //line 1 the instructions
            fw.write("Source: ServeOpticsDesk user export. Date: " + date_time_picker_format.format(now)  + "Instructions:  '*' is a required field.  NO DOUBLE QUOTES ,,,,,,,,,,,,,,,,,,,,,,,,," + System.lineSeparator());
            //line 2 
            fw.write("Overwrite=true,,,,,,,,,,,,,,,,,,,,,,,,," + System.lineSeparator());
            //line 3
            fw.write("Leave blank for new users,Best Practice use emaill,UnEncrypted password. Will be encryped when saved to ServeOptics,First Name,Middle Initial,Last Name,Address line 1,Address line 2,city,state,zip code,location,department,site,company,Email. Although not required it is necessary to email the user,Office Phone,Mobile Phone,notes,VIP status 'true' or 'false',Is an admin 'true' or 'false',Official Timezone name: see ServeOptics for the complete list (601 zones),Official Timezone offset., Example: Any character string 255 char long. Can be used to sync with external apps,Assign this user the self service role? 'true' or 'false',Other role by role name. See ServeOptics for defined roles names. Must be exact name match." + System.lineSeparator());
            //line 4
            fw.write("id,*username,password,first,mi,last,address_1,Address_2,city,state,zip,location,department,site,company,email,phone_office,phone_mobile,notes,vip,is_admin,tz_name,tz_time,external_id,self_service,role" + System.lineSeparator());
            
            for(int b=0; b < records.size(); b++)
            {
                String record[] = records.get(b);
                fw.write(record[0] + "," 
                        + record[1] + "," 
                        + record[2] + "," 
                        + record[3] + "," 
                        + record[4] + "," 
                        + record[5] + "," 
                        + record[6] + "," 
                        + record[7] + "," 
                        + record[8] + "," 
                        + record[9] + "," 
                        + record[10] + "," 
                        + record[11] + "," 
                        + record[12] + "," 
                        + record[13] + "," 
                        + record[14] + "," 
                        + record[15] + "," 
                        + record[16] + "," 
                        + record[17] + "," 
                        + record[18] + "," 
                        + record[19] + "," 
                        + record[20] + "," 
                        + record[21] + "," 
                        + record[22] + "," 
                        + record[23] + "," 
                        + record[24] + "," 
                        + record[25] + System.lineSeparator());
            }
            fw.flush();
            fw.close();
            //System.lineSeparator()
            
            FileInputStream inStream = new FileInputStream(downloadFile);

            // obtains ServletContext
            ServletContext context = getServletContext();

            // gets MIME type of the file
            String mimeType = context.getMimeType(filePath);
            if (mimeType == null) 
            {        
                // set to binary type if MIME mapping not found
                mimeType = "application/octet-stream";
            }
            
            // modifies response
            response.setContentType(mimeType);
            response.setContentLength((int) downloadFile.length());

            // forces download
            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
            response.setHeader(headerKey, headerValue);

            // obtains response's output stream
            OutputStream outStream = response.getOutputStream();

            byte[] buffer = new byte[4096];
            int bytesRead = -1;

            while ((bytesRead = inStream.read(buffer)) != -1) 
            {
                outStream.write(buffer, 0, bytesRead);
            }
            inStream.close();
            outStream.close(); 
            //delete the file
            downloadFile.delete();
        }
        catch(Exception e)
        {
            System.out.println("ERROR Exception in servlet export_users:=" + e);
        } 
        response.sendRedirect("admin_users_add_bulk.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

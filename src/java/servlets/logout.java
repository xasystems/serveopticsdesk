//Copyright 2019 XaSystems, Inc. , All rights reserved.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.LinkedHashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author server-xc6701
 */
public class logout extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String username = "";
        try
        {
            if(session != null)
            {
                
                try
                {
                    Connection con = db.db_util.get_contract_connection(context_dir, session);  
                    String user_id = session.getAttribute("user_id").toString();
                    PreparedStatement stmt = con.prepareStatement("DELETE FROM user_sessions WHERE user_id=?");
                    stmt.setString(1, user_id);
                    stmt.executeUpdate();
                }
                catch(Exception e)
                {
                    System.out.println("Exception in logout deleting session info=" + e);
                }
                
                username = session.getAttribute("username").toString();   
                session.invalidate();  
                //System.out.println("active sessions=" + support.session_listener.getActiveSessions());
            }
            logger.debug("Successful logout user " + username);
        }        
        catch(Exception e)
        {
            logger.error("Exception in logout servlet=" + e);
        }
        //System.out.println("login_url=" + props.get("login_url").toString());
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
        //String RedirectURL = "logged_off.jsp" ;
        //response.sendRedirect(RedirectURL);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.LinkedHashMap;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class admin_license_add extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        int is_valid = -1;
        //since this is only for standalone installation read the serveoptics.properties file to get database connection info
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        
        //do work here
        try 
        {
            String license = request.getParameter("license");
            is_valid = support.util.validate_license(context_dir, license);
            if(is_valid != -1)
            {
                Connection con = db.db_util.get_local_connection(props, "local", context_dir);
                String delete = "DELETE FROM system_license";
                PreparedStatement stmt = con.prepareStatement(delete);
                stmt.executeUpdate();

                String insert = "INSERT INTO system_license (license) VALUES (?)";
                stmt = con.prepareStatement(insert);
                stmt.setString(1, license);
                stmt.executeUpdate();
                stmt.close();
                con.close();
            }
        }
        catch (Exception e) 
        {
            logger.error("ERROR Exception in servlet admin_license_edit:=" + e);
        }
        if(is_valid != -1)
        {
            response.sendRedirect("admin_license_add_only.jsp?message=License is valid.");
        }
        else
        {
            response.sendRedirect("admin_license_add_only.jsp?error=License is INVALID.");
        }            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class admin_survey_csat_edit extends HttpServlet 
{

     private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            try 
            {
                String id = request.getParameter("id");
                String name = request.getParameter("name");
                //System.out.println("name=" + name);
                String description = request.getParameter("description");
                //System.out.println("description=" + description);
                String version = request.getParameter("version");
                //System.out.println("target=" + target);
                String status = request.getParameter("status");
                String contact_method = request.getParameter("contact_method");
                String trigger_object = request.getParameter("trigger_object");
                String trigger_state = request.getParameter("trigger_state");
                String trigger_interval = request.getParameter("trigger_interval");
                String max_per_person = request.getParameter("max_per_person");
                if(max_per_person.equalsIgnoreCase("unlimited"))
                {
                    max_per_person = null;
                }
                String lifespan = request.getParameter("lifespan");
                String email_subject = request.getParameter("email_subject");
                String email_body = request.getParameter("email_body");
                String email_reminder_schedule = request.getParameter("email_reminder_schedule");
                String email_reminder_subject = request.getParameter("email_reminder_subject");
                String email_reminder_body = request.getParameter("email_reminder_body");
                
                StringBuilder sb_survey_questions = new StringBuilder(); 
                //start the survey json
                sb_survey_questions.append("{\"survey\": {\"questions\": [");   //{ "survey" : {"questions" : [
                boolean is_first_question = true;
                Enumeration all_names = request.getParameterNames();
                while(all_names.hasMoreElements())
                {
                    String parameterName = all_names.nextElement().toString();
                    String parameterValue = request.getParameter(parameterName);
                    if(parameterName.startsWith("question_number~"))
                    {
                        //get question type
                        String question_type = request.getParameter("question_type~" + parameterValue);
                        if(question_type.equalsIgnoreCase("true_false"))
                        {
                            if(is_first_question)
                            {
                                sb_survey_questions.append("{");
                                sb_survey_questions.append("\"question_text\": \"" + request.getParameter("question~" + parameterValue) + "\",");    //"question_text": "You Happy?",
                                sb_survey_questions.append("\"question_type\": \"true_false\",");
                                sb_survey_questions.append("\"question_number\": " + parameterValue + ",");
                                sb_survey_questions.append("\"answer_text_5\": \"" + request.getParameter("value_5~" + parameterValue) + "\",");
                                sb_survey_questions.append("\"answer_text_1\": \"" + request.getParameter("value_1~" + parameterValue) + "\"");
                                sb_survey_questions.append("}");
                                is_first_question = false;
                            }
                            else
                            {
                                sb_survey_questions.append(",{");
                                sb_survey_questions.append("\"question_text\": \"" + request.getParameter("question~" + parameterValue) + "\",");    //"question_text": "You Happy?",
                                sb_survey_questions.append("\"question_type\": \"true_false\",");
                                sb_survey_questions.append("\"question_number\": " + parameterValue + ",");
                                sb_survey_questions.append("\"answer_text_5\": \"" + request.getParameter("value_5~" + parameterValue) + "\",");
                                sb_survey_questions.append("\"answer_text_1\": \"" + request.getParameter("value_1~" + parameterValue) + "\"");
                                sb_survey_questions.append("}");
                            }
                        }
                        else
                        {
                            if(question_type.equalsIgnoreCase("rating_scale_5"))
                            {
                                if(is_first_question)
                                {
                                    sb_survey_questions.append("{");
                                    sb_survey_questions.append("\"question_text\": \"" + request.getParameter("question~" + parameterValue) + "\",");
                                    sb_survey_questions.append("\"question_type\": \"rating_scale_5\",");
                                    sb_survey_questions.append("\"question_number\": " + parameterValue + ",");
                                    sb_survey_questions.append("\"answer_text_5\": \"" + request.getParameter("value_5~" + parameterValue) + "\",");
                                    sb_survey_questions.append("\"answer_text_4\": \"" + request.getParameter("value_4~" + parameterValue) + "\",");
                                    sb_survey_questions.append("\"answer_text_3\": \"" + request.getParameter("value_3~" + parameterValue) + "\",");
                                    sb_survey_questions.append("\"answer_text_2\": \"" + request.getParameter("value_2~" + parameterValue) + "\",");
                                    sb_survey_questions.append("\"answer_text_1\": \"" + request.getParameter("value_1~" + parameterValue) + "\"");
                                    sb_survey_questions.append("}");
                                    is_first_question = false;
                                }
                                else
                                {
                                    sb_survey_questions.append(",{");
                                    sb_survey_questions.append("\"question_text\": \"" + request.getParameter("question~" + parameterValue) + "\",");
                                    sb_survey_questions.append("\"question_type\": \"rating_scale_5\",");
                                    sb_survey_questions.append("\"question_number\": " + parameterValue + ",");
                                    sb_survey_questions.append("\"answer_text_5\": \"" + request.getParameter("value_5~" + parameterValue) + "\",");
                                    sb_survey_questions.append("\"answer_text_4\": \"" + request.getParameter("value_4~" + parameterValue) + "\",");
                                    sb_survey_questions.append("\"answer_text_3\": \"" + request.getParameter("value_3~" + parameterValue) + "\",");
                                    sb_survey_questions.append("\"answer_text_2\": \"" + request.getParameter("value_2~" + parameterValue) + "\",");
                                    sb_survey_questions.append("\"answer_text_1\": \"" + request.getParameter("value_1~" + parameterValue) + "\"");
                                    sb_survey_questions.append("}");
                                }
                            }
                            else
                            {
                                if(question_type.equalsIgnoreCase("opinion"))
                                {
                                    if(is_first_question)
                                    {
                                        sb_survey_questions.append("{");
                                        sb_survey_questions.append("\"question_text\": \"" + request.getParameter("question~" + parameterValue) + "\",");
                                        sb_survey_questions.append("\"question_type\": \"opinion\",");
                                        sb_survey_questions.append("\"question_number\": " + parameterValue + "");
                                        sb_survey_questions.append("}");
                                        is_first_question = false;
                                    }
                                    else
                                    {
                                        sb_survey_questions.append(",{");
                                        sb_survey_questions.append("\"question_text\": \"" + request.getParameter("question~" + parameterValue) + "\",");
                                        sb_survey_questions.append("\"question_type\": \"opinion\",");
                                        sb_survey_questions.append("\"question_number\": " + parameterValue + "");
                                        sb_survey_questions.append("}");
                                    }
                                }
                            }
                        }
                    }
                }
                //end the survey
                sb_survey_questions.append("]}}");
                String survey_json = sb_survey_questions.toString();
                
                
                java.util.Date now = new java.util.Date();
                Timestamp create_date = new Timestamp(now.getTime());
                                
                String query = "UPDATE survey " +
                        "SET create_date=?,name=?,description=?,version=?,status=?,type=?,contact_method=?,trigger_object=?,trigger_state=?,survey_trigger=?,trigger_interval=?,max_per_person=?,lifespan=?,email_subject=?,email_body=?,email_reminder_schedule=?,email_reminder_subject=?,email_reminder_body=?,survey_json=? " +
                        "WHERE id=?";
                
                
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement(query);
                stmt.setTimestamp(1,create_date);
                stmt.setString(2,name);
                stmt.setString(3,description);
                stmt.setString(4,version);
                stmt.setString(5,status);
                stmt.setString(6,"CSAT");
                stmt.setString(7,contact_method);
                stmt.setString(8,trigger_object);
                stmt.setString(9,trigger_state);                
                stmt.setString(10,"{\"survey\":{\"object_selection\":[]}}");
                stmt.setString(11,trigger_interval);
                stmt.setString(12,max_per_person);
                stmt.setString(13,lifespan);
                stmt.setString(14,email_subject);
                stmt.setString(15,email_body);
                stmt.setString(16,email_reminder_schedule);
                stmt.setString(17,email_reminder_subject);
                stmt.setString(18,email_reminder_body);
                stmt.setString(19,survey_json);
                stmt.setString(20,id);
                stmt.executeUpdate();
                stmt.close();
                con.close();      
                
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_survey_csat_edit:=" + e);
            }
            response.sendRedirect("admin_survey.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

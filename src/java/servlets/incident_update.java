/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import com.google.gson.Gson;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.Tika;
import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;

@MultipartConfig()
/**
 *
 * @author Ralph
 */
public class incident_update extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //get all parms
            String user_tz_name = "US/Eastern";
            String user_tz_time = "-05:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "US/Eastern";
                    user_tz_time = "-05:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "US/Eastern";
                user_tz_time = "-05:00";
            }
            int id = 0;
            String state = "New";
            int caller_id = 0;
            int caller_group_id = 0;
            String caller_username = "";
            int create_by_id = 0;
            String contact_method = "Phone";
            String location = "";
            String department = "";
            String site = "";
            String company = "";
            String priority = "";
            String impact = "";
            String urgency = "";
            int category_id = 0;
            String category = "";
            String subcategory = "";
            String ci_id = "";
            int assigned_group_id = 0;
            int assigned_to_id = 0;
            String assigned_to_name = "";
            String first_contact_resolution = "false";
            String description = "";
            String notes = "";
            String desk_notes = "";
            String related = "";
            String closed_reason = "";
            java.util.Date now = new java.util.Date();
            String currentTimestamp = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(support.date_utils.now_in_utc());
            String incident_time = "";
            String create_date = "";
            String state_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(support.date_utils.now_in_utc());
            String closed_date = null;
            String pending_date = null;
            String pending_reason = "";
            int external_id = 0;
            try
            {                
                String context_dir = request.getServletContext().getRealPath("");
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                //get all the parameters
                LinkedHashMap props = support.config.get_config(context_dir);

                id = Integer.parseInt(request.getParameter("id"));
                String queried_incident[]  = db.get_incidents.incident_by_id(con, request.getParameter("id"));
                create_date = queried_incident[16];
                external_id = id;
                //incident_time
                try
                {
                    String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("incident_time")).getTime()));
                    ZonedDateTime incident_time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                    incident_time = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(incident_time_utc);
                }
                catch(Exception e)
                {
                    String in_time = timestamp_format.format(new java.sql.Timestamp(now.getTime()));
                    ZonedDateTime incident_time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                    incident_time = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(incident_time_utc);
                }
                try{state = request.getParameter("state");}catch (Exception e){state = "New";}
                try{caller_id = Integer.parseInt(request.getParameter("caller_id"));}catch (Exception e){caller_id=0;}
                //caller_username:Abra.Marshall2 //not needed
                //caller_group_id
                try{caller_group_id = Integer.parseInt(request.getParameter("caller_group_id"));}catch (Exception e){caller_group_id=0;}
                try{create_by_id = Integer.parseInt(request.getParameter("create_by_id"));}catch (Exception e){create_by_id=0;}
                //create_by_username:Vera.Mcfarland154 //not needed
                try{contact_method = request.getParameter("contact_method");}catch (Exception e){contact_method = "Phone";}
                try{location = request.getParameter("location");}catch (Exception e){location = "";}
                try{department = request.getParameter("department");}catch (Exception e){department = "";}
                try{site = request.getParameter("site");}catch (Exception e){site = "";}
                try{company = request.getParameter("company");}catch (Exception e){company = "";}
                try{priority = request.getParameter("priority");}catch (Exception e){priority = "";}
                try{impact = request.getParameter("impact");}catch (Exception e){impact = "";}
                try{urgency = request.getParameter("urgency");}catch (Exception e){urgency = "";}                
                try{related = request.getParameter("related");}catch (Exception e){related = "";}//related
                try{category_id = Integer.parseInt(request.getParameter("category_id"));}catch (Exception e){category_id=0;}
                category = db.get_category.from_category_id(con, category_id);
                try{subcategory = request.getParameter("subcategory");}catch (Exception e){subcategory="";}                
                try{ci_id = request.getParameter("ci_id");}catch (Exception e){ci_id = "";}
                try{assigned_group_id = Integer.parseInt(request.getParameter("assigned_group_id"));}catch (Exception e){assigned_group_id=0;}
                //assigned_group_name:Helpdesk
                try{assigned_to_id = Integer.parseInt(request.getParameter("assigned_to_id"));}catch (Exception e){assigned_to_id=0;}
                //assigned_to_name:Ralph.Byers2783  //if blank then assigned to id = 0
                try
                {
                    assigned_to_name = request.getParameter("assigned_to_name");
                    if(assigned_to_name.equalsIgnoreCase("") || assigned_to_name == null)
                    {
                        assigned_to_name = "";
                        assigned_to_id = 0;
                    }
                }
                catch (Exception e)
                {
                    assigned_to_name="";
                    assigned_to_id = 0;
                }
                try
                {
                    first_contact_resolution = request.getParameter("first_contact_resolution");
                    if(first_contact_resolution == null || first_contact_resolution.equalsIgnoreCase("null"))
                    {
                        first_contact_resolution="false";
                    }
                    else
                    {
                        first_contact_resolution="true";
                    }
                }
                catch (Exception e)
                {
                    first_contact_resolution="false";
                }
                try{description = request.getParameter("description");}catch (Exception e){description="";}
                try{notes = request.getParameter("notes");}catch (Exception e){notes="";}
                try{desk_notes = request.getParameter("desk_notes");}catch (Exception e){desk_notes="";}   
                //closed_date=28,
                try
                {
                    String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("closed_date")).getTime()));
                    ZonedDateTime closed_date_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                    closed_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(closed_date_utc);
                }
                catch(Exception e)
                {
                    closed_date = null;
                }
                //closed_reason=29,
                try{closed_reason = request.getParameter("closed_reason");}catch (Exception e){closed_reason="";}
                //pending_date=30,
                try
                {
                    String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("pending_date")).getTime()));
                    ZonedDateTime pending_date_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                    pending_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(pending_date_utc);
                }
                catch(Exception e)
                {
                    pending_date = null;
                }
                //pending_reason31,
                try{pending_reason = request.getParameter("pending_reason");}catch (Exception e){pending_reason="";}
                
                //replace the incident in incidents
                String replace_incidents_table = "REPLACE INTO incidents (id,incident_type,incident_time,caller_id,caller_group_id,location,department,site,company,category,subcategory,ci_id,impact,urgency,priority,description,create_date,create_by_id,contact_method,state,state_date,assigned_group_id,assigned_to_id,notes,desk_notes,related,first_contact_resolution,closed_date,closed_reason,pending_date,pending_reason,external_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                PreparedStatement stmt = con.prepareStatement(replace_incidents_table);
                stmt.setInt(1, id); //id
                stmt.setString(2,""); //incident_type
                stmt.setString(3, incident_time);//incident_time
                stmt.setInt(4, caller_id);//caller_id
                stmt.setInt(5, caller_group_id);//caller_group_id
                stmt.setString(6,location); //location
                stmt.setString(7,department); //department
                stmt.setString(8,site);//site=8
                stmt.setString(9,company);//company=9,
                stmt.setString(10,category);//category=10,
                stmt.setString(11,subcategory);//subcategory=11,
                stmt.setString(12,ci_id);//ci_id=12,
                stmt.setString(13,impact);//impact=13,
                stmt.setString(14,urgency);//urgency=14,
                stmt.setString(15,priority);//priority=15,
                stmt.setString(16,description);//description=16,
                stmt.setString(17,create_date);//create_date=17,
                stmt.setInt(18,create_by_id);//create_by_id=18,
                stmt.setString(19,contact_method);//contact_method=19,
                stmt.setString(20,state);//state=20,
                stmt.setString(21,state_date);//state_date=20,
                stmt.setInt(22,assigned_group_id);//assigned_group_id=22,
                stmt.setInt(23,assigned_to_id);//assigned_to_id=23,
                stmt.setString(24,notes);//notes=24,
                stmt.setString(25,desk_notes);//desk_notes=25,
                stmt.setString(26,related);//related=26,
                stmt.setString(27,first_contact_resolution);//first_contact_resolution=27,
                stmt.setString(28,closed_date);//closed_date=28,
                stmt.setString(29,closed_reason);//closed_reason=29,
                stmt.setString(30,pending_date);//pending_date=30,
                stmt.setString(31,pending_reason);//pending_reason=31,
                stmt.setInt(32, id);//external_id=32    
                logger.info(stmt.toString());
                stmt.execute();

                CouchDbClient dbClient = null;

                try
                {
                    dbClient = db.couchdb.get_client(props);
                    if (dbClient == null)
                    {
                        throw new Exception("dbClient is null");
                    }
                    String user_id = session.getAttribute("user_id").toString();
                    String name_on_disk;
                    String file_name;
                    Tika tika = new Tika();
                    
//                    JsonObject couchdb_document = dbClient.find(JsonObject.class, "incidents:" + id);
//                    logger.info("couchdb document {}", couchdb_document);

                    for (Part filePart : request.getParts()) {
    //                    logger.info("filepart:" + filePart);
                        String fileName = filePart.getSubmittedFileName();
                        if (fileName != null && !"".equals(fileName)) 
                        {
    //                        logger.info("filename:" + fileName);
                            long file_size = filePart.getSize();
                            String mimeType = tika.detect(filePart.getInputStream());
                            file_name = fileName.replaceAll("[^a-zA-Z0-9\\._]+", "_");
    //                        logger.info("file size" + file_size);
    //                    filePart.write(fileName);
    //                    out.println("... uploaded to: /uploads/" + fileName);

                            UUID Uuid = UUID.randomUUID(); //Generates random UUID                                  
                            String uuid = Uuid.toString();

                            Map<String, Object> map = new HashMap<>(); 
                            map.put("_id", uuid);
                            map.put("Filename", file_name);
                            map.put("MIME_type", mimeType);
                            map.put("FileSize", file_size);
                            map.put("DateUploaded", now);
                            map.put("UploadedBy", user_id);
                            map.put("AssociatedObject", "Incident");
                            map.put("AssociatedObjectId", id);

                            Response dbclient_response = dbClient.save(map);
                            name_on_disk = uuid + "/"+ file_name; 
 
                            dbclient_response = dbClient.saveAttachment(filePart.getInputStream(), file_name, mimeType, dbclient_response.getId(), dbclient_response.getRev());

                            if (dbclient_response.getError() == null)
                            {
                                String query_attachment = "INSERT INTO incident_attachements (`incident_id`, `uuid`, `name_on_disk`,`file_name`,`file_date`,`file_size`,`rev`,`uploaded_by`) VALUES (?,?,?,?,?,?,?,?)";
                                PreparedStatement stmt_attachment = con.prepareStatement(query_attachment);
                                stmt_attachment.setInt(1, id);
                                stmt_attachment.setString(2, uuid);
                                stmt_attachment.setString(3, name_on_disk);
                                stmt_attachment.setString(4, file_name);
                                Timestamp file_date = new Timestamp(now.getTime());
                                stmt_attachment.setTimestamp(5, file_date);
                                stmt_attachment.setInt(6, Math.toIntExact(file_size));
                                stmt_attachment.setString(7, dbclient_response.getRev());
                                stmt_attachment.setInt(8, Integer.parseInt(user_id));
                                stmt_attachment.execute();
                            } else {
                                logger.error("CouchDB attachment upload error: {}", dbclient_response.getError());
                            }

                        }

                    }
                } catch (Exception e)
                {
                    logger.error("ERROR attachments process in servlet incident_update:=" + e);
                }

                try
                {
                    String[] attachments_delete_ids = request.getParameterValues("attachment_delete_id");
                    if (attachments_delete_ids != null && attachments_delete_ids.length > 0)
                    {
                        ArrayList<String[]> attachments = db.get_incidents.get_attachments_by_incident_id(con, String.valueOf(id));
                        ArrayList <String> deleted_ids = new ArrayList();
                    
                        for (int i = 0; i < attachments.size(); i++)
                        {
                            String[] attachment = attachments.get(i);
                            if (Arrays.stream(attachments_delete_ids).anyMatch(attachment[0]::equals))
                            {
                                try
                                {
                                    dbClient.remove(attachment[2], attachment[7]);
                                    deleted_ids.add(attachment[0]);
                                } catch (Exception e)
                                {
                                }
                            }
                        }
                        if (deleted_ids.size() > 0)
                        {
                            String query_attachment = "DELETE FROM incident_attachements WHERE incident_id = ? AND id IN (" + String.join(",", deleted_ids) + ")";
                            PreparedStatement stmt_attachment = con.prepareStatement(query_attachment);

                            stmt_attachment.setInt(1, id);
                            stmt_attachment.execute();
                        }

                    }
                    
                } catch (Exception e)
                {
                    e.printStackTrace();
                    logger.error("ERROR Exception in attachment deletion servlet incident_update:=" + e);
                }

                // do linked problems
                String request_problem_ids = StringUtils.defaultString(request.getParameter("problem_ids"));
                if (!request_problem_ids.equals("") && !request_problem_ids.equals("[]"))
                {
                    Gson gson = new Gson();
                    String[] problem_ids = gson.fromJson(request_problem_ids, String[].class); 
                    if (problem_ids.length > 0)
                    {

                        String unlink_problems_query = "DELETE FROM incident_problem WHERE incident_id = ? AND NOT problem_id IN (";
                        for (int i = 0; i < problem_ids.length; i++)
                        {
                            unlink_problems_query += "?" + (i != (problem_ids.length-1) ? "," : "");
                        }
                        unlink_problems_query += ")";
                        PreparedStatement unlink_stmt = con.prepareStatement(unlink_problems_query);
                        unlink_stmt.setInt(1, id); 
                        for (int i = 0; i < problem_ids.length; i++)
                        {
                            String problem_id = problem_ids[i];
                            unlink_stmt.setString(i + 2, problem_id); 
                        }
                        unlink_stmt.execute();
//                        logger.info(unlink_stmt.toString());
                    }

                    String link_problems_query = "INSERT IGNORE incident_problem (incident_id, problem_id) VALUES (?, ?)";
                    PreparedStatement link_stmt = con.prepareStatement(link_problems_query);
                    for (String problem_id : problem_ids)
                    {
                        if (!problem_id.equals("")) 
                        {
                            link_stmt.setInt(1, id); 
                            link_stmt.setString(2, problem_id); 
                            link_stmt.execute();
                        }
                    }
                }
                
                //insert into incident_history
                String insert_incident_history_table = "INSERT INTO incident_history (incident_id,incident_type,incident_time,caller_id,caller_group_id,location,department,site,company,category,subcategory,ci_id,impact,urgency,priority,description,create_date,create_by_id,contact_method,state,state_date,assigned_group_id,assigned_to_id,notes,desk_notes,related,first_contact_resolution,closed_date,closed_reason,pending_date,pending_reason,external_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                PreparedStatement stmt2 = con.prepareStatement(insert_incident_history_table);
                stmt2.setInt(1, id); //id
                stmt2.setString(2,""); //incident_type
                stmt2.setString(3, incident_time);//incident_time
                stmt2.setInt(4, caller_id);//caller_id
                stmt2.setInt(5, caller_group_id);//caller_group_id
                stmt2.setString(6,location); //location
                stmt2.setString(7,department); //department
                stmt2.setString(8,site);//site=8
                stmt2.setString(9,company);//company=9,
                stmt2.setString(10,category);//category=10,
                stmt2.setString(11,subcategory);//subcategory=11,
                stmt2.setString(12,ci_id);//ci_id=12,
                stmt2.setString(13,impact);//impact=13,
                stmt2.setString(14,urgency);//urgency=14,
                stmt2.setString(15,priority);//priority=15,
                stmt2.setString(16,description);//description=16,
                stmt2.setString(17,create_date);//create_date=17,
                stmt2.setInt(18,create_by_id);//create_by_id=18,
                stmt2.setString(19,contact_method);//contact_method=19,
                stmt2.setString(20,state);//state=20,
                stmt2.setString(21,state_date);//state_date=20,
                stmt2.setInt(22,assigned_group_id);//assigned_group_id=22,
                stmt2.setInt(23,assigned_to_id);//assigned_to_id=23,
                stmt2.setString(24,notes);//notes=24,
                stmt2.setString(25,desk_notes);//desk_notes=25,
                stmt2.setString(26,related);//related=26,
                stmt2.setString(27,first_contact_resolution);//first_contact_resolution=27,
                stmt2.setString(28,closed_date);//closed_date=28,
                stmt2.setString(29,closed_reason);//closed_reason=29,
                stmt2.setString(30,pending_date);//pending_date=30,
                stmt2.setString(31,pending_reason);//pending_reason=31,
                stmt2.setInt(32, id);//external_id=32    
                stmt2.execute();
                
                
                //do custom fields
                //get active custom fields
                ArrayList<String[]> active_custom_fields = db.get_custom_fields.active_for_form(con, "incident"); 
                //System.out.println("Number of custom incident fields=" + active_custom_fields.size());
                String replace_data = "REPLACE INTO custom_form_fields_data (`form`,`object_id`,`field_name`,`field_value`) VALUES (?,?,?,?)";
                PreparedStatement stmt3 = con.prepareStatement(replace_data);
                String insert_history = "INSERT INTO `custom_form_fields_history` (`date`,`form`,`object_id`,`field_name`,`field_value`) VALUES (?,?,?,?,?)";
                PreparedStatement stmt4 = con.prepareStatement(insert_history);

                //get the form values
                for(int a = 0; a < active_custom_fields.size();a++)
                {
                    String this_custom_field[] = active_custom_fields.get(a);
                    String this_field_name = this_custom_field[3];
                    String this_field_value = "";
                    String this_field_type = this_custom_field[7];
                    try
                    {
                        if(this_field_type.equalsIgnoreCase("select"))
                        {
                            this_field_value = support.util.check_for_null(request.getParameter(this_field_name));                                
                        }
                        else if(this_field_type.equalsIgnoreCase("text"))
                        {
                            this_field_value = support.util.check_for_null(request.getParameter(this_field_name));                                
                        }
                        else if(this_field_type.equalsIgnoreCase("textarea"))
                        {
                            this_field_value = support.util.check_for_null(request.getParameter(this_field_name));                                
                        }
                        else if(this_field_type.equalsIgnoreCase("checkbox"))
                        {
                            this_field_value = support.util.check_for_null(request.getParameter(this_field_name));    
                            if(this_field_value == null || this_field_value.equalsIgnoreCase(""))
                            {
                                this_field_value = "false";
                            }
                            else
                            {
                                //System.out.println("checkbox=" + this_field_value);
                                this_field_value = "true";
                            }
                        }
                        else if(this_field_type.equalsIgnoreCase("people-lookup"))
                        {
                            this_field_value = support.util.check_for_null(request.getParameter(this_field_name));                                
                        }
                        else if(this_field_type.equalsIgnoreCase("group-lookup"))
                        {
                            this_field_value = support.util.check_for_null(request.getParameter(this_field_name));                                
                        }
                        else if(this_field_type.equalsIgnoreCase("date"))
                        {
                            this_field_value = support.util.check_for_null(request.getParameter(this_field_name));  
                            if(this_field_value.equalsIgnoreCase(""))
                            {
                                this_field_value = null;
                            }
                            try
                            {
                                this_field_value = timestamp_format.format(date_time_picker_format.parse(this_field_value));
                            }
                            catch (Exception e)
                            {
                                this_field_value = null;
                            }
                        }
                    }
                    catch(Exception e)
                    {
                        this_field_value = "";
                    }
                    stmt3.setString(1, "incident"); 
                    stmt3.setInt(2, id); 
                    stmt3.setString(3, this_custom_field[3]); 
                    stmt3.setString(4, this_field_value); 
                    stmt3.execute();

                    stmt4.setString(1, currentTimestamp);
                    stmt4.setString(2, "incident");
                    stmt4.setInt(3, id); 
                    stmt4.setString(4, this_custom_field[3]);
                    stmt4.setString(5, this_field_value);
                    stmt4.execute();
                }
                
                //do global notification
                //caller_incident_update
                if(!state.equalsIgnoreCase("resolved") && !state.equalsIgnoreCase("closed")) //this is update
                {
                    //GLOBAL
                    String caller_incident_update[] = db.get_notifications.specific_global(con, "caller_incident_update");
                    if(caller_incident_update[2].equalsIgnoreCase("true"))
                    {
                        //get subject
                        String incident_subject_format = StringEscapeUtils.unescapeHtml4(caller_incident_update[3]);
                        String subject = support.format_email.text(con, "incident", String.valueOf(id), incident_subject_format, user_tz_time);
                        //get body
                        String incident_body_format = StringEscapeUtils.unescapeHtml4(caller_incident_update[4]);
                        String body = support.format_email.text(con, "incident", String.valueOf(id), incident_body_format, user_tz_time);

                        String user_info[] = db.get_users.by_id(con, String.valueOf(caller_id));
                        String email = user_info[15];
                        if(!email.equalsIgnoreCase(""))
                        {
                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                        }
                    }
                    //assigned USER
                    //System.out.println("PRE start personal assigned to me");
                    String assigned_to_me_incident_update[] = db.get_notifications.user_notifications_for_user_by_notify_on(con, String.valueOf(assigned_to_id), "incident_update_assigned_to_me");
                    if(assigned_to_me_incident_update[3].equalsIgnoreCase("true"))
                    {
                        //get subject
                        String incident_subject_format = StringEscapeUtils.unescapeHtml4(assigned_to_me_incident_update[4]);
                        String subject = support.format_email.text(con, "incident", String.valueOf(id), incident_subject_format, user_tz_time);
                        //get body
                        String incident_body_format = StringEscapeUtils.unescapeHtml4(assigned_to_me_incident_update[5]);
                        String body = support.format_email.text(con, "incident", String.valueOf(id), incident_body_format, user_tz_time);
                        
                        String user_info[] = db.get_users.by_id(con, String.valueOf(assigned_to_id));
                        String email = user_info[15];
                        if(!email.equalsIgnoreCase(""))
                        {
                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                        }
                    }
                    //assigned to group
                    ArrayList<String[]> update_assigned_to_group = db.get_notifications.user_group_notifications_for_user_by_notify_on(con, String.valueOf(assigned_group_id), "incident_update_assigned_to_group");
                    for(int a = 0; a < update_assigned_to_group.size(); a++)
                    {
                        String this_assigned_to_group[] = update_assigned_to_group.get(a);
                        //get subject
                        String incident_subject_format = StringEscapeUtils.unescapeHtml4(this_assigned_to_group[3]);
                        String subject = support.format_email.text(con, "incident", String.valueOf(id), incident_subject_format, user_tz_time);
                        //get body
                        String incident_body_format = StringEscapeUtils.unescapeHtml4(this_assigned_to_group[4]);
                        String body = support.format_email.text(con, "incident", String.valueOf(id), incident_body_format, user_tz_time);
                        
                        String user_info[] = db.get_users.by_id(con, String.valueOf(this_assigned_to_group[1]));
                        String email = user_info[15];
                        if(!email.equalsIgnoreCase(""))
                        {
                            //System.out.println("Sending to emailer  personal assigned to group update SUBJECT=" + subject + " BODY=" + body);
                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                        }
                    }
                }
                
                else if(state.equalsIgnoreCase("resolved")) //this is resolve
                {
                    String caller_incident_resolve[] = db.get_notifications.specific_global(con, "caller_incident_resolve");
                    if(caller_incident_resolve[2].equalsIgnoreCase("true"))
                    {
                        //get subject
                        String incident_subject_format = StringEscapeUtils.unescapeHtml4(caller_incident_resolve[3]);
                        String subject = support.format_email.text(con, "incident", String.valueOf(id), incident_subject_format, user_tz_time);
                        //get body
                        String incident_body_format = StringEscapeUtils.unescapeHtml4(caller_incident_resolve[4]);
                        String body = support.format_email.text(con, "incident", String.valueOf(id), incident_body_format, user_tz_time);

                        String user_info[] = db.get_users.by_id(con, String.valueOf(caller_id));
                        String email = user_info[15];
                        if(!email.equalsIgnoreCase(""))
                        {
                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                        }
                    } 
                    //assigned USER
                    //System.out.println("PRE start personal assigned to me");
                    String assigned_to_me_incident_resolve[] = db.get_notifications.user_notifications_for_user_by_notify_on(con, String.valueOf(assigned_to_id), "incident_resolve_assigned_to_me");
                    if(assigned_to_me_incident_resolve[3].equalsIgnoreCase("true"))
                    {
                        //System.out.println("start personal assigned to me RESOLVE");
                        //get subject
                        String incident_subject_format = StringEscapeUtils.unescapeHtml4(assigned_to_me_incident_resolve[4]);
                        String subject = support.format_email.text(con, "incident", String.valueOf(id), incident_subject_format, user_tz_time);
                        //get body
                        String incident_body_format = StringEscapeUtils.unescapeHtml4(assigned_to_me_incident_resolve[5]);
                        String body = support.format_email.text(con, "incident", String.valueOf(id), incident_body_format, user_tz_time);
                        
                        String user_info[] = db.get_users.by_id(con, String.valueOf(assigned_to_id));
                        String email = user_info[15];
                        if(!email.equalsIgnoreCase(""))
                        {
                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                        }
                    }
                    //assigned to group
                    ArrayList<String[]> resolve_assigned_to_group = db.get_notifications.user_group_notifications_for_user_by_notify_on(con, String.valueOf(assigned_group_id), "incident_resolve_assigned_to_group");
                    for(int a = 0; a < resolve_assigned_to_group.size(); a++)
                    {
                        String this_assigned_to_group[] = resolve_assigned_to_group.get(a);
                        //get subject
                        String incident_subject_format = StringEscapeUtils.unescapeHtml4(this_assigned_to_group[3]);
                        String subject = support.format_email.text(con, "incident", String.valueOf(id), incident_subject_format, user_tz_time);
                        //get body
                        String incident_body_format = StringEscapeUtils.unescapeHtml4(this_assigned_to_group[4]);
                        String body = support.format_email.text(con, "incident", String.valueOf(id), incident_body_format, user_tz_time);
                        
                        String user_info[] = db.get_users.by_id(con, String.valueOf(this_assigned_to_group[1]));
                        String email = user_info[15];
                        if(!email.equalsIgnoreCase(""))
                        {
                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                        }
                    }
                }
                else if(state.equalsIgnoreCase("closed")) //this is closeed
                {
                    String caller_incident_close[] = db.get_notifications.specific_global(con, "caller_incident_close");
                    if(caller_incident_close[2].equalsIgnoreCase("true"))
                    {
                        //get subject
                        String incident_subject_format = StringEscapeUtils.unescapeHtml4(caller_incident_close[3]);
                        String subject = support.format_email.text(con, "incident", String.valueOf(id), incident_subject_format, user_tz_time);
                        //get body
                        String incident_body_format = StringEscapeUtils.unescapeHtml4(caller_incident_close[4]);
                        String body = support.format_email.text(con, "incident", String.valueOf(id), incident_body_format, user_tz_time);

                        String user_info[] = db.get_users.by_id(con, String.valueOf(caller_id));
                        String email = user_info[15];
                        if(!email.equalsIgnoreCase(""))
                        {
                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                        }
                    } 
                    //assigned USER
                    //System.out.println("PRE start personal assigned to me");
                    String assigned_to_me_incident_close[] = db.get_notifications.user_notifications_for_user_by_notify_on(con, String.valueOf(assigned_to_id), "incident_close_assigned_to_me");
                    if(assigned_to_me_incident_close[3].equalsIgnoreCase("true"))
                    {
                        //get subject
                        String incident_subject_format = StringEscapeUtils.unescapeHtml4(assigned_to_me_incident_close[4]);
                        String subject = support.format_email.text(con, "incident", String.valueOf(id), incident_subject_format, user_tz_time);
                        //get body
                        String incident_body_format = StringEscapeUtils.unescapeHtml4(assigned_to_me_incident_close[5]);
                        String body = support.format_email.text(con, "incident", String.valueOf(id), incident_body_format, user_tz_time);
                        
                        String user_info[] = db.get_users.by_id(con, String.valueOf(assigned_to_id));
                        String email = user_info[15];
                        if(!email.equalsIgnoreCase(""))
                        {
                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                        }
                    }
                    //assigned to group
                    ArrayList<String[]> close_assigned_to_group = db.get_notifications.user_group_notifications_for_user_by_notify_on(con, String.valueOf(assigned_group_id), "incident_close_assigned_to_group");
                    for(int a = 0; a < close_assigned_to_group.size(); a++)
                    {
                        String this_assigned_to_group[] = close_assigned_to_group.get(a);
                        //get subject
                        String incident_subject_format = StringEscapeUtils.unescapeHtml4(this_assigned_to_group[3]);
                        String subject = support.format_email.text(con, "incident", String.valueOf(id), incident_subject_format, user_tz_time);
                        //get body
                        String incident_body_format = StringEscapeUtils.unescapeHtml4(this_assigned_to_group[4]);
                        String body = support.format_email.text(con, "incident", String.valueOf(id), incident_body_format, user_tz_time);
                        
                        String user_info[] = db.get_users.by_id(con, String.valueOf(this_assigned_to_group[1]));
                        String email = user_info[15];
                        if(!email.equalsIgnoreCase(""))
                        {
                            //System.out.println("Sending to emailer  personal assigned to group resolve SUBJECT=" + subject + " BODY=" + body);
                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                        }
                    }
                }
                               
            }
            catch(Exception e)
            {
                e.printStackTrace();
                logger.error("ERROR Exception in servlet incident_update:=" + e);
//                String RedirectURL = "error.jsp?source=incident_update&status=Critical&text=Unable to Update the Incident!&reason=" + e;
//                response.sendRedirect(RedirectURL);
                response.sendError(500, e.toString());
            }
            //go back to incident.jsp?id=ID
//            String RedirectURL = "incident_list.jsp?id=" + id;
//            response.sendRedirect(RedirectURL);
//            response.
        
        }//end if authenicated        
    }//end process request

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class admin_contact_sla_edit extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            try 
            {
                String id = request.getParameter("id");
                
                String name = request.getParameter("name");
                //System.out.println("name=" + name);
                String description = request.getParameter("description");
                //System.out.println("description=" + description);
                String target = request.getParameter("target");
                //System.out.println("target=" + target);
                String active = request.getParameter("active");
                if(active == null || active.equalsIgnoreCase("null"))
                {
                    active = "0";
                }
                else
                {
                    active = "1";
                }
                //System.out.println("active=" + active);
                String reportable = request.getParameter("reportable");
                if(reportable == null || reportable.equalsIgnoreCase("null"))
                {
                    reportable = "0";
                }
                else
                {
                    reportable = "1";
                }
                
                //System.out.println("reportable=" + reportable);
                String sla_text = request.getParameter("sla_text");
                //System.out.println("sla_text=" + sla_text);
                String sla_type = request.getParameter("sla_type");
                //System.out.println("sla_type=" + sla_type);
                String abandoned_threshold = null;
                /*<option value="ABANDONED_CONTACT_RATE">Abandoned Contact Rate</option>
                <option value="ABANDONED_CONTACT_COUNT">Abandoned Contact Count</option>
                <option value="AVERAGE_POST_PROCESS_TIME">Average Post Process Time</option>
                <option value="AVERAGE_SPEED_TO_ANSWER">Average Speed to Answer</option>
                <option value="AVERAGE_TOTAL_TIME">Average Total Time</option>
                <option value="AVERAGE_ABANDONED_TIME">Average Abandoned Time</option>
                <option value="AVERAGE_HANDLE_TIME">Average Handle Time</option>
                <option value="AVERAGE_PROCESSING_TIME">Average Processing Time</option>
                <option value="AVERAGE_TIME_IN_QUEUE">Average Time in Queue</option>*/               
                if(sla_type.equalsIgnoreCase("ABANDONED_CONTACT_RATE") || sla_type.equalsIgnoreCase("ABANDONED_CONTACT_COUNT") || sla_type.equalsIgnoreCase("AVERAGE_ABANDONED_TIME"))
                {
                    abandoned_threshold = request.getParameter("abandoned_threshold");
                }
                else 
                {
                    abandoned_threshold = null;
                }
                String sla_timeframe = request.getParameter("sla_timeframe");
                //System.out.println("sla_timeframe=" + sla_timeframe);
                String schedule_id = request.getParameter("schedule_id");
                //System.out.println("schedule_id=" + schedule_id);
                
                String sla_success_threshold_operator = null;
                try
                {
                    sla_success_threshold_operator = request.getParameter("sla_success_threshold_operator");
                    if(sla_success_threshold_operator == null || sla_success_threshold_operator.equalsIgnoreCase("null"))
                    {
                        sla_success_threshold_operator = null;
                    }
                }
                catch(Exception e)
                {
                    sla_success_threshold_operator = null;
                }
                //System.out.println("sla_success_threshold_operator=" + sla_success_threshold_operator);
                
                String sla_success_threshold_value = request.getParameter("sla_success_threshold_value");
                
                String sla_success_threshold_unit = null;
                try
                {
                    sla_success_threshold_unit = request.getParameter("sla_success_threshold_unit");
                    if(sla_success_threshold_unit == null || sla_success_threshold_unit.equalsIgnoreCase("null"))
                    {
                        sla_success_threshold_unit = null;
                    }
                }
                catch(Exception e)
                {
                    sla_success_threshold_unit = null;
                }
                //System.out.println("sla_success_threshold_unit=" + sla_success_threshold_unit);
                String report_name = request.getParameter("report_name");
                //System.out.println("report_name=" + report_name);
                String report_description = request.getParameter("report_description");
                //System.out.println("report_description=" + report_description);
                String report_header = request.getParameter("report_header");
                //System.out.println("report_header=" + report_header);
                String sla_incentive_exceptional = "0";
                String sla_incentive_exceptional_dollars = "0.00";
                String sla_incentive_acceptable = "0";
                String sla_incentive_acceptable_dollars = "0.00";
                String sla_incentive_unacceptable = "0";
                String sla_incentive_unacceptable_dollars = "0.00";
                
                String additional_parameters = "";  //for contact this is the changel list
                //build up the additional_parameters
                boolean has_additional_parameters = false;
                StringBuilder sb_parameters = new StringBuilder(); 
                //start the json
                sb_parameters.append("{ \"parameters\" : [{\"channels\" : [");   //{ "parameters" : [{"channels" : ["Service Desk Chat","Network Support","Business Systems"]}]}
                
                
                String channels[] = request.getParameterValues("channels");
                for(int a = 0; a < channels.length; a++)
                {
                    if(a == 0)
                    {
                        sb_parameters.append("{\"name\":\"");
                        sb_parameters.append(channels[a]);
                        sb_parameters.append("\"}");
                    }
                    else
                    {
                        sb_parameters.append(",{\"name\":\"");
                        sb_parameters.append(channels[a]);
                        sb_parameters.append("\"}");
                    }
                }
                //end the json
                sb_parameters.append("]}]}");
                additional_parameters = sb_parameters.toString();
                //System.out.println("additional_parameters=" + additional_parameters);
                                
                String q = "REPLACE INTO contact_sla (id,name,description,sla_text,sla_type,target,active,reportable,schedule_id,sla_timeframe,abandoned_threshold,sla_success_threshold_operator,sla_success_threshold_value,sla_success_threshold_unit,sla_incentive_exceptional,sla_incentive_exceptional_dollars,sla_incentive_acceptable,sla_incentive_acceptable_dollars,sla_incentive_unacceptable,sla_incentive_unacceptable_dollars,additional_parameters,report_name,report_description,report_header) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement(q);
                stmt.setString(1,id);
                stmt.setString(2,name);
                stmt.setString(3,description);
                stmt.setString(4,sla_text);
                stmt.setString(5,sla_type);
                stmt.setString(6,target);
                stmt.setString(7,active);
                stmt.setString(8,reportable);
                stmt.setString(9,schedule_id);
                stmt.setString(10,sla_timeframe);                
                stmt.setString(11,abandoned_threshold);
                stmt.setString(12,sla_success_threshold_operator);
                stmt.setString(13,sla_success_threshold_value);
                stmt.setString(14,sla_success_threshold_unit);
                stmt.setString(15,sla_incentive_exceptional);
                stmt.setString(16,sla_incentive_exceptional_dollars);
                stmt.setString(17,sla_incentive_acceptable);
                stmt.setString(18,sla_incentive_acceptable_dollars);
                stmt.setString(19,sla_incentive_unacceptable);
                stmt.setString(20,sla_incentive_unacceptable_dollars);
                stmt.setString(21,additional_parameters);
                stmt.setString(22,report_name);
                stmt.setString(23,report_description);
                stmt.setString(24,report_header);
                stmt.executeUpdate();
                stmt.close();
                con.close();      
                
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_contact_sla_edit:=" + e);
            }
            response.sendRedirect("admin_sla.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

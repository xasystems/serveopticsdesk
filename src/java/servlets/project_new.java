/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import static java.time.ZonedDateTime.now;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.Tika;
import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;

@MultipartConfig()
/**
 *
 * @author Ralph
 */
public class project_new extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
    private static SimpleDateFormat form_format = new SimpleDateFormat("HH:mm / dd-MMM-YY"); //10:42 / 24-Jan-20
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            String user_tz_name = "US/Eastern";
            String user_tz_time = "-05:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "US/Eastern";
                    user_tz_time = "-05:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "US/Eastern";
                user_tz_time = "-05:00";
            }
            //do work here
            //java.util.Date now = new java.util.Date();
            //java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
            
            int project_id = 0;
            String name = "";
            try{name = request.getParameter("name");}catch (Exception e){name="";}
            int owner_group_id = 0;
            try{owner_group_id = Integer.parseInt(request.getParameter("owner_group_id"));}catch (Exception e){owner_group_id=0;}
            int owner_id = 0;
            try{owner_id = Integer.parseInt(request.getParameter("owner_id"));}catch (Exception e){owner_id=0;}
            int manager_id = 0;
            try{manager_id = Integer.parseInt(request.getParameter("manager_id"));}catch (Exception e){manager_id=0;}
            int tech_lead_id = 0;
            try{tech_lead_id = Integer.parseInt(request.getParameter("tech_lead_id"));}catch (Exception e){tech_lead_id=0;}
            String description = "";
            try{description = request.getParameter("description");}catch (Exception e){description="";}
            String scheduled_start_date = null;
            try
            {
                String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("scheduled_start_date")).getTime()));
                ZonedDateTime time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                scheduled_start_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(time_utc);
            }
            catch(Exception e)
            {
                String in_time = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
                ZonedDateTime time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                scheduled_start_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(time_utc);
            }
            String actual_start_date = null;
            try
            {
                String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("actual_start_date")).getTime()));
                ZonedDateTime time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                actual_start_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(time_utc);
            }
            catch(Exception e)
            {
                String in_time = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
                ZonedDateTime time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                actual_start_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(time_utc);
            }
            String scheduled_end_date = null;
            try
            {
                String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("scheduled_end_date")).getTime()));
                ZonedDateTime time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                scheduled_end_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(time_utc);
            }
            catch(Exception e)
            {
                String in_time = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
                ZonedDateTime time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                scheduled_end_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(time_utc);
            }
            String actual_end_date = null;
            try
            {
                String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("actual_end_date")).getTime()));
                ZonedDateTime time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                actual_end_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(time_utc);
            }
            catch(Exception e)
            {
                String in_time = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
                ZonedDateTime time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                actual_end_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(time_utc);
            }
            String priority = "";
            try{priority = request.getParameter("priority");}catch (Exception e){priority="";}
            String status = "";
            try{status = request.getParameter("status");}catch (Exception e){status="";}
            String notes = "";
            try{notes = request.getParameter("notes");}catch (Exception e){notes="";}
            //do work here
            CouchDbClient dbClient = null;
            java.util.Date now = new java.util.Date();

            try
            {                
                String context_dir = request.getServletContext().getRealPath("");
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                //get all the parameters
                project_id = db.get_projects.next_project_id(con);
                
                String insert_projects_table = "INSERT INTO projects (id,name,description,owner_group_id,owner_id,manager_id,tech_lead_id,scheduled_start_date,actual_start_date,scheduled_end_date,actual_end_date,status,priority,notes) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                PreparedStatement stmt = con.prepareStatement(insert_projects_table);
                stmt.setInt(1, project_id); //id
                stmt.setString(2,name); //name
                stmt.setString(3,description);//description
                stmt.setInt(4,owner_group_id);//owner_group_id
                stmt.setInt(5,owner_id);//owner_id
                stmt.setInt(6,manager_id);//manager_id
                stmt.setInt(7, tech_lead_id); //tech_lead_id
                stmt.setString(8, scheduled_start_date); //scheduled_start_date
                stmt.setString(9, actual_start_date); //actual_start_date
                stmt.setString(10, scheduled_end_date); //scheduled_end_date
                stmt.setString(11, actual_end_date); //actual_end_date
                stmt.setString(12,status); //status
                stmt.setString(13,priority); //priority
                stmt.setString(14,notes); //notes                
                stmt.execute();
//                String RedirectURL = "project.jsp?project_id=" + id ;
//                response.sendRedirect(RedirectURL);

                try
                {
                    LinkedHashMap props = support.config.get_config(context_dir);
                    dbClient = db.couchdb.get_client(props);
                    if (dbClient == null)
                    {
                        throw new Exception("dbClient is null");
                    }
                    String user_id = session.getAttribute("user_id").toString();
                    String name_on_disk;
                    String file_name;
                    Tika tika = new Tika();

                    for (Part filePart : request.getParts()) {
    //                    logger.info("filepart:" + filePart);
                        String fileName = filePart.getSubmittedFileName();
                        if (fileName != null && !"".equals(fileName)) 
                        {
                            long file_size = filePart.getSize();
                            String mimeType = tika.detect(filePart.getInputStream());
                            file_name = fileName.replaceAll("[^a-zA-Z0-9\\._]+", "_");

                            UUID Uuid = UUID.randomUUID(); //Generates random UUID                                  
                            String uuid = Uuid.toString();

                            Map<String, Object> map = new HashMap<>(); 
                            map.put("_id", uuid);
                            map.put("Filename", file_name);
                            map.put("MIME_type", mimeType);
                            map.put("FileSize", file_size);
                            map.put("DateUploaded", now);
                            map.put("UploadedBy", user_id);
                            map.put("AssociatedObject", "Project");
                            map.put("AssociatedObjectId", project_id);

                            Response dbclient_response = dbClient.save(map);
                            name_on_disk = uuid + "/"+ file_name; 
 
                            dbclient_response = dbClient.saveAttachment(filePart.getInputStream(), file_name, mimeType, dbclient_response.getId(), dbclient_response.getRev());

                            if (dbclient_response.getError() == null)
                            {
                                String query_attachment = "INSERT INTO project_attachements (`project_id`, `uuid`,`name_on_disk`,`file_name`,`file_date`,`file_size`,`rev`,`uploaded_by`) VALUES (?,?,?,?,?,?,?,?)";
                                PreparedStatement stmt_attachment = con.prepareStatement(query_attachment);
                                stmt_attachment.setInt(1, project_id);
                                stmt_attachment.setString(2, uuid);
                                stmt_attachment.setString(3, name_on_disk);
                                stmt_attachment.setString(4, file_name);
                                Timestamp file_date = new Timestamp(now.getTime());
                                stmt_attachment.setTimestamp(5, file_date);
                                stmt_attachment.setInt(6, Math.toIntExact(file_size));
                                stmt_attachment.setString(7, dbclient_response.getRev());
                                stmt_attachment.setInt(8, Integer.parseInt(user_id));
                                stmt_attachment.execute();
                            } else {
                                logger.error("CouchDB attachment upload error: {}", dbclient_response.getError());
                            }

                        }

                    }
                } catch (Exception e)
                {
                    logger.error("ERROR attachments process in servlet project_new:=" + e);
                }

            }
            catch(Exception e)
            {
                logger.error("ERROR Exception in servlet project_new:=" + e);
            } finally {
                if (dbClient != null )
                {
                    dbClient.shutdown();
                }
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

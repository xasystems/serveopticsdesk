/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import java.util.ArrayList;

/**
 *
 * @author Ralph
 */
public class ajax_lookup_custom_field_data extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    protected String check_for_null(String value)
    {
        String return_string = "";
        if(value == null || value.equalsIgnoreCase("null"))
        {
            return_string = "";
        }
        else 
        {
            return_string = value;
        }        
        return return_string;
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
        String request_path = request.getContextPath();
        String user_id = session.getAttribute("user_id").toString();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        JsonObjectBuilder json_builder = Json.createObjectBuilder();
        JsonObject json;
//        JsonObject json = Json.createObjectBuilder().add("foo", "bar").build();
        
        boolean first_record = true;
        boolean has_data = false;
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {

                if (session.getAttribute("authenticated") == null) 
                {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }

                String ADMINISTRATION = session.getAttribute("administration").toString();
                boolean page_authorized = false;
                
                if(ADMINISTRATION.equalsIgnoreCase("true"))
                {
                    page_authorized = true;
                }

                if(!page_authorized)
                {
                    response.sendError(HttpServletResponse.SC_FORBIDDEN);
                }

                //System.out.println("search=" + search);
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String id = request.getParameter("id");
                String custom_field_info[] = db.get_custom_fields.by_id(con, id);
                
                for (int i = 0; i < custom_field_info.length; i++) {
                    String string = custom_field_info[i];
                    json_builder.add(String.valueOf(i), custom_field_info[i]);
                }

                json = json_builder.build();

                con.close();
                //send the JSON data
                out.print(json.toString());
            }
            catch(Exception e)
            {
                logger.error("ERROR Exception in servlet " + request_path + " :=" + e);
                json = json_builder.build();
                out.print(json.toString());

            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

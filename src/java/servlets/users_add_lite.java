//Copyright 2021 XaSystems, Inc. , All rights reserved.

package servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Set;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@MultipartConfig()
/**
 *
 * @author rcampbell
 */
public class users_add_lite extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            String contract_id = session.getAttribute("db").toString();
            String installation_type = props.get("installation_type").toString().trim();
            String user_id = session.getAttribute("user_id").toString();
            //do work here
            //get the params from the referer page
            String referer = request.getParameter("referer");
            String action = request.getParameter("action");
            String message_id = request.getParameter("message_id");
            String message_log = request.getParameter("message_log");
            String in_email = request.getParameter("email");
            Boolean embedded = request.getParameter("embedded") != null && BooleanUtils.toBoolean(request.getParameter("embedded"));
            JsonObjectBuilder json_builder = Json.createObjectBuilder();
            JsonObject json;
            
            try 
            {
                /*returnString[0] = rs.getString("id");
                returnString[1] = rs.getString("username");
                returnString[2] = rs.getString("password");
                returnString[3] = check_for_null(rs.getString("first"));
                returnString[4] = check_for_null(rs.getString("mi"));
                returnString[5] = check_for_null(rs.getString("last"));
                returnString[6] = check_for_null(rs.getString("address_1"));
                returnString[7] = check_for_null(rs.getString("address_2"));
                returnString[8] = check_for_null(rs.getString("city"));
                returnString[9] = check_for_null(rs.getString("state"));
                returnString[10] = check_for_null(rs.getString("zip"));
                returnString[11] = check_for_null(rs.getString("location"));
                returnString[12] = check_for_null(rs.getString("department"));
                returnString[13] = check_for_null(rs.getString("site"));
                returnString[14] = check_for_null(rs.getString("company"));
                returnString[15] = check_for_null(rs.getString("email"));
                returnString[16] = check_for_null(rs.getString("phone_office"));
                returnString[17] = check_for_null(rs.getString("phone_mobile"));
                returnString[18] = check_for_null(rs.getString("notes"));
                returnString[19] = check_for_null(rs.getString("vip"));
                returnString[20] = check_for_null(rs.getString("is_admin"));
                returnString[21] = check_for_null(rs.getString("tz_name"));
                returnString[22] = check_for_null(rs.getString("tz_time"));
                returnString[23] = check_for_null(rs.getString("external_id"));*/                
                
                String username = support.data_check.null_string(request.getParameter("username"));
                String password = support.data_check.null_string(request.getParameter("password"));
                String first = support.data_check.null_string(request.getParameter("first"));
                String mi = support.data_check.null_string(request.getParameter("mi"));
                String last = support.data_check.null_string(request.getParameter("last"));
                String address_1 = support.data_check.null_string(request.getParameter("address_1"));
                String address_2 = support.data_check.null_string(request.getParameter("address_2"));
                String city = support.data_check.null_string(request.getParameter("city"));
                String state = support.data_check.null_string(request.getParameter("state"));
                String zip = support.data_check.null_string(request.getParameter("zip"));
                String location = support.data_check.null_string(request.getParameter("location"));
                String department = support.data_check.null_string(request.getParameter("department"));
                String site = support.data_check.null_string(request.getParameter("site"));
                String company = support.data_check.null_string(request.getParameter("company"));
                String email = support.data_check.null_string(request.getParameter("email"));
                String phone_office = support.data_check.null_string(request.getParameter("phone_office"));
                String phone_mobile = support.data_check.null_string(request.getParameter("phone_mobile"));
                String tz[] = support.data_check.null_string(request.getParameter("tz")).split(",");
                String tz_name = tz[0];
                String tz_time = tz[1];
                String notes = support.data_check.null_string(request.getParameter("notes"));                
                String vip = "false";
                if(request.getParameter("vip") == null)
                {
                    //checkbox not checked
                    vip = "false";
                }
                else
                {
                    //checkbox checked
                    vip = "true";
                }
                String is_admin = "false";
                if(request.getParameter("is_admin") == null)
                {
                    //checkbox not checked
                    is_admin = "false";
                }
                else
                {
                    //checkbox checked
                    is_admin = "true";
                }  
                
                String external_id = "0";
                
                password = support.encrypt_utils.encrypt(context_dir, password);
                
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                 
                PreparedStatement stmt = con.prepareStatement("INSERT INTO users (username,password,first,mi,last,address_1,address_2,city,state,zip,location,department,site,company,email,phone_office,phone_mobile,notes,vip,is_admin,tz_name,tz_time,external_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                stmt.setString(1,username);
                stmt.setString(2,password);
                stmt.setString(3,first);
                stmt.setString(4,mi);
                stmt.setString(5,last);
                stmt.setString(6,address_1);
                stmt.setString(7,address_2);
                stmt.setString(8,city);
                stmt.setString(9,state);
                stmt.setString(10,zip);
                stmt.setString(11,location);
                stmt.setString(12,department);
                stmt.setString(13,site);
                stmt.setString(14,company);
                stmt.setString(15,email);
                stmt.setString(16,phone_office);
                stmt.setString(17,phone_mobile);
                stmt.setString(18,notes);
                stmt.setString(19,vip);
                stmt.setString(20,is_admin);
                stmt.setString(21,tz_name);
                stmt.setString(22,tz_time);
                stmt.setString(23,external_id);
                stmt.executeUpdate();
                //get the user id for this new user
                String new_user_info[] = db.get_users.by_username(con, username);
                String new_user_id = new_user_info[0];
                //do roles now
                ArrayList <String> role_ids = new ArrayList();
                Set<String> paramNames = request.getParameterMap().keySet();
                // iterating over parameter names and get its value
                for (String name : paramNames) 
                {
                    String value = request.getParameter(name);
                    if(name.startsWith("role_id_"))
                    {
                        role_ids.add(value);
                    }
                }
                boolean shared = false;
                if(installation_type.equalsIgnoreCase("shared"))
                {
                    shared = true;
                }
                String shared_key = props.get("shared_key").toString().trim(); 
                String shared_user = props.get("shared_user").toString().trim(); 
                boolean needs_login = false;
                stmt = con.prepareStatement("INSERT INTO user_roles (user_id,role_id) VALUES (?,?)");
                for(int a = 0; a < role_ids.size(); a++)
                {
                    //if the role is 0-3 or contains self_service
                    if(role_ids.get(a).equalsIgnoreCase("0") || role_ids.get(a).equalsIgnoreCase("1") || role_ids.get(a).equalsIgnoreCase("2") || role_ids.get(a).equalsIgnoreCase("3"))
                    {
                        needs_login = true;
                    }
                    else
                    {
                        String role_info[] = db.get_roles.by_role_id(con, role_ids.get(a));
                        if(!role_info[19].equalsIgnoreCase("none"))
                        {
                            needs_login = true;
                        }
                    }
                    stmt.setString(1,new_user_id);
                    stmt.setString(2,role_ids.get(a));
                    stmt.executeUpdate();
                }
                //if a shared installation then go thru all roles looking for self_service != none
                if(shared)
                {
                    if(needs_login)
                    {
                        java.util.Date now = new java.util.Date();
                        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");    
                        
                        String authorization_string = shared_user + "sEp,sEp" + shared_key + "sEp,sEp" + timestamp_format.format(now);
                        String encrypted_authorization_string = support.encrypt_utils.encrypt(context_dir, authorization_string);
                        //create json object
                        JsonObjectBuilder factory = Json.createObjectBuilder();
                        factory.add("authorization", encrypted_authorization_string);
                        factory.add("request_time", timestamp_format.format(now));
                        //encrypt jsonarray
                        JsonArrayBuilder users = Json.createArrayBuilder();
                        JsonObject user_record = Json.createObjectBuilder()
                        .add("action", "create")
                        .add("contract_id", contract_id)
                        .add("id",new_user_id)        
                        .add("username",username)
                        .add("password",password)
                        .add("first",first)
                        .add("mi",mi)
                        .add("last", last)
                        .add("address_1",address_1)
                        .add("address_2",address_2)
                        .add("city",city)
                        .add("state",state)
                        .add("zip",zip)
                        .add("email",email)
                        .add("phone_office",phone_office)
                        .add("phone_mobile",phone_mobile)
                        .add("notes",notes)
                        .build(); 
                        users.add(user_record);
                        JsonArray unencrypted_users = users.build();
                        JsonArray encrypted_users = support.encrypt_utils.encrypt_jsonarray(context_dir, unencrypted_users);                        
                        factory.add("users", encrypted_users);
                        JsonObject json_string = factory.build();
                        //send this json array to the login_url
                        try
                        {
                            URL url = new URL(props.get("login_url") + "/user_update");
                            URLConnection connection = url.openConnection();
                            connection.setDoOutput(true);
                            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());

                            String stringToSend = URLEncoder.encode(json_string.toString(), "UTF-8");
                            out.write("data=" + stringToSend);
                            out.flush();
                            out.close();
                            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                            String decodedString;
                            while ((decodedString = in.readLine()) != null) 
                            {
                                System.out.println(decodedString);
                            }
                            in.close();

                        }
                        catch(Exception cn)
                        {
                            System.out.println("users_add_lite failed to update login server. Exception=" + cn);
                        }
                    } 
                }
                //do group membership
                try
                {
                    String groups[] = request.getParameterValues("group_ids");
                    if(groups != null)
                    {
                        stmt = con.prepareStatement("INSERT INTO users_groups (user_id,group_id,role,external_id) VALUES (?,?,?,?)");
                        for(int a = 0; a < groups.length; a++)
                        {
                            stmt.setString(1,new_user_id);
                            stmt.setString(2,groups[a]);
                            stmt.setString(3,"");
                            stmt.setString(4,"");                    
                            stmt.executeUpdate();
                        }    
                    }
                }
                catch(Exception e)
                {
                    System.out.println("ERROR Exception in servlet users_add_lite do group membership:=" + e);
                }
                //create all the notification table stuff
                String notify_on[] = {"incident_create_assigned_to_me",
                    "incident_create_assigned_to_group",
                    "incident_update_assigned_to_me",
                    "incident_update_assigned_to_group",
                    "incident_resolve_assigned_to_me",
                    "incident_resolve_assigned_to_group",
                    "incident_close_assigned_to_me",
                    "incident_close_assigned_to_group",
                    "request_create_assigned_to_me",
                    "request_create_assigned_to_group",
                    "request_update_assigned_to_me",
                    "request_update_assigned_to_group",
                    "request_resolve_assigned_to_me",
                    "request_resolve_assigned_to_group",
                    "request_close_assigned_to_me",
                    "request_close_assigned_to_group",
                    "job_create_assigned_to_me",
                    "job_create_assigned_to_group",
                    "job_update_assigned_to_me",
                    "job_update_assigned_to_group",
                    "job_close_assigned_to_me",
                    "job_close_assigned_to_group",
                    "task_create_assigned_to_me",
                    "task_create_assigned_to_group",
                    "task_update_assigned_to_me",
                    "task_update_assigned_to_group",
                    "task_close_assigned_to_me",
                    "task_close_assigned_to_group"                   
                };
                try
                {
                    stmt = con.prepareStatement("INSERT INTO notifications_user (user_id,notify_on,active,subject,body) VALUES (?,?,?,?,?)");
                    for(int a = 0; a < notify_on.length; a++)
                    {
                        stmt.setString(1,new_user_id);
                        stmt.setString(2,notify_on[a]);
                        stmt.setString(3,"false");
                        stmt.setString(4,""); 
                        stmt.setString(5,"");
                        stmt.executeUpdate();
                    } 
                    stmt.close();
                    con.close();
                }
                catch(Exception e)
                {
                    System.out.println("ERROR Exception in servlet users_add_lite do notifications:=" + e);
                }
                if (!embedded.equals("")) {
                    try {
                        PrintWriter out = response.getWriter();
                        json_builder.add("error", "");
                        json_builder.add("user_id", new_user_id);
                        json_builder.add("user_name", username);
                        json = json_builder.build();
                        out.print(json.toString());
                        return;
                    }
                    catch(Exception e)
                    {
                        System.out.println("ERROR Exception in servlet users_add_lite do notifications:=" + e);
                    }
                }
                if(referer.equalsIgnoreCase("message_process.jsp"))
                {
                    response.sendRedirect("message_process.jsp?id=" + message_id);    
                    //String values[] = {"none","new_incident","new_request","follow-up_incident","follow-up_request","spam","update"};
                    /*
                    if(action.equalsIgnoreCase("new_incident"))
                    {
                        String message_info[] = db.get_message.message_by_id(con, message_id);
                        
                        //get next incident_id
                        int incident_id = db.get_incidents.next_incident_id(con);
                        //insert new incident stub
                        //create fields/values for save_incident_stub
                        ArrayList<String[]> field_values = new ArrayList();
                        String id[] = {"id",String.valueOf(incident_id)};
                        field_values.add(id);
                        
                        String incident_time[] = {"incident_time", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(support.date_utils.now_in_utc())};
                        field_values.add(incident_time);
                        
                        String caller_id[] = {"caller_id",new_user_id};
                        field_values.add(caller_id);
                        
                        String description[] = {"description",message_info[5]};
                        field_values.add(description);
                        
                        String create_date[] = {"create_date", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(support.date_utils.now_in_utc())};
                        field_values.add(create_date);
                        
                        String create_by_id[] = {"create_by_id",user_id};
                        field_values.add(create_by_id);
                        
                        String contact_method[] = {"contact_method","Self-Service"};
                        field_values.add(contact_method);
                                
                        String incident_state[] = {"state","New"};
                        field_values.add(incident_state);
                                
                        String state_date[] = {"state_date", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(support.date_utils.now_in_utc())};
                        field_values.add(state_date);
                                
                        String incident_notes[] = {"notes", message_log};
                        field_values.add(incident_notes);        
                        
                        String incident_external_id[] = {"external_id",String.valueOf(incident_id)};
                        field_values.add(incident_external_id);        
                        
                        boolean saved_incident = db.save_incident_stub.new_with_history(con, field_values);
                        
                        
                        

                        //update the message status=closed status_date=utc_now  status_by_user_id  related_to=incident  related_to_id=incident_id
                        String query = "UPDATE message SET `status`=?,`status_date`=?, `status_by_user_id`=?,`log`=?,`related_to`=?,`related_to_id`=? WHERE `id`=?";
                        try
                        {
                            stmt = con.prepareStatement(query);
                            stmt.setString(1, "Closed");
                            stmt.setString(2, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(support.date_utils.now_in_utc()));
                            stmt.setString(3, user_id);
                            message_log = message_log + " Message Closed at " + DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(support.date_utils.now_in_utc()) + ".";
                            message_log = message_log + " Incident:" + incident_id + " was created.";
                            stmt.setString(4, message_log); 
                            stmt.setString(5, "incident");
                            stmt.setString(6, String.valueOf(incident_id));
                            stmt.setString(7, message_id); 
                            stmt.executeUpdate(); 
                        }
                        catch(Exception e)
                        {
                            System.out.println("ERROR Exception in servlet users_add_lite do notifications:=" + e);
                        }
                        //redirect to incident.jsp?id=INCIDENT_ID
                        response.sendRedirect("incident.jsp?id=" + incident_id);
                    }
                    else if(action.equalsIgnoreCase("new_request"))
                    {
                        //go to service catalog
                        response.sendRedirect("home_service_catalog.jsp?message_id=" + message_id + "&requested_for_id=" + new_user_id);    
                        //open message in small window

                    }*/
                }
                
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet users_add_lite:=" + e);
                try {
                    PrintWriter out = response.getWriter();
                    json_builder.add("error", e.getMessage());
                    json_builder.add("user_id", "");
                    json = json_builder.build();
                    out.print(json.toString());
                }
                catch(Exception ee)
                {
                    System.out.println("ERROR Exception in servlet users_add_lite:=" + ee.getMessage());
                }
            }
            
            
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
<!--Copyright 2020 Real Data Technologies, Inc. , All rights reserved.-->
 */
package servlets;

import com.google.gson.JsonObject;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.Tika;
import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;

@MultipartConfig()
/**
 *
 * @author Campbellr_2
 */
public class admin_service_catalog_edit extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            Connection con = null;
            PreparedStatement stmt = null;
            //do work here
            try 
            {
                String id = request.getParameter("id");
                String name = request.getParameter("name");
                String description = request.getParameter("description");
                String state = request.getParameter("state");
                String catalog_type = request.getParameter("catalog_type");
                String icon = request.getParameter("icon");
                con = db.db_util.get_contract_connection(context_dir, session);
                stmt = con.prepareStatement("UPDATE service_catalog SET name=?,description=?,state=?,catalog_type=?, icon=? WHERE id=?");
                stmt.setString(1,name);
                stmt.setString(2,description);
                stmt.setString(3,state);
                stmt.setString(4,catalog_type);
                stmt.setString(5,icon);
                stmt.setString(6,id);
                stmt.executeUpdate();
                CouchDbClient dbClient = null;
                LinkedHashMap props = support.config.get_config(context_dir);

                //start get the file attachment if there is one
                try
                {
                    Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
                    if (filePart != null)
                    {
                        try
                        {
                            dbClient = db.couchdb.get_client(props);
                            if (dbClient == null)
                            {
                                throw new Exception("dbClient is null");
                            }
                            String file_name;
                            Tika tika = new Tika();

                            String fileName = filePart.getSubmittedFileName();
                            logger.info("filename:" + fileName);
                            logger.info("content-type:" + filePart.getContentType());
                            logger.info("content-size:" + filePart.getSize());
//                            if (fileName != null && !"".equals(fileName)) 
                            long file_size = filePart.getSize();
                            if (file_size > 0) 
                            {
                                String mimeType = tika.detect(filePart.getInputStream());
                                String request_mime_type = filePart.getContentType();
                                if (request_mime_type.equals("image/svg+xml")) {
                                    mimeType = request_mime_type;
                                }
                                file_name = "image";

                                String service_catalog_info[] = db.get_service_catalog.service_catalog_by_id(con, id);
                                String uuid = "";
                                String rev = "";
                                int new_uuid = 0;
                                Response dbclient_response;

                                UUID Uuid = UUID.randomUUID(); //Generates random UUID                                  
                                
                                String user_id = session.getAttribute("user_id").toString();
                                java.util.Date now = new java.util.Date();

                                Map<String, Object> map = new HashMap<>(); 
                                map.put("Filename", file_name);
                                map.put("MIME_type", mimeType);
                                map.put("FileSize", file_size);
                                map.put("DateUploaded", now);
                                map.put("UploadedBy", user_id);
                                map.put("AssociatedObject", "Service Catalog");
                                map.put("AssociatedObjectId", id);

                                if (service_catalog_info[6] != null && !service_catalog_info[6].equals(""))
                                {
                                    try
                                    {
                                        JsonObject json = dbClient.find(JsonObject.class, service_catalog_info[6]);        
                                        rev = json.get("_rev").getAsString();
                                        uuid = service_catalog_info[6];
                                        map.put("_id", uuid);
                                        map.put("_rev", rev);
                                        dbclient_response = dbClient.update(map);
                                        rev = dbclient_response.getRev();
                                    } catch (Exception e)
                                    {
                                        logger.error("Existing CouchDB doc with uuid " + service_catalog_info[6] + " can't be found. The new doc will be created. Error is: " + e.getMessage());   
                                    }
                                }
                                if (rev.equals("")) {
                                    new_uuid = 1;
                                    uuid = Uuid.toString();
                                    map.put("_id", uuid);
                                    map.remove("_rev");
                                    dbclient_response = dbClient.save(map);
                                    rev = dbclient_response.getRev();
                                }

                                dbclient_response = dbClient.saveAttachment(filePart.getInputStream(), file_name, mimeType, uuid, rev);

                                if (dbclient_response.getError() != null)
                                {
                                    throw new Exception("CouchDB attachment upload error: " + dbclient_response.getError());
                                }

                                if (new_uuid > 0)
                                {
                                    String query_image = "UPDATE service_catalog SET image = ? WHERE id = ?";
                                    PreparedStatement stmt_attachment = con.prepareStatement(query_image);
                                    stmt_attachment.setString(1, uuid);
                                    stmt_attachment.setString(2, id);
                                    stmt_attachment.execute();

                                    int rows_affected = stmt_attachment.executeUpdate();
                                    if (rows_affected <= 0)
                                    {
                                        throw new Exception("Image field is somehow not updated in the service_catalog_table");
                                    }
                                }
                            }
                        } catch (Exception e)
                        {
                            logger.error("ERROR attachments process in servlet service_catalog_edit:=" + e);
                        }

                    }
                }
                catch(Exception e)
                {
                    logger.error("ERROR Exception image saving in servlet admin_service_catalog:=" + e);
                }                
                
                String delete_uuid = StringUtils.defaultString(request.getParameter("image_delete"));
                if (!delete_uuid.equals(""))
                {
                    try
                    {                     
                        dbClient = db.couchdb.get_client(props);
                        if (dbClient == null)
                        {
                            throw new Exception("dbClient is null");
                        }
                        String rev;

                        JsonObject json = dbClient.find(JsonObject.class, delete_uuid);        
                        rev = json.get("_rev").getAsString();
                        dbClient.remove(delete_uuid, rev);

                        String query_image = "UPDATE service_catalog SET image = null WHERE id = ?";
                        PreparedStatement stmt_attachment = con.prepareStatement(query_image);
                        stmt_attachment.setString(1, id);
                        stmt_attachment.execute();

                    } catch (Exception e)
                    {
                        logger.error("ERROR image deletion in servlet service_catalog_edit:=" + e);
                    }
                }


            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_service_catalog_edit:=" + e);
            } finally {
                try
                {
                    if (stmt != null) stmt.close();
                    if (con != null) con.close();
                } catch (SQLException ex)
                {
                    java.util.logging.Logger.getLogger(admin_service_catalog_edit.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            response.sendRedirect("admin_service_catalog.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

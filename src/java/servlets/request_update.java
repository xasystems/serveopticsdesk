/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.GregorianCalendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@MultipartConfig()
/**
 *
 * @author Ralph
 */
public class request_update extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
    private static SimpleDateFormat form_format = new SimpleDateFormat("HH:mm / dd-MM-YY"); //10:42 / 24-Jan-20
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            String user_tz_name = "US/Eastern";
            String user_tz_time = "-05:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "US/Eastern";
                    user_tz_time = "-05:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "US/Eastern";
                user_tz_time = "-05:00";
            }  
            String now_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(support.date_utils.now_in_utc());
            
            
            String user_id = session.getAttribute("user_id").toString();
            try
            {      
                //get the old and new record info
                String context_dir = request.getServletContext().getRealPath("");
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                //get all the parameters
                String request_id = request.getParameter("id");
                String current_record[] = db.get_requests.request_by_id(con, request_id);
                //System.out.println("current_record[1]=" + current_record[1]);
                int current_rev = Integer.parseInt(current_record[1]);                
                int this_rev = current_rev + 1;
                //System.out.println("current_rev=" + current_rev + " this_rev=" + this_rev);
                
                String this_rev_date = now_date;
                int this_rev_by_user_id = Integer.parseInt(user_id);
                int current_service_catalog_id = Integer.parseInt(current_record[4]);
                int this_service_catalog_id = current_service_catalog_id;
                int current_service_catalog_item_id = Integer.parseInt(current_record[5]);
                int this_service_catalog_item_id = current_service_catalog_item_id;                                        
                
                String create_date = current_record[6];              
                //System.out.println("pre=" + current_record[6] + "  create_date=" + create_date);
                
                String current_request_date = current_record[7];   
                
                //System.out.println("request.getParameter(\"request_date\")=" + request.getParameter("request_date"));
                String this_request_date = timestamp_format.format(date_time_picker_format.parse(request.getParameter("request_date")).getTime());           
                //System.out.println("request.getParameter(\"request_date\")=" + this_request_date);
                
                String current_resolve_date = current_record[9]; 
                String this_resolve_date = null;
                try
                {
                    //this_resolve_date = timestamp_format.format(date_time_picker_format.parse(request.getParameter("resolve_date")).getTime()); 
                    String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("resolve_date")).getTime()));
                    ZonedDateTime in_time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                    this_resolve_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(in_time_utc);
                    
                }
                catch(Exception e)
                {
                    System.out.println("Exception in request_update resolve_date=" + e);
                    this_resolve_date = null;
                }
                String current_closed_date = current_record[10]; 
                String this_closed_date = null;
                try
                {
                    //current_closed_date = timestamp_format.format(timestamp_format.parse(current_record[10]).getTime());
                    String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("closed_date")).getTime()));
                    ZonedDateTime in_time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                    this_closed_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(in_time_utc);
                }  
                catch(Exception e)
                {
                    this_closed_date = null;
                }
                
                System.out.println("post_dates");
                int current_assigned_group_id = 0;
                try
                {
                    current_assigned_group_id = Integer.parseInt(current_record[11]);
                }
                catch(Exception e)
                {
                    current_assigned_group_id = 0;
                }
                int this_assigned_group_id = 0;
                try
                {
                    this_assigned_group_id = Integer.parseInt(request.getParameter("assigned_group_id"));
                }
                catch(Exception e)
                {
                    this_assigned_group_id = 0;
                }
                int current_assigned_to_id = 0;
                try
                {
                    current_assigned_to_id = Integer.parseInt(current_record[12]);
                }
                catch(Exception e)
                {
                    current_assigned_to_id = 0;
                }
                int this_assigned_to_id = 0;
                try
                {
                    this_assigned_to_id = Integer.parseInt(request.getParameter("assigned_to_id"));
                }
                catch(Exception e)
                {
                    this_assigned_to_id = 0;
                }
                int current_create_by_id = 0;
                try
                {
                    current_create_by_id = Integer.parseInt(current_record[13]);
                }
                catch(Exception e)
                {
                    current_create_by_id = 0;
                }
                int this_create_by_id = 0;
                try
                {
                    this_create_by_id = Integer.parseInt(request.getParameter("create_by_id"));
                }
                catch(Exception e)
                {
                    this_create_by_id = 0;
                }
                
                int current_requested_for_id = 0;
                try
                {
                    current_requested_for_id = Integer.parseInt(current_record[14]);
                }
                catch(Exception e)
                {
                    current_requested_for_id = 0;
                }
                int this_requested_for_id = 0;
                try
                {
                    this_requested_for_id = Integer.parseInt(request.getParameter("requested_for_id"));
                }
                catch(Exception e)
                {
                    this_requested_for_id = 0;
                }                
                int current_requested_for_group_id = 0;
                try
                {
                    current_requested_for_group_id = Integer.parseInt(current_record[15]);
                }
                catch(Exception e)
                {
                    current_requested_for_group_id = 0;
                }
                int this_requested_for_group_id = 0;
                try
                {
                    this_requested_for_group_id = Integer.parseInt(request.getParameter("requested_for_group_id"));
                }
                catch(Exception e)
                {
                    this_requested_for_group_id = 0;
                } 
                System.out.println("post ints");
                String current_location = current_record[16];
                String this_location = request.getParameter("location");
                
                String current_department = current_record[17];
                String this_department = request.getParameter("department");
                
                String current_site = current_record[18];
                String this_site = request.getParameter("site");
                
                String current_company = current_record[19];
                String this_company = request.getParameter("company");
                
                String current_impact = current_record[20];
                String this_impact = request.getParameter("impact");
                
                String current_urgency = current_record[21];
                String this_urgency = request.getParameter("urgency");
                
                String current_priority = current_record[22];
                String this_priority = request.getParameter("priority");
                
                String current_state = current_record[23];
                String this_state = request.getParameter("state");
                
                String current_contact_method = current_record[24];
                String this_contact_method = request.getParameter("contact_method");
                
                String current_approval = current_record[25];
                String this_approval = request.getParameter("approval");
                
                Double current_price = Double.parseDouble(current_record[26]);
                Double this_price = current_price;
                
                String current_notes = current_record[27];                
                String escaped_notes = StringEscapeUtils.escapeHtml4(request.getParameter("notes")); //turn it back to html
                
                String current_desk_notes = current_record[28];
                String escaped_desk_notes = StringEscapeUtils.escapeHtml4(request.getParameter("desk_notes")); //turn it back to html
                
                String current_description = current_record[29];
                String this_description = current_description;
                
                String current_closed_reason = current_record[30];
                String this_closed_reason = request.getParameter("closed_reason");
                
                String current_required_info = current_record[31];
                String this_required_info = current_required_info;
                
                String current_required_info_response = current_record[32];
                String this_required_info_response = request.getParameter("required_info_response");
                
                String current_optional_info = current_record[33];
                String this_optional_info = current_optional_info;
                
                String current_optional_info_response = current_record[34];
                String this_optional_info_response = request.getParameter("optional_info_response");
                
                //get request_item info so we can determine the due date
                System.out.println("current_service_catalog_item_id=" + String.valueOf(current_service_catalog_item_id));
                String requested_item_info[] = db.get_service_catalog.service_catalog_items_by_id(con, String.valueOf(current_service_catalog_item_id));   
                
                //calulate the due_date and price
                String temp_date = timestamp_format.format(date_time_picker_format.parse(request.getParameter("request_date")));
                ZonedDateTime zdt_temp_date = support.date_utils.user_tz_to_utc(user_tz_name, temp_date);
                
                String this_due_date = support.sla_request_calc.due_date_for_request_id(con, String.valueOf(current_service_catalog_item_id), this_priority, zdt_temp_date);
                System.out.println("post  this_due_date");
                int amount = 0;
                String unit = "";
                GregorianCalendar cal = new GregorianCalendar();                
                // commenting this block due to Markup issue https://app.markup.io/markup/7a9aa95b-4b57-4f14-af59-dcfb6ba64bb9
//                if(this_priority.equalsIgnoreCase("Critical"))
//                {
//                    try{this_assigned_group_id = Integer.parseInt(requested_item_info[13]);}catch (Exception e){this_assigned_group_id = 0;}
//                    try{this_price = Double.parseDouble(requested_item_info[7]);}catch (Exception e){this_price = 0.00;}
//                }
//                else if(this_priority.equalsIgnoreCase("High"))
//                {
//                    try{this_assigned_group_id = Integer.parseInt(requested_item_info[19]);}catch (Exception e){this_assigned_group_id = 0;}
//                    try{this_price = Double.parseDouble(requested_item_info[14]);}catch (Exception e){this_price = 0.00;}
//                }
//                else if(this_priority.equalsIgnoreCase("Medium"))
//                {
//                    try{this_assigned_group_id = Integer.parseInt(requested_item_info[24]);}catch (Exception e){this_assigned_group_id = 0;}
//                    try{this_price = Double.parseDouble(requested_item_info[19]);}catch (Exception e){this_price = 0.00;}
//                }
//                else if(this_priority.equalsIgnoreCase("Low"))
//                {
//                    try{this_assigned_group_id = Integer.parseInt(requested_item_info[30]);}catch (Exception e){this_assigned_group_id = 0;}
//                    try{this_price = Double.parseDouble(requested_item_info[25]);}catch (Exception e){this_price = 0.00;}
//                }
                System.out.println("pre update request");
                String query = "UPDATE request SET rev=?,rev_date=?,rev_by_user_id=?,service_catalog_id=?,service_catalog_item_id=?,request_date=?,due_date=?,resolve_date=?,closed_date=?,assigned_group_id=?,assigned_to_id=?,create_by_id=?,requested_for_id=?,requested_for_group_id=?,location=?,department=?,site=?,company=?,impact=?,urgency=?,priority=?,state=?,contact_method=?,approval=?,price=?,notes=?,desk_notes=?,description=?,closed_reason=?,required_info=?,required_info_response=?,optional_info=?,optional_info_response=? WHERE id=?";
                PreparedStatement stmt = con.prepareStatement(query);
                stmt.setInt(1, this_rev); //rev
                stmt.setString(2, this_rev_date);
                stmt.setInt(3, this_rev_by_user_id); //rev_by_user_id
                stmt.setInt(4, this_service_catalog_id); //service_catalog_id
                stmt.setInt(5, this_service_catalog_item_id); //service_catalog_item_id
                stmt.setString(6, this_request_date); //request_date
                stmt.setString(7, this_due_date); //due_date
                stmt.setString(8, this_resolve_date); //resolve_date
                stmt.setString(9, this_closed_date); //closed_date
                stmt.setInt(10, this_assigned_group_id); //assigned_group_id
                stmt.setInt(11, this_assigned_to_id); //assigned_to_id
                stmt.setInt(12, this_create_by_id); //create_by_id
                stmt.setInt(13, this_requested_for_id); //requested_for_id
                stmt.setInt(14, this_requested_for_group_id); //requested_for_group_id
                stmt.setString(15, this_location); //location
                stmt.setString(16, this_department); //department
                stmt.setString(17, this_site); //site
                stmt.setString(18, this_company); //company
                stmt.setString(19, this_impact); //impact
                stmt.setString(20, this_urgency); //urgency
                stmt.setString(21, this_priority); //priority
                stmt.setString(22, this_state); //state
                stmt.setString(23, this_contact_method); //contact_method
                stmt.setString(24, this_approval); //approval
                stmt.setDouble(25, this_price); //price
                stmt.setString(26, escaped_notes); //notes
                stmt.setString(27, escaped_desk_notes); //desk_notes
                stmt.setString(28, this_description); //description
                stmt.setString(29, this_closed_reason); //closed_reason
                stmt.setString(30, requested_item_info[31]); //required_info
                stmt.setString(31, this_required_info_response); //required_info_response
                stmt.setString(32, requested_item_info[32]); //required_info
                stmt.setString(33, this_optional_info_response); //required_info_response
                stmt.setString(34, request_id); //id
                System.out.println("req q=" + stmt);
                stmt.execute();
                
                //save the request to the history table
                query = "INSERT INTO request_history (id,rev,rev_date,rev_by_user_id,service_catalog_id,service_catalog_item_id,create_date,request_date,due_date,resolve_date,closed_date,assigned_group_id,assigned_to_id,create_by_id,requested_for_id,requested_for_group_id,location,department,site,company,impact,urgency,priority,state,contact_method,approval,price,notes,desk_notes,description,closed_reason,required_info,required_info_response,optional_info,optional_info_response) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                stmt = con.prepareStatement(query);
                stmt.setString(1, request_id); //id
                stmt.setInt(2, this_rev); //rev
                stmt.setString(3, this_rev_date);
                stmt.setInt(4, this_rev_by_user_id); //rev_by_user_id
                stmt.setInt(5, this_service_catalog_id); //service_catalog_id
                stmt.setInt(6, this_service_catalog_item_id); //service_catalog_item_id     
                stmt.setString(7, create_date); //request_date
                stmt.setString(8, this_request_date); //request_date
                stmt.setString(9, this_due_date); //due_date
                stmt.setString(10, this_resolve_date); //resolve_date
                stmt.setString(11, this_closed_date); //closed_date
                stmt.setInt(12, this_assigned_group_id); //assigned_group_id
                stmt.setInt(13, this_assigned_to_id); //assigned_to_id
                stmt.setInt(14, this_create_by_id); //create_by_id
                stmt.setInt(15, this_requested_for_id); //requested_for_id
                stmt.setInt(16, this_requested_for_group_id); //requested_for_group_id
                stmt.setString(17, this_location); //location
                stmt.setString(18, this_department); //department
                stmt.setString(19, this_site); //site
                stmt.setString(20, this_company); //company
                stmt.setString(21, this_impact); //impact
                stmt.setString(22, this_urgency); //urgency
                stmt.setString(23, this_priority); //priority
                stmt.setString(24, this_state); //state
                stmt.setString(25, this_contact_method); //contact_method
                stmt.setString(26, this_approval); //approval
                stmt.setDouble(27, this_price); //price
                stmt.setString(28, escaped_notes); //notes
                stmt.setString(29, escaped_desk_notes); //desk_notes
                stmt.setString(30, this_description); //description
                stmt.setString(31, this_closed_reason); //closed_reason
                stmt.setString(32, requested_item_info[31]); //required_info
                stmt.setString(33, this_required_info_response); //required_info_response
                stmt.setString(34, requested_item_info[32]); //required_info
                stmt.setString(35, this_optional_info_response); //required_info_response
                System.out.println("req history q=" + stmt);
                stmt.execute();
                
                //do global notification
                //request_created_by_update
                if(!this_state.contains("Closed")) //this is created by update
                {
                    String request_created_by_update[] = db.get_notifications.specific_global(con, "request_created_by_update");
                    if(request_created_by_update[2].equalsIgnoreCase("true"))
                    {
                        //get subject
                        String request_subject_format = StringEscapeUtils.unescapeHtml4(request_created_by_update[3]);
                        String subject = support.format_email.text(con, "request", String.valueOf(request_id), request_subject_format, user_tz_time);
                        //get body
                        String request_body_format = StringEscapeUtils.unescapeHtml4(request_created_by_update[4]);
                        String body = support.format_email.text(con, "request", String.valueOf(request_id), request_body_format, user_tz_time);

                        String user_info[] = db.get_users.by_id(con, String.valueOf(this_create_by_id));
                        String email = user_info[15];
                        if(!email.equalsIgnoreCase(""))
                        {
                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                        }
                    } 
                    //now do requested_for                    
                    String request_requester_update[] = db.get_notifications.specific_global(con, "request_requester_update");
                    if(request_requester_update[2].equalsIgnoreCase("true"))
                    {
                        //get subject
                        String request_subject_format = StringEscapeUtils.unescapeHtml4(request_created_by_update[3]);
                        String subject = support.format_email.text(con, "request", String.valueOf(request_id), request_subject_format, user_tz_time);
                        //get body
                        String request_body_format = StringEscapeUtils.unescapeHtml4(request_created_by_update[4]);
                        String body = support.format_email.text(con, "request", String.valueOf(request_id), request_body_format, user_tz_time);

                        String user_info[] = db.get_users.by_id(con, String.valueOf(this_requested_for_id));
                        String email = user_info[15];
                        if(!email.equalsIgnoreCase(""))
                        {
                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                        }
                    } 
                    
                    
                }
                else if(this_state.contains("Closed")) //this is created by closed
                {
                    String request_created_by_close[] = db.get_notifications.specific_global(con, "request_created_by_close");
                    if(request_created_by_close[2].equalsIgnoreCase("true"))
                    {
                        //get subject
                        String request_subject_format = StringEscapeUtils.unescapeHtml4(request_created_by_close[3]);
                        String subject = support.format_email.text(con, "request", String.valueOf(request_id), request_subject_format, user_tz_time);
                        //get body
                        String request_body_format = StringEscapeUtils.unescapeHtml4(request_created_by_close[4]);
                        String body = support.format_email.text(con, "request", String.valueOf(request_id), request_body_format, user_tz_time);

                        String user_info[] = db.get_users.by_id(con, String.valueOf(this_create_by_id));
                        String email = user_info[15];
                        if(!email.equalsIgnoreCase(""))
                        {
                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                        }
                    } 
                    //now do requested_for
                    String request_requester_close[] = db.get_notifications.specific_global(con, "request_requester_close");
                    if(request_requester_close[2].equalsIgnoreCase("true"))
                    {
                        //get subject
                        String request_subject_format = StringEscapeUtils.unescapeHtml4(request_requester_close[3]);
                        String subject = support.format_email.text(con, "request", String.valueOf(request_id), request_subject_format, user_tz_time);
                        //get body
                        String request_body_format = StringEscapeUtils.unescapeHtml4(request_requester_close[4]);
                        String body = support.format_email.text(con, "request", String.valueOf(request_id), request_body_format, user_tz_time);

                        String user_info[] = db.get_users.by_id(con, String.valueOf(this_requested_for_id));
                        String email = user_info[15];
                        if(!email.equalsIgnoreCase(""))
                        {
                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                        }
                    } 
                }
                
                String RedirectURL = "request_list.jsp";
                response.sendRedirect(RedirectURL);  
                
            }
            catch(Exception e)
            {
                e.printStackTrace();
                logger.error("ERROR Exception in servlet request_update:=" + e);
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

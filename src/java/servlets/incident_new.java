/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Campbellr_2
 */
public class incident_new extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();    
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
    private static SimpleDateFormat form_format = new SimpleDateFormat("HH:mm / dd-MMM-YY"); //10:42 / 24-Jan-20
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            String user_tz_name = "US/Eastern";
            String user_tz_time = "-05:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "US/Eastern";
                    user_tz_time = "-05:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "US/Eastern";
                user_tz_time = "-05:00";
            }
            int id = 0;
            String state = "New";
            int caller_id = 0;
            int caller_group_id = 0;
            String caller_username = "";
            int create_by_id = 0;
            String contact_method = "Phone";
            String location = "";
            String department = "";
            String site = "";
            String company = "";
            String priority = "";
            String impact = "";
            String urgency = "";

            int category_id = 0;
            String category = "";
            String subcategory = "";
            String ci_id = "";
            int assigned_group_id = 0;
            int assigned_to_id = 0;
            String first_contact_resolution = "false";
            String description = "";
            String notes = "";
            String desk_notes = "";
            String related = "";
            String closed_reason = "";
            java.util.Date now = new java.util.Date();
            //java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
            String incident_time = "";
            String create_date = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
            String state_date = create_date;
            String closed_date = null;
            String pending_date = null;
            String pending_reason = "";
            int external_id = 0;

            if (session.getAttribute("authenticated") == null) 
            {
                response.sendRedirect("index.jsp");
            }
            else
            {
                //do work here
                try
                {                
                    String context_dir = request.getServletContext().getRealPath("");
                    Connection con = db.db_util.get_contract_connection(context_dir, session);
                    //get all the parameters
                    id = db.get_incidents.next_incident_id(con);
                    external_id = id;

                    int copy = NumberUtils.toInt(request.getParameter("copy"));
                    if (copy == 0) {
                        //incident_time
                        try
                        {
                            //must convert this time from users TZ to UTC
                            String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("incident_time")).getTime()));
                            System.out.println("Raw incident_time=" + request.getParameter("incident_time"));
                            ZonedDateTime incident_time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);                        
                            incident_time = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(incident_time_utc);
                            System.out.println("UTC incident_time=" + incident_time);
                        }
                        catch(Exception e)
                        {

                            String in_time = timestamp_format.format(new java.sql.Timestamp(now.getTime()));
                            ZonedDateTime incident_time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                            incident_time = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(incident_time_utc);
                        }
                        try{state = request.getParameter("state");}catch (Exception e){state = "New";}
                        try{caller_id = Integer.parseInt(request.getParameter("caller_id"));}catch (Exception e){caller_id=0;}
                        //caller_username:Abra.Marshall2 //not needed
                        //caller_group_id
                        try{caller_group_id = Integer.parseInt(request.getParameter("caller_group_id"));}catch (Exception e){caller_group_id=0;}
                        try{create_by_id = Integer.parseInt(request.getParameter("create_by_id"));}catch (Exception e){create_by_id=0;}
                        //create_by_username:Vera.Mcfarland154 //not needed
                        try{contact_method = request.getParameter("contact_method");}catch (Exception e){contact_method = "Phone";}
                        try{location = request.getParameter("location");}catch (Exception e){location = "";}
                        try{department = request.getParameter("department");}catch (Exception e){department = "";}
                        try{site = request.getParameter("site");}catch (Exception e){site = "";}
                        try{company = request.getParameter("company");}catch (Exception e){company = "";}
                        try{priority = request.getParameter("priority");}catch (Exception e){priority = "";}
                        try{impact = request.getParameter("impact");}catch (Exception e){impact = "";}
                        try{urgency = request.getParameter("urgency");}catch (Exception e){urgency = "";}
                        //create_date
                        //Create date already defined at declaration
                        try{related = request.getParameter("related");}catch (Exception e){related = "";}//related
                        try{category_id = Integer.parseInt(request.getParameter("category_id"));}catch (Exception e){category_id=0;}
                        category = db.get_category.from_category_id(con, category_id);
                        try{subcategory = request.getParameter("subcategory");}catch (Exception e){subcategory="";}
                        try{ci_id = request.getParameter("ci_id");}catch (Exception e){ci_id = "";}
                        try{assigned_group_id = Integer.parseInt(request.getParameter("assigned_group_id"));}catch (Exception e){assigned_group_id=0;}
                        //assigned_group_name:Helpdesk
                        try{assigned_to_id = Integer.parseInt(request.getParameter("assigned_to_id"));}catch (Exception e){assigned_to_id=0;}
                        //assigned_to_name:Ralph.Byers2783  //not needed
                        try
                        {
                            first_contact_resolution = request.getParameter("first_contact_resolution");
                            if(first_contact_resolution == null || first_contact_resolution.equalsIgnoreCase("null"))
                            {
                                first_contact_resolution="false";
                            }
                            else
                            {
                                first_contact_resolution="true";
                            }
                        }
                        catch (Exception e)
                        {
                            first_contact_resolution="false";
                        }
                        try{description = request.getParameter("description");}catch (Exception e){description="";}
                        try{notes = request.getParameter("notes");}catch (Exception e){notes="";}
                        try{desk_notes = request.getParameter("desk_notes");}catch (Exception e){desk_notes="";}                

                        //closed_date=28,
                        try
                        {
                            String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("closed_date")).getTime()));
                            ZonedDateTime closed_date_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                            closed_date = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(closed_date_utc);
                        }
                        catch(Exception e)
                        {
                            closed_date = null;
                        }
                        //closed_reason=29,
                        try{closed_reason = request.getParameter("closed_reason");}catch (Exception e){closed_reason="";}
                        //pending_date=30,
                        try
                        {
                            String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("pending_date")).getTime()));
                            ZonedDateTime pending_date_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                            pending_date = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(pending_date_utc);
                        }
                        catch(Exception e)
                        {
                            pending_date = null;
                        }
                        //pending_reason31,
                        try{pending_reason = request.getParameter("pending_reason");}catch (Exception e){pending_reason="";}

                    } else {
                        SimpleDateFormat db_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String[] incident = db.get_incidents.incident_by_id(con, String.valueOf(copy));
                        incident_time = incident[2];
                        caller_id = Integer.parseInt(incident[3]);
                        caller_group_id = Integer.parseInt(incident[4]);
                        location = incident[5];
                        department = incident[6];
                        site = incident[7];
                        company = incident[8];
                        category = incident[9];
                        subcategory = incident[10];
                        ci_id = incident[11];
                        impact = incident[12];
                        urgency = incident[13];
                        priority = incident[14];
                        description = incident[15];                        
                        create_date = incident[16].equals("") ? null : db_format.format(timestamp_format.parse(incident[16]));
                        create_by_id = Integer.parseInt(incident[17]);
                        contact_method = incident[18];
                        state = incident[19];
                        state_date = incident[20].equals("") ? null : db_format.format(timestamp_format.parse(incident[20]));
                        assigned_group_id = Integer.parseInt(incident[21]);
                        assigned_to_id = Integer.parseInt(incident[22]);
                        notes = incident[23];
                        desk_notes = incident[24];
                        related = incident[25];
                        first_contact_resolution = incident[26];
                        closed_date = incident[27].equals("") ? null : db_format.format(timestamp_format.parse(incident[27]));
                        closed_reason = incident[28];
                        pending_date = incident[29].equals("") ? null : db_format.format(timestamp_format.parse(incident[29]));
                        pending_reason = incident[30];
                        external_id = Integer.parseInt(incident[31]);
                    }


                    /*submit:Submit
                    ci_id:
                    incident_time:11:33 / 26-Jan-20
                    state:New
                    caller_id:
                    caller_username:
                    caller_group_id:
                    caller_group_name:Help desk
                    created_by_id:
                    created_by_username:
                    contact_method:
                    location:
                    department:
                    site:
                    company:
                    priority:
                    category_id:
                    subcategory:
                    assigned_group_id:0
                    assigned_group_name:Group Unassigned
                    assigned_to_id:2
                    assigned_to_name:Abra.Marshall2
                    description:
                    notes:
                    desk_notes:*/


                    String insert_incidents_table = "INSERT INTO incidents (id,incident_type,incident_time,caller_id,caller_group_id,location,department,site,company,category,subcategory,ci_id,impact,urgency,priority,description,create_date,create_by_id,contact_method,state,state_date,assigned_group_id,assigned_to_id,notes,desk_notes,related,first_contact_resolution,closed_date,closed_reason,pending_date,pending_reason,external_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    String insert_incident_history_table = "INSERT INTO incident_history (incident_id,incident_type,incident_time,caller_id,caller_group_id,location,department,site,company,category,subcategory,ci_id,impact,urgency,priority,description,create_date,create_by_id,contact_method,state,state_date,assigned_group_id,assigned_to_id,notes,desk_notes,related,first_contact_resolution,closed_date,closed_reason,pending_date,pending_reason,external_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                    
                    /*
                    id=1,
                    incident_type=2,
                    incident_time=3,
                    caller_id=4,
                    caller_group_id=5,
                    location=6,
                    department=7,
                    site=8,
                    company=9,
                    category=10,
                    subcategory=11,
                    ci_id=12,
                    impact=13,
                    urgency=14,
                    priority=15,
                    description=16,
                    create_date=17,
                    create_by_id=18,
                    contact_method=19,
                    state=20,
                    state_date=21,
                    assigned_group_id=22,
                    assigned_to_id=23,
                    notes=24,
                    desk_notes=25,
                    related=26,
                    first_contact_resolution=27,
                    closed_date=28,
                    closed_reason=29,
                    pending_date=30,
                    pending_reason31,
                    external_id=32
                    */

                    PreparedStatement stmt = con.prepareStatement(insert_incidents_table);
                    stmt.setInt(1, id); //id
                    stmt.setString(2,""); //incident_type
                    stmt.setString(3, incident_time);//incident_time
                    stmt.setInt(4, caller_id);//caller_id
                    stmt.setInt(5, caller_group_id);//caller_group_id
                    stmt.setString(6,location); //location
                    stmt.setString(7,department); //department
                    stmt.setString(8,site);//site=8
                    stmt.setString(9,company);//company=9,
                    stmt.setString(10,category);//category=10,
                    stmt.setString(11,subcategory);//subcategory=11,
                    stmt.setString(12,ci_id);//ci_id=12,
                    stmt.setString(13,impact);//impact=13,
                    stmt.setString(14,urgency);//urgency=14,
                    stmt.setString(15,priority);//priority=15,
                    stmt.setString(16,description);//description=16,
                    stmt.setString(17,create_date);//create_date=17,
                    stmt.setInt(18,create_by_id);//create_by_id=18,
                    stmt.setString(19,contact_method);//contact_method=19,
                    stmt.setString(20,state);//state=20,
                    stmt.setString(21,state_date);//state_date=20,
                    stmt.setInt(22,assigned_group_id);//assigned_group_id=22,
                    stmt.setInt(23,assigned_to_id);//assigned_to_id=23,
                    stmt.setString(24,notes);//notes=24,
                    stmt.setString(25,desk_notes);//desk_notes=25,
                    stmt.setString(26,related);//related=26,
                    stmt.setString(27,first_contact_resolution);//first_contact_resolution=27,
                    stmt.setString(28,closed_date);//closed_date=28,
                    stmt.setString(29,closed_reason);//closed_reason=29,
                    stmt.setString(30,pending_date);//pending_date=30,
                    stmt.setString(31,pending_reason);//pending_reason=31,
                    stmt.setInt(32, id);//external_id=32    
                    stmt.execute();
                    
                    PreparedStatement stmt2 = con.prepareStatement(insert_incident_history_table);
                    stmt2.setInt(1, id); //id
                    stmt2.setString(2,""); //incident_type
                    stmt2.setString(3, incident_time);//incident_time
                    stmt2.setInt(4, caller_id);//caller_id
                    stmt2.setInt(5, caller_group_id);//caller_group_id
                    stmt2.setString(6,location); //location
                    stmt2.setString(7,department); //department
                    stmt2.setString(8,site);//site=8
                    stmt2.setString(9,company);//company=9,
                    stmt2.setString(10,category);//category=10,
                    stmt2.setString(11,subcategory);//subcategory=11,
                    stmt2.setString(12,ci_id);//ci_id=12,
                    stmt2.setString(13,impact);//impact=13,
                    stmt2.setString(14,urgency);//urgency=14,
                    stmt2.setString(15,priority);//priority=15,
                    stmt2.setString(16,description);//description=16,
                    stmt2.setString(17,create_date);//create_date=17,
                    stmt2.setInt(18,create_by_id);//create_by_id=18,
                    stmt2.setString(19,contact_method);//contact_method=19,
                    stmt2.setString(20,state);//state=20,
                    stmt2.setString(21,state_date);//state_date=20,
                    stmt2.setInt(22,assigned_group_id);//assigned_group_id=22,
                    stmt2.setInt(23,assigned_to_id);//assigned_to_id=23,
                    stmt2.setString(24,notes);//notes=24,
                    stmt2.setString(25,desk_notes);//desk_notes=25,
                    stmt2.setString(26,related);//related=26,
                    stmt2.setString(27,first_contact_resolution);//first_contact_resolution=27,
                    stmt2.setString(28,closed_date);//closed_date=28,
                    stmt2.setString(29,closed_reason);//closed_reason=29,
                    stmt2.setString(30,pending_date);//pending_date=30,
                    stmt2.setString(31,pending_reason);//pending_reason=31,
                    stmt2.setInt(32, id);//external_id=32    
                    stmt2.execute();
                    
                    //do custom fields
                    //get active custom fields
                    ArrayList<String[]> active_custom_fields = db.get_custom_fields.active_for_form(con, "incident"); 
                    //System.out.println("Number of custom incident fields=" + active_custom_fields.size());
                    String replace_data = "REPLACE INTO custom_form_fields_data (`form`,`object_id`,`field_name`,`field_value`) VALUES (?,?,?,?)";
                    PreparedStatement stmt3 = con.prepareStatement(replace_data);
                    String insert_history = "INSERT INTO `custom_form_fields_history` (`date`,`form`,`object_id`,`field_name`,`field_value`) VALUES (?,?,?,?,?)";
                    PreparedStatement stmt4 = con.prepareStatement(insert_history);
                    
                    //get the form values
                    for(int a = 0; a < active_custom_fields.size();a++)
                    {
                        String this_custom_field[] = active_custom_fields.get(a);
                        String this_field_name = this_custom_field[3];
                        String this_field_value = "";
                        String this_field_type = this_custom_field[7];
                        String field_value_to_db = "";
                        if (copy == 0) {
                            field_value_to_db = request.getParameter(this_field_name);
                        } else {
                            ArrayList<String[]> db_values = db.get_custom_field_data.active_by_form(con, "incident", String.valueOf(copy));
                            //find the saved value if any
                            for(int b = 0; b < db_values.size(); b++)
                            {
                                String temp[] = db_values.get(b);
                                if(temp[2].equalsIgnoreCase(this_field_name))
                                {
                                    field_value_to_db = temp[3];
                                    break;
                                }
                            }
                        }

                        try
                        {
                            if(this_field_type.equalsIgnoreCase("select"))
                            {
                                this_field_value = support.util.check_for_null(field_value_to_db);                                
                            }
                            else if(this_field_type.equalsIgnoreCase("text"))
                            {
                                this_field_value = support.util.check_for_null(field_value_to_db);                                
                            }
                            else if(this_field_type.equalsIgnoreCase("textarea"))
                            {
                                this_field_value = support.util.check_for_null(field_value_to_db);                                
                            }
                            else if(this_field_type.equalsIgnoreCase("checkbox"))
                            {
                                this_field_value = support.util.check_for_null(field_value_to_db);    
                                if(this_field_value == null)
                                {
                                    this_field_value = "false";
                                }
                                else
                                {
                                    this_field_value = "true";
                                }
                            }
                            else if(this_field_type.equalsIgnoreCase("people-lookup"))
                            {
                                this_field_value = support.util.check_for_null(field_value_to_db);                                
                            }
                            else if(this_field_type.equalsIgnoreCase("group-lookup"))
                            {
                                this_field_value = support.util.check_for_null(field_value_to_db);                                
                            }
                            else if(this_field_type.equalsIgnoreCase("date"))
                            {
                                this_field_value = support.util.check_for_null(field_value_to_db);  
                                if(this_field_value.equalsIgnoreCase(""))
                                {
                                    this_field_value = null;
                                }
                                try
                                {
                                    String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(this_field_value).getTime()));
                                    ZonedDateTime this_field_time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                                    this_field_value = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(this_field_time_utc);
                                }
                                catch (Exception e)
                                {
                                    this_field_value = null;
                                }
                            }
                        }
                        catch(Exception e)
                        {
                            this_field_value = "";
                        }
                        stmt3.setString(1, "incident"); 
                        stmt3.setInt(2, id); 
                        stmt3.setString(3, this_custom_field[3]); 
                        stmt3.setString(4, this_field_value); 
                        stmt3.execute();
                        
                        stmt4.setString(1, create_date);
                        stmt4.setString(2, "incident");
                        stmt4.setInt(3, id); 
                        stmt4.setString(4, this_custom_field[3]);
                        stmt4.setString(5, this_field_value);
                        stmt4.execute();
                    }
                    //do incident new notification
                    
                    //do global notification
                    //caller_incident_create
                    String caller_incident_create[] = db.get_notifications.specific_global(con, "caller_incident_create");
                    if(caller_incident_create[2].equalsIgnoreCase("true"))
                    {
                        //get subject
                        String incident_subject_format = StringEscapeUtils.unescapeHtml4(caller_incident_create[3]);
                        String subject = support.format_email.text(con, "incident", String.valueOf(id), incident_subject_format, user_tz_time);
                        //get body
                        String incident_body_format = StringEscapeUtils.unescapeHtml4(caller_incident_create[4]);
                        String body = support.format_email.text(con, "incident", String.valueOf(id), incident_body_format, user_tz_time);
                        
                        String user_info[] = db.get_users.by_id(con, String.valueOf(caller_id));
                        String email = user_info[15];
                        if(!email.equalsIgnoreCase(""))
                        {
                            //System.out.println("Sending to emailer SUBJECT=" + subject + " BODY=" + body);
                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                        }
                    }  
                    //do personal notifications                    
                    //get incident_create_assigned_to_me  FROM notifications_user WHERE notifications_user.user_id = assigned_to_id
                    //System.out.println("PRE start personal assigned to me");
                    String assigned_to_me_incident_create[] = db.get_notifications.user_notifications_for_user_by_notify_on(con, String.valueOf(assigned_to_id), "incident_create_assigned_to_me");
                    if(assigned_to_me_incident_create[3].equalsIgnoreCase("true"))
                    {
                        //get subject
                        String incident_subject_format = StringEscapeUtils.unescapeHtml4(assigned_to_me_incident_create[4]);
                        String subject = support.format_email.text(con, "incident", String.valueOf(id), incident_subject_format,user_tz_time);
                        //get body
                        String incident_body_format = StringEscapeUtils.unescapeHtml4(assigned_to_me_incident_create[5]);
                        String body = support.format_email.text(con, "incident", String.valueOf(id), incident_body_format, user_tz_time);
                        
                        String user_info[] = db.get_users.by_id(con, String.valueOf(assigned_to_id));
                        String email = user_info[15];
                        if(!email.equalsIgnoreCase(""))
                        {
                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                        }
                    }  
                    //do personal group notifications                    
                    //incident_create_assigned_to_group
                    ArrayList<String[]> create_assigned_to_group = db.get_notifications.user_group_notifications_for_user_by_notify_on(con, String.valueOf(assigned_group_id), "incident_create_assigned_to_group");
                    for(int a = 0; a < create_assigned_to_group.size(); a++)
                    {
                        String this_assigned_to_group[] = create_assigned_to_group.get(a);
                        //get subject
                        String incident_subject_format = StringEscapeUtils.unescapeHtml4(this_assigned_to_group[3]);
                        String subject = support.format_email.text(con, "incident", String.valueOf(id), incident_subject_format,user_tz_time);
                        //get body
                        String incident_body_format = StringEscapeUtils.unescapeHtml4(this_assigned_to_group[4]);
                        String body = support.format_email.text(con, "incident", String.valueOf(id), incident_body_format,user_tz_time);
                        
                        String user_info[] = db.get_users.by_id(con, String.valueOf(this_assigned_to_group[1]));
                        String email = user_info[15];
                        if(!email.equalsIgnoreCase(""))
                        {
                            boolean email_status = db.save_pending_email.now(con, email, subject, body);
                        }
                    }
                    String RedirectURL = "incident_list.jsp?id=" + id ;
                    response.sendRedirect(RedirectURL);
                }
                catch(Exception e)
                {
                    logger.error("ERROR Exception in servlet incident_new:=" + e);
e.printStackTrace();
                }          
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

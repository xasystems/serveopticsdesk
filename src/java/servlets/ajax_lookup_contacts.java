/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
@WebServlet("/ajax_lookup_contacts")
public class ajax_lookup_contacts extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
//        String user_id = request.getParameter("user_id");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

//        String user_id = request.getParameter("user_id");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        JsonObjectBuilder json_builder = Json.createObjectBuilder();
        JsonObject json;
        
        boolean first_record = true;
        boolean has_data = false;
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {

                if (session.getAttribute("authenticated") == null) 
                {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }

                Connection con = db.db_util.get_contract_connection(context_dir, session);
                int starting_record = 0;
                int previous_record = 0;
                int next_record = 0;
                int fetch_size = 50;
                
                try
                {
                    starting_record  = Integer.parseInt(request.getParameter("start"));
                    fetch_size  = Integer.parseInt(request.getParameter("length"));
                    next_record = starting_record + fetch_size;
                    previous_record = starting_record - fetch_size;
                    if(previous_record <= 0)
                    {
                        previous_record = 0;
                    }
                }
                catch(Exception e)
                {
                    starting_record = 0;
                    previous_record = 0;
                    next_record = starting_record + fetch_size;
                }

                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String date_range = "";
                String start = "";
                String end = "";
                java.util.Date filter_start = new java.util.Date();
                java.util.Date filter_end = new java.util.Date();

                try 
                {
                    date_range = request.getParameter("date_range");
                    if (date_range.equalsIgnoreCase("null") || date_range == null) 
                    {
                        date_range = support.filter_dates.past_30_days();
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to past 30 days
                    date_range = support.filter_dates.past_30_days();
                }

                SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
                SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
                try 
                {

                    //split the start and end date  //01/12/2019 12:00 AM - 01/12/2019 11:59 PM
                    String temp[] = date_range.split("-");
                    filter_start = filter_format.parse(temp[0]);
                    filter_end = filter_format.parse(temp[1]);                
                    start = timestamp.format(filter_start);
                    end = timestamp.format(filter_end);
                } 
                catch (Exception e) 
                {
                    //if not set then default to past 30 days
                    System.out.println("ajax_lookup_contacts exception=" + e);
                }

                ArrayList<String[]> jobs = new ArrayList();

                Map<String, String> filters = new HashMap<String, String>();
                String contact_type = StringUtils.defaultString(request.getParameter("contact_type"));
                if (contact_type.equals(""))
                {
                    throw new Exception("contact_type is empty");
                }
                String[][] table_values;
                switch (contact_type)
                {
                    case "phone":
                        table_values =  db.get_contact.contact_home_phone_contacts_table_for_timeframe(con, filter_start, filter_end);
                        break;
                    case "voice_mail":
                        table_values = db.get_contact.contact_home_voice_mail_contacts_table_for_timeframe(con, filter_start, filter_end);
                        break;
                    case "chat":
                        table_values = db.get_contact.contact_home_chat_contacts_table_for_timeframe(con, filter_start, filter_end);
                        break;
                    case "email":
                        table_values = db.get_contact.contact_home_email_contacts_table_for_timeframe(con, filter_start, filter_end);
                        break;
                    default:
                        throw new Exception("contact_type " + contact_type + " is unknown");
                }
                
                int total_filtered_count = 10;
                JsonArrayBuilder json_array_builder = Json.createArrayBuilder();
                for(int a = 0; a < table_values.length;a++)
                {
                    String table_value[] = table_values[a];

                    json_array_builder
                        .add(Json.createObjectBuilder()
                            .add("channel", support.string_utils.check_for_null(table_value[0]))
                            .add("volume", support.string_utils.check_for_null(table_value[1]))
                            .add("avg_asa", support.string_utils.check_for_null(table_value[2]))
                            .add("avg_handle_time", support.string_utils.check_for_null(table_value[3]))
                            .add("avg_duration", support.string_utils.check_for_null(table_value[4]))
                            .add("wrap_up_time", support.string_utils.check_for_null(table_value[5]))
                            .add("abandoned_rate", support.string_utils.check_for_null(table_value[6]) + "%")
                        );
                }
                total_filtered_count = table_values.length;
                json_builder.add("recordsTotal", total_filtered_count);
                json_builder.add("recordsFiltered", total_filtered_count);
                json_builder.add("draw", request.getParameter("draw"));
                json_builder.add("data", json_array_builder);



                json = json_builder.build();

                con.close();
                //send the JSON data
                out.print(json.toString());
            }
            catch(Exception e)
            {
                e.printStackTrace();
                logger.error("ERROR Exception in servlet ajax_lookup_contacts:=" + e);
                stringBuilder.setLength(0);
                stringBuilder.append(e);
                out.print(stringBuilder.toString());

            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class assets_new extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
        
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            String user_tz_name = "US/Eastern";
            String user_tz_time = "-05:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "US/Eastern";
                    user_tz_time = "-05:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "US/Eastern";
                user_tz_time = "-05:00";
            }
            try 
            {
                
                String serial_number = support.util.check_for_null(request.getParameter("serial_number"));
                String asset_tag  = support.util.check_for_null(request.getParameter("asset_tag"));
                String manufacturer  = support.util.check_for_null(request.getParameter("manufacturer"));
                String model = support.util.check_for_null(request.getParameter("model"));
                String asset_type = support.util.check_for_null(request.getParameter("asset_type"));
                String asset_subtype = support.util.check_for_null(request.getParameter("asset_subtype"));
                String purchase_order = support.util.check_for_null(request.getParameter("purchase_order"));
                String warrant_string = support.util.check_for_null(request.getParameter("warranty_expire"));
                String warranty_expire = null;
                try
                {
                    String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("warranty_expire")).getTime()));
                    ZonedDateTime time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                    warranty_expire = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(time_utc);
                }
                catch(Exception e)
                {
                    warranty_expire = null;
                }     
                String state= support.util.check_for_null(request.getParameter("state"));
                String state_date = null;
                try
                {
                    String in_time = timestamp_format.format(new java.sql.Timestamp(date_time_picker_format.parse(request.getParameter("state_date")).getTime()));
                    ZonedDateTime time_utc = support.date_utils.user_tz_to_utc(user_tz_name,in_time);
                    state_date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(time_utc);
                }
                catch(Exception e)
                {
                    state_date = null;
                }
                String assigned_to_id = request.getParameter("assigned_to_id");
                String assigned_group_id= request.getParameter("assigned_group_id");
                String department = support.util.check_for_null(request.getParameter("department"));
                String location = support.util.check_for_null(request.getParameter("location"));
                String site = support.util.check_for_null(request.getParameter("site"));
                String company = support.util.check_for_null(request.getParameter("company"));
                String ip = support.util.check_for_null(request.getParameter("ip"));
                String notes = support.util.check_for_null(request.getParameter("notes"));
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement("INSERT INTO assets (serial_number,asset_tag,manufacturer,model,asset_type,asset_subtype,purchase_order,warranty_expire,state,state_date,assigned_to_id,assigned_group_id,department,location,site,company,ip,notes) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                stmt.setString(1,serial_number);
                stmt.setString(2,asset_tag);
                stmt.setString(3,manufacturer);
                stmt.setString(4,model);
                stmt.setString(5,asset_type);
                stmt.setString(6,asset_subtype);
                stmt.setString(7,purchase_order);
                stmt.setString(8,warranty_expire);
                stmt.setString(9,state);
                stmt.setString(10,state_date);
                stmt.setString(11,assigned_to_id);
                stmt.setString(12,assigned_group_id);
                stmt.setString(13,department);
                stmt.setString(14,location);
                stmt.setString(15,site);
                stmt.setString(16,company);
                stmt.setString(17,ip);
                stmt.setString(18,notes);
                stmt.executeUpdate();
                stmt.close();
                con.close();
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet assets_new:=" + e);
            }
            response.sendRedirect("assets.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

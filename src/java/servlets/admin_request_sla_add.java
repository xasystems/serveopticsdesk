/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class admin_request_sla_add extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            try 
            {
                String name = request.getParameter("name");
                //System.out.println("name=" + name);
                String service_catalog_item_id = request.getParameter("service_catalog_item_id");
                //System.out.println("service_catalog_item_id=" + service_catalog_item_id);
                String description = request.getParameter("description");
                //System.out.println("description=" + description);
                String target = request.getParameter("target");
                //System.out.println("target=" + target);
                String active = request.getParameter("active");
                if(active == null || active.equalsIgnoreCase("null"))
                {
                    active = "0";
                }
                else
                {
                    active = "1";
                }
                //System.out.println("active=" + active);
                String reportable = request.getParameter("reportable");
                if(reportable == null || reportable.equalsIgnoreCase("null"))
                {
                    reportable = "0";
                }
                else
                {
                    reportable = "1";
                }
                
                //System.out.println("reportable=" + reportable);
                String sla_text = request.getParameter("sla_text");
                //System.out.println("sla_text=" + sla_text);
                String request_priority = request.getParameter("request_priority");
                //System.out.println("incident_priority=" + incident_priority);
                String sla_type = request.getParameter("sla_type");
                //System.out.println("sla_type=" + sla_type);
                String sla_timeframe = request.getParameter("sla_timeframe");
                //System.out.println("sla_timeframe=" + sla_timeframe);
                //String schedule_id = request.getParameter("schedule_id");
                String schedule_id = "-1";
                //System.out.println("schedule_id=" + schedule_id);
                //convert into JSON
                //String start_condition = "{\"parameters\":[{\"field_name\":\"start_condition\",\"operator\":\"equal\",\"value\":[\"" + request.getParameter("start_condition") +"\"]}]}" ;
                String start_condition = request.getParameter("start_condition");
                //System.out.println("start_condition=" + start_condition);
                //String pause_condition = "{\"parameters\":[{\"field_name\":\"pause_condition\",\"operator\":\"equal\",\"value\":[\"" + request.getParameter("pause_condition") +"\"]}]}" ;
                String pause_condition = "";
                //System.out.println("pause_condition=" + pause_condition);
                //String stop_condition = "{\"parameters\":[{\"field_name\":\"stop_condition\",\"operator\":\"equal\",\"value\":[\"" + request.getParameter("stop_condition") +"\"]}]}" ;
                String stop_condition = request.getParameter("stop_condition");
                //System.out.println("stop_condition=" + stop_condition);
                String stop_state = request.getParameter("stop_state");
                //System.out.println("stop_condition=" + stop_condition);
                String sla_success_threshold_operator = request.getParameter("sla_success_threshold_operator");
                //System.out.println("sla_success_threshold_operator=" + sla_success_threshold_operator);
                String sla_success_threshold_value = request.getParameter("sla_success_threshold_value");
                //System.out.println("sla_success_threshold_value=" + sla_success_threshold_value);
                String sla_success_threshold_unit = request.getParameter("sla_success_threshold_unit");
                //System.out.println("sla_success_threshold_unit=" + sla_success_threshold_unit);
                String report_name = request.getParameter("report_name");
                //System.out.println("report_name=" + report_name);
                String report_description = request.getParameter("report_description");
                //System.out.println("report_description=" + report_description);
                String report_header = request.getParameter("report_header");
                //System.out.println("report_header=" + report_header);
                String sla_incentive_exceptional = "0";
                String sla_incentive_exceptional_dollars = "0.00";
                String sla_incentive_acceptable = "0";
                String sla_incentive_acceptable_dollars = "0.00";
                String sla_incentive_unacceptable = "0";
                String sla_incentive_unacceptable_dollars = "0.00";
                
                String additional_parameters = "";
                //build up the additional_parameters
                boolean has_additional_parameters = false;
                boolean first_parameter = true;
                StringBuilder sb_parameters = new StringBuilder(); 
                String this_array = "";
                //start the json
                sb_parameters.append("{ \"parameters\" : [");
                
                Enumeration paramNames = request.getParameterNames();
                while(paramNames.hasMoreElements()) 
                {
                    String paramName = (String)paramNames.nextElement();
                    String paramValues[] = request.getParameterValues(paramName);
                    String paramValue = paramValues[0]; 
                    if(paramName.startsWith("field_name~"))
                    {
                        this_array = "";
                        has_additional_parameters = true;
                        //get index
                        //System.out.println("paramName=" + paramName);
                        
                        //start the json array
                        if(!first_parameter)
                        {
                            this_array = ",{\"field_name\" : \"" + paramValue + "\",";
                        }
                        else
                        {
                            this_array = "{\"field_name\" : \"" + paramValue + "\",";
                        }
                        
                        String temp[] = paramName.split("~");
                        //System.out.println("temp[1]=" + temp[1]);
                        //get measure
                        String measure = request.getParameter("field_measure~" + temp[1]);
                        this_array = this_array + " \"operator\" : \"" + measure + "\",";
                                                
                        //get values
                        String values = request.getParameter("field_values~" + temp[1]);
                        //trim the last ~
                        if(values.endsWith("~"))
                        {
                            values = values.substring(0, values.length() -1);
                        }
                        String vals[] = values.split("~");
                        String jvalue = "[";
                        for(int a = 0; a < vals.length; a++)
                        {
                            if(a == 0) //["Security","Test"]
                            {
                                jvalue = jvalue + "\"" + vals[a] + "\"";
                            }
                            else
                            {
                                jvalue = jvalue + ",\"" + vals[a] + "\"";
                            }
                        }
                        jvalue = jvalue + "]";
                        this_array = this_array + " \"value\" : " + jvalue;
                        
                        //close out this_array
                        this_array = this_array + "}";
                        
                        
                        //System.out.println(temp[1] + " field_name=" + paramValue + " measure=" + measure + " values=" + values);
                        first_parameter = false;
                        sb_parameters.append(this_array);
                    }                    
                }
                //end the json
                sb_parameters.append("]}");
                additional_parameters = sb_parameters.toString();
                //System.out.println("additional_parameters=" + additional_parameters);
                
                String q = "INSERT INTO request_sla (name,description,sla_type,sla_text,request_priority,target,start_condition,pause_condition,stop_condition,stop_state,report_name,report_description,active,reportable,report_header,sla_timeframe,schedule_id,sla_success_threshold_operator,sla_success_threshold_value,sla_success_threshold_unit,sla_incentive_exceptional,sla_incentive_exceptional_dollars,sla_incentive_acceptable,sla_incentive_acceptable_dollars,sla_incentive_unacceptable,sla_incentive_unacceptable_dollars,additional_parameters,service_catalog_item_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
   
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement(q);
                stmt.setString(1,name);
                stmt.setString(2,description);
                stmt.setString(3,sla_type);
                stmt.setString(4,sla_text);
                stmt.setString(5,request_priority);
                stmt.setString(6,target);
                stmt.setString(7,start_condition);
                stmt.setString(8,pause_condition);
                stmt.setString(9,stop_condition);
                stmt.setString(10,stop_state);
                stmt.setString(11,report_name);
                stmt.setString(12,report_description);
                stmt.setString(13,active);
                stmt.setString(14,reportable);
                stmt.setString(15,report_header);
                stmt.setString(16,sla_timeframe);
                stmt.setString(17,schedule_id);
                stmt.setString(18,sla_success_threshold_operator);
                stmt.setString(19,sla_success_threshold_value);
                stmt.setString(20,sla_success_threshold_unit);
                stmt.setString(21,sla_incentive_exceptional);
                stmt.setString(22,sla_incentive_exceptional_dollars);
                stmt.setString(23,sla_incentive_acceptable);
                stmt.setString(24,sla_incentive_acceptable_dollars);
                stmt.setString(25,sla_incentive_unacceptable);
                stmt.setString(26,sla_incentive_unacceptable_dollars);
                stmt.setString(27,additional_parameters);  
                stmt.setString(28,service_catalog_item_id);  
                stmt.executeUpdate();
                stmt.close();
                con.close();      
                
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet admin_request_sla_add:=" + e);
            }
            response.sendRedirect("admin_sla.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class admin_category_subcategory_add_subcategory extends HttpServlet 
{

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String query = "";
        String category_id = "";
        try
        {
            HttpSession session = request.getSession();
            String context_dir = request.getServletContext().getRealPath("");
            if (session.getAttribute("authenticated") == null) 
            {
                response.sendRedirect("index.jsp");
            }
            else
            {
                //do work here
                category_id = request.getParameter("category_id");
                String subcategory_active = "false";
                try
                {
                    subcategory_active = request.getParameter("subcategory_active");
                    if(subcategory_active == null)
                    {
                        subcategory_active = "false";
                    }
                    else
                    {
                        subcategory_active = "true";
                    }
                }
                catch(Exception e)
                {
                    subcategory_active = "false";
                }
                String subcategory_name = request.getParameter("subcategory_name");
                
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                //delete all the current settings
                query = "INSERT INTO subcategory (category_id,name,active) VALUES (?,?,?)";
                PreparedStatement stmt = con.prepareStatement(query);
                stmt.setString(1,category_id);
                stmt.setString(2,subcategory_name);
                stmt.setString(3,subcategory_active);
                stmt.executeUpdate();                
                //stmt.close();
                con.close();
            }            
        }
        catch (Exception e) 
        {
            logger.error("ERROR Exception in servlet admin_category_subcategory_add_subcategory:=" + e);
        }
        response.sendRedirect("admin_category_subcategory_edit.jsp?category_id=" + category_id);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

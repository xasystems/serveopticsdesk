/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Enumeration;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class survey_submit extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String survey_key = request.getParameter("survey_key");
        String id = request.getParameter("id");
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        try 
        {
            JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
            JsonArrayBuilder answer_array = Json.createArrayBuilder();
            
            Enumeration<String> parameterNames = request.getParameterNames();
            while (parameterNames.hasMoreElements()) 
            {
                String parameter_name = parameterNames.nextElement();
                String value = request.getParameter(parameter_name);
                //name="question-1-true_false"   
                if(parameter_name.startsWith("question"))
                {
                    //System.out.println("question"
                    String temp[] = parameter_name.split("-");
                    String question_number = temp[1];
                    String question_type = temp[2];
                    String answer_value = value;
                    
                    // create an array of key-value pairs
                    
                    // create each key-value pair as seperate object and add it to the array
                    answer_array.add(Json.createObjectBuilder()
                            .add("question_number", question_number)
                            .add("question_type", question_type)
                            .add("answer_value", answer_value).build());
                    
                    //System.out.println("#" + question_number + "  question_type=" + question_type + "  answer_value=" + answer_value);
                }
            }
            JsonArray answers_array = answer_array.build();
            // add contacts array object
            jsonBuilder.add("answers", answers_array);
            JsonObject answers_object = jsonBuilder.build();
            StringWriter strWtr = new StringWriter();
            JsonWriter jsonWtr = Json.createWriter(strWtr);
            jsonWtr.writeObject(answers_object);
            jsonWtr.close();
            //System.out.println("JSON=" + strWtr.toString());
            java.util.Date now = new java.util.Date();
            Timestamp date_completed = new Timestamp(now.getTime());
            //save the result to the survey_result table.
            String query = "UPDATE survey_results SET date_completed=?,answers_json=? WHERE id=?";
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setTimestamp(1,date_completed);
            stmt.setString(2,strWtr.toString());
            stmt.setString(3,id);            
            stmt.executeUpdate();
            stmt.close();
            con.close();     
        }
        catch(Exception e)
        {
            logger.error("exception=" + e);
        }
        String RedirectURL = "survey_complete.jsp" ;
        response.sendRedirect(RedirectURL);    
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

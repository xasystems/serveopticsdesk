/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import com.google.gson.JsonObject;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.Tika;
import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;

@MultipartConfig()
/**
 *
 * @author ralph
 */
public class edit_profile extends HttpServlet 
{
    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String installation_type = props.get("installation_type").toString().trim();
        String contract_id = session.getAttribute("db").toString();
        CouchDbClient dbClient = null;
        if (session.getAttribute("authenticated") == null) 
        {
            response.sendRedirect("index.jsp");
        }
        else
        {
            //do work here
            try 
            {
                String id = request.getParameter("id");
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                String first = request.getParameter("first");
                String mi = request.getParameter("mi");
                String last = request.getParameter("last");
                String address_1 = request.getParameter("address_1");
                String address_2 = request.getParameter("address_2");
                String city = request.getParameter("city");
                String state = request.getParameter("state");
                String zip = request.getParameter("zip");
                String location = request.getParameter("location");
                String department = request.getParameter("department");
                String site = request.getParameter("site");
                String company = request.getParameter("company");
                String email = request.getParameter("email");
                String phone_office = request.getParameter("phone_office");
                String phone_mobile = request.getParameter("phone_mobile");
                String tz[] = request.getParameter("tz").split(",");
                String tz_name = tz[0];
                String tz_time = tz[1];
                session.setAttribute("tz_name",tz_name);
                session.setAttribute("tz_time",tz_time);
                password = support.encrypt_utils.encrypt(context_dir, password);
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                PreparedStatement stmt = con.prepareStatement("UPDATE users SET password=?,first=?,mi=?,last=?,address_1=?,address_2=?,city=?,state=?,zip=?,location=?,department=?,site=?,company=?,email=?,phone_office=?,phone_mobile=?,tz_name=?,tz_time=? WHERE id=?"); 
                stmt.setString(1,password);
                stmt.setString(2,first);
                stmt.setString(3,mi);
                stmt.setString(4,last);
                stmt.setString(5,address_1);
                stmt.setString(6,address_2);
                stmt.setString(7,city);
                stmt.setString(8,state);
                stmt.setString(9,zip);
                stmt.setString(10,location);
                stmt.setString(11,department);
                stmt.setString(12,site);
                stmt.setString(13,company);
                stmt.setString(14,email);
                stmt.setString(15,phone_office);
                stmt.setString(16,phone_mobile);  
                stmt.setString(17,tz_name);  
                stmt.setString(18,tz_time);                  
                stmt.setString(19,id);                  
                stmt.executeUpdate();                

                //start get the file attachment if there is one
                try
                {
                    Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
                    if (filePart != null)
                    {
                        try
                        {
                            dbClient = db.couchdb.get_avatars_client(props);
                            if (dbClient == null)
                            {
                                throw new Exception("dbClient is null");
                            }
                            String file_name;
                            Tika tika = new Tika();

        //                    logger.info("filepart:" + filePart);
                            String fileName = filePart.getSubmittedFileName();
                            if (fileName != null && !"".equals(fileName)) 
                            {
                                long file_size = filePart.getSize();
                                String mimeType = tika.detect(filePart.getInputStream());
                                file_name = "avatar";

                                String user_info[] = db.get_users.by_id(con, id);
                                String uuid;
                                String rev;
                                int new_uuid = 0;
                                Response dbclient_response;
                                java.util.Date now = new java.util.Date();
                                String user_id = session.getAttribute("user_id").toString();

                                if (user_info[24] != null && !user_info[24].equals(""))
                                {
                                    uuid = user_info[24];
                                    JsonObject json = new JsonObject();
                                    json = dbClient.find(JsonObject.class, uuid);
                                    Map<String, Object> map = new HashMap<>(); 
                                    map.put("_id", uuid);
                                    map.put("Filename", file_name);
                                    map.put("MIME_type", mimeType);
                                    map.put("FileSize", file_size);
                                    map.put("DateUploaded", now);
                                    map.put("UploadedBy", user_id);
                                    dbclient_response = dbClient.update(map);
                                    rev = dbclient_response.getRev();

//                                    rev = json.get("_rev").getAsString();
                                } else {
                                    UUID Uuid = UUID.randomUUID(); //Generates random UUID                                  
                                    uuid = Uuid.toString();
                                    new_uuid = 1;
                                    Map<String, Object> map = new HashMap<>(); 

                                    map.put("_id", uuid);
                                    map.put("Filename", file_name);
                                    map.put("MIME_type", mimeType);
                                    map.put("FileSize", file_size);
                                    map.put("DateUploaded", now);
                                    map.put("UploadedBy", user_id);
                                    dbclient_response = dbClient.save(map);
                                    rev = dbclient_response.getRev();
                                }

                                dbclient_response = dbClient.saveAttachment(filePart.getInputStream(), file_name, mimeType, uuid, rev);

                                if (dbclient_response.getError() != null)
                                {
                                    throw new Exception("CouchDB attachment upload error: " + dbclient_response.getError());
                                }

                                if (new_uuid > 0)
                                {
                                    String query_avatar = "UPDATE users SET avatar = ? WHERE id = ?";
                                    PreparedStatement stmt_attachment = con.prepareStatement(query_avatar);
                                    stmt_attachment.setString(1, uuid);
                                    stmt_attachment.setString(2, id);
                                    stmt_attachment.execute();

                                    int rows_affected = stmt_attachment.executeUpdate();
                                    if (rows_affected <= 0)
                                    {
                                        throw new Exception("Avatar field is somehow not updated in the users table");
                                    }
                                }
                            }
                        } catch (Exception e)
                        {
                            logger.error("ERROR attachments process in servlet admin_users_edit:=" + e);
                        }

                    }
                }
                catch(Exception e)
                {
                    logger.error("ERROR Exception avatar saving in servlet admin_users_add:=" + e);
                }                

                String[] attachments_delete_ids = request.getParameterValues("attachment_delete_id");
                if (attachments_delete_ids != null && attachments_delete_ids.length > 0)
                {
                    try
                    {                     
                        String query_avatar = "UPDATE users SET avatar = null WHERE id = ?";
                        PreparedStatement stmt_attachment = con.prepareStatement(query_avatar);
                        stmt_attachment.setString(1, id);
                        stmt_attachment.execute();

                        dbClient = db.couchdb.get_avatars_client(props);
                        if (dbClient == null)
                        {
                            throw new Exception("dbClient is null");
                        }
                        String uuid = attachments_delete_ids[0];
                        String rev;

                        JsonObject json = new JsonObject();
                        json = dbClient.find(JsonObject.class, uuid);        
                        rev = json.get("_rev").getAsString();
                        dbClient.remove(uuid, rev);

                    } catch (Exception e)
                    {
                        logger.error("ERROR avatar deletion in servlet admin_users_edit:=" + e);
                    }
                }


                stmt.close();
                con.close();
            }
            catch (Exception e) 
            {
                logger.error("ERROR Exception in servlet edit_profile:=" + e);
            }
            response.sendRedirect("edit_profile.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

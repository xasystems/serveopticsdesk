/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.Tika;
import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;

/**
 *
 * @author ralph
 */
@MultipartConfig()
@WebServlet("/chat_message_add")
public class chat_message_add extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
//        String user_id = request.getParameter("user_id");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

//        String user_id = request.getParameter("user_id");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        JsonObjectBuilder json_builder = Json.createObjectBuilder();
        JsonObject json;
        
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {

                if (session.getAttribute("authenticated") == null) 
                {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }

                logger.info(session);
                Connection con = db.db_util.get_contract_connection(context_dir, session);
//                StringBuffer jb = new StringBuffer();
//                String line = null;
//                BufferedReader reader = request.getReader();
//                while ((line = reader.readLine()) != null) {
//                    jb.append(line);
//                }

//                JsonReader jr = Json.createReader(new StringReader(jb.toString()));
//                JsonObject jo = jr.readObject();
                   
                String user_id = session.getAttribute("user_id").toString();
                String object_type = request.getParameter("object_type");
                Integer object_id = Integer.parseInt(request.getParameter("object_id"));
                String parent_object_type = request.getParameter("parent_object_type");
                Integer parent_object_id = Integer.parseInt(request.getParameter("parent_object_id"));
//                String sender_id = request.getParameter("sender_id");
                String content = request.getParameter("content");
                String reply_to_id = StringUtils.defaultString(request.getParameter("reply_to_id"));
                String reply_to_content = StringUtils.defaultString(request.getParameter("reply_to_content"));
                String edit_message_id = StringUtils.defaultString(request.getParameter("edit_message_id"));
                String delete = StringUtils.defaultString(request.getParameter("delete"));

                boolean update_session_table = db.get_user_sessions.update(con, user_id);

                long message_id;

                if (edit_message_id.equals("")) {
                    String query = ""
                        + "INSERT INTO chat_messages "
                        + "(object_type,object_id,parent_object_type,parent_object_id,sender_id,content,saved,reply_to_id,reply_to_content) "
                        + "VALUES "
                        + "(?,?,?,?,?,?,?,?,?)";
                    PreparedStatement stmt = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                    stmt.setString(1, object_type); 
                    stmt.setInt(2, object_id); 
                    stmt.setString(3, parent_object_type); 
                    stmt.setInt(4, parent_object_id); 
                    stmt.setString(5, user_id); 
                    stmt.setString(6, content); 
                    stmt.setBoolean(7, true); 
                    stmt.setInt(8, NumberUtils.toInt(reply_to_id, 0)); 
                    stmt.setString(9, reply_to_content); 
                    int affectedRows = stmt.executeUpdate();
                    if (affectedRows == 0) {
                        throw new SQLException("Creating message failed, no rows affected.");
                    }

                    try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                        if (generatedKeys.next()) {
                            message_id = generatedKeys.getLong(1);
                        }
                        else {
                            throw new SQLException("Creating message failed, no ID obtained.");
                        }
                    }
                } else if (!delete.equals("")) {
                    String query = ""
                        + "UPDATE chat_messages SET deleted = ? WHERE id = ? AND sender_id = ?";
                    PreparedStatement stmt = con.prepareStatement(query);
                    stmt.setBoolean(1, true); 
                    stmt.setString(2, edit_message_id); 
                    stmt.setString(3, user_id); 
                    int affectedRows = stmt.executeUpdate();
                    if (affectedRows == 0) {
                        throw new SQLException("deleting message failed, no rows affected.");
                    }
                    message_id = NumberUtils.toInt(edit_message_id, 0);
                } else {
                    String query = ""
                        + "UPDATE chat_messages SET content = ? WHERE id = ? AND sender_id = ?";
                    PreparedStatement stmt = con.prepareStatement(query);
                    stmt.setString(1, content); 
                    stmt.setString(2, edit_message_id); 
                    stmt.setString(3, user_id); 
                    int affectedRows = stmt.executeUpdate();
                    if (affectedRows == 0) {
                        throw new SQLException("Updating message failed, no rows affected.");
                    }
                    message_id = NumberUtils.toInt(edit_message_id, 0);
                }

                CouchDbClient dbClient = null;

                try
                {
                    LinkedHashMap props = support.config.get_config(context_dir);

                    dbClient = db.couchdb.get_client(props);
                    if (dbClient == null)
                    {
                        throw new Exception("dbClient is null");
                    }
                    String name_on_disk;
                    String file_name;
                    Tika tika = new Tika();
                    java.util.Date now = new java.util.Date();

                    for (Part filePart : request.getParts()) {
    //                    logger.info("filepart:" + filePart);
                        String fileName = filePart.getSubmittedFileName();
                        if (fileName != null && !"".equals(fileName)) 
                        {
    //                        logger.info("filename:" + fileName);
                            long file_size = filePart.getSize();
                            String mimeType;
                            String request_mime_type = filePart.getContentType();
                            if (request_mime_type.equals("image/svg+xml")) {
                                mimeType = request_mime_type;
                            } else {
                                mimeType = tika.detect(filePart.getInputStream());
                            }

                            file_name = fileName.replaceAll("[^a-zA-Z0-9\\._]+", "_");
    //                        logger.info("file size" + file_size);
    //                    filePart.write(fileName);
    //                    out.println("... uploaded to: /uploads/" + fileName);

                            UUID Uuid = UUID.randomUUID(); //Generates random UUID                                  
                            String uuid = Uuid.toString();

                            Map<String, Object> map = new HashMap<>(); 
                            map.put("_id", uuid);
                            map.put("Filename", file_name);
                            map.put("MIME_type", mimeType);
                            map.put("FileSize", file_size);
                            map.put("DateUploaded", now);
                            map.put("UploadedBy", user_id);
                            map.put("AssociatedObject", object_type);
                            map.put("AssociatedObjectId", object_id);
                            map.put("AssociatedParentObject", parent_object_type);
                            map.put("AssociatedParentObjectId", parent_object_id);

                            Response dbclient_response = dbClient.save(map);
                            name_on_disk = uuid + "/"+ file_name; 
 
                            dbclient_response = dbClient.saveAttachment(filePart.getInputStream(), file_name, mimeType, dbclient_response.getId(), dbclient_response.getRev());

                            if (dbclient_response.getError() == null)
                            {
                                String query_attachment = "INSERT INTO chat_message_attachements "
                                    + "(`object_type`, `object_id`, `parent_object_type`, `parent_object_id`, `uuid`, `name_on_disk`,`file_name`,`file_date`,`file_size`,`rev`,`uploaded_by`,`chat_message_id`,`file_type`) "
                                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                PreparedStatement stmt_attachment = con.prepareStatement(query_attachment);
                                stmt_attachment.setString(1, object_type); 
                                stmt_attachment.setInt(2, object_id); 
                                stmt_attachment.setString(3, parent_object_type); 
                                stmt_attachment.setInt(4, parent_object_id); 
                                stmt_attachment.setString(5, uuid);
                                stmt_attachment.setString(6, name_on_disk);
                                stmt_attachment.setString(7, file_name);
                                Timestamp file_date = new Timestamp(now.getTime());
                                stmt_attachment.setTimestamp(8, file_date);
                                stmt_attachment.setInt(9, Math.toIntExact(file_size));
                                stmt_attachment.setString(10, dbclient_response.getRev());
                                stmt_attachment.setInt(11, Integer.parseInt(user_id));
                                stmt_attachment.setLong(12, message_id);
                                stmt_attachment.setString(13, mimeType);
                                stmt_attachment.execute();
                            } else {
                                logger.error("CouchDB attachment upload error: {}", dbclient_response.getError());
                            }

                        }

                    }

                    boolean success = message_id > 0;
//                    logger.info("success {}", success);
                    json_builder.add("success", success);

                    json = json_builder.build();
                    con.close();
                    //send the JSON data
                    out.print(json.toString());
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                    logger.error("ERROR Exception in servlet chat_message_add:=" + e);
                    json_builder.add("error", e.getMessage());
                    json = json_builder.build();
                    //send the JSON data
                    out.print(json.toString());
                } finally {
                    if (dbClient != null )
                    {
                        dbClient.shutdown();
                    }
                }
            }   catch(Exception e)
            {
                e.printStackTrace();
                logger.error("ERROR Exception in servlet chat_message_add:=" + e);
                json_builder.add("error", e.getMessage());
                json = json_builder.build();
                //send the JSON data
                out.print(json.toString());
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

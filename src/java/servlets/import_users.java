/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.UUID;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpSession;
/**
 *
 * @author rcampbell
 */
@MultipartConfig()


public class import_users extends HttpServlet 
{
    private static PreparedStatement insert_user_stmt;
    private static PreparedStatement insert_user_roles_stmt;
    private static PreparedStatement find_user_stmt;
    private static Connection con = null;
    
    public static String null_string(String value) throws Exception
    {
        String return_value = null;
        
        try
        {
            if(value == null || value.equalsIgnoreCase("null"))
            {
                return_value = null;
            }
            else
            {
                return_value = value;
            }
        }
        catch(Exception e)
        {
            return_value = null;
        }        
        return return_value;
    }
    public static boolean user_exists(String username) throws Exception
    {
        boolean exists = false;
        
        try
        {
            find_user_stmt.setString(1,username);
            ResultSet rs = find_user_stmt.executeQuery();
            while(rs.next())
            {
                exists = true;
            }
        }
        catch(Exception e)
        {
            exists = false;
        }        
        return exists;
    }    
    public static String user_id_from_username(String username) throws Exception
    {
        String id = "";
        
        try
        {
            find_user_stmt.setString(1,username);
            ResultSet rs = find_user_stmt.executeQuery();
            while(rs.next())
            {
                id = rs.getString("id");
            }
        }
        catch(Exception e)
        {
            id = "";
        }        
        return id;
    }    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        ArrayList<String[]> records = new ArrayList();
        UUID uuid = UUID.randomUUID();
        String filePath = context_dir + "/WEB-INF/" + uuid;
        int success = 0;
        int failed = 0;
        String failed_users = "";
        String username	= "";
        String password	= "";        
        String first = "";
        String mi = "";
        String last = "";
        String address_1 = "";
        String address_2 = "";
        String city = "";
        String state = "";
        String zip = "";
        String location	= "";
        String department = "";
        String site = "";
        String company = "";
        String email = "";
        String phone_office = "";
        String phone_mobile = "";
        String notes = "";
        String vip = "";
        String is_admin	= "";
        String tz_name	= "";
        String tz_time = "";
        String external_id = "";
        String self_service = "";
        String role = "";
        String insert = "INSERT INTO `users` (`username`," 
                    + "`password`," 
                    + "`first`,"
                    + "`mi`,"
                    + "`last`,"
                    + "`address_1`,"
                    + "`address_2`,"
                    + "`city`,"
                    + "`state`,"
                    + "`zip`," 
                    + "`location`,"
                    + "`department`,"
                    + "`site`," 
                    + "`company`," 
                    + "`email`,"
                    + "`phone_office`,"
                    + "`phone_mobile`,"
                    + "`notes`,"
                    + "`vip`,"
                    + "`is_admin`,"
                    + "`tz_name`,"
                    + "`tz_time`,"
                    + "`external_id`) "
                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";	
        String find_user_by_username = "SELECT * FROM `users` WHERE username=?";
        String insert_user_roles_query = "INSERT INTO `user_roles` (`user_id`,`role_id`) VALUES (?,?)";
        try
        {
            con = db.db_util.get_contract_connection(context_dir, session);
            find_user_stmt = con.prepareStatement(find_user_by_username);
            insert_user_stmt = con.prepareStatement(insert);
            insert_user_roles_stmt = con.prepareStatement(insert_user_roles_query);
            
            /* Receive file uploaded to the Servlet from the HTML5 form */
            Part filePart = request.getPart("import_file");
            for (Part part : request.getParts()) 
            {
                System.out.println("writting file=" + filePath);
                part.write(filePath);
            }
            //read the file line by line
            String line;
            try 
            {
                BufferedReader bufferreader = new BufferedReader(new FileReader(filePath));
                int line_number = 1;
                String user_line[] = null;
                boolean this_record_failed = false;
                while ((line = bufferreader.readLine()) != null) 
                {  
                    try
                    {
                        user_line = line.split(",");
                        if(line_number > 3) //user rows
                        {
                            this_record_failed = false;
                            //check if user exists
                            if(!user_exists(user_line[0]))
                            {
                               //insert record
                                insert_user_stmt.setString(1, user_line[0]); //username`," 
                                try
                                {
                                    user_line[1] = support.encrypt_utils.encrypt(context_dir, user_line[1]);
                                }
                                catch(Exception re)
                                {
                                    user_line[1] = "null";
                                }
                                insert_user_stmt.setString(2, user_line[1]); //password`," 
                                insert_user_stmt.setString(3, user_line[2]); //`first`,"
                                if(user_line[3].length() > 4)
                                {
                                    user_line[3] = user_line[3].substring(0,3);
                                }
                                insert_user_stmt.setString(4, user_line[3]); //`mi`,"
                                insert_user_stmt.setString(5, user_line[4]); //last`,"
                                insert_user_stmt.setString(6, user_line[5]); //address_1`,"
                                insert_user_stmt.setString(7, user_line[6]); //address_2`,"
                                insert_user_stmt.setString(8, user_line[7]); //city`,"
                                insert_user_stmt.setString(9, user_line[8]); //state`,"
                                insert_user_stmt.setString(10, user_line[9]); //zip`," 
                                insert_user_stmt.setString(11, user_line[10]); //location`,"
                                insert_user_stmt.setString(12, user_line[11]); //department`,"
                                insert_user_stmt.setString(13, user_line[12]); //site`," 
                                insert_user_stmt.setString(14, user_line[13]); //company`," 
                                insert_user_stmt.setString(15, user_line[14]); //email`,"
                                insert_user_stmt.setString(16, user_line[15]); //phone_office`,"
                                insert_user_stmt.setString(17, user_line[16]); //phone_mobile`,"
                                insert_user_stmt.setString(18, user_line[17]); //notes`,"
                                if(user_line[18].equalsIgnoreCase("") || user_line[18] == null)
                                {
                                    user_line[18] = "false";
                                }
                                insert_user_stmt.setString(19, user_line[18]); //vip`,"
                                if(user_line[19].equalsIgnoreCase("") || user_line[19] == null)
                                {
                                    user_line[19] = "false";                                }
                                insert_user_stmt.setString(20, user_line[19]); //is_admin`,"
                                insert_user_stmt.setString(21, user_line[20]); //tz_name`,"
                                insert_user_stmt.setString(22, user_line[21]); //tz_time`,"
                                insert_user_stmt.setString(23, user_line[22]); //external_id`) "       
                                insert_user_stmt.execute();
                                
                                if(user_line[23].equalsIgnoreCase("true")) //self service
                                {
                                    String user_id = user_id_from_username(user_line[0]);
                                    insert_user_roles_stmt.setString(1,user_id);
                                    insert_user_roles_stmt.setString(2,"3");
                                    insert_user_roles_stmt.execute();
                                }
                                //System.out.println("user_line.length=" + user_line.length);
                                if(user_line.length == 24) //no roles
                                {
                                    //just move on then                                
                                }
                                else if(user_line.length == 25) //yes roles
                                {
                                    //get user id
                                    String user_id = user_id_from_username(user_line[0]);
                                    try
                                    {
                                        String roles[] = user_line[24].split(";");
                                        for(int z = 0; z < roles.length;z++)
                                        {
                                            //get role_id for role_name
                                            String role_info[] = db.get_roles.by_name(con, roles[z]);
                                            if(!role_info[0].equalsIgnoreCase("0")) //found the role
                                            {
                                                insert_user_roles_stmt.setString(1,user_id);
                                                insert_user_roles_stmt.setString(2,role_info[0]);
                                                insert_user_roles_stmt.execute();
                                            }
                                        }
                                    }
                                    catch(Exception e)
                                    {
                                        //System.out.println("Exception inserting user_roles:=" + e);
                                        failed_users = failed_users + ";" + "Line " + line_number + " failed to insert role.";
                                    }
                                }                                
                            }
                            else
                            {
                                failed_users = failed_users + ";" + "Line " + line_number + " failed to insert user:" + user_line[0] + ". User already exists";
                                this_record_failed = true;
                            }
                            if(this_record_failed)
                            {
                                failed++;
                            }
                            else
                            {
                                success++;
                            }               
                        }
                    }
                    catch(Exception e)
                    {
                        this_record_failed = true;
                        //System.out.println("Exception inserting user:=" + e);
                        failed_users = failed_users + ";" + "Line " + line_number + " failed to insert";
                    }
                         
                    line_number++;
                }
            }
            catch (FileNotFoundException ex) 
            {
                ex.printStackTrace();
            } 
            catch (IOException ex) 
            {
                ex.printStackTrace();
            }
            //delete the file
            File temp_file = new File (filePath);
            temp_file.delete();
            //System.out.println("The file uploaded sucessfully.");
            //System.out.println("File successfully uploaded");
        }
        catch(Exception e)
        {
            System.out.println("Exception in import_users=" + e);
        }
        //System.out.println("import_users done");
        response.sendRedirect("admin_users_add_bulk_results.jsp?success=" + success + "&failed=" + failed + "&failed_users=" + failed_users);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author rcampbell
 */
public class ajax_lookup_service_catalog extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        StringBuilder stringBuilder = new StringBuilder();
        String context_dir = request.getServletContext().getRealPath("");
        String search = request.getParameter("search");
        String full = StringUtils.defaultString(request.getParameter("full"));

        //System.out.println("name=" + request.getParameter("name"));
        response.setContentType("text/html;charset=UTF-8");
        PreparedStatement stmt;
        boolean first_record = true;
        boolean has_data = false;
        try (PrintWriter out = response.getWriter()) 
        {
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                stringBuilder.append("[");
                if (full.equals("")) {
                    stmt = con.prepareStatement("SELECT * FROM `service_catalog` WHERE `name` like CONCAT( '%',?,'%') LIMIT 10");
                    stmt.setString(1, search);
                } else {
                    stmt = con.prepareStatement("SELECT * FROM `service_catalog` WHERE `name` like CONCAT( '%',?,'%') OR `description` like CONCAT( '%',?,'%')  LIMIT 10");
                    stmt.setString(1, search);
                    stmt.setString(2, search);
                }
                //System.out.println("stmt=" + stmt);
                ResultSet rs = stmt.executeQuery();
                while(rs.next())
                {
                    has_data = true;
                    if(first_record)
                    {
                        stringBuilder.append("{");
                        stringBuilder.append("\"label\": \"" + rs.getString("name") + "\",");
                        stringBuilder.append("\"value\": \"" + rs.getString("id") + "\"");
                        
                        stringBuilder.append("}");
                    }
                    else
                    {
                        stringBuilder.append(",{");
                        stringBuilder.append("\"label\": \"" + rs.getString("name") + "\",");
                        stringBuilder.append("\"value\": \"" + rs.getString("id") + "\"");
                        stringBuilder.append("}");
                    }
                    first_record = false;
                }
                stringBuilder.append("]");                
                if(!has_data)
                {
                    stringBuilder.setLength(0);
                    stringBuilder.append("[{\"label\":\"\",\"value\":\"\"}]");
                }                
                stmt.close();
                con.close();
                
                //send the JSON data
                out.print(stringBuilder.toString());
                out.flush();
            }
            catch(Exception e)
            {
                logger.error("ERROR Exception in servlet ajax_lookup_service_catalog:=" + e);
            }            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

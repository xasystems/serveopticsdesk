//Copyright 2021 XaSystems, Inc. , All rights reserved.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 *
 * @author server-xc6701
 */
public class get_users 
{
    private static Logger logger = LogManager.getLogger();
    
    public static String[][] all_as_array(Connection con) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM users", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][24];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("username");
                returnString[count][2] = rs.getString("password");
                returnString[count][3] = support.string_utils.check_for_null(rs.getString("first"));
                returnString[count][4] = support.string_utils.check_for_null(rs.getString("mi"));
                returnString[count][5] = support.string_utils.check_for_null(rs.getString("last"));
                returnString[count][6] = support.string_utils.check_for_null(rs.getString("address_1"));
                returnString[count][7] = support.string_utils.check_for_null(rs.getString("address_2"));
                returnString[count][8] = support.string_utils.check_for_null(rs.getString("city"));
                returnString[count][9] = support.string_utils.check_for_null(rs.getString("state"));
                returnString[count][10] = support.string_utils.check_for_null(rs.getString("zip"));
                returnString[count][11] = support.string_utils.check_for_null(rs.getString("location"));
                returnString[count][12] = support.string_utils.check_for_null(rs.getString("department"));
                returnString[count][13] = support.string_utils.check_for_null(rs.getString("site"));
                returnString[count][14] = support.string_utils.check_for_null(rs.getString("company"));
                returnString[count][15] = support.string_utils.check_for_null(rs.getString("email"));
                returnString[count][16] = support.string_utils.check_for_null(rs.getString("phone_office"));
                returnString[count][17] = support.string_utils.check_for_null(rs.getString("phone_mobile"));
                returnString[count][18] = support.string_utils.check_for_null(rs.getString("notes"));
                returnString[count][19] = support.string_utils.check_for_null(rs.getString("vip"));
                returnString[count][20] = support.string_utils.check_for_null(rs.getString("is_admin"));
                returnString[count][21] = support.string_utils.check_for_null(rs.getString("tz_name"));
                returnString[count][22] = support.string_utils.check_for_null(rs.getString("tz_time"));
                returnString[count][23] = support.string_utils.check_for_null(rs.getString("external_id"));
                count++;                
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_users.all:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_users.all:=" + exc);
	}
        return returnString;
    }
    public static ArrayList<String[]> all(Connection con) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList();        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM users");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = new String[24];
                temp[0] = rs.getString("id");
                temp[1] = rs.getString("username");
                temp[2] = rs.getString("password");
                temp[3] = support.string_utils.check_for_null(rs.getString("first"));
                temp[4] = support.string_utils.check_for_null(rs.getString("mi"));
                temp[5] = support.string_utils.check_for_null(rs.getString("last"));
                temp[6] = support.string_utils.check_for_null(rs.getString("address_1"));
                temp[7] = support.string_utils.check_for_null(rs.getString("address_2"));
                temp[8] = support.string_utils.check_for_null(rs.getString("city"));
                temp[9] = support.string_utils.check_for_null(rs.getString("state"));
                temp[10] = support.string_utils.check_for_null(rs.getString("zip"));
                temp[11] = support.string_utils.check_for_null(rs.getString("location"));
                temp[12] = support.string_utils.check_for_null(rs.getString("department"));
                temp[13] = support.string_utils.check_for_null(rs.getString("site"));
                temp[14] = support.string_utils.check_for_null(rs.getString("company"));
                temp[15] = support.string_utils.check_for_null(rs.getString("email"));
                temp[16] = support.string_utils.check_for_null(rs.getString("phone_office"));
                temp[17] = support.string_utils.check_for_null(rs.getString("phone_mobile"));
                temp[18] = support.string_utils.check_for_null(rs.getString("notes"));
                temp[19] = support.string_utils.check_for_null(rs.getString("vip"));
                temp[20] = support.string_utils.check_for_null(rs.getString("is_admin"));
                temp[21] = support.string_utils.check_for_null(rs.getString("tz_name"));
                temp[22] = support.string_utils.check_for_null(rs.getString("tz_time"));
                temp[23] = support.string_utils.check_for_null(rs.getString("external_id"));
                return_arraylist.add(temp);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_users.all:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_users.all:=" + exc);
	}
        return return_arraylist;
    }
    public static String[][] all_limited(Connection con, int starting_record, int fetch_size) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM users ORDER BY username LIMIT " + starting_record + "," + fetch_size , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][24];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("username");
                returnString[count][2] = rs.getString("password");
                returnString[count][3] = support.string_utils.check_for_null(rs.getString("first"));
                returnString[count][4] = support.string_utils.check_for_null(rs.getString("mi"));
                returnString[count][5] = support.string_utils.check_for_null(rs.getString("last"));
                returnString[count][6] = support.string_utils.check_for_null(rs.getString("address_1"));
                returnString[count][7] = support.string_utils.check_for_null(rs.getString("address_2"));
                returnString[count][8] = support.string_utils.check_for_null(rs.getString("city"));
                returnString[count][9] = support.string_utils.check_for_null(rs.getString("state"));
                returnString[count][10] = support.string_utils.check_for_null(rs.getString("zip"));
                returnString[count][11] = support.string_utils.check_for_null(rs.getString("location"));
                returnString[count][12] = support.string_utils.check_for_null(rs.getString("department"));
                returnString[count][13] = support.string_utils.check_for_null(rs.getString("site"));
                returnString[count][14] = support.string_utils.check_for_null(rs.getString("company"));
                returnString[count][15] = support.string_utils.check_for_null(rs.getString("email"));
                returnString[count][16] = support.string_utils.check_for_null(rs.getString("phone_office"));
                returnString[count][17] = support.string_utils.check_for_null(rs.getString("phone_mobile"));
                returnString[count][18] = support.string_utils.check_for_null(rs.getString("notes"));
                returnString[count][19] = support.string_utils.check_for_null(rs.getString("vip"));
                returnString[count][20] = support.string_utils.check_for_null(rs.getString("is_admin"));
                returnString[count][21] = support.string_utils.check_for_null(rs.getString("tz_name"));
                returnString[count][22] = support.string_utils.check_for_null(rs.getString("tz_time"));
                returnString[count][23] = support.string_utils.check_for_null(rs.getString("external_id"));
                count++;                
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_users.all:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_users.all:=" + exc);
	}
        return returnString;
    }
    
    public static String[][] all_limited_filtered(Connection con, int starting_record, int fetch_size, List<Map<String, String>> filters, int role_id) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {

//            logger.info("role_id " + role_id);

            String where = "";
            if (role_id > 0) {
                stmt = con.prepareStatement("SELECT user_id FROM user_roles WHERE role_id=?");
                stmt.setString(1, String.valueOf(role_id));
                logger.info("query=" + stmt.toString());
                //System.out.println("query=" + stmt.toString());
                ResultSet rs = stmt.executeQuery();
                ArrayList<String> role_user_ids = new ArrayList<String>();
                while(rs.next())
                {
//                    logger.info("!!!user :=" + rs.getString("user_id"));
                    role_user_ids.add(rs.getString("user_id"));
                }
                stmt.close();
                if (role_user_ids.size() > 0) {
                    where = "id in (" + String.join(", ", role_user_ids) + ")";
                } else {
                    where = "id = -1";
                }
            }
            
            for (int i = 0; i < filters.size(); i++)
            {
              String filter_key = filters.get(i).get("key");
              String filter_value = filters.get(i).get("value");
              if (!"".equals(where)) 
              {
                  where += " AND ";
              }
              where += filter_key + " = ? ";
                
            }
            
            if (!"".equals(where)) {
                where = " WHERE " + where;
            }
            
            // Create a Statement Object
//            logger.info("!!!condition :=" + where);
            stmt = con.prepareStatement("SELECT * FROM users " + where + " ORDER BY id DESC LIMIT ? , ?" , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            for (int i = 0; i < filters.size(); i++)
            {
              String filter_value = filters.get(i).get("value");
              stmt.setString(i+1, filter_value);
            }
            stmt.setInt(filters.size()+1, starting_record);
            stmt.setInt(filters.size()+2, fetch_size);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][24];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("username");
                returnString[count][2] = rs.getString("password");
                returnString[count][3] = support.string_utils.check_for_null(rs.getString("first"));
                returnString[count][4] = support.string_utils.check_for_null(rs.getString("mi"));
                returnString[count][5] = support.string_utils.check_for_null(rs.getString("last"));
                returnString[count][6] = support.string_utils.check_for_null(rs.getString("address_1"));
                returnString[count][7] = support.string_utils.check_for_null(rs.getString("address_2"));
                returnString[count][8] = support.string_utils.check_for_null(rs.getString("city"));
                returnString[count][9] = support.string_utils.check_for_null(rs.getString("state"));
                returnString[count][10] = support.string_utils.check_for_null(rs.getString("zip"));
                returnString[count][11] = support.string_utils.check_for_null(rs.getString("location"));
                returnString[count][12] = support.string_utils.check_for_null(rs.getString("department"));
                returnString[count][13] = support.string_utils.check_for_null(rs.getString("site"));
                returnString[count][14] = support.string_utils.check_for_null(rs.getString("company"));
                returnString[count][15] = support.string_utils.check_for_null(rs.getString("email"));
                returnString[count][16] = support.string_utils.check_for_null(rs.getString("phone_office"));
                returnString[count][17] = support.string_utils.check_for_null(rs.getString("phone_mobile"));
                returnString[count][18] = support.string_utils.check_for_null(rs.getString("notes"));
                returnString[count][19] = support.string_utils.check_for_null(rs.getString("vip"));
                returnString[count][20] = support.string_utils.check_for_null(rs.getString("is_admin"));
                returnString[count][21] = support.string_utils.check_for_null(rs.getString("tz_name"));
                returnString[count][22] = support.string_utils.check_for_null(rs.getString("tz_time"));
                returnString[count][23] = support.string_utils.check_for_null(rs.getString("external_id"));
                count++;                
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_users.all:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_users.all:=" + exc);
	}
        return returnString;
    }

    public static int all_filtered_count(Connection con, List<Map<String, String>> filters, int role_id) throws IOException, SQLException
    {
        int count = 0;
        PreparedStatement stmt;
        try 
        {

            String where = "";
            if (role_id > 0) {
                stmt = con.prepareStatement("SELECT user_id FROM user_roles WHERE role_id=?");
                stmt.setString(1, String.valueOf(role_id));
                logger.info("query=" + stmt.toString());
                //System.out.println("query=" + stmt.toString());
                ResultSet rs = stmt.executeQuery();
                ArrayList<String> role_user_ids = new ArrayList<String>();
                while(rs.next())
                {
//                    logger.info("!!!user :=" + rs.getString("user_id"));
                    role_user_ids.add(rs.getString("user_id"));
                }
                stmt.close();
                if (role_user_ids.size() > 0) {
                    where = "id in (" + String.join(", ", role_user_ids) + ")";
                } else {
                    where = "id = -1";
                }
            }
            
            for (int i = 0; i < filters.size(); i++)
            {
              String filter_key = filters.get(i).get("key");
              String filter_value = filters.get(i).get("value");
              if (!"".equals(where)) 
              {
                  where += " AND ";
              }
              where += filter_key + " = ? ";
                
            }
            
            if (!"".equals(where)) {
                where = " WHERE " + where;
            }
            // Create a Statement Object

            stmt = con.prepareStatement("SELECT count(*) as counter FROM users " + where, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            
            for (int i = 0; i < filters.size(); i++)
            {
              String filter_value = filters.get(i).get("value");
              stmt.setString(i+1, filter_value);
            }
//            logger.info("!!!counter condition :=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("counter");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_users.all:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_users.all:=" + exc);
	}
        return count;
    }

    public static String[] by_id(Connection con, String id) throws IOException, SQLException
    {
        String returnString[] = {"","","","","","","","","","","","","","","","","","","","","","","","",""};
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM users WHERE id=?");
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                returnString[0] = rs.getString("id");
                returnString[1] = rs.getString("username");
                returnString[2] = rs.getString("password");
                returnString[3] = support.string_utils.check_for_null(rs.getString("first"));
                returnString[4] = support.string_utils.check_for_null(rs.getString("mi"));
                returnString[5] = support.string_utils.check_for_null(rs.getString("last"));
                returnString[6] = support.string_utils.check_for_null(rs.getString("address_1"));
                returnString[7] = support.string_utils.check_for_null(rs.getString("address_2"));
                returnString[8] = support.string_utils.check_for_null(rs.getString("city"));
                returnString[9] = support.string_utils.check_for_null(rs.getString("state"));
                returnString[10] = support.string_utils.check_for_null(rs.getString("zip"));
                returnString[11] = support.string_utils.check_for_null(rs.getString("location"));
                returnString[12] = support.string_utils.check_for_null(rs.getString("department"));
                returnString[13] = support.string_utils.check_for_null(rs.getString("site"));
                returnString[14] = support.string_utils.check_for_null(rs.getString("company"));
                returnString[15] = support.string_utils.check_for_null(rs.getString("email"));
                returnString[16] = support.string_utils.check_for_null(rs.getString("phone_office"));
                returnString[17] = support.string_utils.check_for_null(rs.getString("phone_mobile"));
                returnString[18] = support.string_utils.check_for_null(rs.getString("notes"));
                returnString[19] = support.string_utils.check_for_null(rs.getString("vip"));
                returnString[20] = support.string_utils.check_for_null(rs.getString("is_admin"));
                returnString[21] = support.string_utils.check_for_null(rs.getString("tz_name"));
                returnString[22] = support.string_utils.check_for_null(rs.getString("tz_time"));
                returnString[23] = support.string_utils.check_for_null(rs.getString("external_id"));
                returnString[24] = support.string_utils.check_for_null(rs.getString("avatar"));
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_users.by_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_users.by_id:=" + exc);
	}
        return returnString;
    }
    public static ArrayList<byte[]> salt_and_hash_by_id(Connection con, String id) throws IOException, SQLException
    {
        ArrayList <byte[]> return_array_list = new ArrayList();
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT password_salt,password_hash FROM users WHERE id=?");
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_array_list.add(rs.getBytes("password_salt"));
                return_array_list.add(rs.getBytes("password_hash"));
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_users.salt_and_hash_by_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_users.salt_and_hash_by_id:=" + exc);
	}
        return return_array_list;
    }
    public static String[] by_username(Connection con, String username) throws IOException, SQLException
    {
        String returnString[] = {"","","","","","","","","","","","","","","","","","","","","","","",""};
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM users WHERE username=?");
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                returnString[0] = rs.getString("id");
                returnString[1] = rs.getString("username");
                returnString[2] = rs.getString("password");
                returnString[3] = support.string_utils.check_for_null(rs.getString("first"));
                returnString[4] = support.string_utils.check_for_null(rs.getString("mi"));
                returnString[5] = support.string_utils.check_for_null(rs.getString("last"));
                returnString[6] = support.string_utils.check_for_null(rs.getString("address_1"));
                returnString[7] = support.string_utils.check_for_null(rs.getString("address_2"));
                returnString[8] = support.string_utils.check_for_null(rs.getString("city"));
                returnString[9] = support.string_utils.check_for_null(rs.getString("state"));
                returnString[10] = support.string_utils.check_for_null(rs.getString("zip"));
                returnString[11] = support.string_utils.check_for_null(rs.getString("location"));
                returnString[12] = support.string_utils.check_for_null(rs.getString("department"));
                returnString[13] = support.string_utils.check_for_null(rs.getString("site"));
                returnString[14] = support.string_utils.check_for_null(rs.getString("company"));
                returnString[15] = support.string_utils.check_for_null(rs.getString("email"));
                returnString[16] = support.string_utils.check_for_null(rs.getString("phone_office"));
                returnString[17] = support.string_utils.check_for_null(rs.getString("phone_mobile"));
                returnString[18] = support.string_utils.check_for_null(rs.getString("notes"));
                returnString[19] = support.string_utils.check_for_null(rs.getString("vip"));
                returnString[20] = support.string_utils.check_for_null(rs.getString("is_admin"));
                returnString[21] = support.string_utils.check_for_null(rs.getString("tz_name"));
                returnString[22] = support.string_utils.check_for_null(rs.getString("tz_time"));
                returnString[23] = support.string_utils.check_for_null(rs.getString("external_id"));
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_users.by_username:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_users.by_username:=" + exc);
	}
        return returnString;
    }
    public static String[][] by_query(Connection con, String query) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT id,name FROM ("
                + "(SELECT id,CONCAT(first,' ',last) as name FROM users WHERE first LIKE CONCAT( '%',?,'%') OR last LIKE CONCAT( '%',?,'%')) UNION ALL"
                + "(select id,`name` from `groups` where `name` LIKE CONCAT( '%',?,'%')))"
                + " as t1 order by name LIMIT 5" , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            query = query
                .replace("!", "!!")
                .replace("%", "!%")
                .replace("_", "!_")
                .replace("[", "![");

            stmt.setString(1, query);
            stmt.setString(2, query);
            stmt.setString(3, query);
            System.out.println("query=" + stmt.toString());
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][2];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("name");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_users.by_query:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_users.by_query:=" + exc);
	}
        return returnString;
    }
    public static String[][] users_in_group(Connection con, String group_id) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT users.* FROM users INNER JOIN users_groups ON users_groups.user_id = users.id WHERE users_groups.group_id=?", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, group_id);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][24];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("username");
                returnString[count][2] = rs.getString("password");
                returnString[count][3] = support.string_utils.check_for_null(rs.getString("first"));
                returnString[count][4] = support.string_utils.check_for_null(rs.getString("mi"));
                returnString[count][5] = support.string_utils.check_for_null(rs.getString("last"));
                returnString[count][6] = support.string_utils.check_for_null(rs.getString("address_1"));
                returnString[count][7] = support.string_utils.check_for_null(rs.getString("address_2"));
                returnString[count][8] = support.string_utils.check_for_null(rs.getString("city"));
                returnString[count][9] = support.string_utils.check_for_null(rs.getString("state"));
                returnString[count][10] = support.string_utils.check_for_null(rs.getString("zip"));
                returnString[count][11] = support.string_utils.check_for_null(rs.getString("location"));
                returnString[count][12] = support.string_utils.check_for_null(rs.getString("department"));
                returnString[count][13] = support.string_utils.check_for_null(rs.getString("site"));
                returnString[count][14] = support.string_utils.check_for_null(rs.getString("company"));
                returnString[count][15] = support.string_utils.check_for_null(rs.getString("email"));
                returnString[count][16] = support.string_utils.check_for_null(rs.getString("phone_office"));
                returnString[count][17] = support.string_utils.check_for_null(rs.getString("phone_mobile"));
                returnString[count][18] = support.string_utils.check_for_null(rs.getString("notes"));
                returnString[count][19] = support.string_utils.check_for_null(rs.getString("vip"));
                returnString[count][20] = support.string_utils.check_for_null(rs.getString("is_admin"));
                returnString[count][21] = support.string_utils.check_for_null(rs.getString("tz_name"));
                returnString[count][22] = support.string_utils.check_for_null(rs.getString("tz_time"));
                returnString[count][23] = support.string_utils.check_for_null(rs.getString("external_id"));
                count++;                
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_users.users_in_group:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_users.users_in_group:=" + exc);
	}
        return returnString;
    }    
    public static ArrayList<String[]> all_users_with_logins(Connection con) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM users WHERE password IS NOT NULL");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String user[] = new String[24];
                user[0] = rs.getString("id");
                user[1] = rs.getString("username");
                user[2] = rs.getString("password");
                user[3] = support.string_utils.check_for_null(rs.getString("first"));
                user[4] = support.string_utils.check_for_null(rs.getString("mi"));
                user[5] = support.string_utils.check_for_null(rs.getString("last"));
                user[6] = support.string_utils.check_for_null(rs.getString("address_1"));
                user[7] = support.string_utils.check_for_null(rs.getString("address_2"));
                user[8] = support.string_utils.check_for_null(rs.getString("city"));
                user[9] = support.string_utils.check_for_null(rs.getString("state"));
                user[10] = support.string_utils.check_for_null(rs.getString("zip"));
                user[11] = support.string_utils.check_for_null(rs.getString("location"));
                user[12] = support.string_utils.check_for_null(rs.getString("department"));
                user[13] = support.string_utils.check_for_null(rs.getString("site"));
                user[14] = support.string_utils.check_for_null(rs.getString("company"));
                user[15] = support.string_utils.check_for_null(rs.getString("email"));
                user[16] = support.string_utils.check_for_null(rs.getString("phone_office"));
                user[17] = support.string_utils.check_for_null(rs.getString("phone_mobile"));
                user[18] = support.string_utils.check_for_null(rs.getString("notes"));
                user[19] = support.string_utils.check_for_null(rs.getString("vip"));
                user[20] = support.string_utils.check_for_null(rs.getString("is_admin"));
                user[21] = support.string_utils.check_for_null(rs.getString("tz_name"));
                user[22] = support.string_utils.check_for_null(rs.getString("tz_time"));
                user[23] = support.string_utils.check_for_null(rs.getString("external_id"));
                return_array_list.add(user);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_users.all_users_with_logins:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_users.all_users_with_logins:=" + exc);
	}
        return return_array_list;
    }   
    public static ArrayList<String[]> users_with_role(Connection con, String role_id) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT users.* FROM users INNER JOIN user_roles ON user_roles.user_id = users.id WHERE user_roles.role_id=?");
            stmt.setString(1, role_id);
            //System.out.println("query=" + stmt.toString());
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String user[] = new String[24];
                user[0] = rs.getString("id");
                user[1] = rs.getString("username");
                user[2] = rs.getString("password");
                user[3] = support.string_utils.check_for_null(rs.getString("first"));
                user[4] = support.string_utils.check_for_null(rs.getString("mi"));
                user[5] = support.string_utils.check_for_null(rs.getString("last"));
                user[6] = support.string_utils.check_for_null(rs.getString("address_1"));
                user[7] = support.string_utils.check_for_null(rs.getString("address_2"));
                user[8] = support.string_utils.check_for_null(rs.getString("city"));
                user[9] = support.string_utils.check_for_null(rs.getString("state"));
                user[10] = support.string_utils.check_for_null(rs.getString("zip"));
                user[11] = support.string_utils.check_for_null(rs.getString("location"));
                user[12] = support.string_utils.check_for_null(rs.getString("department"));
                user[13] = support.string_utils.check_for_null(rs.getString("site"));
                user[14] = support.string_utils.check_for_null(rs.getString("company"));
                user[15] = support.string_utils.check_for_null(rs.getString("email"));
                user[16] = support.string_utils.check_for_null(rs.getString("phone_office"));
                user[17] = support.string_utils.check_for_null(rs.getString("phone_mobile"));
                user[18] = support.string_utils.check_for_null(rs.getString("notes"));
                user[19] = support.string_utils.check_for_null(rs.getString("vip"));
                user[20] = support.string_utils.check_for_null(rs.getString("is_admin"));
                user[21] = support.string_utils.check_for_null(rs.getString("tz_name"));
                user[22] = support.string_utils.check_for_null(rs.getString("tz_time"));
                user[23] = support.string_utils.check_for_null(rs.getString("external_id"));
                return_array_list.add(user);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_users.users_with_role:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_users.users_with_role:=" + exc);
	}
        return return_array_list;
    } 
    public static String[] by_email(Connection con, String email) throws IOException, SQLException
    {
        String returnString[] = {"","","","","","","","","","","","","","","","","","","","","","","","",""};
        
        //clean up email address
        //Microsoft Outlook <MicrosoftExchange329e71ec88ae4615bbc36ab6ce41109e@xasystems1.onmicrosoft.com>
        if(email.contains("<"))
        {
            String t1[] = email.split("<");
            String t2[] = t1[1].split(">");
            email = t2[0];
        }
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM users WHERE email=?");
            stmt.setString(1, email);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                returnString[0] = rs.getString("id");
                returnString[1] = rs.getString("username");
                returnString[2] = rs.getString("password");
                returnString[3] = support.string_utils.check_for_null(rs.getString("first"));
                returnString[4] = support.string_utils.check_for_null(rs.getString("mi"));
                returnString[5] = support.string_utils.check_for_null(rs.getString("last"));
                returnString[6] = support.string_utils.check_for_null(rs.getString("address_1"));
                returnString[7] = support.string_utils.check_for_null(rs.getString("address_2"));
                returnString[8] = support.string_utils.check_for_null(rs.getString("city"));
                returnString[9] = support.string_utils.check_for_null(rs.getString("state"));
                returnString[10] = support.string_utils.check_for_null(rs.getString("zip"));
                returnString[11] = support.string_utils.check_for_null(rs.getString("location"));
                returnString[12] = support.string_utils.check_for_null(rs.getString("department"));
                returnString[13] = support.string_utils.check_for_null(rs.getString("site"));
                returnString[14] = support.string_utils.check_for_null(rs.getString("company"));
                returnString[15] = support.string_utils.check_for_null(rs.getString("email"));
                returnString[16] = support.string_utils.check_for_null(rs.getString("phone_office"));
                returnString[17] = support.string_utils.check_for_null(rs.getString("phone_mobile"));
                returnString[18] = support.string_utils.check_for_null(rs.getString("notes"));
                returnString[19] = support.string_utils.check_for_null(rs.getString("vip"));
                returnString[20] = support.string_utils.check_for_null(rs.getString("is_admin"));
                returnString[21] = support.string_utils.check_for_null(rs.getString("tz_name"));
                returnString[22] = support.string_utils.check_for_null(rs.getString("tz_time"));
                returnString[23] = support.string_utils.check_for_null(rs.getString("external_id"));
                returnString[24] = support.string_utils.check_for_null(rs.getString("avatar"));
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_users.by_email:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_users.by_email:=" + exc);
	}
        return returnString;
    } 
    public static boolean email_exists(Connection con, String email) throws IOException, SQLException
    {
        boolean returnBolean = false;
        
        //clean up email address
        //Microsoft Outlook <MicrosoftExchange329e71ec88ae4615bbc36ab6ce41109e@xasystems1.onmicrosoft.com>
        if(email.contains("<"))
        {
            String t1[] = email.split("<");
            String t2[] = t1[1].split(">");
            email = t2[0];
        }
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM users WHERE email=?");
            stmt.setString(1, email);
            //System.out.println("stmt=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                returnBolean = true;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_users.email_exists:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_users.email_exists:=" + exc);
	}
        return returnBolean;
    } 
    
}

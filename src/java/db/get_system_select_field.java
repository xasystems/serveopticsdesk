/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class get_system_select_field 
{
    private static Logger logger = LogManager.getLogger();
    
    public static ArrayList<String[]> active_for_table_column(Connection con, String table, String column)
    {
        ArrayList<String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            //stmt = con.prepareStatement("SELECT * FROM system_select_fields WHERE `table`=? AND `column`=? AND `active`='true' ORDER BY `order` ASC");
            stmt = con.prepareStatement("SELECT * FROM system_select_fields WHERE `table`=? AND `column`=? AND `active`='true' ORDER BY `order` ASC");
            stmt.setString(1,table);
            stmt.setString(2,column);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = {"","","","","","",""};
                temp[0] = rs.getString("id");
                temp[1] = rs.getString("table");
                temp[2] = rs.getString("column");
                temp[3] = rs.getString("value");
                temp[4] = rs.getString("label");
                temp[5] = rs.getString("order");
                temp[6] = rs.getString("active");
                return_arraylist.add(temp);
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_system_select_field.for_table_column:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> for_table_column(Connection con, String table, String column)
    {
        ArrayList<String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT * FROM system_select_fields WHERE `table`=? AND `column`=? ORDER BY `order` ASC");
            stmt.setString(1,table);
            stmt.setString(2,column);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = {"","","","","","",""};
                temp[0] = rs.getString("id");
                temp[1] = rs.getString("table");
                temp[2] = rs.getString("column");
                temp[3] = rs.getString("value");
                temp[4] = rs.getString("label");
                temp[5] = rs.getString("order");
                temp[6] = rs.getString("active");
                return_arraylist.add(temp);
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_system_select_field.for_table_column:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> distinct_table_column(Connection con)
    {
        ArrayList<String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT DISTINCT `table`, `column` FROM system_select_fields;");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = {"",""};
                temp[0] = rs.getString("table");
                temp[1] = rs.getString("column");                
                return_arraylist.add(temp);
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_system_select_field.distinct_table_column:=" + exc);
	}
        return return_arraylist;
    }
}

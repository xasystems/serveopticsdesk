/*
//Copyright 2020 XaSystems, Inc. , All rights reserved.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author rcampbell
 */
public class get_notifications 
{
    private static Logger logger = LogManager.getLogger();
    
    public static ArrayList<String[]> all_global(Connection con) throws IOException, SQLException
    {
        ArrayList<String[]> returnArrayList = new ArrayList();
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM `notifications_global`");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String[] this_record = new String[5];
                this_record[0] = rs.getString("id");
                this_record[1] = rs.getString("notify_on");
                this_record[2] = rs.getString("active");
                this_record[3] = rs.getString("subject");
                this_record[4] = rs.getString("body");
                returnArrayList.add(this_record);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_notifications.all_global:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_notifications.all_global:=" + exc);
	}
        return returnArrayList;
    }
    public static String[] specific_global(Connection con, String notify_on) throws IOException, SQLException
    {
        String[] return_array = {"","","","",""};
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM `notifications_global` WHERE notify_on=?");
            stmt.setString(1, notify_on);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_array[0] = rs.getString("id");
                return_array[1] = rs.getString("notify_on");
                return_array[2] = rs.getString("active");
                return_array[3] = rs.getString("subject");
                return_array[4] = rs.getString("body");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_notifications.specific_global:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_notifications.specific_global:=" + exc);
	}
        return return_array;
    }
    public static String[] user_notifications_for_user_by_notify_on(Connection con, String user_id, String notify_on) throws IOException, SQLException
    {
        String[] return_array = {"","","","","",""};
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM notifications_user WHERE user_id=? AND notify_on=?");
            stmt.setString(1, user_id);
            stmt.setString(2, notify_on);
            //System.out.println("query=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_array[0] = rs.getString("id");
                return_array[1] = rs.getString("user_id");                
                return_array[2] = rs.getString("notify_on");
                return_array[3] = rs.getString("active");
                return_array[4] = rs.getString("subject");
                return_array[5] = rs.getString("body");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_notifications.user_notifications_for_user_by_notify_on:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_notifications.user_notifications_for_user_by_notify_on:=" + exc);
	}
        return return_array;
    }
    public static ArrayList<String[]> user_group_notifications_for_user_by_notify_on(Connection con, String group_id, String notify_on) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList();
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT notifications.type,notifications.user_id,notifications.entity_id,notifications_user.subject,notifications_user.body FROM notifications INNER JOIN notifications_user ON notifications.user_id = notifications_user.user_id WHERE notifications_user.active='true' AND  notifications.entity_id=? AND notifications.type=? AND notifications_user.notify_on=?");
            stmt.setString(1, group_id);
            stmt.setString(2, notify_on);
            stmt.setString(3, notify_on);
            //System.out.println("query=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = {"","","","",""};
                //# type, user_id, entity_id, subject, body
                temp[0] = rs.getString("notifications.type");
                temp[1] = rs.getString("notifications.user_id");                
                temp[2] = rs.getString("notifications.entity_id");
                temp[3] = rs.getString("notifications_user.subject");
                temp[4] = rs.getString("notifications_user.body");
                return_arraylist.add(temp);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_notifications.user_group_notifications_for_user_by_notify_on:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_notifications.user_group_notifications_for_user_by_notify_on:=" + exc);
	}
        return return_arraylist;
    }    
    public static ArrayList<String[]> user_notifications_for_user(Connection con, String user_id) throws IOException, SQLException
    {
        ArrayList<String[]> returnArrayList = new ArrayList();
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM notifications_user WHERE user_id=?");
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String[] this_record = new String[6];
                this_record[0] = rs.getString("id");
                this_record[1] = rs.getString("user_id");                
                this_record[2] = rs.getString("notify_on");
                this_record[3] = rs.getString("active");
                this_record[4] = rs.getString("subject");
                this_record[5] = rs.getString("body");
                returnArrayList.add(this_record);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_notifications.all_personal:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_notifications.all_personal:=" + exc);
	}
        return returnArrayList;
    }
    public static ArrayList<String[]> notifications_for_user(Connection con, String user_id) throws IOException, SQLException
    {
        ArrayList<String[]> returnArrayList = new ArrayList();
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM notifications WHERE user_id=?");
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String[] this_record = new String[4];
                this_record[0] = rs.getString("id");
                this_record[1] = rs.getString("type");                
                this_record[2] = rs.getString("user_id");
                this_record[3] = rs.getString("entity_id");
                returnArrayList.add(this_record);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_notifications.notifications_for_user:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_notifications.notifications_for_user:=" + exc);
	}
        return returnArrayList;
    }
}

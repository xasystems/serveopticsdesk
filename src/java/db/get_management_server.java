/*
//Copyright 2021 XaSystems, Inc. , All rights reserved.
 */
package db;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.LinkedHashMap;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class get_management_server 
{
    private static Logger logger = LogManager.getLogger();
    
    public static ArrayList<String[]> run_query(String context_dir, String query, String query_type, String requested_fields[])
    {
        ArrayList<String[]> return_array_list = new ArrayList();
        try
        {
            LinkedHashMap props = support.config.get_config(context_dir);
            String login_url = props.get("login_url").toString();
            String shared_user = props.get("shared_user").toString().trim();
            String shared_key = props.get("shared_key").toString().trim();
            String fields = "";
            for(int a = 0; a < requested_fields.length; a++)
            {
                if(a == 0)
                {
                    fields = requested_fields[a];
                }
                else
                {
                    fields = fields + "," + requested_fields[a];
                }
            }            
            String urlParameters = "";
            urlParameters = "user=" + shared_user + "&key=" + shared_key + "&query_type=" + query_type + "&query=" + query + "&requested_fields=" + fields;
            //encrypt the data
            String encrypted_request = support.encrypt_utils.encrypt(context_dir, urlParameters);
            //encode the encryted to remove url-unfriendly characters
            String encoded_encrypted_request = Base64.getEncoder().encodeToString(encrypted_request.getBytes());
            urlParameters = "request=" + encoded_encrypted_request;
            URL url = new URL(login_url + "/get_query");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Length",Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");  
            connection.setUseCaches(false);
            connection.setDoOutput(true);
            //Send request
            DataOutputStream wr = new DataOutputStream (connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.close();
            //Get Response  
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
            String line;
            while ((line = rd.readLine()) != null) 
            {
                response.append(line);
                response.append('\r');
            }
            rd.close();            
            byte[] decoded_encrypted_records = Base64.getDecoder().decode(response.toString().trim());
            String decodedString = new String(decoded_encrypted_records);
            String decrypted_records = support.encrypt_utils.decrypt(context_dir, decodedString);
            JsonReader reader = Json.createReader(new StringReader(decrypted_records));
            JsonObject resultObject = reader.readObject();
            JsonArray records = resultObject.getJsonArray("records");
            for(int a = 0; a < records.size();a++)
            {
                String this_record[] = new String[requested_fields.length];
                String output = "";
                JsonObject jo_this_record = records.getJsonObject(a); //a single incident record
                for(int b = 0; b < requested_fields.length; b++)
                {
                    output = output + " FieldName=" + requested_fields[b] + " Value= " +jo_this_record.getString(requested_fields[b]);
                    this_record[b] = jo_this_record.getString(requested_fields[b]);
                }
                return_array_list.add(this_record);
                //System.out.println("Record=" + output);
            }
        }
        catch(Exception e)
        {
            System.out.println("Exception in get_management_server.run_query=" + e);
        }
        return return_array_list;
    }
    public static boolean run_insert(String context_dir, String sql_stmt)
    {
        boolean return_boolean = true;
        try
        {
            LinkedHashMap props = support.config.get_config(context_dir);
            String login_url = props.get("login_url").toString();
            String shared_user = props.get("shared_user").toString().trim();
            String shared_key = props.get("shared_key").toString().trim();
            String urlParameters = "";
            urlParameters = "user=" + shared_user + "&key=" + shared_key + "&query_type=POST&query=" + sql_stmt;
            //encrypt the data
            String encrypted_request = support.encrypt_utils.encrypt(context_dir, urlParameters);
            //encode the encryted to remove url-unfriendly characters
            String encoded_encrypted_request = Base64.getEncoder().encodeToString(encrypted_request.getBytes());
            urlParameters = "request=" + encoded_encrypted_request;
            URL url = new URL(login_url + "/get_query");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Length",Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");  
            connection.setUseCaches(false);
            connection.setDoOutput(true);
            //Send request
            DataOutputStream wr = new DataOutputStream (connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.close();
            //Get Response  
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
            String line;
            while ((line = rd.readLine()) != null) 
            {
                response.append(line);
                response.append('\r');
            }
            rd.close();            
            byte[] decoded_encrypted_records = Base64.getDecoder().decode(response.toString().trim());
            String decodedString = new String(decoded_encrypted_records);
            String decrypted_records = support.encrypt_utils.decrypt(context_dir, decodedString);
            //System.out.println("run_insert=" + decrypted_records);
            JsonReader reader = Json.createReader(new StringReader(decrypted_records));
            JsonObject resultObject = reader.readObject();
            String success_value = resultObject.getJsonString("success").toString();
            if(success_value.equalsIgnoreCase("false"))
            {
                return_boolean = false;
            }
        }
        catch(Exception e)
        {
            return_boolean = false;
            System.out.println("Exception in get_management_server.run_insert=" + e);
        }
        return return_boolean;
    }
}

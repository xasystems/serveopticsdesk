/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class get_user_sessions 
{
    private static Logger logger = LogManager.getLogger();
    
    public static int active(Connection con, int session_timeout, String user_id) throws IOException, SQLException
    {
        int return_int = 0;
        java.util.Date now = new java.util.Date();
        java.util.Date access_time = new java.util.Date();
        PreparedStatement stmt;
        try
        {
            //delete all entries that are greater than (now - session_time)
            long session_timeout_msec = TimeUnit.MINUTES.toMillis(session_timeout);
            long kill_time = now.getTime() - session_timeout_msec;
            java.util.Date kill_date = new java.util.Date(kill_time);
            Timestamp delete_timestamp = new Timestamp(kill_date.getTime());
            stmt = con.prepareStatement("DELETE FROM user_sessions WHERE access_time < ?");
            stmt.setTimestamp(1, delete_timestamp);
            stmt.executeUpdate();           
            
            // now count number of entries
            stmt = con.prepareStatement("SELECT count(*) as value FROM user_sessions");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");                 
            }
            
            //add this session to the table
            stmt = con.prepareStatement("REPLACE INTO user_sessions(user_id,access_time) VALUE(?,?)");
            stmt.setString(1, user_id);
            stmt.setTimestamp(2, new Timestamp(now.getTime()));
            stmt.executeUpdate();   

            stmt.close();
            
        }
        catch(Exception e)
        {
            System.out.println("Exception in get_user_sessions.active=" + e);
        }
        return return_int;
    }
    public static int active_for_user_id(Connection con, int session_timeout, String user_id) throws IOException, SQLException
    {
        int return_int = 0;
        java.util.Date now = new java.util.Date();
        java.util.Date access_time = new java.util.Date();
        PreparedStatement stmt;
        try
        {
            //delete all entries that are greater than (now - session_time)
            long session_timeout_msec = TimeUnit.MINUTES.toMillis(session_timeout);
            long kill_time = now.getTime() - session_timeout_msec;
            java.util.Date kill_date = new java.util.Date(kill_time);
            Timestamp delete_timestamp = new Timestamp(kill_date.getTime());
            stmt = con.prepareStatement("SELECT count(*) as value FROM user_sessions WHERE user_id = ? AND access_time >= ? ");
            stmt.setString(1, user_id);
            stmt.setTimestamp(2, delete_timestamp);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");                 
            }
            
            stmt.close();
            
        }
        catch(Exception e)
        {
            System.out.println("Exception in get_user_sessions.active=" + e);
        }
        return return_int;
    }
    public static String last_active(Connection con, String user_id, String user_tz_name) throws IOException, SQLException
    {
        String access_time = "";
        String users_tz_time = "";
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT DATE_FORMAT(access_time,'%Y%m%d%H%i%s') AS access_time FROM user_sessions_history WHERE user_id = ? ");
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                access_time = rs.getString("access_time");                 
            }
            if (!access_time.equals("")) {
                users_tz_time = DateTimeFormatter.ofPattern("HH:mm / dd-MMM-yy").format(support.date_utils.utc_to_user_tz(user_tz_name, access_time));
            }
        }
        catch(Exception e)
        {
            System.out.println("Exception in get_user_sessions.active=" + e);
        }
        return users_tz_time;
    }
    public static boolean update(Connection con, String user_id) throws IOException, SQLException
    {
        boolean return_boolean = true;
        
        java.util.Date access_time = new java.util.Date();
        PreparedStatement stmt;
        try
        {
            //delete all entries that are greater than (now - session_time)
            Timestamp access_time_timestamp = new Timestamp(access_time.getTime());
            stmt = con.prepareStatement("REPLACE INTO user_sessions(user_id,access_time) VALUES(?,?)");
            stmt.setString(1, user_id);
            stmt.setTimestamp(2, access_time_timestamp);
            stmt.executeUpdate(); 

            //add this session to the sessions history table
            stmt = con.prepareStatement("REPLACE INTO user_sessions_history (user_id,access_time) VALUES(?,?)");
            stmt.setString(1, user_id);
            stmt.setTimestamp(2, access_time_timestamp);
            stmt.executeUpdate(); 
            stmt.close();

        }
        catch(Exception e)
        {
            return_boolean = false;
            System.out.println("Exception in get_user_sessions.update=" + e);
        }
        return return_boolean;
    }
    
}

//Copyright 2021 XaSystems, Inc. , All rights reserved.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.InputStream;
import java.util.LinkedHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lightcouch.CouchDbClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.StringEntity;
import org.lightcouch.CouchDbInfo;

/**
 *
 * @author server-xc6701
 */
public class couchdb 
{
    private static Logger logger = LogManager.getLogger();
    
    public static CouchDbClient get_client(LinkedHashMap props)
    {
        CouchDbClient dbClient = null;
        int status_code;
        HttpResponse response = null;

        try
        {
            dbClient = new CouchDbClient(
                props.get("couchdb.name").toString(),
                false,
                props.get("couchdb.protocol").toString(),
                props.get("couchdb.host").toString(),
                Integer.parseInt(props.get("couchdb.port").toString()),
                props.get("couchdb.username").toString(),
                props.get("couchdb.password").toString()
            );         
              
            try
            {
                HttpHead head = new HttpHead(dbClient.getDBUri());
                response = dbClient.executeRequest(head);
            } catch (org.lightcouch.NoDocumentException e)
            {
                logger.info("CouchDB database is missing, trying to create...");
                dbClient.context().createDB(props.get("couchdb.name").toString());
                HttpPut put = new HttpPut(dbClient.getDBUri() + "_security");
                put.setHeader("Accept", "application/json");
                put.setHeader("Content-type", "application/json");
                String inputJson = "{\"admins\": {\"roles\": [\"_admin\"]}, \"members\": {\"roles\": [\"_admin\",\"users\"]}}";
                StringEntity stringEntity = new StringEntity(inputJson);
                put.setEntity(stringEntity);
//                System.out.println("Executing request " + put.getRequestLine());
                response = dbClient.executeRequest(put);
                status_code = response.getStatusLine().getStatusCode();
                if (status_code == 200)
                {
                    logger.info("CouchDB database successfully created" + status_code);
                } else {
                    throw new Exception("CouchDB database creation failed with code: " + status_code);
                }                
            }
        } catch (Exception e)
        {
            logger.error("support.couchdb get_client exception: " + e);
            e.printStackTrace();
            dbClient = null;
        } finally {
            if (response != null) 
            {
                HttpClientUtils.closeQuietly(response);
            }
        }

        return dbClient;
    }
    public static CouchDbClient get_avatars_client(LinkedHashMap props)
    {
        CouchDbClient dbClient = null;
        int status_code;
        HttpResponse response = null;

        try
        {
            dbClient = new CouchDbClient(
                props.get("couchdb.avatars_db").toString(),
                false,
                props.get("couchdb.protocol").toString(),
                props.get("couchdb.host").toString(),
                Integer.parseInt(props.get("couchdb.port").toString()),
                props.get("couchdb.username").toString(),
                props.get("couchdb.password").toString()
            );         
              
            try
            {
                HttpHead head = new HttpHead(dbClient.getDBUri());
                response = dbClient.executeRequest(head);
            } catch (org.lightcouch.NoDocumentException e)
            {
                logger.info("CouchDB avatars database is missing, trying to create...");
                dbClient.context().createDB(props.get("couchdb.avatars_db").toString());
                HttpPut put = new HttpPut(dbClient.getDBUri() + "_security");
                put.setHeader("Accept", "application/json");
                put.setHeader("Content-type", "application/json");
                String inputJson = "{\"admins\": {\"roles\": [\"_admin\"]}, \"members\": {\"roles\": []}}";
                StringEntity stringEntity = new StringEntity(inputJson);
                put.setEntity(stringEntity);
//                System.out.println("Executing request " + put.getRequestLine());
                response = dbClient.executeRequest(put);
                status_code = response.getStatusLine().getStatusCode();
                if (status_code == 200)
                {
                    logger.info("CouchDB avatars database successfully created" + status_code);
                } else {
                    throw new Exception("CouchDB avatars database creation failed with code: " + status_code);
                }                
            }
        } catch (Exception e)
        {
            logger.error("support.couchdb get_avatars_client exception: " + e);
            e.printStackTrace();
            dbClient = null;
        } finally {
            if (response != null) 
            {
                HttpClientUtils.closeQuietly(response);
            }
        }

        return dbClient;
    }
}

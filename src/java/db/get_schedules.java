//Copyright 2020 XaSystems, Inc. , All rights reserved.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 *
 * @author server-xc6701
 */
public class get_schedules 
{
    private static Logger logger = LogManager.getLogger();
    
    public static String[][] all(Connection con) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM schedules", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][14];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("name");
                returnString[count][2] = rs.getString("description");
                returnString[count][3] = rs.getString("start_time");
                returnString[count][4] = rs.getString("end_time");
                returnString[count][5] = rs.getString("is_all_day");
                returnString[count][6] = rs.getString("include_holidays");     
                returnString[count][7] = rs.getString("sunday");     
                returnString[count][8] = rs.getString("monday");     
                returnString[count][9] = rs.getString("tuesday");     
                returnString[count][10] = rs.getString("wednesday");     
                returnString[count][11] = rs.getString("thursday");     
                returnString[count][12] = rs.getString("friday");     
                returnString[count][13] = rs.getString("saturday");     
                count++;                
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_schedules.all:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_schedules.all:=" + exc);
	}
        return returnString;
    }
    public static String[] by_id(Connection con, String id) throws IOException, SQLException
    {
        String returnString[] = {"0","Default-24x7","24x7","00:00:00","11:59:59","1","1","1","1","1","1","1","1","1"};
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM schedules WHERE id=?");
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                returnString[0] = rs.getString("id");
                returnString[1] = rs.getString("name");
                returnString[2] = rs.getString("description");
                returnString[3] = rs.getString("start_time");
                returnString[4] = rs.getString("end_time");
                returnString[5] = rs.getString("is_all_day");
                returnString[6] = rs.getString("include_holidays");     
                returnString[7] = rs.getString("sunday");     
                returnString[8] = rs.getString("monday");     
                returnString[9] = rs.getString("tuesday");     
                returnString[10] = rs.getString("wednesday");     
                returnString[11] = rs.getString("thursday");     
                returnString[12] = rs.getString("friday");     
                returnString[13] = rs.getString("saturday");     
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_schedules.by_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_schedules.by_id:=" + exc);
	}
        return returnString;
    }  
    public static String[] for_catalog_item_priority(Connection con, String priority, String sc_item_info[]) throws IOException, SQLException
    {
        String returnString[] = {"0","Default-24x7","24x7","00:00:00","11:59:59","1","1","1","1","1","1","1","1","1"};
        String schedule_id = "0";
        if(priority.equalsIgnoreCase("critical"))
        {
            schedule_id = sc_item_info[12];
        }
        else if(priority.equalsIgnoreCase("high"))
        {
            schedule_id = sc_item_info[19];
        }
        else if(priority.equalsIgnoreCase("medium"))
        {
            schedule_id = sc_item_info[26];
        }
        else if(priority.equalsIgnoreCase("low"))
        {
            schedule_id = sc_item_info[33];
        }        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM schedules WHERE id=?");
            stmt.setString(1, schedule_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                returnString[0] = rs.getString("id");
                returnString[1] = rs.getString("name");
                returnString[2] = rs.getString("description");
                returnString[3] = rs.getString("start_time");
                returnString[4] = rs.getString("end_time");
                returnString[5] = rs.getString("is_all_day");
                returnString[6] = rs.getString("include_holidays");     
                returnString[7] = rs.getString("sunday");     
                returnString[8] = rs.getString("monday");     
                returnString[9] = rs.getString("tuesday");     
                returnString[10] = rs.getString("wednesday");     
                returnString[11] = rs.getString("thursday");     
                returnString[12] = rs.getString("friday");     
                returnString[13] = rs.getString("saturday");     
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_schedules.for_catalog_item_priority:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_schedules.for_catalog_item_priority:=" + exc);
	}
        return returnString;
    }  
}

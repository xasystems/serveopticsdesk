/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class get_help 
{
    private static Logger logger = LogManager.getLogger();
    
    public static ArrayList<String[]> all(Connection con) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try 
        {
            stmt = con.prepareStatement("SELECT * FROM help");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = {"","","","","",""};
                temp[0] = rs.getString("id");
                temp[1] = rs.getString("topic");
                temp[2] = rs.getString("anchor");
                temp[3] = rs.getString("url");
                temp[4] = rs.getString("description");
                temp[5] = rs.getString("html_content");
                return_arraylist.add(temp);    
            }           
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_help.all:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_help.all:=" + exc);
	}
        return return_arraylist;
    }   
}

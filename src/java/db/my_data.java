/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class my_data 
{
    private static Logger logger = LogManager.getLogger();
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
    private static SimpleDateFormat form_format = new SimpleDateFormat("HH:mm / dd-MMM-YY"); //10:42 / 24-Jan-20
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    public static ArrayList<String[]> my_list(Connection con, String user_id) throws IOException, SQLException
    {
        java.util.Date now = new java.util.Date();
        ArrayList <String[]> return_arraylist = new ArrayList();
        
        long array_to_sort[][] = new long[0][0];
        String array_content[][] = new String[0][0];
        
        int incident_count=0;
        int request_count=0;
        int task_count = 0;
        int job_count = 0;
        //get incidents
            //link=incident.jsp?id=106487
            //ID INCxxxxxxxx 
            //Name
            //Description
            //Priority
            //Time
            //State
        ArrayList<String[]> my_incidents = db.get_incidents.my_open_incidents(con, user_id);
        incident_count = my_incidents.size();
        
        //get requests
            //link=request.jsp?id=106487
            //ID REQxxxxxxxx 
            //Name
            //Description
            //Priority
            //Time
            //State
        ArrayList<String[]> my_requests = db.get_requests.my_open_requests(con, user_id);
        request_count = my_requests.size();
        
        //get tasks
            //link=project_task.jsp?id=106487
            //ID TSKxxxxxxxx 
            //Name
            //Description
            //Priority
            //Time
            //State
        ArrayList <String[]> my_tasks = db.get_projects.my_open_project_tasks(con, user_id);
        task_count = my_tasks.size();
        //jobs
            //link=job.jsp?id=106487
            //ID JOBxxxxxxxx 
            //Name
            //Description
            //Priority
            //Time
            //State
        ArrayList <String[]> my_jobs = db.get_jobs.my_open_jobs(con, user_id);
        job_count = my_jobs.size();
        
        array_content = new String[incident_count + request_count + task_count + job_count][8];
        array_to_sort = new long[incident_count + request_count + task_count + job_count][2];
        
        //add inc
        for(int a = 0; a < my_incidents.size(); a++)
        {
            String this_incident[] = my_incidents.get(a);
            array_to_sort[a][0] = a;
            array_content[a][0] = "incident.jsp?id=" + this_incident[0];
            array_content[a][1] = "INC" + this_incident[0];
            array_content[a][2] = this_incident[15];
            array_content[a][3] = "Category(" + this_incident[9] + ") Subcategory(" + this_incident[10] + ")";
            array_content[a][4] = this_incident[14];
            //do time to long
            try
            {
                java.util.Date temp = timestamp_format.parse(this_incident[2]);
                array_content[a][5] = String.valueOf(temp.getTime());
                array_to_sort[a][1] = temp.getTime();
            }
            catch(Exception e)
            {
                array_content[a][5] = String.valueOf(now.getTime());
                array_to_sort[a][1] = now.getTime();
            }            
            array_content[a][6] = this_incident[19];
        }
        //add req
        int count = incident_count;
        for(int a = 0; a < my_requests.size(); a++)
        {
            String request[] = my_requests.get(a);
            array_to_sort[count][0] = count;
            array_content[count][0] = "request.jsp?id=" + request[0];
            array_content[count][1] = "REQ" + request[0];
            array_content[count][2] = request[29];
            array_content[count][3] = request[29];
            array_content[count][4] = request[22];
            //do time to long
            try
            {
                java.util.Date temp = timestamp_format.parse(request[5]);
                array_content[count][5] = String.valueOf(temp.getTime());
                array_to_sort[count][1] = temp.getTime();
            }
            catch(Exception e)
            {
                array_content[count][5] = String.valueOf(now.getTime());
                array_to_sort[count][1] = now.getTime();
            }            
            array_content[count][6] = request[23];
            count++;
        }
        //add task
        count = (incident_count + request_count);
        for(int a = 0; a < my_tasks.size(); a++)
        {
            array_to_sort[count][0] = count;
            String this_task[] = my_tasks.get(a);
            array_content[count][0] = "project_task_edit.jsp?task_id=" + this_task[0] + "&project_id=" + this_task[1];
            array_content[count][1] = "TSK" + this_task[0];
            array_content[count][2] = this_task[2];
            array_content[count][3] = this_task[3];
            array_content[count][4] = this_task[6];
            //do time to long
            try
            {
                java.util.Date temp = timestamp_format.parse(this_task[8]);
                array_content[count][5] = String.valueOf(temp.getTime());
                array_to_sort[count][1] = temp.getTime();
            }
            catch(Exception e)
            {
                array_content[count][5] = String.valueOf(now.getTime());
                array_to_sort[count][1] = now.getTime();
            }            
            array_content[count][6] = this_task[7];
            count++;
        }
        //add job
        count = (incident_count + request_count + task_count);
        for(int a = 0; a < my_jobs.size(); a++)
        {
            array_to_sort[count][0] = count;
            String this_job[] = my_jobs.get(a);
            array_content[count][0] = "job.jsp?id=" + this_job[0];
            array_content[count][1] = "JOB" + this_job[0];
            array_content[count][2] = this_job[1];
            array_content[count][3] = this_job[2];
            array_content[count][4] = this_job[5];
            //do time to long
            try
            {
                java.util.Date temp = timestamp_format.parse(this_job[7]);
                array_content[count][5] = String.valueOf(temp.getTime());
                array_to_sort[count][1] = temp.getTime();
            }
            catch(Exception e)
            {
                array_content[count][5] = String.valueOf(now.getTime());
                array_to_sort[count][1] = now.getTime();
            }            
            array_content[count][6] = this_job[6];
            count++;
        }        
        //sortbyColumn(array_to_sort, col - 1); 
        Arrays.sort(array_to_sort, (a, b) -> Long.compare(a[1], b[1]));    
        for(int b = 0; b < array_to_sort.length; b++)
        {
            int index = (int)array_to_sort[b][0];
            String row[] = {array_content[index][0], array_content[index][1],array_content[index][2],array_content[index][3],array_content[index][4],array_content[index][5],array_content[index][6]};
            return_arraylist.add(row);
            
        }
        return return_arraylist;
    }
    public static ArrayList<String[]> my_self_service_list(Connection con, String user_id, String user_tz_time) throws IOException, SQLException
    {
        java.util.Date now = new java.util.Date();
        ArrayList <String[]> return_arraylist = new ArrayList();
        
        long array_to_sort[][] = new long[0][0];
        String array_content[][] = new String[0][0];
        
        int incident_count=0;
        int request_count=0;
        int task_count = 0;
        int job_count = 0;
        //get incidents
            //link=incident.jsp?id=106487
            //ID INCxxxxxxxx 
            //Name
            //Description
            //Priority
            //Time
            //State
        ArrayList<String[]> my_incidents = db.get_incidents.incidents_for_user_id(con, user_id);
        incident_count = my_incidents.size();        
        //get requests
            //link=request.jsp?id=106487
            //ID REQxxxxxxxx 
            //Name
            //Description
            //Priority
            //Time
            //State
        ArrayList<String[]> my_requests = db.get_requests.my_requests_for_self_help(con, user_id, user_tz_time);
        request_count = my_requests.size();
        
        //get tasks
            //link=project_task.jsp?id=106487
            //ID TSKxxxxxxxx 
            //Name
            //Description
            //Priority
            //Time
            //State
        
        
        array_content = new String[incident_count + request_count][8];
        array_to_sort = new long[incident_count + request_count][2];
        
        //add inc
        for(int a = 0; a < my_incidents.size(); a++)
        {
            String this_incident[] = my_incidents.get(a);
            array_to_sort[a][0] = a;
            array_content[a][0] = "incident.jsp?id=" + this_incident[0];
            array_content[a][1] = "INC" + this_incident[0];
            array_content[a][2] = this_incident[15];
            array_content[a][3] = "Category(" + this_incident[9] + ") Subcategory(" + this_incident[10] + ")";
            array_content[a][4] = this_incident[14];
            //do time to long
            try
            {
                java.util.Date temp = timestamp_format.parse(this_incident[2]);
                array_content[a][5] = String.valueOf(temp.getTime());
                array_to_sort[a][1] = temp.getTime();
            }
            catch(Exception e)
            {
                array_content[a][5] = String.valueOf(now.getTime());
                array_to_sort[a][1] = now.getTime();
            }            
            array_content[a][6] = this_incident[19];
            array_content[a][7] = this_incident[23];
        }
        //add req
        int count = incident_count;
        for(int a = 0; a < my_requests.size(); a++)
        {
            String request[] = my_requests.get(a);
            array_to_sort[count][0] = count;
            array_content[count][0] = "request.jsp?id=" + request[0];
            array_content[count][1] = "REQ" + request[0];
            array_content[count][2] = request[57];
            array_content[count][3] = request[58];
            array_content[count][4] = request[22];
            //do time to long
            try
            {
                java.util.Date temp = timestamp_format.parse(request[7]);
                array_content[count][5] = String.valueOf(temp.getTime());
                array_to_sort[count][1] = temp.getTime();
            }
            catch(Exception e)
            {
                array_content[count][5] = String.valueOf(now.getTime());
                array_to_sort[count][1] = now.getTime();
            }            
            array_content[count][6] = request[23];
            array_content[count][7] = request[27];
            count++;
        }
        //add task
        count = (incident_count + request_count);
               
        //sortbyColumn(array_to_sort, col - 1); 
        Arrays.sort(array_to_sort, (a, b) -> Long.compare(a[1], b[1]));    
        for(int b = 0; b < array_to_sort.length; b++)
        {
            int index = (int)array_to_sort[b][0];
            String row[] = {array_content[index][0], array_content[index][1],array_content[index][2],array_content[index][3],array_content[index][4],array_content[index][5],array_content[index][6],array_content[index][7]};
            return_arraylist.add(row);            
        }
        return return_arraylist;
    }    
}

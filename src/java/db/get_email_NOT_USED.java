/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class get_email_NOT_USED 
{
    private static Logger logger = LogManager.getLogger();
    static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
    
    
    public static ArrayList<String[]> all_email(Connection con) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        String query = "SELECT * FROM email"; 
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String email[] = new String[17];
                //init the array
                for(int a = 0; a < email.length;a++)
                {
                    email[a] = "--";
                }
                
                email[0] = rs.getString("id");
                email[1] = rs.getString("uuid");
                email[2] = support.string_utils.check_for_null(timestamp_format.format(rs.getTimestamp("date_sent")));
                email[3] = support.string_utils.check_for_null(rs.getString("from"));
                email[4] = support.string_utils.check_for_null(rs.getString("subject"));                
                email[5] = support.string_utils.check_for_null(rs.getString("to"));  
                email[6] = support.string_utils.check_for_null(rs.getString("body"));  
                email[7] = support.string_utils.check_for_null(rs.getString("mime_type"));  
                email[8] = support.string_utils.check_for_null(rs.getString("status"));  
                email[9] = support.string_utils.check_for_null(rs.getString("status_date"));  
                email[10] = support.string_utils.check_for_null(rs.getString("status_by_user_id"));  
                email[11] = support.string_utils.check_for_null(rs.getString("result"));  
                email[12] = support.string_utils.check_for_null(rs.getString("has_attachment"));  
                email[13] = support.string_utils.check_for_null(rs.getString("description"));  
                email[14] = support.string_utils.check_for_null(rs.getString("log"));  
                email[15] = support.string_utils.check_for_null(rs.getString("related_to"));  
                email[16] = support.string_utils.check_for_null(rs.getString("related_to_id"));                  
                return_array_list.add(email);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_email.all_email:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_email.all_email:=" + exc);
	}
        return return_array_list;
    }
    public static String[] email_by_id(Connection con, String id) throws IOException, SQLException
    {
        String[] return_array = new String[17];
        String query = "SELECT * FROM email WHERE id=?"; 
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                for(int a = 0; a < return_array.length;a++)
                {
                    return_array[a] = "--";
                }                
                return_array[0] = rs.getString("id");
                return_array[1] = rs.getString("uuid");
                return_array[2] = support.string_utils.check_for_null(timestamp_format.format(rs.getTimestamp("date_sent")));
                return_array[3] = support.string_utils.check_for_null(rs.getString("from"));
                return_array[4] = support.string_utils.check_for_null(rs.getString("subject"));                
                return_array[5] = support.string_utils.check_for_null(rs.getString("to"));  
                return_array[6] = support.string_utils.check_for_null(rs.getString("body"));  
                return_array[7] = support.string_utils.check_for_null(rs.getString("mime_type"));  
                return_array[8] = support.string_utils.check_for_null(rs.getString("status"));  
                return_array[9] = support.string_utils.check_for_null(timestamp_format.format(rs.getTimestamp("status_date")));  
                return_array[10] = support.string_utils.check_for_null(rs.getString("status_by_user_id"));  
                return_array[11] = support.string_utils.check_for_null(rs.getString("result"));  
                return_array[12] = support.string_utils.check_for_null(rs.getString("has_attachment"));  
                return_array[13] = support.string_utils.check_for_null(rs.getString("description"));  
                return_array[14] = support.string_utils.check_for_null(rs.getString("log"));  
                return_array[15] = support.string_utils.check_for_null(rs.getString("related_to"));  
                return_array[16] = support.string_utils.check_for_null(rs.getString("related_to_id"));  
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_email.email_by_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_email.email_by_id:=" + exc);
	}
        return return_array;
    }
    public static String[] email_by_uuid(Connection con, String uuid) throws IOException, SQLException
    {
        String[] return_array = new String[17];
        String query = "SELECT * FROM email WHERE uuid=?"; 
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, uuid);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                for(int a = 0; a < return_array.length;a++)
                {
                    return_array[a] = "--";
                }                
                return_array[0] = rs.getString("id");
                return_array[1] = rs.getString("uuid");
                return_array[2] = support.string_utils.check_for_null(timestamp_format.format(rs.getTimestamp("date_sent")));
                return_array[3] = support.string_utils.check_for_null(rs.getString("from"));
                return_array[4] = support.string_utils.check_for_null(rs.getString("subject"));                
                return_array[5] = support.string_utils.check_for_null(rs.getString("to"));  
                return_array[6] = support.string_utils.check_for_null(rs.getString("body"));  
                return_array[7] = support.string_utils.check_for_null(rs.getString("mime_type"));  
                return_array[8] = support.string_utils.check_for_null(rs.getString("status"));  
                return_array[9] = support.string_utils.check_for_null(rs.getString("status_date"));  
                return_array[10] = support.string_utils.check_for_null(rs.getString("status_by_user_id"));  
                return_array[11] = support.string_utils.check_for_null(rs.getString("result"));  
                return_array[12] = support.string_utils.check_for_null(rs.getString("has_attachment"));  
                return_array[13] = support.string_utils.check_for_null(rs.getString("description"));  
                return_array[14] = support.string_utils.check_for_null(rs.getString("log"));  
                return_array[15] = support.string_utils.check_for_null(rs.getString("related_to"));  
                return_array[16] = support.string_utils.check_for_null(rs.getString("related_to_id"));  
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_email.email_by_uuid:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_email.email_by_uuid:=" + exc);
	}
        return return_array;
    }
    public static ArrayList<String[]> all_email_attachment_info(Connection con) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        String query = "SELECT * FROM email_attachements"; 
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String email[] = new String[6];
                //init the array
                for(int a = 0; a < email.length;a++)
                {
                    email[a] = "--";
                }
                
                email[0] = rs.getString("id");
                email[1] = rs.getString("uuid");
                email[2] = support.string_utils.check_for_null(rs.getString("name_on_disk"));
                email[3] = support.string_utils.check_for_null(rs.getString("file_name"));
                email[4] = support.string_utils.check_for_null(rs.getString("file_date"));                
                email[5] = support.string_utils.check_for_null(rs.getString("file_size"));                   
                return_array_list.add(email);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_email.all_email_attachment_info:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_email.all_email_attachment_info:=" + exc);
	}
        return return_array_list;
    }
    public static String[] email_attachment_info_for_id(Connection con, String id) throws IOException, SQLException
    {
        String[] return_array = new String[6];
        String query = "SELECT * FROM email_attachements WHERE id=?"; 
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                for(int a = 0; a < return_array.length;a++)
                {
                    return_array[a] = "--";
                }
                
                return_array[0] = rs.getString("id");
                return_array[1] = rs.getString("uuid");
                return_array[2] = support.string_utils.check_for_null(rs.getString("name_on_disk"));
                return_array[3] = support.string_utils.check_for_null(rs.getString("file_name"));
                return_array[4] = support.string_utils.check_for_null(rs.getString("file_date"));                
                return_array[5] = support.string_utils.check_for_null(rs.getString("file_size"));    
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_email.email_attachment_info_for_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_email.email_attachment_info_for_id:=" + exc);
	}
        return return_array;
    }
    public static ArrayList<String[]> all_email_attachment_info_for_uuid(Connection con, String uuid) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        String query = "SELECT * FROM email_attachements WHERE uuid=?"; 
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, uuid);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String email[] = new String[6];
                //init the array
                for(int a = 0; a < email.length;a++)
                {
                    email[a] = "--";
                }
                
                email[0] = rs.getString("id");
                email[1] = rs.getString("uuid");
                email[2] = support.string_utils.check_for_null(rs.getString("name_on_disk"));
                email[3] = support.string_utils.check_for_null(rs.getString("file_name"));
                email[4] = support.string_utils.check_for_null(rs.getString("file_date"));                
                email[5] = support.string_utils.check_for_null(rs.getString("file_size"));                   
                return_array_list.add(email);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_email.all_email_attachment_info_for_uuid:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_email.all_email_attachment_info_for_uuid:=" + exc);
	}
        return return_array_list;
    }
    public static File file_attachement_by_id(Connection con, LinkedHashMap sys_props, String customer_db, String id)
    {
        File return_file = null;
        String file_name = "";
        try
        {
            String file_info[] = db.get_email_NOT_USED.email_attachment_info_for_id(con, id);
            file_name = file_info[2];
        }
        catch(Exception e)
        {
            
        }
        String config_email_attachment_store = sys_props.get("email_attachment_store").toString();
        
        File this_customer_email_attachement = new File(config_email_attachment_store + "/" + customer_db + "/attachments/" + file_name);
        if(!this_customer_email_attachement.exists()) //if the customer attachment dir not exist ctreate it
        {   
            return_file = null;
        }
        else
        {
            return_file = this_customer_email_attachement;
        }
        
        return return_file;
    }
    public static ArrayList <File> file_attachement_by_email_uuid(Connection con, LinkedHashMap sys_props, String customer_db, String uuid)
    {
        ArrayList <File> return_files_arraylist = new ArrayList();
        ArrayList <String> file_names = new ArrayList();
        try
        {
            ArrayList <String[]> all_attachements = db.get_email_NOT_USED.all_email_attachment_info_for_uuid(con, uuid);
            for(int a = 0; a < all_attachements.size(); a++)
            {
                String attachment[] = all_attachements.get(a);
                file_names.add(attachment[2]);
            }
        }
        catch(Exception e)
        {
        }
        
        String config_email_attachment_store = sys_props.get("email_attachment_store").toString();  
        for(int b = 0; b < file_names.size();b++)
        {
            String this_file_name = file_names.get(b);
            File this_customer_email_attachement = new File(config_email_attachment_store + "/" + customer_db + "/attachments/" + this_file_name);
            if(this_customer_email_attachement.exists()) //if the customer attachment dir not exist ctreate it
            {   
                return_files_arraylist.add(this_customer_email_attachement);
            }
        }
        return return_files_arraylist;
    }    
    public static ArrayList<String[]> email_by_status(Connection con, String status) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        String query = "SELECT * FROM email WHERE status=? ORDER BY date_sent DESC"; 
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, status);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String email[] = new String[17];
                //init the array
                for(int a = 0; a < email.length;a++)
                {
                    email[a] = "--";
                }
                email[0] = rs.getString("id");
                email[1] = rs.getString("uuid");
                email[2] = support.string_utils.check_for_null(timestamp_format.format(rs.getTimestamp("date_sent")));
                email[3] = support.string_utils.check_for_null(rs.getString("from"));
                email[4] = support.string_utils.check_for_null(rs.getString("subject"));                
                email[5] = support.string_utils.check_for_null(rs.getString("to"));  
                email[6] = support.string_utils.check_for_null(rs.getString("body"));  
                email[7] = support.string_utils.check_for_null(rs.getString("mime_type"));  
                email[8] = support.string_utils.check_for_null(rs.getString("status"));  
                email[9] = support.string_utils.check_for_null(rs.getString("status_date"));  
                email[10] = support.string_utils.check_for_null(rs.getString("status_by_user_id"));  
                email[11] = support.string_utils.check_for_null(rs.getString("result"));  
                email[12] = support.string_utils.check_for_null(rs.getString("has_attachment"));  
                email[13] = support.string_utils.check_for_null(rs.getString("description"));  
                email[14] = support.string_utils.check_for_null(rs.getString("log"));  
                email[15] = support.string_utils.check_for_null(rs.getString("related_to"));  
                email[16] = support.string_utils.check_for_null(rs.getString("related_to_id"));                  
                return_array_list.add(email);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_email.email_by_status:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_email.email_by_status:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> email_by_open_status(Connection con) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        String query = "SELECT * FROM email WHERE status='Open' OR status='INW'"; 
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String email[] = new String[17];
                //init the array
                for(int a = 0; a < email.length;a++)
                {
                    email[a] = "--";
                }
                email[0] = rs.getString("id");
                email[1] = rs.getString("uuid");
                email[2] = support.string_utils.check_for_null(timestamp_format.format(rs.getTimestamp("date_sent")));
                email[3] = support.string_utils.check_for_null(rs.getString("from"));
                email[4] = support.string_utils.check_for_null(rs.getString("subject"));                
                email[5] = support.string_utils.check_for_null(rs.getString("to"));  
                email[6] = support.string_utils.check_for_null(rs.getString("body"));  
                email[7] = support.string_utils.check_for_null(rs.getString("mime_type"));  
                email[8] = support.string_utils.check_for_null(rs.getString("status"));  
                email[9] = support.string_utils.check_for_null(rs.getString("status_date"));  
                email[10] = support.string_utils.check_for_null(rs.getString("status_by_user_id"));  
                email[11] = support.string_utils.check_for_null(rs.getString("result"));  
                email[12] = support.string_utils.check_for_null(rs.getString("has_attachment"));  
                email[13] = support.string_utils.check_for_null(rs.getString("description"));  
                email[14] = support.string_utils.check_for_null(rs.getString("log"));  
                email[15] = support.string_utils.check_for_null(rs.getString("related_to"));  
                email[16] = support.string_utils.check_for_null(rs.getString("related_to_id"));                  
                return_array_list.add(email);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_email.email_by_open_status:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_email.email_by_open_status:=" + exc);
	}
        return return_array_list;
    }
}

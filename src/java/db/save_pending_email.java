//Copyright 2020 XaSystems, Inc. , All rights reserved.
package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author rcampbell
 */
public class save_pending_email 
{
    private static Logger logger = LogManager.getLogger();
    
    public static boolean now(Connection con, String email_address, String subject, String body)
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss"); 
        java.util.Date now = new java.util.Date();
        String timestamp = timestamp_format.format(now);
        Boolean status = false;
        PreparedStatement stmt;
        String email_status = "pending";
        subject = StringEscapeUtils.escapeHtml4(subject);
        body = StringEscapeUtils.escapeHtml4(body);
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("INSERT INTO pending_email (email,subject,body,timestamp,status) VALUES (?,?,?,?,?)");
            stmt.setString(1, email_address);
            stmt.setString(2, subject);
            stmt.setString(3, body);
            stmt.setString(4, timestamp);
            stmt.setString(5, email_status);            
            stmt.executeUpdate();
        }
        catch(Exception e)
        {
            logger.error("ERROR Exception save_pending_email.now:=" + e);
        }
        return status;
    }
}

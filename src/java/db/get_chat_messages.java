/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class get_chat_messages 
{
    private static Logger logger = LogManager.getLogger();

    public static String[] chat_room_users(Connection con, String object_type, String object_id, String parent_object_type, String parent_object_id) throws IOException, SQLException
    {
        PreparedStatement stmt;
        Set<String> room_users = new HashSet<String>();
        try 
        {

            // Create a Statement Object
            String query = "SELECT "
                    + "messages.sender_id"
                + " FROM chat_messages AS messages "
                + " WHERE object_type = ? AND object_id = ? AND parent_object_type = ? AND parent_object_id = ? AND sender_id > 0"
                + " GROUP BY sender_id";
            stmt = con.prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, object_type);
            stmt.setString(2, object_id);
            stmt.setString(3, parent_object_type);
            stmt.setString(4, parent_object_id);
//            System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                room_users.add(rs.getString("sender_id"));
            }

            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chat_messages.chat_room_users:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chat_messages.chat_room_users:=" + exc);
	}
        return room_users.toArray(new String[0]);
    }

    public static ArrayList<String[]> get_messages(Connection con, String object_type, String object_id, String parent_object_type, String parent_object_id) throws IOException, SQLException
    {
        return get_messages(con, object_type, object_id, parent_object_type, parent_object_id, 0, 20);
    }

    public static ArrayList<String[]> get_messages(Connection con, String object_type, String object_id, String parent_object_type, String parent_object_id, int start, int length) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        PreparedStatement stmt;

        try 
        {

            // Create a Statement Object
            String query = "SELECT "
                    + "messages.id,"
                    + "messages.object_type,"
                    + "messages.object_id,"
                    + "messages.parent_object_type,"
                    + "messages.parent_object_id,"
                    + "messages.sender_id,"
                    + "CONCAT(users.first, ' ', users.last) as sender_name,"
                    + "messages.content,"
                    + "DATE_FORMAT(messages.date_sent,'%Y-%m-%dT%H:%i:%s') as date_sent,"
                    + "messages.is_system,"
                    + "messages.saved,"
                    + "messages.distibuted,"
                    + "messages.seen,"
                    + "messages.deleted, "
                    + "messages.reply_to_id, "
                    + "messages.reply_to_content "
                + " FROM chat_messages AS messages "
                + " LEFT JOIN users on messages.sender_id = users.id "
//                + " LEFT JOIN chat_message_reactions on messages.id = chat_message_reactions.message_id "
                + " WHERE object_type = ? AND object_id = ? AND parent_object_type = ? AND parent_object_id = ? " + (start > 0 ? "AND messages.id < ?" : "")
                + " ORDER BY id DESC"
                + " LIMIT ?";
            stmt = con.prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, object_type);
            stmt.setString(2, object_id);
            stmt.setString(3, parent_object_type);
            stmt.setString(4, parent_object_id);
            if (start > 0) {
                stmt.setInt(5, start);
                stmt.setInt(6, length);
            } else {
                stmt.setInt(5, length);
            }
            System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();

            while(rs.next())
            {
                String message[] = new String[16];
                message[0] = rs.getString("id");
                message[1] = rs.getString("object_type");
                message[2] = rs.getString("object_id");
                message[3] = rs.getString("parent_object_type");
                message[4] = rs.getString("parent_object_id");
                message[5] = rs.getString("sender_id");
                message[6] = rs.getString("sender_name");
                message[7] = rs.getString("content");
                message[8] = rs.getString("date_sent");
                message[9] = rs.getString("is_system");
                message[10] = rs.getString("saved");
                message[11] = rs.getString("distibuted");
                message[12] = rs.getString("seen");
                message[13] = rs.getString("deleted");
                message[14] = rs.getString("reply_to_id");
                message[15] = rs.getString("reply_to_content");
                return_array_list.add(message);
            }

            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chat_messages.get_messages:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chat_messages.get_messages:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> get_attachments(Connection con, long message_id) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        PreparedStatement stmt;

        try 
        {

            // Create a Statement Object
            String query = "SELECT * FROM chat_message_attachements WHERE chat_message_id = ?";
            stmt = con.prepareStatement(query);
            stmt.setLong(1, message_id);
//            System.out.println("query=" + stmt.toString());
            ResultSet rs = stmt.executeQuery();

            while(rs.next())
            {
                String attachment[] = new String[7];
                attachment[0] = rs.getString("id");
                attachment[1] = rs.getString("uuid");
                attachment[2] = rs.getString("name_on_disk");
                attachment[3] = rs.getString("file_name");
                attachment[4] = rs.getString("file_date");
                attachment[5] = rs.getString("file_size");
                attachment[6] = rs.getString("file_type");
                return_array_list.add(attachment);
            }

            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chat_messages.get_attachments:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chat_messages.get_attachments:=" + exc);
	}
        return return_array_list;
    }
}

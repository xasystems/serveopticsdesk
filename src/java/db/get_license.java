//Copyright 2020 XaSystems, Inc. , All rights reserved.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 *
 * @author server-xc6701
 */
public class get_license 
{
    private static Logger logger = LogManager.getLogger();
    
    public static String all(Connection con) throws IOException, SQLException
    {
        String return_string = "";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM system_license");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_string = rs.getString("license");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_license.all:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_license.all:=" + exc);
	}
        return return_string;
    } 
    public static String[] decoded(String context_dir, String license)
    {
        String return_array[] = {"","",""}; //licensee + "sEp,sEp" + seats + "sEp,sEp" + expire_date;
        try
        {
            
            String unencrypted_license = support.encrypt_utils.decrypt(context_dir, license);
            return_array = unencrypted_license.split("sEp,sEp");
        }
        catch(Exception e)
        {
            System.out.println("Exception in get_license.decode=" + e);
        }
        return return_array;
    }
}
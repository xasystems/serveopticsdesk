//Copyright 2019 XaSystems, Inc. , All rights reserved.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 *
 * @author server-xc6701
 */
public class get_holidays 
{
    private static Logger logger = LogManager.getLogger();
    
    public static String[][] all(Connection con) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM holidays", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][5];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("name");
                returnString[count][2] = rs.getString("description");
                returnString[count][3] = timestamp_format.format(rs.getTimestamp("start"));
                returnString[count][4] = timestamp_format.format(rs.getTimestamp("end"));     
                count++;                
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_holidays.all:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_holidays.all:=" + exc);
	}
        return returnString;
    }
    public static String[] by_id(Connection con, String id) throws IOException, SQLException
    {
        String returnString[] = {"","","","",""};
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM holidays WHERE id=?");
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                returnString[0] = rs.getString("id");
                returnString[1] = rs.getString("name");
                returnString[2] = rs.getString("description");
                returnString[3] = timestamp_format.format(rs.getTimestamp("start").getTime());
                returnString[4] = timestamp_format.format(rs.getTimestamp("end").getTime());
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_holidays.by_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_holidays.by_id:=" + exc);
	}
        return returnString;
    }    
}

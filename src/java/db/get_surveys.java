/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class get_surveys 
{
    private static Logger logger = LogManager.getLogger();
    
    public static String[][] all_csat_by_trigger_object(Connection con, String trigger_object) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        PreparedStatement stmt = null;
        String query = "";        
        try 
        {
            // Create a Statement Object
            if(trigger_object.equalsIgnoreCase("all"))
            {
                query = "SELECT * FROM survey WHERE type='CSAT'";
                stmt = con.prepareStatement(query);
            }
            else
            {
                query = "SELECT * FROM survey WHERE type='CSAT' AND trigger_object=?";
                stmt = con.prepareStatement(query);
                stmt.setString(1, "trigger_object");
            }
            stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count][20];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("id");
                return_string[count][1] = timestamp_format.format(rs.getTimestamp("create_date"));
                return_string[count][2] = rs.getString("name");
                return_string[count][3] = rs.getString("description");
                return_string[count][4] = rs.getString("version");
                return_string[count][5] = rs.getString("status");
                return_string[count][6] = rs.getString("type");
                return_string[count][7] = rs.getString("contact_method");
                return_string[count][8] = rs.getString("trigger_object");
                return_string[count][9] = rs.getString("trigger_state");
                return_string[count][10] = rs.getString("survey_trigger");
                return_string[count][11] = rs.getString("trigger_interval");
                return_string[count][12] = rs.getString("max_per_person");
                return_string[count][13] = rs.getString("lifespan");
                return_string[count][14] = rs.getString("email_subject");
                return_string[count][15] = rs.getString("email_body");
                return_string[count][16] = rs.getString("email_reminder_schedule");
                return_string[count][17] = rs.getString("email_reminder_subject");
                return_string[count][18] = rs.getString("email_reminder_body");
                return_string[count][19] = rs.getString("survey_json");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_surveys.all_csat_by_trigger_object:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_surveys.all_csat_by_trigger_object:=" + exc);
	}
        
        return return_string;
    }    
    public static String[][] all_csat_by_filters(Connection con, List<Map<String, String>> filters) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        PreparedStatement stmt;
        String query;        
        try 
        {
            String where = "";
            for (int i = 0; i < filters.size(); i++)
            {
              String filter_key = filters.get(i).get("key");
              String filter_value = filters.get(i).get("value");
              where += " AND " + filter_key + " = ? ";
            }
            
            // Create a Statement Object
            query = "SELECT * FROM survey WHERE type='CSAT' " + where;
            stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            for (int i = 0; i < filters.size(); i++)
            {
              String filter_value = filters.get(i).get("value");
              stmt.setString(i+1, filter_value);
            }

            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count][20];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("id");
                return_string[count][1] = timestamp_format.format(rs.getTimestamp("create_date"));
                return_string[count][2] = rs.getString("name");
                return_string[count][3] = rs.getString("description");
                return_string[count][4] = rs.getString("version");
                return_string[count][5] = rs.getString("status");
                return_string[count][6] = rs.getString("type");
                return_string[count][7] = rs.getString("contact_method");
                return_string[count][8] = rs.getString("trigger_object");
                return_string[count][9] = rs.getString("trigger_state");
                return_string[count][10] = rs.getString("survey_trigger");
                return_string[count][11] = rs.getString("trigger_interval");
                return_string[count][12] = rs.getString("max_per_person");
                return_string[count][13] = rs.getString("lifespan");
                return_string[count][14] = rs.getString("email_subject");
                return_string[count][15] = rs.getString("email_body");
                return_string[count][16] = rs.getString("email_reminder_schedule");
                return_string[count][17] = rs.getString("email_reminder_subject");
                return_string[count][18] = rs.getString("email_reminder_body");
                return_string[count][19] = rs.getString("survey_json");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_surveys.all_csat_by_filters:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_surveys.all_csat_by_filters:=" + exc);
	}
        
        return return_string;
    }    
    public static String[][] all_active_csat(Connection con) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        PreparedStatement stmt = null;
        String query = "";        
        try 
        {
            // Create a Statement Object
            query = "SELECT * FROM survey WHERE type='CSAT' AND status='Active' AND (trigger_object='Incident' OR trigger_object='Request')";
            stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count][20];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("id");
                return_string[count][1] = timestamp_format.format(rs.getTimestamp("create_date"));
                return_string[count][2] = rs.getString("name");
                return_string[count][3] = rs.getString("description");
                return_string[count][4] = rs.getString("version");
                return_string[count][5] = rs.getString("status");
                return_string[count][6] = rs.getString("type");
                return_string[count][7] = rs.getString("contact_method");
                return_string[count][8] = rs.getString("trigger_object");
                return_string[count][9] = rs.getString("trigger_state");
                return_string[count][10] = rs.getString("survey_trigger");
                return_string[count][11] = rs.getString("trigger_interval");
                return_string[count][12] = rs.getString("max_per_person");
                return_string[count][13] = rs.getString("lifespan");
                return_string[count][14] = rs.getString("email_subject");
                return_string[count][15] = rs.getString("email_body");
                return_string[count][16] = rs.getString("email_reminder_schedule");
                return_string[count][17] = rs.getString("email_reminder_subject");
                return_string[count][18] = rs.getString("email_reminder_body");
                return_string[count][19] = rs.getString("survey_json");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_surveys.all_active_csat:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_surveys.all_active_csat:=" + exc);
	}
        
        return return_string;
    }
    public static String[][] reportable_csat_by_trigger_object(Connection con, String trigger_object) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        PreparedStatement stmt = null;
        String query = "";        
        try 
        {
            // Create a Statement Object
            
            
            if(trigger_object.equalsIgnoreCase("all"))
            {
                query = "SELECT * FROM survey WHERE type='CSAT' AND (`status`='Active' OR `status`='Inactive')";
                stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            }
            else
            {
                
                stmt = con.prepareStatement("SELECT * FROM survey WHERE type='CSAT' AND trigger_object=? AND (`status`='Active' OR `status`='Inactive')", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
                stmt.setString(1,trigger_object);
                //System.out.println("q=" + stmt);
            }
            
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count][20];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("id");
                return_string[count][1] = timestamp_format.format(rs.getTimestamp("create_date"));
                return_string[count][2] = rs.getString("name");
                return_string[count][3] = rs.getString("description");
                return_string[count][4] = rs.getString("version");
                return_string[count][5] = rs.getString("status");
                return_string[count][6] = rs.getString("type");
                return_string[count][7] = rs.getString("contact_method");
                return_string[count][8] = rs.getString("trigger_object");
                return_string[count][9] = rs.getString("trigger_state");
                return_string[count][10] = rs.getString("survey_trigger");
                return_string[count][11] = rs.getString("trigger_interval");
                return_string[count][12] = rs.getString("max_per_person");
                return_string[count][13] = rs.getString("lifespan");
                return_string[count][14] = rs.getString("email_subject");
                return_string[count][15] = rs.getString("email_body");
                return_string[count][16] = rs.getString("email_reminder_schedule");
                return_string[count][17] = rs.getString("email_reminder_subject");
                return_string[count][18] = rs.getString("email_reminder_body");
                return_string[count][19] = rs.getString("survey_json");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_surveys.reportable_csat_by_trigger_object:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_surveys.reportable_csat_by_trigger_object:=" + exc);
	}
        
        return return_string;
    }
    public static String[] by_id(Connection con, String id) throws IOException, SQLException
    {
        String return_string[] = new String[0];
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM survey WHERE id=?");
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_string = new String[20];
                return_string[0] = rs.getString("id");
                return_string[1] = timestamp_format.format(rs.getTimestamp("create_date"));
                return_string[2] = rs.getString("name");
                return_string[3] = rs.getString("description");
                return_string[4] = rs.getString("version");
                return_string[5] = rs.getString("status");
                return_string[6] = rs.getString("type");
                return_string[7] = rs.getString("contact_method");
                return_string[8] = rs.getString("trigger_object");
                return_string[9] = rs.getString("trigger_state");
                return_string[10] = rs.getString("survey_trigger");
                return_string[11] = rs.getString("trigger_interval");
                return_string[12] = rs.getString("max_per_person");
                return_string[13] = rs.getString("lifespan");
                return_string[14] = rs.getString("email_subject");
                return_string[15] = rs.getString("email_body");
                return_string[16] = rs.getString("email_reminder_schedule");
                return_string[17] = rs.getString("email_reminder_subject");
                return_string[18] = rs.getString("email_reminder_body");
                return_string[19] = rs.getString("survey_json");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_surveys.all_csat:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_surveys.by_id:=" + exc);
	}
        return return_string;
    }
    public static String[][] all_like_name(Connection con, String name) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM survey WHERE name LIKE ?", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, '%' + name + '%');
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count][2];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("id");
                return_string[count][1] = rs.getString("name");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_surveys.by_name:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_surveys.by_name:=" + exc);
	}
        return return_string;
    }
    public static String[][] csat_questions_by_survey_id(Connection con, String survey_id) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];
        try
        {
            String survey_info[] = db.get_surveys.by_id(con, survey_id);
            String survey_json = survey_info[19];
            JsonReader reader = Json.createReader(new StringReader(survey_json));
            JsonObject resultObject = reader.readObject();
            JsonObject survey = null;
            JsonArray questions = null;
            int question_number = 0;
            String question_type = "";
            String question_text = "";
            try
            {
                survey = (JsonObject) resultObject.getJsonObject("survey");
                questions = (JsonArray) survey.getJsonArray("questions");
                return_string = new String[questions.size()][8]; //q number  q type   q text  1  2   3   4   5 
                
                //System.out.println("is_trending_positive answers.size=" + answers.size());
                for(int a = 0; a < questions.size(); a++) //for each question
                {
                    JsonObject this_record = questions.getJsonObject(a); //a single question
                    question_number = this_record.getInt("question_number");
                    question_type = this_record.getString("question_type");
                    question_text = this_record.getString("question_text");                    
                    
                    if(question_type.equalsIgnoreCase("true_false"))
                    {
                        return_string[a][0] =  String.valueOf(question_number);
                        return_string[a][1] =  question_type;
                        return_string[a][2] =  question_text;
                        return_string[a][3] =  this_record.getString("answer_text_1");   
                        return_string[a][4] = "";
                        return_string[a][5] = "";
                        return_string[a][6] = "";
                        return_string[a][7] = this_record.getString("answer_text_5");   
                    }
                    else
                    {
                        if(question_type.equalsIgnoreCase("rating_scale_5"))
                        {
                            return_string[a][0] = String.valueOf(question_number);
                            return_string[a][1] = question_type;
                            return_string[a][2] = question_text;
                            return_string[a][3] = this_record.getString("answer_text_1");   
                            return_string[a][4] = this_record.getString("answer_text_2");   
                            return_string[a][5] = this_record.getString("answer_text_3");   
                            return_string[a][6] = this_record.getString("answer_text_4");   
                            return_string[a][7] = this_record.getString("answer_text_5");   
                        }
                        else
                        {
                            if(question_type.equalsIgnoreCase("opinion"))
                            {
                                return_string[a][0] = String.valueOf(question_number);
                                return_string[a][1] = question_type;
                                return_string[a][2] = question_text;
                                return_string[a][3] = "";   
                                return_string[a][4] = "";
                                return_string[a][5] = "";
                                return_string[a][6] = ""; 
                                return_string[a][7] = ""; 
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {
                logger.error("ERROR get_surveys.csat_questions_by_survey_id JSON Parse:=" + e);
            }
            /*
            {
                "survey" : {
                        "questions" : [
                                {
                                        "answer_text_1" : "Not Satisfied",
                                        "answer_text_2" : "Somewhat Satisfied",
                                        "answer_text_3" : "Mostly Satisfied",
                                        "answer_text_4" : "Satisfied",
                                        "answer_text_5" : "Very satisfied",
                                        "question_number" : 1,
                                        "question_text" : "How courteous and respectful was the technician who responded?",
                                        "question_type" : "rating_scale_5"
                                },
                {
                    "answer_text_1" : "Not Satisfied",
                                        "answer_text_2" : "Somewhat Satisfied",
                                        "answer_text_3" : "Mostly Satisfied",
                                        "answer_text_4" : "Satisfied",
                                        "answer_text_5" : "Very satisfied",
                                        "question_number" : 2,
                                        "question_text" : "How satisfied were you with the response time to your incident?",
                                        "question_type" : "rating_scale_5"
                                },
                {
                    "answer_text_1" : "Not Satisfied",
                                        "answer_text_2" : "Somewhat Satisfied",
                                        "answer_text_3" : "Mostly Satisfied",
                                        "answer_text_4" : "Satisfied",
                                        "answer_text_5" : "Very satisfied",
                                        "question_number" : 3,
                                        "question_text" : "Please rate the technical competency of the technician serving you.",
                                        "question_type" : "rating_scale_5"
                                },
                {
                    "answer_text_1" : "Not Satisfied",
                                        "answer_text_2" : "Somewhat Satisfied",
                                        "answer_text_3" : "Mostly Satisfied",
                                        "answer_text_4" : "Satisfied",
                                        "answer_text_5" : "Very satisfied",
                                        "question_number" : 4,
                                        "question_text" : "How satisfied are you with your overall service experience?",
                                        "question_type" : "rating_scale_5"
                                },
                {
                    "answer_text_1" : "Not Satisfied",
                                        "answer_text_2" : "Somewhat Satisfied",
                                        "answer_text_3" : "Mostly Satisfied",
                                        "answer_text_4" : "Satisfied",
                                        "answer_text_5" : "Very satisfied",
                                        "question_number" : 5,
                                        "question_text" : "Was technician able to resolve your issue during the first call?",
                                        "question_type" : "rating_scale_5"
                                },
                {
                    "question_number" : 6,
                                        "question_text" : "Comments",
                                        "question_type" : "opinion"
                                }
                        ]
                }
            }*/
        }
        catch(Exception e)
        {
            logger.error("ERROR SQLException get_surveys.csat_questions_by_survey_id:=" + e);
        }
        return return_string;
    }
}

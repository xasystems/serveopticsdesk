/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class get_assets 
{
    private static Logger logger = LogManager.getLogger();
    public static ArrayList<String[]> all_limited(Connection con, int starting_record, int fetch_size) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM assets ORDER BY id LIMIT " + starting_record + "," + fetch_size , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[19];
                this_record[0] = rs.getString("id");
                this_record[1] = rs.getString("serial_number");
                this_record[2] = rs.getString("asset_tag");
                this_record[3] = rs.getString("manufacturer");
                this_record[4] = rs.getString("model");
                this_record[5] = rs.getString("asset_type");
                this_record[6] = rs.getString("asset_subtype");
                this_record[7] = rs.getString("purchase_order");
                this_record[8] = rs.getString("warranty_expire");
                this_record[9] = rs.getString("state");
                this_record[10] = rs.getString("state_date");
                this_record[11] = rs.getString("assigned_to_id");
                this_record[12] = rs.getString("assigned_group_id");
                this_record[13] = rs.getString("department");
                this_record[14] = rs.getString("location");
                this_record[15] = rs.getString("site");
                this_record[16] = rs.getString("company");
                this_record[17] = rs.getString("ip");
                this_record[18] = rs.getString("notes");                
                return_array_list.add(this_record);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_assets.all_limited:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_assets.all_limited:=" + exc);
	}
        return return_array_list;
    }
    public static int all_filtered_count(Connection con, List<Map<String, String>> filters) throws IOException, SQLException
    {
        int count = 0;
        List<String> wheres = new ArrayList<>();
        PreparedStatement stmt;
        String query;        
        String where = "";

        try 
        {
            // Processing filters
            for (int i = 0; i < filters.size(); i++)
            {
              String filter_key = filters.get(i).get("key");
              wheres.add(filter_key + " LIKE ? ");
            }
            
            if (wheres.size() > 0)
            {
                where = " WHERE " + String.join(" AND ", wheres);
            }
            // Create a Statement Object
            query = "SELECT count(*) as counter FROM assets " + where;

            stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);

            for (int i = 0; i < filters.size(); i++)
            {
              String filter_value = filters.get(i).get("value");
              stmt.setString(i+1, "%" + filter_value + "%");
            }
//                System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("counter");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_assets.all_filtered_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_assets.all_filtered_count:=" + exc);
	}
        return count;
    }
    public static ArrayList<String[]> all_limited_filtered(Connection con, int starting_record, int fetch_size, List<Map<String, String>> filters) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        List<String> wheres = new ArrayList<>();
        PreparedStatement stmt;
        String query;        
        String where = "";

        try 
        {
            // Processing filters
            for (int i = 0; i < filters.size(); i++)
            {
              String filter_key = filters.get(i).get("key");
              wheres.add(filter_key + " LIKE ? ");
            }
            
            if (wheres.size() > 0)
            {
                where = " WHERE " + String.join(" AND ", wheres);
            }

            // Create a Statement Object
            query = "SELECT * FROM assets " + where + " ORDER BY id LIMIT ? , ?";
            stmt = con.prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);

            for (int i = 0; i < filters.size(); i++)
            {
              String filter_value = filters.get(i).get("value");
              stmt.setString(i+1, "%" + filter_value + "%");
            }
            stmt.setInt(filters.size()+1, starting_record);
            stmt.setInt(filters.size()+2, fetch_size);

            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[19];
                this_record[0] = rs.getString("id");
                this_record[1] = rs.getString("serial_number");
                this_record[2] = rs.getString("asset_tag");
                this_record[3] = rs.getString("manufacturer");
                this_record[4] = rs.getString("model");
                this_record[5] = rs.getString("asset_type");
                this_record[6] = rs.getString("asset_subtype");
                this_record[7] = rs.getString("purchase_order");
                this_record[8] = rs.getString("warranty_expire");
                this_record[9] = rs.getString("state");
                this_record[10] = rs.getString("state_date");
                this_record[11] = rs.getString("assigned_to_id");
                this_record[12] = rs.getString("assigned_group_id");
                this_record[13] = rs.getString("department");
                this_record[14] = rs.getString("location");
                this_record[15] = rs.getString("site");
                this_record[16] = rs.getString("company");
                this_record[17] = rs.getString("ip");
                this_record[18] = rs.getString("notes");                
                return_array_list.add(this_record);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_assets.all_limited_filtered:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_assets.all_limited_filtered:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> all_types(Connection con)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT * FROM asset_type ORDER BY name");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = {"","",""};
                temp[0] = rs.getString("id");
                temp[1] = rs.getString("name");
                temp[2] = rs.getString("active");
                return_arraylist.add(temp);
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_assets.all_types:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> all_active_types(Connection con)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT * FROM asset_type WHERE active='true' ORDER BY name");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = {"","",""};
                temp[0] = rs.getString("id");
                temp[1] = rs.getString("name");
                temp[2] = rs.getString("active");
                return_arraylist.add(temp);
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_assets.all_active_types:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> all_subtypes_for_type_id(Connection con, String type_id)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT * FROM asset_subtype WHERE asset_type_id=?");
            stmt.setString(1, type_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = {"","","",""};
                temp[0] = rs.getString("id");
                temp[1] = rs.getString("asset_type_id");
                temp[2] = rs.getString("name");
                temp[3] = rs.getString("active");
                return_arraylist.add(temp);                
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_assets.all_subtypes_for_type_id:=" + exc);
	}
        return return_arraylist;
    }
    public static String[] info_by_type_id(Connection con, String type_id)
    {
        String[] return_string = {"","",""};
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT * FROM asset_type WHERE id=?");
            stmt.setString(1, type_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_string[0] = rs.getString("id");
                return_string[1] = rs.getString("name");
                return_string[2] = rs.getString("active");
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_assets.info_by_type_id:=" + exc);
	}
        return return_string;
    }
    public static String[] for_id(Connection con, String id) throws IOException, SQLException
    {
        String return_array[] = {"","","","","","","","","","","","","","","","","","",""};
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            String query = "SELECT id,"
            + "serial_number,"
            + "asset_tag,"
            + "manufacturer," 
            + "model," 
            + "asset_type,"
            + "asset_subtype," 
            + "purchase_order," 
            + "DATE_FORMAT(warranty_expire,'%Y%m%d%H%i%s') AS warranty_expire, "
            + "state," 
            + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date, "
            + "assigned_to_id,"
            + "assigned_group_id,"
            + "department,"
            + "location,"
            + "site,"
            + "company,"
            + "ip,"
            + "notes "
            + "FROM assets WHERE id=?";

            stmt = con.prepareStatement(query);
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_array[0] = rs.getString("id");
                return_array[1] = rs.getString("serial_number");
                return_array[2] = rs.getString("asset_tag");
                return_array[3] = rs.getString("manufacturer");
                return_array[4] = rs.getString("model");
                return_array[5] = rs.getString("asset_type");
                return_array[6] = rs.getString("asset_subtype");
                return_array[7] = rs.getString("purchase_order");
                return_array[8] = rs.getString("warranty_expire");
                return_array[9] = rs.getString("state");
                return_array[10] = rs.getString("state_date");
                return_array[11] = rs.getString("assigned_to_id");
                return_array[12] = rs.getString("assigned_group_id");
                return_array[13] = rs.getString("department");
                return_array[14] = rs.getString("location");
                return_array[15] = rs.getString("site");
                return_array[16] = rs.getString("company");
                return_array[17] = rs.getString("ip");
                return_array[18] = rs.getString("notes");   
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_assets.for_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_assets.for_id:=" + exc);
	}
        return return_array;
    }
}

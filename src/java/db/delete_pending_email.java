/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author rcampbell
 */
public class delete_pending_email 
{
    private static Logger logger = LogManager.getLogger();
    
    public static boolean now(Connection con, String id)
    {
        Boolean status = true;
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("DELETE FROM pending_email WHERE id=?");
            stmt.setString(1, id);           
            stmt.executeUpdate();
        }
        catch(Exception e)
        {
            logger.error("ERROR Exception delete_pending_email.now:=" + e);
            status = false;
        }
        return status;
    }
}

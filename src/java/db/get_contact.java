/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class get_contact 
{
    private static Logger logger = LogManager.getLogger();
    
    public static int contact_count_for_media_type_timeframe(Connection con,String media_type,java.util.Date start, java.util.Date end) throws IOException, SQLException
    {
        int return_int = 0;
        PreparedStatement stmt;
        try 
        {
            Timestamp start_timestamp = new Timestamp(start.getTime());
            Timestamp end_timestamp = new Timestamp(end.getTime());
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT count(*) AS value FROM calls WHERE (create_time >= ? and create_time <= ?) AND media_type = ?");
            stmt.setTimestamp(1, start_timestamp);
            stmt.setTimestamp(2, end_timestamp);
            stmt.setString(3, media_type);   
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                return_int = rs.getInt("value");
            }           
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_contact.contact_count_for_media_type_timeframe:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_contact.contact_count_for_media_type_timeframe:=" + exc);
	}
        return return_int;
    }
    public static String[] all_channels(Connection con) throws IOException, SQLException
    {
        String returnString[] = new String[0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT DISTINCT(channel) FROM calls", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count] = rs.getString("channel");
                count++;                
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_contact.all_channels:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_contact.all_channels:=" + exc);
	}
        return returnString;
    }
    public static int all_contacts_for_timeframe(Connection con, java.util.Date start, java.util.Date end) throws IOException, SQLException
    {
        int return_int = 0;
        PreparedStatement stmt;
        try 
        {
            Timestamp start_timestamp = new Timestamp(start.getTime());
            Timestamp end_timestamp = new Timestamp(end.getTime());
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT count(*) as value FROM calls WHERE create_time >= ? AND create_time <= ?");
            stmt.setTimestamp(1, start_timestamp);
            stmt.setTimestamp(2, end_timestamp);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");
                //System.out.println("value=" + return_int);
            }    
            //get walkins 
            int incident_walkins = db.get_incidents.incident_walkin_count_start_date_stop_date(con, start, end);
            int request_walkins = db.get_requests.requests_walkin_count_start_date_stop_date_(con, start, end);
            //add it all together
            return_int =  return_int + incident_walkins + request_walkins;  
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_contact.all_contacts_for_timeframe:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_contact.all_contacts_for_timeframe:=" + exc);
	}
        return return_int;
    }
    public static int self_service_contacts_for_timeframe(Connection con, java.util.Date start, java.util.Date end) throws IOException, SQLException
    {
        int return_int = 0;
        PreparedStatement stmt;
        try 
        {
            Timestamp start_timestamp = new Timestamp(start.getTime());
            Timestamp end_timestamp = new Timestamp(end.getTime());
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT count(*) as value FROM calls WHERE create_time >= ? AND create_time <= ? AND channel='Self Service' AND media_type='Email'");
            stmt.setTimestamp(1, start_timestamp);
            stmt.setTimestamp(2, end_timestamp);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");
                //System.out.println("value=" + return_int);
            }    
            //get walkins 
            int incident_walkins = db.get_incidents.incident_walkin_count_start_date_stop_date(con, start, end);
            int request_walkins = db.get_requests.requests_walkin_count_start_date_stop_date_(con, start, end);
            //add it all together
            return_int =  return_int + incident_walkins + request_walkins;  
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_contact.self_service_contacts_for_timeframe:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_contact.self_service_contacts_for_timeframe:=" + exc);
	}
        return return_int;
    }
    public static String[][] all_contacts_for_channel_media_type_timeframe(Connection con, String channel_name, String channel_type,java.util.Date start, java.util.Date end) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            Timestamp start_timestamp = new Timestamp(start.getTime());
            Timestamp end_timestamp = new Timestamp(end.getTime());
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM calls WHERE (create_time >= ? and create_time <= ?) AND media_type = ? AND channel = ? AND direction='0'", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start_timestamp);
            stmt.setTimestamp(2, end_timestamp);
            stmt.setString(3, channel_type);
            stmt.setString(4, channel_name);    
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }    
            return_string = new String[count][15];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("id");
                return_string[count][1] = rs.getString("media_type");
                return_string[count][2] = rs.getString("create_time");
                return_string[count][3] = rs.getString("accept_time");
                return_string[count][4] = rs.getString("accept_time_seconds");
                return_string[count][5] = rs.getString("abandon_time");
                return_string[count][6] = rs.getString("abandon_time_seconds");
                return_string[count][7] = rs.getString("call_duration_seconds");
                return_string[count][8] = rs.getString("channel");
                return_string[count][9] = rs.getString("direction");
                return_string[count][10] = rs.getString("agent_name");
                return_string[count][11] = rs.getString("post_process_time");
                return_string[count][12] = rs.getString("process_time");
                return_string[count][13] = rs.getString("total_time");
                return_string[count][14] = rs.getString("interaction_id");
                count++;
            }             
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_contact.all_contacts_for_channel_media_type_timeframe:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_contact.all_contacts_for_channel_media_type_timeframe:=" + exc);
	}
        return return_string;
    }
    public static String[][] all_contacts_for_channel_timeframe(Connection con, String channel_name,java.util.Date start, java.util.Date end) throws IOException, SQLException
    {
        SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
        String return_string[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            Timestamp start_timestamp = new Timestamp(start.getTime());
            Timestamp end_timestamp = new Timestamp(end.getTime());
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM calls WHERE (create_time >= ? and create_time <= ?) AND channel = ? AND direction='0' ORDER BY create_time", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start_timestamp);
            stmt.setTimestamp(2, end_timestamp);
            stmt.setString(3, channel_name);    
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }    
            return_string = new String[count][15];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("id");
                return_string[count][1] = rs.getString("media_type");
                return_string[count][2] = timestamp.format(rs.getTimestamp("create_time"));
                //System.out.println("create_time=" + return_string[count][2]);
                return_string[count][3] = rs.getString("accept_time");
                return_string[count][4] = rs.getString("accept_time_seconds");
                return_string[count][5] = rs.getString("abandon_time");
                return_string[count][6] = rs.getString("abandon_time_seconds");
                return_string[count][7] = rs.getString("call_duration_seconds");
                return_string[count][8] = rs.getString("channel");
                return_string[count][9] = rs.getString("direction");
                return_string[count][10] = rs.getString("agent_name");
                return_string[count][11] = rs.getString("post_process_time");
                return_string[count][12] = rs.getString("process_time");
                return_string[count][13] = rs.getString("total_time");
                return_string[count][14] = rs.getString("interaction_id");
                count++;
            }             
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_contact.all_contacts_for_channel_timeframe:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_contact.all_contacts_for_channel_timeframe:=" + exc);
	}
        return return_string;
    }
    public static String[][] contact_home_phone_contacts_table_for_timeframe (Connection con, java.util.Date start, java.util.Date end) throws IOException, SQLException
    {
        /*Channel
        Volume
        Avg. ASA (Sec)
        Avg. Handle Time(Sec)
        Avg. Duration(Sec)
        Wrap Up Time(Sec)
        Abandoned Rate*/
        String return_string[][] = new String[0][0];
        PreparedStatement stmt;
        int accept_time;
        int total_time;
        int call_duration_seconds;
        int abandoned_calls;
        int wrap_up_time;
        
        //get abandoned threshold   //abandoned_time_phone
        int abandoned_time_phone = 60;
        try
        {
            abandoned_time_phone = Integer.parseInt(db.get_configuration_properties.by_name(con, "abandoned_time_phone"));
        }
        catch(Exception e)
        {
            abandoned_time_phone = 60;
        }
        try
        {
            Timestamp start_timestamp = new Timestamp(start.getTime());
            Timestamp end_timestamp = new Timestamp(end.getTime());
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT distinct(channel) FROM calls WHERE (create_time >= ? and create_time <= ?) AND media_type='Phone' AND direction='0'", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start_timestamp);
            stmt.setTimestamp(2, end_timestamp);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }    
            return_string = new String[count][7];
            count = 0;
            rs.beforeFirst();
            //Populate Channel
            while(rs.next())
            {
                return_string[count][0] = rs.getString("channel"); //channel
                return_string[count][1] = "0";  //volume
                return_string[count][2] = "0"; //Avg. ASA (Sec)
                return_string[count][3] = "0"; //Avg. Handle Time(Sec)
                return_string[count][4] = "0"; //Avg. Duration(Sec)
                return_string[count][5] = "0"; //Wrap Up Time(Sec)
                return_string[count][6] = "0"; //Abandoned Rate
                count++;
            }
            //Go thru each channel and get metrics
            for(int b = 0; b < return_string.length; b++)
            {
                String calls[][] = all_contacts_for_channel_media_type_timeframe(con, return_string[b][0], "Phone", start, end);
                
                //Populate Volume
                return_string[b][1] = String.valueOf(calls.length);
                
                //All answered calls 
                int total_answered_calls = 0;
                int total_abandoned_calls = 0;
                //Avg. ASA (Sec)
                int total_asa = 0;
                //Avg. Handle Time(Sec)  //total_time
                int aht_total = 0;
                //Avg. Duration(Sec) //call_duration_seconds
                int total_duration = 0;
                //Avg. Wrap Up time
                int total_wrap_up_time = 0;
                
                
                for(int c = 0; c < calls.length;c++) //main metric loop
                {
                    if(calls[c][4] != null)
                    {
                        accept_time = Integer.parseInt(calls[c][4]);
                        total_asa = total_asa + accept_time;
                        
                        total_time = Integer.parseInt(calls[c][13]);
                        aht_total = aht_total + total_time;
                        
                        call_duration_seconds = Integer.parseInt(calls[c][7]);
                        total_duration = total_duration + call_duration_seconds;
                        
                        wrap_up_time = Integer.parseInt(calls[c][11]);
                        total_wrap_up_time = total_wrap_up_time + wrap_up_time;
                        
                        total_answered_calls++;
                    }
                    else
                    {
                        try
                        {
                            int abandon_time = Integer.parseInt(calls[c][6]);
                            if(abandon_time >  abandoned_time_phone)
                            {
                                total_abandoned_calls++;
                            }
                        }
                        catch(Exception e){}
                    }
                }
                return_string[b][2] = String.valueOf(total_asa / total_answered_calls);
                return_string[b][3] = String.valueOf(aht_total / total_answered_calls);
                return_string[b][4] = String.valueOf(total_duration / total_answered_calls);
                return_string[b][5] = String.valueOf(total_wrap_up_time / total_answered_calls);                
                return_string[b][6] = String.valueOf(String.format("%1$,.2f", ((double)total_abandoned_calls / (double)calls.length) * 100)); 
                
            }
            stmt.close();
        }
        catch(Exception ex) 
        {
            logger.error("ERROR Exception get_contact.contact_home_phone_contacts_table_for_timeframe:=" + ex);
        }
        return return_string;    
    }
    public static String[][] contact_home_voice_mail_contacts_table_for_timeframe (Connection con, java.util.Date start, java.util.Date end) throws IOException, SQLException
    {
        /*Channel
        Volume
        Avg. ASA (Sec)
        Avg. Handle Time(Sec)
        Avg. Duration(Sec)
        Wrap Up Time(Sec)
        Abandoned Rate*/
        String return_string[][] = new String[0][0];
        PreparedStatement stmt;
        int accept_time;
        int total_time;
        int call_duration_seconds;
        int abandoned_calls;
        int wrap_up_time;
        
        //get abandoned threshold   //abandoned_time_phone
        int abandoned_time_phone = 60;
        try
        {
            abandoned_time_phone = Integer.parseInt(db.get_configuration_properties.by_name(con, "abandoned_time_phone"));
        }
        catch(Exception e)
        {
            abandoned_time_phone = 60;
        }
        try
        {
            Timestamp start_timestamp = new Timestamp(start.getTime());
            Timestamp end_timestamp = new Timestamp(end.getTime());
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT distinct(channel) FROM calls WHERE (create_time >= ? and create_time <= ?) AND media_type='Voice Mail' AND direction='0'", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start_timestamp);
            stmt.setTimestamp(2, end_timestamp);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }    
            return_string = new String[count][7];
            count = 0;
            rs.beforeFirst();
            //Populate Channel
            while(rs.next())
            {
                return_string[count][0] = rs.getString("channel"); //channel
                return_string[count][1] = "0";  //volume
                return_string[count][2] = "0"; //Avg. ASA (Sec)
                return_string[count][3] = "0"; //Avg. Handle Time(Sec)
                return_string[count][4] = "0"; //Avg. Duration(Sec)
                return_string[count][5] = "0"; //Wrap Up Time(Sec)
                return_string[count][6] = "0"; //Abandoned Rate
                count++;
            }
            //Go thru each channel and get metrics
            for(int b = 0; b < return_string.length; b++)
            {
                String calls[][] = all_contacts_for_channel_media_type_timeframe(con, return_string[b][0], "Voice Mail", start, end);
                
                //Populate Volume
                return_string[b][1] = String.valueOf(calls.length);
                
                //All answered calls 
                int total_answered_calls = 0;
                int total_abandoned_calls = 0;
                //Avg. ASA (Sec)
                int total_asa = 0;
                //Avg. Handle Time(Sec)  //total_time
                int aht_total = 0;
                //Avg. Duration(Sec) //call_duration_seconds
                int total_duration = 0;
                //Avg. Wrap Up time
                int total_wrap_up_time = 0;
                
                
                for(int c = 0; c < calls.length;c++) //main metric loop
                {
                    if(calls[c][4] != null)
                    {
                        accept_time = Integer.parseInt(calls[c][4]);
                        total_asa = total_asa + accept_time;
                        
                        total_time = Integer.parseInt(calls[c][13]);
                        aht_total = aht_total + total_time;
                        
                        call_duration_seconds = Integer.parseInt(calls[c][7]);
                        total_duration = total_duration + call_duration_seconds;
                        
                        wrap_up_time = Integer.parseInt(calls[c][11]);
                        total_wrap_up_time = total_wrap_up_time + wrap_up_time;
                        
                        total_answered_calls++;
                    }
                    else
                    {
                        try
                        {
                            int abandon_time = Integer.parseInt(calls[c][6]);
                            if(abandon_time >  abandoned_time_phone)
                            {
                                total_abandoned_calls++;
                            }
                        }
                        catch(Exception e){}
                    }
                }
                return_string[b][2] = String.valueOf(total_asa / total_answered_calls);
                return_string[b][3] = String.valueOf(aht_total / total_answered_calls);
                return_string[b][4] = String.valueOf(total_duration / total_answered_calls);
                return_string[b][5] = String.valueOf(total_wrap_up_time / total_answered_calls);                
                return_string[b][6] = String.valueOf(String.format("%1$,.2f", ((double)total_abandoned_calls / (double)calls.length) * 100)); 
                
            }
            stmt.close();
        }
        catch(Exception ex) 
        {
            logger.error("ERROR Exception get_contact.contact_home_voice_mail_contacts_table_for_timeframe:=" + ex);
        }
        return return_string;   
    }
    public static String[][] contact_home_chat_contacts_table_for_timeframe (Connection con, java.util.Date start, java.util.Date end) throws IOException, SQLException
    {
        /*Channel
        Volume
        Avg. ASA (Sec)
        Avg. Handle Time(Sec)
        Avg. Duration(Sec)
        Wrap Up Time(Sec)
        Abandoned Rate*/
        String return_string[][] = new String[0][0];
        PreparedStatement stmt;
        int accept_time;
        int total_time;
        int call_duration_seconds;
        int abandoned_chats;
        int wrap_up_time;
        
        //get abandoned threshold   //abandoned_time_phone
        int abandoned_time_chat = 60;
        try
        {
            abandoned_time_chat = Integer.parseInt(db.get_configuration_properties.by_name(con, "abandoned_time_chat"));
        }
        catch(Exception e)
        {
            abandoned_time_chat = 60;
        }
        try
        {
            Timestamp start_timestamp = new Timestamp(start.getTime());
            Timestamp end_timestamp = new Timestamp(end.getTime());
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT distinct(channel) FROM calls WHERE (create_time >= ? and create_time <= ?) AND media_type='Chat' AND direction='0'", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start_timestamp);
            stmt.setTimestamp(2, end_timestamp);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }    
            return_string = new String[count][7];
            count = 0;
            rs.beforeFirst();
            //Populate Channel
            while(rs.next())
            {
                return_string[count][0] = rs.getString("channel"); //channel
                return_string[count][1] = "0";  //volume
                return_string[count][2] = "0"; //Avg. ASA (Sec)
                return_string[count][3] = "0"; //Avg. Handle Time(Sec)
                return_string[count][4] = "0"; //Avg. Duration(Sec)
                return_string[count][5] = "0"; //Wrap Up Time(Sec)
                return_string[count][6] = "0"; //Abandoned Rate
                count++;
            }
            //Go thru each channel and get metrics
            for(int b = 0; b < return_string.length; b++)
            {
                String calls[][] = all_contacts_for_channel_media_type_timeframe(con, return_string[b][0], "Chat", start, end);
                
                //Populate Volume
                return_string[b][1] = String.valueOf(calls.length);
                
                //All answered calls 
                int total_answered_chats = 0;
                int total_abandoned_chats = 0;
                //Avg. ASA (Sec)
                int total_asa = 0;
                //Avg. Handle Time(Sec)  //total_time
                int aht_total = 0;
                //Avg. Duration(Sec) //call_duration_seconds
                int total_duration = 0;
                //Avg. Wrap Up time
                int total_wrap_up_time = 0;
                
                
                for(int c = 0; c < calls.length;c++) //main metric loop
                {
                    if(calls[c][4] != null)
                    {
                        accept_time = Integer.parseInt(calls[c][4]);
                        total_asa = total_asa + accept_time;
                        
                        total_time = Integer.parseInt(calls[c][13]);
                        aht_total = aht_total + total_time;
                        
                        call_duration_seconds = Integer.parseInt(calls[c][7]);
                        total_duration = total_duration + call_duration_seconds;
                        
                        wrap_up_time = Integer.parseInt(calls[c][11]);
                        total_wrap_up_time = total_wrap_up_time + wrap_up_time;
                        
                        total_answered_chats++;
                    }
                    else
                    {
                        try
                        {
                            int abandon_time = Integer.parseInt(calls[c][6]);
                            if(abandon_time >  abandoned_time_chat)
                            {
                                total_abandoned_chats++;
                            }
                        }
                        catch(Exception e){}
                    }
                }
                return_string[b][2] = String.valueOf(total_asa / total_answered_chats);
                return_string[b][3] = String.valueOf(aht_total / total_answered_chats);
                return_string[b][4] = String.valueOf(total_duration / total_answered_chats);
                return_string[b][5] = String.valueOf(total_wrap_up_time / total_answered_chats);                
                return_string[b][6] = String.valueOf(String.format("%1$,.2f", ((double)total_abandoned_chats / (double)calls.length) * 100)); 
                
            }
            stmt.close();
        }
        catch(Exception ex) 
        {
            logger.error("ERROR Exception get_contact.contact_home_chat_contacts_table_for_timeframe:=" + ex);
        }
        return return_string;     
    }
    public static String[][] contact_home_email_contacts_table_for_timeframe (Connection con, java.util.Date start, java.util.Date end) throws IOException, SQLException
    {
        /*Channel
        Volume
        Avg. ASA (Sec)
        Avg. Handle Time(Sec)
        Avg. Duration(Sec)
        Wrap Up Time(Sec)
        Abandoned Rate*/
        String return_string[][] = new String[0][0];
        PreparedStatement stmt;
        int accept_time;
        int total_time;
        int call_duration_seconds;
        int abandoned_calls;
        int wrap_up_time;
        
        //get abandoned threshold   //abandoned_time_phone
        int abandoned_time_phone = 60;
        try
        {
            abandoned_time_phone = Integer.parseInt(db.get_configuration_properties.by_name(con, "abandoned_time_phone"));
        }
        catch(Exception e)
        {
            abandoned_time_phone = 60;
        }
        try
        {
            Timestamp start_timestamp = new Timestamp(start.getTime());
            Timestamp end_timestamp = new Timestamp(end.getTime());
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT distinct(channel) FROM calls WHERE (create_time >= ? and create_time <= ?) AND media_type='Email' AND direction='0'", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start_timestamp);
            stmt.setTimestamp(2, end_timestamp);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }    
            return_string = new String[count][7];
            count = 0;
            rs.beforeFirst();
            //Populate Channel
            while(rs.next())
            {
                return_string[count][0] = rs.getString("channel"); //channel
                return_string[count][1] = "0";  //volume
                return_string[count][2] = "0"; //Avg. ASA (Sec)
                return_string[count][3] = "0"; //Avg. Handle Time(Sec)
                return_string[count][4] = "0"; //Avg. Duration(Sec)
                return_string[count][5] = "0"; //Wrap Up Time(Sec)
                return_string[count][6] = "0"; //Abandoned Rate
                count++;
            }
            //Go thru each channel and get metrics
            for(int b = 0; b < return_string.length; b++)
            {
                String calls[][] = all_contacts_for_channel_media_type_timeframe(con, return_string[b][0], "Email", start, end);
                
                //Populate Volume
                return_string[b][1] = String.valueOf(calls.length);
                
                //All answered calls 
                int total_answered_calls = 0;
                int total_abandoned_calls = 0;
                //Avg. ASA (Sec)
                int total_asa = 0;
                //Avg. Handle Time(Sec)  //total_time
                int aht_total = 0;
                //Avg. Duration(Sec) //call_duration_seconds
                int total_duration = 0;
                //Avg. Wrap Up time
                int total_wrap_up_time = 0;
                
                
                for(int c = 0; c < calls.length;c++) //main metric loop
                {
                    if(calls[c][4] != null)
                    {
                        accept_time = Integer.parseInt(calls[c][4]);
                        total_asa = total_asa + accept_time;
                        
                        total_time = Integer.parseInt(calls[c][13]);
                        aht_total = aht_total + total_time;
                        
                        call_duration_seconds = Integer.parseInt(calls[c][7]);
                        total_duration = total_duration + call_duration_seconds;
                        
                        wrap_up_time = Integer.parseInt(calls[c][11]);
                        total_wrap_up_time = total_wrap_up_time + wrap_up_time;
                        
                        total_answered_calls++;
                    }
                    else
                    {
                        try
                        {
                            int abandon_time = Integer.parseInt(calls[c][6]);
                            if(abandon_time >  abandoned_time_phone)
                            {
                                total_abandoned_calls++;
                            }
                        }
                        catch(Exception e){}
                    }
                }
                return_string[b][2] = String.valueOf(total_asa / total_answered_calls);
                return_string[b][3] = String.valueOf(aht_total / total_answered_calls);
                return_string[b][4] = String.valueOf(total_duration / total_answered_calls);
                return_string[b][5] = String.valueOf(total_wrap_up_time / total_answered_calls);                
                return_string[b][6] = String.valueOf(String.format("%1$,.2f", ((double)total_abandoned_calls / (double)calls.length) * 100)); 
                
            }
            stmt.close();
        }
        catch(Exception ex) 
        {
            logger.error("ERROR Exception get_contact.contact_home_email_contacts_table_for_timeframe:=" + ex);
        }
        return return_string;    
    }
    public static String[][] all_contacts_for_media_type_channel_timeframe(Connection con, String media_type,String channel, java.util.Date start, java.util.Date end) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            Timestamp start_timestamp = new Timestamp(start.getTime());
            Timestamp end_timestamp = new Timestamp(end.getTime());
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM calls WHERE (create_time >= ? and create_time <= ?) AND media_type = ? AND channel = ? AND direction='0'", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start_timestamp);
            stmt.setTimestamp(2, end_timestamp);
            stmt.setString(3, media_type);   
            stmt.setString(4, channel);   
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }    
            return_string = new String[count][15];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("id");
                return_string[count][1] = rs.getString("media_type");
                return_string[count][2] = rs.getString("create_time");
                return_string[count][3] = rs.getString("accept_time");
                return_string[count][4] = rs.getString("accept_time_seconds");
                return_string[count][5] = rs.getString("abandon_time");
                return_string[count][6] = rs.getString("abandon_time_seconds");
                return_string[count][7] = rs.getString("call_duration_seconds");
                return_string[count][8] = rs.getString("channel");
                return_string[count][9] = rs.getString("direction");
                return_string[count][10] = rs.getString("agent_name");
                return_string[count][11] = rs.getString("post_process_time");
                return_string[count][12] = rs.getString("process_time");
                return_string[count][13] = rs.getString("total_time");
                return_string[count][14] = rs.getString("interaction_id");
                count++;
            }             
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_contact.all_contacts_for_media_type_channel_timeframe:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_contact.all_contacts_for_media_type_channel_timeframe:=" + exc);
	}
        return return_string;
    }
    
}

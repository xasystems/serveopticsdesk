/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author ralph
 */
public class get_call 
{
    public static String[] for_id(Connection con, String id) throws IOException, SQLException
    {
        String return_array[] = {"","","","","","","","","","","","","","",""};
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            String query = "SELECT `id`," 
                    + "`media_type`,"
                    + "DATE_FORMAT(create_time,'%Y%m%d%H%i%s') AS create_time,"
                    + "DATE_FORMAT(accept_time,'%Y%m%d%H%i%s') AS accept_time,"
                    + "`accept_time_seconds`," 
                    + "DATE_FORMAT(abandon_time,'%Y%m%d%H%i%s') AS abandon_time,"
                    + "`abandon_time_seconds`," 
                    + "`call_duration_seconds`," 
                    + "`channel`," 
                    + "`direction`,"
                    + "`agent_name`," 
                    + "`post_process_time`," 
                    + "`process_time`," 
                    + "`total_time`," 
                    + "`interaction_id`" 
                    + " FROM `calls` WHERE id=?";

            stmt = con.prepareStatement(query);
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_array[0] = rs.getString("id");
                return_array[1] = rs.getString("media_type");
                return_array[2] = rs.getString("create_time");
                return_array[3] = rs.getString("accept_time");
                return_array[4] = rs.getString("accept_time_seconds");
                return_array[5] = rs.getString("abandon_time");
                return_array[6] = rs.getString("abandon_time_seconds");
                return_array[7] = rs.getString("call_duration_seconds");
                return_array[8] = rs.getString("channel");
                return_array[9] = rs.getString("direction");
                return_array[10] = rs.getString("agent_name");
                return_array[11] = rs.getString("post_process_time");
                return_array[12] = rs.getString("process_time");
                return_array[13] = rs.getString("total_time");
                return_array[14] = rs.getString("interaction_id");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            System.out.println("ERROR SQLException get_call.for_id:=" + ex);
        }
        catch(Exception exc) 
        {
            System.out.println("ERROR Exception get_call.for_id:=" + exc);
	}
        return return_array;
    }
    public static String[] for_interaction_id(Connection con, String interaction_id) throws IOException, SQLException
    {
        String return_array[] = {"","","","","","","","","","","","","","",""};
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            String query = "SELECT `id`," 
                    + "`media_type`,"
                    + "DATE_FORMAT(create_time,'%Y%m%d%H%i%s') AS create_time,"
                    + "DATE_FORMAT(accept_time,'%Y%m%d%H%i%s') AS accept_time,"
                    + "`accept_time_seconds`," 
                    + "DATE_FORMAT(abandon_time,'%Y%m%d%H%i%s') AS abandon_time,"
                    + "`abandon_time_seconds`," 
                    + "`call_duration_seconds`," 
                    + "`channel`," 
                    + "`direction`,"
                    + "`agent_name`," 
                    + "`post_process_time`," 
                    + "`process_time`," 
                    + "`total_time`," 
                    + "`interaction_id`" 
                    + " FROM `calls` WHERE interaction_id=?";

            stmt = con.prepareStatement(query);
            stmt.setString(1, interaction_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_array[0] = rs.getString("id");
                return_array[1] = rs.getString("media_type");
                return_array[2] = rs.getString("create_time");
                return_array[3] = rs.getString("accept_time");
                return_array[4] = rs.getString("accept_time_seconds");
                return_array[5] = rs.getString("abandon_time");
                return_array[6] = rs.getString("abandon_time_seconds");
                return_array[7] = rs.getString("call_duration_seconds");
                return_array[8] = rs.getString("channel");
                return_array[9] = rs.getString("direction");
                return_array[10] = rs.getString("agent_name");
                return_array[11] = rs.getString("post_process_time");
                return_array[12] = rs.getString("process_time");
                return_array[13] = rs.getString("total_time");
                return_array[14] = rs.getString("interaction_id");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            System.out.println("ERROR SQLException get_call.for_interaction_id:=" + ex);
        }
        catch(Exception exc) 
        {
            System.out.println("ERROR Exception get_call.for_interaction_id:=" + exc);
	}
        return return_array;
    }
}

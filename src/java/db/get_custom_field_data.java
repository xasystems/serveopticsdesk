/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class get_custom_field_data 
{
    private static Logger logger = LogManager.getLogger();
    
    public static ArrayList<String[]> active_by_form(Connection con, String form, String object_id) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT custom_form_fields_data.form, custom_form_fields_data.object_id, custom_form_fields_data.field_name, custom_form_fields_data.field_value FROM custom_form_fields_data INNER JOIN custom_form_fields ON custom_form_fields_data.form = custom_form_fields.form AND custom_form_fields_data.field_name = custom_form_fields.field_name AND custom_form_fields_data.form=? AND custom_form_fields_data.object_id=? AND custom_form_fields.active='true'");
            stmt.setString(1, form);
            stmt.setString(2, object_id);
            //System.out.println("q=" + stmt.toString());
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = {"","","",""};
                temp[0] = support.util.check_for_null(rs.getString("custom_form_fields_data.form"));
                temp[1] = support.util.check_for_null(rs.getString("custom_form_fields_data.object_id"));
                temp[2] = support.util.check_for_null(rs.getString("custom_form_fields_data.field_name"));
                temp[3] = support.util.check_for_null(rs.getString("custom_form_fields_data.field_value"));
                return_arraylist.add(temp);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_custom_field_data.active_by_form:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_custom_field_data.active_by_form:=" + exc);
	}
        return return_arraylist;
    }
}

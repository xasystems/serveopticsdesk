/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class get_so_instance 
{
    private static Logger logger = LogManager.getLogger();    
    public static HashMap active_instances(String context_dir, LinkedHashMap sys_props)
    {
        String so_instance_id = sys_props.get("so_instance_id").toString();
        //System.out.println("this so_instance=" + so_instance_id );
        HashMap actice_instances_map = new HashMap();         
        try
        {
            String requested_fields[] = {"id"};            
            ArrayList<String[]> results = db.get_management_server.run_query(context_dir, "SELECT * FROM contracts WHERE so_instance_id='" + so_instance_id + "' AND active='true'", "GET", requested_fields);
            for(int a = 0; a < results.size(); a++)
            {
                String result[] = results.get(a);
                for(int b = 0; b < result.length; b++)
                {
                    actice_instances_map.put("contract_id_" + result[b],result[b]);
                }
            }
        }
        catch(Exception e)
        {
            logger.error("ERROR SQLException get_so_instance.active_instances:=" + e);
        }
        return actice_instances_map;
    }
}

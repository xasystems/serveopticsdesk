/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class get_custom_fields 
{
    private static Logger logger = LogManager.getLogger();
    
    public static ArrayList<String[]> all(Connection con)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT * FROM custom_form_fields ORDER BY form");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = {"","","","","","","","","","",""};
                temp[0] = rs.getString("id");
                temp[1] = rs.getString("form");
                temp[2] = rs.getString("index");
                temp[3] = rs.getString("field_name");
                temp[4] = rs.getString("field_label");
                temp[5] = rs.getString("default_text");
                temp[6] = rs.getString("number_cols");
                temp[7] = rs.getString("field_type");
                temp[8] = rs.getString("field_values");
                temp[9] = rs.getString("required");
                temp[10] = rs.getString("active");
                return_arraylist.add(temp);
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_custom_fileds.all:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> active_for_form(Connection con, String form)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT * FROM custom_form_fields WHERE form=? AND active='true' ORDER BY `index`");
            stmt.setString(1,form);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = {"","","","","","","","","","",""};
                temp[0] = rs.getString("id");
                temp[1] = rs.getString("form");
                temp[2] = rs.getString("index");
                temp[3] = rs.getString("field_name");
                temp[4] = rs.getString("field_label");
                temp[5] = rs.getString("default_text");
                temp[6] = rs.getString("number_cols");
                temp[7] = rs.getString("field_type");
                temp[8] = rs.getString("field_values");
                temp[9] = rs.getString("required");
                temp[10] = rs.getString("active");
                return_arraylist.add(temp);
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_custom_fileds.active_for_form:=" + exc);
	}
        return return_arraylist;
    }
    public static String[] by_id(Connection con, String id)
    {
        String[] return_array = {"","","","","","","","","","",""};
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT * FROM custom_form_fields WHERE id=?");
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_array[0] = rs.getString("id");
                return_array[1] = rs.getString("form");
                return_array[2] = rs.getString("index");
                return_array[3] = rs.getString("field_name");
                return_array[4] = rs.getString("field_label");
                return_array[5] = rs.getString("default_text");
                return_array[6] = rs.getString("number_cols");
                return_array[7] = rs.getString("field_type");
                return_array[8] = rs.getString("field_values");
                return_array[9] = rs.getString("required");
                return_array[10] = rs.getString("active");
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_custom_fileds.by_id:=" + exc);
	}
        return return_array;
    }
    
}

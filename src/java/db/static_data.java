//Copyright 2021 XaSystems, Inc. , All rights reserved.
package db;

import java.sql.*;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import support.encrypt_utils;


public class static_data extends Thread 
{
    private static Logger logger = LogManager.getLogger();
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static Map<String, String> get_color_class_map()
    {
        Map<String, String> color_class_map = new HashMap<>();
        color_class_map.put("Published", "success");
        color_class_map.put("Draft", "primary-new");
        color_class_map.put("Invalid", "danger");
        color_class_map.put("Awaiting Approval", "warning");

        return color_class_map;
    }
    
    public static String get_css_class_by_state(String state)
    {
        Map<String, String> color_class_map = static_data.get_color_class_map();
        String css_color_class = color_class_map.get(state);
        if (css_color_class != null)
        {
            return css_color_class;
        } else {
            return "";
        }
    }
    
}
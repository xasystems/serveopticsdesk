/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class get_jobs 
{
    private static Logger logger = LogManager.getLogger();
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
    private static SimpleDateFormat form_format = new SimpleDateFormat("HH:mm / dd-MMM-YY"); //10:42 / 24-Jan-20
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   

    public static int all_filtered_count(Connection con, Map<String, String> filters, String add_query) throws IOException, SQLException
    {
        int count = 0;
        PreparedStatement stmt;
        String query_filter = "";

        try 
        {

            for (Entry<String, String> entry : filters.entrySet()) {
                query_filter = query_filter + (query_filter.equals("") ? "" : " AND ") + " jobs." + entry.getKey() + " = ? ";          
            }
            if (!add_query.equals("")){
                query_filter = query_filter + (query_filter.equals("") ? "" : " AND ") + add_query;
            }
            String query = "SELECT count(*) as counter FROM jobs " + ( query_filter.equals("") ? "" : " WHERE " + query_filter);

            stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);

            int parameters_count = 0;
            for (Entry<String, String> entry : filters.entrySet()) {
                parameters_count++;
                stmt.setString(parameters_count, entry.getValue());
            }

//                System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("counter");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.all_filtered__by_query_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.all_filtered_by_query_count:=" + exc);
	}
        return count;
    }

    public static ArrayList<String[]> all_limited_filtered(Connection con, int starting_record, int fetch_size, Map<String, String> filters, String order_column, String order_dir, String add_query) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        PreparedStatement stmt;
        String query_filter = "";

        try 
        {

            for (Entry<String, String> entry : filters.entrySet()) {
                query_filter = query_filter + (query_filter.equals("") ? "" : " AND ") + " jobs." + entry.getKey() + " = ? ";          
            }
            if (!add_query.equals("")){
                query_filter = query_filter + (query_filter.equals("") ? "" : " AND ") + add_query;
            }

            // Create a Statement Object
        String query = "SELECT "
                + "jobs.id," 
                + "jobs.name,"
                + "jobs.description,"
                + "jobs.assigned_group_id,"
                + "jobs.assigned_to_id,"
                + "jobs.priority,"
                + "jobs.status,"
                + "DATE_FORMAT(jobs.scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(jobs.actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(jobs.scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(jobs.actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "jobs.estimated_duration,"
                + "jobs.actual_duration,"
                + "jobs.notes, " 
                + "assigned_to.username assigned_to_username,assigned_to.first assigned_to_first,assigned_to.mi assigned_to_mi,assigned_to.last assigned_to_last,assigned_to.phone_mobile assigned_to_phone_mobile,assigned_to.avatar assigned_to_avatar, "
                + "assigned_groups.name assigned_group_name "
                + "FROM jobs "
                + "LEFT JOIN users as assigned_to ON jobs.assigned_to_id = assigned_to.id " 
                + "LEFT JOIN `groups` assigned_groups ON jobs.assigned_group_id = assigned_groups.id " 
                + ( query_filter.equals("") ? "" : " WHERE " + query_filter);

            if (!order_column.equals(""))
            {
                query += " ORDER BY " + order_column + " " + order_dir;
            } else {
                query += " ORDER BY id DESC";
            }

            query += " LIMIT ? , ?";

            stmt = con.prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            
            int parameters_count = 0;
            for (Entry<String, String> entry : filters.entrySet()) {
                parameters_count++;
                stmt.setString(parameters_count, entry.getValue());
            }

            stmt.setInt(parameters_count + 1, starting_record);
            stmt.setInt(parameters_count + 2, fetch_size);
//            System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();
            return_array_list = process_resultset(rs);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.all_limited_filtered:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.all_limited_filtered:=" + exc);
	}
        return return_array_list;
    }

    public static ArrayList<String[]> process_resultset(ResultSet rs)
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        try
        {
            while(rs.next())
            {
                String job_task[] = new String[23];
                job_task[0] = rs.getString("id");
                job_task[1] = rs.getString("name");
                job_task[2] = rs.getString("description");
                job_task[3] = rs.getString("assigned_group_id");
                job_task[4] = rs.getString("assigned_to_id");
                job_task[5] = rs.getString("priority");
                job_task[6] = rs.getString("status");
                job_task[7] = rs.getString("scheduled_start_date");
                job_task[8] = rs.getString("actual_start_date");
                job_task[9] = rs.getString("scheduled_end_date");
                job_task[10] = rs.getString("actual_end_date");
                job_task[11] = rs.getString("estimated_duration");
                job_task[12] = rs.getString("actual_duration");
                job_task[13] = rs.getString("notes");
                //LOOKUP populate the assigned_to_id info
                job_task[14] = rs.getString("assigned_to_username");
                job_task[15] = rs.getString("assigned_to_first");
                job_task[16] = rs.getString("assigned_to_mi");
                job_task[17] = rs.getString("assigned_to_last");
                job_task[18] = rs.getString("assigned_to_phone_mobile");
                //LOOKUP assigned_group info
                job_task[19] = rs.getString("assigned_group_name");

                if (job_task[8] != null && job_task[10] != null && !job_task[8].equals("") && !job_task[10].equals(""))
                {
                    final DateTimeFormatter formatter_timestamp = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");                    
                    final DateTimeFormatter formatter_month_day = DateTimeFormatter.ofPattern("MMM dd");                    

                    try {

                        final LocalDateTime date_start = LocalDateTime.parse(job_task[8], formatter_timestamp);
                        final LocalDateTime date_end = LocalDateTime.parse(job_task[10], formatter_timestamp);
                        job_task[20] = formatter_month_day.format(date_start) + " - " + formatter_month_day.format(date_end);
                        LocalDateTime now = LocalDateTime.now();
                        long seconds_total = ChronoUnit.SECONDS.between(date_start, date_end);
                        long seconds_since_start = ChronoUnit.SECONDS.between(date_start, now);
                        int percentage = (int)(seconds_since_start * 100.0 / seconds_total + 0.5);
                        job_task[21] = seconds_total + " " + date_start.toString() + " " + date_end.toString() + " " + seconds_since_start + " " + String.valueOf(percentage);

                    } catch(Exception e) {
                        System.out.println("Exception timeline construct in get_jobs.process_resultset=" + e);
//                        e.printStackTrace();
                        job_task[20] = "";
                        job_task[21] = "";
                    }

                }
                job_task[22] = rs.getString("assigned_to_avatar");
    
                return_array_list.add(job_task);
            }
        }
        catch(Exception e)
        {
            System.out.println("Exception in get_jobs.process_job_task_resultset=" + e);
        }
        return return_array_list;

    }

    public static ArrayList <String[]> job_tasks_for_job(Connection con, String job_id, String[] status_filters) throws IOException, SQLException
    {
        ArrayList<String[]> return_array_list = new ArrayList();        
        String status_filter = "";
        if (status_filters != null && status_filters.length>0)
        {
            for (int i = 0; i < status_filters.length; i++)
            {
                status_filter = status_filter + (status_filter.equals("") ? "" : " OR ") + " tasks.status = ? ";
            }
        }
        String ps_query_string = "SELECT "
                + "tasks.id," 
                + "tasks.job_id,"
                + "tasks.name,"
                + "tasks.description,"
                + "tasks.assigned_group_id,"
                + "tasks.assigned_to_id,"
                + "tasks.priority,"
                + "tasks.status,"
                + "DATE_FORMAT(tasks.scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(tasks.actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(tasks.scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(tasks.actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "tasks.estimated_duration,"
                + "tasks.actual_duration,"
                + "tasks.notes, " 
                + "jobs.name job_name,jobs.description job_description, "
                + "assigned_to.username assigned_to_username,assigned_to.first assigned_to_first,assigned_to.mi assigned_to_mi,assigned_to.last assigned_to_last,assigned_to.phone_mobile assigned_to_phone_mobile, "
                + "assigned_groups.name assigned_group_name "
                + "FROM job_tasks as tasks "
                + "LEFT JOIN users as assigned_to ON tasks.assigned_to_id = assigned_to.id " 
                + "LEFT JOIN `groups` assigned_groups ON tasks.assigned_group_id = assigned_groups.id " 
                + "LEFT JOIN `jobs` ON tasks.job_id = jobs.id "
                + "WHERE job_id=? " + (status_filter.equals("") ? "" : " AND (" + status_filter + ")"); 
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, job_id);
            if (status_filters != null)
            {
                for (int i = 0; i < status_filters.length; i++)
                {
                    stmt.setString(i+2, status_filters[i]);
                }
            }

            ResultSet rs = stmt.executeQuery();
            return_array_list = process_job_task_resultset(rs);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.job_tasks_for_job:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.job_tasks_for_job:=" + exc);
	}
        return return_array_list;
    }
    public static String[] job_by_id(Connection con, String job_id) throws IOException, SQLException
    {
        String job_info[] = {"","","","","","","","","","","","","",""};
        
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        //String ps_query_string = "SELECT * FROM job_tasks WHERE job_id=? AND id=?";
        String ps_query_string = "SELECT id," 
                + "name,"
                + "description,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "priority,"
                + "status,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "estimated_duration,"
                + "actual_duration,"
                + "notes " 
                + "FROM jobs WHERE id=?";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, job_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                job_info[0] = rs.getString("id");
                job_info[1] = rs.getString("name");
                job_info[2] = rs.getString("description");
                job_info[3] = rs.getString("assigned_group_id");
                job_info[4] = rs.getString("assigned_to_id");
                job_info[5] = rs.getString("priority");
                job_info[6] = rs.getString("status");
                job_info[7] = rs.getString("scheduled_start_date");
                job_info[8] = rs.getString("actual_start_date");
                job_info[9] = rs.getString("scheduled_end_date");
                job_info[10] = rs.getString("actual_end_date");
                job_info[11] = rs.getString("estimated_duration");
                job_info[12] = rs.getString("actual_duration");
                job_info[13] = rs.getString("notes");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.job_by_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.job_by_id:=" + exc);
	}
        return job_info;
    }

    public static ArrayList<String[]> get_attachments_by_job_id(Connection con, String job_id) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        PreparedStatement stmt;

        try 
        {

            // Create a Statement Object
            String query = "SELECT job_attachements.*,users.first,users.last from job_attachements LEFT JOIN users ON job_attachements.uploaded_by = users.id WHERE job_id = ?";
            stmt = con.prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, job_id);
//            System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();

            while(rs.next())
            {
                String attachment[] = new String[10];
                attachment[0] = rs.getString("id");
                attachment[1] = rs.getString("job_id");
                attachment[2] = rs.getString("uuid");
                attachment[3] = rs.getString("name_on_disk");
                attachment[4] = rs.getString("file_name");
                attachment[5] = rs.getString("file_date");
                attachment[6] = rs.getString("file_size");
                attachment[7] = rs.getString("rev");
                attachment[8] = rs.getString("uploaded_by");
                attachment[9] = rs.getString("first") + " " + rs.getString("last");
                return_array_list.add(attachment);
            }

            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.get_attachments_by_job_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.get_attachments_by_job_id:=" + exc);
	}
        return return_array_list;
    }

    public static ArrayList<String[]> get_attachments_by_job_task_id(Connection con, String job_id, String task_id) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        PreparedStatement stmt;

        try 
        {

            // Create a Statement Object
            String query = "SELECT job_task_attachements.*,users.first,users.last from job_task_attachements LEFT JOIN users ON job_task_attachements.uploaded_by = users.id WHERE job_id = ? AND task_id = ?";
            stmt = con.prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, job_id);
            stmt.setString(2, task_id);
//            System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();

            while(rs.next())
            {
                String attachment[] = new String[11];
                attachment[0] = rs.getString("id");
                attachment[1] = rs.getString("job_id");
                attachment[2] = rs.getString("task_id");
                attachment[3] = rs.getString("uuid");
                attachment[4] = rs.getString("name_on_disk");
                attachment[5] = rs.getString("file_name");
                attachment[6] = rs.getString("file_date");
                attachment[7] = rs.getString("file_size");
                attachment[8] = rs.getString("rev");
                attachment[9] = rs.getString("uploaded_by");
                attachment[10] = rs.getString("first") + " " + rs.getString("last");
                return_array_list.add(attachment);
            }

            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.get_attachments_by_job_task_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.get_attachments_by_job_task_id:=" + exc);
	}
        return return_array_list;
    }

    public static ArrayList<String[]> get_messages_by_job_task_id(Connection con, String job_id, String task_id, int start, int length) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        PreparedStatement stmt;

        try 
        {

            // Create a Statement Object
            String query = "SELECT "
                    + "messages.id,"
                    + "messages.job_id,"
                    + "messages.task_id,"
                    + "messages.sender_id,"
                    + "CONCAT(users.first, ' ', users.last) as sender_name,"
                    + "messages.content,"
                    + "DATE_FORMAT(messages.date_sent,'%Y-%m-%dT%H:%i:%s') as date_sent,"
                    + "messages.is_system,"
                    + "messages.saved,"
                    + "messages.distibuted,"
                    + "messages.seen,"
                    + "messages.deleted "
                + " FROM job_task_messages AS messages "
                + " LEFT JOIN users on messages.sender_id = users.id "
                + " WHERE job_id = ? AND task_id = ? " + (start > 0 ? "AND messages.id < ?" : "")
                + " ORDER BY id DESC"
                + " LIMIT ?";
            stmt = con.prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, job_id);
            stmt.setString(2, task_id);
            if (start > 0) {
                stmt.setInt(3, start);
                stmt.setInt(4, length);
            } else {
                stmt.setInt(3, length);
            }
//            System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();

            while(rs.next())
            {
                String message[] = new String[12];
                message[0] = rs.getString("id");
                message[1] = rs.getString("job_id");
                message[2] = rs.getString("task_id");
                message[3] = rs.getString("sender_id");
                message[4] = rs.getString("sender_name");
                message[5] = rs.getString("content");
                message[6] = rs.getString("date_sent");
                message[7] = rs.getString("is_system");
                message[8] = rs.getString("saved");
                message[9] = rs.getString("distibuted");
                message[10] = rs.getString("seen");
                message[11] = rs.getString("deleted");
                return_array_list.add(message);
            }

            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.get_messages_by_job_task_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.get_messages_by_job_task_id:=" + exc);
	}
        return return_array_list;
    }
    
    public static ArrayList<String[]> process_resultset(ResultSet rs, ArrayList<String[]> users, ArrayList<String[]> groups)
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        try
        {
            while(rs.next())
            {
                String job_info[] = new String[30];
                //init the array
                for(int a = 0; a < job_info.length;a++)
                {
                    job_info[a] = "--";
                }
                job_info[0] = rs.getString("id");
                job_info[1] = rs.getString("name");
                job_info[2] = rs.getString("description");
                job_info[3] = rs.getString("owner_group_id");
                job_info[4] = rs.getString("owner_id");
                job_info[5] = rs.getString("manager_id");
                job_info[6] = rs.getString("tech_lead_id");
                job_info[7] = rs.getString("scheduled_start_date");
                job_info[8] = rs.getString("actual_start_date");
                job_info[9] = rs.getString("scheduled_end_date");
                job_info[10] = rs.getString("actual_end_date");
                job_info[11] = rs.getString("status");
                job_info[12] = rs.getString("priority");
                job_info[13] = rs.getString("notes");                
                //LOOKUP owner_group info
                job_info[14] = "";
                for(int b = 0; b < groups.size(); b++)
                {
                    String this_group[] = groups.get(b);
                    if(this_group[0].equalsIgnoreCase(rs.getString("owner_group_id")))
                    {
                        job_info[14] = this_group[1];
                        b = groups.size();
                    }
                }
                //LOOKUP populate the owner info
                job_info[15] = "";
                job_info[16] = "";
                job_info[17] = "";
                job_info[18] = "";
                job_info[19] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("owner_id")))
                    {
                        job_info[15] = this_user[1];
                        job_info[16] = this_user[3];
                        job_info[17] = this_user[4];
                        job_info[18] = this_user[5];
                        job_info[19] = this_user[17];
                        a = users.size();
                    }
                }
                //LOOKUP populate the Manager info
                job_info[20] = "";
                job_info[21] = "";
                job_info[22] = "";
                job_info[23] = "";
                job_info[24] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("manager_id")))
                    {
                        job_info[20] = this_user[1];
                        job_info[21] = this_user[3];
                        job_info[22] = this_user[4];
                        job_info[23] = this_user[5];
                        job_info[24] = this_user[17];
                        a = users.size();
                    }
                }
                //LOOKUP populate the tech_lead info
                job_info[25] = "";
                job_info[26] = "";
                job_info[27] = "";
                job_info[28] = "";
                job_info[29] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("tech_lead_id")))
                    {
                        job_info[25] = this_user[1];
                        job_info[26] = this_user[3];
                        job_info[27] = this_user[4];
                        job_info[28] = this_user[5];
                        job_info[29] = this_user[17];
                        a = users.size();
                    }
                }
                return_array_list.add(job_info);
            }
        }
        catch(Exception e)
        {
            System.out.println("Exception in get_jobs.process_resultset=" + e);
        }
        return return_array_list;
    }

    public static ArrayList<String[]> process_job_task_resultset(ResultSet rs, ArrayList<String[]> jobs, ArrayList<String[]> users, ArrayList<String[]> groups)
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        try
        {
            while(rs.next())
            {
                String job_task[] = new String[23];
                job_task[0] = rs.getString("id");
                job_task[1] = rs.getString("job_id");
                job_task[2] = rs.getString("name");
                job_task[3] = rs.getString("description");
                job_task[4] = rs.getString("assigned_group_id");
                job_task[5] = rs.getString("assigned_to_id");
                job_task[6] = rs.getString("priority");
                job_task[7] = rs.getString("status");
                job_task[8] = rs.getString("scheduled_start_date");
                job_task[9] = rs.getString("actual_start_date");
                job_task[10] = rs.getString("scheduled_end_date");
                job_task[11] = rs.getString("actual_end_date");
                job_task[12] = rs.getString("estimated_duration");
                job_task[13] = rs.getString("actual_duration");
                job_task[14] = rs.getString("notes");
                //LOOKUP assigned_group info
                job_task[15] = "";
                for(int b = 0; b < groups.size(); b++)
                {
                    String this_group[] = groups.get(b);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        job_task[15] = this_group[1];
                        b = groups.size();
                    }
                }
                //LOOKUP populate the assigned_to_id info
                job_task[16] = "";
                job_task[17] = "";
                job_task[18] = "";
                job_task[19] = "";
                job_task[20] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        job_task[16] = this_user[1];
                        job_task[17] = this_user[3];
                        job_task[18] = this_user[4];
                        job_task[19] = this_user[5];
                        job_task[20] = this_user[17];
                        a = users.size();
                    }
                }
                job_task[21] = "";
                job_task[22] = "";
                for(int a = 0; a < jobs.size(); a++)
                {
                    String job[] = jobs.get(a);
                    if(job[0].equalsIgnoreCase(rs.getString("job_id")))
                    {
                        job_task[21] = job[1];
                        job_task[22] = job[2];
                        a = jobs.size();
                    }
                }
                return_array_list.add(job_task);
            }
        }
        catch(Exception e)
        {
            System.out.println("Exception in get_jobs.process_job_task_resultset=" + e);
        }
        return return_array_list;
    }
    public static ArrayList<String[]> process_job_task_resultset(ResultSet rs)
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        try
        {
            while(rs.next())
            {
                String job_task[] = new String[23];
                job_task[0] = rs.getString("id");
                job_task[1] = rs.getString("job_id");
                job_task[2] = rs.getString("name");
                job_task[3] = rs.getString("description");
                job_task[4] = rs.getString("assigned_group_id");
                job_task[5] = rs.getString("assigned_to_id");
                job_task[6] = rs.getString("priority");
                job_task[7] = rs.getString("status");
                job_task[8] = rs.getString("scheduled_start_date");
                job_task[9] = rs.getString("actual_start_date");
                job_task[10] = rs.getString("scheduled_end_date");
                job_task[11] = rs.getString("actual_end_date");
                job_task[12] = rs.getString("estimated_duration");
                job_task[13] = rs.getString("actual_duration");
                job_task[14] = rs.getString("notes");
                //LOOKUP assigned_group info
                job_task[15] = rs.getString("assigned_group_name");
                //LOOKUP populate the assigned_to_id info
                job_task[16] = rs.getString("assigned_to_username");
                job_task[17] = rs.getString("assigned_to_first");
                job_task[18] = rs.getString("assigned_to_mi");
                job_task[19] = rs.getString("assigned_to_last");
                job_task[20] = rs.getString("assigned_to_phone_mobile");
                job_task[21] = rs.getString("job_name");
                job_task[22] = rs.getString("job_description");
                return_array_list.add(job_task);
            }
        }
        catch(Exception e)
        {
            System.out.println("Exception in get_jobs.process_job_task_resultset=" + e);
        }
        return return_array_list;
    }
    
    public static int next_job_id(Connection con) throws IOException, SQLException
    {
        int return_int = 0;  
        String ps_query_string = "SELECT max(id) AS value FROM jobs";        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
            }
            stmt.close();
            //add 1 to max
            return_int++;
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.next_job_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.next_job_id:=" + exc);
	}
        return return_int;
    }
    public static ArrayList<String[]> all(Connection con) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        String query = "SELECT " 
                + "id,"
                + "name,"
                + "description,"
                + "owner_group_id," 
                + "owner_id," 
                + "manager_id,"
                + "tech_lead_id,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "status,"
                + "priority,"
                + "notes "
                + "FROM jobs";
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            return_array_list = process_resultset(rs);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.all_open:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.all_open:=" + exc);
	}
        return return_array_list;
    }
    public static int next_job_task_id_for_job(Connection con, int job_id) throws IOException, SQLException
    {
        int return_int = 0;  
        String ps_query_string = "SELECT max(id) AS value FROM job_tasks";        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            
            stmt = con.prepareStatement(ps_query_string);
            stmt.setInt(1, job_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
            }
            stmt.close();
            //add 1 to max
            return_int++;
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.next_job_task_id_for_job:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.next_job_task_id_for_job:=" + exc);
	}
        return return_int;
    }
    public static ArrayList<String[]> filtered_list(Connection con, String status) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        String query = "SELECT * FROM jobs WHERE status=?";
        if(status.equalsIgnoreCase("all"))
        {
            query = "SELECT * FROM jobs";
            String ps_query_string = "SELECT id," 
                + "job_id,"
                + "name,"
                + "description,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "priority,"
                + "status,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "estimated_duration,"
                + "actual_duration,"
                + "notes " 
                + "FROM job_tasks WHERE job_id=?";
        }
        else
        {
             query = "SELECT * FROM jobs WHERE status='" + status + "'";
        }
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            //System.out.println("query=" + stmt);
            ResultSet rs = stmt.executeQuery();
            return_array_list = process_resultset(rs);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.filtered_list:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.filtered_list:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> all_open(Connection con) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM jobs WHERE status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%'";
        String query = "SELECT " 
                + "id,"
                + "name,"
                + "description,"
                + "owner_group_id," 
                + "owner_id," 
                + "manager_id,"
                + "tech_lead_id,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "status,"
                + "priority,"
                + "notes "
                + "FROM jobs WHERE status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%'";
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            //System.out.println("query=" + stmt);
            ResultSet rs = stmt.executeQuery();
            return_array_list = process_resultset(rs);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.all_open:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.all_open:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> all_open_assigned_to_id(Connection con, String user_id) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM jobs WHERE (status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%') AND (owner_id = ? OR manager_id= ? OR tech_lead_id= ?)";
        String query = "SELECT " 
                + "id,"
                + "name,"
                + "description,"
                + "owner_group_id," 
                + "owner_id," 
                + "manager_id,"
                + "tech_lead_id,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "status,"
                + "priority,"
                + "notes "
                + "FROM jobs WHERE (status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%') AND (owner_id = ? OR manager_id= ? OR tech_lead_id= ?)";
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, user_id);
            stmt.setString(2, user_id);
            stmt.setString(3, user_id);
            
            //System.out.println("all_open_assigned_to_id query=" + stmt);
            ResultSet rs = stmt.executeQuery();
            return_array_list = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.all_open_assigned_to_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.all_open_assigned_to_id:=" + exc);
	}
        return return_array_list;
    }
    public static int all_open_assigned_to_id_count(Connection con, String user_id) throws IOException, SQLException
    {
        int count = 0;
        String query = "SELECT count(*) AS job_count FROM jobs WHERE (status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%') AND (owner_id = ? OR manager_id= ? OR tech_lead_id= ?)";
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, user_id);
            stmt.setString(2, user_id);
            stmt.setString(3, user_id);
            
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("job_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.all_open_assigned_to_id_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.all_open_assigned_to_id_count:=" + exc);
	}
        return count;
    }
    public static ArrayList <String[]> open_job_tasks_for_user_id(Connection con, String user_id) throws IOException, SQLException
    {
        ArrayList<String[]> job_tasks = new ArrayList();
        //String ps_query_string = "SELECT * FROM job_tasks WHERE assigned_to_id=? AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        String ps_query_string = "SELECT id," 
                + "job_id,"
                + "name,"
                + "description,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "priority,"
                + "status,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "estimated_duration,"
                + "actual_duration,"
                + "notes " 
                + "FROM job_tasks WHERE assigned_to_id=? AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        ArrayList<String[]> jobs = get_jobs.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            job_tasks = process_job_task_resultset(rs,jobs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.open_job_tasks_for_user_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.open_job_tasks_for_user_id:=" + exc);
	}
        return job_tasks;
    }
    public static int open_job_tasks_count_for_user_id(Connection con, String user_id) throws IOException, SQLException
    {
        int count = 0;
        String ps_query_string = "SELECT count(*) AS task_count FROM job_tasks WHERE assigned_to_id=? AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("task_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.open_job_tasks_count_for_user_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.open_job_tasks_count_for_user_id:=" + exc);
	}
        return count;
    }
    public static ArrayList <String[]> open_job_tasks(Connection con) throws IOException, SQLException
    {
        ArrayList<String[]> job_tasks = new ArrayList();
        //String ps_query_string = "SELECT * FROM job_tasks WHERE status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        String ps_query_string = "SELECT id," 
                + "job_id,"
                + "name,"
                + "description,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "priority,"
                + "status,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "estimated_duration,"
                + "actual_duration,"
                + "notes " 
                + "FROM job_tasks WHERE status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        ArrayList<String[]> jobs = get_jobs.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            job_tasks = process_job_task_resultset(rs,jobs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.open_job_tasks:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.open_job_tasks:=" + exc);
	}
        return job_tasks;
    }
    public static ArrayList <String[]> delinquent_job_tasks_for_user_id(Connection con, String user_id) throws IOException, SQLException
    {
        ArrayList<String[]> job_tasks = new ArrayList();
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        java.util.Date now = new java.util.Date();
        String now_string = timestamp_format.format(now);
        //String ps_query_string = "SELECT * FROM job_tasks WHERE assigned_to_id=? AND scheduled_end_date < '" + now_string + "' AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        String ps_query_string = "SELECT id," 
                + "job_id,"
                + "name,"
                + "description,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "priority,"
                + "status,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "estimated_duration,"
                + "actual_duration,"
                + "notes " 
                + "FROM job_tasks WHERE assigned_to_id=? AND scheduled_end_date < '" + now_string + "' AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        ArrayList<String[]> jobs = get_jobs.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            job_tasks = process_job_task_resultset(rs, jobs, users, groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.delinquent_job_tasks_for_user_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.delinquent_job_tasks_for_user_id:=" + exc);
	}
        return job_tasks;
    }
    public static int delinquent_job_tasks_count_for_user_id(Connection con, String user_id) throws IOException, SQLException
    {
        int count = 0;
        java.util.Date now = new java.util.Date();
        String now_string = timestamp_format.format(now);
        String ps_query_string = "SELECT count(*) AS task_count FROM job_tasks WHERE assigned_to_id=? AND scheduled_end_date < '" + now_string + "' AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("task_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.delinquent_job_tasks_count_for_user_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.delinquent_job_tasks_count_for_user_id:=" + exc);
	}
        return count;
    }
    public static ArrayList <String[]> delinquent_job_tasks(Connection con) throws IOException, SQLException
    {
        ArrayList<String[]> job_tasks = new ArrayList();
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        java.util.Date now = new java.util.Date();
        String now_string = timestamp_format.format(now);
        //String ps_query_string = "SELECT * FROM job_tasks WHERE scheduled_end_date < '" + now_string + "' AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        String ps_query_string = "SELECT id," 
                + "job_id,"
                + "name,"
                + "description,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "priority,"
                + "status,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "estimated_duration,"
                + "actual_duration,"
                + "notes " 
                + "FROM job_tasks WHERE scheduled_end_date < '" + now_string + "' AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        ArrayList<String[]> jobs = get_jobs.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            job_tasks = process_job_task_resultset(rs, jobs, users, groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.delinquent_job_tasks:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.delinquent_job_tasks:=" + exc);
	}
        return job_tasks;
    }
    public static ArrayList<Integer> my_jobs(Connection con, String user_id)
    {
        PreparedStatement stmt;
        int total = 0;
        int critical = 0;
        int high = 0;
        int medium = 0;
        int low = 0;
        String priority = "";
        ArrayList<Integer> return_arraylist = new ArrayList();
        try
        {
            //total
            //String ps_query_string = "SELECT * FROM jobs WHERE owner_id=? OR manager_id=? OR tech_lead_id=? AND status <> 'Closed' AND status <> 'Cancelled' AND status <> 'Complete'";  
            String ps_query_string = "SELECT " 
                + "id,"
                + "name,"
                + "description,"
                + "owner_group_id," 
                + "owner_id," 
                + "manager_id,"
                + "tech_lead_id,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "status,"
                + "priority,"
                + "notes "
                + "FROM jobs WHERE owner_id=? OR manager_id=? OR tech_lead_id=? AND status <> 'Closed' AND status <> 'Cancelled' AND status <> 'Complete'";
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, user_id);
            stmt.setString(2, user_id);
            stmt.setString(3, user_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                total++;
                priority = rs.getString("priority");
                if(priority.equalsIgnoreCase("critical"))
                {
                    critical++;
                }
                else if(priority.equalsIgnoreCase("high"))
                {
                    high++;
                }
                else if(priority.equalsIgnoreCase("medium"))
                {
                    medium++;
                }
                else if(priority.equalsIgnoreCase("low"))
                {
                    low++;
                }
            }
            stmt.close();
            
            return_arraylist.add(0,total);
            //critical
            return_arraylist.add(1,critical);
            //high
            return_arraylist.add(2,high);
            //medium
            return_arraylist.add(3,medium);
            //low
            return_arraylist.add(4,low);
        }
        catch(Exception e)
        {
            System.out.println("Exception in db.get_jobs.my_jobs=" + e);
        }
        return return_arraylist;
    }
    public static ArrayList<Integer> jobs(Connection con)
    {
        PreparedStatement stmt;
        int total = 0;
        int critical = 0;
        int high = 0;
        int medium = 0;
        int low = 0;
        String priority = "";
        ArrayList<Integer> return_arraylist = new ArrayList();
        try
        {
            //total
            //String ps_query_string = "SELECT * FROM jobs WHERE status <> 'Closed' AND status <> 'Cancelled' AND status <> 'Complete'";  
            String ps_query_string = "SELECT " 
                + "id,"
                + "name,"
                + "description,"
                + "owner_group_id," 
                + "owner_id," 
                + "manager_id,"
                + "tech_lead_id,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "status,"
                + "priority,"
                + "notes "
                + "FROM jobs WHERE status <> 'Closed' AND status <> 'Cancelled' AND status <> 'Complete'";
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                total++;
                priority = rs.getString("priority");
                if(priority.equalsIgnoreCase("critical"))
                {
                    critical++;
                }
                else if(priority.equalsIgnoreCase("high"))
                {
                    high++;
                }
                else if(priority.equalsIgnoreCase("medium"))
                {
                    medium++;
                }
                else if(priority.equalsIgnoreCase("low"))
                {
                    low++;
                }
            }
            stmt.close();
            
            return_arraylist.add(0,total);
            //critical
            return_arraylist.add(1,critical);
            //high
            return_arraylist.add(2,high);
            //medium
            return_arraylist.add(3,medium);
            //low
            return_arraylist.add(4,low);
        }
        catch(Exception e)
        {
            System.out.println("Exception in db.get_jobs.jobs=" + e);
        }
        return return_arraylist;
    }
    public static ArrayList<String[]> all_open_jobs_by_priority(Connection con, String priority) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM jobs WHERE priority=? AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%'";
        String query = "SELECT " 
                + "id,"
                + "name,"
                + "description,"
                + "owner_group_id," 
                + "owner_id," 
                + "manager_id,"
                + "tech_lead_id,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "status,"
                + "priority,"
                + "notes "
                + "FROM jobs WHERE priority=? AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%'";  
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, priority);
            ResultSet rs = stmt.executeQuery();
            return_array_list = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.all_open_jobs_by_priority:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.all_open_jobs_by_priority:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> my_jobs_by_priority(Connection con, String priority, String user_id) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM jobs WHERE priority=? AND (owner_id=? OR manager_id=? OR tech_lead_id=?) AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%'";
        String query = "SELECT " 
                + "id,"
                + "name,"
                + "description,"
                + "owner_group_id," 
                + "owner_id," 
                + "manager_id,"
                + "tech_lead_id,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "status,"
                + "priority,"
                + "notes "
                + "FROM jobs WHERE priority=? AND (owner_id=? OR manager_id=? OR tech_lead_id=?) AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%'";  
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, priority);
            stmt.setString(2, user_id);
            stmt.setString(3, user_id);
            stmt.setString(4, user_id);
            ResultSet rs = stmt.executeQuery();
            return_array_list = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.my_jobs_by_priority:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.my_jobs_by_priority:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList <String[]> my_open_job_tasks(Connection con, String user_id) throws IOException, SQLException
    {
        ArrayList<String[]> job_tasks = new ArrayList();
        //String ps_query_string = "SELECT * FROM job_tasks WHERE assigned_to_id=? AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        String ps_query_string = "SELECT id," 
                + "job_id,"
                + "name,"
                + "description,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "priority,"
                + "status,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "estimated_duration,"
                + "actual_duration,"
                + "notes " 
                + "FROM job_tasks WHERE assigned_to_id=? AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        ArrayList<String[]> jobs = get_jobs.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            job_tasks = process_job_task_resultset(rs,jobs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.my_open_job_tasks:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.my_open_job_tasks:=" + exc);
	}
        return job_tasks;
    }
    public static int all_open_jobs_count(Connection con) throws IOException, SQLException
    {
        int return_int = 0;
        String query = "SELECT count(*) as value FROM jobs WHERE status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%'";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");                
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.all_open_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.all_open_count:=" + exc);
	}
        return return_int;
    }
    public static int open_job_tasks_count(Connection con) throws IOException, SQLException
    {
        int job_tasks = 0;
        String ps_query_string = "SELECT count(*) as value FROM job_tasks WHERE status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                job_tasks = rs.getInt("value");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.open_job_tasks_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.open_job_tasks_count:=" + exc);
	}
        return job_tasks;
    }
    public static int delinquent_job_tasks_count(Connection con) throws IOException, SQLException
    {
        int count = 0;
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        java.util.Date now = new java.util.Date();
        String now_string = timestamp_format.format(now);
        String ps_query_string = "SELECT count(*) AS task_count FROM job_tasks WHERE scheduled_end_date < '" + now_string + "' AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("task_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.delinquent_job_tasks_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.delinquent_job_tasks_count:=" + exc);
	}
        return count;
    }
    public static HashMap<String, String> job_task_full_info(Connection con, String task_id) throws IOException, SQLException
    {
        HashMap<String,String> return_hashmap = new HashMap();    
        
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        String ps_query_string = "SELECT job_tasks.*, jobs.name AS job_name,jobs.description AS job_description FROM job_tasks INNER JOIN jobs ON job_tasks.job_id = jobs.id WHERE job_tasks.id=?";
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, task_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_hashmap.put("id", rs.getString("id"));
                return_hashmap.put("job_id_name", rs.getString("job_name"));
                return_hashmap.put("name", rs.getString("name"));
                return_hashmap.put("description", rs.getString("description"));
                
                return_hashmap.put("assigned_group_id_name", "");
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        return_hashmap.put("assigned_group_id_name", this_group[1]);
                        a = all_group_info.size();
                    }
                }
                
                try
                {
                    String user_info[] = db.get_users.by_id(con, rs.getString("assigned_to_id"));
                    return_hashmap.put("assigned_to_id_username", user_info[1]);
                }
                catch(Exception e)
                {
                    return_hashmap.put("assigned_to_id_username", "");
                }
                return_hashmap.put("priority", rs.getString("priority"));
                return_hashmap.put("status", rs.getString("status"));
                try
                {
                    return_hashmap.put("scheduled_start_date", date_time_picker_format.format(rs.getTimestamp("scheduled_start_date")));
                }
                catch(Exception e)
                {
                    return_hashmap.put("scheduled_start_date","");
                }
                try
                {
                    return_hashmap.put("actual_start_date", date_time_picker_format.format(rs.getTimestamp("actual_start_date")));
                }
                catch(Exception e)
                {
                    return_hashmap.put("actual_start_date","");
                }
                try
                {
                    return_hashmap.put("scheduled_end_date", date_time_picker_format.format(rs.getTimestamp("scheduled_end_date")));
                }
                catch(Exception e)
                {
                    return_hashmap.put("scheduled_end_date","");
                }
                try
                {
                    return_hashmap.put("actual_end_date", date_time_picker_format.format(rs.getTimestamp("actual_end_date")));
                }
                catch(Exception e)
                {
                    return_hashmap.put("actual_end_date","");
                }
                return_hashmap.put("estimated_duration", rs.getString("estimated_duration"));
                return_hashmap.put("actual_duration", rs.getString("actual_duration"));
                return_hashmap.put("notes", rs.getString("notes"));                
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.job_task_full_info:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.job_task_full_info:=" + exc);
	}
        return return_hashmap;
    }
    public static ArrayList<String[]> delinquent_jobs_for_user_id(Connection con, String user_id) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        java.util.Date now = new java.util.Date();
        String now_string = timestamp_format.format(now);
        //String query = "SELECT * FROM jobs WHERE assigned_to_id=? AND scheduled_end_date < '" + now_string + "' AND status NOT LIKE '%complete%' AND  status NOT LIKE '%cancelled%'";
        String ps_query_string = "SELECT id," 
                + "name,"
                + "description,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "priority,"
                + "status,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, "
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, "
                + "actual_end_date,"
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, "
                + "estimated_duration,"
                + "actual_duration,"
                + "notes " 
                + "FROM jobs WHERE assigned_to_id=? AND scheduled_end_date < '" + now_string + "' AND status NOT LIKE '%complete%' AND  status NOT LIKE '%cancelled%'";
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            return_array_list = process_resultset(rs);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.delinquent_jobs_for_user_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.delinquent_jobs_for_user_id:=" + exc);
	}
        return return_array_list;
    }
    public static int delinquent_jobs_count_for_user_id(Connection con, String user_id) throws IOException, SQLException
    {
        int count = 0;
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        java.util.Date now = new java.util.Date();
        String now_string = timestamp_format.format(now);
        String ps_query_string = "SELECT count(*) AS job_count FROM jobs WHERE assigned_to_id=? AND scheduled_end_date < '" + now_string + "' AND status NOT LIKE '%complete%' AND  status NOT LIKE '%cancelled%'";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("job_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.delinquent_project_tasks_count_for_user_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.delinquent_project_tasks_count_for_user_id:=" + exc);
	}
        return count;
    }  
    public static ArrayList<String[]> all_delinquent_jobs(Connection con) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        java.util.Date now = new java.util.Date();
        String now_string = timestamp_format.format(now);
        //String query = "SELECT * FROM jobs WHERE scheduled_end_date < '" + now_string + "' AND status NOT LIKE '%complete%' AND  status NOT LIKE '%cancelled%'";
        String ps_query_string = "SELECT id," 
                + "name,"
                + "description,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "priority,"
                + "status,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, "
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, "
                + "actual_end_date,"
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, "
                + "estimated_duration,"
                + "actual_duration,"
                + "notes" 
                + "FROM jobs WHERE scheduled_end_date < '" + now_string + "' AND status NOT LIKE '%complete%' AND  status NOT LIKE '%cancelled%'";
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            return_array_list = process_resultset(rs);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.all_delinquent_jobs:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.all_delinquent_jobs:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<Integer> my_open_jobs_count_by_priority(Connection con, String user_id) throws IOException, SQLException
    {
        ArrayList<Integer> return_arraylist = new ArrayList();
        int total = 0;
        String priority = "";
        int critical = 0;
        int high = 0;
        int medium = 0;
        int low = 0;
        //String query = "SELECT * FROM jobs WHERE assigned_to_id = ? AND status NOT LIKE '%complete%' AND status NOT LIKE '%cancelled%'";
        String ps_query_string = "SELECT id," 
                + "name,"
                + "description,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "priority,"
                + "status,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, "
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, "
                + "actual_end_date,"
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, "
                + "estimated_duration,"
                + "actual_duration,"
                + "notes " 
                + "FROM jobs WHERE assigned_to_id = ? AND status NOT LIKE '%complete%' AND status NOT LIKE '%cancelled%'";
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                total++;
                priority = rs.getString("priority");
                if(priority.equalsIgnoreCase("critical"))
                {
                    critical++;
                }
                else if(priority.equalsIgnoreCase("high"))
                {
                    high++;
                }
                else if(priority.equalsIgnoreCase("medium"))
                {
                    medium++;
                }
                else if(priority.equalsIgnoreCase("low"))
                {
                    low++;
                }
            }
            stmt.close();
            
            return_arraylist.add(0,total);
            //critical
            return_arraylist.add(1,critical);
            //high
            return_arraylist.add(2,high);
            //medium
            return_arraylist.add(3,medium);
            //low
            return_arraylist.add(4,low);
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.my_open_jobs_count_by_priority:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.my_open_jobs_count_by_priority:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<Integer> open_jobs_count_by_priority(Connection con) throws IOException, SQLException
    {
        
        int total = 0;
        String priority = "";
        int critical = 0;
        int high = 0;
        int medium = 0;
        int low = 0;
        ArrayList<Integer> return_arraylist = new ArrayList();
        String query = "SELECT * FROM jobs WHERE status NOT LIKE '%complete%' AND status NOT LIKE '%cancelled%'";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                total++;
                priority = rs.getString("priority");
                if(priority.equalsIgnoreCase("critical"))
                {
                    critical++;
                }
                else if(priority.equalsIgnoreCase("high"))
                {
                    high++;
                }
                else if(priority.equalsIgnoreCase("medium"))
                {
                    medium++;
                }
                else if(priority.equalsIgnoreCase("low"))
                {
                    low++;
                }
            }
            stmt.close();
            
            return_arraylist.add(0,total);
            //critical
            return_arraylist.add(1,critical);
            //high
            return_arraylist.add(2,high);
            //medium
            return_arraylist.add(3,medium);
            //low
            return_arraylist.add(4,low);
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.open_jobs_count_by_priority:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.open_jobs_count_by_priority:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> my_open_jobs_by_priority(Connection con, String priority, String user_id) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM jobs WHERE priority=? AND assigned_to_id = ? AND status NOT LIKE '%complete%' AND status NOT LIKE '%cancelled%'";
        String ps_query_string = "SELECT id," 
                + "name,"
                + "description,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "priority,"
                + "status,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, "
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, "
                + "actual_end_date,"
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, "
                + "estimated_duration,"
                + "actual_duration,"
                + "notes " 
                + "FROM jobs WHERE priority=? AND assigned_to_id = ? AND status NOT LIKE '%complete%' AND status NOT LIKE '%cancelled%'";
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, priority);
            stmt.setString(2, user_id);
            ResultSet rs = stmt.executeQuery();
            return_array_list = process_resultset(rs);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.my_open_jobs_by_priority:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.my_open_jobs_by_priority:=" + exc);
	}
        return return_array_list;
    }    
    public static int open_jobs_count(Connection con) throws IOException, SQLException
    {
        int return_int = 0;
        String query = "SELECT count(*) as value FROM jobs WHERE status NOT LIKE '%complete%' AND status NOT LIKE '%cancelled%'";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.open_jobs_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.open_jobs_count:=" + exc);
	}
        return return_int;
    }
    public static int delinquent_jobs_count(Connection con) throws IOException, SQLException
    {
        int return_int = 0;
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        java.util.Date now = new java.util.Date();
        String now_string = timestamp_format.format(now);
        String query = "SELECT count(*) as value FROM jobs WHERE scheduled_end_date < '" + now_string + "' AND status NOT LIKE '%complete%' AND  status NOT LIKE '%cancelled%'";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.delinquent_jobs_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.delinquent_jobs_count:=" + exc);
	}
        return return_int;
    }
    public static HashMap<String, String> job_by_id_full(Connection con, String id) throws IOException, SQLException
    {
        HashMap<String,String> return_hashmap = new HashMap();    
        
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        String ps_query_string = "SELECT * FROM jobs WHERE id=?";
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                boolean is_set = false;
                return_hashmap.put("id", rs.getString("id"));
                return_hashmap.put("name", support.string_utils.check_for_null(rs.getString("name")));
                return_hashmap.put("description", support.string_utils.check_for_null(rs.getString("description")));
                return_hashmap.put("assigned_group_id_name", "");
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        is_set = true;
                        return_hashmap.put("assigned_group_id_name", support.string_utils.check_for_null(this_group[1]));
                        a = all_group_info.size();
                    }
                }
                if(!is_set)
                {
                    return_hashmap.put("assigned_group_id_name", "");
                }
                
                try
                {
                    String user_info[] = db.get_users.by_id(con, rs.getString("assigned_to_id"));
                    return_hashmap.put("assigned_to_id_username", user_info[1]);
                }
                catch(Exception e)
                {
                    return_hashmap.put("assigned_to_id_username", "");
                }
                return_hashmap.put("priority", support.string_utils.check_for_null(rs.getString("priority")));
                return_hashmap.put("status", support.string_utils.check_for_null(rs.getString("status")));
                
                try
                {
                    return_hashmap.put("scheduled_start_date", date_time_picker_format.format(rs.getTimestamp("scheduled_start_date")));
                }
                catch(Exception e)
                {
                    return_hashmap.put("scheduled_start_date","");
                }
                try
                {
                    return_hashmap.put("actual_start_date", date_time_picker_format.format(rs.getTimestamp("actual_start_date")));
                }
                catch(Exception e)
                {
                    return_hashmap.put("actual_start_date","");
                }
                try
                {
                    return_hashmap.put("scheduled_end_date", date_time_picker_format.format(rs.getTimestamp("scheduled_end_date")));
                }
                catch(Exception e)
                {
                    return_hashmap.put("scheduled_end_date","");
                }
                try
                {
                    return_hashmap.put("actual_end_date", date_time_picker_format.format(rs.getTimestamp("actual_end_date")));
                }
                catch(Exception e)
                {
                    return_hashmap.put("actual_end_date","");
                }
                return_hashmap.put("estimated_duration", rs.getString("estimated_duration"));
                return_hashmap.put("actual_duration", rs.getString("actual_duration"));
                return_hashmap.put("notes", rs.getString("notes"));  
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.job_by_id_full:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.job_by_id_full:=" + exc);
	}
        return return_hashmap;
    }
    public static ArrayList<String[]> my_open_jobs(Connection con, String user_id) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM jobs WHERE assigned_to_id = ? AND status NOT LIKE '%complete%' AND status NOT LIKE '%cancelled%'";
        String ps_query_string = "SELECT id," 
                + "name,"
                + "description,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "priority,"
                + "status,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, "
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, "
                + "actual_end_date,"
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, "
                + "estimated_duration,"
                + "actual_duration,"
                + "notes " 
                + "FROM jobs WHERE assigned_to_id = ? AND status NOT LIKE '%complete%' AND status NOT LIKE '%cancelled%'";
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            return_array_list = process_resultset(rs, users, groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_jobs.my_open_jobs:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_jobs.my_open_jobs:=" + exc);
	}
        return return_array_list;
    }

}

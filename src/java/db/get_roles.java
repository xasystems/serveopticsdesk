//Copyright 2021 XaSystems, Inc. , All rights reserved.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 *
 * @author server-xc6701
 */
public class get_roles 
{
    private static Logger logger = LogManager.getLogger();
    
    public static String[] by_role_id(Connection con, String system_role_id) throws IOException, SQLException
    {
        //id=0 name=1 description=2 created=3 created_by_id=4 administration=5 manager=6 financial=7 incident=8 request=9 contact=10 cx=11 survey=12 sla=13 project=14 job=15 
        String returnString[] = {"0","--","--","--","--","false","false","none","none","none","none","none","none","none","none","none","none","none","none","none","none"};
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM roles WHERE id=?");
            stmt.setString(1, system_role_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                returnString[0] = rs.getString("id");
                returnString[1] = rs.getString("name");
                returnString[2] = rs.getString("description");
                returnString[3] = rs.getString("created");
                returnString[4] = rs.getString("created_by_id");
                returnString[5] = rs.getString("administration");  //true or false
                returnString[6] = rs.getString("manager");   //true or false
                returnString[7] = rs.getString("financial");//create,read,update,delete  
                returnString[8] = rs.getString("incident"); //create,read,update,delete
                returnString[9] = rs.getString("request"); //create,read,update,delete
                returnString[10] = rs.getString("contact"); //create,read,update,delete
                returnString[11] = rs.getString("cx"); //create,read,update,delete
                returnString[12] = rs.getString("survey");//create,read,update,delete  
                returnString[13] = rs.getString("sla");//create,read,update,delete  
                returnString[14] = rs.getString("project"); //create,read,update,delete                        
                returnString[15] = rs.getString("job"); //create,read,update,delete
                returnString[16] = rs.getString("metric"); //create,read,update,delete
                returnString[17] = rs.getString("asset"); //create,read,update,delete
                returnString[18] = rs.getString("user"); //create,read,update,delete
                returnString[19] = rs.getString("self_service"); //create,read,update,delete
                returnString[20] = rs.getString("problem"); //create,read,update,delete
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_roles.by_role_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_roles.by_role_id:=" + exc);
	}
        return returnString;
    }   

    public static ArrayList<String[]> all(Connection con) throws IOException, SQLException
    {
        ArrayList<String[]>return_arraylist= new ArrayList();
        //id=0 name=1 description=2 created=3 created_by_id=4 administration=5 manager=6 financial=7 incident=8 request=9 contact=10 cx=11 survey=12 sla=13 project=14 job=15 
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM roles ORDER BY id");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = new String[21];
                temp[0] = rs.getString("id");
                temp[1] = rs.getString("name");
                temp[2] = rs.getString("description");
                temp[3] = rs.getString("created");
                temp[4] = rs.getString("created_by_id");
                temp[5] = rs.getString("administration");  //true or false
                temp[6] = rs.getString("manager");   //true or false
                temp[7] = rs.getString("financial");//create,read,update,delete  
                temp[8] = rs.getString("incident"); //create,read,update,delete
                temp[9] = rs.getString("request"); //create,read,update,delete
                temp[10] = rs.getString("contact"); //create,read,update,delete
                temp[11] = rs.getString("cx"); //create,read,update,delete
                temp[12] = rs.getString("survey");//create,read,update,delete  
                temp[13] = rs.getString("sla");//create,read,update,delete  
                temp[14] = rs.getString("project"); //create,read,update,delete                        
                temp[15] = rs.getString("job"); //create,read,update,delete
                temp[16] = rs.getString("metric"); //create,read,update,delete
                temp[17] = rs.getString("asset"); //create,read,update,delete
                temp[18] = rs.getString("user"); //create,read,update,delete
                temp[19] = rs.getString("self_service"); //create,read,update,delete
                temp[20] = rs.getString("problem"); //create,read,update,delete
                return_arraylist.add(temp);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_roles.all:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_roles.all:=" + exc);
	}
        return return_arraylist;
    }   
    public static ArrayList<String[]> all_filtered(Connection con, List<Map<String, String>> filters) throws IOException, SQLException
    {
        ArrayList<String[]>return_arraylist= new ArrayList();
        //id=0 name=1 description=2 created=3 created_by_id=4 administration=5 manager=6 financial=7 incident=8 request=9 contact=10 cx=11 survey=12 sla=13 project=14 job=15 
        PreparedStatement stmt;
        try 
        {
            // 
            String where = "";
            for (int i = 0; i < filters.size(); i++)
            {
              String filter_key = filters.get(i).get("key");
              String filter_value = filters.get(i).get("value");
              if (!"".equals(where)) 
              {
                  where += " AND ";
              }
              where += filter_key + " = ? ";
            }
            
            if (!"".equals(where)) {
                where = " WHERE " + where;
            }
            // Create a Statement Object

            stmt = con.prepareStatement("SELECT * FROM roles " + where + " ORDER BY id" , ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            
            for (int i = 0; i < filters.size(); i++)
            {
              String filter_value = filters.get(i).get("value");
              stmt.setString(i+1, filter_value);
            }
            
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = new String[21];
                temp[0] = rs.getString("id");
                temp[1] = rs.getString("name");
                temp[2] = rs.getString("description");
                temp[3] = rs.getString("created");
                temp[4] = rs.getString("created_by_id");
                temp[5] = rs.getString("administration");  //true or false
                temp[6] = rs.getString("manager");   //true or false
                temp[7] = rs.getString("financial");//create,read,update,delete  
                temp[8] = rs.getString("incident"); //create,read,update,delete
                temp[9] = rs.getString("request"); //create,read,update,delete
                temp[10] = rs.getString("contact"); //create,read,update,delete
                temp[11] = rs.getString("cx"); //create,read,update,delete
                temp[12] = rs.getString("survey");//create,read,update,delete  
                temp[13] = rs.getString("sla");//create,read,update,delete  
                temp[14] = rs.getString("project"); //create,read,update,delete                        
                temp[15] = rs.getString("job"); //create,read,update,delete
                temp[16] = rs.getString("metric"); //create,read,update,delete
                temp[17] = rs.getString("asset"); //create,read,update,delete
                temp[18] = rs.getString("user"); //create,read,update,delete
                temp[19] = rs.getString("self_service"); //create,read,update,delete
                temp[20] = rs.getString("problem"); //create,read,update,delete
                return_arraylist.add(temp);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_roles.all:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_roles.all:=" + exc);
	}
        return return_arraylist;
    }   
    public static String all_roles_for_user_id(Connection con, String user_id)
    {
        String return_string = "";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT user_roles.user_id , roles.* FROM user_roles INNER JOIN roles ON user_roles.role_id=roles.id WHERE user_roles.user_id=?");
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                if(return_string.equals(""))
                {
                    return_string = rs.getString("name");
                }
                else
                {
                    return_string = return_string + " , " + rs.getString("name");
                }
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_roles.all_roles_for_user_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_roles.all_roles_for_user_id:=" + exc);
	}
        return return_string;
    }
    public static ArrayList<String[]> roles_for_user_id(Connection con, String user_id)
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        ArrayList<String[]>return_arraylist= new ArrayList();
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT user_roles.user_id , roles.* FROM user_roles INNER JOIN roles ON user_roles.role_id=roles.id WHERE user_roles.user_id=?");
            stmt.setString(1, user_id);
            //System.out.println("stmt=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                //System.out.println("found a role");
                String role[] = new String[22];
                role[0] = rs.getString("user_roles.user_id");
                role[1] = rs.getString("roles.id");
                role[2] = rs.getString("roles.name");
                role[3] = rs.getString("roles.description");
                try
                {
                    role[4] = timestamp_format.format(rs.getTimestamp("roles.created"));
                }
                catch(Exception e)
                {
                    role[4] = "";
                }
                role[5] = rs.getString("roles.created_by_id");
                role[6] = rs.getString("roles.administration");
                role[7] = rs.getString("roles.manager");
                role[8] = rs.getString("roles.financial");
                role[9] = rs.getString("roles.incident");
                role[10] = rs.getString("roles.request");
                role[11] = rs.getString("roles.contact");
                role[12] = rs.getString("roles.cx");
                role[13] = rs.getString("roles.survey");
                role[14] = rs.getString("roles.sla");
                role[15] = rs.getString("roles.project");
                role[16] = rs.getString("roles.job");  
                role[17] = rs.getString("roles.metric");  
                role[18] = rs.getString("roles.asset");  
                role[19] = rs.getString("roles.user");  
                role[20] = rs.getString("roles.self_service");                  
                role[21] = rs.getString("roles.problem");                  
                return_arraylist.add(role);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_roles.roles_for_user_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_roles.roles_for_user_id:=" + exc);
	}
        return return_arraylist;
    }
    public static String[] by_name(Connection con, String role_name) throws IOException, SQLException
    {
        //id=0 name=1 description=2 created=3 created_by_id=4 administration=5 manager=6 financial=7 incident=8 request=9 contact=10 cx=11 survey=12 sla=13 project=14 job=15 
        String returnString[] = {"0","--","--","--","--","false","false","none","none","none","none","none","none","none","none","none","none","none","none","none","none"};
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM roles WHERE name=?");
            stmt.setString(1, role_name);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                returnString[0] = rs.getString("id");
                returnString[1] = rs.getString("name");
                returnString[2] = rs.getString("description");
                returnString[3] = rs.getString("created");
                returnString[4] = rs.getString("created_by_id");
                returnString[5] = rs.getString("administration");  //true or false
                returnString[6] = rs.getString("manager");   //true or false
                returnString[7] = rs.getString("financial");//create,read,update,delete  
                returnString[8] = rs.getString("incident"); //create,read,update,delete
                returnString[9] = rs.getString("request"); //create,read,update,delete
                returnString[10] = rs.getString("contact"); //create,read,update,delete
                returnString[11] = rs.getString("cx"); //create,read,update,delete
                returnString[12] = rs.getString("survey");//create,read,update,delete  
                returnString[13] = rs.getString("sla");//create,read,update,delete  
                returnString[14] = rs.getString("project"); //create,read,update,delete                        
                returnString[15] = rs.getString("job"); //create,read,update,delete
                returnString[16] = rs.getString("metric"); //create,read,update,delete
                returnString[17] = rs.getString("asset"); //create,read,update,delete
                returnString[18] = rs.getString("user"); //create,read,update,delete
                returnString[19] = rs.getString("self_service"); //create,read,update,delete
                returnString[20] = rs.getString("problem"); //create,read,update,delete
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_roles.by_name:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_roles.by_name:=" + exc);
	}
        return returnString;
    }  
    public static boolean has_login(ArrayList<String[]> user_roles)
    {
        boolean has_login = false;
        try
        {
            for(int a = 0; a < user_roles.size();a++)
            {
                String role[] = user_roles.get(a);

                /*role[6] = rs.getString("roles.administration");
                role[7] = rs.getString("roles.manager");
                role[8] = rs.getString("roles.financial");
                role[9] = rs.getString("roles.incident");
                role[10] = rs.getString("roles.request");
                role[11] = rs.getString("roles.contact");
                role[12] = rs.getString("roles.cx");
                role[13] = rs.getString("roles.survey");
                role[14] = rs.getString("roles.sla");
                role[15] = rs.getString("roles.project");
                role[16] = rs.getString("roles.job");  
                role[17] = rs.getString("roles.metric");  
                role[18] = rs.getString("roles.asset");  
                role[19] = rs.getString("roles.user");  
                role[20] = rs.getString("roles.self_service");  */
                if(role[6].equalsIgnoreCase("true"))
                {
                    has_login = true;
                }
                else if(role[7].equalsIgnoreCase("true"))
                {
                    has_login = true;
                }
                else if(!role[7].equalsIgnoreCase("none"))
                {
                    has_login = true;
                }
                else if(!role[8].equalsIgnoreCase("none"))
                {
                    has_login = true;
                }
                else if(!role[9].equalsIgnoreCase("none"))
                {
                    has_login = true;
                }
                else if(!role[10].equalsIgnoreCase("none"))
                {
                    has_login = true;
                }
                else if(!role[11].equalsIgnoreCase("none"))
                {
                    has_login = true;
                }
                else if(!role[12].equalsIgnoreCase("none"))
                {
                    has_login = true;
                }
                else if(!role[13].equalsIgnoreCase("none"))
                {
                    has_login = true;
                }
                else if(!role[14].equalsIgnoreCase("none"))
                {
                    has_login = true;
                }
                else if(!role[15].equalsIgnoreCase("none"))
                {
                    has_login = true;
                }
                else if(!role[16].equalsIgnoreCase("none"))
                {
                    has_login = true;
                }
                else if(!role[17].equalsIgnoreCase("none"))
                {
                    has_login = true;
                }
                else if(!role[18].equalsIgnoreCase("none"))
                {
                    has_login = true;
                }
                else if(!role[19].equalsIgnoreCase("none"))
                {
                    has_login = true;
                }
                else if(!role[20].equalsIgnoreCase("none"))
                {
                    has_login = true;
                }
            }
        }
        catch(Exception e)
        {
            System.out.println("ERROR Exception get_roles.by_name:=" + e);
        }
        return has_login;
    }
}
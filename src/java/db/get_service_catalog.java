//Copyright 2020 XaSystems, Inc. , All rights reserved.
package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Campbellr_2
 */
public class get_service_catalog
{
    private static Logger logger = LogManager.getLogger();

    public static ArrayList<String[]> service_catalog_all(Connection con)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM service_catalog");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_catalog[] = {"","","","","",""};
                this_catalog[0] = rs.getString("id");
                this_catalog[1] = rs.getString("name");
                this_catalog[2] = rs.getString("description");
                this_catalog[3] = rs.getString("state");
                this_catalog[4] = rs.getString("catalog_type");
                this_catalog[5] = rs.getString("icon");
                return_arraylist.add(this_catalog);
            }
            stmt.close();
        }
        catch(SQLException ex)
        {
            logger.error("ERROR SQLException get_service_catalog.service_catalog_all:=" + ex);
        }
        catch(Exception exc)
        {
            logger.error("ERROR Exception get_service_catalog.service_catalog_all:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> service_catalog_by_state(Connection con, String state)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT service_catalog.*,t2.items_count FROM service_catalog LEFT JOIN (SELECT catalog_id,count(id) as items_count FROM service_catalog_item WHERE service_catalog_item.state = ? GROUP BY catalog_id) t2 ON t2.catalog_id = service_catalog.id WHERE service_catalog.state = ?");
            stmt.setString(1, state);
            stmt.setString(2, state);
            ResultSet rs = stmt.executeQuery();            
            while(rs.next())
            {
                String this_catalog[] = {"","","","","","","", ""};
                this_catalog[0] = rs.getString("id");
                this_catalog[1] = rs.getString("name");
                this_catalog[2] = rs.getString("description");
                this_catalog[3] = rs.getString("state");
                this_catalog[4] = rs.getString("catalog_type");
                this_catalog[5] = rs.getString("icon");
                this_catalog[6] = rs.getString("items_count");
                this_catalog[7] = rs.getString("image");
                return_arraylist.add(this_catalog);
            }
            stmt.close();
        }
        catch(SQLException ex)
        {
            logger.error("ERROR SQLException get_service_catalog.service_catalog_by_state:=" + ex);
        }
        catch(Exception exc)
        {
            logger.error("ERROR Exception get_service_catalog.service_catalog_by_state:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> service_catalog_by_type_and_state(Connection con, String type, String state)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM service_catalog WHERE type=? AND state=?");
            stmt.setString(1, type);
            stmt.setString(2, state);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_catalog[] = {"","","","","",""};
                this_catalog[0] = rs.getString("id");
                this_catalog[1] = rs.getString("name");
                this_catalog[2] = rs.getString("description");
                this_catalog[3] = rs.getString("state");
                this_catalog[4] = rs.getString("catalog_type");
                this_catalog[5] = rs.getString("icon");
                return_arraylist.add(this_catalog);
            }
            stmt.close();
        }
        catch(SQLException ex)
        {
            logger.error("ERROR SQLException get_service_catalog.service_catalog_by_state:=" + ex);
        }
        catch(Exception exc)
        {
            logger.error("ERROR Exception get_service_catalog.service_catalog_by_state:=" + exc);
	}
        return return_arraylist;
    }
    public static String[] service_catalog_by_id(Connection con, String id)
    {
        String[] return_array = {"","","","","","", ""};
        PreparedStatement stmt;
        try
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM service_catalog WHERE id=?");
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_array[0] = rs.getString("id");
                return_array[1] = rs.getString("name");
                return_array[2] = rs.getString("description");
                return_array[3] = rs.getString("state");
                return_array[4] = rs.getString("catalog_type");
                return_array[5] = rs.getString("icon");
                return_array[6] = rs.getString("image");
            }
            stmt.close();
        }
        catch(SQLException ex)
        {
            logger.error("ERROR SQLException get_service_catalog.service_catalog_by_id:=" + ex);
        }
        catch(Exception exc)
        {
            logger.error("ERROR Exception get_service_catalog.service_catalog_by_id:=" + exc);
	}
        return return_array;
    }
    public static ArrayList<String[]> service_catalog_items_all(Connection con)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT service_catalog_item.*,service_catalog.name FROM service_catalog_item INNER JOIN service_catalog ON service_catalog.id = service_catalog_item.catalog_id");
            
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_catalog_item[] = {"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};
                this_catalog_item[0] = rs.getString("service_catalog_item.id");
                this_catalog_item[1] = rs.getString("service_catalog_item.catalog_id");
                this_catalog_item[2] = rs.getString("service_catalog_item.name");
                this_catalog_item[3] = rs.getString("service_catalog_item.description");
                this_catalog_item[4] = rs.getString("service_catalog_item.category");
                this_catalog_item[5] = rs.getString("service_catalog_item.state");
                this_catalog_item[6] = rs.getString("service_catalog_item.approver_list");
                this_catalog_item[7] = rs.getString("service_catalog_item.critical_fixed_cost");
                this_catalog_item[8] = rs.getString("service_catalog_item.critical_recurring_cost");
                this_catalog_item[9] = rs.getString("service_catalog_item.critical_recurring_cost_frequency");
                this_catalog_item[10] = rs.getString("service_catalog_item.critical_delivery_time");
                this_catalog_item[11] = rs.getString("service_catalog_item.critical_delivery_time_unit");
                this_catalog_item[12] = rs.getString("service_catalog_item.critical_schedule_id");
                this_catalog_item[13] = rs.getString("service_catalog_item.critical_delivery_group_id");
                this_catalog_item[14] = rs.getString("service_catalog_item.high_fixed_cost");
                this_catalog_item[15] = rs.getString("service_catalog_item.high_recurring_cost");
                this_catalog_item[16] = rs.getString("service_catalog_item.high_recurring_cost_frequency");
                this_catalog_item[17] = rs.getString("service_catalog_item.high_delivery_time");
                this_catalog_item[18] = rs.getString("service_catalog_item.high_delivery_time_unit");
                this_catalog_item[19] = rs.getString("service_catalog_item.high_schedule_id");
                this_catalog_item[20] = rs.getString("service_catalog_item.high_delivery_group_id");
                this_catalog_item[21] = rs.getString("service_catalog_item.medium_fixed_cost");
                this_catalog_item[22] = rs.getString("service_catalog_item.medium_recurring_cost");
                this_catalog_item[23] = rs.getString("service_catalog_item.medium_recurring_cost_frequency");
                this_catalog_item[24] = rs.getString("service_catalog_item.medium_delivery_time");
                this_catalog_item[25] = rs.getString("service_catalog_item.medium_delivery_time_unit");
                this_catalog_item[26] = rs.getString("service_catalog_item.medium_schedule_id");
                this_catalog_item[27] = rs.getString("service_catalog_item.medium_delivery_group_id");
                this_catalog_item[28] = rs.getString("service_catalog_item.low_fixed_cost");
                this_catalog_item[29] = rs.getString("service_catalog_item.low_recurring_cost");
                this_catalog_item[30] = rs.getString("service_catalog_item.low_recurring_cost_frequency");
                this_catalog_item[31] = rs.getString("service_catalog_item.low_delivery_time");
                this_catalog_item[32] = rs.getString("service_catalog_item.low_delivery_time_unit");
                this_catalog_item[33] = rs.getString("service_catalog_item.low_schedule_id");
                this_catalog_item[34] = rs.getString("service_catalog_item.low_delivery_group_id");
                this_catalog_item[35] = rs.getString("service_catalog_item.required_user_input");
                this_catalog_item[36] = rs.getString("service_catalog_item.optional_user_input");
                this_catalog_item[37] = rs.getString("service_catalog.name");  
                return_arraylist.add(this_catalog_item);
            }
            stmt.close();
        }
        catch(SQLException ex)
        {
            logger.error("ERROR SQLException get_service_catalog.service_catalog_items_all:=" + ex);
        }
        catch(Exception exc)
        {
            logger.error("ERROR Exception get_service_catalog.service_catalog_items_all:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> service_catalog_items_by_state(Connection con, String state)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            System.out.println("start by state");
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT service_catalog_item.*,service_catalog.name FROM service_catalog_item INNER JOIN service_catalog ON service_catalog.id = service_catalog_item.catalog_id WHERE service_catalog_item.state=?");
            stmt.setString(1, state);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_catalog_item[] = {"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};
                this_catalog_item[0] = rs.getString("service_catalog_item.id");
                this_catalog_item[1] = rs.getString("service_catalog_item.catalog_id");
                this_catalog_item[2] = rs.getString("service_catalog_item.name");
                this_catalog_item[3] = rs.getString("service_catalog_item.description");
                this_catalog_item[4] = rs.getString("service_catalog_item.category");
                this_catalog_item[5] = rs.getString("service_catalog_item.state");
                this_catalog_item[6] = rs.getString("service_catalog_item.approver_list");
                this_catalog_item[7] = rs.getString("service_catalog_item.critical_fixed_cost");
                this_catalog_item[8] = rs.getString("service_catalog_item.critical_recurring_cost");
                this_catalog_item[9] = rs.getString("service_catalog_item.critical_recurring_cost_frequency");
                this_catalog_item[10] = rs.getString("service_catalog_item.critical_delivery_time");
                this_catalog_item[11] = rs.getString("service_catalog_item.critical_delivery_time_unit");
                this_catalog_item[12] = rs.getString("service_catalog_item.critical_schedule_id");
                this_catalog_item[13] = rs.getString("service_catalog_item.critical_delivery_group_id");
                this_catalog_item[14] = rs.getString("service_catalog_item.high_fixed_cost");
                this_catalog_item[15] = rs.getString("service_catalog_item.high_recurring_cost");
                this_catalog_item[16] = rs.getString("service_catalog_item.high_recurring_cost_frequency");
                this_catalog_item[17] = rs.getString("service_catalog_item.high_delivery_time");
                this_catalog_item[18] = rs.getString("service_catalog_item.high_delivery_time_unit");
                this_catalog_item[19] = rs.getString("service_catalog_item.high_schedule_id");
                this_catalog_item[20] = rs.getString("service_catalog_item.high_delivery_group_id");
                this_catalog_item[21] = rs.getString("service_catalog_item.medium_fixed_cost");
                this_catalog_item[22] = rs.getString("service_catalog_item.medium_recurring_cost");
                this_catalog_item[23] = rs.getString("service_catalog_item.medium_recurring_cost_frequency");
                this_catalog_item[24] = rs.getString("service_catalog_item.medium_delivery_time");
                this_catalog_item[25] = rs.getString("service_catalog_item.medium_delivery_time_unit");
                this_catalog_item[26] = rs.getString("service_catalog_item.medium_schedule_id");
                this_catalog_item[27] = rs.getString("service_catalog_item.medium_delivery_group_id");
                this_catalog_item[28] = rs.getString("service_catalog_item.low_fixed_cost");
                this_catalog_item[29] = rs.getString("service_catalog_item.low_recurring_cost");
                this_catalog_item[30] = rs.getString("service_catalog_item.low_recurring_cost_frequency");
                this_catalog_item[31] = rs.getString("service_catalog_item.low_delivery_time");
                this_catalog_item[32] = rs.getString("service_catalog_item.low_delivery_time_unit");
                this_catalog_item[33] = rs.getString("service_catalog_item.low_schedule_id");
                this_catalog_item[34] = rs.getString("service_catalog_item.low_delivery_group_id");
                this_catalog_item[35] = rs.getString("service_catalog_item.required_user_input");
                this_catalog_item[36] = rs.getString("service_catalog_item.optional_user_input");
                this_catalog_item[37] = rs.getString("service_catalog.name");  
                return_arraylist.add(this_catalog_item);
            }
            stmt.close();
        }
        catch(SQLException ex)
        {
            logger.error("ERROR SQLException get_service_catalog.service_catalog_items_by_state:=" + ex);
        }
        catch(Exception exc)
        {
            logger.error("ERROR Exception get_service_catalog.service_catalog_items_by_state:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> categories_by_catalog_id_and_state(Connection con, String catalog_id, String state)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
//            System.out.println("start by state");
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT category,count(category) as items_count FROM service_catalog_item WHERE state=? AND catalog_id = ? GROUP BY category");
            stmt.setString(1, state);
            stmt.setString(2, catalog_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String category[] = {"",""};
                category[0] = rs.getString("category");
                category[1] = rs.getString("items_count");
                return_arraylist.add(category);
            }
            stmt.close();
        }
        catch(SQLException ex)
        {
            logger.error("ERROR SQLException get_service_catalog.categories_by_catalog_id_and_state:=" + ex);
        }
        catch(Exception exc)
        {
            logger.error("ERROR Exception get_service_catalog.categories_by_catalog_id_and_state:=" + exc);
	}
        return return_arraylist;
    }
    public static String[] service_catalog_items_by_id(Connection con, String id)
    {
        String[] return_array = {"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};
        PreparedStatement stmt;
        try
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT service_catalog_item.*,service_catalog.name FROM service_catalog_item INNER JOIN service_catalog ON service_catalog.id = service_catalog_item.catalog_id WHERE service_catalog_item.id=?");
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_array[0] = rs.getString("service_catalog_item.id");
                return_array[1] = rs.getString("service_catalog_item.catalog_id");
                return_array[2] = rs.getString("service_catalog_item.name");
                return_array[3] = rs.getString("service_catalog_item.description");
                return_array[4] = rs.getString("service_catalog_item.category");
                return_array[5] = rs.getString("service_catalog_item.state");
                return_array[6] = rs.getString("service_catalog_item.approver_list");
                return_array[7] = rs.getString("service_catalog_item.critical_fixed_cost");
                return_array[8] = rs.getString("service_catalog_item.critical_recurring_cost");
                return_array[9] = rs.getString("service_catalog_item.critical_recurring_cost_frequency");
                return_array[10] = rs.getString("service_catalog_item.critical_delivery_time");
                return_array[11] = rs.getString("service_catalog_item.critical_delivery_time_unit");
                return_array[12] = rs.getString("service_catalog_item.critical_schedule_id");
                return_array[13] = rs.getString("service_catalog_item.critical_delivery_group_id");
                return_array[14] = rs.getString("service_catalog_item.high_fixed_cost");
                return_array[15] = rs.getString("service_catalog_item.high_recurring_cost");
                return_array[16] = rs.getString("service_catalog_item.high_recurring_cost_frequency");
                return_array[17] = rs.getString("service_catalog_item.high_delivery_time");
                return_array[18] = rs.getString("service_catalog_item.high_delivery_time_unit");
                return_array[19] = rs.getString("service_catalog_item.high_schedule_id");
                return_array[20] = rs.getString("service_catalog_item.high_delivery_group_id");
                return_array[21] = rs.getString("service_catalog_item.medium_fixed_cost");
                return_array[22] = rs.getString("service_catalog_item.medium_recurring_cost");
                return_array[23] = rs.getString("service_catalog_item.medium_recurring_cost_frequency");
                return_array[24] = rs.getString("service_catalog_item.medium_delivery_time");
                return_array[25] = rs.getString("service_catalog_item.medium_delivery_time_unit");
                return_array[26] = rs.getString("service_catalog_item.medium_schedule_id");
                return_array[27] = rs.getString("service_catalog_item.medium_delivery_group_id");
                return_array[28] = rs.getString("service_catalog_item.low_fixed_cost");
                return_array[29] = rs.getString("service_catalog_item.low_recurring_cost");
                return_array[30] = rs.getString("service_catalog_item.low_recurring_cost_frequency");
                return_array[31] = rs.getString("service_catalog_item.low_delivery_time");
                return_array[32] = rs.getString("service_catalog_item.low_delivery_time_unit");
                return_array[33] = rs.getString("service_catalog_item.low_schedule_id");
                return_array[34] = rs.getString("service_catalog_item.low_delivery_group_id");
                return_array[35] = rs.getString("service_catalog_item.required_user_input");
                return_array[36] = rs.getString("service_catalog_item.optional_user_input");
                return_array[37] = rs.getString("service_catalog.name"); 
                return_array[38] = rs.getString("service_catalog_item.image"); 
            }
            stmt.close();
        }
        catch(SQLException ex)
        {
            logger.error("ERROR SQLException get_service_catalog.service_catalog_items_by_id:=" + ex);
        }
        catch(Exception exc)
        {
            logger.error("ERROR Exception get_service_catalog.service_catalog_items_by_id:=" + exc);
	}
        return return_array;
    }
    public static String[] service_catalog_items_by_category(Connection con, String category)
    {
        String[] return_array = {"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};
        PreparedStatement stmt;
        try
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT service_catalog_item.*,service_catalog.name FROM service_catalog_item INNER JOIN service_catalog ON service_catalog.id = service_catalog_item.catalog_id WHERE service_catalog_item.category=?");
            stmt.setString(1, category);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_array[0] = rs.getString("service_catalog_item.id");
                return_array[1] = rs.getString("service_catalog_item.catalog_id");
                return_array[2] = rs.getString("service_catalog_item.name");
                return_array[3] = rs.getString("service_catalog_item.description");
                return_array[4] = rs.getString("service_catalog_item.category");
                return_array[5] = rs.getString("service_catalog_item.state");
                return_array[6] = rs.getString("service_catalog_item.approver_list");
                return_array[7] = rs.getString("service_catalog_item.critical_fixed_cost");
                return_array[8] = rs.getString("service_catalog_item.critical_recurring_cost");
                return_array[9] = rs.getString("service_catalog_item.critical_recurring_cost_frequency");
                return_array[10] = rs.getString("service_catalog_item.critical_delivery_time");
                return_array[11] = rs.getString("service_catalog_item.critical_delivery_time_unit");
                return_array[12] = rs.getString("service_catalog_item.critical_schedule_id");
                return_array[13] = rs.getString("service_catalog_item.critical_delivery_group_id");
                return_array[14] = rs.getString("service_catalog_item.high_fixed_cost");
                return_array[15] = rs.getString("service_catalog_item.high_recurring_cost");
                return_array[16] = rs.getString("service_catalog_item.high_recurring_cost_frequency");
                return_array[17] = rs.getString("service_catalog_item.high_delivery_time");
                return_array[18] = rs.getString("service_catalog_item.high_delivery_time_unit");
                return_array[19] = rs.getString("service_catalog_item.high_schedule_id");
                return_array[20] = rs.getString("service_catalog_item.high_delivery_group_id");
                return_array[21] = rs.getString("service_catalog_item.medium_fixed_cost");
                return_array[22] = rs.getString("service_catalog_item.medium_recurring_cost");
                return_array[23] = rs.getString("service_catalog_item.medium_recurring_cost_frequency");
                return_array[24] = rs.getString("service_catalog_item.medium_delivery_time");
                return_array[25] = rs.getString("service_catalog_item.medium_delivery_time_unit");
                return_array[26] = rs.getString("service_catalog_item.medium_schedule_id");
                return_array[27] = rs.getString("service_catalog_item.medium_delivery_group_id");
                return_array[28] = rs.getString("service_catalog_item.low_fixed_cost");
                return_array[29] = rs.getString("service_catalog_item.low_recurring_cost");
                return_array[30] = rs.getString("service_catalog_item.low_recurring_cost_frequency");
                return_array[31] = rs.getString("service_catalog_item.low_delivery_time");
                return_array[32] = rs.getString("service_catalog_item.low_delivery_time_unit");
                return_array[33] = rs.getString("service_catalog_item.low_schedule_id");
                return_array[34] = rs.getString("service_catalog_item.low_delivery_group_id");
                return_array[35] = rs.getString("service_catalog_item.required_user_input");
                return_array[36] = rs.getString("service_catalog_item.optional_user_input");
                return_array[37] = rs.getString("service_catalog.name"); 
            }
            stmt.close();
        }
        catch(SQLException ex)
        {
            logger.error("ERROR SQLException get_service_catalog.service_catalog_items_by_id:=" + ex);
        }
        catch(Exception exc)
        {
            logger.error("ERROR Exception get_service_catalog.service_catalog_items_by_id:=" + exc);
	}
        return return_array;
    }
    public static ArrayList<String[]> service_catalog_items_in_catalog(Connection con, String catalog_id)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT service_catalog_item.*,service_catalog.name FROM service_catalog_item INNER JOIN service_catalog ON service_catalog.id = service_catalog_item.catalog_id WHERE service_catalog_item.catalog_id=?");
            stmt.setString(1, catalog_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_catalog_item[] = {"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};
                this_catalog_item[0] = rs.getString("service_catalog_item.id");
                this_catalog_item[1] = rs.getString("service_catalog_item.catalog_id");
                this_catalog_item[2] = rs.getString("service_catalog_item.name");
                this_catalog_item[3] = rs.getString("service_catalog_item.description");
                this_catalog_item[4] = rs.getString("service_catalog_item.category");
                this_catalog_item[5] = rs.getString("service_catalog_item.state");
                this_catalog_item[6] = rs.getString("service_catalog_item.approver_list");
                this_catalog_item[7] = rs.getString("service_catalog_item.critical_fixed_cost");
                this_catalog_item[8] = rs.getString("service_catalog_item.critical_recurring_cost");
                this_catalog_item[9] = rs.getString("service_catalog_item.critical_recurring_cost_frequency");
                this_catalog_item[10] = rs.getString("service_catalog_item.critical_delivery_time");
                this_catalog_item[11] = rs.getString("service_catalog_item.critical_delivery_time_unit");
                this_catalog_item[12] = rs.getString("service_catalog_item.critical_schedule_id");
                this_catalog_item[13] = rs.getString("service_catalog_item.critical_delivery_group_id");
                this_catalog_item[14] = rs.getString("service_catalog_item.high_fixed_cost");
                this_catalog_item[15] = rs.getString("service_catalog_item.high_recurring_cost");
                this_catalog_item[16] = rs.getString("service_catalog_item.high_recurring_cost_frequency");
                this_catalog_item[17] = rs.getString("service_catalog_item.high_delivery_time");
                this_catalog_item[18] = rs.getString("service_catalog_item.high_delivery_time_unit");
                this_catalog_item[19] = rs.getString("service_catalog_item.high_schedule_id");
                this_catalog_item[20] = rs.getString("service_catalog_item.high_delivery_group_id");
                this_catalog_item[21] = rs.getString("service_catalog_item.medium_fixed_cost");
                this_catalog_item[22] = rs.getString("service_catalog_item.medium_recurring_cost");
                this_catalog_item[23] = rs.getString("service_catalog_item.medium_recurring_cost_frequency");
                this_catalog_item[24] = rs.getString("service_catalog_item.medium_delivery_time");
                this_catalog_item[25] = rs.getString("service_catalog_item.medium_delivery_time_unit");
                this_catalog_item[26] = rs.getString("service_catalog_item.medium_schedule_id");
                this_catalog_item[27] = rs.getString("service_catalog_item.medium_delivery_group_id");
                this_catalog_item[28] = rs.getString("service_catalog_item.low_fixed_cost");
                this_catalog_item[29] = rs.getString("service_catalog_item.low_recurring_cost");
                this_catalog_item[30] = rs.getString("service_catalog_item.low_recurring_cost_frequency");
                this_catalog_item[31] = rs.getString("service_catalog_item.low_delivery_time");
                this_catalog_item[32] = rs.getString("service_catalog_item.low_delivery_time_unit");
                this_catalog_item[33] = rs.getString("service_catalog_item.low_schedule_id");
                this_catalog_item[34] = rs.getString("service_catalog_item.low_delivery_group_id");
                this_catalog_item[35] = rs.getString("service_catalog_item.required_user_input");
                this_catalog_item[36] = rs.getString("service_catalog_item.optional_user_input");
                this_catalog_item[37] = rs.getString("service_catalog.name");  
                return_arraylist.add(this_catalog_item);
            }
            stmt.close();
        }
        catch(SQLException ex)
        {
            logger.error("ERROR SQLException get_service_catalog.service_catalog_items_in_catalog:=" + ex);
        }
        catch(Exception exc)
        {
            logger.error("ERROR Exception get_service_catalog.service_catalog_items_in_catalog:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> service_catalog_items_in_catalog_by_state(Connection con, String catalog_id, String state)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT service_catalog_item.*,service_catalog.name FROM service_catalog_item INNER JOIN service_catalog ON service_catalog.id = service_catalog_item.catalog_id WHERE service_catalog_item.catalog_id=? AND service_catalog_item.state=?");
            stmt.setString(1, catalog_id);
            stmt.setString(2, state);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_catalog_item[] = {"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};
                this_catalog_item[0] = rs.getString("service_catalog_item.id");
                this_catalog_item[1] = rs.getString("service_catalog_item.catalog_id");
                this_catalog_item[2] = rs.getString("service_catalog_item.name");
                this_catalog_item[3] = rs.getString("service_catalog_item.description");
                this_catalog_item[4] = rs.getString("service_catalog_item.category");
                this_catalog_item[5] = rs.getString("service_catalog_item.state");
                this_catalog_item[6] = rs.getString("service_catalog_item.approver_list");
                this_catalog_item[7] = rs.getString("service_catalog_item.critical_fixed_cost");
                this_catalog_item[8] = rs.getString("service_catalog_item.critical_recurring_cost");
                this_catalog_item[9] = rs.getString("service_catalog_item.critical_recurring_cost_frequency");
                this_catalog_item[10] = rs.getString("service_catalog_item.critical_delivery_time");
                this_catalog_item[11] = rs.getString("service_catalog_item.critical_delivery_time_unit");
                this_catalog_item[12] = rs.getString("service_catalog_item.critical_schedule_id");
                this_catalog_item[13] = rs.getString("service_catalog_item.critical_delivery_group_id");
                this_catalog_item[14] = rs.getString("service_catalog_item.high_fixed_cost");
                this_catalog_item[15] = rs.getString("service_catalog_item.high_recurring_cost");
                this_catalog_item[16] = rs.getString("service_catalog_item.high_recurring_cost_frequency");
                this_catalog_item[17] = rs.getString("service_catalog_item.high_delivery_time");
                this_catalog_item[18] = rs.getString("service_catalog_item.high_delivery_time_unit");
                this_catalog_item[19] = rs.getString("service_catalog_item.high_schedule_id");
                this_catalog_item[20] = rs.getString("service_catalog_item.high_delivery_group_id");
                this_catalog_item[21] = rs.getString("service_catalog_item.medium_fixed_cost");
                this_catalog_item[22] = rs.getString("service_catalog_item.medium_recurring_cost");
                this_catalog_item[23] = rs.getString("service_catalog_item.medium_recurring_cost_frequency");
                this_catalog_item[24] = rs.getString("service_catalog_item.medium_delivery_time");
                this_catalog_item[25] = rs.getString("service_catalog_item.medium_delivery_time_unit");
                this_catalog_item[26] = rs.getString("service_catalog_item.medium_schedule_id");
                this_catalog_item[27] = rs.getString("service_catalog_item.medium_delivery_group_id");
                this_catalog_item[28] = rs.getString("service_catalog_item.low_fixed_cost");
                this_catalog_item[29] = rs.getString("service_catalog_item.low_recurring_cost");
                this_catalog_item[30] = rs.getString("service_catalog_item.low_recurring_cost_frequency");
                this_catalog_item[31] = rs.getString("service_catalog_item.low_delivery_time");
                this_catalog_item[32] = rs.getString("service_catalog_item.low_delivery_time_unit");
                this_catalog_item[33] = rs.getString("service_catalog_item.low_schedule_id");
                this_catalog_item[34] = rs.getString("service_catalog_item.low_delivery_group_id");
                this_catalog_item[35] = rs.getString("service_catalog_item.required_user_input");
                this_catalog_item[36] = rs.getString("service_catalog_item.optional_user_input");
                this_catalog_item[37] = rs.getString("service_catalog.name");  
                this_catalog_item[38] = rs.getString("service_catalog_item.image");
                return_arraylist.add(this_catalog_item);
            }
            stmt.close();
        }
        catch(SQLException ex)
        {
            logger.error("ERROR SQLException get_service_catalog.service_catalog_items_in_catalog_by_state:=" + ex);
        }
        catch(Exception exc)
        {
            logger.error("ERROR Exception get_service_catalog.service_catalog_items_in_catalog_by_state:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> service_catalog_items_in_catalog_by_state_and_category(Connection con, String catalog_id, String state, String category)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT service_catalog_item.*,service_catalog.name FROM service_catalog_item INNER JOIN service_catalog ON service_catalog.id = service_catalog_item.catalog_id WHERE service_catalog_item.catalog_id=? AND service_catalog_item.state=? AND service_catalog_item.category = ?");
            stmt.setString(1, catalog_id);
            stmt.setString(2, state);
            stmt.setString(3, category);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_catalog_item[] = {"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};
                this_catalog_item[0] = rs.getString("service_catalog_item.id");
                this_catalog_item[1] = rs.getString("service_catalog_item.catalog_id");
                this_catalog_item[2] = rs.getString("service_catalog_item.name");
                this_catalog_item[3] = rs.getString("service_catalog_item.description");
                this_catalog_item[4] = rs.getString("service_catalog_item.category");
                this_catalog_item[5] = rs.getString("service_catalog_item.state");
                this_catalog_item[6] = rs.getString("service_catalog_item.approver_list");
                this_catalog_item[7] = rs.getString("service_catalog_item.critical_fixed_cost");
                this_catalog_item[8] = rs.getString("service_catalog_item.critical_recurring_cost");
                this_catalog_item[9] = rs.getString("service_catalog_item.critical_recurring_cost_frequency");
                this_catalog_item[10] = rs.getString("service_catalog_item.critical_delivery_time");
                this_catalog_item[11] = rs.getString("service_catalog_item.critical_delivery_time_unit");
                this_catalog_item[12] = rs.getString("service_catalog_item.critical_schedule_id");
                this_catalog_item[13] = rs.getString("service_catalog_item.critical_delivery_group_id");
                this_catalog_item[14] = rs.getString("service_catalog_item.high_fixed_cost");
                this_catalog_item[15] = rs.getString("service_catalog_item.high_recurring_cost");
                this_catalog_item[16] = rs.getString("service_catalog_item.high_recurring_cost_frequency");
                this_catalog_item[17] = rs.getString("service_catalog_item.high_delivery_time");
                this_catalog_item[18] = rs.getString("service_catalog_item.high_delivery_time_unit");
                this_catalog_item[19] = rs.getString("service_catalog_item.high_schedule_id");
                this_catalog_item[20] = rs.getString("service_catalog_item.high_delivery_group_id");
                this_catalog_item[21] = rs.getString("service_catalog_item.medium_fixed_cost");
                this_catalog_item[22] = rs.getString("service_catalog_item.medium_recurring_cost");
                this_catalog_item[23] = rs.getString("service_catalog_item.medium_recurring_cost_frequency");
                this_catalog_item[24] = rs.getString("service_catalog_item.medium_delivery_time");
                this_catalog_item[25] = rs.getString("service_catalog_item.medium_delivery_time_unit");
                this_catalog_item[26] = rs.getString("service_catalog_item.medium_schedule_id");
                this_catalog_item[27] = rs.getString("service_catalog_item.medium_delivery_group_id");
                this_catalog_item[28] = rs.getString("service_catalog_item.low_fixed_cost");
                this_catalog_item[29] = rs.getString("service_catalog_item.low_recurring_cost");
                this_catalog_item[30] = rs.getString("service_catalog_item.low_recurring_cost_frequency");
                this_catalog_item[31] = rs.getString("service_catalog_item.low_delivery_time");
                this_catalog_item[32] = rs.getString("service_catalog_item.low_delivery_time_unit");
                this_catalog_item[33] = rs.getString("service_catalog_item.low_schedule_id");
                this_catalog_item[34] = rs.getString("service_catalog_item.low_delivery_group_id");
                this_catalog_item[35] = rs.getString("service_catalog_item.required_user_input");
                this_catalog_item[36] = rs.getString("service_catalog_item.optional_user_input");
                this_catalog_item[37] = rs.getString("service_catalog.name");  
                this_catalog_item[38] = rs.getString("service_catalog_item.image");
                return_arraylist.add(this_catalog_item);
            }
            stmt.close();
        }
        catch(SQLException ex)
        {
            logger.error("ERROR SQLException get_service_catalog.service_catalog_items_in_catalog_by_state:=" + ex);
        }
        catch(Exception exc)
        {
            logger.error("ERROR Exception get_service_catalog.service_catalog_items_in_catalog_by_state:=" + exc);
	}
        return return_arraylist;
    }
}

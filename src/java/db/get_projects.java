/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class get_projects 
{
    private static Logger logger = LogManager.getLogger();
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
    private static SimpleDateFormat form_format = new SimpleDateFormat("HH:mm / dd-MMM-YY"); //10:42 / 24-Jan-20
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   

    public static int all_filtered_by_query_count(Connection con, Map<String, String> filters, String add_query) throws IOException, SQLException
    {
        int count = 0;
        PreparedStatement stmt;
        String query_filter = "";

        try 
        {

            for (Entry<String, String> entry : filters.entrySet()) {
                query_filter = query_filter + (query_filter.equals("") ? "" : " AND ") + " projects." + entry.getKey() + " = ? ";          
            }
            if (!add_query.equals("")){
                query_filter = query_filter + (query_filter.equals("") ? "" : " AND ") + add_query;
            }
            String query = "SELECT count(*) as counter FROM projects " + ( query_filter.equals("") ? "" : " WHERE " + query_filter);

            stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);

            int parameters_count = 0;
            for (Entry<String, String> entry : filters.entrySet()) {
                parameters_count++;
                stmt.setString(parameters_count, entry.getValue());
            }

//                System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("counter");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.all_filtered__by_query_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.all_filtered_by_query_count:=" + exc);
	}
        return count;
    }
    public static ArrayList<String[]> all_limited_filtered_by_query(Connection con, int starting_record, int fetch_size, String query) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        List<String> wheres = new ArrayList<>();
        PreparedStatement stmt;

        try 
        {
            // Create a Statement Object
            query = "SELECT "
                + "projects.id,"
                + "projects.name,"
                + "projects.description,"
                + "projects.owner_group_id," 
                + "projects.owner_id," 
                + "projects.manager_id,"
                + "projects.tech_lead_id,"
                + "DATE_FORMAT(projects.scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(projects.actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(projects.scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(projects.actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "projects.status,"
                + "projects.priority,"
                + "projects.notes,"
                + "owners.username owner_username,owners.first owner_first,owners.mi owner_mi,owners.last owner_last,owners.phone_mobile owner_phone_mobile,"
                + "manager.username manager_username,manager.first manager_first,manager.mi manager_mi,manager.last manager_last,manager.phone_mobile manager_phone_mobile,"
                + "tech_lead.username tech_lead_username,tech_lead.first tech_lead_first,tech_lead.mi tech_lead_mi,tech_lead.last tech_lead_last,tech_lead.phone_mobile tech_lead_phone_mobile,"
                + "owner_groups.name owner_group_name "
                + "FROM projects "
                + "LEFT JOIN users as owners ON projects.owner_id = owners.id " 
                + "LEFT JOIN users as manager ON projects.manager_id = manager.id " 
                + "LEFT JOIN users as tech_lead ON projects.tech_lead_id = tech_lead.id " 
                + "LEFT JOIN `groups` owner_groups ON projects.owner_group_id = owner_groups.id " 
                + ( query.equals("") ? "" : " WHERE " + query) + " ORDER BY id DESC LIMIT ? , ?";
            stmt = con.prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);

            stmt.setInt(1, starting_record);
            stmt.setInt(2, fetch_size);
//            System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();
            return_array_list = process_project_resultset(rs);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.all_limited_filtered_by_query:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.all_limited_filtered_by_query:=" + exc);
	}
        return return_array_list;
    }

    public static ArrayList<String[]> all_limited_filtered_by_query(Connection con, int starting_record, int fetch_size, Map<String, String> filters, String order_column, String order_dir, String add_query) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        PreparedStatement stmt;
        String query_filter = "";

        try 
        {

            for (Entry<String, String> entry : filters.entrySet()) {
                query_filter = query_filter + (query_filter.equals("") ? "" : " AND ") + " projects." + entry.getKey() + " = ? ";          
            }
            if (!add_query.equals("")){
                query_filter = query_filter + (query_filter.equals("") ? "" : " AND ") + add_query;
            }

            // Create a Statement Object
            String query = "SELECT "
                + "projects.id,"
                + "projects.name,"
                + "projects.description,"
                + "projects.owner_group_id," 
                + "projects.owner_id," 
                + "projects.manager_id,"
                + "projects.tech_lead_id,"
                + "DATE_FORMAT(projects.scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(projects.actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(projects.scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(projects.actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "projects.status,"
                + "projects.priority,"
                + "projects.notes,"
                + "owners.username owner_username,owners.first owner_first,owners.mi owner_mi,owners.last owner_last,owners.phone_mobile owner_phone_mobile,"
                + "manager.username manager_username,manager.first manager_first,manager.mi manager_mi,manager.last manager_last,manager.phone_mobile manager_phone_mobile,"
                + "tech_lead.username tech_lead_username,tech_lead.first tech_lead_first,tech_lead.mi tech_lead_mi,tech_lead.last tech_lead_last,tech_lead.phone_mobile tech_lead_phone_mobile,"
                + "owner_groups.name owner_group_name "
                + "FROM projects "
                + "LEFT JOIN users as owners ON projects.owner_id = owners.id " 
                + "LEFT JOIN users as manager ON projects.manager_id = manager.id " 
                + "LEFT JOIN users as tech_lead ON projects.tech_lead_id = tech_lead.id " 
                + "LEFT JOIN `groups` owner_groups ON projects.owner_group_id = owner_groups.id " 
                + ( query_filter.equals("") ? "" : " WHERE " + query_filter);

            if (!order_column.equals(""))
            {
                query += " ORDER BY " + order_column + " " + order_dir;
            } else {
                query += " ORDER BY id DESC";
            }

            query += " LIMIT ? , ?";

            stmt = con.prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            
            int parameters_count = 0;
            for (Entry<String, String> entry : filters.entrySet()) {
                parameters_count++;
                stmt.setString(parameters_count, entry.getValue());
            }

            stmt.setInt(parameters_count + 1, starting_record);
            stmt.setInt(parameters_count + 2, fetch_size);
            System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();
            return_array_list = process_project_resultset(rs);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.all_limited_filtered_by_query:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.all_limited_filtered_by_query:=" + exc);
	}
        return return_array_list;
    }

    public static ArrayList<String[]> get_attachments_by_project_id(Connection con, String project_id) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        PreparedStatement stmt;

        try 
        {

            // Create a Statement Object
            String query = "SELECT project_attachements.*,users.first,users.last from project_attachements LEFT JOIN users ON project_attachements.uploaded_by = users.id WHERE project_id = ?";
            stmt = con.prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, project_id);
//            System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();

            while(rs.next())
            {
                String attachment[] = new String[10];
                attachment[0] = rs.getString("id");
                attachment[1] = rs.getString("project_id");
                attachment[2] = rs.getString("uuid");
                attachment[3] = rs.getString("name_on_disk");
                attachment[4] = rs.getString("file_name");
                attachment[5] = rs.getString("file_date");
                attachment[6] = rs.getString("file_size");
                attachment[7] = rs.getString("rev");
                attachment[8] = rs.getString("uploaded_by");
                attachment[9] = rs.getString("first") + " " + rs.getString("last");
                return_array_list.add(attachment);
            }

            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.get_attachments_by_project_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.get_attachments_by_project_id:=" + exc);
	}
        return return_array_list;
    }

    public static ArrayList<String[]> get_attachments_by_project_task_id(Connection con, String project_id, String task_id) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        PreparedStatement stmt;

        try 
        {

            // Create a Statement Object
            String query = "SELECT project_task_attachements.*,users.first,users.last from project_task_attachements LEFT JOIN users ON project_task_attachements.uploaded_by = users.id WHERE project_id = ? AND task_id = ?";
            stmt = con.prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, project_id);
            stmt.setString(2, task_id);
//            System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();

            while(rs.next())
            {
                String attachment[] = new String[11];
                attachment[0] = rs.getString("id");
                attachment[1] = rs.getString("project_id");
                attachment[2] = rs.getString("task_id");
                attachment[3] = rs.getString("uuid");
                attachment[4] = rs.getString("name_on_disk");
                attachment[5] = rs.getString("file_name");
                attachment[6] = rs.getString("file_date");
                attachment[7] = rs.getString("file_size");
                attachment[8] = rs.getString("rev");
                attachment[9] = rs.getString("uploaded_by");
                attachment[10] = rs.getString("first") + " " + rs.getString("last");
                return_array_list.add(attachment);
            }

            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.get_attachments_by_project_task_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.get_attachments_by_project_task_id:=" + exc);
	}
        return return_array_list;
    }

    public static ArrayList<String[]> get_messages_by_project_task_id(Connection con, String project_id, String task_id, int start, int length) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        PreparedStatement stmt;

        try 
        {

            // Create a Statement Object
            String query = "SELECT "
                    + "messages.id,"
                    + "messages.project_id,"
                    + "messages.task_id,"
                    + "messages.sender_id,"
                    + "CONCAT(users.first, ' ', users.last) as sender_name,"
                    + "messages.content,"
                    + "DATE_FORMAT(messages.date_sent,'%Y-%m-%dT%H:%i:%s') as date_sent,"
                    + "messages.is_system,"
                    + "messages.saved,"
                    + "messages.distibuted,"
                    + "messages.seen,"
                    + "messages.deleted "
                + " FROM project_task_messages AS messages "
                + " LEFT JOIN users on messages.sender_id = users.id "
                + " WHERE project_id = ? AND task_id = ? " + (start > 0 ? "AND messages.id < ?" : "")
                + " ORDER BY id DESC"
                + " LIMIT ?";
            stmt = con.prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, project_id);
            stmt.setString(2, task_id);
            if (start > 0) {
                stmt.setInt(3, start);
                stmt.setInt(4, length);
            } else {
                stmt.setInt(3, length);
            }
//            System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();

            while(rs.next())
            {
                String message[] = new String[12];
                message[0] = rs.getString("id");
                message[1] = rs.getString("project_id");
                message[2] = rs.getString("task_id");
                message[3] = rs.getString("sender_id");
                message[4] = rs.getString("sender_name");
                message[5] = rs.getString("content");
                message[6] = rs.getString("date_sent");
                message[7] = rs.getString("is_system");
                message[8] = rs.getString("saved");
                message[9] = rs.getString("distibuted");
                message[10] = rs.getString("seen");
                message[11] = rs.getString("deleted");
                return_array_list.add(message);
            }

            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.get_messages_by_project_task_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.get_messages_by_project_task_id:=" + exc);
	}
        return return_array_list;
    }
    
    public static ArrayList<String[]> process_project_resultset(ResultSet rs, ArrayList<String[]> users, ArrayList<String[]> groups)
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        try
        {
            while(rs.next())
            {
                String project_info[] = new String[30];
                //init the array
                for(int a = 0; a < project_info.length;a++)
                {
                    project_info[a] = "--";
                }
                project_info[0] = rs.getString("id");
                project_info[1] = rs.getString("name");
                project_info[2] = rs.getString("description");
                project_info[3] = rs.getString("owner_group_id");
                project_info[4] = rs.getString("owner_id");
                project_info[5] = rs.getString("manager_id");
                project_info[6] = rs.getString("tech_lead_id");
                project_info[7] = rs.getString("scheduled_start_date");
                project_info[8] = rs.getString("actual_start_date");
                project_info[9] = rs.getString("scheduled_end_date");
                project_info[10] = rs.getString("actual_end_date");
                project_info[11] = rs.getString("status");
                project_info[12] = rs.getString("priority");
                project_info[13] = rs.getString("notes");                
                //LOOKUP owner_group info
                project_info[14] = "";
                for(int b = 0; b < groups.size(); b++)
                {
                    String this_group[] = groups.get(b);
                    if(this_group[0].equalsIgnoreCase(rs.getString("owner_group_id")))
                    {
                        project_info[14] = this_group[1];
                        b = groups.size();
                    }
                }
                //LOOKUP populate the owner info
                project_info[15] = "";
                project_info[16] = "";
                project_info[17] = "";
                project_info[18] = "";
                project_info[19] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("owner_id")))
                    {
                        project_info[15] = this_user[1];
                        project_info[16] = this_user[3];
                        project_info[17] = this_user[4];
                        project_info[18] = this_user[5];
                        project_info[19] = this_user[17];
                        a = users.size();
                    }
                }
                //LOOKUP populate the Manager info
                project_info[20] = "";
                project_info[21] = "";
                project_info[22] = "";
                project_info[23] = "";
                project_info[24] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("manager_id")))
                    {
                        project_info[20] = this_user[1];
                        project_info[21] = this_user[3];
                        project_info[22] = this_user[4];
                        project_info[23] = this_user[5];
                        project_info[24] = this_user[17];
                        a = users.size();
                    }
                }
                //LOOKUP populate the tech_lead info
                project_info[25] = "";
                project_info[26] = "";
                project_info[27] = "";
                project_info[28] = "";
                project_info[29] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("tech_lead_id")))
                    {
                        project_info[25] = this_user[1];
                        project_info[26] = this_user[3];
                        project_info[27] = this_user[4];
                        project_info[28] = this_user[5];
                        project_info[29] = this_user[17];
                        a = users.size();
                    }
                }
                return_array_list.add(project_info);
            }
        }
        catch(Exception e)
        {
            System.out.println("Exception in get_projects.process_project_resultset=" + e);
        }
        return return_array_list;
    }
    public static ArrayList<String[]> process_project_resultset(ResultSet rs)
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        try
        {
            while(rs.next())
            {
                String project_info[] = new String[30];
                //init the array
                for(int a = 0; a < project_info.length;a++)
                {
                    project_info[a] = "--";
                }
//                System.out.println("process_project_resultset processing " + rs.getString("id"));

                project_info[0] = rs.getString("id");
                project_info[1] = rs.getString("name");
                project_info[2] = rs.getString("description");
                project_info[3] = rs.getString("owner_group_id");
                project_info[4] = rs.getString("owner_id");
                project_info[5] = rs.getString("manager_id");
                project_info[6] = rs.getString("tech_lead_id");
                project_info[7] = rs.getString("scheduled_start_date");
                project_info[8] = rs.getString("actual_start_date");
                project_info[9] = rs.getString("scheduled_end_date");
                project_info[10] = rs.getString("actual_end_date");
                project_info[11] = rs.getString("status");
                project_info[12] = rs.getString("priority");
                project_info[13] = rs.getString("notes");                
                project_info[14] = rs.getString("owner_group_name");
                //LOOKUP populate the owner info
                project_info[15] = rs.getString("owner_username");
                project_info[16] = rs.getString("owner_first");
                project_info[17] = rs.getString("owner_mi");
                project_info[18] = rs.getString("owner_last");
                project_info[19] = rs.getString("owner_phone_mobile");
                //LOOKUP populate the Manager info
                project_info[20] = rs.getString("manager_username");
                project_info[21] = rs.getString("manager_first");
                project_info[22] = rs.getString("manager_mi");
                project_info[23] = rs.getString("manager_last");
                project_info[24] = rs.getString("manager_phone_mobile");
                //LOOKUP populate the tech_lead info
                project_info[25] = rs.getString("tech_lead_username");
                project_info[26] = rs.getString("tech_lead_first");
                project_info[27] = rs.getString("tech_lead_mi");
                project_info[28] = rs.getString("tech_lead_last");
                project_info[29] = rs.getString("tech_lead_phone_mobile");

                return_array_list.add(project_info);
            }
        }
        catch(Exception e)
        {
            System.out.println("Exception in get_projects.process_project_resultset=" + e);
            e.printStackTrace();
        }
//        System.out.println("process_project_resultset count " + return_array_list.size());
        return return_array_list;
    }
    public static ArrayList<String[]> process_project_task_resultset(ResultSet rs, ArrayList<String[]> projects, ArrayList<String[]> users, ArrayList<String[]> groups)
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        try
        {
            while(rs.next())
            {
                String project_task[] = new String[23];
                project_task[0] = rs.getString("id");
                project_task[1] = rs.getString("project_id");
                project_task[2] = rs.getString("name");
                project_task[3] = rs.getString("description");
                project_task[4] = rs.getString("assigned_group_id");
                project_task[5] = rs.getString("assigned_to_id");
                project_task[6] = rs.getString("priority");
                project_task[7] = rs.getString("status");
                project_task[8] = rs.getString("scheduled_start_date");
                project_task[9] = rs.getString("actual_start_date");
                project_task[10] = rs.getString("scheduled_end_date");
                project_task[11] = rs.getString("actual_end_date");
                project_task[12] = rs.getString("estimated_duration");
                project_task[13] = rs.getString("actual_duration");
                project_task[14] = rs.getString("notes");
                //LOOKUP assigned_group info
                project_task[15] = "";
                for(int b = 0; b < groups.size(); b++)
                {
                    String this_group[] = groups.get(b);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        project_task[15] = this_group[1];
                        b = groups.size();
                    }
                }
                //LOOKUP populate the assigned_to_id info
                project_task[16] = "";
                project_task[17] = "";
                project_task[18] = "";
                project_task[19] = "";
                project_task[20] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        project_task[16] = this_user[1];
                        project_task[17] = this_user[3];
                        project_task[18] = this_user[4];
                        project_task[19] = this_user[5];
                        project_task[20] = this_user[17];
                        a = users.size();
                    }
                }
                project_task[21] = "";
                project_task[22] = "";
                for(int a = 0; a < projects.size(); a++)
                {
                    String project[] = projects.get(a);
                    if(project[0].equalsIgnoreCase(rs.getString("project_id")))
                    {
                        project_task[21] = project[1];
                        project_task[22] = project[2];
                        a = projects.size();
                    }
                }
                return_array_list.add(project_task);
            }
        }
        catch(Exception e)
        {
            System.out.println("Exception in get_projects.process_project_task_resultset=" + e);
        }
        return return_array_list;
    }
    public static ArrayList<String[]> process_project_task_resultset(ResultSet rs)
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        try
        {
            while(rs.next())
            {
                String project_task[] = new String[23];
                project_task[0] = rs.getString("id");
                project_task[1] = rs.getString("project_id");
                project_task[2] = rs.getString("name");
                project_task[3] = rs.getString("description");
                project_task[4] = rs.getString("assigned_group_id");
                project_task[5] = rs.getString("assigned_to_id");
                project_task[6] = rs.getString("priority");
                project_task[7] = rs.getString("status");
                project_task[8] = rs.getString("scheduled_start_date");
                project_task[9] = rs.getString("actual_start_date");
                project_task[10] = rs.getString("scheduled_end_date");
                project_task[11] = rs.getString("actual_end_date");
                project_task[12] = rs.getString("estimated_duration");
                project_task[13] = rs.getString("actual_duration");
                project_task[14] = rs.getString("notes");
                //LOOKUP assigned_group info
                project_task[15] = rs.getString("assigned_group_name");
                //LOOKUP populate the assigned_to_id info
                project_task[16] = rs.getString("assigned_to_username");
                project_task[17] = rs.getString("assigned_to_first");
                project_task[18] = rs.getString("assigned_to_mi");
                project_task[19] = rs.getString("assigned_to_last");
                project_task[20] = rs.getString("assigned_to_phone_mobile");
                project_task[21] = rs.getString("project_name");
                project_task[22] = rs.getString("project_description");
                return_array_list.add(project_task);
            }
        }
        catch(Exception e)
        {
            System.out.println("Exception in get_projects.process_project_task_resultset=" + e);
        }
        return return_array_list;
    }
    
    public static int next_project_id(Connection con) throws IOException, SQLException
    {
        int return_int = 0;  
        String ps_query_string = "SELECT max(id) AS value FROM projects";        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
            }
            stmt.close();
            //add 1 to max
            return_int++;
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.next_project_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.next_project_id:=" + exc);
	}
        return return_int;
    }
    public static ArrayList<String[]> all(Connection con) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        String query = "SELECT " 
                + "id,"
                + "name,"
                + "description,"
                + "owner_group_id," 
                + "owner_id," 
                + "manager_id,"
                + "tech_lead_id,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "status,"
                + "priority,"
                + "notes "
                + "FROM projects";
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            return_array_list = process_project_resultset(rs, users, groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.all_open:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.all_open:=" + exc);
	}
        return return_array_list;
    }
    public static int next_project_task_id_for_project(Connection con, int project_id) throws IOException, SQLException
    {
        int return_int = 0;  
        String ps_query_string = "SELECT max(id) AS value FROM project_tasks";        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            
            stmt = con.prepareStatement(ps_query_string);
            stmt.setInt(1, project_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
            }
            stmt.close();
            //add 1 to max
            return_int++;
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.next_project_task_id_for_project:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.next_project_task_id_for_project:=" + exc);
	}
        return return_int;
    }
    public static String[] project_by_id(Connection con, String id) throws IOException, SQLException
    {
        String return_string[] = new String[30];
        //init the array
        for(int a = 0; a < return_string.length;a++)
        {
            return_string[a] = "--";
        }
        String ps_query_string = "SELECT " 
                + "id,"
                + "name,"
                + "description,"
                + "owner_group_id," 
                + "owner_id," 
                + "manager_id,"
                + "tech_lead_id,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "status,"
                + "priority,"
                + "notes "
                + "FROM projects WHERE id=?";
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_string[0] = rs.getString("id");
                return_string[1] = rs.getString("name");
                return_string[2] = rs.getString("description");
                return_string[3] = rs.getString("owner_group_id");
                return_string[4] = rs.getString("owner_id");
                return_string[5] = rs.getString("manager_id");
                return_string[6] = rs.getString("tech_lead_id");
                return_string[7] = rs.getString("scheduled_start_date");
                return_string[8] = rs.getString("actual_start_date");
                return_string[9] = rs.getString("scheduled_end_date");
                return_string[10] = rs.getString("actual_end_date");
                return_string[11] = rs.getString("status");
                return_string[12] = rs.getString("priority");
                return_string[13] = rs.getString("notes");
                
                //LOOKUP owner_group info
                return_string[14] = "";
                for(int b = 0; b < groups.size(); b++)
                {
                    String this_group[] = groups.get(b);
                    if(this_group[0].equalsIgnoreCase(rs.getString("owner_group_id")))
                    {
                        return_string[14] = this_group[1];
                        b = groups.size();
                    }
                }
                //LOOKUP populate the owner info
                return_string[15] = "";
                return_string[16] = "";
                return_string[17] = "";
                return_string[18] = "";
                return_string[19] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("owner_id")))
                    {
                        return_string[15] = this_user[1];
                        return_string[16] = this_user[3];
                        return_string[17] = this_user[4];
                        return_string[18] = this_user[5];
                        return_string[19] = this_user[17];
                        a = users.size();
                    }
                }
                //LOOKUP populate the Manager info
                return_string[20] = "";
                return_string[21] = "";
                return_string[22] = "";
                return_string[23] = "";
                return_string[24] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("manager_id")))
                    {
                        return_string[20] = this_user[1];
                        return_string[21] = this_user[3];
                        return_string[22] = this_user[4];
                        return_string[23] = this_user[5];
                        return_string[24] = this_user[17];
                        a = users.size();
                    }
                }
                //LOOKUP populate the tech_lead info
                return_string[25] = "";
                return_string[26] = "";
                return_string[27] = "";
                return_string[28] = "";
                return_string[29] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("tech_lead_id")))
                    {
                        return_string[25] = this_user[1];
                        return_string[26] = this_user[3];
                        return_string[27] = this_user[4];
                        return_string[28] = this_user[5];
                        return_string[29] = this_user[17];
                        a = users.size();
                    }
                }
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.project_by_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.project_by_id:=" + exc);
	}
        return return_string;
    }
    public static ArrayList <String[]> project_tasks_for_project(Connection con, String project_id) throws IOException, SQLException
    {
        ArrayList<String[]> return_array_list = new ArrayList();        
        String ps_query_string = "SELECT "
                + "tasks.id," 
                + "tasks.project_id,"
                + "tasks.name,"
                + "tasks.description,"
                + "tasks.assigned_group_id,"
                + "tasks.assigned_to_id,"
                + "tasks.priority,"
                + "tasks.status,"
                + "DATE_FORMAT(tasks.scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(tasks.actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(tasks.scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(tasks.actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "tasks.estimated_duration,"
                + "tasks.actual_duration,"
                + "tasks.notes, " 
                + "projects.name project_name,projects.description project_description, "
                + "assigned_to.username assigned_to_username,assigned_to.first assigned_to_first,assigned_to.mi assigned_to_mi,assigned_to.last assigned_to_last,assigned_to.phone_mobile assigned_to_phone_mobile, "
                + "assigned_groups.name assigned_group_name "
                + "FROM project_tasks as tasks "
                + "LEFT JOIN users as assigned_to ON tasks.assigned_to_id = assigned_to.id " 
                + "LEFT JOIN `groups` assigned_groups ON tasks.assigned_group_id = assigned_groups.id " 
                + "LEFT JOIN `projects` ON tasks.project_id = projects.id "
                + "WHERE project_id=? "; 
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, project_id);
            ResultSet rs = stmt.executeQuery();
            return_array_list = process_project_task_resultset(rs);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.project_tasks_for_project:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.project_tasks_for_project:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList <String[]> project_tasks_for_project(Connection con, String project_id, String[] status_filters) throws IOException, SQLException
    {
        ArrayList<String[]> return_array_list = new ArrayList();        
        String status_filter = "";
        if (status_filters != null && status_filters.length>0)
        {
            for (int i = 0; i < status_filters.length; i++)
            {
                status_filter = status_filter + (status_filter.equals("") ? "" : " OR ") + " tasks.status = ? ";
            }
        }
        String ps_query_string = "SELECT "
                + "tasks.id," 
                + "tasks.project_id,"
                + "tasks.name,"
                + "tasks.description,"
                + "tasks.assigned_group_id,"
                + "tasks.assigned_to_id,"
                + "tasks.priority,"
                + "tasks.status,"
                + "DATE_FORMAT(tasks.scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(tasks.actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(tasks.scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(tasks.actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "tasks.estimated_duration,"
                + "tasks.actual_duration,"
                + "tasks.notes, " 
                + "projects.name project_name,projects.description project_description, "
                + "assigned_to.username assigned_to_username,assigned_to.first assigned_to_first,assigned_to.mi assigned_to_mi,assigned_to.last assigned_to_last,assigned_to.phone_mobile assigned_to_phone_mobile, "
                + "assigned_groups.name assigned_group_name "
                + "FROM project_tasks as tasks "
                + "LEFT JOIN users as assigned_to ON tasks.assigned_to_id = assigned_to.id " 
                + "LEFT JOIN `groups` assigned_groups ON tasks.assigned_group_id = assigned_groups.id " 
                + "LEFT JOIN `projects` ON tasks.project_id = projects.id "
                + "WHERE project_id=? " + (status_filter.equals("") ? "" : " AND (" + status_filter + ")"); 
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, project_id);
            if (status_filters != null)
            {
                for (int i = 0; i < status_filters.length; i++)
                {
                    stmt.setString(i+2, status_filters[i]);
                }
            }

            ResultSet rs = stmt.executeQuery();
            return_array_list = process_project_task_resultset(rs);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.project_tasks_for_project:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.project_tasks_for_project:=" + exc);
	}
        return return_array_list;
    }
    public static String[] project_task_info(Connection con, String project_id, String task_id) throws IOException, SQLException
    {
        String task_info[] = {"","","","","","","","","","","","","","",""};
        
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        //String ps_query_string = "SELECT * FROM project_tasks WHERE project_id=? AND id=?";
        String ps_query_string = "SELECT id," 
                + "project_id,"
                + "name,"
                + "description,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "priority,"
                + "status,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "estimated_duration,"
                + "actual_duration,"
                + "notes " 
                + "FROM project_tasks WHERE project_id=? AND id=?";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, project_id);
            stmt.setString(2, task_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                task_info[0] = rs.getString("id");
                task_info[1] = rs.getString("project_id");
                task_info[2] = rs.getString("name");
                task_info[3] = rs.getString("description");
                task_info[4] = rs.getString("assigned_group_id");
                task_info[5] = rs.getString("assigned_to_id");
                task_info[6] = rs.getString("priority");
                task_info[7] = rs.getString("status");
                task_info[8] = rs.getString("scheduled_start_date");
                task_info[9] = rs.getString("actual_start_date");
                task_info[10] = rs.getString("scheduled_end_date");
                task_info[11] = rs.getString("actual_end_date");
                task_info[12] = rs.getString("estimated_duration");
                task_info[13] = rs.getString("actual_duration");
                task_info[14] = rs.getString("notes");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.project_task_info:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.project_task_info:=" + exc);
	}
        return task_info;
    }
    public static ArrayList<String[]> filtered_list(Connection con, String status) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        String query = "SELECT * FROM projects WHERE status=?";
        if(status.equalsIgnoreCase("all"))
        {
            query = "SELECT * FROM projects";
            String ps_query_string = "SELECT id," 
                + "project_id,"
                + "name,"
                + "description,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "priority,"
                + "status,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "estimated_duration,"
                + "actual_duration,"
                + "notes " 
                + "FROM project_tasks WHERE project_id=?";
        }
        else
        {
             query = "SELECT * FROM projects WHERE status='" + status + "'";
        }
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            //System.out.println("query=" + stmt);
            ResultSet rs = stmt.executeQuery();
            return_array_list = process_project_resultset(rs, users, groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.filtered_list:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.filtered_list:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> all_open(Connection con) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM projects WHERE status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%'";
        String query = "SELECT " 
                + "id,"
                + "name,"
                + "description,"
                + "owner_group_id," 
                + "owner_id," 
                + "manager_id,"
                + "tech_lead_id,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "status,"
                + "priority,"
                + "notes "
                + "FROM projects WHERE status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%'";
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            //System.out.println("query=" + stmt);
            ResultSet rs = stmt.executeQuery();
            return_array_list = process_project_resultset(rs, users, groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.all_open:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.all_open:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> all_open_assigned_to_id(Connection con, String user_id) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM projects WHERE (status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%') AND (owner_id = ? OR manager_id= ? OR tech_lead_id= ?)";
        String query = "SELECT " 
                + "id,"
                + "name,"
                + "description,"
                + "owner_group_id," 
                + "owner_id," 
                + "manager_id,"
                + "tech_lead_id,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "status,"
                + "priority,"
                + "notes "
                + "FROM projects WHERE (status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%') AND (owner_id = ? OR manager_id= ? OR tech_lead_id= ?)";
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, user_id);
            stmt.setString(2, user_id);
            stmt.setString(3, user_id);
            
            //System.out.println("all_open_assigned_to_id query=" + stmt);
            ResultSet rs = stmt.executeQuery();
            return_array_list = process_project_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.all_open_assigned_to_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.all_open_assigned_to_id:=" + exc);
	}
        return return_array_list;
    }
    public static int all_open_assigned_to_id_count(Connection con, String user_id) throws IOException, SQLException
    {
        int count = 0;
        String query = "SELECT count(*) AS project_count FROM projects WHERE (status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%') AND (owner_id = ? OR manager_id= ? OR tech_lead_id= ?)";
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, user_id);
            stmt.setString(2, user_id);
            stmt.setString(3, user_id);
            
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("project_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.all_open_assigned_to_id_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.all_open_assigned_to_id_count:=" + exc);
	}
        return count;
    }
    public static ArrayList <String[]> open_project_tasks_for_user_id(Connection con, String user_id) throws IOException, SQLException
    {
        ArrayList<String[]> project_tasks = new ArrayList();
        //String ps_query_string = "SELECT * FROM project_tasks WHERE assigned_to_id=? AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        String ps_query_string = "SELECT id," 
                + "project_id,"
                + "name,"
                + "description,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "priority,"
                + "status,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "estimated_duration,"
                + "actual_duration,"
                + "notes " 
                + "FROM project_tasks WHERE assigned_to_id=? AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        ArrayList<String[]> projects = get_projects.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            project_tasks = process_project_task_resultset(rs,projects,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.open_project_tasks_for_user_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.open_project_tasks_for_user_id:=" + exc);
	}
        return project_tasks;
    }
    public static int open_project_tasks_count_for_user_id(Connection con, String user_id) throws IOException, SQLException
    {
        int count = 0;
        String ps_query_string = "SELECT count(*) AS task_count FROM project_tasks WHERE assigned_to_id=? AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("task_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.open_project_tasks_count_for_user_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.open_project_tasks_count_for_user_id:=" + exc);
	}
        return count;
    }
    public static ArrayList <String[]> open_project_tasks(Connection con) throws IOException, SQLException
    {
        ArrayList<String[]> project_tasks = new ArrayList();
        //String ps_query_string = "SELECT * FROM project_tasks WHERE status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        String ps_query_string = "SELECT id," 
                + "project_id,"
                + "name,"
                + "description,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "priority,"
                + "status,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "estimated_duration,"
                + "actual_duration,"
                + "notes " 
                + "FROM project_tasks WHERE status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        ArrayList<String[]> projects = get_projects.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            project_tasks = process_project_task_resultset(rs,projects,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.open_project_tasks:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.open_project_tasks:=" + exc);
	}
        return project_tasks;
    }
    public static ArrayList <String[]> delinquent_project_tasks_for_user_id(Connection con, String user_id) throws IOException, SQLException
    {
        ArrayList<String[]> project_tasks = new ArrayList();
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        java.util.Date now = new java.util.Date();
        String now_string = timestamp_format.format(now);
        //String ps_query_string = "SELECT * FROM project_tasks WHERE assigned_to_id=? AND scheduled_end_date < '" + now_string + "' AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        String ps_query_string = "SELECT id," 
                + "project_id,"
                + "name,"
                + "description,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "priority,"
                + "status,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "estimated_duration,"
                + "actual_duration,"
                + "notes " 
                + "FROM project_tasks WHERE assigned_to_id=? AND scheduled_end_date < '" + now_string + "' AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        ArrayList<String[]> projects = get_projects.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            project_tasks = process_project_task_resultset(rs, projects, users, groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.delinquent_project_tasks_for_user_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.delinquent_project_tasks_for_user_id:=" + exc);
	}
        return project_tasks;
    }
    public static int delinquent_project_tasks_count_for_user_id(Connection con, String user_id) throws IOException, SQLException
    {
        int count = 0;
        java.util.Date now = new java.util.Date();
        String now_string = timestamp_format.format(now);
        String ps_query_string = "SELECT count(*) AS task_count FROM project_tasks WHERE assigned_to_id=? AND scheduled_end_date < '" + now_string + "' AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("task_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.delinquent_project_tasks_count_for_user_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.delinquent_project_tasks_count_for_user_id:=" + exc);
	}
        return count;
    }
    public static ArrayList <String[]> delinquent_project_tasks(Connection con) throws IOException, SQLException
    {
        ArrayList<String[]> project_tasks = new ArrayList();
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        java.util.Date now = new java.util.Date();
        String now_string = timestamp_format.format(now);
        //String ps_query_string = "SELECT * FROM project_tasks WHERE scheduled_end_date < '" + now_string + "' AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        String ps_query_string = "SELECT id," 
                + "project_id,"
                + "name,"
                + "description,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "priority,"
                + "status,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "estimated_duration,"
                + "actual_duration,"
                + "notes " 
                + "FROM project_tasks WHERE scheduled_end_date < '" + now_string + "' AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        ArrayList<String[]> projects = get_projects.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            project_tasks = process_project_task_resultset(rs, projects, users, groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.delinquent_project_tasks:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.delinquent_project_tasks:=" + exc);
	}
        return project_tasks;
    }
    public static ArrayList<Integer> my_projects(Connection con, String user_id)
    {
        PreparedStatement stmt;
        int total = 0;
        int critical = 0;
        int high = 0;
        int medium = 0;
        int low = 0;
        String priority = "";
        ArrayList<Integer> return_arraylist = new ArrayList();
        try
        {
            //total
            //String ps_query_string = "SELECT * FROM projects WHERE owner_id=? OR manager_id=? OR tech_lead_id=? AND status <> 'Closed' AND status <> 'Cancelled' AND status <> 'Complete'";  
            String ps_query_string = "SELECT " 
                + "id,"
                + "name,"
                + "description,"
                + "owner_group_id," 
                + "owner_id," 
                + "manager_id,"
                + "tech_lead_id,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "status,"
                + "priority,"
                + "notes "
                + "FROM projects WHERE owner_id=? OR manager_id=? OR tech_lead_id=? AND status <> 'Closed' AND status <> 'Cancelled' AND status <> 'Complete'";
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, user_id);
            stmt.setString(2, user_id);
            stmt.setString(3, user_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                total++;
                priority = rs.getString("priority");
                if(priority.equalsIgnoreCase("critical"))
                {
                    critical++;
                }
                else if(priority.equalsIgnoreCase("high"))
                {
                    high++;
                }
                else if(priority.equalsIgnoreCase("medium"))
                {
                    medium++;
                }
                else if(priority.equalsIgnoreCase("low"))
                {
                    low++;
                }
            }
            stmt.close();
            
            return_arraylist.add(0,total);
            //critical
            return_arraylist.add(1,critical);
            //high
            return_arraylist.add(2,high);
            //medium
            return_arraylist.add(3,medium);
            //low
            return_arraylist.add(4,low);
        }
        catch(Exception e)
        {
            System.out.println("Exception in db.get_projects.my_projects=" + e);
        }
        return return_arraylist;
    }
    public static ArrayList<Integer> projects(Connection con)
    {
        PreparedStatement stmt;
        int total = 0;
        int critical = 0;
        int high = 0;
        int medium = 0;
        int low = 0;
        String priority = "";
        ArrayList<Integer> return_arraylist = new ArrayList();
        try
        {
            //total
            //String ps_query_string = "SELECT * FROM projects WHERE status <> 'Closed' AND status <> 'Cancelled' AND status <> 'Complete'";  
            String ps_query_string = "SELECT " 
                + "id,"
                + "name,"
                + "description,"
                + "owner_group_id," 
                + "owner_id," 
                + "manager_id,"
                + "tech_lead_id,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "status,"
                + "priority,"
                + "notes "
                + "FROM projects WHERE status <> 'Closed' AND status <> 'Cancelled' AND status <> 'Complete'";
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                total++;
                priority = rs.getString("priority");
                if(priority.equalsIgnoreCase("critical"))
                {
                    critical++;
                }
                else if(priority.equalsIgnoreCase("high"))
                {
                    high++;
                }
                else if(priority.equalsIgnoreCase("medium"))
                {
                    medium++;
                }
                else if(priority.equalsIgnoreCase("low"))
                {
                    low++;
                }
            }
            stmt.close();
            
            return_arraylist.add(0,total);
            //critical
            return_arraylist.add(1,critical);
            //high
            return_arraylist.add(2,high);
            //medium
            return_arraylist.add(3,medium);
            //low
            return_arraylist.add(4,low);
        }
        catch(Exception e)
        {
            System.out.println("Exception in db.get_projects.projects=" + e);
        }
        return return_arraylist;
    }
    public static ArrayList<String[]> all_open_projects_by_priority(Connection con, String priority) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM projects WHERE priority=? AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%'";
        String query = "SELECT " 
                + "id,"
                + "name,"
                + "description,"
                + "owner_group_id," 
                + "owner_id," 
                + "manager_id,"
                + "tech_lead_id,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "status,"
                + "priority,"
                + "notes "
                + "FROM projects WHERE priority=? AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%'";  
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, priority);
            ResultSet rs = stmt.executeQuery();
            return_array_list = process_project_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.all_open_projects_by_priority:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.all_open_projects_by_priority:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> my_projects_by_priority(Connection con, String priority, String user_id) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM projects WHERE priority=? AND (owner_id=? OR manager_id=? OR tech_lead_id=?) AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%'";
        String query = "SELECT " 
                + "id,"
                + "name,"
                + "description,"
                + "owner_group_id," 
                + "owner_id," 
                + "manager_id,"
                + "tech_lead_id,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "status,"
                + "priority,"
                + "notes "
                + "FROM projects WHERE priority=? AND (owner_id=? OR manager_id=? OR tech_lead_id=?) AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%'";  
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, priority);
            stmt.setString(2, user_id);
            stmt.setString(3, user_id);
            stmt.setString(4, user_id);
            ResultSet rs = stmt.executeQuery();
            return_array_list = process_project_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.my_projects_by_priority:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.my_projects_by_priority:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList <String[]> my_open_project_tasks(Connection con, String user_id) throws IOException, SQLException
    {
        ArrayList<String[]> project_tasks = new ArrayList();
        //String ps_query_string = "SELECT * FROM project_tasks WHERE assigned_to_id=? AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        String ps_query_string = "SELECT id," 
                + "project_id,"
                + "name,"
                + "description,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "priority,"
                + "status,"
                + "DATE_FORMAT(scheduled_start_date,'%Y%m%d%H%i%s') AS scheduled_start_date, "
                + "DATE_FORMAT(actual_start_date,'%Y%m%d%H%i%s') AS actual_start_date, " 
                + "DATE_FORMAT(scheduled_end_date,'%Y%m%d%H%i%s') AS scheduled_end_date, " 
                + "DATE_FORMAT(actual_end_date,'%Y%m%d%H%i%s') AS actual_end_date, " 
                + "estimated_duration,"
                + "actual_duration,"
                + "notes " 
                + "FROM project_tasks WHERE assigned_to_id=? AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        ArrayList<String[]> projects = get_projects.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            project_tasks = process_project_task_resultset(rs,projects,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.my_open_project_tasks:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.my_open_project_tasks:=" + exc);
	}
        return project_tasks;
    }
    public static int all_open_projects_count(Connection con) throws IOException, SQLException
    {
        int return_int = 0;
        String query = "SELECT count(*) as value FROM projects WHERE status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND status NOT LIKE '%cancelled%'";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");                
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.all_open_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.all_open_count:=" + exc);
	}
        return return_int;
    }
    public static int open_project_tasks_count(Connection con) throws IOException, SQLException
    {
        int project_tasks = 0;
        String ps_query_string = "SELECT count(*) as value FROM project_tasks WHERE status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                project_tasks = rs.getInt("value");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.open_project_tasks_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.open_project_tasks_count:=" + exc);
	}
        return project_tasks;
    }
    public static int delinquent_project_tasks_count(Connection con) throws IOException, SQLException
    {
        int count = 0;
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        java.util.Date now = new java.util.Date();
        String now_string = timestamp_format.format(now);
        String ps_query_string = "SELECT count(*) AS task_count FROM project_tasks WHERE scheduled_end_date < '" + now_string + "' AND status NOT LIKE '%draft%' AND status NOT LIKE '%closed%' AND  status NOT LIKE '%cancelled%'";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("task_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.delinquent_project_tasks_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.delinquent_project_tasks_count:=" + exc);
	}
        return count;
    }
    public static HashMap<String, String> project_task_full_info(Connection con, String task_id) throws IOException, SQLException
    {
        HashMap<String,String> return_hashmap = new HashMap();    
        
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        String ps_query_string = "SELECT project_tasks.*, projects.name AS project_name,projects.description AS project_description FROM project_tasks INNER JOIN projects ON project_tasks.project_id = projects.id WHERE project_tasks.id=?";
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, task_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_hashmap.put("id", rs.getString("id"));
                return_hashmap.put("project_id_name", rs.getString("project_name"));
                return_hashmap.put("name", rs.getString("name"));
                return_hashmap.put("description", rs.getString("description"));
                
                return_hashmap.put("assigned_group_id_name", "");
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        return_hashmap.put("assigned_group_id_name", this_group[1]);
                        a = all_group_info.size();
                    }
                }
                
                try
                {
                    String user_info[] = db.get_users.by_id(con, rs.getString("assigned_to_id"));
                    return_hashmap.put("assigned_to_id_username", user_info[1]);
                }
                catch(Exception e)
                {
                    return_hashmap.put("assigned_to_id_username", "");
                }
                return_hashmap.put("priority", rs.getString("priority"));
                return_hashmap.put("status", rs.getString("status"));
                try
                {
                    return_hashmap.put("scheduled_start_date", date_time_picker_format.format(rs.getTimestamp("scheduled_start_date")));
                }
                catch(Exception e)
                {
                    return_hashmap.put("scheduled_start_date","");
                }
                try
                {
                    return_hashmap.put("actual_start_date", date_time_picker_format.format(rs.getTimestamp("actual_start_date")));
                }
                catch(Exception e)
                {
                    return_hashmap.put("actual_start_date","");
                }
                try
                {
                    return_hashmap.put("scheduled_end_date", date_time_picker_format.format(rs.getTimestamp("scheduled_end_date")));
                }
                catch(Exception e)
                {
                    return_hashmap.put("scheduled_end_date","");
                }
                try
                {
                    return_hashmap.put("actual_end_date", date_time_picker_format.format(rs.getTimestamp("actual_end_date")));
                }
                catch(Exception e)
                {
                    return_hashmap.put("actual_end_date","");
                }
                return_hashmap.put("estimated_duration", rs.getString("estimated_duration"));
                return_hashmap.put("actual_duration", rs.getString("actual_duration"));
                return_hashmap.put("notes", rs.getString("notes"));                
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_projects.project_task_full_info:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_projects.project_task_full_info:=" + exc);
	}
        return return_hashmap;
    }
}

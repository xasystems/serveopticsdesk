/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class get_message 
{
    private static Logger logger = LogManager.getLogger();
    static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
    
    
    public static ArrayList<String[]> all_messages(Connection con) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM messages"; 
        String query = "SELECT `id`,"
                + "`uuid`,"
                + "`source`,"
                + "DATE_FORMAT(date_sent,'%Y%m%d%H%i%s') AS date_sent, "
                + "`from`,"
                + "`subject`,"
                + "`to`,"
                + "`body`,"
                + "`mime_type`,"
                + "`priority`,"
                + "`status`,"
                + "DATE_FORMAT(status_date,'%Y%m%d%H%i%s') AS status_date, "
                + "`status_by_user_id`,"
                + "`result`,"
                + "`has_attachment`,"
                + "`description`,"
                + "`log`,"
                + "`related_to`,"
                + "`related_to_id`,"
                + "`assigned_to_id`,"
                + "DATE_FORMAT(date_assigned,'%Y%m%d%H%i%s') AS date_assigned, " 
                + "DATE_FORMAT(date_closed,'%Y%m%d%H%i%s') AS date_closed " 
                + "FROM `message` ORDER by date_sent DESC";
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String message[] = new String[22];
                //init the array
                for(int a = 0; a < message.length;a++)
                {
                    message[a] = "--";
                }                
                message[0] = rs.getString("id");
                message[1] = rs.getString("uuid");
                message[2] = support.string_utils.check_for_null(rs.getString("source"));
                message[3] = support.string_utils.check_for_null(rs.getString("date_sent"));
                message[4] = support.string_utils.check_for_null(rs.getString("from"));                
                message[5] = support.string_utils.check_for_null(rs.getString("subject"));  
                message[6] = support.string_utils.check_for_null(rs.getString("to"));  
                message[7] = support.string_utils.check_for_null(rs.getString("body"));  
                message[8] = support.string_utils.check_for_null(rs.getString("mime_type"));  
                message[9] = support.string_utils.check_for_null(rs.getString("priority"));  
                message[10] = support.string_utils.check_for_null(rs.getString("status"));  
                message[11] = support.string_utils.check_for_null(rs.getString("status_date"));  
                message[12] = support.string_utils.check_for_null(rs.getString("status_by_user_id"));  
                message[13] = support.string_utils.check_for_null(rs.getString("result"));  
                message[14] = support.string_utils.check_for_null(rs.getString("has_attachment"));  
                message[15] = support.string_utils.check_for_null(rs.getString("description"));  
                message[16] = support.string_utils.check_for_null(rs.getString("log"));  
                message[17] = support.string_utils.check_for_null(rs.getString("related_to"));     
                message[18] = support.string_utils.check_for_null(rs.getString("related_to_id"));  
                message[19] = support.string_utils.check_for_null(rs.getString("assigned_to_id"));     
                message[20] = support.string_utils.check_for_null(rs.getString("date_assigned"));    
                message[21] = support.string_utils.check_for_null(rs.getString("date_closed"));    
                return_array_list.add(message);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_message.all_messages:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_message.all_messages:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> all_messages_limited(Connection con, int starting_record, int fetch_size) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM messages"; 
        String query = "SELECT `id`,"
                + "`uuid`,"
                + "`source`,"
                + "DATE_FORMAT(date_sent,'%Y%m%d%H%i%s') AS date_sent, "
                + "`from`,"
                + "`subject`,"
                + "`to`,"
                + "`body`,"
                + "`mime_type`,"
                + "`priority`,"
                + "`status`,"
                + "DATE_FORMAT(status_date,'%Y%m%d%H%i%s') AS status_date, "
                + "`status_by_user_id`,"
                + "`result`,"
                + "`has_attachment`,"
                + "`description`,"
                + "`log`,"
                + "`related_to`,"
                + "`related_to_id`,"
                + "`assigned_to_id`,"
                + "DATE_FORMAT(date_assigned,'%Y%m%d%H%i%s') AS date_assigned, " 
                + "DATE_FORMAT(date_closed,'%Y%m%d%H%i%s') AS date_closed " 
                + "FROM `message` ORDER by date_sent DESC LIMIT ?, ?";
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setInt(1, starting_record);
            stmt.setInt(2, fetch_size);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String message[] = new String[22];
                //init the array
                for(int a = 0; a < message.length;a++)
                {
                    message[a] = "--";
                }                
                message[0] = rs.getString("id");
                message[1] = rs.getString("uuid");
                message[2] = support.string_utils.check_for_null(rs.getString("source"));
                message[3] = support.string_utils.check_for_null(rs.getString("date_sent"));
                message[4] = support.string_utils.check_for_null(rs.getString("from"));                
                message[5] = support.string_utils.check_for_null(rs.getString("subject"));  
                message[6] = support.string_utils.check_for_null(rs.getString("to"));  
                message[7] = support.string_utils.check_for_null(rs.getString("body"));  
                message[8] = support.string_utils.check_for_null(rs.getString("mime_type"));  
                message[9] = support.string_utils.check_for_null(rs.getString("priority"));  
                message[10] = support.string_utils.check_for_null(rs.getString("status"));  
                message[11] = support.string_utils.check_for_null(rs.getString("status_date"));  
                message[12] = support.string_utils.check_for_null(rs.getString("status_by_user_id"));  
                message[13] = support.string_utils.check_for_null(rs.getString("result"));  
                message[14] = support.string_utils.check_for_null(rs.getString("has_attachment"));  
                message[15] = support.string_utils.check_for_null(rs.getString("description"));  
                message[16] = support.string_utils.check_for_null(rs.getString("log"));  
                message[17] = support.string_utils.check_for_null(rs.getString("related_to"));     
                message[18] = support.string_utils.check_for_null(rs.getString("related_to_id"));  
                message[19] = support.string_utils.check_for_null(rs.getString("assigned_to_id"));     
                message[20] = support.string_utils.check_for_null(rs.getString("date_assigned"));    
                message[21] = support.string_utils.check_for_null(rs.getString("date_closed"));    
                return_array_list.add(message);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_message.all_messages:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_message.all_messages:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> all_messages_for_timeframe(Connection con, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        String start = timestamp_format.format(start_date);
        String end = timestamp_format.format(end_date);        
        //String query = "SELECT * FROM messages"; 
        String query = "SELECT `id`,"
                + "`uuid`,"
                + "`source`,"
                + "DATE_FORMAT(date_sent,'%Y%m%d%H%i%s') AS date_sent, "
                + "`from`,"
                + "`subject`,"
                + "`to`,"
                + "`body`,"
                + "`mime_type`,"
                + "`priority`,"
                + "`status`,"
                + "DATE_FORMAT(status_date,'%Y%m%d%H%i%s') AS status_date, "
                + "`status_by_user_id`,"
                + "`result`,"
                + "`has_attachment`,"
                + "`description`,"
                + "`log`,"
                + "`related_to`,"
                + "`related_to_id`,"
                + "`assigned_to_id`,"
                + "DATE_FORMAT(date_assigned,'%Y%m%d%H%i%s') AS date_assigned," 
                + "DATE_FORMAT(date_closed,'%Y%m%d%H%i%s') AS date_closed " 
                + "FROM `message` WHERE date_sent > ? AND date_sent < ? ORDER by date_sent DESC";        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, start);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String message[] = new String[22];
                //init the array
                for(int a = 0; a < message.length;a++)
                {
                    message[a] = "--";
                }
                
                message[0] = rs.getString("id");
                message[1] = rs.getString("uuid");
                message[2] = support.string_utils.check_for_null(rs.getString("source"));
                message[3] = support.string_utils.check_for_null(rs.getString("date_sent"));
                message[4] = support.string_utils.check_for_null(rs.getString("from"));                
                message[5] = support.string_utils.check_for_null(rs.getString("subject"));  
                message[6] = support.string_utils.check_for_null(rs.getString("to"));  
                message[7] = support.string_utils.check_for_null(rs.getString("body"));  
                message[8] = support.string_utils.check_for_null(rs.getString("mime_type"));  
                message[9] = support.string_utils.check_for_null(rs.getString("priority"));  
                message[10] = support.string_utils.check_for_null(rs.getString("status"));  
                message[11] = support.string_utils.check_for_null(rs.getString("status_date"));  
                message[12] = support.string_utils.check_for_null(rs.getString("status_by_user_id"));  
                message[13] = support.string_utils.check_for_null(rs.getString("result"));  
                message[14] = support.string_utils.check_for_null(rs.getString("has_attachment"));  
                message[15] = support.string_utils.check_for_null(rs.getString("description"));  
                message[16] = support.string_utils.check_for_null(rs.getString("log"));  
                message[17] = support.string_utils.check_for_null(rs.getString("related_to"));     
                message[18] = support.string_utils.check_for_null(rs.getString("related_to_id"));  
                message[19] = support.string_utils.check_for_null(rs.getString("assigned_to_id"));     
                message[20] = support.string_utils.check_for_null(rs.getString("date_assigned"));     
                message[21] = support.string_utils.check_for_null(rs.getString("date_closed"));     
                return_array_list.add(message);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_message.all_messages_for_timeframe:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_message.all_messages_for_timeframe:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> all_messages_filtered_limited(Connection con, int starting_record, int fetch_size, String filter, String order_column, String order_dir) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM messages"; 
        String query = "SELECT `id`,"
                + "`uuid`,"
                + "`source`,"
                + "DATE_FORMAT(date_sent,'%Y%m%d%H%i%s') AS date_sent, "
                + "`from`,"
                + "`subject`,"
                + "`to`,"
                + "`body`,"
                + "`mime_type`,"
                + "`priority`,"
                + "`status`,"
                + "DATE_FORMAT(status_date,'%Y%m%d%H%i%s') AS status_date, "
                + "`status_by_user_id`,"
                + "`result`,"
                + "`has_attachment`,"
                + "`description`,"
                + "`log`,"
                + "`related_to`,"
                + "`related_to_id`,"
                + "`assigned_to_id`,"
                + "DATE_FORMAT(date_assigned,'%Y%m%d%H%i%s') AS date_assigned," 
                + "DATE_FORMAT(date_closed,'%Y%m%d%H%i%s') AS date_closed " 
                + "FROM `message` " + (filter.trim().equals("") ? "" : " WHERE " + filter);

        if (!order_column.equals(""))
        {
            query += "ORDER BY " + order_column + " " + order_dir;
        } else {
            query += "ORDER BY date_sent DESC";
        }

        query += " LIMIT ? , ?";

        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setInt(1, starting_record);
            stmt.setInt(2, fetch_size);
            System.out.println("query=" + stmt.toString());
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String message[] = new String[22];
                //init the array
                for(int a = 0; a < message.length;a++)
                {
                    message[a] = "--";
                }
                
                message[0] = rs.getString("id");
                message[1] = rs.getString("uuid");
                message[2] = support.string_utils.check_for_null(rs.getString("source"));
                message[3] = support.string_utils.check_for_null(rs.getString("date_sent"));
                message[4] = support.string_utils.check_for_null(rs.getString("from"));                
                message[5] = support.string_utils.check_for_null(rs.getString("subject"));  
                message[6] = support.string_utils.check_for_null(rs.getString("to"));  
                message[7] = support.string_utils.check_for_null(rs.getString("body"));  
                message[8] = support.string_utils.check_for_null(rs.getString("mime_type"));  
                message[9] = support.string_utils.check_for_null(rs.getString("priority"));  
                message[10] = support.string_utils.check_for_null(rs.getString("status"));  
                message[11] = support.string_utils.check_for_null(rs.getString("status_date"));  
                message[12] = support.string_utils.check_for_null(rs.getString("status_by_user_id"));  
                message[13] = support.string_utils.check_for_null(rs.getString("result"));  
                message[14] = support.string_utils.check_for_null(rs.getString("has_attachment"));  
                message[15] = support.string_utils.check_for_null(rs.getString("description"));  
                message[16] = support.string_utils.check_for_null(rs.getString("log"));  
                message[17] = support.string_utils.check_for_null(rs.getString("related_to"));     
                message[18] = support.string_utils.check_for_null(rs.getString("related_to_id"));  
                message[19] = support.string_utils.check_for_null(rs.getString("assigned_to_id"));     
                message[20] = support.string_utils.check_for_null(rs.getString("date_assigned"));     
                message[21] = support.string_utils.check_for_null(rs.getString("date_closed"));     
                return_array_list.add(message);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_message.all_messages_for_timeframe:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_message.all_messages_for_timeframe:=" + exc);
	}
        return return_array_list;
    }
    public static String[] message_by_id(Connection con, String id) throws IOException, SQLException
    {
        String[] return_array = new String[22];
        //init the array
        for(int a = 0; a < return_array.length;a++)
        {
            return_array[a] = "--";
        }
        //String query = "SELECT * FROM email WHERE id=?"; 
        String query = "SELECT `id`,"
                + "`uuid`,"
                + "`source`,"
                + "DATE_FORMAT(date_sent,'%Y%m%d%H%i%s') AS date_sent, "
                + "`from`,"
                + "`subject`,"
                + "`to`,"
                + "`body`,"
                + "`mime_type`,"
                + "`priority`,"
                + "`status`,"
                + "DATE_FORMAT(status_date,'%Y%m%d%H%i%s') AS status_date, "
                + "`status_by_user_id`,"
                + "`result`,"
                + "`has_attachment`,"
                + "`description`,"
                + "`log`,"
                + "`related_to`,"
                + "`related_to_id`,"
                + "`assigned_to_id`,"
                + "DATE_FORMAT(date_assigned,'%Y%m%d%H%i%s') AS date_assigned, " 
                + "DATE_FORMAT(date_closed,'%Y%m%d%H%i%s') AS date_closed " 
                + "FROM `message` WHERE id=?"; 
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_array[0] = rs.getString("id");
                return_array[1] = rs.getString("uuid");
                return_array[2] = support.string_utils.check_for_null(rs.getString("source"));
                return_array[3] = support.string_utils.check_for_null(rs.getString("date_sent"));
                return_array[4] = support.string_utils.check_for_null(rs.getString("from"));                
                return_array[5] = support.string_utils.check_for_null(rs.getString("subject"));  
                return_array[6] = support.string_utils.check_for_null(rs.getString("to"));  
                return_array[7] = support.string_utils.check_for_null(rs.getString("body"));  
                return_array[8] = support.string_utils.check_for_null(rs.getString("mime_type"));  
                return_array[9] = support.string_utils.check_for_null(rs.getString("priority"));  
                return_array[10] = support.string_utils.check_for_null(rs.getString("status"));  
                return_array[11] = support.string_utils.check_for_null(rs.getString("status_date"));  
                return_array[12] = support.string_utils.check_for_null(rs.getString("status_by_user_id"));  
                return_array[13] = support.string_utils.check_for_null(rs.getString("result"));  
                return_array[14] = support.string_utils.check_for_null(rs.getString("has_attachment"));  
                return_array[15] = support.string_utils.check_for_null(rs.getString("description"));  
                return_array[16] = support.string_utils.check_for_null(rs.getString("log"));  
                return_array[17] = support.string_utils.check_for_null(rs.getString("related_to"));     
                return_array[18] = support.string_utils.check_for_null(rs.getString("related_to_id"));  
                return_array[19] = support.string_utils.check_for_null(rs.getString("assigned_to_id"));     
                return_array[20] = support.string_utils.check_for_null(rs.getString("date_assigned")); 
                return_array[21] = support.string_utils.check_for_null(rs.getString("date_closed")); 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_message.message_by_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_message.message_by_id:=" + exc);
	}
        return return_array;
    }
    public static String[] message_by_uuid(Connection con, String uuid) throws IOException, SQLException
    {
        String[] return_array = new String[22];
        //init the array
        for(int a = 0; a < return_array.length;a++)
        {
            return_array[a] = "--";
        }
        //String query = "SELECT * FROM email WHERE id=?"; 
        String query = "SELECT `id`,"
                + "`uuid`,"
                + "`source`,"
                + "DATE_FORMAT(date_sent,'%Y%m%d%H%i%s') AS date_sent, "
                + "`from`,"
                + "`subject`,"
                + "`to`,"
                + "`body`,"
                + "`mime_type`,"
                + "`priority`,"
                + "`status`,"
                + "DATE_FORMAT(status_date,'%Y%m%d%H%i%s') AS status_date, "
                + "`status_by_user_id`,"
                + "`result`,"
                + "`has_attachment`,"
                + "`description`,"
                + "`log`,"
                + "`related_to`,"
                + "`related_to_id`,"
                + "`assigned_to_id`,"
                + "DATE_FORMAT(date_assigned,'%Y%m%d%H%i%s') AS date_assigned, " 
                + "DATE_FORMAT(date_closed,'%Y%m%d%H%i%s') AS date_closed " 
                + "FROM `message` WHERE uuid=?"; 
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, uuid);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_array[0] = rs.getString("id");
                return_array[1] = rs.getString("uuid");
                return_array[2] = support.string_utils.check_for_null(rs.getString("source"));
                return_array[3] = support.string_utils.check_for_null(rs.getString("date_sent"));
                return_array[4] = support.string_utils.check_for_null(rs.getString("from"));                
                return_array[5] = support.string_utils.check_for_null(rs.getString("subject"));  
                return_array[6] = support.string_utils.check_for_null(rs.getString("to"));  
                return_array[7] = support.string_utils.check_for_null(rs.getString("body"));  
                return_array[8] = support.string_utils.check_for_null(rs.getString("mime_type"));  
                return_array[9] = support.string_utils.check_for_null(rs.getString("priority"));  
                return_array[10] = support.string_utils.check_for_null(rs.getString("status"));  
                return_array[11] = support.string_utils.check_for_null(rs.getString("status_date"));  
                return_array[12] = support.string_utils.check_for_null(rs.getString("status_by_user_id"));  
                return_array[13] = support.string_utils.check_for_null(rs.getString("result"));  
                return_array[14] = support.string_utils.check_for_null(rs.getString("has_attachment"));  
                return_array[15] = support.string_utils.check_for_null(rs.getString("description"));  
                return_array[16] = support.string_utils.check_for_null(rs.getString("log"));  
                return_array[17] = support.string_utils.check_for_null(rs.getString("related_to"));     
                return_array[18] = support.string_utils.check_for_null(rs.getString("related_to_id"));  
                return_array[19] = support.string_utils.check_for_null(rs.getString("assigned_to_id"));     
                return_array[20] = support.string_utils.check_for_null(rs.getString("date_assigned"));
                return_array[21] = support.string_utils.check_for_null(rs.getString("date_closed")); 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_message.email_by_uuid:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_message.email_by_uuid:=" + exc);
	}
        return return_array;
    }
    public static ArrayList<String[]> all_message_attachment_info(Connection con) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM message_attachements"; 
        String query = "SELECT `id`,"
                + "`uuid`," 
                + "`name_on_disk`,"
                + "`file_name`,"
                + "`file_date`,"
                + "`file_size` "
                + "FROM message_attachements";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String attachment[] = new String[6];
                //init the array
                for(int a = 0; a < attachment.length;a++)
                {
                    attachment[a] = "--";
                }
                
                attachment[0] = rs.getString("id");
                attachment[1] = rs.getString("uuid");
                attachment[2] = support.string_utils.check_for_null(rs.getString("name_on_disk"));
                attachment[3] = support.string_utils.check_for_null(rs.getString("file_name"));
                attachment[4] = support.string_utils.check_for_null(rs.getString("file_date"));                
                attachment[5] = support.string_utils.check_for_null(rs.getString("file_size"));                   
                return_array_list.add(attachment);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_message.all_message_attachment_info:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_message.all_message_attachment_info:=" + exc);
	}
        return return_array_list;
    }
    public static String[] message_attachment_info_for_id(Connection con, String id) throws IOException, SQLException
    {
        String[] return_array = new String[6];
        //String query = "SELECT * FROM email_attachements WHERE id=?"; 
        String query = "SELECT `id`,"
                + "`uuid`," 
                + "`name_on_disk`,"
                + "`file_name`,"
                + "`file_date`,"
                + "`file_size` "
                + "FROM message_attachements WHERE id=?"; 
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                for(int a = 0; a < return_array.length;a++)
                {
                    return_array[a] = "--";
                }
                
                return_array[0] = rs.getString("id");
                return_array[1] = rs.getString("uuid");
                return_array[2] = support.string_utils.check_for_null(rs.getString("name_on_disk"));
                return_array[3] = support.string_utils.check_for_null(rs.getString("file_name"));
                return_array[4] = support.string_utils.check_for_null(rs.getString("file_date"));                
                return_array[5] = support.string_utils.check_for_null(rs.getString("file_size"));    
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_message.message_attachment_info_for_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_message.message_attachment_info_for_id:=" + exc);
	}
        return return_array;
    }
    public static ArrayList<String[]> all_message_attachment_info_for_uuid(Connection con, String uuid) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM message_attachements WHERE uuid=?"; 
        String query = "SELECT `id`,"
                + "`uuid`," 
                + "`name_on_disk`,"
                + "`file_name`,"
                + "`file_date`,"
                + "`file_size` "
                + "FROM message_attachements WHERE uuid=?"; 
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, uuid);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String attachment[] = new String[6];
                //init the array
                for(int a = 0; a < attachment.length;a++)
                {
                    attachment[a] = "--";
                }
                
                attachment[0] = rs.getString("id");
                attachment[1] = rs.getString("uuid");
                attachment[2] = support.string_utils.check_for_null(rs.getString("name_on_disk"));
                attachment[3] = support.string_utils.check_for_null(rs.getString("file_name"));
                attachment[4] = support.string_utils.check_for_null(rs.getString("file_date"));                
                attachment[5] = support.string_utils.check_for_null(rs.getString("file_size"));                   
                return_array_list.add(attachment);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_message.all_email_attachment_info_for_uuid:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_message.all_email_attachment_info_for_uuid:=" + exc);
	}
        return return_array_list;
    }
    public static File file_attachement_by_id(Connection con, LinkedHashMap sys_props, String customer_db, String id)
    {
        File return_file = null;
        String file_name = "";
        try
        {
            String file_info[] = db.get_message.message_attachment_info_for_id(con, id);
            file_name = file_info[2];
        }
        catch(Exception e)
        {
            
        }
        String config_message_attachment_store = sys_props.get("message_attachment_store").toString();
        
        File this_customer_message_attachement = new File(config_message_attachment_store + "/" + customer_db + "/attachments/" + file_name);
        if(!this_customer_message_attachement.exists()) //if the customer attachment dir not exist ctreate it
        {   
            return_file = null;
        }
        else
        {
            return_file = this_customer_message_attachement;
        }
        
        return return_file;
    }
    public static ArrayList <File> file_attachement_by_message_uuid(Connection con, LinkedHashMap sys_props, String customer_db, String uuid)
    {
        ArrayList <File> return_files_arraylist = new ArrayList();
        ArrayList <String> file_names = new ArrayList();
        try
        {
            ArrayList <String[]> all_attachements = db.get_message.all_message_attachment_info_for_uuid(con, uuid);
            for(int a = 0; a < all_attachements.size(); a++)
            {
                String attachment[] = all_attachements.get(a);
                file_names.add(attachment[2]);
            }
        }
        catch(Exception e)
        {
        }
        
        String config_message_attachment_store = sys_props.get("message_attachment_store").toString();  
        for(int b = 0; b < file_names.size();b++)
        {
            String this_file_name = file_names.get(b);
            File this_customer_email_attachement = new File(config_message_attachment_store + "/" + customer_db + "/attachments/" + this_file_name);
            if(this_customer_email_attachement.exists()) //if the customer attachment dir not exist ctreate it
            {   
                return_files_arraylist.add(this_customer_email_attachement);
            }
        }
        return return_files_arraylist;
    }    
    public static ArrayList<String[]> message_by_status(Connection con, String status) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM email WHERE status=? ORDER BY date_sent DESC"; 
        String query = "SELECT `id`,"
                + "`uuid`,"
                + "`source`,"
                + "DATE_FORMAT(date_sent,'%Y%m%d%H%i%s') AS date_sent, "
                + "`from`,"
                + "`subject`,"
                + "`to`,"
                + "`body`,"
                + "`mime_type`,"
                + "`priority`,"
                + "`status`,"
                + "DATE_FORMAT(status_date,'%Y%m%d%H%i%s') AS status_date, "
                + "`status_by_user_id`,"
                + "`result`,"
                + "`has_attachment`,"
                + "`description`,"
                + "`log`,"
                + "`related_to`,"
                + "`related_to_id`,"
                + "`assigned_to_id`,"
                + "DATE_FORMAT(date_assigned,'%Y%m%d%H%i%s') AS date_assigned, " 
                + "DATE_FORMAT(date_closed,'%Y%m%d%H%i%s') AS date_closed " 
                + "FROM `message` WHERE status=? ORDER BY date_sent DESC"; 
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, status);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String message[] = new String[22];
                //init the array
                for(int a = 0; a < message.length;a++)
                {
                    message[a] = "--";
                }
                message[0] = rs.getString("id");
                message[1] = rs.getString("uuid");
                message[2] = support.string_utils.check_for_null(rs.getString("source"));
                message[3] = support.string_utils.check_for_null(rs.getString("date_sent"));
                message[4] = support.string_utils.check_for_null(rs.getString("from"));                
                message[5] = support.string_utils.check_for_null(rs.getString("subject"));  
                message[6] = support.string_utils.check_for_null(rs.getString("to"));  
                message[7] = support.string_utils.check_for_null(rs.getString("body"));  
                message[8] = support.string_utils.check_for_null(rs.getString("mime_type"));  
                message[9] = support.string_utils.check_for_null(rs.getString("priority"));  
                message[10] = support.string_utils.check_for_null(rs.getString("status"));  
                message[11] = support.string_utils.check_for_null(rs.getString("status_date"));  
                message[12] = support.string_utils.check_for_null(rs.getString("status_by_user_id"));  
                message[13] = support.string_utils.check_for_null(rs.getString("result"));  
                message[14] = support.string_utils.check_for_null(rs.getString("has_attachment"));  
                message[15] = support.string_utils.check_for_null(rs.getString("description"));  
                message[16] = support.string_utils.check_for_null(rs.getString("log"));  
                message[17] = support.string_utils.check_for_null(rs.getString("related_to"));     
                message[18] = support.string_utils.check_for_null(rs.getString("related_to_id"));  
                message[19] = support.string_utils.check_for_null(rs.getString("assigned_to_id"));     
                message[20] = support.string_utils.check_for_null(rs.getString("date_assigned"));               
                message[21] = support.string_utils.check_for_null(rs.getString("date_closed"));               
                return_array_list.add(message);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_message.message_by_status:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_message.message_by_status:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> message_by_open_status(Connection con) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM message WHERE status='Open' OR status='INW'"; 
        String query = "SELECT `id`,"
                + "`uuid`,"
                + "`source`,"
                + "DATE_FORMAT(date_sent,'%Y%m%d%H%i%s') AS date_sent, "
                + "`from`,"
                + "`subject`,"
                + "`to`,"
                + "`body`,"
                + "`mime_type`,"
                + "`priority`,"
                + "`status`,"
                + "DATE_FORMAT(status_date,'%Y%m%d%H%i%s') AS status_date, "
                + "`status_by_user_id`,"
                + "`result`,"
                + "`has_attachment`,"
                + "`description`,"
                + "`log`,"
                + "`related_to`,"
                + "`related_to_id`,"
                + "`assigned_to_id`,"
                + "DATE_FORMAT(date_assigned,'%Y%m%d%H%i%s') AS date_assigned, " 
                + "DATE_FORMAT(date_closed,'%Y%m%d%H%i%s') AS date_closed " 
                + "FROM `message` WHERE status='Open' OR status='INW' OR status='Assigned'";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String message[] = new String[22];
                //init the array
                for(int a = 0; a < message.length;a++)
                {
                    message[a] = "--";
                }
                message[0] = rs.getString("id");
                message[1] = rs.getString("uuid");
                message[2] = support.string_utils.check_for_null(rs.getString("source"));
                message[3] = support.string_utils.check_for_null(rs.getString("date_sent"));
                message[4] = support.string_utils.check_for_null(rs.getString("from"));                
                message[5] = support.string_utils.check_for_null(rs.getString("subject"));  
                message[6] = support.string_utils.check_for_null(rs.getString("to"));  
                message[7] = support.string_utils.check_for_null(rs.getString("body"));  
                message[8] = support.string_utils.check_for_null(rs.getString("mime_type"));  
                message[9] = support.string_utils.check_for_null(rs.getString("priority"));  
                message[10] = support.string_utils.check_for_null(rs.getString("status"));  
                message[11] = support.string_utils.check_for_null(rs.getString("status_date"));  
                message[12] = support.string_utils.check_for_null(rs.getString("status_by_user_id"));  
                message[13] = support.string_utils.check_for_null(rs.getString("result"));  
                message[14] = support.string_utils.check_for_null(rs.getString("has_attachment"));  
                message[15] = support.string_utils.check_for_null(rs.getString("description"));  
                message[16] = support.string_utils.check_for_null(rs.getString("log"));  
                message[17] = support.string_utils.check_for_null(rs.getString("related_to"));     
                message[18] = support.string_utils.check_for_null(rs.getString("related_to_id"));  
                message[19] = support.string_utils.check_for_null(rs.getString("assigned_to_id"));     
                message[20] = support.string_utils.check_for_null(rs.getString("date_assigned"));                 
                message[21] = support.string_utils.check_for_null(rs.getString("date_closed"));                 
                return_array_list.add(message);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_message.message_by_open_status:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_email.email_by_open_status:=" + exc);
	}
        return return_array_list;
    }
    public static int message_count_for_filter(Connection con, String filter) throws IOException, SQLException
    {
        int return_int = 0;
        //String query = "SELECT * FROM messages"; 
        String query = "SELECT count(*) as count FROM `message` " + (filter.trim().equals("") ? "" : " WHERE " + filter);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            System.out.println("ERROR SQLException get_message.message_count_for_filter:=" + ex);
        }
        catch(Exception exc) 
        {
            System.out.println("ERROR Exception get_message.message_count_for_filter:=" + exc);
	}
        return return_int;
    }
    public static int message_count_for_timeframe(Connection con, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {
        int return_int = 0;
        String start = timestamp_format.format(start_date);
        String end = timestamp_format.format(end_date);
        
        //String query = "SELECT * FROM messages"; 
        String query = "SELECT count(*) as count FROM `message` WHERE date_sent > ? AND date_sent < ?";        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, start);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            System.out.println("ERROR SQLException get_message.message_count_for_timeframe:=" + ex);
        }
        catch(Exception exc) 
        {
            System.out.println("ERROR Exception get_message.message_count_for_timeframe:=" + exc);
	}
        return return_int;
    }
    public static int message_count_for_status(Connection con, String status) throws IOException, SQLException
    {
        int return_int = 0;
        
        //String query = "SELECT * FROM messages"; 
        String query = "SELECT count(*) as count FROM `message` WHERE status = ?";        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, status);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            System.out.println("ERROR SQLException get_message.message_count_for_status:=" + ex);
        }
        catch(Exception exc) 
        {
            System.out.println("ERROR Exception get_message.message_count_for_status:=" + exc);
	}
        return return_int;
    }
    public static int message_count_for_timeframe_and_status(Connection con, java.util.Date start_date, java.util.Date end_date, String status) throws IOException, SQLException
    {
        int return_int = 0;
        String start = timestamp_format.format(start_date);
        String end = timestamp_format.format(end_date);
        
        //String query = "SELECT * FROM messages"; 
        String query = "SELECT count(*) as count FROM `message` WHERE date_sent > ? AND date_sent < ? and status = ?";        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, start);
            stmt.setString(2, end);
            stmt.setString(3, status);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            System.out.println("ERROR SQLException get_message.message_count_for_timeframe_and_status:=" + ex);
        }
        catch(Exception exc) 
        {
            System.out.println("ERROR Exception get_message.message_count_for_timeframe_and_status:=" + exc);
	}
        return return_int;
    }
    public static int all_message_count(Connection con) throws IOException, SQLException
    {
        int return_int = 0;
        
        //String query = "SELECT * FROM messages"; 
        String query = "SELECT count(*) as count FROM `message`";        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            System.out.println("ERROR SQLException get_message.all_message_count:=" + ex);
        }
        catch(Exception exc) 
        {
            System.out.println("ERROR Exception get_message.all_message_count:=" + exc);
	}
        return return_int;
    }
}

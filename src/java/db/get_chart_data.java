/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class get_chart_data 
{
    private static Logger logger = LogManager.getLogger();
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
    private static SimpleDateFormat user_tz_timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
    
    public static String[][] incident_dashboard_queue_by_priority(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        String return_string[][] = {{"Critical", "0", "Critical", "#a781e4"}, {"High", "0", "High", "#da5656"}, {"Medium", "0", "Medium", "#e8aa4e"}, {"Low", "0", "Low", "#6dba95"}};  
        //name: "Critical",y: 1,priority: "Critical",color: "#ef5350"
        SimpleDateFormat timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT COUNT(*) AS value FROM incidents WHERE (incident_time >= ? AND incident_time <= ?) AND priority=? AND state <> 'Closed' AND state <> 'Resolved'";
        }
        else
        {
            ps_query_string = "SELECT COUNT(*) as value FROM incidents WHERE (incident_time >= ? AND incident_time <= ?) AND priority=? AND assigned_group_id='" + group_id + "' AND state <> 'Closed' AND state <> 'Resolved'";
        }
        String priorities[] = {"Critical","High","Medium","Low"};
        PreparedStatement stmt;
        try 
        {
            int value = 0;
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            for(int a = 0; a < priorities.length;a++)
            {
                
                stmt.setTimestamp(1, start);
                stmt.setTimestamp(2, end);
                stmt.setString(3, priorities[a]);
                ResultSet rs = stmt.executeQuery();
                while(rs.next())
                {
                    value = rs.getInt("value");  
                    //System.out.println("queue stmt=" + stmt.toString());
                    //System.out.println("queue stmt returned=" + value);
                }
                return_string[a][1] = String.valueOf(value);
            }            
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chart_data.incident_dashboard_count_by_priority:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chart_data.incident_dashboard_count_by_priority:=" + exc);
	}
        return return_string;
    }
    public static String[][] incident_dashboard_queue_by_priority_customer(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        String return_string[][] = {{"Critical", "0", "Critical", "#a781e4"}, {"High", "0", "High", "#da5656"}, {"Medium", "0", "Medium", "#e8aa4e"}, {"Low", "0", "Low", "#6dba95"}};  
        //name: "Critical",y: 1,priority: "Critical",color: "#ef5350"
        SimpleDateFormat timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT COUNT(*) AS value FROM incidents WHERE (incident_time >= ? AND incident_time <= ?) AND priority=? AND state <> 'Closed' AND state <> 'Resolved'";
        }
        else
        {
            ps_query_string = "SELECT COUNT(*) as value FROM incidents WHERE (incident_time >= ? AND incident_time <= ?) AND priority=? AND caller_group_id='" + group_id + "' AND state <> 'Closed' AND state <> 'Resolved'";
        }
        String priorities[] = {"Critical","High","Medium","Low"};
        PreparedStatement stmt;
        try 
        {
            int value = 0;
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            for(int a = 0; a < priorities.length;a++)
            {
                
                stmt.setTimestamp(1, start);
                stmt.setTimestamp(2, end);
                stmt.setString(3, priorities[a]);
                ResultSet rs = stmt.executeQuery();
                while(rs.next())
                {
                    value = rs.getInt("value");  
                    //System.out.println("queue stmt=" + stmt.toString());
                    //System.out.println("queue stmt returned=" + value);
                }
                return_string[a][1] = String.valueOf(value);
            }            
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chart_data.incident_dashboard_queue_by_priority_customer:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chart_data.incident_dashboard_queue_by_priority_customer:=" + exc);
	}
        return return_string;
    }
    public static String[][] incident_dashboard_number_of_incidents(Connection con, String user_tz_name, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {       
        //System.out.println(" pre tz incident_dashboard_number_of_incidents start=" + timestamp_format.format(start_date) + " stop=" + timestamp_format.format(end_date));
        user_tz_timestamp_format.setTimeZone(TimeZone.getTimeZone(user_tz_name));
        
        //set the start and stop to the beginning and end of the day respectively IN USERs TZ
        GregorianCalendar start_cal = new GregorianCalendar();
        start_cal.setTime(start_date);
        start_cal.set(start_cal.HOUR_OF_DAY, 0);
        start_cal.set(start_cal.MINUTE, 0);
        start_cal.set(start_cal.SECOND, 0);
        start_date.setTime(start_cal.getTimeInMillis());
        
        GregorianCalendar stop_cal = new GregorianCalendar();
        stop_cal.setTime(end_date);
        stop_cal.set(stop_cal.HOUR_OF_DAY, 23);
        stop_cal.set(stop_cal.MINUTE, 59);
        stop_cal.set(stop_cal.SECOND, 59);
        end_date.setTime(stop_cal.getTimeInMillis());
        
        //convert input times to utc for query
        ZonedDateTime zdt_start_date = support.date_utils.user_tz_to_utc(user_tz_name, timestamp_format.format(start_date));
        start_date = java.util.Date.from(zdt_start_date.toInstant()); 
        ZonedDateTime zdt_end_date = support.date_utils.user_tz_to_utc(user_tz_name, timestamp_format.format(end_date));
        end_date = java.util.Date.from(zdt_end_date.toInstant()); 
        
        if(group_id == null)
        {
            group_id = "all";
        }
        SimpleDateFormat day_month = new SimpleDateFormat("dd-MMM");
        SimpleDateFormat month_day_year = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
        
        long startTime = start_date.getTime();
        long endTime = end_date.getTime();
        long diffTime = endTime - startTime;
        long diffDays = diffTime / (1000 * 60 * 60 * 24); //number of days
        //System.out.println("number of days=" + diffDays);
        
        String return_string[][] = new String[(int)diffDays][4]; //12-Dec,y_value,12/12/2018,01/02/2019 12:00 AM - 02/01/2019 12:00 AM   
        //build an array of days in longs starts and ends to compare to the queried incident_time
        long days[][] = new long[(int)diffDays][2];
        String timestamps[][] = new String[(int)diffDays][2];
        
        java.util.Date chart_data_start_date = new java.util.Date();
        java.util.Date chart_data_end_date = new java.util.Date();
                
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(end_date);

//        for(int a = 0; a < (int)diffDays; a++) // changed to reverse loop because the last day was always lost
        for(int a = (int)diffDays - 1; a >= 0 ; a--)
        {            
            //cal.set(cal.HOUR_OF_DAY, 0);
            //cal.set(cal.MINUTE, 0);
            //cal.set(cal.SECOND, 0);
            //change days to user tz
            java.util.Date temp = cal.getTime();
            timestamps[a][0] = timestamp_format.format(temp);
            days[a][0] = temp.getTime(); //start
            chart_data_start_date = cal.getTime();
            //initialize the return_string
            return_string[a][0] = day_month.format(cal.getTime());
            return_string[a][1] = "0";
            return_string[a][2] = month_day_year.format(cal.getTime());     
            
//            cal.add(cal.HOUR_OF_DAY, 23);
//            cal.add(cal.MINUTE, 59);
//            cal.add(cal.SECOND, 59);
            cal.add(cal.DAY_OF_MONTH, -1);
            days[a][1] = cal.getTimeInMillis(); //end   
            chart_data_end_date = cal.getTime();
            java.util.Date temp1 = cal.getTime();
            timestamps[a][1] = timestamp_format.format(temp1);
            
            // System.out.println("day=" + timestamps[a][0] + " " + "day=" + timestamps[a][1]);
            
            //get date range for the chartdrilldown page
            return_string[a][3] = filter_format.format(chart_data_start_date) + " - " + filter_format.format(chart_data_end_date);            
            
            //add a day to the calendar            
            //cal.add(cal.DATE, 1);
            cal.add(cal.SECOND, 1);
        }
        // logger.info(Arrays.deepToString(return_string));
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT count(*) AS incident_count FROM incidents WHERE (incident_time >= ? AND incident_time <= ?) ORDER BY incident_time ASC";
        }
        else
        {
            ps_query_string = "SELECT count(*) AS incident_count FROM incidents WHERE (incident_time >= ? AND incident_time <= ?)AND assigned_group_id='" + group_id + "' ORDER BY incident_time ASC";
        }
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            for(int a = (timestamps.length - 1); a >= 0 ; a--)
            {
                stmt.setString(1, timestamps[a][0]);
                stmt.setString(2, timestamps[a][1]);
                //System.out.println("date q=" + stmt);
                ResultSet rs = stmt.executeQuery();
                while(rs.next())
                {
                    return_string[a][1] = rs.getString("incident_count");
                    //System.out.println("start=" + timestamps[a][0] + " stop=" + timestamps[a][1] + " count=" + return_string[a][1]);
                }
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chart_data.incident_dashboard_number_of_incidents:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chart_data.incident_dashboard_number_of_incidents:=" + exc);
	}
        return return_string;
    }
    public static String[][] incident_dashboard_number_of_incidents_customer(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {       
        SimpleDateFormat day_month = new SimpleDateFormat("dd-MMM");
        SimpleDateFormat month_day_year = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
        
        long startTime = start_date.getTime();
        long endTime = end_date.getTime();
        long diffTime = endTime - startTime;
        long diffDays = diffTime / (1000 * 60 * 60 * 24); //number of days
        //System.out.println("number of days=" + diffDays);
        
        String return_string[][] = new String[(int)diffDays][4]; //12-Dec,y_value,12/12/2018,01/02/2019 12:00 AM - 02/01/2019 12:00 AM   
        //build an array of days in longs starts and ends to compare to the queried incident_time
        long days[][] = new long[(int)diffDays][2];
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(start_date);
        
        java.util.Date chart_data_start_date = new java.util.Date();
        java.util.Date chart_data_end_date = new java.util.Date();
                
        
        for(int a = 0; a < (int)diffDays; a++)
        {            
            cal.set(cal.HOUR_OF_DAY, 0);
            cal.set(cal.MINUTE, 0);
            cal.set(cal.SECOND, 0);
            days[a][0] = cal.getTimeInMillis(); //start
            chart_data_start_date = cal.getTime();
            //initialize the return_string
            return_string[a][0] = day_month.format(cal.getTime());
            return_string[a][1] = "0";
            return_string[a][2] = month_day_year.format(cal.getTime());     
            
            cal.set(cal.HOUR_OF_DAY, 23);
            cal.set(cal.MINUTE, 59);
            cal.set(cal.SECOND, 59);
            days[a][1] = cal.getTimeInMillis(); //end   
            chart_data_end_date = cal.getTime();
            
            //get date range for the chartdrilldown page
            return_string[a][3] = filter_format.format(chart_data_start_date) + " - " + filter_format.format(chart_data_end_date);
            
            
            //add a day to the calendar            
            cal.add(cal.DATE, 1);
        }
        
        //12-Dec,y_value,12/12/2018   
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT * FROM incidents WHERE (incident_time >= ? AND incident_time <= ?) ORDER BY incident_time ASC";
        }
        else
        {
            ps_query_string = "SELECT * FROM incidents WHERE (incident_time >= ? AND incident_time <= ?)AND caller_group_id='" + group_id + "' ORDER BY incident_time ASC";
        }
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            java.util.Date incident_time = new java.util.Date();
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next())
            {
                incident_time = rs.getDate("incident_time"); 
                for(int b = 0; b < days.length;b++)
                {
                    if(incident_time.getTime() >= days[b][0] && incident_time.getTime() <= days[b][1])
                    {
                        //this is the day to add the incident to
                        int current_value = Integer.parseInt(return_string[b][1]);
                        current_value++;
                        return_string[b][1] = String.valueOf(current_value);
                        b = days.length;
                    }
                }                
            }                         
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chart_data.incident_dashboard_number_of_incidents_customer:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chart_data.incident_dashboard_number_of_incidents_customer:=" + exc);
	}
        return return_string;
    }
    public static String[][] incident_dashboard_top_5_categories(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {       
        String return_string[][] = {{"No Incidents","0"}}; //category,y_value   
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT category, count(*) FROM incidents WHERE (incident_time >= ? AND incident_time <=?) GROUP BY category ORDER BY count(*) DESC LIMIT 5";
        }
        else
        {
            ps_query_string = "SELECT category, count(*) FROM incidents WHERE (incident_time >= ? AND incident_time <=?) AND assigned_group_id='" + group_id + "'  GROUP BY category ORDER BY count(*) DESC LIMIT 5";
        }
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            java.util.Date incident_time = new java.util.Date();
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }  
            if(count > 0)
            {
                return_string = new String[count][2];
            }
            rs.beforeFirst();
            count = 0;
            while(rs.next())
            {
                return_string[count][0] = rs.getString(1);
                return_string[count][1] = rs.getString(2);
                count++;
            } 
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chart_data.incident_dashboard_top_5_categories:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chart_data.incident_dashboard_top_5_categories:=" + exc);
	}
        return return_string;
    } 
    public static String[][] incident_dashboard_top_5_categories_customer(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {       
        String return_string[][] = {{"No Incidents","0"}}; //category,y_value   
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT category, count(*) FROM incidents WHERE (incident_time >= ? AND incident_time <=?) GROUP BY category ORDER BY count(*) DESC LIMIT 5";
        }
        else
        {
            ps_query_string = "SELECT category, count(*) FROM incidents WHERE (incident_time >= ? AND incident_time <=?) AND caller_group_id='" + group_id + "'  GROUP BY category ORDER BY count(*) DESC LIMIT 5";
        }
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            java.util.Date incident_time = new java.util.Date();
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            ResultSet rs = stmt.executeQuery();
            //System.out.println("stmt=" + stmt);
            int count = 0;
            while(rs.next())
            {
                count++;
            }  
            if(count > 0)
            {
                return_string = new String[count][2];
            }
            rs.beforeFirst();
            count = 0;
            while(rs.next())
            {
                return_string[count][0] = rs.getString(1);
                return_string[count][1] = rs.getString(2);
                count++;
            } 
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chart_data.incident_dashboard_top_5_categories_customer:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chart_data.incident_dashboard_top_5_categories_customer:=" + exc);
	}
        return return_string;
    } 
    public static String[][] incident_dashboard_closure_rate(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {       
        DecimalFormat df2 = new DecimalFormat("#.##");
        SimpleDateFormat day_month = new SimpleDateFormat("dd-MMM");
        SimpleDateFormat month_day_year = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
        java.util.Date chart_data_start_date = new java.util.Date();
        java.util.Date chart_data_end_date = new java.util.Date();
        
        long startTime = start_date.getTime();
        long endTime = end_date.getTime();
        long diffTime = endTime - startTime;
        long diffDays = diffTime / (1000 * 60 * 60 * 24); //number of days
        //System.out.println("number of days=" + diffDays);
        
        String return_string[][] = new String[(int)diffDays][7]; //12-Dec,12/12/2018,Low_Rate,Med_Rate,Hi_Rate,Critical_Rate,date_range   
        Double create_close[][] = new Double[(int)diffDays][8]; //low_create, low_close, medium create, medium close, high create, high close, critical create critical close
        //build an array of days in longs starts and ends to compare to the queried incident_time
        long days[][] = new long[(int)diffDays][2];
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(start_date);
        
        for(int a = 0; a < (int)diffDays; a++)
        {       
            //store the start of a day
            cal.set(cal.HOUR_OF_DAY, 0);
            cal.set(cal.MINUTE, 0);
            cal.set(cal.SECOND, 0);
            days[a][0] = cal.getTimeInMillis(); //start
            chart_data_start_date = cal.getTime(); 
            //initialize the return_string
            return_string[a][0] = day_month.format(cal.getTime());
            return_string[a][1] = month_day_year.format(cal.getTime());      
            
            //initialize the create_close array
            create_close[a][0]= 0.0;
            create_close[a][1]= 0.0;
            create_close[a][2]= 0.0;
            create_close[a][3]= 0.0;
            create_close[a][4]= 0.0;
            create_close[a][5]= 0.0;
            create_close[a][6]= 0.0;
            create_close[a][7]= 0.0;
            
            cal.set(cal.HOUR_OF_DAY, 23);
            cal.set(cal.MINUTE, 59);
            cal.set(cal.SECOND, 59);
            //store the end of the day
            days[a][1] = cal.getTimeInMillis(); //end     
            chart_data_end_date = cal.getTime();
            
            //get date range for the chartdrilldown page
            return_string[a][6] = filter_format.format(chart_data_start_date) + " - " + filter_format.format(chart_data_end_date);
            //add a day to the calendar            
            cal.add(cal.DATE, 1);
        }
        
        //12-Dec,y_value,12/12/2018   
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT * FROM incidents WHERE ((incident_time >= ? AND incident_time <= ?) OR (state='closed' AND closed_date >= ? AND closed_date <= ?)) ORDER BY incident_time ASC";
        }
        else
        {
            ps_query_string = "SELECT * FROM incidents WHERE ((incident_time >= ? AND incident_time <= ?)  OR (state='closed' AND closed_date >= ? AND closed_date <= ?)) AND assigned_group_id='" + group_id + "' ORDER BY incident_time ASC";
        }
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            java.util.Date incident_time = new java.util.Date();
            java.util.Date closed_date = new java.util.Date();
            String state = "";
            String priority = "";
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            stmt.setTimestamp(3, start);
            stmt.setTimestamp(4, end);
            //System.out.println("closure rate q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next())
            {
                try
                {
                    incident_time = rs.getDate("incident_time"); 
                    state = rs.getString("state");
                    priority = rs.getString("priority");
                    //find incident that were created 
                    for(int b = 0; b < days.length;b++)
                    {
                        if(incident_time.getTime() >= days[b][0] && incident_time.getTime() <= days[b][1])
                        {
                            //this is the day to add the incident to
                            if(priority.equalsIgnoreCase("Low"))
                            {
                                create_close[b][0]++;
                            }
                            else
                            {
                                if(priority.equalsIgnoreCase("Medium"))
                                {
                                    create_close[b][2]++;
                                }
                                else
                                {
                                    if(priority.equalsIgnoreCase("High"))
                                    {
                                        create_close[b][4]++;
                                    }
                                    else
                                    {
                                        if(priority.equalsIgnoreCase("Critical"))
                                        {
                                            create_close[b][6]++;
                                        }
                                    }
                                }
                            }
                            b = days.length;
                        }
                    } 

                    //find incident that were closed 
                    closed_date = rs.getDate("closed_date"); 
                    for(int c = 0; c < days.length;c++)
                    {
                        if(state.equalsIgnoreCase("closed") && closed_date.getTime() >= days[c][0] && closed_date.getTime() <= days[c][1])
                        {
                            //this is the day to add the incident to
                            if(priority.equalsIgnoreCase("Low"))
                            {
                                create_close[c][1]++;
                            }
                            else
                            {
                                if(priority.equalsIgnoreCase("Medium"))
                                {
                                    create_close[c][3]++;
                                }
                                else
                                {
                                    if(priority.equalsIgnoreCase("High"))
                                    {
                                        create_close[c][5]++;
                                    }
                                    else
                                    {
                                        if(priority.equalsIgnoreCase("Critical"))
                                        {
                                            create_close[c][7]++;
                                        }
                                    }
                                }
                            }
                            c = days.length;
                        }
                    }
                }
                catch(Exception e)
                {
                    System.out.println("Bad record in get_chart_data.incident_dashboard_closure_rate ignoring incident_id=" + rs.getString("id"));
                }
            }                         
            stmt.close();
            //calc and populate the return_string
            //12-Dec,12/12/2018,Low_Rate,Med_Rate,Hi_Rate,Critical_Rate   
            for(int d = 0; d < create_close.length;d++)
            {
                //low
                if(create_close[d][0] == 0.0)
                {
                    return_string[d][2] = "0";
                }
                else
                {
                    Double value = (create_close[d][1] / create_close[d][0]) * 100;      
                    return_string[d][2] = df2.format(value);     
                }
                //medium
                if(create_close[d][2] == 0.0)
                {
                    return_string[d][3] = "0";
                }
                else
                {
                    Double value = (create_close[d][3] / create_close[d][2]) * 100;      
                    return_string[d][3] = df2.format(value);
                }
                //high
                if(create_close[d][4] == 0.0)
                {
                    return_string[d][4] = "0";
                }
                else
                {
                    Double value = (create_close[d][5] / create_close[d][4]) * 100;      
                    return_string[d][4] = df2.format(value);
                }
                //critical
                if(create_close[d][6] == 0.0)
                {
                    return_string[d][5] = "0";
                }
                else
                {
                    Double value = (create_close[d][7] / create_close[d][6]) * 100;      
                    return_string[d][5] = df2.format(value);
                }
                logger.error("---" + return_string[d][2] + "---" + return_string[d][3] + "---" + return_string[d][4] + "---" + return_string[d][5]);
            }
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chart_data.incident_dashboard_closure_rate:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chart_data.incident_dashboard_closure_rate:=" + exc);
	}
        return return_string;
    }
    public static String[][] incident_dashboard_closure_rate_customer(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {      
        //System.out.println("Start incident_dashboard_closure_rate_customer");
        DecimalFormat df2 = new DecimalFormat(".##");
        SimpleDateFormat day_month = new SimpleDateFormat("dd-MMM");
        SimpleDateFormat month_day_year = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
        java.util.Date chart_data_start_date = new java.util.Date();
        java.util.Date chart_data_end_date = new java.util.Date();
        
        long startTime = start_date.getTime();
        long endTime = end_date.getTime();
        long diffTime = endTime - startTime;
        long diffDays = diffTime / (1000 * 60 * 60 * 24); //number of days
        //System.out.println("number of days=" + diffDays);
        
        String return_string[][] = new String[(int)diffDays][7]; //12-Dec,12/12/2018,Low_Rate,Med_Rate,Hi_Rate,Critical_Rate,date_range   
        Double create_close[][] = new Double[(int)diffDays][8]; //low_create, low_close, medium create, medium close, high create, high close, critical create critical close
        //build an array of days in longs starts and ends to compare to the queried incident_time
        long days[][] = new long[(int)diffDays][2];
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(start_date);
        
        for(int a = 0; a < (int)diffDays; a++)
        {       
            //store the start of a day
            cal.set(cal.HOUR_OF_DAY, 0);
            cal.set(cal.MINUTE, 0);
            cal.set(cal.SECOND, 0);
            days[a][0] = cal.getTimeInMillis(); //start
            chart_data_start_date = cal.getTime(); 
            //initialize the return_string
            return_string[a][0] = day_month.format(cal.getTime());
            return_string[a][1] = month_day_year.format(cal.getTime());      
            
            //initialize the create_close array
            create_close[a][0]= 0.0;
            create_close[a][1]= 0.0;
            create_close[a][2]= 0.0;
            create_close[a][3]= 0.0;
            create_close[a][4]= 0.0;
            create_close[a][5]= 0.0;
            create_close[a][6]= 0.0;
            create_close[a][7]= 0.0;
            
            cal.set(cal.HOUR_OF_DAY, 23);
            cal.set(cal.MINUTE, 59);
            cal.set(cal.SECOND, 59);
            //store the end of the day
            days[a][1] = cal.getTimeInMillis(); //end     
            chart_data_end_date = cal.getTime();
            
            //get date range for the chartdrilldown page
            return_string[a][6] = filter_format.format(chart_data_start_date) + " - " + filter_format.format(chart_data_end_date);
            //add a day to the calendar            
            cal.add(cal.DATE, 1);
        }
        
        //12-Dec,y_value,12/12/2018   
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT * FROM incidents WHERE ((incident_time >= ? AND incident_time <= ?) OR (closed_date >= ? AND closed_date <= ? AND state='Closed')) ORDER BY incident_time ASC";
        }
        else
        {
            ps_query_string = "SELECT * FROM incidents WHERE ((incident_time >= ? AND incident_time <= ?) OR (closed_date >= ? AND closed_date <= ? AND state='Closed')) AND caller_group_id='" + group_id + "' ORDER BY incident_time ASC";
        }
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            java.util.Date incident_time = new java.util.Date();
            java.util.Date closed_date = new java.util.Date();
            String state = "";
            String priority = "";
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            stmt.setTimestamp(3, start);
            stmt.setTimestamp(4, end);
            ResultSet rs = stmt.executeQuery();
            //System.out.println("get_chart_data=" + stmt);
            while(rs.next())
            {
                try
                {
                    incident_time = rs.getDate("incident_time"); 
                    state = rs.getString("state");
                    priority = rs.getString("priority");
                    //find incident that were created 
                    for(int b = 0; b < days.length;b++)
                    {
                        if(incident_time.getTime() >= days[b][0] && incident_time.getTime() <= days[b][1])
                        {
                            //this is the day to add the incident to
                            if(priority.equalsIgnoreCase("Low"))
                            {
                                create_close[b][0]++;
                            }
                            else
                            {
                                if(priority.equalsIgnoreCase("Medium"))
                                {
                                    create_close[b][2]++;
                                }
                                else
                                {
                                    if(priority.equalsIgnoreCase("High"))
                                    {
                                        create_close[b][4]++;
                                    }
                                    else
                                    {
                                        if(priority.equalsIgnoreCase("Critical"))
                                        {
                                            create_close[b][6]++;
                                        }
                                    }
                                }
                            }
                            b = days.length;
                        }
                    } 

                    //find incident that were closed 
                    closed_date = rs.getDate("closed_date"); 
                    for(int c = 0; c < days.length;c++)
                    {
                        if(state.equalsIgnoreCase("closed") && closed_date.getTime() >= days[c][0] && closed_date.getTime() <= days[c][1])
                        {
                            //this is the day to add the incident to
                            if(priority.equalsIgnoreCase("Low"))
                            {
                                create_close[c][1]++;
                            }
                            else
                            {
                                if(priority.equalsIgnoreCase("Medium"))
                                {
                                    create_close[c][3]++;
                                }
                                else
                                {
                                    if(priority.equalsIgnoreCase("High"))
                                    {
                                        create_close[c][5]++;
                                    }
                                    else
                                    {
                                        if(priority.equalsIgnoreCase("Critical"))
                                        {
                                            create_close[c][7]++;
                                        }
                                    }
                                }
                            }
                            c = days.length;
                        }
                    } 
                }
                catch(Exception e)
                {
                    System.out.println("Bad incdient record, ignoring incident_id=" + rs.getString("id") + " in get_chart_data.incident_dashboard_closure_rate_customer=" + e);
                }
            }                         
            stmt.close();
            //calc and populate the return_string
            //12-Dec,12/12/2018,Low_Rate,Med_Rate,Hi_Rate,Critical_Rate   
            for(int d = 0; d < create_close.length;d++)
            {
                //System.out.println("low created=" + create_close[d][0] + " low closed=" + create_close[d][1]);
                //low
                if(create_close[d][0] == 0)
                {
                    return_string[d][2] = "0";
                }
                else
                {
                    Double value = (create_close[d][1] / create_close[d][0]) * 100;                    
                    return_string[d][2] = (df2.format(value));    
                }
                //medium
                if(create_close[d][2] == 0)
                {
                    return_string[d][3] = "0";
                }
                else
                {
                    Double value = (create_close[d][3] / create_close[d][2]) * 100;  
                    return_string[d][3] = df2.format(value);
                }
                //high
                if(create_close[d][4] == 0)
                {
                    return_string[d][4] = "0";
                }
                else
                {
                    Double value = (create_close[d][5] / create_close[d][4]) * 100;  
                    return_string[d][4] = df2.format(value);
                }
                //critical
                if(create_close[d][6] == 0)
                {
                    return_string[d][5] = "0";
                }
                else
                {
                    Double value = (create_close[d][7] / create_close[d][6]) * 100; 
                    return_string[d][5] = df2.format(value);
                }
            }
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chart_data.incident_dashboard_closure_rate_customer:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chart_data.incident_dashboard_closure_rate_customer:=" + exc);
	}
        return return_string;
    }
    public static String[][] request_support_dashboard_queue_by_priority_support(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        String return_string[][] = {{"Critical", "0", "Critical", "#a781e4"}, {"High", "0", "High", "#da5656"}, {"Medium", "0", "Medium", "#e8aa4e"}, {"Low", "0", "Low", "#6dba95"}};  
        //name: "Critical",y: 1,priority: "Critical",color: "#ef5350"
        SimpleDateFormat timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT COUNT(*) AS value FROM request WHERE (request_date >= ? AND request_date <= ?) AND priority=? AND state NOT LIKE '%closed%'";
        }
        else
        {
            ps_query_string = "SELECT COUNT(*) as value FROM request WHERE (request_date >= ? AND request_date <= ?) AND priority=? AND assigned_group_id='" + group_id + "' AND state NOT LIKE '%closed%'";
        }
        String priorities[] = {"Critical","High","Medium","Low"};
        PreparedStatement stmt;
        try 
        {
            int value = 0;
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            for(int a = 0; a < priorities.length;a++)
            {
                
                stmt.setTimestamp(1, start);
                stmt.setTimestamp(2, end);
                stmt.setString(3, priorities[a]);
                ResultSet rs = stmt.executeQuery();
                while(rs.next())
                {
                    value = rs.getInt("value");  
                    //System.out.println("queue stmt=" + stmt.toString());
                    //System.out.println("queue stmt returned=" + value);
                }
                return_string[a][1] = String.valueOf(value);
            }            
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chart_data.request_support_dashboard_queue_by_priority:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chart_data.request_support_dashboard_queue_by_priority:=" + exc);
	}
        return return_string;
    }
    public static String[][] request_support_dashboard_queue_by_priority_customer(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        String return_string[][] = {{"Critical", "0", "Critical", "#a781e4"}, {"High", "0", "High", "#da5656"}, {"Medium", "0", "Medium", "#e8aa4e"}, {"Low", "0", "Low", "#6dba95"}};  
        //name: "Critical",y: 1,priority: "Critical",color: "#ef5350"
        SimpleDateFormat timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT COUNT(*) AS value FROM request WHERE (request_date >= ? AND request_date <= ?) AND priority=? AND state NOT LIKE '%closed%'";
        }
        else
        {
            ps_query_string = "SELECT COUNT(*) as value FROM request WHERE (request_date >= ? AND request_date <= ?) AND priority=? AND requested_for_group_id='" + group_id + "' AND state NOT LIKE '%closed%'";
        }
        String priorities[] = {"Critical","High","Medium","Low"};
        PreparedStatement stmt;
        try 
        {
            int value = 0;
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            for(int a = 0; a < priorities.length;a++)
            {
                
                stmt.setTimestamp(1, start);
                stmt.setTimestamp(2, end);
                stmt.setString(3, priorities[a]);
                ResultSet rs = stmt.executeQuery();
                while(rs.next())
                {
                    value = rs.getInt("value");  
                    //System.out.println("queue stmt=" + stmt.toString());
                    //System.out.println("queue stmt returned=" + value);
                }
                return_string[a][1] = String.valueOf(value);
            }            
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chart_data.request_support_dashboard_queue_by_priority_customer:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chart_data.request_support_dashboard_queue_by_priority_customer:=" + exc);
	}
        return return_string;
    }
    public static String[][] request_dashboard_number_of_requests_support(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {       
        SimpleDateFormat day_month = new SimpleDateFormat("dd-MMM");
        SimpleDateFormat month_day_year = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
        long startTime = start_date.getTime();
        long endTime = end_date.getTime();
        long diffTime = endTime - startTime;
        long diffDays = diffTime / (1000 * 60 * 60 * 24); //number of days
        //System.out.println("number of days=" + diffDays);
        java.util.Date chart_data_start_date = new java.util.Date();
        java.util.Date chart_data_end_date = new java.util.Date();
        
        String return_string[][] = new String[(int)diffDays][4]; //12-Dec,y_value,12/12/2018   
        //build an array of days in longs starts and ends to compare to the queried incident_time
        long days[][] = new long[(int)diffDays][2];
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(start_date);
        
        for(int a = 0; a < (int)diffDays; a++)
        {            
            cal.set(cal.HOUR_OF_DAY, 0);
            cal.set(cal.MINUTE, 0);
            cal.set(cal.SECOND, 0);
            days[a][0] = cal.getTimeInMillis(); //start
            chart_data_start_date = cal.getTime();
            
            //initialize the return_string
            return_string[a][0] = day_month.format(cal.getTime());
            return_string[a][1] = "0";
            return_string[a][2] = month_day_year.format(cal.getTime());     
            
            cal.set(cal.HOUR_OF_DAY, 23);
            cal.set(cal.MINUTE, 59);
            cal.set(cal.SECOND, 59);
            days[a][1] = cal.getTimeInMillis(); //end     
            chart_data_end_date = cal.getTime();
            
            //get date range for the chartdrilldown page
            return_string[a][3] = filter_format.format(chart_data_start_date) + " - " + filter_format.format(chart_data_end_date);
            
            //add a day to the calendar            
            cal.add(cal.DATE, 1);
        }
        
        //12-Dec,y_value,12/12/2018   
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT * FROM request WHERE (request_date >= ? AND request_date <= ?) ORDER BY request_date ASC";
        }
        else
        {
            ps_query_string = "SELECT * FROM request WHERE (request_date >= ? AND request_date <= ?)AND assigned_group_id='" + group_id + "' ORDER BY request_date ASC";
        }
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            java.util.Date request_date = new java.util.Date();
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next())
            {
                request_date = rs.getDate("request_date"); 
                for(int b = 0; b < days.length;b++)
                {
                    if(request_date.getTime() >= days[b][0] && request_date.getTime() <= days[b][1])
                    {
                        
                        //this is the day to add the incident to
                        int current_value = Integer.parseInt(return_string[b][1]);
                        current_value++;
                        return_string[b][1] = String.valueOf(current_value);
                        b = days.length;                        
                    }
                }                
            }                         
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chart_data.request_dashboard_number_of_requests_support:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chart_data.request_dashboard_number_of_requests_support:=" + exc);
	}
        return return_string;
    }
    public static String[][] request_dashboard_number_of_requests_customer(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {       
        SimpleDateFormat day_month = new SimpleDateFormat("dd-MMM");
        SimpleDateFormat month_day_year = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
        long startTime = start_date.getTime();
        long endTime = end_date.getTime();
        long diffTime = endTime - startTime;
        long diffDays = diffTime / (1000 * 60 * 60 * 24); //number of days
        //System.out.println("number of days=" + diffDays);
        java.util.Date chart_data_start_date = new java.util.Date();
        java.util.Date chart_data_end_date = new java.util.Date();
        
        String return_string[][] = new String[(int)diffDays][4]; //12-Dec,y_value,12/12/2018   
        //build an array of days in longs starts and ends to compare to the queried incident_time
        long days[][] = new long[(int)diffDays][2];
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(start_date);
        
        for(int a = 0; a < (int)diffDays; a++)
        {            
            cal.set(cal.HOUR_OF_DAY, 0);
            cal.set(cal.MINUTE, 0);
            cal.set(cal.SECOND, 0);
            days[a][0] = cal.getTimeInMillis(); //start
            chart_data_start_date = cal.getTime();
            
            //initialize the return_string
            return_string[a][0] = day_month.format(cal.getTime());
            return_string[a][1] = "0";
            return_string[a][2] = month_day_year.format(cal.getTime());     
            
            cal.set(cal.HOUR_OF_DAY, 23);
            cal.set(cal.MINUTE, 59);
            cal.set(cal.SECOND, 59);
            days[a][1] = cal.getTimeInMillis(); //end     
            chart_data_end_date = cal.getTime();
            
            //get date range for the chartdrilldown page
            return_string[a][3] = filter_format.format(chart_data_start_date) + " - " + filter_format.format(chart_data_end_date);
            
            //add a day to the calendar            
            cal.add(cal.DATE, 1);
        }
        
        //12-Dec,y_value,12/12/2018   
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT * FROM request WHERE (request_date >= ? AND request_date <= ?) ORDER BY request_date ASC";
        }
        else
        {
            ps_query_string = "SELECT * FROM request WHERE (request_date >= ? AND request_date <= ?)AND requested_for_group_id='" + group_id + "' ORDER BY request_date ASC";
        }
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            java.util.Date request_date = new java.util.Date();
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next())
            {
                request_date = rs.getDate("request_date"); 
                for(int b = 0; b < days.length;b++)
                {
                    if(request_date.getTime() >= days[b][0] && request_date.getTime() <= days[b][1])
                    {
                        //this is the day to add the incident to
                        int current_value = Integer.parseInt(return_string[b][1]);
                        current_value++;
                        return_string[b][1] = String.valueOf(current_value);
                        b = days.length;
                    }
                }                
            }                         
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chart_data.request_dashboard_number_of_requests_customer:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chart_data.request_dashboard_number_of_requests_customer:=" + exc);
	}
        return return_string;
    }
    public static String[][] request_dashboard_top_5_categories_support(Connection con, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {       
        String return_string[][] = {{"No Requests","0","0"}}; //category,y_value,assigned_group_id   
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        ps_query_string = "SELECT assigned_group_id, count(*) AS c_num FROM request WHERE (request_date >= ? AND request_date <=?) GROUP BY assigned_group_id ORDER BY c_num DESC LIMIT 5";
        PreparedStatement stmt;
        try 
        {
            ArrayList <String[]> all_group_info = db.get_groups.all(con);
            
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }  
            if(count > 0)
            {
                return_string = new String[count][3];
            }
            rs.beforeFirst();
            count = 0;
            while(rs.next())
            {
                for(int a = 0; a < all_group_info.size();a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString(1)))
                    {
                        return_string[count][0] = this_group[1];
                        return_string[count][2] = this_group[0];
                        a = all_group_info.size();
                    }
                }
                return_string[count][1] = rs.getString(2);
                
                count++;
            } 
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chart_data.request_dashboard_top_5_categories_support:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chart_data.request_dashboard_top_5_categories_support:=" + exc);
	}
        return return_string;
    } 
    public static String[][] request_dashboard_top_5_categories_customer(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {       
        String return_string[][] = {{"No Requests","0","0"}}; //category,y_value,assigned_group_id   
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        ps_query_string = "SELECT assigned_group_id, count(*) AS c_num FROM request WHERE (request_date >= ? AND request_date <=?) AND requested_for_group_id=? GROUP BY requested_for_group_id ORDER BY c_num DESC LIMIT 5";
        PreparedStatement stmt;
        try 
        {
            ArrayList<String[]> all_group_info = db.get_groups.all(con);
            
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            stmt.setString(3, group_id);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }  
            if(count > 0)
            {
                return_string = new String[count][3];
            }
            rs.beforeFirst();
            count = 0;
            while(rs.next())
            {
                for(int a = 0; a < all_group_info.size();a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString(1)))
                    {
                        return_string[count][0] = this_group[1];
                        return_string[count][2] = this_group[0];
                        a = all_group_info.size();
                    }
                }
                return_string[count][1] = rs.getString(2);
                
                count++;
            } 
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chart_data.request_dashboard_top_5_categories_customer:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chart_data.request_dashboard_top_5_categories_customer:=" + exc);
	}
        return return_string;
    } 
    public static String[][] request_dashboard_closure_rate_support(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {       
        DecimalFormat df2 = new DecimalFormat(".##");
        SimpleDateFormat day_month = new SimpleDateFormat("dd-MMM");
        SimpleDateFormat month_day_year = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
        java.util.Date chart_data_start_date = new java.util.Date();
        java.util.Date chart_data_end_date = new java.util.Date();
        
        long startTime = start_date.getTime();
        long endTime = end_date.getTime();
        long diffTime = endTime - startTime;
        long diffDays = diffTime / (1000 * 60 * 60 * 24); //number of days
        //System.out.println("number of days=" + diffDays);
        
        String return_string[][] = new String[(int)diffDays][7]; //12-Dec,12/12/2018,Low_Rate,Med_Rate,Hi_Rate,Critical_Rate, date_range   
        Double create_close[][] = new Double[(int)diffDays][8]; //low_create, low_close, medium create, medium close, high create, high close, critical create critical close
        //build an array of days in longs starts and ends to compare to the queried incident_time
        long days[][] = new long[(int)diffDays][2];
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(start_date);
        
        for(int a = 0; a < (int)diffDays; a++)
        {       
            //store the start of a day
            cal.set(cal.HOUR_OF_DAY, 0);
            cal.set(cal.MINUTE, 0);
            cal.set(cal.SECOND, 0);
            days[a][0] = cal.getTimeInMillis(); //start
            chart_data_start_date = cal.getTime(); 
            
            //initialize the return_string
            return_string[a][0] = day_month.format(cal.getTime());
            return_string[a][1] = month_day_year.format(cal.getTime());      
            
            //initialize the create_close array
            create_close[a][0]= 0.0;
            create_close[a][1]= 0.0;
            create_close[a][2]= 0.0;
            create_close[a][3]= 0.0;
            create_close[a][4]= 0.0;
            create_close[a][5]= 0.0;
            create_close[a][6]= 0.0;
            create_close[a][7]= 0.0;
            
            cal.set(cal.HOUR_OF_DAY, 23);
            cal.set(cal.MINUTE, 59);
            cal.set(cal.SECOND, 59);
            //store the end of the day
            days[a][1] = cal.getTimeInMillis(); //end     
            chart_data_end_date = cal.getTime();
            
            //get date range for the chartdrilldown page
            return_string[a][6] = filter_format.format(chart_data_start_date) + " - " + filter_format.format(chart_data_end_date);
            //add a day to the calendar            
            cal.add(cal.DATE, 1);
        }
        
        //12-Dec,y_value,12/12/2018   
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT * FROM request WHERE (request_date >= ? AND request_date <= ?) OR (state LIKE '%closed%' AND closed_date >= ? AND closed_date <= ?) ORDER BY request_date ASC";
        }
        else
        {
            ps_query_string = "SELECT * FROM request WHERE (request_date >= ? AND request_date <= ?)  OR (state LIKE '%closed%' AND closed_date >= ? AND closed_date <= ?) AND assigned_group_id='" + group_id + "' ORDER BY request_date ASC";
        }
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            java.util.Date request_date = new java.util.Date();
            java.util.Date closed_date = new java.util.Date();
            String state = "";
            String priority = "";
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            stmt.setTimestamp(3, start);
            stmt.setTimestamp(4, end);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next())
            {
                request_date = rs.getDate("request_date"); 
                state = rs.getString("state").toLowerCase(); //Why? contains is case sensitive
                priority = rs.getString("priority");
                //find incident that were created 
                for(int b = 0; b < days.length;b++)
                {
                    if(request_date.getTime() >= days[b][0] && request_date.getTime() <= days[b][1])
                    {
                        //this is the day to add the incident to
                        if(priority.equalsIgnoreCase("Low"))
                        {
                            create_close[b][0]++;
                        }
                        else
                        {
                            if(priority.equalsIgnoreCase("Medium"))
                            {
                                create_close[b][2]++;
                            }
                            else
                            {
                                if(priority.equalsIgnoreCase("High"))
                                {
                                    create_close[b][4]++;
                                }
                                else
                                {
                                    if(priority.equalsIgnoreCase("Critical"))
                                    {
                                        create_close[b][6]++;
                                    }
                                }
                            }
                        }
                        b = days.length;
                    }
                } 
                
                //find incident that were closed 
                closed_date = rs.getDate("closed_date"); 
                for(int c = 0; c < days.length;c++)
                {
                    if(state.contains("closed") && closed_date.getTime() >= days[c][0] && closed_date.getTime() <= days[c][1])
                    {
                        //this is the day to add the incident to
                        if(priority.equalsIgnoreCase("Low"))
                        {
                            create_close[c][1]++;
                        }
                        else
                        {
                            if(priority.equalsIgnoreCase("Medium"))
                            {
                                create_close[c][3]++;
                            }
                            else
                            {
                                if(priority.equalsIgnoreCase("High"))
                                {
                                    create_close[c][5]++;
                                }
                                else
                                {
                                    if(priority.equalsIgnoreCase("Critical"))
                                    {
                                        create_close[c][7]++;
                                    }
                                }
                            }
                        }
                        c = days.length;
                    }
                }                 
            }                         
            stmt.close();
            //calc and populate the return_string
            //12-Dec,12/12/2018,Low_Rate,Med_Rate,Hi_Rate,Critical_Rate   
            for(int d = 0; d < create_close.length;d++)
            {
                //low
                if(create_close[d][0] == 0)
                {
                    return_string[d][2] = "0";
                }
                else
                {
                    Double value = (create_close[d][1] / create_close[d][0]) * 100;   
                    try
                    {
                        if(value.isNaN())
                        {
                            value = 0.0;
                        }
                    }
                    catch(Exception e)
                    {
                        
                    }
                    return_string[d][2] = df2.format(value); 
                }
                //medium
                if(create_close[d][2] == 0)
                {
                    return_string[d][3] = "0";
                }
                else
                {
                    Double value = (create_close[d][1] / create_close[d][0]) * 100;
                    try
                    {
                        if(value.isNaN())
                        {
                            value = 0.0;
                        }
                    }
                    catch(Exception e)
                    {
                        
                    }
                    return_string[d][3] = df2.format(value);
                }
                //high
                if(create_close[d][4] == 0)
                {
                    return_string[d][4] = "0";
                }
                else
                {
                    Double value = (create_close[d][1] / create_close[d][0]) * 100;
                    try
                    {
                        if(value.isNaN())
                        {
                            value = 0.0;
                        }
                    }
                    catch(Exception e)
                    {
                        
                    }
                    return_string[d][4] = df2.format(value);
                }
                //critical
                if(create_close[d][6] == 0)
                {
                    return_string[d][5] = "0";
                }
                else
                {
                    Double value = (create_close[d][1] / create_close[d][0]) * 100;
                    try
                    {
                        if(value.isNaN())
                        {
                            value = 0.0;
                        }
                    }
                    catch(Exception e)
                    {
                        
                    }
                    return_string[d][5] = df2.format(value);
                }
            }
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chart_data.request_dashboard_closure_rate_support:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chart_data.request_dashboard_closure_rate_support:=" + exc);
	}
        return return_string;
    }
    public static String[][] request_dashboard_closure_rate_customer(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {       
        DecimalFormat df2 = new DecimalFormat(".##");
        SimpleDateFormat day_month = new SimpleDateFormat("dd-MMM");
        SimpleDateFormat month_day_year = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
        java.util.Date chart_data_start_date = new java.util.Date();
        java.util.Date chart_data_end_date = new java.util.Date();
        
        long startTime = start_date.getTime();
        long endTime = end_date.getTime();
        long diffTime = endTime - startTime;
        long diffDays = diffTime / (1000 * 60 * 60 * 24); //number of days
        //System.out.println("number of days=" + diffDays);
        
        String return_string[][] = new String[(int)diffDays][7]; //12-Dec,12/12/2018,Low_Rate,Med_Rate,Hi_Rate,Critical_Rate, date_range   
        Double create_close[][] = new Double[(int)diffDays][8]; //low_create, low_close, medium create, medium close, high create, high close, critical create critical close
        //build an array of days in longs starts and ends to compare to the queried incident_time
        long days[][] = new long[(int)diffDays][2];
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(start_date);
        
        for(int a = 0; a < (int)diffDays; a++)
        {       
            //store the start of a day
            cal.set(cal.HOUR_OF_DAY, 0);
            cal.set(cal.MINUTE, 0);
            cal.set(cal.SECOND, 0);
            days[a][0] = cal.getTimeInMillis(); //start
            chart_data_start_date = cal.getTime(); 
            
            //initialize the return_string
            return_string[a][0] = day_month.format(cal.getTime());
            return_string[a][1] = month_day_year.format(cal.getTime());      
            
            //initialize the create_close array
            create_close[a][0]= 0.0;
            create_close[a][1]= 0.0;
            create_close[a][2]= 0.0;
            create_close[a][3]= 0.0;
            create_close[a][4]= 0.0;
            create_close[a][5]= 0.0;
            create_close[a][6]= 0.0;
            create_close[a][7]= 0.0;
            
            cal.set(cal.HOUR_OF_DAY, 23);
            cal.set(cal.MINUTE, 59);
            cal.set(cal.SECOND, 59);
            //store the end of the day
            days[a][1] = cal.getTimeInMillis(); //end     
            chart_data_end_date = cal.getTime();
            
            //get date range for the chartdrilldown page
            return_string[a][6] = filter_format.format(chart_data_start_date) + " - " + filter_format.format(chart_data_end_date);
            //add a day to the calendar            
            cal.add(cal.DATE, 1);
        }
        
        //12-Dec,y_value,12/12/2018   
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT * FROM request WHERE (request_date >= ? AND request_date <= ?) OR (state LIKE '%closed%' AND closed_date >= ? AND closed_date <= ?) ORDER BY request_date ASC";
        }
        else
        {
            ps_query_string = "SELECT * FROM request WHERE (request_date >= ? AND request_date <= ?)  OR (state LIKE '%closed%' AND closed_date >= ? AND closed_date <= ?) AND requested_for_group_id='" + group_id + "' ORDER BY request_date ASC";
        }
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            java.util.Date request_date = new java.util.Date();
            java.util.Date closed_date = new java.util.Date();
            String state = "";
            String priority = "";
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            stmt.setTimestamp(3, start);
            stmt.setTimestamp(4, end);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next())
            {
                request_date = rs.getDate("request_date"); 
                state = rs.getString("state").toLowerCase(); //Why? contains is case sensitive
                priority = rs.getString("priority");
                //find incident that were created 
                for(int b = 0; b < days.length;b++)
                {
                    if(request_date.getTime() >= days[b][0] && request_date.getTime() <= days[b][1])
                    {
                        //this is the day to add the incident to
                        if(priority.equalsIgnoreCase("Low"))
                        {
                            create_close[b][0]++;
                        }
                        else
                        {
                            if(priority.equalsIgnoreCase("Medium"))
                            {
                                create_close[b][2]++;
                            }
                            else
                            {
                                if(priority.equalsIgnoreCase("High"))
                                {
                                    create_close[b][4]++;
                                }
                                else
                                {
                                    if(priority.equalsIgnoreCase("Critical"))
                                    {
                                        create_close[b][6]++;
                                    }
                                }
                            }
                        }
                        b = days.length;
                    }
                } 
                
                //find incident that were closed 
                closed_date = rs.getDate("closed_date"); 
                for(int c = 0; c < days.length;c++)
                {
                    if(state.contains("closed") && closed_date.getTime() >= days[c][0] && closed_date.getTime() <= days[c][1])
                    {
                        //this is the day to add the incident to
                        if(priority.equalsIgnoreCase("Low"))
                        {
                            create_close[c][1]++;
                        }
                        else
                        {
                            if(priority.equalsIgnoreCase("Medium"))
                            {
                                create_close[c][3]++;
                            }
                            else
                            {
                                if(priority.equalsIgnoreCase("High"))
                                {
                                    create_close[c][5]++;
                                }
                                else
                                {
                                    if(priority.equalsIgnoreCase("Critical"))
                                    {
                                        create_close[c][7]++;
                                    }
                                }
                            }
                        }
                        c = days.length;
                    }
                }                 
            }                         
            stmt.close();
            //calc and populate the return_string
            //12-Dec,12/12/2018,Low_Rate,Med_Rate,Hi_Rate,Critical_Rate   
            for(int d = 0; d < create_close.length;d++)
            {
                //low
                if(create_close[d][0] == 0)
                {
                    return_string[d][2] = "0";
                }
                else
                {
                    Double value = (create_close[d][1] / create_close[d][0]) * 100;   
                    try
                    {
                        if(value.isNaN())
                        {
                            value = 0.0;
                        }
                    }
                    catch(Exception e)
                    {
                        
                    }
                    return_string[d][2] = df2.format(value); 
                }
                //medium
                if(create_close[d][2] == 0)
                {
                    return_string[d][3] = "0";
                }
                else
                {
                    Double value = (create_close[d][1] / create_close[d][0]) * 100;
                    try
                    {
                        if(value.isNaN())
                        {
                            value = 0.0;
                        }
                    }
                    catch(Exception e)
                    {
                        
                    }
                    return_string[d][3] = df2.format(value);
                }
                //high
                if(create_close[d][4] == 0)
                {
                    return_string[d][4] = "0";
                }
                else
                {
                    Double value = (create_close[d][1] / create_close[d][0]) * 100;
                    try
                    {
                        if(value.isNaN())
                        {
                            value = 0.0;
                        }
                    }
                    catch(Exception e)
                    {
                        
                    }
                    return_string[d][4] = df2.format(value);
                }
                //critical
                if(create_close[d][6] == 0)
                {
                    return_string[d][5] = "0";
                }
                else
                {
                    Double value = (create_close[d][1] / create_close[d][0]) * 100;
                    try
                    {
                        if(value.isNaN())
                        {
                            value = 0.0;
                        }
                    }
                    catch(Exception e)
                    {
                        
                    }
                    return_string[d][5] = df2.format(value);
                }
            }
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chart_data.request_dashboard_closure_rate_customer:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chart_data.request_dashboard_closure_rate_customer:=" + exc);
	}
        return return_string;
    }
    public static String[][] contact_dashboard_number_of_unique_users_per_day(Connection con, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {       
        SimpleDateFormat day_month = new SimpleDateFormat("dd-MMM");
        SimpleDateFormat month_day_year = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
        
        long startTime = start_date.getTime();
        long endTime = end_date.getTime();
        long diffTime = endTime - startTime;
        long diffDays = diffTime / (1000 * 60 * 60 * 24); //number of days
        //System.out.println("number of days=" + diffDays);
        
        String return_string[][] = new String[(int)diffDays][4]; //12-Dec,y_value,12/12/2018,01/02/2019 12:00 AM - 02/01/2019 12:00 AM   
        //build an array of days in longs starts and ends to compare to the queried incident_time
        long days[][] = new long[(int)diffDays][2];
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(start_date);
        
        java.util.Date chart_data_start_date = new java.util.Date();
        java.util.Date chart_data_end_date = new java.util.Date();
        
        // Create a Statement Object
        String ps_query_string = "SELECT count(distinct user_id) AS value FROM article_usages WHERE viewed > ? AND viewed < ?";
        
        PreparedStatement stmt = con.prepareStatement(ps_query_string);
        try 
        {
            for(int a = 0; a < (int)diffDays; a++)
            {            
                cal.set(cal.HOUR_OF_DAY, 0);
                cal.set(cal.MINUTE, 0);
                cal.set(cal.SECOND, 0);
                days[a][0] = cal.getTimeInMillis(); //start
                chart_data_start_date = cal.getTime();
                //initialize the return_string
                return_string[a][0] = day_month.format(cal.getTime());
                return_string[a][1] = "0";
                return_string[a][2] = month_day_year.format(cal.getTime());     

                cal.set(cal.HOUR_OF_DAY, 23);
                cal.set(cal.MINUTE, 59);
                cal.set(cal.SECOND, 59);
                days[a][1] = cal.getTimeInMillis(); //end   
                chart_data_end_date = cal.getTime();  
                //get date range for the chartdrilldown page
                return_string[a][3] = filter_format.format(chart_data_start_date) + " - " + filter_format.format(chart_data_end_date);
                //get the unique value
                stmt.setTimestamp(1, new Timestamp(chart_data_start_date.getTime()));
                stmt.setTimestamp(2, new Timestamp(chart_data_end_date.getTime()));
                ResultSet rs = stmt.executeQuery();
                while(rs.next())
                {
                    return_string[a][1] = rs.getString("value"); 
                }
                //add a day to the calendar            
                cal.add(cal.DATE, 1);
            }             
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chart_data.contact_dashboard_number_of_unique_users_per_day:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chart_data.contact_dashboard_number_of_unique_users_per_day:=" + exc);
	}
        return return_string;
    }
    public static String[][] contact_dashboard_number_article_views_per_day(Connection con, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {       
        SimpleDateFormat day_month = new SimpleDateFormat("dd-MMM");
        SimpleDateFormat month_day_year = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
        
        long startTime = start_date.getTime();
        long endTime = end_date.getTime();
        long diffTime = endTime - startTime;
        long diffDays = diffTime / (1000 * 60 * 60 * 24); //number of days
        //System.out.println("number of days=" + diffDays);
        
        String return_string[][] = new String[(int)diffDays][4]; //12-Dec,y_value,12/12/2018,01/02/2019 12:00 AM - 02/01/2019 12:00 AM   
        //build an array of days in longs starts and ends to compare to the queried incident_time
        long days[][] = new long[(int)diffDays][2];
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(start_date);
        
        java.util.Date chart_data_start_date = new java.util.Date();
        java.util.Date chart_data_end_date = new java.util.Date();
        
        // Create a Statement Object
        String ps_query_string = "SELECT count(*) AS value FROM article_usages WHERE viewed > ? AND viewed < ?";
        
        PreparedStatement stmt = con.prepareStatement(ps_query_string);
        try 
        {
            for(int a = 0; a < (int)diffDays; a++)
            {            
                cal.set(cal.HOUR_OF_DAY, 0);
                cal.set(cal.MINUTE, 0);
                cal.set(cal.SECOND, 0);
                days[a][0] = cal.getTimeInMillis(); //start
                chart_data_start_date = cal.getTime();
                //initialize the return_string
                return_string[a][0] = day_month.format(cal.getTime());
                return_string[a][1] = "0";
                return_string[a][2] = month_day_year.format(cal.getTime());     

                cal.set(cal.HOUR_OF_DAY, 23);
                cal.set(cal.MINUTE, 59);
                cal.set(cal.SECOND, 59);
                days[a][1] = cal.getTimeInMillis(); //end   
                chart_data_end_date = cal.getTime();  
                //get date range for the chartdrilldown page
                return_string[a][3] = filter_format.format(chart_data_start_date) + " - " + filter_format.format(chart_data_end_date);
                //get the unique value
                stmt.setTimestamp(1, new Timestamp(chart_data_start_date.getTime()));
                stmt.setTimestamp(2, new Timestamp(chart_data_end_date.getTime()));
                ResultSet rs = stmt.executeQuery();
                while(rs.next())
                {
                    return_string[a][1] = rs.getString("value"); 
                }
                //add a day to the calendar            
                cal.add(cal.DATE, 1);
            }             
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chart_data.contact_dashboard_number_article_views_per_day:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chart_data.contact_dashboard_number_article_views_per_day:=" + exc);
	}
        return return_string;
    }
    public static String[][] contact_dashboard_fcr_per_day(Connection con, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {       
        SimpleDateFormat day_month = new SimpleDateFormat("dd-MMM");
        SimpleDateFormat month_day_year = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
        
        long startTime = start_date.getTime();
        long endTime = end_date.getTime();
        long diffTime = endTime - startTime;
        long diffDays = diffTime / (1000 * 60 * 60 * 24); //number of days
        //System.out.println("number of days=" + diffDays);
        
        String return_string[][] = new String[(int)diffDays][4]; //12-Dec,y_value,12/12/2018,01/02/2019 12:00 AM - 02/01/2019 12:00 AM   
        //build an array of days in longs starts and ends to compare to the queried incident_time
        long days[][] = new long[(int)diffDays][2];
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(start_date);
        
        java.util.Date chart_data_start_date = new java.util.Date();
        java.util.Date chart_data_end_date = new java.util.Date();
        
        // Create a Statement Object
        String ps_query_string_fcr = "SELECT count(*) as value FROM incidents WHERE incident_time >= ? AND incident_time <= ? AND first_contact_resolution='True'";
        String ps_query_string_total = "SELECT count(*) as value FROM incidents WHERE incident_time >= ? AND incident_time <= ?";
        
        PreparedStatement stmt_fcr = con.prepareStatement(ps_query_string_fcr);
        PreparedStatement stmt_total = con.prepareStatement(ps_query_string_total);
        try 
        {
            for(int a = 0; a < (int)diffDays; a++)
            {      
                double fcr_count = 0;
                double total_count = 0;
                double percent = 0;
                cal.set(cal.HOUR_OF_DAY, 0);
                cal.set(cal.MINUTE, 0);
                cal.set(cal.SECOND, 0);
                days[a][0] = cal.getTimeInMillis(); //start
                chart_data_start_date = cal.getTime();
                //initialize the return_string
                return_string[a][0] = day_month.format(cal.getTime());
                return_string[a][1] = "0";
                return_string[a][2] = month_day_year.format(cal.getTime());     

                cal.set(cal.HOUR_OF_DAY, 23);
                cal.set(cal.MINUTE, 59);
                cal.set(cal.SECOND, 59);
                days[a][1] = cal.getTimeInMillis(); //end   
                chart_data_end_date = cal.getTime();  
                //get date range for the chartdrilldown page
                return_string[a][3] = filter_format.format(chart_data_start_date) + " - " + filter_format.format(chart_data_end_date);
                //get the unique value
                stmt_fcr.setTimestamp(1, new Timestamp(chart_data_start_date.getTime()));
                stmt_fcr.setTimestamp(2, new Timestamp(chart_data_end_date.getTime()));
                ResultSet rs = stmt_fcr.executeQuery();
                while(rs.next())
                {
                    fcr_count = rs.getDouble("value"); 
                }
                stmt_total.setTimestamp(1, new Timestamp(chart_data_start_date.getTime()));
                stmt_total.setTimestamp(2, new Timestamp(chart_data_end_date.getTime()));
                rs = stmt_total.executeQuery();
                while(rs.next())
                {
                    total_count = rs.getDouble("value"); 
                }
                try
                {
                    percent = (fcr_count / total_count) * 100;
                    
                }
                catch(Exception e)
                {
                    percent = 0;
                }
                return_string[a][1] = String.valueOf(String.format("%1$,.2f", percent));
                //add a day to the calendar            
                cal.add(cal.DATE, 1);
            }             
            stmt_fcr.close();
            stmt_total.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_chart_data.contact_dashboard_fcr_per_day:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_chart_data.contact_dashboard_fcr_per_day:=" + exc);
	}
        return return_string;
    }
}

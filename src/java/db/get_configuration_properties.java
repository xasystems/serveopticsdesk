//Copyright 2021 XaSystems, Inc. , All rights reserved.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 *
 * @author server-xc6701
 */
public class get_configuration_properties 
{
    private static Logger logger = LogManager.getLogger();
    
    public static String by_name(Connection con, String name) throws IOException, SQLException
    {
        String returnString = "";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM configuration_properties WHERE name=?");
            stmt.setString(1, name);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                returnString = rs.getString("value");                 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            System.out.println("ERROR SQLException get_configuration_properties.by_name:=" + ex);
        }
        catch(Exception exc) 
        {
            System.out.println("ERROR Exception get_configuration_properties.by_name:=" + exc);
	}
        return returnString;
    }    
    public static HashMap<String,String> get_email_server(Connection con, String context_dir) throws IOException, SQLException
    {
        HashMap return_hashmap = new HashMap();
        return_hashmap.put("get_email_enable","");
        return_hashmap.put("get_email_host","");
        return_hashmap.put("get_email_password","");
        return_hashmap.put("get_email_user","");
        
        String mail_store_protocol = "";
        String query = "";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            //get store type //imap or pop
            stmt = con.prepareStatement("SELECT * FROM configuration_properties WHERE `name`LIKE 'get_email_%'");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                if(support.data_check.null_string(rs.getString("name")).equalsIgnoreCase("get_email_enable"))
                {
                    //System.out.println("get_email_enable=" + support.data_check.null_string(rs.getString("value")));
                    return_hashmap.put("get_email_enable",support.data_check.null_string(rs.getString("value")));
                }
                else if(support.data_check.null_string(rs.getString("name")).equalsIgnoreCase("get_email_host"))
                {
                    return_hashmap.put("get_email_host",support.data_check.null_string(rs.getString("value")));
                }
                else if(support.data_check.null_string(rs.getString("name")).equalsIgnoreCase("get_email_password"))
                {
                    //System.out.println("get_email_password=" + rs.getString("value"));
                    return_hashmap.put("get_email_password",support.data_check.null_string(support.encrypt_utils.decrypt(context_dir, rs.getString("value"))));
                }
                else if(support.data_check.null_string(rs.getString("name")).equalsIgnoreCase("get_email_user"))
                {
                    return_hashmap.put("get_email_user",support.data_check.null_string(rs.getString("value")));
                }
                else if(support.data_check.null_string(rs.getString("name")).equalsIgnoreCase("get_email_store_protocol"))
                {
                    return_hashmap.put("get_email_store_protocol",support.data_check.null_string(rs.getString("value")));
                }
            }
            //Iterator it = return_hashmap.entrySet().iterator();
            //while (it.hasNext()) 
            //{
            //    Map.Entry pair = (Map.Entry)it.next();
            //    System.out.println("get_email_server=" + pair.getKey() + " = " + pair.getValue());
            //}
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_configuration_properties.get_email_server:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_configuration_properties.get_email_server:=" + exc);
	}
        return return_hashmap;
    }
    public static HashMap get_email_server_properties(Connection con, String protocol) throws IOException, SQLException
    {
        HashMap return_hashmap = new HashMap();
        String mail_store_protocol = "";
        String query = "";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            if(protocol.equalsIgnoreCase("imap"))
            {
                query = "SELECT * FROM configuration_properties WHERE `name` LIKE '%.imap%'";
            }
            else
            {
                query = "SELECT * FROM configuration_properties WHERE `name` LIKE '%.pop3%'";
            }            
            stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_hashmap.put(rs.getString("name"), rs.getString("value"));
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_configuration_properties.get_email_server:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_configuration_properties.get_email_server:=" + exc);
	}
        return return_hashmap;
    }
}

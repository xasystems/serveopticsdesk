//Copyright 2021 XaSystems, Inc. , All rights reserved.
package db;

import java.sql.*;
import java.util.LinkedHashMap;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import support.encrypt_utils;


public class db_util extends Thread 
{
    private static Logger logger = LogManager.getLogger();
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static synchronized void take_rest(long time) 
    {
        try 
        {
            Thread.currentThread().sleep(time);
        }
        catch (InterruptedException ey) 
        {
            logger.error("ERROR in db_util.take_rest:=" + ey);
            ey.printStackTrace();
        }
    }
    public static Connection get_contract_connection(String context_dir, HttpSession session)
    {
        Connection con = null;
        String db = session.getAttribute("db").toString();
        String db_host = session.getAttribute("db_host").toString();
        String db_port = session.getAttribute("db_port").toString();
        String db_username = session.getAttribute("db_user").toString();
        String db_password = session.getAttribute("db_password").toString();
        String db_password_encrypted = session.getAttribute("db_password_encrypted").toString();
        String debug = session.getAttribute("debug").toString();
        String log_level = session.getAttribute("log_level").toString();
         
        if(db_password_encrypted.equalsIgnoreCase("true"))
        {
            db_password = encrypt_utils.decrypt(context_dir, db_password);
        }
        
        int trys = 0;
        while(trys < 9 && con == null)
        {
            String url = "jdbc:mysql://" + db_host + ":" + db_port + "/" + db.toLowerCase() + "?useTimezone=true&serverTimezone=UTC&characterEncoding=utf8&useSSL=false&useServerPrepStmts=false&autoReconnect=true";
            //String url = "jdbc:mysql://" + db_host + ":" + db_port + "/" + db.toLowerCase() + "?noDatetimeStringSync=true&useLegacyDatetimeCode=false&sessionVariables=time_zone='-00:00'&characterEncoding=utf8&useSSL=false&useServerPrepStmts=false&autoReconnect=true";
            try
            {
                Class.forName("com.mysql.cj.jdbc.Driver").newInstance(); 
            }
            catch(ClassNotFoundException e)
            {
                trys++;
                System.out.print("get_contract_connection Can't find the database driver.  ClassNotFoundException: ");
                System.out.println(e.getMessage());
                take_rest(1000);
            }
            catch(Exception ep)
            {
                trys++;
                System.out.println("General Exception:=" + ep);
                take_rest(1000);
            }
            
            try
            {
                //System.out.println("desk url=" + url + " user=" + db_username + " password=" + db_password);
                con = DriverManager.getConnection(url, db_username, db_password);                
            }
            catch(SQLException ex)
            {
                trys++;
                System.out.println("==> SQLException: In db_util.get_contract_connection: URL=" + url);
                while(ex != null) 
                {
                    System.out.println("Message:   " + ex.getMessage());
                    System.out.println("SQLState:  " + ex.getSQLState());
                    System.out.println("ErrorCode: " + ex.getErrorCode());
                    ex = ex.getNextException();
                    System.out.println("");
                }
                take_rest(1000);
            }
        }//End while  
        return con;
    }
    public static Connection get_contract_connection_by_contract_id(LinkedHashMap sys_props, String contract_id, String context_dir)
    {
        Connection con = null;
        String db = contract_id;
        String db_host = sys_props.get("db_host").toString();
        String db_port = sys_props.get("db_port").toString();
        String db_username = sys_props.get("db_user").toString();
        String db_password = sys_props.get("db_password").toString();
        String db_password_encrypted = sys_props.get("db_password_encrypted").toString();
        String debug = sys_props.get("debug").toString();
        String log_level = sys_props.get("log_level").toString();
         
        if(db_password_encrypted.equalsIgnoreCase("true"))
        {
            db_password = encrypt_utils.decrypt(context_dir, db_password);
        }
        
        int trys = 0;
        while(trys < 9 && con == null)
        {
            String url = "jdbc:mysql://" + db_host + ":" + db_port + "/" + db.toLowerCase() + "?useTimezone=true&serverTimezone=UTC&characterEncoding=utf8&useSSL=false&useServerPrepStmts=false&autoReconnect=true";
            //String url = "jdbc:mysql://" + db_host + ":" + db_port + "/" + db.toLowerCase() + "?noDatetimeStringSync=true&useLegacyDatetimeCode=false&sessionVariables=time_zone='-00:00'&characterEncoding=utf8&useSSL=false&useServerPrepStmts=false&autoReconnect=true";
            //System.out.println("db_util url=" + url);
            try
            {
                Class.forName("com.mysql.cj.jdbc.Driver").newInstance(); 
            }
            catch(ClassNotFoundException e)
            {
                trys++;
                System.out.print("get_contract_connection_by_contract_id Can't find the database driver.  ClassNotFoundException: ");
                System.out.println(e.getMessage());
                take_rest(1000);
            }
            catch(Exception ep)
            {
                trys++;
                System.out.println("get_contract_connection_by_contract_id General Exception:=" + ep);
                take_rest(1000);
            }
            
            try
            {
                //System.out.println("desk url=" + url + " user=" + db_username + " password=" + db_password);
                con = DriverManager.getConnection(url, db_username, db_password);                
            }
            catch(SQLException ex)
            {
                trys++;
                System.out.println("==> SQLException: In db_util.get_contract_connection_by_contract_id: URL=" + url);
                while(ex != null) 
                {
                    System.out.println("Message:   " + ex.getMessage());
                    System.out.println("SQLState:  " + ex.getSQLState());
                    System.out.println("ErrorCode: " + ex.getErrorCode());
                    ex = ex.getNextException();
                    System.out.println("");
                }
                take_rest(1000);
            }
        }//End while  
        return con;
    }
    public static Connection get_local_connection(LinkedHashMap sys_props, String contract_id, String context_dir)
    {
        Connection con = null;
        String db = sys_props.get("db").toString();
        String db_host = sys_props.get("db_host").toString();
        String db_port = sys_props.get("db_port").toString();
        String db_username = sys_props.get("db_user").toString();
        String db_password = sys_props.get("db_password").toString();
        String db_password_encrypted = sys_props.get("db_password_encrypted").toString();
        String debug = sys_props.get("debug").toString();
        String log_level = sys_props.get("log_level").toString();
         
        if(db_password_encrypted.equalsIgnoreCase("true"))
        {
            db_password = encrypt_utils.decrypt(context_dir, db_password);
        }
        
        int trys = 0;
        while(trys < 9 && con == null)
        {
            String url = "jdbc:mysql://" + db_host + ":" + db_port + "/" + db.toLowerCase() + "?useTimezone=true&serverTimezone=UTC&characterEncoding=utf8&useSSL=false&useServerPrepStmts=false&autoReconnect=true";
            //String url = "jdbc:mysql://" + db_host + ":" + db_port + "/" + db.toLowerCase() + "?noDatetimeStringSync=true&useLegacyDatetimeCode=false&sessionVariables=time_zone='-00:00'&characterEncoding=utf8&useSSL=false&useServerPrepStmts=false&autoReconnect=true";
            //System.out.println("db_util url=" + url);
            try
            {
                Class.forName("com.mysql.cj.jdbc.Driver").newInstance(); 
            }
            catch(ClassNotFoundException e)
            {
                trys++;
                System.out.print("get_local_connection Can't find the database driver.  ClassNotFoundException: ");
                System.out.println(e.getMessage());
                take_rest(1000);
            }
            catch(Exception ep)
            {
                trys++;
                System.out.println("get_local_connection General Exception:=" + ep);
                take_rest(1000);
            }
            
            try
            {
                //System.out.println("desk url=" + url + " user=" + db_username + " password=" + db_password);
                con = DriverManager.getConnection(url, db_username, db_password);                
            }
            catch(SQLException ex)
            {
                trys++;
                System.out.println("==> SQLException: In db_util.get_local_connection: URL=" + url);
                while(ex != null) 
                {
                    System.out.println("Message:   " + ex.getMessage());
                    System.out.println("SQLState:  " + ex.getSQLState());
                    System.out.println("ErrorCode: " + ex.getErrorCode());
                    ex = ex.getNextException();
                    System.out.println("");
                }
                take_rest(1000);
            }
        }//End while  
        return con;
    }
    
}
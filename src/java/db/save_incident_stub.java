/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author rcampbell
 */
public class save_incident_stub 
{
    private static Logger logger = LogManager.getLogger();
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
    private static SimpleDateFormat form_format = new SimpleDateFormat("HH:mm / dd-MMM-YY"); //10:42 / 24-Jan-20
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20
    public static boolean new_with_history(Connection con, Properties incident_props)
    {
        boolean return_boolean = true;
        String incident_id = incident_props.getProperty("incident_id");
        String incident_time = incident_props.getProperty("incident_time");
        String caller_id = incident_props.getProperty("caller_id");
        String description = incident_props.getProperty("description");
        String create_date = incident_props.getProperty("create_date");
        String create_by_id = incident_props.getProperty("create_by_id");
        String contact_method = incident_props.getProperty("contact_method");
        String state = incident_props.getProperty("state");
        String state_date = incident_props.getProperty("state_date");
        String notes = incident_props.getProperty("notes");
        String external_id = incident_props.getProperty("external_id");
        try
        {       
            String insert_incident_query = "INSERT INTO incidents (id,incident_time,caller_id,description,create_date,create_by_id,contact_method,state,state_date,notes,external_id) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps_stmt = con.prepareStatement(insert_incident_query);   
            ps_stmt.setString(1, incident_id);
            ps_stmt.setString(2, incident_time);
            ps_stmt.setString(3, caller_id);
            ps_stmt.setString(4, description);
            ps_stmt.setString(5, create_date);
            ps_stmt.setString(6, create_by_id);
            ps_stmt.setString(7, contact_method);
            ps_stmt.setString(8, state);
            ps_stmt.setString(9, state_date);
            ps_stmt.setString(10, notes);
            ps_stmt.setString(11, external_id);
            System.out.println("save_incident_stub.new_with_history q=" + ps_stmt);
            ps_stmt.execute();
        }
        catch(Exception e)
        {
            System.out.println("ERROR Exception in save_incident_stub.new_with_history incident_table:=" + e);
            logger.error("ERROR Exception in save_incident_stub.new_with_history:=" + e);
            return_boolean = false;
        } 
        try
        {     
            String insert_incident_history_query = "INSERT INTO incident_history (change_time,incident_id,incident_time,caller_id,description,create_date,create_by_id,contact_method,state,state_date,notes,external_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps_stmt2 = con.prepareStatement(insert_incident_history_query);    
            ps_stmt2.setString(1, incident_time);
            ps_stmt2.setString(2, incident_id);
            ps_stmt2.setString(3, incident_time);
            ps_stmt2.setString(4, caller_id);
            ps_stmt2.setString(5, description);
            ps_stmt2.setString(6, create_date);
            ps_stmt2.setString(7, create_by_id);
            ps_stmt2.setString(8, contact_method);
            ps_stmt2.setString(9, state);
            ps_stmt2.setString(10, state_date);
            ps_stmt2.setString(11, notes);
            ps_stmt2.setString(12, external_id);   
            System.out.println("save_incident_stub.new_with_history history q=" + ps_stmt2);
            ps_stmt2.execute();
        }
        catch(Exception e)
        {
            System.out.println("ERROR Exception in save_incident_stub.new_with_history history table:=" + e);
            logger.error("ERROR Exception in save_incident_stub.new_with_history:=" + e);
            return_boolean = false;
        }        
        return return_boolean;
    }
}

/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class get_survey_results 
{
    private static Logger logger = LogManager.getLogger();
    public static int csat_by_trigger_object_timeframe(Connection con, String trigger_object, java.util.Date start, java.util.Date end) throws IOException, SQLException
    {
        int return_int = 0;
        PreparedStatement stmt;
        try 
        {
            Timestamp start_timestamp = new Timestamp(start.getTime());
            Timestamp end_timestamp = new Timestamp(end.getTime());
            // Create a Statement Object
            if(trigger_object.equalsIgnoreCase("all"))
            {
                stmt = con.prepareStatement("SELECT count(*) AS value FROM survey_results WHERE date_sent >= ? AND date_sent <= ?");
            }
            else
            {
                stmt = con.prepareStatement("SELECT count(*) AS value FROM survey_results WHERE trigger_object='"+ trigger_object + "' AND date_sent >= ? AND date_sent <= ?");
            }
            stmt.setTimestamp(1, start_timestamp);
            stmt.setTimestamp(2, end_timestamp);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");
            }           
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_survey_results.csat_by_trigger_object_timeframe:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_survey_results.csat_by_trigger_object_timeframe:=" + exc);
	}
        return return_int;
    }   
    public static int completed_csat_by_trigger_object_timeframe(Connection con, String trigger_object, java.util.Date start, java.util.Date end) throws IOException, SQLException
    {
        int return_int = 0;
        PreparedStatement stmt;
        try 
        {
            Timestamp start_timestamp = new Timestamp(start.getTime());
            Timestamp end_timestamp = new Timestamp(end.getTime());
            // Create a Statement Object
            if(trigger_object.equalsIgnoreCase("all"))
            {
                stmt = con.prepareStatement("SELECT count(*) AS value FROM survey_results WHERE date_completed IS NOT NULL AND date_sent >= ? AND date_sent <= ?");
            }
            else
            {
                stmt = con.prepareStatement("SELECT count(*) AS value FROM survey_results WHERE date_completed IS NOT NULL AND trigger_object='"+ trigger_object + "' AND date_sent >= ? AND date_sent <= ?");
            }
            stmt.setTimestamp(1, start_timestamp);
            stmt.setTimestamp(2, end_timestamp);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");
            }           
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_survey_results.completed_csat_by_trigger_object_timeframe:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_survey_results.completed_csat_by_trigger_object_timeframe:=" + exc);
	}
        return return_int;
    }   
    public static String[] by_survey_key(Connection con, String survey_key) throws IOException, SQLException
    {
        String return_string[] = new String[0];
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        PreparedStatement stmt = null;
        try 
        {
            String query = "SELECT * FROM survey_results WHERE survey_key = ?";
            stmt = con.prepareStatement(query);
            stmt.setString(1,survey_key);
                 
            // Create a Statement Object
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_string = new String[21];
                return_string[0] = rs.getString("id");
                return_string[1] = rs.getString("survey_id");
                return_string[2] = rs.getString("user_id");
                return_string[3] = rs.getString("trigger_object");
                return_string[4] = rs.getString("trigger_id");
                return_string[5] = "";
                try
                {
                    return_string[5] = timestamp_format.format(rs.getDate("date_sent"));
                }
                catch(Exception e)
                {
                    return_string[5] = "";
                }
                return_string[6] = "";
                try
                {
                    return_string[6] = timestamp_format.format(rs.getDate("date_expire"));
                }
                catch(Exception e)
                {
                    return_string[6] = "";
                }
                return_string[7] = "";
                try
                {
                    return_string[7] = timestamp_format.format(rs.getDate("date_reminder_sent"));
                }
                catch(Exception e)
                {
                    return_string[7] = "";
                }
                return_string[8] = "";
                try
                {
                    return_string[8] = timestamp_format.format(rs.getDate("date_completed"));
                }
                catch(Exception e)
                {
                    return_string[8] = "";
                }
                
                
                return_string[9] = rs.getString("answers_json");
                return_string[10] = rs.getString("survey_key");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_survey_results.by_survey_key:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_survey_results.by_survey_key:="+ exc);
	}        
        return return_string;
    } 
    public static String[][] csat_incident_view_group_start_date_stop_date(Connection con, String view, String group_id, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {
        String query = "";
        String return_string[][] = new String[0][0];
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        PreparedStatement stmt = null;
        try 
        {
            if(view.equalsIgnoreCase("support"))
            {
                query = "SELECT survey_results.*, incidents.caller_group_id, incidents.assigned_group_id FROM survey_results INNER JOIN incidents ON survey_results.trigger_id = incidents.id WHERE date_sent >= ? AND date_sent <= ? AND trigger_object='incident' AND incidents.assigned_group_id LIKE ?";
                if(group_id.equalsIgnoreCase("all"))
                {
                    group_id = "%";
                }
            }
            else
            {
                query = "SELECT survey_results.*, incidents.caller_group_id, incidents.assigned_group_id FROM survey_results INNER JOIN incidents ON survey_results.trigger_id = incidents.id WHERE date_sent >= ? AND date_sent <= ? AND trigger_object='incident' AND incidents.caller_group_id LIKE ?";
                if(group_id.equalsIgnoreCase("all"))
                {
                    group_id = "%";
                }
            }
            stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1,new Timestamp(start_date.getTime()));
            stmt.setTimestamp(2,new Timestamp(end_date.getTime())); 
            stmt.setString(3,group_id);
                 
            // Create a Statement Object
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count][20];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("id");
                return_string[count][1] = rs.getString("survey_id");
                return_string[count][2] = rs.getString("user_id");
                return_string[count][3] = rs.getString("trigger_object");
                return_string[count][4] = rs.getString("trigger_id");
                return_string[count][5] = "";
                try
                {
                    return_string[count][5] = timestamp_format.format(rs.getDate("date_sent"));
                }
                catch(Exception e)
                {
                    return_string[count][5] = "";
                }
                return_string[count][6] = "";
                try
                {
                    return_string[count][6] = timestamp_format.format(rs.getDate("date_expire"));
                }
                catch(Exception e)
                {
                    return_string[count][6] = "";
                }
                return_string[count][7] = "";
                try
                {
                    return_string[count][7] = timestamp_format.format(rs.getDate("date_completed"));
                }
                catch(Exception e)
                {
                    return_string[count][7] = "";
                }
                return_string[count][8] = rs.getString("answers_json");
                return_string[count][9] = rs.getString("survey_key");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_survey_results.csat_incident_view_group_start_date_stop_date:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_survey_results.csat_incident_view_group_start_date_stop_date:="+ exc);
	}        
        return return_string;
    }
    public static String[][] by_trigger_object_date_range(Connection con, String trigger_object, String view, String support_group_id,String customer_group_id, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        PreparedStatement stmt = null;
        try 
        {
            String incident_query = "SELECT survey_results.*, incidents.caller_group_id, incidents.assigned_group_id FROM survey_results INNER JOIN incidents ON survey_results.trigger_id = incidents.id WHERE date_sent >= ? AND date_sent <= ? AND trigger_object='incident' AND incidents.caller_group_id LIKE ? AND incidents.assigned_group_id LIKE ?";
            String request_query = "SELECT survey_results.*, request.requested_for_group_id, request.assigned_group_id FROM survey_results INNER JOIN request ON survey_results.trigger_id = request.id WHERE date_sent >= ? AND date_sent <= ? and trigger_object='request' AND request.requested_for_group_id LIKE ? AND request.assigned_group_id LIKE ?";
            
            if(support_group_id.equalsIgnoreCase("all"))
            {
                support_group_id = "%";
            }
            if(customer_group_id.equalsIgnoreCase("all"))
            {
                customer_group_id = "%";
            }
            if(trigger_object.equalsIgnoreCase("incident"))
            {
                stmt = con.prepareStatement(incident_query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
                stmt.setTimestamp(1,new Timestamp(start_date.getTime()));
                stmt.setTimestamp(2,new Timestamp(end_date.getTime())); 
                stmt.setString(3,customer_group_id);
                stmt.setString(4,support_group_id);
            }
            else
            {
                if(trigger_object.equalsIgnoreCase("request"))
                {
                    stmt = con.prepareStatement(request_query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
                    stmt.setTimestamp(1,new Timestamp(start_date.getTime()));
                    stmt.setTimestamp(2,new Timestamp(end_date.getTime())); 
                    stmt.setString(3,customer_group_id);
                    stmt.setString(4,support_group_id);
                }
            }     
            // Create a Statement Object
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count][20];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("id");
                return_string[count][1] = rs.getString("survey_id");
                return_string[count][2] = rs.getString("user_id");
                return_string[count][3] = rs.getString("trigger_object");
                return_string[count][4] = rs.getString("trigger_id");
                return_string[count][5] = "";
                try
                {
                    return_string[count][5] = timestamp_format.format(rs.getDate("date_sent"));
                }
                catch(Exception e)
                {
                    return_string[count][5] = "";
                }
                return_string[count][6] = "";
                try
                {
                    return_string[count][6] = timestamp_format.format(rs.getDate("date_expire"));
                }
                catch(Exception e)
                {
                    return_string[count][6] = "";
                }
                return_string[count][7] = "";
                try
                {
                    return_string[count][7] = timestamp_format.format(rs.getDate("date_completed"));
                }
                catch(Exception e)
                {
                    return_string[count][7] = "";
                }
                return_string[count][8] = rs.getString("answers_json");
                return_string[count][9] = rs.getString("survey_key");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_survey_results.by_trigger_object_date_range:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_survey_results.by_trigger_object_date_range:="+ exc);
	}        
        return return_string;
    }           
    //get survey results for this survey and filter_start and filter_stop and support_group_id and customer_group_id
    public static String[][] for_survey_id_start_date_end_date_support_group_id_customer_group_id(Connection con, String survey_id, java.util.Date start_date, java.util.Date end_date, String support_group_id,String customer_group_id) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        PreparedStatement stmt = null;
        String trigger_object = "";
        
        //get survey info
        String survey_info[] = db.get_surveys.by_id(con, survey_id);
        //System.out.println("get_survey_results.for_survey_id_start_date_end_date_support_group_id_customer_group_id survey_id=" + survey_id);
        trigger_object = survey_info[8];
        
        String incident_query = "SELECT survey_results.*, incidents.caller_group_id, incidents.assigned_group_id FROM survey_results INNER JOIN incidents ON survey_results.trigger_id = incidents.id WHERE survey_results.survey_id = ? AND date_sent >= ? AND date_sent <= ? AND trigger_object='incident' AND incidents.caller_group_id LIKE ? AND incidents.assigned_group_id LIKE ? ORDER BY survey_results.date_sent";
        String request_query = "SELECT survey_results.*, request.requested_for_group_id, request.assigned_group_id FROM survey_results INNER JOIN request ON survey_results.trigger_id = request.external_id WHERE survey_results.survey_id = ? AND date_sent >= ? AND date_sent <= ? and trigger_object='request' AND request.requested_for_group_id LIKE ? AND request.assigned_group_id LIKE ? ORDER BY survey_results.date_sent";
        
        try 
        {
            if(support_group_id.equalsIgnoreCase("all"))
            {
                support_group_id = "%";
            }
            if(customer_group_id.equalsIgnoreCase("all"))
            {
                customer_group_id = "%";
            }
            //is survey incident/request/contact
            if(trigger_object.equalsIgnoreCase("incident"))
            {
                stmt = con.prepareStatement(incident_query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
                stmt.setString(1,survey_id);
                stmt.setTimestamp(2,new Timestamp(start_date.getTime()));
                stmt.setTimestamp(3,new Timestamp(end_date.getTime())); 
                stmt.setString(4,customer_group_id);
                stmt.setString(5,support_group_id);
            }
            else
            {
                if(trigger_object.equalsIgnoreCase("request"))
                {
                    stmt = con.prepareStatement(request_query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
                    stmt.setString(1,survey_id);
                    stmt.setTimestamp(2,new Timestamp(start_date.getTime()));
                    stmt.setTimestamp(3,new Timestamp(end_date.getTime())); 
                    stmt.setString(4,customer_group_id);
                    stmt.setString(5,support_group_id);
                }
                else
                {
                    if(trigger_object.equalsIgnoreCase("contact"))
                    {

                    }
                }
            }
            

            //System.out.println("q=" + stmt);
            // Create a Statement Object
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count][20];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("id");
                return_string[count][1] = rs.getString("survey_id");
                return_string[count][2] = rs.getString("user_id");
                return_string[count][3] = rs.getString("trigger_object");
                return_string[count][4] = rs.getString("trigger_id");
                return_string[count][5] = "";
                try
                {
                    return_string[count][5] = timestamp_format.format(rs.getTimestamp("date_sent"));
                }
                catch(Exception e)
                {
                    return_string[count][5] = "";
                }
                return_string[count][6] = "";
                try
                {
                    return_string[count][6] = timestamp_format.format(rs.getTimestamp("date_expire"));
                }
                catch(Exception e)
                {
                    return_string[count][6] = "";
                }
                return_string[count][7] = "";
                try
                {
                    return_string[count][7] = timestamp_format.format(rs.getTimestamp("date_completed"));
                }
                catch(Exception e)
                {
                    return_string[count][7] = "";
                }
                
                
                return_string[count][8] = rs.getString("answers_json");
                return_string[count][9] = rs.getString("survey_key");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_survey_results.for_survey_id_start_date_end_date_support_group_id_customer_group_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_survey_results.for_survey_id_start_date_end_date_support_group_id_customer_group_id:="+ exc);
	}        
        return return_string;
    }     
    public static String[] is_trending_positive(String[][] survey_results)
    {
        String return_string[] = {"0","neutral","0"}; //negative_count,  negative or neutral, positive_count
        int positive_count = 0;
        int negative_count = 0;
        int positive_survey_count = 0;
        int negative_survey_count = 0;
        int this_survey_positive_count = 0;
        int this_survey_negative_count = 0;
        
        for(int a = 0; a < survey_results.length;a++)
        {
            if(survey_results[a][8] != null) //if answered
            {
                //System.out.println("json=" + survey_results[a][8]);
                this_survey_positive_count = 0;
                this_survey_negative_count = 0;
                //8 is json answers
                String json_answers = survey_results[a][8];
                JsonReader reader = Json.createReader(new StringReader(json_answers));
                JsonObject resultObject = reader.readObject();
                JsonArray answers = null;
                try
                {
                    answers = (JsonArray) resultObject.getJsonArray("answers");
                    //System.out.println("is_trending_positive answers.size=" + answers.size());
                    for(int b = 0; b < answers.size(); b++)
                    {
                        JsonObject this_record = answers.getJsonObject(b); //a single incident record
                        int question_number = Integer.parseInt(this_record.getString("question_number"));
                        String question_type = this_record.getString("question_type");
                        if(question_type.equalsIgnoreCase("true_false"))
                        {
                            int answer_value = Integer.parseInt(this_record.getString("answer_value"));
                            if(answer_value > 3)
                            {
                                this_survey_positive_count++;
                            }
                            else
                            {
                                this_survey_negative_count++;
                            }
                        }
                        else
                        {
                            if(question_type.equalsIgnoreCase("rating_scale_5"))
                            {
                                int answer_value = Integer.parseInt(this_record.getString("answer_value"));
                                if(answer_value > 3)
                                {
                                    this_survey_positive_count++;
                                }
                                else
                                {
                                    this_survey_negative_count++;
                                }
                            }
                            else
                            {
                                if(question_type.equalsIgnoreCase("rating_scale_7"))
                                {
                                    int answer_value = Integer.parseInt(this_record.getString("answer_value"));
                                    if(answer_value > 4)
                                    {
                                        this_survey_positive_count++;
                                    }
                                    else
                                    {
                                        this_survey_negative_count++;
                                    }
                                }
                                else
                                {
                                    if(question_type.equalsIgnoreCase("rating_scale_11"))
                                    {
                                        int answer_value = Integer.parseInt(this_record.getString("answer_value"));
                                        if(answer_value > 8)
                                        {
                                            this_survey_positive_count++;
                                        }
                                        else
                                        {
                                            this_survey_negative_count++;
                                        }
                                    }
                                    else
                                    {
                                        if(question_type.equalsIgnoreCase("opinion"))
                                        {
                                            //DOES NOT COUNT FOR POOP
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch(Exception e)
                {
                    logger.error("Exception in get_survey_results.is_trending_positive=" + e);
                }
            }//end if answered
            if(this_survey_positive_count > this_survey_negative_count)
            {
                positive_count++;
            }
            else
            {
                if(this_survey_positive_count < this_survey_negative_count)
                {
                    negative_count++;
                }
            }
        }//for each survey
        
        if(positive_count > negative_count)
        {
            return_string[0] = String.valueOf(negative_count);
            return_string[1] = "positive";
            return_string[2] = String.valueOf(positive_count);
        }
        else
        {
            if(positive_count < negative_count)
            {
                return_string[0] = String.valueOf(negative_count);
                return_string[1] = "neutral";
                return_string[2] = String.valueOf(positive_count);
            }
            else
            {
                if(positive_count == negative_count)
                {
                    return_string[0] = String.valueOf(negative_count);
                    return_string[1] = "negative";
                    return_string[2] = String.valueOf(positive_count);
                }
            }
        }
        return return_string;
    }
    public static double[] csat_score_for_survey_results(String[][] survey_results)
    {
        double return_double[] = {0.0,0.0,0.0,0.0};  //Sent,Completed,Completion Rate,Avg. Score
        double total_answered_questions = 0;
        double total_question_score = 0;            

        int number_of_completed_surveys = 0;
        int number_of_surveys_sent = survey_results.length;
        
        //get survey results for this survey and filter_start and filter_stop and support_group_id and customer_group_id
        for(int b = 0; b < survey_results.length;b++) //each survey_result
        {
            //8 answers
            if(survey_results[b][8] != null) //if answered
            {
                number_of_completed_surveys++;
                //System.out.println("number_of_completed_surveys=" + number_of_completed_surveys);
                String json_answers = survey_results[b][8];
                JsonReader reader = Json.createReader(new StringReader(json_answers));
                JsonObject resultObject = reader.readObject();
                JsonArray answers = null;
                try
                {
                    //System.out.println("pre answers");
                    answers = (JsonArray) resultObject.getJsonArray("answers");
                    //System.out.println("post answers");
                    //System.out.println("is_trending_positive answers.size=" + answers.size());
                    for(int c = 0; c < answers.size(); c++) //for each answer
                    {
                        JsonObject this_record = answers.getJsonObject(c); //a single incident record  
                        //int question_number = this_record.getInt("question_number");
                        String question_type = this_record.getString("question_type");  //true_false, rating_scale_5,  opinion
                        if(question_type.equalsIgnoreCase("true_false"))
                        {
                            int answer_value = Integer.parseInt(this_record.getString("answer_value"));
                            total_answered_questions++;
                            total_question_score = total_question_score + answer_value;
                        }
                        else
                        {
                            if(question_type.equalsIgnoreCase("rating_scale_5"))
                            {
                                int answer_value = Integer.parseInt(this_record.getString("answer_value"));
                                total_answered_questions++;
                                total_question_score = total_question_score + answer_value;
                            }
                        }
                    }
                }
                catch(Exception e)
                {
                    logger.error("Exception in get_survey_results.csat_score_for_survey_results=" + e);
                }
            }
        }  
        //Sent,Completed,Completion Rate,Avg. Score
        //completed / sent = completion rate
        double percent_answered = ((double)number_of_completed_surveys / (double)number_of_surveys_sent) * (double)100;
        //System.out.println("number_of_completed_surveys=" + number_of_completed_surveys);
        //System.out.println("number_of_surveys_sent=" + number_of_surveys_sent);
        //System.out.println("number_of_completed_surveys / number_of_surveys_sent=" + (double)number_of_completed_surveys / (double)number_of_surveys_sent * (double)100 );
        double avg_score = total_question_score / total_answered_questions;
        return_double[0] = (double)number_of_surveys_sent;
        return_double[1] = (double)number_of_completed_surveys;
        return_double[2] = percent_answered;
        return_double[3] = avg_score;   
        if(number_of_surveys_sent == 0 || number_of_completed_surveys == 0)
        {
            return_double[2] = 0.00;
            return_double[3] = 0.00;
        }
        
        //System.out.println("return_double[2]=" + return_double[2]);
        //System.out.println("number_of_completed_surveys=" + number_of_completed_surveys);
        
        return return_double;
    }  
    public static double[]csat_for_survey_id_survey_question(String survey_id, String question_number, String[][] survey_results)
    {
        double return_double[] = {0.0,0.0,0.0,0.0,0.0,0.0};  //avg_score,1,2,3,4,5
        double avg_score = 0.0;
        int answer_1 = 0;
        int answer_2 = 0;
        int answer_3 = 0;
        int answer_4 = 0;
        int answer_5 = 0;
        double total_answers = 0;
        double total_score = 0;
        for(int b = 0; b < survey_results.length;b++) //each survey_result
        {
            if(survey_results[b][8] != null) //if answered
            {
                String json_answers = survey_results[b][8];
                JsonReader reader = Json.createReader(new StringReader(json_answers));
                JsonObject resultObject = reader.readObject();
                JsonArray answers = null;
                try
                {
                    answers = (JsonArray) resultObject.getJsonArray("answers");
                    //System.out.println("is_trending_positive answers.size=" + answers.size());
                    for(int c = 0; c < answers.size(); c++) //for each answer
                    {
                        JsonObject this_record = answers.getJsonObject(c); //a single incident record
                        int this_question_number = Integer.parseInt(this_record.getString("question_number"));
                        //System.out.println("this_question_number=" + this_question_number);
                        String q_number = String.valueOf(this_question_number);
                        String question_type = this_record.getString("question_type");
                        
                        if(q_number.equalsIgnoreCase(question_number))
                        {
                            if(question_type.equalsIgnoreCase("opinion"))
                            {
                                //System.out.println("opinion answer=" + this_question_number);
                                String answer_string_value = this_record.getString("answer_value");
                                if(answer_string_value.length() > 0)
                                {
                                    answer_1++;
                                }
                            }
                            else
                            {
                                int answer_value = Integer.parseInt(this_record.getString("answer_value"));
                                if(answer_value == 1)
                                {
                                    answer_1++;
                                    total_answers++;
                                    total_score = total_score + 1;
                                }
                                else
                                {
                                    if(answer_value == 2)
                                    {
                                        answer_2++;
                                        total_answers++;
                                        total_score = total_score + 2;
                                    }
                                    else
                                    {
                                        if(answer_value == 3)
                                        {
                                            answer_3++;
                                            total_answers++;
                                            total_score = total_score + 3;
                                        }
                                        else
                                        {
                                            if(answer_value == 4)
                                            {
                                                answer_4++;
                                                total_answers++;
                                                total_score = total_score + 4;
                                            }
                                            else
                                            {
                                                if(answer_value == 5)
                                                {
                                                    answer_5++;
                                                    total_answers++;
                                                    total_score = total_score + 5;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch(Exception e)
                {
                    logger.error("Exception in get_survey_results.csat_for_survey_id_survey_question=" + e);
                }
            }
        } 
        avg_score = total_score / total_answers;
        return_double[0] = avg_score; //avg score
        return_double[1] = answer_1;
        return_double[2] = answer_2;
        return_double[3] = answer_3;
        return_double[4] = answer_4;
        return_double[5] = answer_5;
        return return_double;
    }
    public static double csat_score_for_a_survey(String answers_json)
    {
        double return_double = 0.0; 
        double total_answered_questions = 0;
        double total_question_score = 0;            

        JsonReader reader = Json.createReader(new StringReader(answers_json));
        JsonObject resultObject = reader.readObject();
        JsonArray answers = null;
        try
        {
            answers = (JsonArray) resultObject.getJsonArray("answers");
            //System.out.println("is_trending_positive answers.size=" + answers.size());
            for(int c = 0; c < answers.size(); c++) //for each answer
            {
                JsonObject this_record = answers.getJsonObject(c); //a single incident record
                String question_type = this_record.getString("question_type");
                if(question_type.equalsIgnoreCase("true_false"))
                {
                    int answer_value = Integer.parseInt(this_record.getString("answer_value"));
                    total_answered_questions++;
                    total_question_score = total_question_score + answer_value;
                }
                else
                {
                    if(question_type.equalsIgnoreCase("rating_scale_5"))
                    {
                        int answer_value = Integer.parseInt(this_record.getString("answer_value"));
                        total_answered_questions++;
                        total_question_score = total_question_score + answer_value;
                    }
                }
            }
        }
        catch(Exception e)
        {
            logger.error("Exception in get_survey_results.csat_score_for_a_survey=" + e);
            return_double = 0.0;
        } 
        try
        {
            Double avg_score = total_question_score / total_answered_questions;
            if(avg_score.isNaN())
            {
                return_double = 0.0;
            }
        }
        catch(Exception e)
        {
            return_double = 0.0;
        }
        return return_double;
    } 
    public static String[] get_user_info_from_all_user_array(ArrayList<String[]> users, String user_id)
    {
        String return_string[] = {"","","","","","","","","","","","","","","","","","","",""};
        for(int a = 0; a < users.size();a++)
        {
            String this_user[] = users.get(a);
            if(this_user[0].equalsIgnoreCase(user_id))
            {
                return_string[0] = this_user[0];
                return_string[1] = this_user[1];
                return_string[2] = this_user[2];
                return_string[3] = this_user[3];
                return_string[4] = this_user[4];
                return_string[5] = this_user[5];
                return_string[6] = this_user[6];
                return_string[7] = this_user[7];
                return_string[8] = this_user[8];
                return_string[9] = this_user[9];
                return_string[10] = this_user[10];
                return_string[11] = this_user[11];
                return_string[12] = this_user[12];
                return_string[13] = this_user[13];
                return_string[14] = this_user[14];
                return_string[15] = this_user[15];
                return_string[16] = this_user[16];
                return_string[17] = this_user[17];
                return_string[18] = this_user[18];
                return_string[19] = this_user[19];
            }
        }
        return return_string;
    }
    public static String[][] csat_survey_detail_table(Connection con, String[][] survey_results)
    {
        DecimalFormat two_decimals = new DecimalFormat("0.00");
        String return_string[][] = new String[survey_results.length][6];
        double total_answered_questions = 0;
        double total_question_score = 0;   
        try
        {
            ArrayList<String[]> users = db.get_users.all(con);

            //5 Date Sent,  7 Date Completed, 2 User Name  ,Overall score, 3 trigger_object, 4 ID
            for(int a=0; a < survey_results.length;a++)
            {
                //init the return values
                return_string[a][0] = survey_results[a][5]; //date_sent
                return_string[a][1] = "";  //date_completed
                
                String user_info[] = get_survey_results.get_user_info_from_all_user_array(users, survey_results[a][2]);
                String first_name = user_info[3];
                String last_name = user_info[5];
                return_string[a][2] = first_name + " " + last_name;  //user
                return_string[a][3] = ""; //overall score
                return_string[a][4] = survey_results[a][3]; //trigger_object
                return_string[a][5] = survey_results[a][4]; //trigger_object_id
                
                
                if(survey_results[a][8] != null) //if answered
                {
                    
                    return_string[a][1] = survey_results[a][7];  //date_completed
                    String json_answers = survey_results[a][8];
                    JsonReader reader = Json.createReader(new StringReader(json_answers));
                    JsonObject resultObject = reader.readObject();
                    JsonArray answers = null;
                    try
                    {
                        answers = (JsonArray) resultObject.getJsonArray("answers");

                        //System.out.println("is_trending_positive answers.size=" + answers.size());
                        for(int c = 0; c < answers.size(); c++) //for each answer
                        {
                            JsonObject this_record = answers.getJsonObject(c); //a single incident record
                            //int question_number = this_record.getInt("question_number");
                            String question_type = this_record.getString("question_type");
                            if(question_type.equalsIgnoreCase("true_false"))
                            {
                                int answer_value = Integer.parseInt(this_record.getString("answer_value"));
                                total_answered_questions++;
                                total_question_score = total_question_score + answer_value;
                            }
                            else
                            {
                                if(question_type.equalsIgnoreCase("rating_scale_5"))
                                {
                                    int answer_value = Integer.parseInt(this_record.getString("answer_value"));
                                    total_answered_questions++;
                                    total_question_score = total_question_score + answer_value;
                                }
                            }
                        }
                        try
                        {
                            Double avg_score = total_question_score / total_answered_questions;
                            if(avg_score.isNaN())
                            {
                                return_string[a][3] = "0.0"; //overall score
                            }
                            else
                            {
                                return_string[a][3] = two_decimals.format(avg_score); //overall score; //overall score
                            }
                        }
                        catch(Exception e)
                        {
                            return_string[a][3] = "0.0"; //overall score
                        }
                    }
                    catch(Exception e)
                    {
                        logger.error("Exception in get_survey_results.csat_survey_detail_table  parsing json answers=" + e);
                    }
                }
                else
                {
                    return_string[a][3] = ""; //overall score not answered
                }
                
            }
        }
        catch(Exception e)
        {
            logger.error("Exception in get_survey_results.csat_survey_detail_table=" + e);
        }
        return return_string;
    }
    public static String[][] for_all_csat_date_range(Connection con, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        PreparedStatement stmt = null;
        try 
        {
            String query = "SELECT survey_results FROM survey_results WHERE date_sent >= ? AND date_sent <= ? AND (trigger_object='incident' OR trigger_object='request')";
            stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1,new Timestamp(start_date.getTime()));
            stmt.setTimestamp(2,new Timestamp(end_date.getTime())); 
               
            // Create a Statement Object
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count][20];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("id");
                return_string[count][1] = rs.getString("survey_id");
                return_string[count][2] = rs.getString("user_id");
                return_string[count][3] = rs.getString("trigger_object");
                return_string[count][4] = rs.getString("trigger_id");
                return_string[count][5] = "";
                try
                {
                    return_string[count][5] = timestamp_format.format(rs.getDate("date_sent"));
                }
                catch(Exception e)
                {
                    return_string[count][5] = "";
                }
                return_string[count][6] = "";
                try
                {
                    return_string[count][6] = timestamp_format.format(rs.getDate("date_expire"));
                }
                catch(Exception e)
                {
                    return_string[count][6] = "";
                }
                return_string[count][7] = "";
                try
                {
                    return_string[count][7] = timestamp_format.format(rs.getDate("date_completed"));
                }
                catch(Exception e)
                {
                    return_string[count][7] = "";
                }
                return_string[count][8] = rs.getString("answers_json");
                return_string[count][9] = rs.getString("survey_key");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_survey_results.for_all_csat_date_range:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_survey_results.for_all_csat_date_range:="+ exc);
	}        
        return return_string;
    } 
    public static String[][] for_all_csat_results_for_date_range(Connection con, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        PreparedStatement stmt = null;
        try 
        {
            String query = "SELECT * FROM survey_results WHERE date_sent >= ? AND date_sent <= ?";
            stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1,new Timestamp(start_date.getTime()));
            stmt.setTimestamp(2,new Timestamp(end_date.getTime())); 
               
            // Create a Statement Object
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count][20];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("id");
                return_string[count][1] = rs.getString("survey_id");
                return_string[count][2] = rs.getString("user_id");
                return_string[count][3] = rs.getString("trigger_object");
                return_string[count][4] = rs.getString("trigger_id");
                return_string[count][5] = "";
                try
                {
                    return_string[count][5] = timestamp_format.format(rs.getDate("date_sent"));
                }
                catch(Exception e)
                {
                    return_string[count][5] = "";
                }
                return_string[count][6] = "";
                try
                {
                    return_string[count][6] = timestamp_format.format(rs.getDate("date_expire"));
                }
                catch(Exception e)
                {
                    return_string[count][6] = "";
                }
                return_string[count][7] = "";
                try
                {
                    return_string[count][7] = timestamp_format.format(rs.getDate("date_completed"));
                }
                catch(Exception e)
                {
                    return_string[count][7] = "";
                }
                return_string[count][8] = rs.getString("answers_json");
                return_string[count][9] = rs.getString("survey_key");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_survey_results.for_all_csat_date_range:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_survey_results.for_all_csat_date_range:="+ exc);
	}        
        return return_string;
    } 
    public static String[][] for_all_csat_results_for_date_range_and_trigger_object(Connection con, String trigger_object, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        PreparedStatement stmt = null;
        try 
        {
            String query = "SELECT * FROM survey_results WHERE trigger_object = ? AND date_sent >= ? AND date_sent <= ?";
            stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1,trigger_object);
            stmt.setTimestamp(2,new Timestamp(start_date.getTime()));
            stmt.setTimestamp(3,new Timestamp(end_date.getTime())); 
            
            // Create a Statement Object
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            //System.out.println("count=" + count + " stmt=" + stmt);   
            return_string = new String[count][20];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("id");
                return_string[count][1] = rs.getString("survey_id");
                return_string[count][2] = rs.getString("user_id");
                return_string[count][3] = rs.getString("trigger_object");
                return_string[count][4] = rs.getString("trigger_id");
                return_string[count][5] = "";
                try
                {
                    return_string[count][5] = timestamp_format.format(rs.getDate("date_sent"));
                }
                catch(Exception e)
                {
                    return_string[count][5] = "";
                }
                return_string[count][6] = "";
                try
                {
                    return_string[count][6] = timestamp_format.format(rs.getDate("date_expire"));
                }
                catch(Exception e)
                {
                    return_string[count][6] = "";
                }
                return_string[count][7] = "";
                try
                {
                    return_string[count][7] = timestamp_format.format(rs.getDate("date_completed"));
                }
                catch(Exception e)
                {
                    return_string[count][7] = "";
                }
                return_string[count][8] = rs.getString("answers_json");
                return_string[count][9] = rs.getString("survey_key");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_survey_results.for_all_csat_results_for_date_range_and_trigger_object:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_survey_results.for_all_csat_results_for_date_range_and_trigger_object:="+ exc);
	}        
        return return_string;
    } 
    
    
    
    
    public static String[][] for_survey_id_start_date_end_date(Connection con, String survey_id, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        PreparedStatement stmt = null;
        String trigger_object = "";
        
        //get survey info
        String survey_info[] = db.get_surveys.by_id(con, survey_id);
        //System.out.println("get_survey_results.for_survey_id_start_date_end_date survey_id=" + survey_id);
        trigger_object = survey_info[8];
        
        String query = "SELECT * FROM survey_results WHERE survey_results.survey_id = ? AND date_sent >= ? AND date_sent <= ? ORDER BY survey_results.date_sent";
        try 
        {
            stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1,survey_id);
            stmt.setTimestamp(2,new Timestamp(start_date.getTime()));
            stmt.setTimestamp(3,new Timestamp(end_date.getTime())); 
            //System.out.println("q=" + stmt);
            // Create a Statement Object
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count][20];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("id");
                return_string[count][1] = rs.getString("survey_id");
                return_string[count][2] = rs.getString("user_id");
                return_string[count][3] = rs.getString("trigger_object");
                return_string[count][4] = rs.getString("trigger_id");
                return_string[count][5] = "";
                try
                {
                    return_string[count][5] = timestamp_format.format(rs.getTimestamp("date_sent"));
                }
                catch(Exception e)
                {
                    return_string[count][5] = "";
                }
                return_string[count][6] = "";
                try
                {
                    return_string[count][6] = timestamp_format.format(rs.getTimestamp("date_expire"));
                }
                catch(Exception e)
                {
                    return_string[count][6] = "";
                }
                return_string[count][7] = "";
                try
                {
                    return_string[count][7] = timestamp_format.format(rs.getTimestamp("date_completed"));
                }
                catch(Exception e)
                {
                    return_string[count][7] = "";
                }
                return_string[count][8] = rs.getString("answers_json");
                return_string[count][9] = rs.getString("survey_key");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_survey_results.for_survey_id_start_date_end_date:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_survey_results.for_survey_id_start_date_end_date:="+ exc);
	}        
        return return_string;
    }  
}

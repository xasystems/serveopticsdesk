/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Arrays;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Ralph
 */
public class get_incidents 
{
    private static Logger logger = LogManager.getLogger();
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   

    public static int all_filtered_count(Connection con, List<Map<String, String>> filters) throws IOException, SQLException
    {
        int count = 0;
        List<String> wheres = new ArrayList<>();
        PreparedStatement stmt;
        String query;        
        String where = "";

        try 
        {
            // Processing filters
            for (int i = 0; i < filters.size(); i++)
            {
              String filter_key = filters.get(i).get("key");
              wheres.add(filter_key + " LIKE ? ");
            }
            
            if (wheres.size() > 0)
            {
                where = " WHERE " + String.join(" AND ", wheres);
            }
            // Create a Statement Object
            query = "SELECT count(*) as counter FROM incidents " + where;

            stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);

            for (int i = 0; i < filters.size(); i++)
            {
              String filter_value = filters.get(i).get("value");
              stmt.setString(i+1, "%" + filter_value + "%");
            }
//                System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("counter");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_assets.all_filtered_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.all_filtered_count:=" + exc);
	}
        return count;
    }
    public static int all_filtered_by_query_count(Connection con, String query) throws IOException, SQLException
    {
        int count = 0;
        List<String> wheres = new ArrayList<>();
        PreparedStatement stmt;

        try 
        {
            query = "SELECT count(*) as counter FROM incidents " + ( query.equals("") ? "" : " WHERE " + query);

            stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);

//                System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("counter");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_assets.all_filtered__by_query_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.all_filtered_by_query_count:=" + exc);
	}
        return count;
    }
    public static ArrayList<String[]> all_limited_filtered(Connection con, int starting_record, int fetch_size, List<Map<String, String>> filters) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        List<String> wheres = new ArrayList<>();
        PreparedStatement stmt;
        String query;        
        String where = "";

        try 
        {
            // Processing filters
            for (int i = 0; i < filters.size(); i++)
            {
              String filter_key = filters.get(i).get("key");
              wheres.add(filter_key + " LIKE ? ");
            }
            
            if (wheres.size() > 0)
            {
                where = " WHERE " + String.join(" AND ", wheres);
            }

            // Create a Statement Object
            query = "SELECT incidents.*,callers.username caller_username,callers.first caller_first,callers.mi caller_mi,callers.last caller_last,callers.phone_mobile caller_phone_mobile" +
                    ",assigned.username assigned_to_username,assigned.first assigned_to_first,assigned.mi assigned_to_mi,assigned.last assigned_to_last,assigned.phone_mobile assigned_to_phone_mobile" +
                    ",created.username created_by_username,created.first created_by_first,created.mi created_by_mi,created.last created_by_last,created.phone_mobile created_by_phone_mobile" +
                    ",caller_groups.name caller_group_name" +
                    ",assigned_groups.name assigned_group_name" +
                    " FROM incidents " +
                    "LEFT JOIN users as callers ON incidents.caller_id = callers.id " + 
                    "LEFT JOIN users as assigned ON incidents.assigned_to_id = assigned.id " + 
                    "LEFT JOIN users as created ON incidents.create_by_id = created.id " + 
                    "LEFT JOIN `groups` caller_groups ON incidents.caller_group_id = caller_groups.id " + 
                    "LEFT JOIN `groups` assigned_groups ON incidents.assigned_group_id = assigned_groups.id " + 
                    where + " ORDER BY id DESC LIMIT ? , ?";
            stmt = con.prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);

            for (int i = 0; i < filters.size(); i++)
            {
              String filter_value = filters.get(i).get("value");
              stmt.setString(i+1, "%" + filter_value + "%");
            }
            stmt.setInt(filters.size()+1, starting_record);
            stmt.setInt(filters.size()+2, fetch_size);
            System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();
            return_array_list = process_resultset(rs);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.all_limited_filtered:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_assets.all_limited_filtered:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> all_limited_filtered_by_query(Connection con, int starting_record, int fetch_size, String query, String order_column, String order_dir) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        List<String> wheres = new ArrayList<>();
        PreparedStatement stmt;

        try 
        {
            // Create a Statement Object
            query = "SELECT incidents.*,callers.username caller_username,callers.first caller_first,callers.mi caller_mi,callers.last caller_last,callers.phone_mobile caller_phone_mobile,callers.vip caller_vip" +
                    ",assigned.username assigned_to_username,assigned.first assigned_to_first,assigned.mi assigned_to_mi,assigned.last assigned_to_last,assigned.phone_mobile assigned_to_phone_mobile,assigned.avatar assigned_to_avatar" +
                    ",created.username created_by_username,created.first created_by_first,created.mi created_by_mi,created.last created_by_last,created.phone_mobile created_by_phone_mobile" +
                    ",caller_groups.name caller_group_name" +
                    ",assigned_groups.name assigned_group_name" +
                    " FROM incidents " +
                    "LEFT JOIN users as callers ON incidents.caller_id = callers.id " + 
                    "LEFT JOIN users as assigned ON incidents.assigned_to_id = assigned.id " + 
                    "LEFT JOIN users as created ON incidents.create_by_id = created.id " + 
                    "LEFT JOIN `groups` caller_groups ON incidents.caller_group_id = caller_groups.id " + 
                    "LEFT JOIN `groups` assigned_groups ON incidents.assigned_group_id = assigned_groups.id " + 
                    ( query.equals("") ? "" : " WHERE " + query) + " ";

            if (!order_column.equals(""))
            {
                query += "ORDER BY " + order_column + " " + order_dir;
            } else {
                query += "ORDER BY id DESC";
            }

            query += " LIMIT ? , ?";

            stmt = con.prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);


            stmt.setInt(1, starting_record);
            stmt.setInt(2, fetch_size);
//            System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();
            return_array_list = process_resultset(rs);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.all_limited_filtered_by_query:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_assets.all_limited_filtered_by_query:=" + exc);
	}
        return return_array_list;
    }

    public static ArrayList<String[]> process_resultset(ResultSet rs, ArrayList<String[]> users, ArrayList<String[]> groups)
    {
        ArrayList<String[]> return_arraylist = new ArrayList();
        try
        {
            while(rs.next())
            {
                try
                {
                    String temp[] = new String[49];
                    temp[0] = rs.getString("id");
                    temp[1] = rs.getString("incident_type");
                    temp[2] = rs.getString("incident_time");
                    temp[3] = rs.getString("caller_id");
                    temp[4] = rs.getString("caller_group_id");                
                    temp[5] = rs.getString("location");
                    temp[6] = rs.getString("department");
                    temp[7] = rs.getString("site");
                    temp[8] = rs.getString("company");
                    temp[9] = rs.getString("category");
                    temp[10] = rs.getString("subcategory");
                    temp[11] = rs.getString("ci_id");
                    temp[12] = rs.getString("impact");
                    temp[13] = rs.getString("urgency");
                    temp[14] = rs.getString("priority");
                    temp[15] = rs.getString("description");
                    temp[16] = rs.getString("create_date");
                    temp[17] = rs.getString("create_by_id");
                    temp[18] = rs.getString("contact_method");
                    temp[19] = rs.getString("state");
                    temp[20] = rs.getString("state_date");
                    temp[21] = rs.getString("assigned_group_id");
                    temp[22] = rs.getString("assigned_to_id");
                    temp[23] = rs.getString("notes");
                    temp[24] = rs.getString("desk_notes");
                    temp[25] = rs.getString("related");
                    temp[26] = rs.getString("first_contact_resolution");
                    temp[27] = rs.getString("closed_date");
                    temp[28] = rs.getString("closed_reason");
                    temp[29] = rs.getString("pending_date");
                    temp[30] = rs.getString("pending_reason");
                    temp[31] = rs.getString("external_id");
                    //LOOKUP DATA populate the user info
                    temp[32] = "";
                    temp[33] = "";
                    temp[34] = "";
                    temp[35] = "";
                    temp[36] = "";
                    for(int a = 0; a < users.size(); a++)
                    {
                        String this_user[] = users.get(a);
                        if(this_user[0].equalsIgnoreCase(rs.getString("caller_id")))
                        {
                            temp[32] = this_user[1];
                            temp[33] = this_user[3];
                            temp[34] = this_user[4];
                            temp[35] = this_user[5];
                            temp[36] = this_user[17];
                            a = users.size();
                        }
                    }
                    temp[37] = "";
                    for(int b = 0; b < groups.size(); b++)
                    {
                        String this_group[] = groups.get(b);
                        if(this_group[0].equalsIgnoreCase(rs.getString("caller_group_id")))
                        {
                            temp[37] = this_group[1];
                            b = groups.size();
                        }
                    }
                    temp[38] = "";
                    for(int b = 0; b < groups.size(); b++)
                    {
                        String this_group[] = groups.get(b);
                        if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                        {
                            temp[38] = this_group[1];
                            b = groups.size();
                        }
                    }                
                    temp[39] = "";
                    temp[40] = "";
                    temp[41] = "";
                    temp[42] = "";
                    temp[43] = "";
                    for(int c = 0; c < users.size(); c++)
                    {
                        String this_user[] = users.get(c);
                        if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                        {
                            temp[39] = this_user[1];
                            temp[40] = this_user[3];
                            temp[41] = this_user[4];
                            temp[42] = this_user[5];
                            temp[43] = this_user[17];
                            c = users.size();
                        }
                    }
                    temp[44] = "";
                    temp[45] = "";
                    temp[46] = "";
                    temp[47] = "";
                    temp[48] = "";
                    for(int d = 0; d < users.size(); d++)
                    {
                        String this_user[] = users.get(d);
                        if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                        {
                            temp[44] = this_user[1];
                            temp[45] = this_user[3];
                            temp[46] = this_user[4];
                            temp[47] = this_user[5];
                            temp[48] = this_user[17];
                            d = users.size();
                        }
                    } 
                    return_arraylist.add(temp);
                }
                catch (Exception e)
                {
                    System.out.println("Exception in get_incidents.process_resultset=" + e);
                }
            }  
        }
        catch(Exception e)
        {
            System.out.println("Exception in get_incidents.process_resultset=" + e);
        }
        return return_arraylist;
    }
    public static ArrayList<String[]> process_resultset(ResultSet rs)
    {
        ArrayList<String[]> return_arraylist = new ArrayList();
        try
        {
            while(rs.next())
            {
                try
                {
                    String temp[] = new String[51];
                    temp[0] = rs.getString("id");
                    temp[1] = rs.getString("incident_type");
                    temp[2] = rs.getString("incident_time");
                    temp[3] = rs.getString("caller_id");
                    temp[4] = rs.getString("caller_group_id");                
                    temp[5] = rs.getString("location");
                    temp[6] = rs.getString("department");
                    temp[7] = rs.getString("site");
                    temp[8] = rs.getString("company");
                    temp[9] = rs.getString("category");
                    temp[10] = rs.getString("subcategory");
                    temp[11] = rs.getString("ci_id");
                    temp[12] = rs.getString("impact");
                    temp[13] = rs.getString("urgency");
                    temp[14] = rs.getString("priority");
                    temp[15] = rs.getString("description");
                    temp[16] = rs.getString("create_date");
                    temp[17] = rs.getString("create_by_id");
                    temp[18] = rs.getString("contact_method");
                    temp[19] = rs.getString("state");
                    temp[20] = rs.getString("state_date");
                    temp[21] = rs.getString("assigned_group_id");
                    temp[22] = rs.getString("assigned_to_id");
                    temp[23] = rs.getString("notes");
                    temp[24] = rs.getString("desk_notes");
                    temp[25] = rs.getString("related");
                    temp[26] = rs.getString("first_contact_resolution");
                    temp[27] = rs.getString("closed_date");
                    temp[28] = rs.getString("closed_reason");
                    temp[29] = rs.getString("pending_date");
                    temp[30] = rs.getString("pending_reason");
                    temp[31] = rs.getString("external_id");
                    //LOOKUP DATA populate the user info
                    temp[32] = rs.getString("caller_username");
                    temp[33] = rs.getString("caller_first");
                    temp[34] = rs.getString("caller_mi");
                    temp[35] = rs.getString("caller_last");
                    temp[36] = rs.getString("caller_phone_mobile");
                    temp[37] = rs.getString("caller_group_name");
                    temp[38] = rs.getString("assigned_group_name");
                    temp[39] = rs.getString("assigned_to_username");
                    temp[40] = rs.getString("assigned_to_first");
                    temp[41] = rs.getString("assigned_to_mi");
                    temp[42] = rs.getString("assigned_to_last");
                    temp[43] = rs.getString("assigned_to_phone_mobile");
                    temp[44] = rs.getString("created_by_username");
                    temp[45] = rs.getString("created_by_first");
                    temp[46] = rs.getString("created_by_mi");
                    temp[47] = rs.getString("created_by_last");
                    temp[48] = rs.getString("created_by_phone_mobile");
                    temp[49] = rs.getString("caller_vip");
                    temp[50] = rs.getString("assigned_to_avatar");
                    return_arraylist.add(temp);
                }
                catch (Exception e)
                {
                    System.out.println("Exception in get_incidents.process_resultset=" + e);
                }
            }  
        }
        catch(Exception e)
        {
            System.out.println("Exception in get_incidents.process_resultset=" + e);
        }
        return return_arraylist;
    }
    public static int incident_open_count(Connection con) throws IOException, SQLException
    {
        int return_int = 0;  
        String ps_query_string = "SELECT COUNT(*) AS value FROM incidents WHERE (incidents.state <> 'Closed' AND incidents.state <> 'Resolved')";
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
                //System.out.println("q=" + stmt.toString());
                //System.out.println("value=" + return_int);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incident_open_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incident_open_count:=" + exc);
	}
        return return_int;
    }    
    public static int incident_count_start_date_stop_date_group_id(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        int return_int = 0;  
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT COUNT(*) AS value FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?)";
        }
        else
        {
            ps_query_string = "SELECT COUNT(*) AS value	FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND incidents.assigned_group_id='" + group_id + "'";
        }
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
                //System.out.println("q=" + stmt.toString());
                //System.out.println("value=" + return_int);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incident_count_start_date_stop_date_group_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incident_count_start_date_stop_date_group_id:=" + exc);
	}
        return return_int;
    }
    public static int incident_closed_count_start_date_stop_date_group_id(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        int return_int = 0;  
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT COUNT(*) AS value FROM incidents WHERE (incidents.closed_date >= ? AND incidents.closed_date <= ?)";
        }
        else
        {
            ps_query_string = "SELECT COUNT(*) AS value	FROM incidents WHERE (incidents.closed_date >= ? AND incidents.closed_date <= ?) AND incidents.assigned_group_id='" + group_id + "'";
        }
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
                //System.out.println("q=" + stmt.toString());
                //System.out.println("value=" + return_int);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incident_count_start_date_stop_date_group_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incident_count_start_date_stop_date_group_id:=" + exc);
	}
        return return_int;
    }
    public static int incident_open_count_start_date_stop_date_group_id(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        int return_int = 0;  
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT COUNT(*) AS value FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND state <> 'Closed'";
        }
        else
        {
            ps_query_string = "SELECT COUNT(*) AS value	FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND state <> 'Closed' AND incidents.assigned_group_id='" + group_id + "'";
        }
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
                //System.out.println("q=" + stmt.toString());
                //System.out.println("value=" + return_int);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incident_open_count_start_date_stop_date_group_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incident_open_count_start_date_stop_date_group_id:=" + exc);
	}
        return return_int;
    }
    public static int incident_count_start_date_stop_date_group_id_customer(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        int return_int = 0;  
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT COUNT(*) AS value FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?)";
        }
        else
        {
            ps_query_string = "SELECT COUNT(*) AS value	FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND incidents.caller_group_id='" + group_id + "'";
        }
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
                //System.out.println("q=" + stmt.toString());
                //System.out.println("value=" + return_int);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incident_count_start_date_stop_date_group_id_customer:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incident_count_start_date_stop_date_group_id_customer:=" + exc);
	}
        return return_int;
    }
    public static ArrayList<String[]> incidents_start_date_stop_date_group_id(Connection con, java.util.Date start_date, java.util.Date end_date, String group_type, String group_id) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList();  
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            //ps_query_string = "SELECT * FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?)";
            ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?)";  
        }
        else
        {
            if(group_type.equalsIgnoreCase("support"))
            {
                //ps_query_string = "SELECT *	FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND incidents.assigned_group_id='" + group_id + "'";
                ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND incidents.assigned_group_id='" + group_id + "'";
            }
            else
            {
                //ps_query_string = "SELECT *	FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND incidents.caller_group_id='" + group_id + "'";
                ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND incidents.caller_group_id='" + group_id + "'";
            }
        }
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            ResultSet rs = stmt.executeQuery();            
            return_arraylist = process_resultset(rs,users,groups);   
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incidents_start_date_stop_date_group_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incidents_start_date_stop_date_group_id:=" + exc);
	}
        return return_arraylist;
    }
    public static String[] incident_by_id(Connection con, String id) throws IOException, SQLException
    {
        String return_string[] = new String[49];
        //init the array
        for(int a = 0; a < return_string.length;a++)
        {
            return_string[a] = "--";
        }
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        String ps_query_string = "";
        //ps_query_string = "SELECT * FROM incidents WHERE id=?";        
        ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date, "
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date, "
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE id=?";
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_string[0] = rs.getString("id");
                return_string[1] = rs.getString("incident_type");
                return_string[2] = rs.getString("incident_time"); //timestamp_format.format(rs.getTimestamp("tz_incident_time"));
                return_string[3] = rs.getString("caller_id");
                return_string[4] = rs.getString("caller_group_id");                
                return_string[5] = rs.getString("location");
                return_string[6] = rs.getString("department");
                return_string[7] = rs.getString("site");
                return_string[8] = rs.getString("company");                 
                return_string[9] = rs.getString("category");
                return_string[10] = rs.getString("subcategory");
                return_string[11] = rs.getString("ci_id");
                return_string[12] = rs.getString("impact");
                return_string[13] = rs.getString("urgency");
                return_string[14] = rs.getString("priority");
                return_string[15] = rs.getString("description");
                return_string[16] = rs.getString("create_date");
                return_string[17] = rs.getString("create_by_id");
                return_string[18] = rs.getString("contact_method");
                return_string[19] = rs.getString("state");
                return_string[20] = rs.getString("state_date");//timestamp_format.format(rs.getTimestamp("state_date"));
                return_string[21] = rs.getString("assigned_group_id");
                return_string[22] = rs.getString("assigned_to_id");
                return_string[23] = rs.getString("notes");
                return_string[24] = rs.getString("desk_notes");
                return_string[25] = rs.getString("related");
                return_string[26] = rs.getString("first_contact_resolution");
                return_string[27] = rs.getString("closed_date");//timestamp_format.format(rs.getTimestamp("closed_date"));
                return_string[28] = rs.getString("closed_reason");
                return_string[29] = rs.getString("pending_date");
                return_string[30] = rs.getString("pending_reason");
                return_string[31] = rs.getString("external_id");
                //LOOKUP DATA populate the user info
                return_string[32] = "";
                return_string[33] = "";
                return_string[34] = "";
                return_string[35] = "";
                return_string[36] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("caller_id")))
                    {
                        return_string[32] = this_user[1];
                        return_string[33] = this_user[3];
                        return_string[34] = this_user[4];
                        return_string[35] = this_user[5];
                        return_string[36] = this_user[17];
                        a = users.size();
                    }
                }
                return_string[37] = "";
                for(int b = 0; b < groups.size(); b++)
                {
                    String this_group[] = groups.get(b);
                    if(this_group[0].equalsIgnoreCase(rs.getString("caller_group_id")))
                    {
                        return_string[37] = this_group[1];
                        b = groups.size();
                    }
                }
                return_string[38] = "";
                for(int b = 0; b < groups.size(); b++)
                {
                    String this_group[] = groups.get(b);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        return_string[38] = this_group[1];
                        b = groups.size();
                    }
                }  
                return_string[39] = "";
                return_string[40] = "";
                return_string[41] = "";
                return_string[42] = "";
                return_string[43] = "";
                for(int c = 0; c < users.size(); c++)
                {
                    String this_user[] = users.get(c);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        return_string[39] = this_user[1];
                        return_string[40] = this_user[3];
                        return_string[41] = this_user[4];
                        return_string[42] = this_user[5];
                        return_string[43] = this_user[17];
                        c = users.size();
                    }
                }
                return_string[44] = "";
                return_string[45] = "";
                return_string[46] = "";
                return_string[47] = "";
                return_string[48] = "";
                for(int d = 0; d < users.size(); d++)
                {
                    String this_user[] = users.get(d);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        return_string[44] = this_user[1];
                        return_string[45] = this_user[3];
                        return_string[46] = this_user[4];
                        return_string[47] = this_user[5];
                        return_string[48] = this_user[17];
                        d = users.size();
                    }
                }
            }
            //fix all nulls
            for(int a = 0; a < return_string.length; a++)
            {
                return_string[a] = support.string_utils.check_for_null(return_string[a]);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incident_by_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incident_by_id:=" + exc);
	}
        return return_string;
    }
    public static HashMap<String,String> incident_hash_by_id(Connection con, String id) throws IOException, SQLException
    {
        //used by format_email only
        HashMap<String,String> return_hashmap = new HashMap();        
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        String ps_query_string = "";
        //ps_query_string = "SELECT * FROM incidents WHERE id=?";
        //Date is formated to the datetimepicker format 
        //SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");
        ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%H:%i %m/%d/%Y') AS incident_time, "  
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%H:%i %m/%d/%Y') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%H:%i %m/%d/%Y') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%H:%i %m/%d/%Y') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%H:%i %m/%d/%Y') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE id=?"; 
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_hashmap.put("id", rs.getString("id"));
                return_hashmap.put("incident_type", rs.getString("incident_type"));
                return_hashmap.put("incident_time", rs.getString("incident_time"));
                return_hashmap.put("caller_id", rs.getString("caller_id"));
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("caller_id")))
                    {
                        return_hashmap.put("caller_id_username", this_user[1]);
                        return_hashmap.put("caller_id_first", this_user[3]);
                        return_hashmap.put("caller_id_mi", this_user[4]);
                        return_hashmap.put("caller_id_last", this_user[5]);
                        return_hashmap.put("caller_id_vip", this_user[17]);
                        a = users.size();
                    }
                }                
                return_hashmap.put("caller_group_id", rs.getString("caller_group_id"));
                return_hashmap.put("caller_group_id_name", "");
                for(int b = 0; b < all_group_info.size(); b++)
                {
                    String this_group[] = all_group_info.get(b);
                    if(this_group[0].equalsIgnoreCase(rs.getString("caller_group_id")))
                    {
                        return_hashmap.put("caller_group_id_name", this_group[1]);
                        b = all_group_info.size();
                    }
                }                
                return_hashmap.put("location", rs.getString("location"));
                return_hashmap.put("department", rs.getString("department"));
                return_hashmap.put("site", rs.getString("site"));
                return_hashmap.put("company", rs.getString("company"));
                return_hashmap.put("category", rs.getString("category"));
                return_hashmap.put("subcategory", rs.getString("subcategory"));
                return_hashmap.put("ci_id", rs.getString("ci_id"));
                return_hashmap.put("impact", rs.getString("impact"));
                return_hashmap.put("urgency", rs.getString("urgency"));
                return_hashmap.put("priority", rs.getString("priority"));
                return_hashmap.put("description", rs.getString("description"));
                return_hashmap.put("create_date", rs.getString("create_date"));
                return_hashmap.put("create_by_id", rs.getString("create_by_id"));
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        return_hashmap.put("create_by_id_username", this_user[1]);
                        return_hashmap.put("create_by_id_first", this_user[3]);
                        return_hashmap.put("create_by_id_mi", this_user[4]);
                        return_hashmap.put("create_by_id_last", this_user[5]);
                        return_hashmap.put("create_by_id_vip", this_user[17]);
                        a = users.size();
                    }
                }
                return_hashmap.put("contact_method", rs.getString("contact_method"));
                return_hashmap.put("state", rs.getString("state"));
                return_hashmap.put("state_date", rs.getString("state_date"));
                return_hashmap.put("assigned_group_id", rs.getString("assigned_group_id"));
                return_hashmap.put("assigned_group_id_name", "");
                for(int b = 0; b < all_group_info.size(); b++)
                {
                    String this_group[] = all_group_info.get(b);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        return_hashmap.put("assigned_group_id_name", this_group[1]);
                        b = all_group_info.size();
                    }
                }                
                return_hashmap.put("assigned_to_id", rs.getString("assigned_to_id"));
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        return_hashmap.put("assigned_to_id_username", this_user[1]);
                        return_hashmap.put("assigned_to_id_first", this_user[3]);
                        return_hashmap.put("assigned_to_id_mi", this_user[4]);
                        return_hashmap.put("assigned_to_id_last", this_user[5]);
                        return_hashmap.put("assigned_to_id_vip", this_user[17]);
                        a = users.size();
                    }
                }                
                return_hashmap.put("notes", rs.getString("notes"));
                return_hashmap.put("desk_notes", rs.getString("desk_notes"));
                return_hashmap.put("related", rs.getString("related"));
                return_hashmap.put("first_contact_resolution", rs.getString("first_contact_resolution"));
                return_hashmap.put("closed_date", rs.getString("closed_date"));
                return_hashmap.put("closed_reason", rs.getString("closed_reason"));
                return_hashmap.put("pending_date", rs.getString("pending_date"));
                return_hashmap.put("pending_reason", rs.getString("pending_reason"));
                return_hashmap.put("external_id", rs.getString("external_id"));
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incident_hash_by_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incident_hash_by_id:=" + exc);
	}
        return return_hashmap;
    }
    public static ArrayList<String[]> incident_state_log(Connection con, String incident_id) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList();  
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        String ps_query_string = "";
        ps_query_string = "SELECT * FROM incident_history WHERE incident_id=?";
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, incident_id);
            ResultSet rs = stmt.executeQuery();            
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incident_state_log:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incident_state_log:=" + exc);
	}
        return return_arraylist;
    }   
    public static ArrayList<String[]> incident_chart_drilldown_queue(Connection con, String priority,java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList();  
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            //ps_query_string = "SELECT * FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND priority = ? AND state <> 'Closed' AND state <> 'Resolved'";
            ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND priority = ? AND state <> 'Closed' AND state <> 'Resolved'";    
            
            
        }
        else
        {
            //ps_query_string = "SELECT *	FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?)  AND priority = ?  AND state <> 'Closed' AND state <> 'Resolved' AND incidents.assigned_group_id='" + group_id + "'";
            ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?)  AND priority = ?  AND state <> 'Closed' AND state <> 'Resolved' AND incidents.assigned_group_id='" + group_id + "'"; 
        }
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            stmt.setString(3, priority);
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incidents_priority_start_date_stop_date_group_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incidents_priority_start_date_stop_date_group_id:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> incident_chart_drilldown_queue_customer(Connection con, String priority,java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList();   
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            //ps_query_string = "SELECT * FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND priority = ? AND state <> 'Closed' AND state <> 'Resolved'";
            ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND priority = ? AND state <> 'Closed' AND state <> 'Resolved'"; 
        }
        else
        {
            //ps_query_string = "SELECT *	FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?)  AND priority = ?  AND state <> 'Closed' AND state <> 'Resolved' AND incidents.caller_group_id='" + group_id + "'";
            ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?)  AND priority = ?  AND state <> 'Closed' AND state <> 'Resolved' AND incidents.caller_group_id='" + group_id + "'"; 
        }
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            stmt.setString(3, priority);
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incident_chart_drilldown_queue_customer:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incident_chart_drilldown_queue_customer:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> incident_chart_drilldown_number(Connection con, String start_date, String end_date, String group_id) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList();  
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            //ps_query_string = "SELECT * FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?)";
            ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?)"; 
        }
        else
        {
            //ps_query_string = "SELECT *	FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND assigned_group_id='" + group_id + "'";
            ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND assigned_group_id='" + group_id + "'"; 
        }
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, start_date);
            stmt.setString(2, end_date);
            System.out.println("incident_chart_drilldown_number q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incident_chart_drilldown_number:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incident_chart_drilldown_number:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> incident_chart_drilldown_priority(Connection con, String priority, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList();  
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            //ps_query_string = "SELECT * FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ? AND priority=?)";
            ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ? AND priority=?)"; 
        }
        else
        {
            //ps_query_string = "SELECT *	FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND AND priority=? AND assigned_group_id='" + group_id + "'";
            ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND AND priority=? AND assigned_group_id='" + group_id + "'"; 
        }
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            stmt.setString(3, priority);
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incident_chart_drilldown_number:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incident_chart_drilldown_number:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> incident_chart_drilldown_number_customer(Connection con, String priority, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            //ps_query_string = "SELECT * FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?)";
            ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?)";
        }
        else
        {
            //ps_query_string = "SELECT *	FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND caller_group_id='" + group_id + "'";
            ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND caller_group_id='" + group_id + "'";
        }
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incident_chart_drilldown_number_customer:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incident_chart_drilldown_number_customer:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> incident_chart_drilldown_category(Connection con, String category,java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            //ps_query_string = "SELECT * FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND category = ?";
            ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND category = ?";
        }
        else
        {
            //ps_query_string = "SELECT *	FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?)  AND category = ? AND assigned_group_id='" + group_id + "'";
            ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?)  AND category = ? AND assigned_group_id='" + group_id + "'";
        }
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            stmt.setString(3, category);
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incident_chart_drilldown_category:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incident_chart_drilldown_category:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> incident_chart_drilldown_category_customer(Connection con, String category,java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList();  
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            //ps_query_string = "SELECT * FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND category = ?";
            ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) AND category = ?";
        }
        else
        {
            //ps_query_string = "SELECT *	FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?)  AND category = ? AND caller_group_id='" + group_id + "'";
            ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?)  AND category = ? AND caller_group_id='" + group_id + "'";
        }
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            stmt.setString(3, category);
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incident_chart_drilldown_category_customer:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incident_chart_drilldown_category_customer:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> incident_chart_drilldown_closure(Connection con, String priority, java.util.Date start_date, java.util.Date end_date,  String group_id) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        //date=12/21/2018&group_id=all
        SimpleDateFormat chart_date_format = new SimpleDateFormat("MM/dd/yyyy");
        
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            //ps_query_string = "SELECT * FROM incidents WHERE state='closed' AND (closed_date >= ? AND closed_date <= ?) AND priority = ? ";
            //ps_query_string = "SELECT * FROM incidents WHERE ((incident_time >= ? AND incident_time <= ?) OR (closed_date >= ? AND closed_date <= ? AND state='Closed'))  AND priority=? ORDER BY incident_time ASC";
            ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE ((incident_time >= ? AND incident_time <= ?) OR (closed_date >= ? AND closed_date <= ? AND state='Closed'))  AND priority=? ORDER BY incident_time ASC";
        }
        else
        {
            //ps_query_string = "SELECT * FROM incidents WHERE ((incident_time >= ? AND incident_time <= ?) OR (closed_date >= ? AND closed_date <= ? AND state='Closed')) AND assigned_group_id='" + group_id + "' AND priority=? ORDER BY incident_time ASC";
            ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE ((incident_time >= ? AND incident_time <= ?) OR (closed_date >= ? AND closed_date <= ? AND state='Closed')) AND assigned_group_id='" + group_id + "' AND priority=? ORDER BY incident_time ASC";
        }
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            stmt.setTimestamp(3, start);
            stmt.setTimestamp(4, end);
            stmt.setString(5,priority);
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incident_chart_drilldown_closure:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incident_chart_drilldown_closure_customer:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> incident_chart_drilldown_closure_customer(Connection con, String priority, java.util.Date start_date, java.util.Date end_date,  String group_id) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        //date=12/21/2018&group_id=all
        SimpleDateFormat chart_date_format = new SimpleDateFormat("MM/dd/yyyy");
        
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            //ps_query_string = "SELECT * FROM incidents WHERE state='closed' AND (closed_date >= ? AND closed_date <= ?) AND priority = ? ";
            //ps_query_string = "SELECT * FROM incidents WHERE ((incident_time >= ? AND incident_time <= ?) OR (closed_date >= ? AND closed_date <= ? AND state='Closed'))  AND priority=? ORDER BY incident_time ASC";
            ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE ((incident_time >= ? AND incident_time <= ?) OR (closed_date >= ? AND closed_date <= ? AND state='Closed'))  AND priority=? ORDER BY incident_time ASC";
        
        }
        else
        {
            //ps_query_string = "SELECT * FROM incidents WHERE ((incident_time >= ? AND incident_time <= ?) OR (closed_date >= ? AND closed_date <= ? AND state='Closed')) AND caller_group_id='" + group_id + "' AND priority=? ORDER BY incident_time ASC";
            ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE ((incident_time >= ? AND incident_time <= ?) OR (closed_date >= ? AND closed_date <= ? AND state='Closed')) AND caller_group_id='" + group_id + "' AND priority=? ORDER BY incident_time ASC";
        
        }
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            stmt.setTimestamp(3, start);
            stmt.setTimestamp(4, end);
            stmt.setString(5,priority);
            //System.out.println("get_incidents q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incident_chart_drilldown_closure_customer:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incident_chart_drilldown_closure_customer:=" + exc);
	}
        return return_arraylist;
    }
    public static int incident_walkin_count_start_date_stop_date(Connection con, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {
        int return_int = 0;  
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        ps_query_string = "SELECT COUNT(*) AS value FROM incidents WHERE (incident_time >= ? AND incident_time <= ?) AND contact_method='Walk-in'";
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incident_walkin_count_start_date_stop_date:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incident_walkin_count_start_date_stop_date:=" + exc);
	}
        return return_int;
    }
    public static int incident_fcr_count_start_date_stop_date(Connection con, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {
        int return_int = 0;  
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        ps_query_string = "SELECT count(*) as value FROM incidents WHERE incident_time >= ? AND incident_time <= ? AND first_contact_resolution='True'";
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incident_fcr_count_start_date_stop_date:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incident_fcr_count_start_date_stop_date:=" + exc);
	}
        return return_int;
    }
    public static String[] unique_category_subcategory(Connection con)
    {
        String return_string[] = new String[0];
        PreparedStatement stmt;
        int count = 0;
        try 
        {
            // Create a Statement Object
            String ps_query_string = "SELECT category, subcategory FROM incidents GROUP BY category, subcategory";
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count] = rs.getString("category") + " / " + rs.getString("subcategory");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.unique_category_subcategory:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.unique_category_subcategory:=" + exc);
	}
        return return_string;
    }
    public static String[] distinct_company(Connection con)
    {
        String return_string[] = new String[0];
        PreparedStatement stmt;
        int count = 0;
        try 
        {
            // Create a Statement Object
            String ps_query_string = "SELECT distinct(company) FROM incidents";
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count] = rs.getString("company");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.distinct_company:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.distinct_company:=" + exc);
	}
        return return_string;
    }
    public static String[] distinct_department(Connection con)
    {
        String return_string[] = new String[0];
        PreparedStatement stmt;
        int count = 0;
        try 
        {
            // Create a Statement Object
            String ps_query_string = "SELECT distinct(department) FROM incidents";
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();            
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count] = rs.getString("department");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.distinct_department:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.distinct_department:=" + exc);
	}
        return return_string;
    }
    public static String[] distinct_site(Connection con)
    {
        String return_string[] = new String[0];
        PreparedStatement stmt;
        int count = 0;
        try 
        {
            // Create a Statement Object
            String ps_query_string = "SELECT distinct(site) FROM incidents";
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();            
            
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count] = rs.getString("site");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.distinct_site:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.distinct_site:=" + exc);
	}
        return return_string;
    }
    public static String[] distinct_location(Connection con)
    {
        String return_string[] = new String[0];
        PreparedStatement stmt;
        int count = 0;
        try 
        {
            // Create a Statement Object
            String ps_query_string = "SELECT distinct(location) FROM incidents";
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();            
            
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count] = rs.getString("location");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.distinct_location:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.distinct_location:=" + exc);
	}
        return return_string;
    }    
    //NOT USED?
    public static String[][] incident_fcr_list_start_date_stop_date(Connection con, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];  
        String fcr_cat_sub[][] = new String[0][0]; //FCR, NotFCR, Category, SubCategory
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        //ps_query_string = "SELECT * FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?)";
        ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?)";        
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;                    
            }
            return_string = new String[count][32];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("id");
                return_string[count][1] = rs.getString("incident_type");
                return_string[count][2] = rs.getString("incident_time");
                return_string[count][3] = rs.getString("caller_id");
                return_string[count][4] = rs.getString("caller_group_id");                
                return_string[count][5] = rs.getString("location");
                return_string[count][6] = rs.getString("department");
                return_string[count][7] = rs.getString("site");
                return_string[count][8] = rs.getString("company");                 
                return_string[count][9] = rs.getString("category");
                return_string[count][10] = rs.getString("subcategory");
                return_string[count][11] = rs.getString("ci_id");
                return_string[count][12] = rs.getString("impact");
                return_string[count][13] = rs.getString("urgency");
                return_string[count][14] = rs.getString("priority");
                return_string[count][15] = rs.getString("description");
                return_string[count][16] = rs.getString("create_date");                
                return_string[count][17] = rs.getString("create_by_id");
                return_string[count][18] = rs.getString("contact_method");
                return_string[count][19] = rs.getString("state");
                return_string[count][20] = rs.getString("state_date");
                return_string[count][21] = rs.getString("assigned_group_id");
                return_string[count][22] = rs.getString("assigned_to_id");
                return_string[count][23] = rs.getString("notes");
                return_string[count][24] = rs.getString("desk_notes");
                return_string[count][25] = rs.getString("related");
                return_string[count][26] = rs.getString("first_contact_resolution");
                return_string[count][27] = rs.getString("closed_date");
                return_string[count][28] = rs.getString("closed_reason");
                return_string[count][29] = rs.getString("pending_date");
                return_string[count][30] = rs.getString("pending_reason");
                return_string[count][31] = rs.getString("external_id");                 
                count++;  
            }
            //get the distinct cat_subs
            
            ps_query_string = "SELECT DISTINCT category, subcategory FROM incidents WHERE incident_time > ? AND incident_time < ?";
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            rs = stmt.executeQuery();
            count = 0;
            while(rs.next())
            {
                count++;                    
            }
            //load an array of distinct cat_subcats  //FCR, NotFCR, Category, SubCategory
            fcr_cat_sub = new String[count][4];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                fcr_cat_sub[count][0] = "0"; //FCR
                fcr_cat_sub[count][1] = "0"; //Not FCR
                fcr_cat_sub[count][2] = rs.getString("category"); //Cat
                fcr_cat_sub[count][3] = rs.getString("subcategory"); //SubCat
                count++;
            }
            
            //loop thru the incident list  increment FCR or NotFCR
            String this_incident_cat, this_incident_sub, this_incident_fcr;
            for(int a = 0; a < return_string.length; a++)
            {
                this_incident_cat = return_string[a][9];
                this_incident_sub = return_string[a][10];
                this_incident_fcr = return_string[a][26];
                //find this cat and sub
                for(int b = 0; b < fcr_cat_sub.length; b++)
                {
                    if(fcr_cat_sub[b][2].equals(this_incident_cat) && fcr_cat_sub[b][3].equals(this_incident_sub))
                    {
                        //found it
                        if(this_incident_fcr.equalsIgnoreCase("true"))
                        {
                            //increment FCR
                            int fcr = Integer.parseInt(fcr_cat_sub[b][0]);
                            fcr++;
                            fcr_cat_sub[b][0] = String.valueOf(fcr);
                        }
                        else
                        {
                            //increment Not FCR
                            int not_fcr = Integer.parseInt(fcr_cat_sub[b][1]);
                            not_fcr++;
                            fcr_cat_sub[b][1] = String.valueOf(not_fcr);
                        }
                    }
                }
            }
            
            
            //sort array by FCR
            Arrays.sort(fcr_cat_sub, new Comparator<String[]>() 
            {
                @Override
                public int compare(final String[] entry1, final String[] entry2) 
                {
                    final String fcr1 = entry1[0];
                    final String fcr2 = entry2[0];
                    return fcr1.compareTo(fcr2);
                }
            });
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incident_fcr_list_start_date_stop_date:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incident_fcr_list_start_date_stop_date:=" + exc);
	}
        return fcr_cat_sub;
    }
    public static int next_incident_id(Connection con) throws IOException, SQLException
    {
        int return_int = 0;  
        String ps_query_string = "SELECT max(id) AS value FROM incidents";        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
            }
            stmt.close();
            //add 1 to max
            return_int++;
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.next_incident_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.next_incident_id:=" + exc);
	}
        return return_int;
    }
    public static ArrayList<String[]> incidents_by_date_range(Connection con, String user_tz_name, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        //String ps_query_string = "";
        //ps_query_string = "SELECT * FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) ORDER BY incident_time DESC";
        String ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (incidents.incident_time >= ? AND incidents.incident_time <= ?) ORDER BY incident_time DESC";        
        
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            System.out.println("?=" + stmt );
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incidents_by_date_range:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incidents_by_date_range:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> incidents_by_date_range_and_assigned_to_id(Connection con, String assigned_to_id) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        ArrayList<String[]> return_arraylist = new ArrayList();  
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        String ps_query_string = "";
        //ps_query_string = "SELECT * FROM incidents WHERE state <> 'Closed' AND assigned_to_id = ? ORDER BY incident_time DESC";
        ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE state <> 'Closed' AND assigned_to_id = ? ORDER BY incident_time DESC";
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, assigned_to_id);
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incidents_by_date_range:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incidents_by_date_range:=" + exc);
	}
        return return_arraylist;
    }
    public static int open_incident_count_assigned_to_id(Connection con, String assigned_to_id) throws IOException, SQLException
    {
        int count = 0;;  
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        String ps_query_string = "";
        ps_query_string = "SELECT count(*) AS incident_count FROM incidents WHERE assigned_to_id = ? AND state <> 'Closed'";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, assigned_to_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("incident_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.open_incident_count_assigned_to_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.open_incident_count_assigned_to_id:=" + exc);
	}
        return count;
    }
    public static int unassigned_incident_count(Connection con) throws IOException, SQLException
    {
        int count = 0;;  
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        String ps_query_string = "";
        ps_query_string = "SELECT count(*) AS incident_count FROM incidents WHERE assigned_to_id = ? AND incidents.state <> 'Closed' ";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, "0");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("incident_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.unassigned_incident_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.unassigned_incident_count:=" + exc);
	}
        return count;
    }
    public static ArrayList<String[]> incidents_unassigned(Connection con) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        ArrayList<String[]> return_arraylist = new ArrayList();  
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        String ps_query_string = "";
        //ps_query_string = "SELECT * FROM incidents WHERE assigned_to_id = ? ORDER BY incident_time DESC";
        ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE state <> 'Closed' AND assigned_to_id = ? ORDER BY incident_time DESC";
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, "0");
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incidents_by_date_range:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incidents_by_date_range:=" + exc);
	}
        return return_arraylist;
    }
    public static int created_today_incident_count(Connection con) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
        int count = 0;;  
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        GregorianCalendar cal = new GregorianCalendar();
        cal.set(cal.HOUR_OF_DAY,00);
        cal.set(cal.MINUTE,00);
        cal.set(cal.SECOND,00);
        String start_date = timestamp_format.format(cal.getTime());
        cal.set(cal.HOUR_OF_DAY,23);
        cal.set(cal.MINUTE,59);
        cal.set(cal.SECOND,59);
        String end_date = timestamp_format.format(cal.getTime());
        String ps_query_string = "";
        ps_query_string = "SELECT count(*) AS incident_count FROM incidents WHERE create_date >= ? AND create_date <= ?";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, start_date);
            stmt.setString(2, end_date);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("incident_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.created_today_incident_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.created_today_incident_count:=" + exc);
	}
        return count;
    }
    public static ArrayList<String[]> incidents_created_today(Connection con) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        String ps_query_string = "";
        GregorianCalendar cal = new GregorianCalendar();
        cal.set(cal.HOUR_OF_DAY,00);
        cal.set(cal.MINUTE,00);
        cal.set(cal.SECOND,00);
        String start_date = timestamp_format.format(cal.getTime());
        cal.set(cal.HOUR_OF_DAY,23);
        cal.set(cal.MINUTE,59);
        cal.set(cal.SECOND,59);
        String end_date = timestamp_format.format(cal.getTime());
        ps_query_string = "SELECT id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id,"
                + "caller_group_id,"
                + "location,"
                + "department,"
                + "site,"
                + "company,"
                + "category,"
                + "subcategory,"
                + "ci_id,"
                + "impact," 
                + "urgency,"
                + "priority,"
                + "description,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "create_by_id,"
                + "contact_method,"
                + "state," 
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date, "
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date, "
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE create_date >= ? AND create_date <= ? ORDER BY incident_time DESC";
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, start_date);
            stmt.setString(2, end_date);
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incidents_created_today:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incidents_created_today:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> incidents_closed_today(Connection con) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        String ps_query_string = "";
        GregorianCalendar cal = new GregorianCalendar();
        cal.set(cal.HOUR_OF_DAY,00);
        cal.set(cal.MINUTE,00);
        cal.set(cal.SECOND,00);
        String start_date = timestamp_format.format(cal.getTime());
        cal.set(cal.HOUR_OF_DAY,23);
        cal.set(cal.MINUTE,59);
        cal.set(cal.SECOND,59);
        String end_date = timestamp_format.format(cal.getTime());
        //ps_query_string = "SELECT * FROM incidents WHERE closed_date >= ? AND closed_date <= ? ORDER BY incident_time DESC";
        ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE closed_date >= ? AND closed_date <= ? ORDER BY incident_time DESC";
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, start_date);
            stmt.setString(2, end_date);
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incidents_closed_today:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incidents_closed_today:=" + exc);
	}
        return return_arraylist;
    }
    public static int closed_today_incident_count(Connection con) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
        int count = 0;;  
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        GregorianCalendar cal = new GregorianCalendar();
        cal.set(cal.HOUR_OF_DAY,00);
        cal.set(cal.MINUTE,00);
        cal.set(cal.SECOND,00);
        String start_date = timestamp_format.format(cal.getTime());
        cal.set(cal.HOUR_OF_DAY,23);
        cal.set(cal.MINUTE,59);
        cal.set(cal.SECOND,59);
        String end_date = timestamp_format.format(cal.getTime());
        String ps_query_string = "";
        ps_query_string = "SELECT count(*) AS incident_count FROM incidents WHERE closed_date >= ? AND closed_date <= ?";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, start_date);
            stmt.setString(2, end_date);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("incident_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.closed_today_incident_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.closed_today_incident_count:=" + exc);
	}
        return count;
    }
    public static ArrayList<String[]> incidents_open(Connection con) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        String ps_query_string = "";
        //ps_query_string = "SELECT * FROM incidents WHERE state NOT LIKE '%Closed%' ORDER BY incident_time DESC";     
        ps_query_string = "SELECT "
                + "id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id," 
                + "caller_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company,"
                + "category,"
                + "subcategory," 
                + "ci_id," 
                + "impact,"
                + "urgency,"
                + "priority," 
                + "description," 
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date,"
                + "create_by_id,"
                + "contact_method,"
                + "state,"
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date,"
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date,"
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date,"
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE state NOT LIKE '%Closed%' ORDER BY incident_time DESC"; 
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incidents_open:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incidents_open:=" + exc);
	}
        return return_arraylist;
    }
    public static int open_incident_count(Connection con) throws IOException, SQLException
    {
        int count = 0;;  
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        String ps_query_string = "";
        ps_query_string = "SELECT count(*) AS incident_count FROM incidents WHERE state NOT LIKE '%Closed%'";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("incident_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.closed_today_incident_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.closed_today_incident_count:=" + exc);
	}
        return count;
    }
    public static ArrayList<String[]> incidents_open_7_14_days(Connection con) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        String ps_query_string = "";
        GregorianCalendar cal = new GregorianCalendar();
        cal.add(cal.DATE, -14);
        cal.set(cal.HOUR_OF_DAY,00);
        cal.set(cal.MINUTE,00);
        cal.set(cal.SECOND,00);
        String start_date = timestamp_format.format(cal.getTime());
        GregorianCalendar cal2 = new GregorianCalendar();
        cal2.add(cal2.DATE, -7);
        cal2.set(cal2.HOUR_OF_DAY,00);
        cal2.set(cal2.MINUTE,00);
        cal2.set(cal2.SECOND,00);
        String end_date = timestamp_format.format(cal2.getTime());
        //ps_query_string = "SELECT * FROM incidents WHERE state <> 'Closed' AND incident_time >= ? AND incident_time <= ? ORDER BY incident_time DESC";
        ps_query_string = "SELECT id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id,"
                + "caller_group_id,"
                + "location,"
                + "department,"
                + "site,"
                + "company,"
                + "category,"
                + "subcategory,"
                + "ci_id,"
                + "impact," 
                + "urgency,"
                + "priority,"
                + "description,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "create_by_id,"
                + "contact_method,"
                + "state," 
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date, "
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date, "
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE state <> 'Closed' AND incident_time >= ? AND incident_time <= ? ORDER BY incident_time DESC";
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, start_date);
            stmt.setString(2, end_date);            
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            System.out.println("ERROR SQLException get_incidents.incidents_open_7_14_days:=" + ex);
            logger.error("ERROR SQLException get_incidents.incidents_open_7_14_days:=" + ex);
        }
        catch(Exception exc) 
        {
            System.out.println("ERROR Exception get_incidents.incidents_open_7_14_days:=" + exc);
            logger.error("ERROR Exception get_incidents.incidents_open_7_14_days:=" + exc);
	}
        return return_arraylist;
    }
    public static int incidents_open_7_14_days_count(Connection con) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        int count = 0;;  
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        GregorianCalendar cal = new GregorianCalendar();
        cal.add(cal.DATE, -14);
        cal.set(cal.HOUR_OF_DAY,00);
        cal.set(cal.MINUTE,00);
        cal.set(cal.SECOND,00);
        String start_date = timestamp_format.format(cal.getTime());
        GregorianCalendar cal2 = new GregorianCalendar();
        cal2.add(cal2.DATE, -7);
        cal2.set(cal2.HOUR_OF_DAY,00);
        cal2.set(cal2.MINUTE,00);
        cal2.set(cal2.SECOND,00);
        String end_date = timestamp_format.format(cal2.getTime());
        String ps_query_string = "SELECT count(*) AS incident_count FROM incidents WHERE state <> 'Closed' AND create_date >= ? AND create_date <= ?";
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, start_date);
            stmt.setString(2, end_date);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("incident_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incidents_open_7_14_days_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incidents_open_7_14_days_count:=" + exc);
	}
        return count;
    }
    public static ArrayList<String[]> incidents_open_14_30_days(Connection con) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        String ps_query_string = "";
        GregorianCalendar cal = new GregorianCalendar();
        cal.add(cal.DATE, -30);
        cal.set(cal.HOUR_OF_DAY,00);
        cal.set(cal.MINUTE,00);
        cal.set(cal.SECOND,00);
        String start_date = timestamp_format.format(cal.getTime());
        GregorianCalendar cal2 = new GregorianCalendar();
        cal2.add(cal2.DATE, -14);
        cal2.set(cal2.HOUR_OF_DAY,00);
        cal2.set(cal2.MINUTE,00);
        cal2.set(cal2.SECOND,00);
        String end_date = timestamp_format.format(cal2.getTime());
        ps_query_string = "SELECT id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id,"
                + "caller_group_id,"
                + "location,"
                + "department,"
                + "site,"
                + "company,"
                + "category,"
                + "subcategory,"
                + "ci_id,"
                + "impact," 
                + "urgency,"
                + "priority,"
                + "description,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "create_by_id,"
                + "contact_method,"
                + "state," 
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date, "
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date, "
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE state <> 'Closed' AND create_date >= ? AND create_date <= ? ORDER BY incident_time DESC";
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, start_date);
            stmt.setString(2, end_date);
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incidents_open_7_14_days:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incidents_open_7_14_days:=" + exc);
	}
        return return_arraylist;
    }
    public static int incidents_open_14_30_days_count(Connection con) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        int count = 0;;  
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        GregorianCalendar cal = new GregorianCalendar();
        cal.add(cal.DATE, -30);
        cal.set(cal.HOUR_OF_DAY,00);
        cal.set(cal.MINUTE,00);
        cal.set(cal.SECOND,00);
        String start_date = timestamp_format.format(cal.getTime());
        GregorianCalendar cal2 = new GregorianCalendar();
        cal2.add(cal2.DATE, -14);
        cal2.set(cal2.HOUR_OF_DAY,00);
        cal2.set(cal2.MINUTE,00);
        cal2.set(cal2.SECOND,00);
        String end_date = timestamp_format.format(cal2.getTime());
        String ps_query_string = "SELECT count(*) AS incident_count FROM incidents WHERE state <> 'Closed' AND create_date >= ? AND create_date <= ?";
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, start_date);
            stmt.setString(2, end_date);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("incident_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incidents_open_7_14_days_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incidents_open_7_14_days_count:=" + exc);
	}
        return count;
    }
    public static ArrayList<String[]> incidents_open_30_plus_days(Connection con) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        String ps_query_string = "";
        GregorianCalendar cal = new GregorianCalendar();
        cal.add(cal.DATE, -30);
        cal.set(cal.HOUR_OF_DAY,00);
        cal.set(cal.MINUTE,00);
        cal.set(cal.SECOND,00);
        String start_date = timestamp_format.format(cal.getTime());
        ps_query_string = "SELECT id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id,"
                + "caller_group_id,"
                + "location,"
                + "department,"
                + "site,"
                + "company,"
                + "category,"
                + "subcategory,"
                + "ci_id,"
                + "impact," 
                + "urgency,"
                + "priority,"
                + "description,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "create_by_id,"
                + "contact_method,"
                + "state," 
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date, "
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date, "
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE state <> 'Closed' AND create_date <= ? ORDER BY incident_time DESC";
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, start_date);
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incidents_open_30_plus_days:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incidents_open_30_plus_days:=" + exc);
	}
        return return_arraylist;
    }
    public static int incidents_open_30_plus_days_count(Connection con) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        int count = 0;;  
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        GregorianCalendar cal = new GregorianCalendar();
        cal.add(cal.DATE, -30);
        cal.set(cal.HOUR_OF_DAY,00);
        cal.set(cal.MINUTE,00);
        cal.set(cal.SECOND,00);
        String start_date = timestamp_format.format(cal.getTime());
        String ps_query_string = "SELECT count(*) AS incident_count FROM incidents WHERE state <> 'Closed' AND create_date <= ?";
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, start_date);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("incident_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incidents_open_30_plus_days_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incidents_open_30_plus_days_count:=" + exc);
	}
        return count;
    }
    public static int count_by_priority(Connection con, String priority) throws IOException, SQLException
    {
        int return_int = 0;  
        String ps_query_string = "SELECT COUNT(*) AS value FROM incidents WHERE priority=? AND (incidents.state <> 'Closed' AND incidents.state <> 'Resolved')";        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, priority);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
                //System.out.println("q=" + stmt.toString());
                //System.out.println("value=" + return_int);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.count_by_priority:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.count_by_priority:=" + exc);
	}
        return return_int;
    }  
    public static ArrayList<String[]> incidents_by_priority(Connection con, String priority) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        //Incident ID,Priority ,Incident Time,State,State Time,Group,Analyst,Category,Sub-Category
        String ps_query_string = "";
        GregorianCalendar cal = new GregorianCalendar();
        cal.add(cal.DATE, -30);
        cal.set(cal.HOUR_OF_DAY,00);
        cal.set(cal.MINUTE,00);
        cal.set(cal.SECOND,00);
        String start_date = timestamp_format.format(cal.getTime());
        ps_query_string = "SELECT id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id,"
                + "caller_group_id,"
                + "location,"
                + "department,"
                + "site,"
                + "company,"
                + "category,"
                + "subcategory,"
                + "ci_id,"
                + "impact," 
                + "urgency,"
                + "priority,"
                + "description,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "create_by_id,"
                + "contact_method,"
                + "state," 
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date, "
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date, "
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE priority=? AND (incidents.state <> 'Closed' AND incidents.state <> 'Resolved')";    
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, priority);
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incidents_critical:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incidents_critical:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<Integer> my_incidents_count(Connection con, String user_id)
    {
        PreparedStatement stmt;
        int total = 0;
        int critical = 0;
        int high = 0;
        int medium = 0;
        int low = 0;
        String priority = "";
        ArrayList<Integer> return_arraylist = new ArrayList();
        try
        {
            //total
            String ps_query_string = "SELECT * from incidents where assigned_to_id=? and state <> 'closed' and state <> 'resolved'";        
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                total++;
                priority = rs.getString("priority");
                if(priority.equalsIgnoreCase("critical"))
                {
                    critical++;
                }
                else if(priority.equalsIgnoreCase("high"))
                {
                    high++;
                }
                else if(priority.equalsIgnoreCase("medium"))
                {
                    medium++;
                }
                else if(priority.equalsIgnoreCase("low"))
                {
                    low++;
                }
            }
            stmt.close();
            //total
            return_arraylist.add(0,total);
            //critical
            return_arraylist.add(1,critical);
            //high
            return_arraylist.add(2,high);
            //medium
            return_arraylist.add(3,medium);
            //low
            return_arraylist.add(4,low);
        }
        catch(Exception e)
        {
            System.out.println("Exception in db.get_incidents.my_incidents=" + e);
        }
        return return_arraylist;
    }
    public static ArrayList<String[]> my_incidents_by_priority(Connection con, String priority, String user_id)
    {
        int count = 0;
        PreparedStatement stmt;
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        try
        {
            ArrayList<String[]> users = db.get_users.all(con);
            ArrayList<String[]> groups = db.get_groups.all(con);
            //total
            //String ps_query_string = "SELECT * from incidents where priority=? AND assigned_to_id=? AND (state <> 'closed' AND state <> 'resolved')";
            String ps_query_string = "SELECT id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id,"
                + "caller_group_id,"
                + "location,"
                + "department,"
                + "site,"
                + "company,"
                + "category,"
                + "subcategory,"
                + "ci_id,"
                + "impact," 
                + "urgency,"
                + "priority,"
                + "description,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "create_by_id,"
                + "contact_method,"
                + "state," 
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date, "
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date, "
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE priority=? AND assigned_to_id=? AND (state <> 'closed' AND state <> 'resolved')";
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, priority);
            stmt.setString(2, user_id);
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
        }
        catch(Exception e)
        {
            System.out.println("Exception in db.get_incidents.my_incidents_by_priority=" + e);
        }
        return return_arraylist;
    }
    public static ArrayList<String[]> my_open_incidents(Connection con, String user_id)
    {
        int count = 0;
        PreparedStatement stmt;
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        try
        {
            ArrayList<String[]> users = db.get_users.all(con);
            ArrayList<String[]> groups = db.get_groups.all(con);
            //total
            //String ps_query_string = "SELECT * from incidents where (caller_id=? OR assigned_to_id=?) AND (state <> 'closed' AND state <> 'resolved')";    
            String ps_query_string = "SELECT id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id,"
                + "caller_group_id,"
                + "location,"
                + "department,"
                + "site,"
                + "company,"
                + "category,"
                + "subcategory,"
                + "ci_id,"
                + "impact," 
                + "urgency,"
                + "priority,"
                + "description,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "create_by_id,"
                + "contact_method,"
                + "state," 
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date, "
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes,"
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date, "
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE (caller_id=? OR assigned_to_id=?) AND (state <> 'closed' AND state <> 'resolved')";    
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, user_id);
            stmt.setString(2, user_id);
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();
            
        }
        catch(Exception e)
        {
            System.out.println("Exception in db.get_incidents.my_open_incidents=" + e);
        }
        return return_arraylist;
    }
    public static ArrayList<Integer> dashboard_incidents_count(Connection con)
    {
        ArrayList<Integer> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        int total = 0;
        int critical = 0;
        int high = 0;
        int medium = 0;
        int low = 0;
        String priority = "";
        try
        {
            //total
            String ps_query_string = "SELECT * from incidents where state <> 'closed' and state <> 'resolved'";        
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                total++;
                priority = support.string_utils.check_for_null(rs.getString("priority"));
                if(priority.equalsIgnoreCase("critical"))
                {
                    critical++;
                }
                else if(priority.equalsIgnoreCase("high"))
                {
                    high++;
                }
                else if(priority.equalsIgnoreCase("medium"))
                {
                    medium++;
                }
                else if(priority.equalsIgnoreCase("low"))
                {
                    low++;
                }
            }
            stmt.close();
            
            return_arraylist.add(0,total);
            //critical
            return_arraylist.add(1,critical);
            //high
            return_arraylist.add(2,high);
            //medium
            return_arraylist.add(3,medium);
            //low
            return_arraylist.add(4,low);
        }
        catch(Exception e)
        {
            System.out.println("Exception in db.get_incidents.dashboard_incidents_count=" + e);
        }
        return return_arraylist;
    }
    public static ArrayList<String[]> incidents_for_user_id(Connection con, String user_id)
    {
        int count = 0;
        PreparedStatement stmt;
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        try
        {
            ArrayList<String[]> users = db.get_users.all(con);
            ArrayList<String[]> groups = db.get_groups.all(con);
            //total
            //String ps_query_string = "SELECT * FROM incidents WHERE caller_id=? order by incident_time desc limit 100;";   
            String ps_query_string = "SELECT id,"
                + "incident_type,"
                + "DATE_FORMAT(incident_time,'%Y%m%d%H%i%s') AS incident_time, "
                + "caller_id,"
                + "caller_group_id,"
                + "location,"
                + "department,"
                + "site,"
                + "company,"
                + "category,"
                + "subcategory,"
                + "ci_id,"
                + "impact," 
                + "urgency,"
                + "priority,"
                + "description,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "create_by_id,"
                + "contact_method,"
                + "state," 
                + "DATE_FORMAT(state_date,'%Y%m%d%H%i%s') AS state_date, "
                + "assigned_group_id,"
                + "assigned_to_id,"
                + "notes," // 23
                + "desk_notes,"
                + "related,"
                + "first_contact_resolution,"
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "closed_reason,"
                + "DATE_FORMAT(pending_date,'%Y%m%d%H%i%s') AS pending_date, "
                + "pending_reason,"
                + "external_id "
                + "FROM incidents WHERE caller_id=? order by incident_time desc limit 100;";   
            
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            return_arraylist = process_resultset(rs,users,groups);
            stmt.close();            
        }
        catch(Exception e)
        {
            System.out.println("Exception in db.get_incidents.my_open_incidents=" + e);
        }
        return return_arraylist;
    }
    public static ArrayList<String[]> get_attachments_by_incident_id(Connection con, String incident_id) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        PreparedStatement stmt;

        try 
        {

            // Create a Statement Object
            String query = "SELECT incident_attachements.*,users.first,users.last from incident_attachements LEFT JOIN users ON incident_attachements.uploaded_by = users.id WHERE incident_id = ?";
            stmt = con.prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, incident_id);
//            System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();

            while(rs.next())
            {
                String attachment[] = new String[10];
                attachment[0] = rs.getString("id");
                attachment[1] = rs.getString("incident_id");
                attachment[2] = rs.getString("uuid");
                attachment[3] = rs.getString("name_on_disk");
                attachment[4] = rs.getString("file_name");
                attachment[5] = rs.getString("file_date");
                attachment[6] = rs.getString("file_size");
                attachment[7] = rs.getString("rev");
                attachment[8] = rs.getString("uploaded_by");
                attachment[9] = rs.getString("first") + " " + rs.getString("last");
                return_array_list.add(attachment);
            }

            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.get_attachments_by_incident_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.get_attachments_by_incident_id:=" + exc);
	}
        return return_array_list;
    }

    public static Map<String, String> incidents_count_by_states(Connection con, String user_id) {
        Map<String, String> map = new HashMap<>();
        PreparedStatement stmt;

        try 
        {

            // Create a Statement Object
            String query = "select state,count(id) as incidents_count FROM incidents " + (!user_id.equals("") ? "WHERE assigned_to_id = " + user_id : "") + " GROUP BY state;";
            stmt = con.prepareStatement(query);
//            System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();
            int total_count = 0;
            while(rs.next())
            {
                map.put(rs.getString("state"), rs.getString("incidents_count"));
                total_count = total_count + Integer.parseInt(rs.getString("incidents_count"));
            }
            map.put("all", String.valueOf(total_count));

            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_incidents.incidents_count_by_states:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_incidents.incidents_count_by_states:=" + exc);
	}
        return map;
    }
}

//Copyright 2019 XaSystems, Inc. , All rights reserved.

package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 *
 * @author server-xc6701
 */
public class get_groups 
{
    private static Logger logger = LogManager.getLogger();
    
    /*public static String[][] all(Connection con) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM `groups`",ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][5];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("name");
                returnString[count][2] = rs.getString("description");
                returnString[count][3] = rs.getString("poc_id");
                returnString[count][4] = rs.getString("external_id");
                count++;                
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_groups.all:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_groups.all:=" + exc);
	}
        return returnString;
    }
    */
    public static ArrayList<String[]> all(Connection con) throws IOException, SQLException
    {
        ArrayList<String[]> returnArrayList = new ArrayList();
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM `groups`");
            ResultSet rs = stmt.executeQuery();
            
            
            while(rs.next())
            {
                String[] this_record = new String[5];
                this_record[0] = rs.getString("id");
                this_record[1] = rs.getString("name");
                this_record[2] = rs.getString("description");
                this_record[3] = rs.getString("poc_id");
                this_record[4] = rs.getString("external_id");
                returnArrayList.add(this_record);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_groups.all:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_groups.all:=" + exc);
	}
        return returnArrayList;
    }
    public static String[] by_id(Connection con, String id) throws IOException, SQLException
    {
        String returnString[] = {"","","","",""};
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM `groups` WHERE id=?");
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                returnString[0] = rs.getString("id");
                returnString[1] = rs.getString("name");
                returnString[2] = rs.getString("description");
                returnString[3] = rs.getString("poc_id");
                returnString[4] = rs.getString("external_id");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_groups.by_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_groups.by_id:=" + exc);
	}
        return returnString;
    }
    public static ArrayList<String[]> for_user_id(Connection con, String user_id) throws IOException, SQLException
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM users_groups WHERE user_id=?",ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, user_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String user_group_record[] = new String[4];
                user_group_record[0] = rs.getString("user_id");
                user_group_record[1] = rs.getString("group_id");
                user_group_record[2] = rs.getString("role");
                user_group_record[3] = rs.getString("external_id");
                return_arraylist.add(user_group_record);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_groups.for_user_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_groups.for_user_id:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> with_poc_info(Connection con) throws IOException, SQLException
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT `groups`.`id`, `groups`.`name`, `groups`.`description`,`users`.`id`,`users`.`username`,`users`.`first`,`users`.`last`,`users`.`email`,`users`.`phone_office`,`users`.`phone_mobile` FROM `groups` LEFT JOIN `users` ON `users`.`id` = `groups`.`poc_id`",ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String user_group_record[] = new String[10];
                user_group_record[0] = rs.getString("groups.id");
                user_group_record[1] = rs.getString("groups.name");
                user_group_record[2] = rs.getString("groups.description");
                user_group_record[3] = rs.getString("users.id");
                user_group_record[4] = rs.getString("users.username");
                user_group_record[5] = rs.getString("users.first");
                user_group_record[6] = rs.getString("users.last");
                user_group_record[7] = rs.getString("users.email");
                user_group_record[8] = rs.getString("users.phone_office");
                user_group_record[9] = rs.getString("users.phone_mobile");                
                return_arraylist.add(user_group_record);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_groups.with_poc_info:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_groups.with_poc_info:=" + exc);
	}
        return return_arraylist;
    }
    public static int next_id(Connection con) throws IOException, SQLException
    {
        int returnInt = 1;
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT max(id) as value FROM `groups`");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                returnInt = rs.getInt("value");
                returnInt++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_groups.next_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_groups.next_id:=" + exc);
	}
        return returnInt;
    }
    
}

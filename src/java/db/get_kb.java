/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class get_kb 
{
    private static Logger logger = LogManager.getLogger();
    static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
    
    public static String check_for_null(String value)
    {
        String return_value = "";
        
        if(value != null && !value.isEmpty())
        {
            return_value = value;
        }
        else
        {
            return_value = "";
        }
        return return_value;
    }    
    public static ArrayList<String[]> all_kb(Connection con) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        String query = "SELECT * FROM knowledge_base"; 
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String kb_info[] = new String[5];
                //init the array
                for(int a = 0; a < kb_info.length;a++)
                {
                    kb_info[a] = "--";
                }
                kb_info[0] = rs.getString("id");
                kb_info[1] = check_for_null(rs.getString("name"));
                kb_info[2] = check_for_null(rs.getString("description"));
                kb_info[3] = check_for_null(rs.getString("state"));
                kb_info[4] = check_for_null(rs.getString("type"));                
                return_array_list.add(kb_info);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_kb.all_kb:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_kb.all_kb:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> all_articles_in_kb(Connection con, String kb_id) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        String query = "SELECT * FROM knowledge_articles WHERE knowledge_base_id=?;"; 
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, kb_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String article_info[] = new String[12];
                //init the array
                for(int a = 0; a < article_info.length;a++)
                {
                    article_info[a] = "--";
                }                
                article_info[0] = rs.getString("id");
                article_info[1] = check_for_null(rs.getString("knowledge_base_id"));
                article_info[2] = check_for_null(rs.getString("article_name"));
                article_info[3] = check_for_null(rs.getString("article_description"));
                article_info[4] = check_for_null(rs.getString("status"));  
                article_info[5] = check_for_null(rs.getString("article_text"));  
                article_info[6] = check_for_null(rs.getString("keywords"));  
                article_info[7] = check_for_null(rs.getString("created_by_id"));  
                article_info[8] = check_for_null(rs.getString("created_date"));  
                article_info[9] = check_for_null(rs.getString("approved_by_id"));  
                article_info[10] = check_for_null(rs.getString("approved_date"));  
                article_info[11] = check_for_null(rs.getString("valid_till"));   
                return_array_list.add(article_info);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_kb.all_articles_in_kb:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_kb.all_articles_in_kb:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> all_articles(Connection con) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM knowledge_articles"; 
        String query = "SELECT `knowledge_articles`.*,`knowledge_base`.`name` AS kb_name FROM knowledge_articles INNER JOIN knowledge_base ON knowledge_articles.knowledge_base_id = knowledge_base.id;";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String article_info[] = new String[13];
                //init the array
                for(int a = 0; a < article_info.length;a++)
                {
                    article_info[a] = "--";
                }                
                article_info[0] = rs.getString("id");
                article_info[1] = check_for_null(rs.getString("knowledge_base_id"));
                article_info[2] = check_for_null(rs.getString("article_name"));
                article_info[3] = check_for_null(rs.getString("article_description"));
                article_info[4] = check_for_null(rs.getString("status"));  
                article_info[5] = check_for_null(rs.getString("article_text"));  
                article_info[6] = check_for_null(rs.getString("keywords"));  
                article_info[7] = check_for_null(rs.getString("created_by_id"));  
                article_info[8] = check_for_null(rs.getString("created_date"));  
                article_info[9] = check_for_null(rs.getString("approved_by_id"));  
                article_info[10] = check_for_null(rs.getString("approved_date"));  
                article_info[11] = check_for_null(rs.getString("valid_till"));   
                article_info[12] = check_for_null(rs.getString("kb_name"));   
                return_array_list.add(article_info);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_kb.all_articles:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_kb.all_articles:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> all_articles_filtered(Connection con, String where) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM knowledge_articles"; 
//        String query = "SELECT `knowledge_articles`.*,`knowledge_base`.`name` AS kb_name FROM knowledge_articles INNER JOIN knowledge_base ON knowledge_articles.knowledge_base_id = knowledge_base.id;";
        PreparedStatement stmt;
        String query;
        try 
        {
            // Create a Statement Object
            query = "SELECT `knowledge_articles`.*,`knowledge_base`.`name` AS kb_name,t_usages.usage_count,t_feedback_true.true_feedback,t_feedback_false.false_feedback,t_chat_messages.messages_count FROM knowledge_articles " +
                    "INNER JOIN knowledge_base ON knowledge_articles.knowledge_base_id = knowledge_base.id " +
                    "LEFT JOIN (SELECT article_id,count(*) as usage_count FROM `knowledge_article_usages` GROUP BY article_id) as t_usages ON t_usages.article_id = `knowledge_articles`.id " + 
                    "LEFT JOIN (SELECT article_id,count(*) as true_feedback FROM `knowledge_article_feedback` WHERE helpful='true' GROUP BY article_id) as t_feedback_true ON t_feedback_true.article_id = `knowledge_articles`.id " + 
                    "LEFT JOIN (SELECT article_id,count(*) as false_feedback FROM `knowledge_article_feedback` WHERE helpful='false' GROUP BY article_id) as t_feedback_false ON t_feedback_false.article_id = `knowledge_articles`.id " + 
                    "LEFT JOIN (SELECT object_id,count(*) as messages_count FROM `chat_messages` WHERE object_type='article' GROUP BY object_id) as t_chat_messages ON t_chat_messages.object_id = `knowledge_articles`.id " + 
                    where;
            stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String article_info[] = new String[17];
                //init the array
                for(int a = 0; a < article_info.length;a++)
                {
                    article_info[a] = "--";
                }                
                article_info[0] = rs.getString("id");
                article_info[1] = check_for_null(rs.getString("knowledge_base_id"));
                article_info[2] = check_for_null(rs.getString("article_name"));
                article_info[3] = check_for_null(rs.getString("article_description"));
                article_info[4] = check_for_null(rs.getString("status"));  
                article_info[5] = check_for_null(rs.getString("article_text"));  
                article_info[6] = check_for_null(rs.getString("keywords"));  
                article_info[7] = check_for_null(rs.getString("created_by_id"));  
                article_info[8] = check_for_null(rs.getString("created_date"));  
                article_info[9] = check_for_null(rs.getString("approved_by_id"));  
                article_info[10] = check_for_null(rs.getString("approved_date"));  
                article_info[11] = check_for_null(rs.getString("valid_till"));   
                article_info[12] = check_for_null(rs.getString("kb_name"));   
                article_info[13] = check_for_null(rs.getString("usage_count"));   
                article_info[14] = check_for_null(rs.getString("true_feedback"));   
                article_info[15] = check_for_null(rs.getString("false_feedback"));   
                article_info[16] = check_for_null(rs.getString("messages_count"));   
                return_array_list.add(article_info);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_kb.all_articles:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_kb.all_articles:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> categories_filtered(Connection con, String where) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM knowledge_articles"; 
//        String query = "SELECT `knowledge_articles`.*,`knowledge_base`.`name` AS kb_name FROM knowledge_articles INNER JOIN knowledge_base ON knowledge_articles.knowledge_base_id = knowledge_base.id;";
        PreparedStatement stmt;
        String query;
        try 
        {
            // Create a Statement Object
            query = "SELECT knowledge_articles.knowledge_base_id, `knowledge_base`.`name` AS kb_name FROM knowledge_articles " +
                    "LEFT JOIN knowledge_base ON knowledge_articles.knowledge_base_id = knowledge_base.id "
                    + (where.equals("") ? "" : " WHERE " + where);
            stmt = con.prepareStatement(query);
//            System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String article_info[] = new String[2];
                //init the array
                for(int a = 0; a < article_info.length;a++)
                {
                    article_info[a] = "--";
                }                
                article_info[0] = check_for_null(rs.getString("knowledge_base_id"));
                article_info[1] = check_for_null(rs.getString("kb_name"));   
                return_array_list.add(article_info);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_kb.all_articles:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_kb.all_articles:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> all_articles_filtered(Connection con, int starting_record, int fetch_size, String where, String order_column, String order_dir) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        //String query = "SELECT * FROM knowledge_articles"; 
//        String query = "SELECT `knowledge_articles`.*,`knowledge_base`.`name` AS kb_name FROM knowledge_articles INNER JOIN knowledge_base ON knowledge_articles.knowledge_base_id = knowledge_base.id;";
        PreparedStatement stmt;
        String query;
        try 
        {
            // Create a Statement Object
            query = "SELECT `knowledge_articles`.*,`knowledge_base`.`name` AS kb_name,t_usages.usage_count,t_feedback_true.true_feedback,t_feedback_false.false_feedback FROM knowledge_articles " +
                    "LEFT JOIN knowledge_base ON knowledge_articles.knowledge_base_id = knowledge_base.id " +
                    "LEFT JOIN (SELECT article_id,count(*) as usage_count FROM `knowledge_article_usages` GROUP BY article_id) as t_usages ON t_usages.article_id = `knowledge_articles`.id " + 
                    "LEFT JOIN (SELECT article_id,count(*) as true_feedback FROM `knowledge_article_feedback` WHERE helpful='true' GROUP BY article_id) as t_feedback_true ON t_feedback_true.article_id = `knowledge_articles`.id " + 
                    "LEFT JOIN (SELECT article_id,count(*) as false_feedback FROM `knowledge_article_feedback` WHERE helpful='false' GROUP BY article_id) as t_feedback_false ON t_feedback_false.article_id = `knowledge_articles`.id " 
                    + (where.equals("") ? "" : " WHERE " + where);
            stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String article_info[] = new String[16];
                //init the array
                for(int a = 0; a < article_info.length;a++)
                {
                    article_info[a] = "--";
                }                
                article_info[0] = rs.getString("id");
                article_info[1] = check_for_null(rs.getString("knowledge_base_id"));
                article_info[2] = check_for_null(rs.getString("article_name"));
                article_info[3] = check_for_null(rs.getString("article_description"));
                article_info[4] = check_for_null(rs.getString("status"));  
                article_info[5] = check_for_null(rs.getString("article_text"));  
                article_info[6] = check_for_null(rs.getString("keywords"));  
                article_info[7] = check_for_null(rs.getString("created_by_id"));  
                article_info[8] = check_for_null(rs.getString("created_date"));  
                article_info[9] = check_for_null(rs.getString("approved_by_id"));  
                article_info[10] = check_for_null(rs.getString("approved_date"));  
                article_info[11] = check_for_null(rs.getString("valid_till"));   
                article_info[12] = check_for_null(rs.getString("kb_name"));   
                article_info[13] = check_for_null(rs.getString("usage_count"));   
                article_info[14] = check_for_null(rs.getString("true_feedback"));   
                article_info[15] = check_for_null(rs.getString("false_feedback"));   
                return_array_list.add(article_info);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_kb.all_articles:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_kb.all_articles:=" + exc);
	}
        return return_array_list;
    }
    public static int all_articles_filtered_count(Connection con, String where) throws IOException, SQLException
    {
        int return_int = 0;  
        PreparedStatement stmt;
        String query;
        try 
        {
            // Create a Statement Object
            query = "SELECT count(*) as value FROM knowledge_articles " + (where.equals("") ? "" : " WHERE " + where);

            stmt = con.prepareStatement(query);
//            System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_kb.all_articles_filtered_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_kb.all_articles_filtered_count:=" + exc);
	}
        return return_int;
    }
    public static String[] kb_info_for_id(Connection con, String id) throws IOException, SQLException
    {
        String[] return_array = {"","","","",""};
        String query = "SELECT * FROM knowledge_base WHERE id=?"; 
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_array[0] = rs.getString("id");
                return_array[1] = check_for_null(rs.getString("name"));
                return_array[2] = check_for_null(rs.getString("description"));
                return_array[3] = check_for_null(rs.getString("state"));
                return_array[4] = check_for_null(rs.getString("type"));   
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_kb.kb_info_for_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_kb.kb_info_for_id:=" + exc);
	}
        return return_array;
    }
    public static String[] article_info_for_id(Connection con, String id) throws IOException, SQLException
    {
        String[] return_array = {"","","","","","","","","","","","","","","","","",""};
        //String query = "SELECT * FROM knowledge_articles"; 
        String query = "SELECT `knowledge_articles`.*,`knowledge_base`.`name` AS kb_name,users.first,users.last,users.mi,t_feedback_true.helpful_true,t_feedback_false.helpful_false  FROM knowledge_articles "
 + "INNER JOIN knowledge_base ON knowledge_articles.knowledge_base_id = knowledge_base.id "
 + "LEFT JOIN users ON knowledge_articles.created_by_id = users.id "
 + "LEFT JOIN (SELECT article_id,count(*) as helpful_true FROM `knowledge_article_feedback` WHERE helpful='true' GROUP BY article_id) as t_feedback_true ON t_feedback_true.article_id = `knowledge_articles`.id "
 + "LEFT JOIN (SELECT article_id,count(*) as helpful_false FROM `knowledge_article_feedback` WHERE helpful='false' GROUP BY article_id) as t_feedback_false ON t_feedback_false.article_id = `knowledge_articles`.id "
 + "WHERE knowledge_articles.id=?";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, id);
//            System.out.println("q=" + stmt);

            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_array[0] = rs.getString("id");
                return_array[1] = check_for_null(rs.getString("knowledge_articles.knowledge_base_id"));
                return_array[2] = check_for_null(rs.getString("knowledge_articles.article_name"));
                return_array[3] = check_for_null(rs.getString("knowledge_articles.article_description"));
                return_array[4] = check_for_null(rs.getString("knowledge_articles.status"));  
                return_array[5] = check_for_null(rs.getString("knowledge_articles.article_text"));  
                return_array[6] = check_for_null(rs.getString("knowledge_articles.keywords"));  
                return_array[7] = check_for_null(rs.getString("knowledge_articles.created_by_id"));  
                try
                {
                    return_array[8] = check_for_null(timestamp_format.format(rs.getTimestamp("knowledge_articles.created_date").getTime())); 
                }
                catch(Exception e)
                {
                    return_array[8] = "";
                }
                return_array[9] = check_for_null(rs.getString("knowledge_articles.approved_by_id"));  
                try
                {
                    return_array[10] = check_for_null(timestamp_format.format(rs.getTimestamp("knowledge_articles.approved_date").getTime())); 
                }
                catch(Exception e)
                {
                    return_array[10] = "";
                }
                try
                {
                    return_array[11] = check_for_null(timestamp_format.format(rs.getTimestamp("knowledge_articles.valid_till").getTime())); 
                }
                catch(Exception e)
                {
                    return_array[11] = "";
                }
                return_array[12] = check_for_null(rs.getString("knowledge_base.kb_name")); 
                return_array[13] = check_for_null(rs.getString("first")); 
                return_array[14] = check_for_null(rs.getString("last")); 
                return_array[15] = check_for_null(rs.getString("mi")); 
                return_array[16] = StringUtils.defaultString(rs.getString("helpful_true"), "0"); 
                return_array[17] = StringUtils.defaultString(rs.getString("helpful_false"), "0"); 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_kb.article_info_for_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_kb.article_info_for_id:=" + exc);
	}
        return return_array;
    }
    public static boolean use_article(Connection con, String user_id, String article_id) throws IOException, SQLException
    {
        boolean return_boolean = false;
        java.util.Date now = new java.util.Date();
        String query = "INSERT INTO knowledge_article_usages (user_id,article_id,date_viewed) VALUES (?,?,?)";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setString(1, user_id);
            stmt.setString(2, article_id);
            stmt.setTimestamp(3, new java.sql.Timestamp(now.getTime()));
            stmt.executeUpdate();
            stmt.close();
            return_boolean = true;
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_kb.use_article:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_kb.use_article:=" + exc);
	}
        return return_boolean;
    }
    
    
    
    
    
    
    
    //////OLD STUFF ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static int[] get_feedback_for_id_and_timeframe(Connection con, String article_id, java.util.Date start, java.util.Date end)
    {
        int return_int[] = new int[2];
        int positive = 0;
        int negative = 0;
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT * FROM article_feedbacks WHERE article_number = ? AND created_date > ? AND created_date < ?", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1,article_id);
            stmt.setTimestamp(2, new Timestamp(start.getTime()));
            stmt.setTimestamp(3, new Timestamp(end.getTime()));
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                if(rs.getInt("useful") == 0)
                {
                    negative++;
                }
                else
                {
                    positive++;
                }
            }
            stmt.close();
            return_int[0] = positive;
            return_int[1] = negative;
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_articles.get_feedback_for_id_and_timeframe:=" + exc);
	}
        //
        return return_int;
    }    
    public static String get_name_from_id(Connection con, String id)
    {
        String return_string = "--";
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT article_name FROM articles WHERE number = ?", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1,id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_string = rs.getString("article_name");
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_articles.get_name_from_id:=" + exc);
	}
        return return_string;
    }
    
    public static String[][] get_top_10_view_for_timeframe(Connection con, java.util.Date start, java.util.Date end) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT article_number, COUNT(article_number) AS value FROM article_usages WHERE viewed > ? and viewed < ? group by article_number ORDER BY value desc LIMIT 10", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, new Timestamp(start.getTime()));
            stmt.setTimestamp(2, new Timestamp(end.getTime()));
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count][5]; //number, name, viewed, positive, negative
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("article_number");
                //get name
                return_string[count][1] = get_name_from_id(con,rs.getString("article_number"));
                return_string[count][2] = rs.getString("value");
                int feedback[] = get_feedback_for_id_and_timeframe(con, rs.getString("article_number"), start, end);
                return_string[count][3] = String.valueOf(feedback[0]);
                return_string[count][4] = String.valueOf(feedback[1]);
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_articles.get_top_10_view_for_timeframe:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_articles.get_top_10_view_for_timeframe:=" + exc);
	}
        
        return return_string;
    }
    
    public static String[][] get_all_articles_view_for_timeframe(Connection con, java.util.Date start, java.util.Date end) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT article_number, COUNT(article_number) AS value FROM article_usages WHERE viewed > ? and viewed < ? group by article_number ORDER BY value desc", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, new Timestamp(start.getTime()));
            stmt.setTimestamp(2, new Timestamp(end.getTime()));
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count][5]; //number, name, viewed, positive, negative
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("article_number");
                //get name
                return_string[count][1] = get_name_from_id(con,rs.getString("article_number"));
                return_string[count][2] = rs.getString("value");
                int feedback[] = get_feedback_for_id_and_timeframe(con, rs.getString("article_number"), start, end);
                return_string[count][3] = String.valueOf(feedback[0]);
                return_string[count][4] = String.valueOf(feedback[1]);
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_articles.get_all_articles_view_for_timeframe:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_articles.get_all_articles_view_for_timeframe:=" + exc);
	}
        
        return return_string;
    }
    
    public static String[][] get_top_10_least_helpful_for_timeframe(Connection con, java.util.Date start, java.util.Date end) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT article_number, COUNT(article_number) as value FROM article_feedbacks WHERE created_date > ? AND created_date < ? AND  useful='0' GROUP BY article_number ORDER BY value desc LIMIT 10", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, new Timestamp(start.getTime()));
            stmt.setTimestamp(2, new Timestamp(end.getTime()));
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count][5]; //number, name, viewed, positive, negative
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("article_number");
                //get name
                return_string[count][1] = get_name_from_id(con,rs.getString("article_number"));
                return_string[count][2] = rs.getString("value");
                int feedback[] = get_feedback_for_id_and_timeframe(con, rs.getString("article_number"), start, end);
                return_string[count][3] = String.valueOf(feedback[0]);
                return_string[count][4] = String.valueOf(feedback[1]);
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_articles.get_top_10_least_helpful_for_timeframe:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_articles.get_top_10_least_helpful_for_timeframe:=" + exc);
	}
        
        return return_string;
    }
    
    public static String[][] get_top_10_most_helpful_for_timeframe(Connection con, java.util.Date start, java.util.Date end) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT article_number, COUNT(article_number) as value FROM article_feedbacks WHERE created_date > ? AND created_date < ? AND  useful='1' GROUP BY article_number ORDER BY value desc LIMIT 10", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, new Timestamp(start.getTime()));
            stmt.setTimestamp(2, new Timestamp(end.getTime()));
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count][5]; //number, name, viewed, positive, negative
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("article_number");
                //get name
                return_string[count][1] = get_name_from_id(con,rs.getString("article_number"));
                return_string[count][2] = rs.getString("value");
                int feedback[] = get_feedback_for_id_and_timeframe(con, rs.getString("article_number"), start, end);
                return_string[count][3] = String.valueOf(feedback[0]);
                return_string[count][4] = String.valueOf(feedback[1]);
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_articles.get_top_10_most_helpful_for_timeframe:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_articles.get_top_10_most_helpful_for_timeframe:=" + exc);
	}
        
        return return_string;
    }
    
    
}

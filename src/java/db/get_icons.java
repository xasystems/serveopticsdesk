//Copyright 2020 XaSystems, Inc. , All rights reserved.

package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class get_icons 
{
    private static Logger logger = LogManager.getLogger();
    public static ArrayList<String> all_icons(Connection con)
    {
        ArrayList <String> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM icons");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {   
                return_arraylist.add(rs.getString("name"));
            }
            stmt.close();
        }
        catch(SQLException ex)
        {
            logger.error("ERROR SQLException get_icons.all_icons:=" + ex);
        }
        catch(Exception exc)
        {
            logger.error("ERROR Exception get_icons.all_icons:=" + exc);
	}
        return return_arraylist;
    }
}

/*
 * Copyright 2019 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class get_cost 
{
    private static Logger logger = LogManager.getLogger();
    
    public static Double per_ticket(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        Double return_double = 0.00;
        double number_of_days = 0;
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        
        //get number of days in request
        long difference = end_date.getTime() - start_date.getTime();
        float days_between = (difference / (1000*60*60*24));        
        //round up number of days
        number_of_days = Math.round( days_between * 100.0 ) / 100.0;
        int i = (int) number_of_days;
        //create an array to hold the cost per day
        double cost_per_day_array[] = new double[i]; 
        //init the array
        for(int a = 0; a < cost_per_day_array.length; a++)
        {
            cost_per_day_array[a] = 0.00;
        }
        
        //get the monthly cost for the requested group
        Statement stmt;
        String query = "";
        try 
        {
            // Create a Statement Object
            stmt = con.createStatement();
            if(group_id.equalsIgnoreCase("all"))
            {
                query = "SELECT * FROM cost WHERE (date >='" + timestamp_format.format(start_date) + "' AND date <='" + timestamp_format.format(end_date) + "')";

            }
            else
            {
                query = "SELECT * FROM cost WHERE (date >='" + timestamp_format.format(start_date) + "' AND date <='" + timestamp_format.format(end_date) + "')";
            }
            
            
            String year = "";
            ResultSet rs = stmt.executeQuery("SELECT * FROM cost WHERE (date >= '" + year + "0101000000' AND date <= '" + year + "1201000000') AND group_id='" + group_id + "' ORDER BY date");
            //System.out.println("q=" + "SELECT * FROM cost WHERE (date >= '" + year + "0101000000' AND date <= '" + year + "1201000000') AND group_id='" + group_id + "' ORDER BY date");
            while(rs.next())
            {
                //figure out which month this is 
                java.util.Date record_date = rs.getDate("date");
                Calendar cal = Calendar.getInstance();
                cal.setTime(record_date);
                int month = cal.get(Calendar.MONTH);
                //System.out.println("date=" + record_date);
                //System.out.println("month=" + month);
                //return_double[month] = rs.getDouble("cost");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_cost.for_year_and_group_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_cost.for_year_and_group_id:=" + exc);
	}
        return return_double;
    }
}

/*
 * Copyright 2019 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class system_schedules 
{
    private static Logger logger = LogManager.getLogger();
    
    public static String[][] get_all(Connection con) throws IOException, SQLException
    {
        String return_string[][] = new String[0][0];  
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        String ps_query_string = "SELECT * FROM system_schedules";
        int count = 0;
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            //init the return
            while(rs.next())
            {
                count++;
            }
            return_string = new String[count][3];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                return_string[count][0] = rs.getString("name");//name
                return_string[count][1] = rs.getString("freq_minutes");//freq_minutes
                try
                {
                    return_string[count][2] = timestamp_format.format(rs.getTimestamp("last_run")); //last_run
                }
                catch(Exception e)
                {
                    return_string[count][3] = "";
                }
                
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_system_schedules.all:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_system_schedules.all:=" + exc);
	}
        return return_string;
    }   
    public static String[] get_by_name(Connection con, String name) throws IOException, SQLException
    {
        String return_string[] = {"","",""};  
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        String ps_query_string = "SELECT * FROM system_schedules WHERE name=?";
        int count = 0;
        PreparedStatement stmt;        
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_string[0] = rs.getString("name");//name
                return_string[1] = rs.getString("freq_minutes");//freq_minutes
                try
                {
                    return_string[2] = timestamp_format.format(rs.getTimestamp("last_run")); //last_run
                }
                catch(Exception e)
                {
                    return_string[2] = "";
                }                
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_system_schedules.by_name:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_system_schedules.by_name:=" + exc);
	}
        return return_string;
    }
    public static void set_last_run(Connection con, java.util.Date last_run_date, String name) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        String ps_query_string = "UPDATE system_schedules SET last_run=? WHERE name=?";
        PreparedStatement stmt;        
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, new Timestamp(last_run_date.getTime()));
            stmt.setString(2,name);
            stmt.executeUpdate();            
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException system_schedules.set_last_run:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception system_schedules.set_last_run:=" + exc);
	}
    }
}

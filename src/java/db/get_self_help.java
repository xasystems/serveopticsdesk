/*
 * Copyright 2019 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class get_self_help 
{
    private static Logger logger = LogManager.getLogger();
    
    public static int sessions_per_timeframe(Connection con, java.util.Date start, java.util.Date end) throws IOException, SQLException
    {
        int return_int = 0;        
        PreparedStatement stmt;
        try 
        {
            Timestamp start_timestamp = new Timestamp(start.getTime());
            Timestamp end_timestamp = new Timestamp(end.getTime());
            
            //get number of self-help sessions
            stmt = con.prepareStatement("SELECT count(*) AS value FROM sessions WHERE start_date >= ? AND start_date <= ?");
            stmt.setTimestamp(1, start_timestamp);
            stmt.setTimestamp(2, end_timestamp);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_self_help.effectiveness_for_timeframe:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_self_help.effectiveness_for_timeframe:=" + exc);
	}
        return return_int;
    }
}

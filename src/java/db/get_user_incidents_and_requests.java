//<!--Copyright 2021 Real Data Technologies, Inc. , All rights reserved.-->

package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author ralph
 */
public class get_user_incidents_and_requests 
{
    static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
    
    public static ArrayList<String[]> for_email(Connection con, String email) throws IOException, SQLException
    {
        ArrayList <String[]> return_arraylist = new ArrayList();        
        long array_to_sort[][] = new long[0][0];
        String array_content[][] = new String[0][0];
        java.util.Date now = new java.util.Date();
        
        //get user info
        String user_info[] = db.get_users.by_email(con, email);
        if(!user_info[0].equalsIgnoreCase(""))
        {
            String user_id = user_info[0];
            //get incident
            int incident_count=0;
            ArrayList<String[]> my_incidents = db.get_incidents.my_open_incidents(con, user_id);
            incident_count = my_incidents.size();
            
            //get requests
            int request_count=0;
            ArrayList<String[]> my_requests = db.get_requests.my_open_requests(con, user_id);
            request_count = my_requests.size();
            
            array_content = new String[incident_count + request_count][8];
            array_to_sort = new long[incident_count + request_count][2];
            //add inc
            for(int a = 0; a < my_incidents.size(); a++)
            {
                String this_incident[] = my_incidents.get(a);
                array_to_sort[a][0] = a;
                array_content[a][0] = "incident.jsp?id=" + this_incident[0];
                array_content[a][1] = "INC" + this_incident[0];
                try
                {
                    array_content[a][2] = support.data_check.null_string(this_incident[15]);
                }
                catch(Exception e)
                {
                    array_content[a][2] = "";
                }
                try
                {
                    array_content[a][3] = "Category(" + support.data_check.null_string(this_incident[9]) + ") Subcategory(" + support.data_check.null_string(this_incident[10]) + ")";
                }
                catch(Exception e)
                {
                    array_content[a][3] = "Category(Not Defined) Subcategory(Not Defined)";
                }  
                try
                {
                    array_content[a][4] = support.data_check.null_string(this_incident[14]);
                }
                catch(Exception e)
                {
                    array_content[a][4] = "";
                }
                
                //do time to long
                try
                {
                    java.util.Date temp = timestamp_format.parse(this_incident[2]);
                    array_content[a][5] = String.valueOf(temp.getTime());
                    array_to_sort[a][1] = temp.getTime();
                }
                catch(Exception e)
                {
                    array_content[a][5] = String.valueOf(now.getTime());
                    array_to_sort[a][1] = now.getTime();
                } 
                try
                {
                    array_content[a][6] = support.data_check.null_string(this_incident[19]);
                }
                catch(Exception e)
                {
                    array_content[a][6] = "";
                }
                
            }
            //add req
            int count = incident_count;
            for(int a = 0; a < my_requests.size(); a++)
            {
                String request[] = my_requests.get(a);
                array_to_sort[count][0] = count;
                array_content[count][0] = "request.jsp?id=" + request[0];
                array_content[count][1] = "REQ" + request[0];
                try
                {
                    array_content[count][2] = support.data_check.null_string(request[29]);
                }
                catch(Exception e)
                {
                    array_content[count][2] = "";
                }                
                try
                {
                    array_content[count][3] = support.data_check.null_string(request[29]);
                }
                catch(Exception e)
                {
                    array_content[count][3] = "";
                }                
                try
                {
                    array_content[count][4] = support.data_check.null_string(request[22]);
                }
                catch(Exception e)
                {
                    array_content[count][4] = "";
                }
                
                //do time to long
                try
                {
                    java.util.Date temp = timestamp_format.parse(request[5]);
                    array_content[count][5] = String.valueOf(temp.getTime());
                    array_to_sort[count][1] = temp.getTime();
                }
                catch(Exception e)
                {
                    array_content[count][5] = String.valueOf(now.getTime());
                    array_to_sort[count][1] = now.getTime();
                }            
                array_content[count][6] = request[23];
                count++;
            }
            //sortbyColumn(array_to_sort, col - 1); 
            Arrays.sort(array_to_sort, (a, b) -> Long.compare(a[1], b[1]));    
            
            for(int b = array_to_sort.length; b > 0 ; b--)
            {
                int index = (int)array_to_sort[b - 1][0];
                String row[] = {array_content[index][0], array_content[index][1],array_content[index][2],array_content[index][3],array_content[index][4],array_content[index][5],array_content[index][6]};
                return_arraylist.add(row);
            }            
        }
        else
        {
            //nothing to return           
        }        
        return return_arraylist;
    }
    
}

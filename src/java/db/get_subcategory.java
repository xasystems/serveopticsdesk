/*
 * Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class get_subcategory 
{
    private static Logger logger = LogManager.getLogger();
    
    public static ArrayList<String[]> all(Connection con)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT * FROM subcategory");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = {"","","",""};
                temp[0] = rs.getString("id");
                temp[1] = rs.getString("category_id");
                temp[2] = rs.getString("name");
                temp[3] = rs.getString("active");
                return_arraylist.add(temp);
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_subcategory.all:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> all_for_category_id(Connection con, String category_id)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT * FROM subcategory WHERE category_id=?");
            stmt.setString(1, category_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = {"","","",""};
                temp[0] = rs.getString("id");
                temp[1] = rs.getString("category_id");
                temp[2] = rs.getString("name");
                temp[3] = rs.getString("active");
                return_arraylist.add(temp);
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_subcategory.all_for_category_id:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> all_active(Connection con)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT * FROM subcategory WHERE active='True'");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = {"","","",""};
                temp[0] = rs.getString("id");
                temp[1] = rs.getString("category_id");
                temp[2] = rs.getString("name");
                temp[3] = rs.getString("active");
                return_arraylist.add(temp);
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_subcategory.all_active:=" + exc);
	}
        return return_arraylist;
    }
    public static String name_from_subcategory_id(Connection con,int id)
    {
        String return_string = "";
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT * FROM subcategory WHERE id=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_string = rs.getString("name");
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_subcategory.name_from_subcategory_id:=" + exc);
	}
        return return_string;
    }
    public static ArrayList<String[]> for_category_id (Connection con, String category_id)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT * FROM subcategory WHERE category_id=?");
            stmt.setString(1, category_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = {"","","",""};
                temp[0] = rs.getString("id");
                temp[1] = rs.getString("category_id");
                temp[2] = rs.getString("name");
                temp[3] = rs.getString("active");
                return_arraylist.add(temp);
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_subcategory.for_category_id:=" + exc);
	}
        return return_arraylist;
    }
    
}

//Copyright 2021 XaSystems, Inc. , All rights reserved.

package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 *
 * @author server-xc6701
 */
public class get_sla 
{
    private static Logger logger = LogManager.getLogger();
    
    public static String[][] all_incident(Connection con) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM incident_sla", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][31];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("name");
                returnString[count][2] = rs.getString("description");
                returnString[count][3] = rs.getString("sla_type");
                returnString[count][4] = rs.getString("sla_text");
                returnString[count][5] = rs.getString("incident_priority");
                returnString[count][6] = rs.getString("target");
                returnString[count][7] = rs.getString("company");
                returnString[count][8] = rs.getString("department");
                returnString[count][9] = rs.getString("site");
                returnString[count][10] = rs.getString("location");                
                returnString[count][11] = rs.getString("start_condition");
                returnString[count][12] = rs.getString("pause_condition");
                returnString[count][13] = rs.getString("stop_condition");
                returnString[count][14] = rs.getString("report_name");
                returnString[count][15] = rs.getString("report_description");
                returnString[count][16] = rs.getString("active");
                returnString[count][17] = rs.getString("reportable");
                returnString[count][18] = rs.getString("report_header");
                returnString[count][19] = rs.getString("sla_timeframe");
                returnString[count][20] = rs.getString("schedule_id");
                returnString[count][21] = rs.getString("sla_success_threshold_operator");
                returnString[count][22] = rs.getString("sla_success_threshold_value");
                returnString[count][23] = rs.getString("sla_success_threshold_unit");
                returnString[count][24] = rs.getString("sla_incentive_exceptional");
                returnString[count][25] = rs.getString("sla_incentive_exceptional_dollars");
                returnString[count][26] = rs.getString("sla_incentive_acceptable");
                returnString[count][27] = rs.getString("sla_incentive_acceptable_dollars");
                returnString[count][28] = rs.getString("sla_incentive_unacceptable");
                returnString[count][29] = rs.getString("sla_incentive_unacceptable_dollars");
                returnString[count][30] = rs.getString("additional_parameters");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_sla.all_incident:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_sla.all_incident:=" + exc);
	}
        return returnString;
    } 
    public static String[][] all_active_incident(Connection con) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM incident_sla WHERE active=?", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, "1");
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][31];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("name");
                returnString[count][2] = rs.getString("description");
                returnString[count][3] = rs.getString("sla_type");
                returnString[count][4] = rs.getString("sla_text");
                returnString[count][5] = rs.getString("incident_priority");
                returnString[count][6] = rs.getString("target");
                returnString[count][7] = rs.getString("company");
                returnString[count][8] = rs.getString("department");
                returnString[count][9] = rs.getString("site");
                returnString[count][10] = rs.getString("location");                
                returnString[count][11] = rs.getString("start_condition");
                returnString[count][12] = rs.getString("pause_condition");
                returnString[count][13] = rs.getString("stop_condition");
                returnString[count][14] = rs.getString("report_name");
                returnString[count][15] = rs.getString("report_description");
                returnString[count][16] = rs.getString("active");
                returnString[count][17] = rs.getString("reportable");
                returnString[count][18] = rs.getString("report_header");
                returnString[count][19] = rs.getString("sla_timeframe");
                returnString[count][20] = rs.getString("schedule_id");
                returnString[count][21] = rs.getString("sla_success_threshold_operator");
                returnString[count][22] = rs.getString("sla_success_threshold_value");
                returnString[count][23] = rs.getString("sla_success_threshold_unit");
                returnString[count][24] = rs.getString("sla_incentive_exceptional");
                returnString[count][25] = rs.getString("sla_incentive_exceptional_dollars");
                returnString[count][26] = rs.getString("sla_incentive_acceptable");
                returnString[count][27] = rs.getString("sla_incentive_acceptable_dollars");
                returnString[count][28] = rs.getString("sla_incentive_unacceptable");
                returnString[count][29] = rs.getString("sla_incentive_unacceptable_dollars");
                returnString[count][30] = rs.getString("additional_parameters");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_sla.all_active_incident:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_sla.all_active_incident:=" + exc);
	}
        return returnString;
    } 
    public static String[] incident_by_id(Connection con, String id) throws IOException, SQLException
    {
        String returnString[] = new String[0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM incident_sla WHERE id=?");
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next())
            {
                returnString = new String[31];
                returnString[0] = rs.getString("id");
                returnString[1] = rs.getString("name");
                returnString[2] = rs.getString("description");
                returnString[3] = rs.getString("sla_type");
                returnString[4] = rs.getString("sla_text");
                returnString[5] = rs.getString("incident_priority");
                returnString[6] = rs.getString("target");
                returnString[7] = rs.getString("company");
                returnString[8] = rs.getString("department");
                returnString[9] = rs.getString("site");
                returnString[10] = rs.getString("location");                
                returnString[11] = rs.getString("start_condition");
                returnString[12] = rs.getString("pause_condition");
                returnString[13] = rs.getString("stop_condition");
                returnString[14] = rs.getString("report_name");
                returnString[15] = rs.getString("report_description");
                returnString[16] = rs.getString("active");
                returnString[17] = rs.getString("reportable");
                returnString[18] = rs.getString("report_header");
                returnString[19] = rs.getString("sla_timeframe");
                returnString[20] = rs.getString("schedule_id");
                returnString[21] = rs.getString("sla_success_threshold_operator");
                returnString[22] = rs.getString("sla_success_threshold_value");
                returnString[23] = rs.getString("sla_success_threshold_unit");
                returnString[24] = rs.getString("sla_incentive_exceptional");
                returnString[25] = rs.getString("sla_incentive_exceptional_dollars");
                returnString[26] = rs.getString("sla_incentive_acceptable");
                returnString[27] = rs.getString("sla_incentive_acceptable_dollars");
                returnString[28] = rs.getString("sla_incentive_unacceptable");
                returnString[29] = rs.getString("sla_incentive_unacceptable_dollars");
                returnString[30] = rs.getString("additional_parameters");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_sla.incident_by_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_sla.incident_by_id:=" + exc);
	}
        return returnString;
    } 
    public static String[][] all_request(Connection con) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM request_sla", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][28];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("name");
                returnString[count][2] = rs.getString("description");
                returnString[count][3] = rs.getString("sla_type");
                returnString[count][4] = rs.getString("sla_text");
                returnString[count][5] = rs.getString("request_priority");
                returnString[count][6] = rs.getString("target");
                returnString[count][7] = rs.getString("start_condition");
                returnString[count][8] = rs.getString("pause_condition");
                returnString[count][9] = rs.getString("stop_condition");
                returnString[count][10] = rs.getString("report_name");
                returnString[count][11] = rs.getString("report_description");
                returnString[count][12] = rs.getString("active");
                returnString[count][13] = rs.getString("reportable");
                returnString[count][14] = rs.getString("report_header");
                returnString[count][15] = rs.getString("sla_timeframe");
                returnString[count][16] = rs.getString("schedule_id");
                returnString[count][17] = rs.getString("sla_success_threshold_operator");
                returnString[count][18] = rs.getString("sla_success_threshold_value");
                returnString[count][19] = rs.getString("sla_success_threshold_unit");
                returnString[count][20] = rs.getString("sla_incentive_exceptional");
                returnString[count][21] = rs.getString("sla_incentive_exceptional_dollars");
                returnString[count][22] = rs.getString("sla_incentive_acceptable");
                returnString[count][23] = rs.getString("sla_incentive_acceptable_dollars");
                returnString[count][24] = rs.getString("sla_incentive_unacceptable");
                returnString[count][25] = rs.getString("sla_incentive_unacceptable_dollars");
                returnString[count][26] = rs.getString("additional_parameters");
                returnString[count][27] = rs.getString("service_catalog_item_id");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_sla.all_request:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_sla.all_request:=" + exc);
	}
        return returnString;
    } 
    public static String[][] all_active_request(Connection con) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM request_sla where active = 1", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][28];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("name");
                returnString[count][2] = rs.getString("description");
                returnString[count][3] = rs.getString("sla_type");
                returnString[count][4] = rs.getString("sla_text");
                returnString[count][5] = rs.getString("request_priority");
                returnString[count][6] = rs.getString("target");
                returnString[count][7] = rs.getString("start_condition");
                returnString[count][8] = rs.getString("pause_condition");
                returnString[count][9] = rs.getString("stop_condition");
                returnString[count][10] = rs.getString("report_name");
                returnString[count][11] = rs.getString("report_description");
                returnString[count][12] = rs.getString("active");
                returnString[count][13] = rs.getString("reportable");
                returnString[count][14] = rs.getString("report_header");
                returnString[count][15] = rs.getString("sla_timeframe");
                returnString[count][16] = rs.getString("schedule_id");
                returnString[count][17] = rs.getString("sla_success_threshold_operator");
                returnString[count][18] = rs.getString("sla_success_threshold_value");
                returnString[count][19] = rs.getString("sla_success_threshold_unit");
                returnString[count][20] = rs.getString("sla_incentive_exceptional");
                returnString[count][21] = rs.getString("sla_incentive_exceptional_dollars");
                returnString[count][22] = rs.getString("sla_incentive_acceptable");
                returnString[count][23] = rs.getString("sla_incentive_acceptable_dollars");
                returnString[count][24] = rs.getString("sla_incentive_unacceptable");
                returnString[count][25] = rs.getString("sla_incentive_unacceptable_dollars");
                returnString[count][26] = rs.getString("additional_parameters");
                returnString[count][27] = rs.getString("service_catalog_item_id");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_sla.all_request:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_sla.all_request:=" + exc);
	}
        return returnString;
    } 
    public static String[][] request_by_filters(Connection con, List<Map<String, String>> filters) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        List<String> wheres = new ArrayList<>();
        PreparedStatement stmt;
        String query;        
        String where = "";

        try 
        {
            // Create a Statement Object
            for (int i = 0; i < filters.size(); i++)
            {
              String filter_key = filters.get(i).get("key");
              wheres.add(filter_key + " = ? ");
            }
            
            if (wheres.size() > 0)
            {
                where = " WHERE " + String.join(" AND ", wheres);
            }
            // Create a Statement Object
            query = "SELECT * FROM request_sla" + where;

            stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            for (int i = 0; i < filters.size(); i++)
            {
              String filter_value = filters.get(i).get("value");
              stmt.setString(i+1, filter_value);
            }
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][27];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("name");
                returnString[count][2] = rs.getString("description");
                returnString[count][3] = rs.getString("sla_type");
                returnString[count][4] = rs.getString("sla_text");
                returnString[count][5] = rs.getString("request_priority");
                returnString[count][6] = rs.getString("target");
                returnString[count][7] = rs.getString("start_condition");
                returnString[count][8] = rs.getString("pause_condition");
                returnString[count][9] = rs.getString("stop_condition");
                returnString[count][10] = rs.getString("report_name");
                returnString[count][11] = rs.getString("report_description");
                returnString[count][12] = rs.getString("active");
                returnString[count][13] = rs.getString("reportable");
                returnString[count][14] = rs.getString("report_header");
                returnString[count][15] = rs.getString("sla_timeframe");
                returnString[count][16] = rs.getString("schedule_id");
                returnString[count][17] = rs.getString("sla_success_threshold_operator");
                returnString[count][18] = rs.getString("sla_success_threshold_value");
                returnString[count][19] = rs.getString("sla_success_threshold_unit");
                returnString[count][20] = rs.getString("sla_incentive_exceptional");
                returnString[count][21] = rs.getString("sla_incentive_exceptional_dollars");
                returnString[count][22] = rs.getString("sla_incentive_acceptable");
                returnString[count][23] = rs.getString("sla_incentive_acceptable_dollars");
                returnString[count][24] = rs.getString("sla_incentive_unacceptable");
                returnString[count][25] = rs.getString("sla_incentive_unacceptable_dollars");
                returnString[count][26] = rs.getString("additional_parameters");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_sla.all_request:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_sla.all_request:=" + exc);
	}
        return returnString;
    } 
    public static String[] request_by_id(Connection con, String id) throws IOException, SQLException
    {
        String returnString[] = new String[0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM request_sla WHERE id=?", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            rs.beforeFirst();
            while(rs.next())
            {
                returnString = new String[29];
                returnString[0] = rs.getString("id");
                returnString[1] = rs.getString("name");
                returnString[2] = rs.getString("description");
                returnString[3] = rs.getString("sla_type");
                returnString[4] = rs.getString("sla_text");
                returnString[5] = rs.getString("request_priority");
                returnString[6] = rs.getString("target");
                returnString[7] = rs.getString("start_condition");
                returnString[8] = rs.getString("pause_condition");
                returnString[9] = rs.getString("stop_condition");
                returnString[10] = rs.getString("stop_state");
                returnString[11] = rs.getString("report_name");
                returnString[12] = rs.getString("report_description");
                returnString[13] = rs.getString("active");
                returnString[14] = rs.getString("reportable");
                returnString[15] = rs.getString("report_header");
                returnString[16] = rs.getString("sla_timeframe");
                returnString[17] = rs.getString("schedule_id");
                returnString[18] = rs.getString("sla_success_threshold_operator");
                returnString[19] = rs.getString("sla_success_threshold_value");
                returnString[20] = rs.getString("sla_success_threshold_unit");
                returnString[21] = rs.getString("sla_incentive_exceptional");
                returnString[22] = rs.getString("sla_incentive_exceptional_dollars");
                returnString[23] = rs.getString("sla_incentive_acceptable");
                returnString[24] = rs.getString("sla_incentive_acceptable_dollars");
                returnString[25] = rs.getString("sla_incentive_unacceptable");
                returnString[26] = rs.getString("sla_incentive_unacceptable_dollars");
                returnString[27] = rs.getString("additional_parameters");
                returnString[28] = rs.getString("service_catalog_item_id");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_sla.request_by_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_sla.request_by_id:=" + exc);
	}
        return returnString;
    } 
    public static String[][] all_contact(Connection con) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM contact_sla", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][28];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("name");
                returnString[count][2] = rs.getString("description");
                returnString[count][3] = rs.getString("sla_text");
                returnString[count][4] = rs.getString("sla_type");
                returnString[count][5] = rs.getString("target");
                returnString[count][6] = rs.getString("company");
                returnString[count][7] = rs.getString("department");
                returnString[count][8] = rs.getString("site");
                returnString[count][9] = rs.getString("location");
                returnString[count][10] = rs.getString("active");
                returnString[count][11] = rs.getString("reportable");
                returnString[count][12] = rs.getString("schedule_id");
                returnString[count][13] = rs.getString("sla_timeframe");                
                returnString[count][14] = rs.getString("abandoned_threshold");
                returnString[count][15] = rs.getString("sla_success_threshold_operator");
                returnString[count][16] = rs.getString("sla_success_threshold_value");
                returnString[count][17] = rs.getString("sla_success_threshold_unit");
                returnString[count][18] = rs.getString("sla_incentive_exceptional");
                returnString[count][19] = rs.getString("sla_incentive_exceptional_dollars");
                returnString[count][20] = rs.getString("sla_incentive_acceptable");
                returnString[count][21] = rs.getString("sla_incentive_acceptable_dollars");
                returnString[count][22] = rs.getString("sla_incentive_unacceptable");
                returnString[count][23] = rs.getString("sla_incentive_unacceptable_dollars");
                returnString[count][24] = rs.getString("additional_parameters");
                returnString[count][25] = rs.getString("report_name");
                returnString[count][26] = rs.getString("report_description");
                returnString[count][27] = rs.getString("report_header");   
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_sla.all_contact:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_sla.all_contact:=" + exc);
	}
        return returnString;
    } 
    public static String[][] contact_by_filters(Connection con, List<Map<String, String>> filters) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        List<String> wheres = new ArrayList<>();
        PreparedStatement stmt;
        String query;        
        String where = "";
        try 
        {
            for (int i = 0; i < filters.size(); i++)
            {
              String filter_key = filters.get(i).get("key");
              wheres.add(filter_key + " = ? ");
            }
            
            if (wheres.size() > 0)
            {
                where = " WHERE " + String.join(" AND ", wheres);
            }
            // Create a Statement Object
            query = "SELECT * FROM contact_sla" + where;
            stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            for (int i = 0; i < filters.size(); i++)
            {
              String filter_value = filters.get(i).get("value");
              stmt.setString(i+1, filter_value);
            }

            ResultSet rs = stmt.executeQuery();

            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][28];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("name");
                returnString[count][2] = rs.getString("description");
                returnString[count][3] = rs.getString("sla_text");
                returnString[count][4] = rs.getString("sla_type");
                returnString[count][5] = rs.getString("target");
                returnString[count][6] = rs.getString("company");
                returnString[count][7] = rs.getString("department");
                returnString[count][8] = rs.getString("site");
                returnString[count][9] = rs.getString("location");
                returnString[count][10] = rs.getString("active");
                returnString[count][11] = rs.getString("reportable");
                returnString[count][12] = rs.getString("schedule_id");
                returnString[count][13] = rs.getString("sla_timeframe");                
                returnString[count][14] = rs.getString("abandoned_threshold");
                returnString[count][15] = rs.getString("sla_success_threshold_operator");
                returnString[count][16] = rs.getString("sla_success_threshold_value");
                returnString[count][17] = rs.getString("sla_success_threshold_unit");
                returnString[count][18] = rs.getString("sla_incentive_exceptional");
                returnString[count][19] = rs.getString("sla_incentive_exceptional_dollars");
                returnString[count][20] = rs.getString("sla_incentive_acceptable");
                returnString[count][21] = rs.getString("sla_incentive_acceptable_dollars");
                returnString[count][22] = rs.getString("sla_incentive_unacceptable");
                returnString[count][23] = rs.getString("sla_incentive_unacceptable_dollars");
                returnString[count][24] = rs.getString("additional_parameters");
                returnString[count][25] = rs.getString("report_name");
                returnString[count][26] = rs.getString("report_description");
                returnString[count][27] = rs.getString("report_header");   
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_sla.all_contact:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_sla.all_contact:=" + exc);
	}
        return returnString;
    } 
    public static String[][] all_reportable_contact(Connection con) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM contact_sla WHERE active='1' AND reportable='1'", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][28];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("name");
                returnString[count][2] = rs.getString("description");
                returnString[count][3] = rs.getString("sla_text");
                returnString[count][4] = rs.getString("sla_type");
                returnString[count][5] = rs.getString("target");
                returnString[count][6] = rs.getString("company");
                returnString[count][7] = rs.getString("department");
                returnString[count][8] = rs.getString("site");
                returnString[count][9] = rs.getString("location");
                returnString[count][10] = rs.getString("active");
                returnString[count][11] = rs.getString("reportable");
                returnString[count][12] = rs.getString("schedule_id");
                returnString[count][13] = rs.getString("sla_timeframe");                
                returnString[count][14] = rs.getString("abandoned_threshold");
                returnString[count][15] = rs.getString("sla_success_threshold_operator");
                returnString[count][16] = rs.getString("sla_success_threshold_value");
                returnString[count][17] = rs.getString("sla_success_threshold_unit");
                returnString[count][18] = rs.getString("sla_incentive_exceptional");
                returnString[count][19] = rs.getString("sla_incentive_exceptional_dollars");
                returnString[count][20] = rs.getString("sla_incentive_acceptable");
                returnString[count][21] = rs.getString("sla_incentive_acceptable_dollars");
                returnString[count][22] = rs.getString("sla_incentive_unacceptable");
                returnString[count][23] = rs.getString("sla_incentive_unacceptable_dollars");
                returnString[count][24] = rs.getString("additional_parameters");
                returnString[count][25] = rs.getString("report_name");
                returnString[count][26] = rs.getString("report_description");
                returnString[count][27] = rs.getString("report_header");   
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_sla.all_reportable_contact:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_sla.all_reportable_contact:=" + exc);
	}
        return returnString;
    } 
    public static String[][] all_nonreportable_contact(Connection con) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM contact_sla WHERE active='1' AND reportable='0'", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][28];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("name");
                returnString[count][2] = rs.getString("description");
                returnString[count][3] = rs.getString("sla_text");
                returnString[count][4] = rs.getString("sla_type");
                returnString[count][5] = rs.getString("target");
                returnString[count][6] = rs.getString("company");
                returnString[count][7] = rs.getString("department");
                returnString[count][8] = rs.getString("site");
                returnString[count][9] = rs.getString("location");
                returnString[count][10] = rs.getString("active");
                returnString[count][11] = rs.getString("reportable");
                returnString[count][12] = rs.getString("schedule_id");
                returnString[count][13] = rs.getString("sla_timeframe");                
                returnString[count][14] = rs.getString("abandoned_threshold");
                returnString[count][15] = rs.getString("sla_success_threshold_operator");
                returnString[count][16] = rs.getString("sla_success_threshold_value");
                returnString[count][17] = rs.getString("sla_success_threshold_unit");
                returnString[count][18] = rs.getString("sla_incentive_exceptional");
                returnString[count][19] = rs.getString("sla_incentive_exceptional_dollars");
                returnString[count][20] = rs.getString("sla_incentive_acceptable");
                returnString[count][21] = rs.getString("sla_incentive_acceptable_dollars");
                returnString[count][22] = rs.getString("sla_incentive_unacceptable");
                returnString[count][23] = rs.getString("sla_incentive_unacceptable_dollars");
                returnString[count][24] = rs.getString("additional_parameters");
                returnString[count][25] = rs.getString("report_name");
                returnString[count][26] = rs.getString("report_description");
                returnString[count][27] = rs.getString("report_header");   
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_sla.all_reportable_contact:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_sla.all_reportable_contact:=" + exc);
	}
        return returnString;
    } 
    public static String[] contact_by_id(Connection con, String id) throws IOException, SQLException
    {
        String returnString[] = new String[0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM contact_sla WHERE id=?", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            rs.beforeFirst();
            while(rs.next())
            {
                returnString = new String[28];
                returnString[0] = rs.getString("id");
                returnString[1] = rs.getString("name");
                returnString[2] = rs.getString("description");
                returnString[3] = rs.getString("sla_text");
                returnString[4] = rs.getString("sla_type");
                returnString[5] = rs.getString("target");
                returnString[6] = rs.getString("company");
                returnString[7] = rs.getString("department");
                returnString[8] = rs.getString("site");
                returnString[9] = rs.getString("location");
                returnString[10] = rs.getString("active");
                returnString[11] = rs.getString("reportable");
                returnString[12] = rs.getString("schedule_id");
                returnString[13] = rs.getString("sla_timeframe");                
                returnString[14] = rs.getString("abandoned_threshold");
                returnString[15] = rs.getString("sla_success_threshold_operator");
                returnString[16] = rs.getString("sla_success_threshold_value");
                returnString[17] = rs.getString("sla_success_threshold_unit");
                returnString[18] = rs.getString("sla_incentive_exceptional");
                returnString[19] = rs.getString("sla_incentive_exceptional_dollars");
                returnString[20] = rs.getString("sla_incentive_acceptable");
                returnString[21] = rs.getString("sla_incentive_acceptable_dollars");
                returnString[22] = rs.getString("sla_incentive_unacceptable");
                returnString[23] = rs.getString("sla_incentive_unacceptable_dollars");
                returnString[24] = rs.getString("additional_parameters");
                returnString[25] = rs.getString("report_name");
                returnString[26] = rs.getString("report_description");
                returnString[27] = rs.getString("report_header");        
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_sla.contact_by_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_sla.contact_by_id:=" + exc);
	}
        return returnString;
    } 
    public static String[][] incident_filter_by(Connection con, String filter_by_type, String filter_by_name) throws IOException, SQLException
    {
        String sql = "";
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            if(filter_by_type.equalsIgnoreCase("all"))
            {
                sql = "SELECT * FROM incident_sla";
            }
            else if (filter_by_type.equalsIgnoreCase("company"))
            {
                sql = "SELECT * FROM incident_sla WHERE company=?";
            }
            else if (filter_by_type.equalsIgnoreCase("department"))
            {
                sql = "SELECT * FROM incident_sla WHERE department=?";
            }
            else if (filter_by_type.equalsIgnoreCase("site"))
            {
                sql = "SELECT * FROM incident_sla WHERE site=?";
            }
            else if (filter_by_type.equalsIgnoreCase("location"))
            {
                sql = "SELECT * FROM incident_sla WHERE location=?";
            }
              
            // Create a Statement Object
            stmt = con.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            if(!filter_by_type.equalsIgnoreCase("all"))
            {
                stmt.setString(1,filter_by_name);
            }            
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][31];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("name");
                returnString[count][2] = rs.getString("description");
                returnString[count][3] = rs.getString("sla_type");
                returnString[count][4] = rs.getString("sla_text");
                returnString[count][5] = rs.getString("incident_priority");
                returnString[count][6] = rs.getString("target");
                returnString[count][7] = rs.getString("company");
                returnString[count][8] = rs.getString("department");
                returnString[count][9] = rs.getString("site");
                returnString[count][10] = rs.getString("location");                
                returnString[count][11] = rs.getString("start_condition");
                returnString[count][12] = rs.getString("pause_condition");
                returnString[count][13] = rs.getString("stop_condition");
                returnString[count][14] = rs.getString("report_name");
                returnString[count][15] = rs.getString("report_description");
                returnString[count][16] = rs.getString("active");
                returnString[count][17] = rs.getString("reportable");
                returnString[count][18] = rs.getString("report_header");
                returnString[count][19] = rs.getString("sla_timeframe");
                returnString[count][20] = rs.getString("schedule_id");
                returnString[count][21] = rs.getString("sla_success_threshold_operator");
                returnString[count][22] = rs.getString("sla_success_threshold_value");
                returnString[count][23] = rs.getString("sla_success_threshold_unit");
                returnString[count][24] = rs.getString("sla_incentive_exceptional");
                returnString[count][25] = rs.getString("sla_incentive_exceptional_dollars");
                returnString[count][26] = rs.getString("sla_incentive_acceptable");
                returnString[count][27] = rs.getString("sla_incentive_acceptable_dollars");
                returnString[count][28] = rs.getString("sla_incentive_unacceptable");
                returnString[count][29] = rs.getString("sla_incentive_unacceptable_dollars");
                returnString[count][30] = rs.getString("additional_parameters");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_sla.incident_filter_by:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_sla.incident_filter_by:=" + exc);
	}
        return returnString;
    } 
    public static String[][] incident_by_filters(Connection con, List<Map<String, String>> filters) throws IOException, SQLException
    {
        String sql = "";
        String returnString[][] = new String[0][0];
        List<String> wheres = new ArrayList<>();
        PreparedStatement stmt;
        String query;        
        String where = "";
        try 
        {
            for (int i = 0; i < filters.size(); i++)
            {
              String filter_key = filters.get(i).get("key");
              wheres.add(filter_key + " = ? ");
            }
            
            if (wheres.size() > 0)
            {
                where = " WHERE " + String.join(" AND ", wheres);
            }
            // Create a Statement Object
            query = "SELECT * FROM incident_sla" + where;
            stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            for (int i = 0; i < filters.size(); i++)
            {
              String filter_value = filters.get(i).get("value");
              stmt.setString(i+1, filter_value);
            }

            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][31];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("name");
                returnString[count][2] = rs.getString("description");
                returnString[count][3] = rs.getString("sla_type");
                returnString[count][4] = rs.getString("sla_text");
                returnString[count][5] = rs.getString("incident_priority");
                returnString[count][6] = rs.getString("target");
                returnString[count][7] = rs.getString("company");
                returnString[count][8] = rs.getString("department");
                returnString[count][9] = rs.getString("site");
                returnString[count][10] = rs.getString("location");                
                returnString[count][11] = rs.getString("start_condition");
                returnString[count][12] = rs.getString("pause_condition");
                returnString[count][13] = rs.getString("stop_condition");
                returnString[count][14] = rs.getString("report_name");
                returnString[count][15] = rs.getString("report_description");
                returnString[count][16] = rs.getString("active");
                returnString[count][17] = rs.getString("reportable");
                returnString[count][18] = rs.getString("report_header");
                returnString[count][19] = rs.getString("sla_timeframe");
                returnString[count][20] = rs.getString("schedule_id");
                returnString[count][21] = rs.getString("sla_success_threshold_operator");
                returnString[count][22] = rs.getString("sla_success_threshold_value");
                returnString[count][23] = rs.getString("sla_success_threshold_unit");
                returnString[count][24] = rs.getString("sla_incentive_exceptional");
                returnString[count][25] = rs.getString("sla_incentive_exceptional_dollars");
                returnString[count][26] = rs.getString("sla_incentive_acceptable");
                returnString[count][27] = rs.getString("sla_incentive_acceptable_dollars");
                returnString[count][28] = rs.getString("sla_incentive_unacceptable");
                returnString[count][29] = rs.getString("sla_incentive_unacceptable_dollars");
                returnString[count][30] = rs.getString("additional_parameters");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_sla.incident_filter_by:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_sla.incident_filter_by:=" + exc);
	}
        return returnString;
    } 
    public static String[][] all_reportable_incident(Connection con) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM incident_sla WHERE active='1' AND reportable='1'", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][31];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("name");
                returnString[count][2] = rs.getString("description");
                returnString[count][3] = rs.getString("sla_type");
                returnString[count][4] = rs.getString("sla_text");
                returnString[count][5] = rs.getString("incident_priority");
                returnString[count][6] = rs.getString("target");
                returnString[count][7] = rs.getString("company");
                returnString[count][8] = rs.getString("department");
                returnString[count][9] = rs.getString("site");
                returnString[count][10] = rs.getString("location");                
                returnString[count][11] = rs.getString("start_condition");
                returnString[count][12] = rs.getString("pause_condition");
                returnString[count][13] = rs.getString("stop_condition");
                returnString[count][14] = rs.getString("report_name");
                returnString[count][15] = rs.getString("report_description");
                returnString[count][16] = rs.getString("active");
                returnString[count][17] = rs.getString("reportable");
                returnString[count][18] = rs.getString("report_header");
                returnString[count][19] = rs.getString("sla_timeframe");
                returnString[count][20] = rs.getString("schedule_id");
                returnString[count][21] = rs.getString("sla_success_threshold_operator");
                returnString[count][22] = rs.getString("sla_success_threshold_value");
                returnString[count][23] = rs.getString("sla_success_threshold_unit");
                returnString[count][24] = rs.getString("sla_incentive_exceptional");
                returnString[count][25] = rs.getString("sla_incentive_exceptional_dollars");
                returnString[count][26] = rs.getString("sla_incentive_acceptable");
                returnString[count][27] = rs.getString("sla_incentive_acceptable_dollars");
                returnString[count][28] = rs.getString("sla_incentive_unacceptable");
                returnString[count][29] = rs.getString("sla_incentive_unacceptable_dollars");
                returnString[count][30] = rs.getString("additional_parameters");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_sla.all_reportable_incident:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_sla.all_reportable_incident:=" + exc);
	}
        return returnString;
    } 
    public static String[][] all_nonreportable_incident(Connection con) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM incident_sla WHERE active='1' AND reportable='0'", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][31];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("name");
                returnString[count][2] = rs.getString("description");
                returnString[count][3] = rs.getString("sla_type");
                returnString[count][4] = rs.getString("sla_text");
                returnString[count][5] = rs.getString("incident_priority");
                returnString[count][6] = rs.getString("target");
                returnString[count][7] = rs.getString("company");
                returnString[count][8] = rs.getString("department");
                returnString[count][9] = rs.getString("site");
                returnString[count][10] = rs.getString("location");                
                returnString[count][11] = rs.getString("start_condition");
                returnString[count][12] = rs.getString("pause_condition");
                returnString[count][13] = rs.getString("stop_condition");
                returnString[count][14] = rs.getString("report_name");
                returnString[count][15] = rs.getString("report_description");
                returnString[count][16] = rs.getString("active");
                returnString[count][17] = rs.getString("reportable");
                returnString[count][18] = rs.getString("report_header");
                returnString[count][19] = rs.getString("sla_timeframe");
                returnString[count][20] = rs.getString("schedule_id");
                returnString[count][21] = rs.getString("sla_success_threshold_operator");
                returnString[count][22] = rs.getString("sla_success_threshold_value");
                returnString[count][23] = rs.getString("sla_success_threshold_unit");
                returnString[count][24] = rs.getString("sla_incentive_exceptional");
                returnString[count][25] = rs.getString("sla_incentive_exceptional_dollars");
                returnString[count][26] = rs.getString("sla_incentive_acceptable");
                returnString[count][27] = rs.getString("sla_incentive_acceptable_dollars");
                returnString[count][28] = rs.getString("sla_incentive_unacceptable");
                returnString[count][29] = rs.getString("sla_incentive_unacceptable_dollars");
                returnString[count][30] = rs.getString("additional_parameters");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_sla.all_nonreportable_incident:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_sla.all_nonreportable_incident:=" + exc);
	}
        return returnString;
    } 
    public static String[][] all_reportable_request(Connection con) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM request_sla WHERE active='1' AND reportable='1'", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][32];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("name");
                returnString[count][2] = rs.getString("description");
                returnString[count][3] = rs.getString("sla_type");
                returnString[count][4] = rs.getString("sla_text");
                returnString[count][5] = rs.getString("request_priority");
                returnString[count][6] = rs.getString("target");
                returnString[count][7] = rs.getString("company");
                returnString[count][8] = rs.getString("department");
                returnString[count][9] = rs.getString("site");
                returnString[count][10] = rs.getString("location");                
                returnString[count][11] = rs.getString("start_condition");
                returnString[count][12] = rs.getString("pause_condition");
                returnString[count][13] = rs.getString("stop_condition");
                returnString[count][14] = rs.getString("report_name");
                returnString[count][15] = rs.getString("report_description");
                returnString[count][16] = rs.getString("active");
                returnString[count][17] = rs.getString("reportable");
                returnString[count][18] = rs.getString("report_header");
                returnString[count][19] = rs.getString("sla_timeframe");
                returnString[count][20] = rs.getString("schedule_id");
                returnString[count][21] = rs.getString("sla_success_threshold_operator");
                returnString[count][22] = rs.getString("sla_success_threshold_value");
                returnString[count][23] = rs.getString("sla_success_threshold_unit");
                returnString[count][24] = rs.getString("sla_incentive_exceptional");
                returnString[count][25] = rs.getString("sla_incentive_exceptional_dollars");
                returnString[count][26] = rs.getString("sla_incentive_acceptable");
                returnString[count][27] = rs.getString("sla_incentive_acceptable_dollars");
                returnString[count][28] = rs.getString("sla_incentive_unacceptable");
                returnString[count][29] = rs.getString("sla_incentive_unacceptable_dollars");
                returnString[count][30] = rs.getString("additional_parameters");
                returnString[count][31] = rs.getString("service_catalog_item_id");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_sla.all_reportable_request:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_sla.all_reportable_request:=" + exc);
	}
        return returnString;
    } 
    public static String[][] all_nonreportable_request(Connection con) throws IOException, SQLException
    {
        String returnString[][] = new String[0][0];
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM request_sla WHERE active='1' AND reportable='0'", ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                count++;
            }
            returnString = new String[count][31];
            count = 0;
            rs.beforeFirst();
            while(rs.next())
            {
                returnString[count][0] = rs.getString("id");
                returnString[count][1] = rs.getString("name");
                returnString[count][2] = rs.getString("description");
                returnString[count][3] = rs.getString("sla_type");
                returnString[count][4] = rs.getString("sla_text");
                returnString[count][5] = rs.getString("request_priority");
                returnString[count][6] = rs.getString("target");
                returnString[count][7] = rs.getString("company");
                returnString[count][8] = rs.getString("department");
                returnString[count][9] = rs.getString("site");
                returnString[count][10] = rs.getString("location");                
                returnString[count][11] = rs.getString("start_condition");
                returnString[count][12] = rs.getString("pause_condition");
                returnString[count][13] = rs.getString("stop_condition");
                returnString[count][14] = rs.getString("report_name");
                returnString[count][15] = rs.getString("report_description");
                returnString[count][16] = rs.getString("active");
                returnString[count][17] = rs.getString("reportable");
                returnString[count][18] = rs.getString("report_header");
                returnString[count][19] = rs.getString("sla_timeframe");
                returnString[count][20] = rs.getString("schedule_id");
                returnString[count][21] = rs.getString("sla_success_threshold_operator");
                returnString[count][22] = rs.getString("sla_success_threshold_value");
                returnString[count][23] = rs.getString("sla_success_threshold_unit");
                returnString[count][24] = rs.getString("sla_incentive_exceptional");
                returnString[count][25] = rs.getString("sla_incentive_exceptional_dollars");
                returnString[count][26] = rs.getString("sla_incentive_acceptable");
                returnString[count][27] = rs.getString("sla_incentive_acceptable_dollars");
                returnString[count][28] = rs.getString("sla_incentive_unacceptable");
                returnString[count][29] = rs.getString("sla_incentive_unacceptable_dollars");
                returnString[count][30] = rs.getString("additional_parameters");
                count++;
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_sla.all_nonreportable_request:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_sla.all_nonreportable_request:=" + exc);
	}
        return returnString;
    } 
}

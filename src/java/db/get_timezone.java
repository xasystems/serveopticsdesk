//Copyright 2021 XaSystems, Inc. , All rights reserved.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class get_timezone 
{
    private static Logger logger = LogManager.getLogger();
    public static ArrayList<String[]> all(Connection con) throws IOException, SQLException
    {
        ArrayList<String[]>return_arraylist= new ArrayList();
        //id=0 name=1 description=2 created=3 created_by_id=4 administration=5 manager=6 financial=7 incident=8 request=9 contact=10 cx=11 survey=12 sla=13 project=14 job=15 
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM tz ORDER BY id");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = new String[3];
                temp[0] = rs.getString("id");
                temp[1] = rs.getString("name");
                temp[2] = rs.getString("time");
                return_arraylist.add(temp);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_timezone.all:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_timezone.all:=" + exc);
	}
        return return_arraylist;
    }   
}

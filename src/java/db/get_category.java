/*
 * Copyright 2019 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class get_category 
{
    private static Logger logger = LogManager.getLogger();
    
    public static ArrayList<String[]> all(Connection con)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT * FROM category ORDER BY name");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = {"","",""};
                temp[0] = rs.getString("id");
                temp[1] = rs.getString("name");
                temp[2] = rs.getString("active");
                return_arraylist.add(temp);
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_category.all:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> all_active(Connection con)
    {
        ArrayList <String[]> return_arraylist = new ArrayList();
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT * FROM category WHERE active='True' ORDER BY name");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String temp[] = {"","",""};
                temp[0] = rs.getString("id");
                temp[1] = rs.getString("name");
                temp[2] = rs.getString("active");                
                return_arraylist.add(temp);
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_category.all_active:=" + exc);
	}
        return return_arraylist;
    }
    public static String[] info_by_category_id(Connection con, String category_id)
    {
        String[] return_array = {"","",""};
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT * FROM category WHERE id=?");
            stmt.setString(1,category_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                
                return_array[0] = rs.getString("id");
                return_array[1] = rs.getString("name");
                return_array[2] = rs.getString("active");  
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_category.info_by_category_id:=" + exc);
	}
        return return_array;
    }
    public static String from_category_id(Connection con, int category_id)
    {
        String return_string = "";
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("SELECT * FROM category WHERE id=?");
            stmt.setInt(1, category_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_string = rs.getString("name");
            }
            stmt.close();
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_category.from_category_id:=" + exc);
	}
        return return_string;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author rcampbell
 */
public class get_pending_email 
{
    private static Logger logger = LogManager.getLogger();
    
    public static ArrayList<String[]> all(Connection con)
    {
        ArrayList<String[]> return_arraylist = new ArrayList();
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss"); 
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM pending_email");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = {"","","","","",""};
                this_record[0] = rs.getString("id");
                this_record[1] = rs.getString("email");
                this_record[2] = rs.getString("subject");
                this_record[3] = rs.getString("body");
                try{this_record[4] = timestamp_format.format(rs.getTimestamp("timestamp"));}catch (Exception e){}
                this_record[5] = rs.getString("status");
                return_arraylist.add(this_record);
            }
        }
        catch(Exception e)
        {
            logger.error("ERROR Exception get_pending_email.all:=" + e);
        }
        return return_arraylist;
    }
}

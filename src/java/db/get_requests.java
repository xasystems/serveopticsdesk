/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ralph
 */
public class get_requests 
{
    private static Logger logger = LogManager.getLogger();
    private static SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
    private static SimpleDateFormat db_timestamp_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   
    
    public static int next_request_id(Connection con) throws IOException, SQLException
    {
        int return_int = 0;  
        String ps_query_string = "SELECT max(id) AS value FROM request";        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
            }
            stmt.close();
            //add 1 to max
            return_int++;
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.next_request_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.next_request_id:=" + exc);
	}
        return return_int;
    }
    public static int request_created_count_start_date_stop_date(Connection con, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {
        int return_int = 0;  
        String ps_query_string = "";
        ps_query_string = "SELECT COUNT(*) AS value FROM request WHERE (request_date >= ? AND request_date <= ?)";
        
        PreparedStatement stmt;
        try 
        {
            Timestamp start = new Timestamp(start_date.getTime());
            Timestamp end = new Timestamp(end_date.getTime());
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.request_created_count_start_date_stop_date:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.request_created_count_start_date_stop_date:=" + exc);
	}
        return return_int;
    }
    public static int request_open_count(Connection con) throws IOException, SQLException
    {
        int return_int = 0;  
        String ps_query_string = "";
        ps_query_string = "SELECT count(*) as value FROM request WHERE state NOT LIKE '%closed%'";        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.request_open_count_start_date_stop_date:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.request_open_count_start_date_stop_date:=" + exc);
	}
        return return_int;
    }
    public static int request_closed_count_start_date_stop_date(Connection con, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {
        int return_int = 0;  
        String ps_query_string = "";
        ps_query_string = "SELECT COUNT(*) AS value FROM request WHERE (closed_date >= ? AND closed_date <= ?)";
        
        PreparedStatement stmt;
        try 
        {
            Timestamp start = new Timestamp(start_date.getTime());
            Timestamp end = new Timestamp(end_date.getTime());
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.request_closed_count_start_date_stop_date:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.request_closed_count_start_date_stop_date:=" + exc);
	}
        return return_int;
    }
    public static int request_count_start_date_stop_date_group_id_support(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        int return_int = 0;  
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT COUNT(*) AS value FROM request WHERE (request_date >= ? AND request_date <= ?)";
        }
        else
        {
            ps_query_string = "SELECT COUNT(*) AS value	FROM request WHERE (request_date >= ? AND request_date <= ?) AND assigned_group_id='" + group_id + "'";
        }
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.request_count_start_date_stop_date_group_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.request_count_start_date_stop_date_group_id:=" + exc);
	}
        return return_int;
    }
    public static int request_closed_count_start_date_stop_date_group_id_support(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        int return_int = 0;  
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT COUNT(*) AS value FROM request WHERE (closed_date >= ? AND closed_date <= ?)";
        }
        else
        {
            ps_query_string = "SELECT COUNT(*) AS value	FROM request WHERE (closed_date >= ? AND closed_date <= ?) AND assigned_group_id='" + group_id + "'";
        }
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.request_closed_count_start_date_stop_date_group_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.request_closed_count_start_date_stop_date_group_id:=" + exc);
	}
        return return_int;
    }
    public static int request_open_count_start_date_stop_date_group_id_support(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        int return_int = 0;  
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT COUNT(*) AS value FROM request WHERE (request_date >= ? AND request_date <= ?) AND state <> 'Closed'";
        }
        else
        {
            ps_query_string = "SELECT COUNT(*) AS value	FROM request WHERE (request_date >= ? AND request_date <= ?) AND state <> 'Closed' AND assigned_group_id='" + group_id + "'";
        }
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.request_open_count_start_date_stop_date_group_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.request_open_count_start_date_stop_date_group_id:=" + exc);
	}
        return return_int;
    }
    public static int request_count_start_date_stop_date_group_id_customer(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        int return_int = 0;  
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT COUNT(*) AS value FROM request WHERE (request_date >= ? AND request_date <= ?)";
        }
        else
        {
            ps_query_string = "SELECT COUNT(*) AS value	FROM request WHERE (request_date >= ? AND request_date <= ?) AND requested_for_group_id='" + group_id + "'";
        }
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");   
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.request_count_start_date_stop_date_group_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.request_count_start_date_stop_date_group_id:=" + exc);
	}
        return return_int;
    }    
    public static String[] request_by_id(Connection con, String id) throws IOException, SQLException
    {
        String return_string[] = new String[57];  
        //Request ID,Priority ,Request Time,State,State Time,Group,Analyst,Category,Sub-Category     
        ArrayList<String[]> users = get_users.all(con);;
        ArrayList<String[]> groups = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            String ps_query_string = "SELECT id,"
            + "rev,"
            + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date,"  // + "CONVERT_TZ(rev_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_rev_date`, "
            + "rev_by_user_id,"
            + "service_catalog_id," 
            + "service_catalog_item_id,"
            + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "  //+ "CONVERT_TZ(create_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_create_date`, "
            + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
            + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, " 
            + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, " 
            + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "  //+ "CONVERT_TZ(closed_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_closed_date`, "
            + "assigned_group_id," 
            + "assigned_to_id,"
            + "create_by_id,"
            + "requested_for_id," 
            + "requested_for_group_id," 
            + "location," 
            + "department," 
            + "site," 
            + "company," 
            + "impact," 
            + "urgency,"
            + "priority,"
            + "state," 
            + "contact_method,"
            + "approval,"
            + "price," 
            + "notes," 
            + "desk_notes," 
            + "description," 
            + "closed_reason,"
            + "required_info," 
            + "required_info_response,"
            + "optional_info," 
            + "optional_info_response " 
            + "FROM request WHERE id = ?";  
             
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, id);
            //System.out.println("request query=" + stmt);
            ResultSet rs = stmt.executeQuery();
            //init the return
            for(int z = 0; z < return_string.length;z++)
            {
                return_string[z] = "";
            }
            while(rs.next())
            {
                return_string[0] = rs.getString("id");//id
                return_string[1] = rs.getString("rev");//rev
                return_string[2] = support.string_utils.check_for_null(rs.getString("rev_date"));//rev_date
                return_string[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                return_string[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                return_string[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                //return_string[6] = support.string_utils.convert_timestamp_to_timestamp_string(rs.getTimestamp("create_date"));//create_date
                return_string[6] = support.string_utils.check_for_null(rs.getString("create_date"));//create_date
                return_string[7] = support.string_utils.check_for_null(rs.getString("request_date"));//request_date
                return_string[8] = support.string_utils.check_for_null(rs.getString("due_date"));//due_date
                return_string[9] = support.string_utils.check_for_null(rs.getString("resolve_date"));//resolve_date
                return_string[10] = support.string_utils.check_for_null(rs.getString("closed_date"));//closed_date
                return_string[11] = rs.getString("assigned_group_id");//assigned_group_id
                return_string[12] = rs.getString("assigned_to_id");//assigned_to_id
                return_string[13] = rs.getString("create_by_id");//create_by_id
                return_string[14] = rs.getString("requested_for_id");//requested_for_id
                return_string[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                return_string[16] = rs.getString("location");//location
                return_string[17] = rs.getString("department");//department
                return_string[18] = rs.getString("site");//site
                return_string[19] = rs.getString("company");//company
                return_string[20] = rs.getString("impact");//impact
                return_string[21] = rs.getString("urgency");//urgency
                return_string[22] = rs.getString("priority");//urgency
                return_string[23] = rs.getString("state");//state
                return_string[24] = rs.getString("contact_method");//contact_method
                return_string[25] = rs.getString("approval");//approval
                return_string[26] = rs.getString("price");//price
                return_string[27] = rs.getString("notes");//notes
                return_string[28] = rs.getString("desk_notes");//desk_notes
                return_string[29] = rs.getString("description");//description
                return_string[30] = rs.getString("closed_reason");//closed_reason
                
                return_string[31] = rs.getString("required_info");//required_info
                return_string[32] = rs.getString("required_info_response");//desk_notes
                return_string[33] = rs.getString("optional_info");//optional_info
                return_string[34] = rs.getString("optional_info_response");//optional_info_response


                //rev_by_user_id
                return_string[35] = "";
                return_string[36] = "";
                return_string[37] = "";
                return_string[38] = "";
                return_string[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        return_string[35] = this_user[1]; //username
                        return_string[36] = this_user[3]; //first
                        return_string[37] = this_user[4]; //MI
                        return_string[38] = this_user[5]; //Last
                        return_string[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                return_string[40] = "";
                for(int a = 0; a < groups.size(); a++)
                {
                    String this_group[] = groups.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        return_string[40] = this_group[1];
                        a = groups.size();
                    }
                }
                //assigned_to_id
                return_string[41] = "";
                return_string[42] = "";
                return_string[43] = "";
                return_string[44] = "";
                return_string[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        return_string[41] = this_user[1]; //username
                        return_string[42] = this_user[3]; //first
                        return_string[43] = this_user[4]; //MI
                        return_string[44] = this_user[5]; //Last
                        return_string[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                return_string[46] = "";
                return_string[47] = "";
                return_string[48] = "";
                return_string[49] = "";
                return_string[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        return_string[46] = this_user[1]; //username
                        return_string[47] = this_user[3]; //first
                        return_string[48] = this_user[4]; //MI
                        return_string[49] = this_user[5]; //Last
                        return_string[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                return_string[51] = "";
                return_string[52] = "";
                return_string[53] = "";
                return_string[54] = "";
                return_string[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        return_string[51] = this_user[1]; //username
                        return_string[52] = this_user[3]; //first
                        return_string[53] = this_user[4]; //MI
                        return_string[54] = this_user[5]; //Last
                        return_string[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                return_string[56] = "";
                for(int a = 0; a < groups.size(); a++)
                {
                    String this_group[] = groups.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        return_string[56] = this_group[1];
                        a = groups.size();
                    }
                }
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.request_by_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.request_by_id:=" + exc);
	}
        return return_string;
    }
    public static HashMap<String,String> request_hash_by_id(Connection con, String id, String user_tz) throws IOException, SQLException
    {
        HashMap<String,String> return_hashmap = new HashMap();  
        
        //String ps_query_string = "SELECT * FROM request WHERE id = ?";
        String ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date, "
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, "
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, "
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request WHERE id = ?";  
        
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next())
            {
                return_hashmap.put("id", rs.getString("id"));
                return_hashmap.put("rev", rs.getString("rev"));
                return_hashmap.put("rev_date", rs.getString("rev_date"));                
                return_hashmap.put("rev_by_user_id", rs.getString("rev_by_user_id"));
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        return_hashmap.put("rev_by_user_id_username", support.string_utils.check_for_null(this_user[1]));//username
                        return_hashmap.put("rev_by_user_id_first", support.string_utils.check_for_null(this_user[3]));//first
                        return_hashmap.put("rev_by_user_id_mi", support.string_utils.check_for_null(this_user[4]));//MI
                        return_hashmap.put("rev_by_user_id_last", support.string_utils.check_for_null(this_user[4]));//Last
                        return_hashmap.put("rev_by_user_id_vip", support.string_utils.check_for_null(this_user[4]));//VIP
                        a = users.size();
                    }
                }
                
                return_hashmap.put("service_catalog_id", support.string_utils.check_for_null(rs.getString("service_catalog_id")));
                String sc_info[] = db.get_service_catalog.service_catalog_by_id(con, rs.getString("service_catalog_id"));
                return_hashmap.put("service_catalog_id_name", support.string_utils.check_for_null(sc_info[1]));
                
                return_hashmap.put("service_catalog_item_id", support.string_utils.check_for_null(rs.getString("service_catalog_item_id")));
                String sc_item_info[] = db.get_service_catalog.service_catalog_items_by_id(con, rs.getString("service_catalog_item_id"));
                return_hashmap.put("service_catalog_item_id_name", support.string_utils.check_for_null(sc_item_info[1]));
                
                return_hashmap.put("create_date", rs.getString("create_date"));
                return_hashmap.put("request_date", support.string_utils.check_for_null(rs.getString("request_date")));
                return_hashmap.put("due_date", support.string_utils.check_for_null(rs.getString("due_date")));
                return_hashmap.put("resolve_date", support.string_utils.check_for_null(rs.getString("resolve_date")));
                return_hashmap.put("closed_date", support.string_utils.check_for_null(rs.getString("closed_date")));
                
                return_hashmap.put("assigned_group_id", rs.getString("assigned_group_id"));
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        return_hashmap.put("assigned_group_id_name", support.string_utils.check_for_null(this_group[1]));
                        a = all_group_info.size();
                    }
                }
                
                return_hashmap.put("assigned_to_id", support.string_utils.check_for_null(rs.getString("assigned_to_id")));
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        return_hashmap.put("assigned_to_id_username", support.string_utils.check_for_null(this_user[1]));
                        return_hashmap.put("assigned_to_id_first", support.string_utils.check_for_null(this_user[3]));
                        return_hashmap.put("assigned_to_id_mi", support.string_utils.check_for_null(this_user[4]));
                        return_hashmap.put("assigned_to_id_last", support.string_utils.check_for_null(this_user[5]));
                        return_hashmap.put("assigned_to_id_vip", support.string_utils.check_for_null(this_user[17]));
                        a = users.size();
                    }
                }
                
                return_hashmap.put("create_by_id", support.string_utils.check_for_null(rs.getString("create_by_id")));
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        return_hashmap.put("create_by_id_username", support.string_utils.check_for_null(this_user[1]));
                        return_hashmap.put("create_by_id_first", support.string_utils.check_for_null(this_user[3]));
                        return_hashmap.put("create_by_id_mi", support.string_utils.check_for_null(this_user[4]));
                        return_hashmap.put("create_by_id_last", support.string_utils.check_for_null(this_user[5]));
                        return_hashmap.put("create_by_id_vip", support.string_utils.check_for_null(this_user[17]));
                        a = users.size();
                    }
                }
                
                return_hashmap.put("requested_for_id", rs.getString("requested_for_id"));
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        return_hashmap.put("requested_for_id_username", support.string_utils.check_for_null(this_user[1]));
                        return_hashmap.put("requested_for_id_first", support.string_utils.check_for_null(this_user[3]));
                        return_hashmap.put("requested_for_id_mi", support.string_utils.check_for_null(this_user[4]));
                        return_hashmap.put("requested_for_id_last", support.string_utils.check_for_null(this_user[5]));
                        return_hashmap.put("requested_for_id_vip", support.string_utils.check_for_null(this_user[17]));
                        a = users.size();
                    }
                }
                
                return_hashmap.put("requested_for_group_id", rs.getString("requested_for_group_id"));
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        return_hashmap.put("requested_for_group_id_name", support.string_utils.check_for_null(this_group[1]));
                        a = all_group_info.size();
                    }
                }
                return_hashmap.put("location", support.string_utils.check_for_null(rs.getString("location")));
                return_hashmap.put("department", support.string_utils.check_for_null(rs.getString("department")));
                return_hashmap.put("site", support.string_utils.check_for_null(rs.getString("site")));
                return_hashmap.put("company", support.string_utils.check_for_null(rs.getString("company")));
                return_hashmap.put("impact", support.string_utils.check_for_null(rs.getString("impact")));
                return_hashmap.put("urgency", support.string_utils.check_for_null(rs.getString("urgency")));
                return_hashmap.put("priority", support.string_utils.check_for_null(rs.getString("priority")));
                return_hashmap.put("state", support.string_utils.check_for_null(rs.getString("state")));
                return_hashmap.put("contact_method", support.string_utils.check_for_null(rs.getString("contact_method")));
                return_hashmap.put("approval", support.string_utils.check_for_null(rs.getString("approval")));
                return_hashmap.put("price", support.string_utils.check_for_null(rs.getString("price")));
                return_hashmap.put("notes", support.string_utils.check_for_null(rs.getString("notes")));
                return_hashmap.put("desk_notes", support.string_utils.check_for_null(rs.getString("desk_notes")));
                return_hashmap.put("description", support.string_utils.check_for_null(rs.getString("description")));
                return_hashmap.put("closed_reason", support.string_utils.check_for_null(rs.getString("closed_reason")));
                return_hashmap.put("required_info", support.string_utils.check_for_null(rs.getString("required_info")));
                return_hashmap.put("required_info_response", support.string_utils.check_for_null(rs.getString("required_info_response")));
                return_hashmap.put("optional_info", support.string_utils.check_for_null(rs.getString("optional_info")));
                return_hashmap.put("optional_info_response", support.string_utils.check_for_null(rs.getString("optional_info_response")));
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.request_hash_by_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.request_hash_by_id:=" + exc);
	}
        //System.out.println("get_requests.request_hash_by_id=" + return_hashmap);
        return return_hashmap;
    }
    public static ArrayList<String[]> request_chart_drilldown_queue_support(Connection con, String priority, java.util.Date start_date, java.util.Date end_date, String group_id, String user_tz) throws IOException, SQLException
    {
        ArrayList return_arraylist = new ArrayList(); 
        //Request ID,Priority ,Request Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            //ps_query_string = "SELECT * FROM request WHERE (request_date >= ? AND request_date <= ?) AND priority = ? AND state NOT LIKE '%Closed%'";
            ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date, "                    
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, "
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, "
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request " 
                + "WHERE (request_date >= ? AND request_date <= ?) AND priority = ? AND state NOT LIKE '%Closed%'"; 
        }
        else
        {
            //ps_query_string = "SELECT *	FROM request WHERE (request_date >= ? AND request_date <= ?)  AND priority = ?  AND state NOT LIKE '%Closed%' AND assigned_group_id='" + group_id + "'";
            ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date, "    
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, "
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, "
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request " 
                + "WHERE (request_date >= ? AND request_date <= ?)  AND priority = ?  AND state NOT LIKE '%Closed%' AND assigned_group_id='" + group_id + "'";
        }
        
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            stmt.setString(3, priority);
            System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response
                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }

                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record); 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.request_chart_drilldown_queue_support:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.request_chart_drilldown_queue_support:=" + exc);
	}
        return return_arraylist;
    } 
    public static ArrayList<String[]> request_chart_drilldown_queue_customer(Connection con, String priority, java.util.Date start_date, java.util.Date end_date, String group_id, String user_tz) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList();  
        //Request ID,Priority ,Request Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            //ps_query_string = "SELECT * FROM request WHERE (request_date >= ? AND request_date <= ?) AND priority = ? AND state NOT LIKE '%Closed%'";
            ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date, "    
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, "
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, "
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request " 
                + "WHERE (request_date >= ? AND request_date <= ?) AND priority = ? AND state NOT LIKE '%Closed%'";
        }
        else
        {
            //ps_query_string = "SELECT *	FROM request WHERE (request_date >= ? AND request_date <= ?)  AND priority = ?  AND state NOT LIKE '%Closed%' AND requested_for_group_id='" + group_id + "'";
            ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date, "    
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, "
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, "
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request " 
                + "WHERE (request_date >= ? AND request_date <= ?)  AND priority = ?  AND state NOT LIKE '%Closed%' AND requested_for_group_id='" + group_id + "'";
        }        
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            stmt.setString(3, priority);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response
                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }

                return_arraylist.add(this_record);  
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.request_chart_drilldown_queue:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.request_chart_drilldown_queue:=" + exc);
	}
        return return_arraylist;
    } 
    public static ArrayList<String[]> request_chart_drilldown_number_support(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id, String user_tz) throws IOException, SQLException
    {
        ArrayList return_arraylist = new ArrayList();
        //Request ID,Priority ,Request Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        //date=12/21/2018&group_id=all
        SimpleDateFormat chart_date_format = new SimpleDateFormat("MM/dd/yyyy");
        
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            //ps_query_string = "SELECT * FROM request WHERE (request_date >= ? AND request_date <= ?)";
            ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date, "    
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, "
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, "
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request " 
                + "WHERE (request_date >= ? AND request_date <= ?)";
        }
        else
        {
            //ps_query_string = "SELECT *	FROM request WHERE (request_date >= ? AND request_date <= ?) AND assigned_group_id='" + group_id + "'";
            ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date, "    
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, "
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, "
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request " 
                + "WHERE (request_date >= ? AND request_date <= ?) AND assigned_group_id='" + group_id + "'";
        }
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            
            System.out.println("request_chart_drilldown_number_support q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason
                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response

                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record);  
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.request_chart_drilldown_number:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.request_chart_drilldown_number:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> request_chart_drilldown_number_customer(Connection con, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList();  
        //Request ID,Priority ,Request Time,State,State Time,Group,Analyst,Category,Sub-Category
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            //ps_query_string = "SELECT * FROM request WHERE (request_date >= ? AND request_date <= ?)";
            ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date, "    
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, "
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, "
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request " 
                + "WHERE (request_date >= ? AND request_date <= ?)";
        }
        else
        {
            //ps_query_string = "SELECT *	FROM request WHERE (request_date >= ? AND request_date <= ?) AND requested_for_group_id='" + group_id + "'";
            ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date, "    
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, "
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, "
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request " 
                + "WHERE (request_date >= ? AND request_date <= ?) AND requested_for_group_id='" + group_id + "'";
        }
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response
                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record);   
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.request_chart_drilldown_number:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.request_chart_drilldown_number:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> request_chart_drilldown_requestor_support(Connection con, String assigned_to_group_id,java.util.Date start_date, java.util.Date end_date, String user_tz) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        //Request ID,Priority ,Request Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        //ps_query_string = "SELECT * FROM request WHERE (request_date >= ? AND request_date <= ?) AND assigned_group_id = ?";
        ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date, "    
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, "
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, "
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request " 
                + "WHERE (request_date >= ? AND request_date <= ?) AND assigned_group_id = ?";     
        
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            stmt.setString(3, assigned_to_group_id);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response


                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record);  
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.request_chart_drilldown_category:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.request_chart_drilldown_category:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> request_chart_drilldown_requestor_customer(Connection con, String requested_for_group_id,java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList();
        String ps_query_string = "";
        //Request ID,Priority ,Request Time,State,State Time,Group,Analyst,Category,Sub-Category
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        if(requested_for_group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT * FROM request WHERE (request_date >= ? AND request_date <= ?)";
        }
        else
        {
            ps_query_string = "SELECT * FROM request WHERE (request_date >= ? AND request_date <= ?) AND requested_for_group_id='" + requested_for_group_id + "'" ;
        }        
        
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = support.string_utils.convert_timestamp_to_timestamp_string(rs.getTimestamp("create_date"));//create_date
                this_record[7] = support.string_utils.convert_timestamp_to_timestamp_string(rs.getTimestamp("request_date"));//request_date
                this_record[8] = support.string_utils.convert_timestamp_to_timestamp_string(rs.getTimestamp("due_date"));//due_date
                this_record[9] = support.string_utils.convert_timestamp_to_timestamp_string(rs.getTimestamp("resolve_date"));//resolve_date
                this_record[10] = support.string_utils.convert_timestamp_to_timestamp_string(rs.getTimestamp("closed_date"));//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason
                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response


                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record);  
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.request_chart_drilldown_category:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.request_chart_drilldown_category:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> request_chart_drilldown_closure_support(Connection con, String priority, java.util.Date start_date, java.util.Date end_date, String group_id, String user_tz) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        //Request ID,Priority ,Request Time,State,State Time,Group,Analyst,Category,Sub-Category
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            //ps_query_string = "SELECT * FROM request WHERE state LIKE '%Closed%' AND closed_date >= ? AND closed_date <= ? AND priority = ? ";
            ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date, "    
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, "
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, "
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request " 
                + "WHERE state LIKE '%Closed%' AND closed_date >= ? AND closed_date <= ? AND priority = ? ";
        }
        else
        {
            //ps_query_string = "SELECT * FROM request WHERE state LIKE '%Closed%' AND closed_date >= ? AND closed_date <= ? AND priority = ? AND assigned_group_id='" + group_id + "'";
            ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date, "    
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, "
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, "
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request " 
                + "WHERE state LIKE '%Closed%' AND closed_date >= ? AND closed_date <= ? AND priority = ? AND assigned_group_id='" + group_id + "'";
        }
        
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            stmt.setString(3, priority);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason
                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response


                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record);  
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.request_chart_drilldown_closure:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.request_chart_drilldown_closure:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> request_chart_drilldown_closure_customer(Connection con, String priority, java.util.Date start_date, java.util.Date end_date, String group_id) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList();  
        //Request ID,Priority ,Request Time,State,State Time,Group,Analyst,Category,Sub-Category
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT * FROM request WHERE state LIKE '%Closed%' AND closed_date >= ? AND closed_date <= ? AND priority = ? ";
        }
        else
        {
            ps_query_string = "SELECT * FROM request WHERE state LIKE '%Closed%' AND closed_date >= ? AND closed_date <= ? AND priority = ? AND requested_for_group_id='" + group_id + "'";
        }
        
        
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            stmt.setString(3, priority);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = support.string_utils.convert_timestamp_to_timestamp_string(rs.getTimestamp("create_date"));//create_date
                this_record[7] = support.string_utils.convert_timestamp_to_timestamp_string(rs.getTimestamp("request_date"));//request_date
                this_record[8] = support.string_utils.convert_timestamp_to_timestamp_string(rs.getTimestamp("due_date"));//due_date
                this_record[9] = support.string_utils.convert_timestamp_to_timestamp_string(rs.getTimestamp("resolve_date"));//resolve_date
                this_record[10] = support.string_utils.convert_timestamp_to_timestamp_string(rs.getTimestamp("closed_date"));//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason
                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response


                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record); 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.request_chart_drilldown_closure:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.request_chart_drilldown_closure:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> requests_start_date_stop_date_group_id(Connection con, java.util.Date start_date, java.util.Date end_date,String group_type, String group_id) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        //Request ID,Priority ,Request Time,State,State Time,Group,Analyst,Category,Sub-Category
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        if(group_id.equalsIgnoreCase("all"))
        {
            ps_query_string = "SELECT * FROM request WHERE (request_date >= ? AND request_date <= ?)";
        }
        else
        {
            if(group_type.equalsIgnoreCase("support"))
            {
                ps_query_string = "SELECT * FROM request WHERE (request_date >= ? AND request_date <= ?) AND assigned_group_id='" + group_id + "'";
            }
            else
            {
                ps_query_string = "SELECT * FROM request WHERE (request_date >= ? AND request_date <= ?) AND requested_for_group_id='" + group_id + "'";
            }
        }
        
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = support.string_utils.convert_timestamp_to_timestamp_string(rs.getTimestamp("create_date"));//create_date
                this_record[7] = support.string_utils.convert_timestamp_to_timestamp_string(rs.getTimestamp("request_date"));//request_date
                this_record[8] = support.string_utils.convert_timestamp_to_timestamp_string(rs.getTimestamp("due_date"));//due_date
                this_record[9] = support.string_utils.convert_timestamp_to_timestamp_string(rs.getTimestamp("resolve_date"));//resolve_date
                this_record[10] = support.string_utils.convert_timestamp_to_timestamp_string(rs.getTimestamp("closed_date"));//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason
                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response


                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record); 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.requests_start_date_stop_date_group_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.requests_start_date_stop_date_group_id:=" + exc);
	}
        return return_arraylist;
    }
    public static int requests_walkin_count_start_date_stop_date_(Connection con, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {
        int return_int = 0;  
        //Request ID,Priority ,Request Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        String ps_query_string = "";
        ps_query_string = "SELECT count(*) as value FROM request WHERE create_date >= ? AND create_date <= ? AND contact_method='Walk-in';";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            ResultSet rs = stmt.executeQuery();
            int count = 0;
            while(rs.next())
            {
                return_int = rs.getInt("value");
            }            
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.requests_walkin_count_start_date_stop_date_:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.requests_walkin_count_start_date_stop_date_:=" + exc);
	}
        return return_int;
    }  
    public static ArrayList<String[]> requests_start_date_stop_date(Connection con, java.util.Date start_date, java.util.Date end_date) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        //Request ID,Priority ,Request Time,State,State Time,Group,Analyst,Category,Sub-Category
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        Timestamp start = new Timestamp(start_date.getTime());
        Timestamp end = new Timestamp(end_date.getTime());
        //String query = "SELECT * FROM request WHERE (request_date >= ? AND request_date <= ?) ORDER BY request_date DESC";        
        String query = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date,"  // + "CONVERT_TZ(rev_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_rev_date`, "
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "  //+ "CONVERT_TZ(create_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_create_date`, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, " 
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, " 
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "  //+ "CONVERT_TZ(closed_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_closed_date`, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request WHERE (request_date >= ? AND request_date <= ?) ORDER BY request_date DESC";            
        ArrayList<String[]> users = get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(query);
            stmt.setTimestamp(1, start);
            stmt.setTimestamp(2, end);
            //System.out.println("q from get_requests.requests_start_date_stop_date=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason
                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response


                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record); 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.requests_start_date_stop_date:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.requests_start_date_stop_date:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> open_requests_assigned_to_id(Connection con, String assigned_to_id) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        //String ps_query_string = "";
        //ps_query_string = "SELECT * FROM request WHERE state NOT LIKE '%Closed%' AND assigned_to_id = ? ORDER BY request_date DESC";
        String ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date,"  // + "CONVERT_TZ(rev_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_rev_date`, "
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "  //+ "CONVERT_TZ(create_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_create_date`, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, " 
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, " 
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "  //+ "CONVERT_TZ(closed_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_closed_date`, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request WHERE state NOT LIKE '%Closed%' AND assigned_to_id = ? ORDER BY request_date DESC";
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, assigned_to_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response

                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record); 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.open_requests_assigned_to_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.open_requests_assigned_to_id:=" + exc);
	}
        return return_arraylist;
    }
    public static int open_request_count_assigned_to_id(Connection con, String assigned_to_id) throws IOException, SQLException
    {
        int count = 0;;  
        //Request ID,Priority ,Request Time,State,State Time,Group,Analyst,Category,Sub-Category
        String ps_query_string = "";
        ps_query_string = "SELECT count(*) AS request_count FROM request WHERE assigned_to_id = ? AND state NOT LIKE '%Closed%'";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, assigned_to_id);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("request_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.open_request_count_assigned_to_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.open_request_count_assigned_to_id:=" + exc);
	}
        return count;
    }
    public static ArrayList<String[]> requests_unassigned(Connection con) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        //String ps_query_string = "";
        //ps_query_string = "SELECT * FROM request WHERE assigned_to_id = ? AND state NOT LIKE '%closed%' ORDER BY request_date DESC";
        String ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date,"  // + "CONVERT_TZ(rev_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_rev_date`, "
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "  //+ "CONVERT_TZ(create_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_create_date`, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, " 
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, " 
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "  //+ "CONVERT_TZ(closed_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_closed_date`, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request WHERE assigned_to_id = ? AND state NOT LIKE '%closed%' ORDER BY request_date DESC";
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, "0");
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response
                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record); 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.requests_unassigned:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.requests_unassigned:=" + exc);
	}
        return return_arraylist;
    }
    public static int requests_unassigned_count(Connection con) throws IOException, SQLException
    {
        int return_int = 0;  
        String ps_query_string = "";
        ps_query_string = "SELECT count(*) as value FROM request WHERE assigned_to_id = ? AND state NOT LIKE '%closed%' ORDER BY request_date DESC;";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1,"0");
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_int = rs.getInt("value");                 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.requests_unassigned_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.requests_unassigned_count:=" + exc);
	}
        return return_int;
    }
    public static int unassigned_request_count(Connection con) throws IOException, SQLException
    {
        int count = 0;;  
        //Request ID,Priority ,Request Time,State,State Time,Group,Analyst,Category,Sub-Category
        String ps_query_string = "";
        ps_query_string = "SELECT count(*) AS request_count FROM request WHERE assigned_to_id = ? AND state <> 'Closed'";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, "0");
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("request_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.unassigned_request_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.unassigned_request_count:=" + exc);
	}
        return count;
    }
    public static ArrayList<String[]> requests_created_today(Connection con, String user_tz_name, String timestamp) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int year = Integer.parseInt(timestamp.substring(0, 4));
        int month = Integer.parseInt(timestamp.substring(4, 6));
        int date = Integer.parseInt(timestamp.substring(6, 8));
        int hour = Integer.parseInt(timestamp.substring(8, 10));
        int minute = Integer.parseInt(timestamp.substring(10, 12));
        int second = Integer.parseInt(timestamp.substring(12, 14));
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month - 1); //cal month starts at 0
        cal.set(Calendar.DATE, date);
        cal.set(Calendar.HOUR_OF_DAY,0);
        cal.set(Calendar.MINUTE,0);
        cal.set(Calendar.SECOND,0);        
        String start_date = timestamp_format.format(cal.getTime());
        cal.set(Calendar.HOUR_OF_DAY,23);
        cal.set(Calendar.MINUTE,59);
        cal.set(Calendar.SECOND,59);
        String end_date = timestamp_format.format(cal.getTime());
        //ps_query_string = "SELECT * FROM request WHERE create_date >= ? AND create_date <= ? ORDER BY request_date DESC";
        String ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date,"  // + "CONVERT_TZ(rev_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_rev_date`, "
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "  //+ "CONVERT_TZ(create_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_create_date`, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, " 
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, " 
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "  //+ "CONVERT_TZ(closed_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_closed_date`, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request WHERE create_date >= ? AND create_date <= ? ORDER BY request_date DESC";
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, start_date);
            stmt.setString(2, end_date);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response
                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record); 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.requests_created_today:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.requests_created_today:=" + exc);
	}
        return return_arraylist;
    }
    public static int requests_created_today_count(Connection con) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
        int count = 0;;  
        GregorianCalendar cal = new GregorianCalendar();
        cal.set(cal.HOUR_OF_DAY,00);
        cal.set(cal.MINUTE,00);
        cal.set(cal.SECOND,00);
        String start_date = timestamp_format.format(cal.getTime());
        cal.set(cal.HOUR_OF_DAY,23);
        cal.set(cal.MINUTE,59);
        cal.set(cal.SECOND,59);
        String end_date = timestamp_format.format(cal.getTime());
        String ps_query_string = "";
        ps_query_string = "SELECT count(*) AS request_count FROM request WHERE create_date >= ? AND create_date <= ?";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, start_date);
            stmt.setString(2, end_date);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("request_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.requests_created_today_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.requests_created_today_count:=" + exc);
	}
        return count;
    }
    public static ArrayList<String[]> requests_closed_today(Connection con, String user_tz_name, String timestamp) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        int year = Integer.parseInt(timestamp.substring(0, 4));
        int month = Integer.parseInt(timestamp.substring(4, 6));
        int date = Integer.parseInt(timestamp.substring(6, 8));
        int hour = Integer.parseInt(timestamp.substring(8, 10));
        int minute = Integer.parseInt(timestamp.substring(10, 12));
        int second = Integer.parseInt(timestamp.substring(12, 14));
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(user_tz_name));
        cal.set(cal.YEAR, year);
        cal.set(cal.MONTH, month - 1); //cal month starts at 0
        cal.set(cal.DATE, date);
        cal.set(cal.HOUR_OF_DAY,00);
        cal.set(cal.MINUTE,00);
        cal.set(cal.SECOND,00);        
        String start_date = timestamp_format.format(cal.getTime());
        cal.set(cal.HOUR_OF_DAY,23);
        cal.set(cal.MINUTE,59);
        cal.set(cal.SECOND,59);
        String end_date = timestamp_format.format(cal.getTime());
        //ps_query_string = "SELECT * FROM request WHERE closed_date >= ? AND closed_date <= ? ORDER BY request_date DESC";
        String ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date,"  // + "CONVERT_TZ(rev_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_rev_date`, "
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "  //+ "CONVERT_TZ(create_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_create_date`, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, " 
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, " 
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "  //+ "CONVERT_TZ(closed_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_closed_date`, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request WHERE closed_date >= ? AND closed_date <= ? ORDER BY request_date DESC";
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, start_date);
            stmt.setString(2, end_date);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response
                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record); 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.requests_closed_today:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.requests_closed_today:=" + exc);
	}
        return return_arraylist;
    }
    public static int requests_closed_today_count(Connection con) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
        int count = 0;;  
        GregorianCalendar cal = new GregorianCalendar();
        cal.set(cal.HOUR_OF_DAY,00);
        cal.set(cal.MINUTE,00);
        cal.set(cal.SECOND,00);
        String start_date = timestamp_format.format(cal.getTime());
        cal.set(cal.HOUR_OF_DAY,23);
        cal.set(cal.MINUTE,59);
        cal.set(cal.SECOND,59);
        String end_date = timestamp_format.format(cal.getTime());
        String ps_query_string = "";
        ps_query_string = "SELECT count(*) AS request_count FROM request WHERE closed_date >= ? AND closed_date <= ?";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, start_date);
            stmt.setString(2, end_date);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("request_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.requests_created_today_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.requests_created_today_count:=" + exc);
	}
        return count;
    }
    public static ArrayList<String[]> requests_open(Connection con) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        String return_string[][] = new String[0][0];  
        //Request ID,Priority ,Request Time,State,State Time,Group,Analyst,Category,Sub-Category
        //String ps_query_string = "";
        //ps_query_string = "SELECT * FROM request WHERE state NOT LIKE '%closed%' ORDER BY request_date DESC";
        String ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date,"  // + "CONVERT_TZ(rev_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_rev_date`, "
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "  //+ "CONVERT_TZ(create_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_create_date`, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, " 
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, " 
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "  //+ "CONVERT_TZ(closed_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_closed_date`, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request WHERE state NOT LIKE '%closed%' ORDER BY request_date DESC";
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason
                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response


                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record);  
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.requests_open:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.requests_open:=" + exc);
	}
        return return_arraylist;
    }
    public static int requests_open_count(Connection con) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
        int count = 0;;  
        String ps_query_string = "";
        ps_query_string = "SELECT count(*) AS request_count FROM request WHERE state NOT LIKE'%closed%' ORDER BY request_date DESC";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("request_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.requests_open_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.requests_open_count:=" + exc);
	}
        return count;
    }
    public static ArrayList<String[]> requests_open_7_14_days(Connection con) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList(); 
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        String return_string[][] = new String[0][0];  
        //Request ID,Priority ,Request Time,State,State Time,Group,Analyst,Category,Sub-Category
        //String ps_query_string = "";
        
        java.util.Date now = new java.util.Date();
        //System.out.println("now=" + now);
        Calendar cal = Calendar.getInstance();
        cal.setTime(new java.util.Date());
        cal.add(Calendar.DATE, -14);
        Date date_minus_14_days = cal.getTime();
        //System.out.println("now - 14 days=" + date_minus_14_days);
        
        cal = Calendar.getInstance();
        cal.setTime(new java.util.Date());
        cal.add(Calendar.DATE, -7);
        Date date_minus_7_days = cal.getTime();
        //System.out.println("now - 7 days=" + date_minus_7_days);
        
        
        
        String start_date = timestamp_format.format(date_minus_14_days);
        String end_date = timestamp_format.format(date_minus_7_days);
        //ps_query_string = "SELECT * FROM request WHERE request.create_date >= ? AND request.create_date <= ? AND state NOT LIKE '%closed%' ORDER BY create_date DESC";
        String ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date,"  // + "CONVERT_TZ(rev_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_rev_date`, "
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "  //+ "CONVERT_TZ(create_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_create_date`, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, " 
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, " 
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "  //+ "CONVERT_TZ(closed_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_closed_date`, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request WHERE request.request_date >= ? AND request.request_date <= ? AND state NOT LIKE '%closed%' ORDER BY request_date DESC";
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, start_date);
            stmt.setString(2, end_date);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason
                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response


                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record); 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.requests_open_7_14_days:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.requests_open_7_14_days:=" + exc);
	}
        return return_arraylist;
    }
    public static int requests_open_7_14_count(Connection con) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
        int count = 0;
        java.util.Date now = new java.util.Date();
        //System.out.println("now=" + now);
        Calendar cal = Calendar.getInstance();
        cal.setTime(new java.util.Date());
        cal.add(Calendar.DATE, -14);
        Date date_minus_14_days = cal.getTime();
        //System.out.println("now - 14 days=" + date_minus_14_days);
        
        cal = Calendar.getInstance();
        cal.setTime(new java.util.Date());
        cal.add(Calendar.DATE, -7);
        Date date_minus_7_days = cal.getTime();
        //System.out.println("now - 7 days=" + date_minus_7_days);
        
        String ps_query_string = "";
        ps_query_string = "SELECT count(*) AS request_count FROM request WHERE request.request_date >= ? AND request.request_date <= ? AND state NOT LIKE '%closed%' ORDER BY create_date DESC";
        
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, timestamp_format.format(date_minus_14_days));
            stmt.setString(2, timestamp_format.format(date_minus_7_days));
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("request_count");
                //System.out.println("count=" + count);
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.requests_open_7_14_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.requests_open_7_14_count:=" + exc);
	}
        return count;
    }
    public static ArrayList<String[]> requests_open_14_30_days(Connection con) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList();
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        GregorianCalendar cal = new GregorianCalendar();
        cal.add(cal.DATE, -30);
        cal.set(cal.HOUR_OF_DAY,00);
        cal.set(cal.MINUTE,00);
        cal.set(cal.SECOND,00);
        String start_date = timestamp_format.format(cal.getTime());
        GregorianCalendar cal2 = new GregorianCalendar();
        cal2.add(cal2.DATE, -14);
        cal2.set(cal2.HOUR_OF_DAY,00);
        cal2.set(cal2.MINUTE,00);
        cal2.set(cal2.SECOND,00);
        String end_date = timestamp_format.format(cal2.getTime());
        String ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date,"  // + "CONVERT_TZ(rev_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_rev_date`, "
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "  //+ "CONVERT_TZ(create_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_create_date`, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, " 
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, " 
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "  //+ "CONVERT_TZ(closed_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_closed_date`, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request WHERE request.request_date >= ? AND request.request_date <= ? AND state NOT LIKE '%closed%' ORDER BY request_date DESC";
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, start_date);
            stmt.setString(2, end_date);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response
                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record); 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.requests_open_14_30_days:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.requests_open_14_30_days:=" + exc);
	}
        return return_arraylist;
    }
    public static int requests_open_14_30_count(Connection con) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
        int count = 0;
        GregorianCalendar cal = new GregorianCalendar();
        cal.add(cal.DATE, -30);
        cal.set(cal.HOUR_OF_DAY,00);
        cal.set(cal.MINUTE,00);
        cal.set(cal.SECOND,00);
        String start_date = timestamp_format.format(cal.getTime());
        GregorianCalendar cal2 = new GregorianCalendar();
        cal2.add(cal2.DATE, -14);
        cal2.set(cal2.HOUR_OF_DAY,00);
        cal2.set(cal2.MINUTE,00);
        cal2.set(cal2.SECOND,00);
        String end_date = timestamp_format.format(cal2.getTime());
        
        String ps_query_string = "";
        ps_query_string = "SELECT count(*) AS request_count FROM request WHERE request.request_date >= ? AND request.request_date <= ? AND state NOT LIKE '%closed%' ORDER BY request_date DESC";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, start_date);
            stmt.setString(2, end_date);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("request_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.requests_open_14_130_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.requests_open_14_130_count:=" + exc);
	}
        return count;
    }
    public static ArrayList<String[]> requests_open_30_plus_days(Connection con) throws IOException, SQLException
    {
        ArrayList<String[]> return_arraylist = new ArrayList();
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        //Request ID,Priority ,Request Time,State,State Time,Group,Analyst,Category,Sub-Category
        GregorianCalendar cal = new GregorianCalendar();
        cal.add(cal.DATE, -30);
        cal.set(cal.HOUR_OF_DAY,00);
        cal.set(cal.MINUTE,00);
        cal.set(cal.SECOND,00);
        String start_date = timestamp_format.format(cal.getTime());
        String ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date,"  // + "CONVERT_TZ(rev_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_rev_date`, "
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "  //+ "CONVERT_TZ(create_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_create_date`, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, " 
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, " 
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "  //+ "CONVERT_TZ(closed_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_closed_date`, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request WHERE request_date <= ? AND state NOT LIKE '%closed%' ORDER BY request_date DESC";
        
        ArrayList<String[]> users = db.get_users.all(con);
        ArrayList<String[]> all_group_info = db.get_groups.all(con);
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, start_date);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response
                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record);  
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.requests_open_30_plus_days:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.requests_open_30_plus_days:=" + exc);
	}
        return return_arraylist;
    }
    public static int requests_open_30_plus_days_count(Connection con) throws IOException, SQLException
    {
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
        int count = 0;
        GregorianCalendar cal = new GregorianCalendar();
        cal.add(cal.DATE, -30);
        cal.set(cal.HOUR_OF_DAY,00);
        cal.set(cal.MINUTE,00);
        cal.set(cal.SECOND,00);
        String start_date = timestamp_format.format(cal.getTime());
        
        String ps_query_string = "";
        ps_query_string = "SELECT count(*) AS request_count FROM request WHERE request_date <= ? AND state NOT LIKE '%closed%' ORDER BY request_date DESC";
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, start_date);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("request_count");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.requests_open_30_plus_days_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.requests_open_30_plus_days_count:=" + exc);
	}
        return count;
    }
    public static ArrayList<Integer> my_requests(Connection con, String user_id)
    {
        PreparedStatement stmt;
        int total = 0;
        int critical = 0;
        int high = 0;
        int medium = 0;
        int low = 0;
        String priority = "";
        ArrayList<Integer> return_arraylist = new ArrayList();
        try
        {
            //total
            String ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date,"  // + "CONVERT_TZ(rev_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_rev_date`, "
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "  //+ "CONVERT_TZ(create_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_create_date`, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, " 
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, " 
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "  //+ "CONVERT_TZ(closed_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_closed_date`, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request WHERE (assigned_to_id=? OR requested_for_id=?) AND (state = 'Pending_Approval' OR state = 'Approved')";   
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, user_id);
            stmt.setString(2, user_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                total++;
                priority = rs.getString("priority");
                if(priority.equalsIgnoreCase("critical"))
                {
                    critical++;
                }
                else if(priority.equalsIgnoreCase("high"))
                {
                    high++;
                }
                else if(priority.equalsIgnoreCase("medium"))
                {
                    medium++;
                }
                else if(priority.equalsIgnoreCase("low"))
                {
                    low++;
                }
            }
            stmt.close();
            
            return_arraylist.add(0,total);
            //critical
            return_arraylist.add(1,critical);
            //high
            return_arraylist.add(2,high);
            //medium
            return_arraylist.add(3,medium);
            //low
            return_arraylist.add(4,low);
        }
        catch(Exception e)
        {
            System.out.println("Exception in db.get_requests.my_requests=" + e);
        }
        return return_arraylist;
    }
    public static ArrayList<String[]> my_requests_by_priority(Connection con, String priority, String user_id)
    {
        ArrayList<String[]> return_arraylist = new ArrayList();
        String ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date,"  // + "CONVERT_TZ(rev_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_rev_date`, "
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "  //+ "CONVERT_TZ(create_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_create_date`, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, " 
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, " 
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "  //+ "CONVERT_TZ(closed_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_closed_date`, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request WHERE state NOT LIKE '%closed%' AND (assigned_to_id = ? OR requested_for_id = ?) AND priority = ? ORDER BY request_date DESC";
        PreparedStatement stmt;
        try 
        {
            ArrayList<String[]> users = db.get_users.all(con);
            ArrayList<String[]> all_group_info = db.get_groups.all(con);
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, user_id);
            stmt.setString(2, user_id);
            stmt.setString(3, priority);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason
                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response


                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record);  
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.my_requests_by_priority:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.my_requests_by_priority:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> my_open_requests(Connection con, String user_id)
    {
        ArrayList<String[]> return_arraylist = new ArrayList();
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        String ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date,"  // + "CONVERT_TZ(rev_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_rev_date`, "
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "  //+ "CONVERT_TZ(create_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_create_date`, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, " 
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, " 
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "  //+ "CONVERT_TZ(closed_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_closed_date`, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request WHERE state NOT LIKE '%closed%' AND (assigned_to_id = ? OR requested_for_id=?) ORDER BY request_date DESC";
        
        PreparedStatement stmt;
        try 
        {
            ArrayList<String[]> users = db.get_users.all(con);
            ArrayList<String[]> all_group_info = db.get_groups.all(con);
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, user_id);
            stmt.setString(2, user_id);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = support.string_utils.check_for_null(rs.getString("id"));//id
                this_record[1] = support.string_utils.check_for_null(rs.getString("rev"));//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = support.string_utils.check_for_null(rs.getString("service_catalog_id"));//service_catalog_item_id
                this_record[5] = support.string_utils.check_for_null(rs.getString("service_catalog_item_id"));//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = support.string_utils.check_for_null(rs.getString("assigned_group_id"));//assigned_group_id
                this_record[12] = support.string_utils.check_for_null(rs.getString("assigned_to_id"));//assigned_to_id
                this_record[13] = support.string_utils.check_for_null(rs.getString("create_by_id"));//create_by_id
                this_record[14] = support.string_utils.check_for_null(rs.getString("requested_for_id"));//requested_for_id
                this_record[15] = support.string_utils.check_for_null(rs.getString("requested_for_group_id"));//requested_for_group_id
                this_record[16] = support.string_utils.check_for_null(rs.getString("location"));//location
                this_record[17] = support.string_utils.check_for_null(rs.getString("department"));//department
                this_record[18] = support.string_utils.check_for_null(rs.getString("site"));//site
                this_record[19] = support.string_utils.check_for_null(rs.getString("company"));//company
                this_record[20] = support.string_utils.check_for_null(rs.getString("impact"));//impact
                this_record[21] = support.string_utils.check_for_null(rs.getString("urgency"));//urgency
                this_record[22] = support.string_utils.check_for_null(rs.getString("priority"));//urgency
                this_record[23] = support.string_utils.check_for_null(rs.getString("state"));//state
                this_record[24] = support.string_utils.check_for_null(rs.getString("contact_method"));//contact_method
                this_record[25] = support.string_utils.check_for_null(rs.getString("approval"));//approval
                this_record[26] = support.string_utils.check_for_null(rs.getString("price"));//price
                this_record[27] = support.string_utils.check_for_null(rs.getString("notes"));//notes
                this_record[28] = support.string_utils.check_for_null(rs.getString("desk_notes"));//desk_notes
                this_record[29] = support.string_utils.check_for_null(rs.getString("description"));//description
                this_record[30] = support.string_utils.check_for_null(rs.getString("closed_reason"));//closed_reason
                
                this_record[31] = support.string_utils.check_for_null(rs.getString("required_info"));//required_info
                this_record[32] = support.string_utils.check_for_null(rs.getString("required_info_response"));//desk_notes
                this_record[33] = support.string_utils.check_for_null(rs.getString("optional_info"));//optional_info
                this_record[34] = support.string_utils.check_for_null(rs.getString("optional_info_response"));//optional_info_response


                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = support.string_utils.check_for_null(this_user[1]); //username
                        this_record[36] = support.string_utils.check_for_null(this_user[3]); //first
                        this_record[37] = support.string_utils.check_for_null(this_user[4]); //MI
                        this_record[38] = support.string_utils.check_for_null(this_user[5]); //Last
                        this_record[39] = support.string_utils.check_for_null(this_user[17]); //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = support.string_utils.check_for_null(this_group[1]);
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = support.string_utils.check_for_null(this_user[1]); //username
                        this_record[42] = support.string_utils.check_for_null(this_user[3]); //first
                        this_record[43] = support.string_utils.check_for_null(this_user[4]); //MI
                        this_record[44] = support.string_utils.check_for_null(this_user[5]); //Last
                        this_record[45] = support.string_utils.check_for_null(this_user[17]); //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = support.string_utils.check_for_null(this_user[1]); //username
                        this_record[47] = support.string_utils.check_for_null(this_user[3]); //first
                        this_record[48] = support.string_utils.check_for_null(this_user[4]); //MI
                        this_record[49] = support.string_utils.check_for_null(this_user[5]); //Last
                        this_record[50] = support.string_utils.check_for_null(this_user[17]); //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = support.string_utils.check_for_null(this_user[1]); //username
                        this_record[52] = support.string_utils.check_for_null(this_user[3]); //first
                        this_record[53] = support.string_utils.check_for_null(this_user[4]); //MI
                        this_record[54] = support.string_utils.check_for_null(this_user[5]); //Last
                        this_record[55] = support.string_utils.check_for_null(this_user[17]); //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record); 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.my_open_requests:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.my_open_requests:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<Integer> dashboard_requests_count(Connection con)
    {
        PreparedStatement stmt;
        int total = 0;
        int critical = 0;
        int high = 0;
        int medium = 0;
        int low = 0;
        String priority = "";
        ArrayList<Integer> return_arraylist = new ArrayList();
        try
        {
            //total
            String ps_query_string = "SELECT * from request where (state = 'Pending_Approval' OR state = 'Approved')";        
            stmt = con.prepareStatement(ps_query_string);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                total++;
                priority = rs.getString("priority");
                if(priority.equalsIgnoreCase("critical"))
                {
                    critical++;
                }
                else if(priority.equalsIgnoreCase("high"))
                {
                    high++;
                }
                else if(priority.equalsIgnoreCase("medium"))
                {
                    medium++;
                }
                else if(priority.equalsIgnoreCase("low"))
                {
                    low++;
                }
            }
            stmt.close();
            
            return_arraylist.add(0,total);
            //critical
            return_arraylist.add(1,critical);
            //high
            return_arraylist.add(2,high);
            //medium
            return_arraylist.add(3,medium);
            //low
            return_arraylist.add(4,low);
        }
        catch(Exception e)
        {
            System.out.println("Exception in db.get_requests.dashboard_requests_count=" + e);
        }
        return return_arraylist;
    }
    public static ArrayList<String[]> open_requests_by_priority(Connection con, String priority)
    {
        ArrayList<String[]> return_arraylist = new ArrayList();
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        String ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date,"  // + "CONVERT_TZ(rev_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_rev_date`, "
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "  //+ "CONVERT_TZ(create_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_create_date`, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, " 
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, " 
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "  //+ "CONVERT_TZ(closed_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_closed_date`, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request WHERE state NOT LIKE '%closed%' AND priority = ? ORDER BY request_date DESC";
        PreparedStatement stmt;
        try 
        {
            ArrayList<String[]> users = db.get_users.all(con);
            ArrayList<String[]> all_group_info = db.get_groups.all(con);
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, priority);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason
                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response


                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_user[] = users.get(a);
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record); 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.my_requests_by_priority:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.my_requests_by_priority:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> closed_requests_for_sc_item_id_by_date(Connection con, String sc_item_id, java.util.Date start, java.util.Date end)
    {
        ArrayList<String[]> return_arraylist = new ArrayList();
        String ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date,"  // + "CONVERT_TZ(rev_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_rev_date`, "
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "  //+ "CONVERT_TZ(create_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_create_date`, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, " 
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, " 
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "  //+ "CONVERT_TZ(closed_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_closed_date`, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request WHERE service_catalog_item_id=? AND state LIKE '%closed%' AND (closed_date >=? AND closed_date <= ?)";  
        PreparedStatement stmt;
        try 
        {
            Timestamp start_ts = new Timestamp(start.getTime());
            Timestamp end_ts = new Timestamp(end.getTime());
            ArrayList<String[]> users = db.get_users.all(con);
            ArrayList<String[]> all_group_info = db.get_groups.all(con);
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, sc_item_id);
            stmt.setTimestamp(2, start_ts);
            stmt.setTimestamp(3, end_ts);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response
                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record); 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.my_requests_by_priority:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.my_requests_by_priority:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> closed_complete_requests_for_sc_item_id_by_date(Connection con, String sc_item_id, java.util.Date start, java.util.Date end)
    {
        ArrayList<String[]> return_arraylist = new ArrayList();
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        String ps_query_string = "SELECT id,"
                + "rev,"
                + "DATE_FORMAT(rev_date,'%Y%m%d%H%i%s') AS rev_date,"  // + "CONVERT_TZ(rev_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_rev_date`, "
                + "rev_by_user_id,"
                + "service_catalog_id," 
                + "service_catalog_item_id,"
                + "DATE_FORMAT(create_date,'%Y%m%d%H%i%s') AS create_date, "  //+ "CONVERT_TZ(create_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_create_date`, "
                + "DATE_FORMAT(request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(due_date,'%Y%m%d%H%i%s') AS due_date, " 
                + "DATE_FORMAT(resolve_date,'%Y%m%d%H%i%s') AS resolve_date, " 
                + "DATE_FORMAT(closed_date,'%Y%m%d%H%i%s') AS closed_date, "  //+ "CONVERT_TZ(closed_date, @@session.time_zone, '" + user_tz_time + "') AS `tz_closed_date`, "
                + "assigned_group_id," 
                + "assigned_to_id,"
                + "create_by_id,"
                + "requested_for_id," 
                + "requested_for_group_id," 
                + "location," 
                + "department," 
                + "site," 
                + "company," 
                + "impact," 
                + "urgency,"
                + "priority,"
                + "state," 
                + "contact_method,"
                + "approval,"
                + "price," 
                + "notes," 
                + "desk_notes," 
                + "description," 
                + "closed_reason,"
                + "required_info," 
                + "required_info_response,"
                + "optional_info," 
                + "optional_info_response " 
                + "FROM request WHERE service_catalog_item_id=? AND state='Closed_Complete' AND due_date >= ? AND due_date <= ?";
        PreparedStatement stmt;
        try 
        {
            Timestamp start_ts = new Timestamp(start.getTime());
            Timestamp end_ts = new Timestamp(end.getTime());
            ArrayList<String[]> users = db.get_users.all(con);
            ArrayList<String[]> all_group_info = db.get_groups.all(con);
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string);
            stmt.setString(1, sc_item_id);
            stmt.setTimestamp(2, start_ts);
            stmt.setTimestamp(3, end_ts);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[57];
                this_record[0] = rs.getString("id");//id
                this_record[1] = rs.getString("rev");//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = rs.getString("rev_by_user_id");//rev_by_user_id
                this_record[4] = rs.getString("service_catalog_id");//service_catalog_item_id
                this_record[5] = rs.getString("service_catalog_item_id");//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = rs.getString("assigned_group_id");//assigned_group_id
                this_record[12] = rs.getString("assigned_to_id");//assigned_to_id
                this_record[13] = rs.getString("create_by_id");//create_by_id
                this_record[14] = rs.getString("requested_for_id");//requested_for_id
                this_record[15] = rs.getString("requested_for_group_id");//requested_for_group_id
                this_record[16] = rs.getString("location");//location
                this_record[17] = rs.getString("department");//department
                this_record[18] = rs.getString("site");//site
                this_record[19] = rs.getString("company");//company
                this_record[20] = rs.getString("impact");//impact
                this_record[21] = rs.getString("urgency");//urgency
                this_record[22] = rs.getString("priority");//urgency
                this_record[23] = rs.getString("state");//state
                this_record[24] = rs.getString("contact_method");//contact_method
                this_record[25] = rs.getString("approval");//approval
                this_record[26] = rs.getString("price");//price
                this_record[27] = rs.getString("notes");//notes
                this_record[28] = rs.getString("desk_notes");//desk_notes
                this_record[29] = rs.getString("description");//description
                this_record[30] = rs.getString("closed_reason");//closed_reason                
                this_record[31] = rs.getString("required_info");//required_info
                this_record[32] = rs.getString("required_info_response");//desk_notes
                this_record[33] = rs.getString("optional_info");//optional_info
                this_record[34] = rs.getString("optional_info_response");//optional_info_response
                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = this_user[1]; //username
                        this_record[36] = this_user[3]; //first
                        this_record[37] = this_user[4]; //MI
                        this_record[38] = this_user[5]; //Last
                        this_record[39] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = this_user[1]; //username
                        this_record[42] = this_user[3]; //first
                        this_record[43] = this_user[4]; //MI
                        this_record[44] = this_user[5]; //Last
                        this_record[45] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = this_user[1]; //username
                        this_record[47] = this_user[3]; //first
                        this_record[48] = this_user[4]; //MI
                        this_record[49] = this_user[5]; //Last
                        this_record[50] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = this_user[1]; //username
                        this_record[52] = this_user[3]; //first
                        this_record[53] = this_user[4]; //MI
                        this_record[54] = this_user[5]; //Last
                        this_record[55] = this_user[17]; //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                return_arraylist.add(this_record); 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.my_requests_by_priority:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.my_requests_by_priority:=" + exc);
	}
        return return_arraylist;
    }
    public static ArrayList<String[]> my_requests_for_self_help(Connection con, String user_id, String user_tz_time)
    {
        ArrayList<String[]> return_arraylist = new ArrayList();
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        String ps_query_string = "SELECT " 
                + "request.id,"
                + "request.rev,"
                + "DATE_FORMAT(request.rev_date,'%Y%m%d%H%i%s') AS rev_date, " 
                + "request.rev_by_user_id,"
                + "request.service_catalog_id," 
                + "request.service_catalog_item_id,"
                + "DATE_FORMAT(request.create_date,'%Y%m%d%H%i%s') AS create_date, "
                + "DATE_FORMAT(request.request_date,'%Y%m%d%H%i%s') AS request_date, "
                + "DATE_FORMAT(request.due_date,'%Y%m%d%H%i%s') AS due_date, "
                + "DATE_FORMAT(request.resolve_date,'%Y%m%d%H%i%s') AS resolve_date, "
                + "DATE_FORMAT(request.closed_date,'%Y%m%d%H%i%s') AS closed_date, "
                + "request.assigned_group_id," 
                + "request.assigned_to_id,"
                + "request.create_by_id,"
                + "request.requested_for_id," 
                + "request.requested_for_group_id," 
                + "request.location," 
                + "request.department," 
                + "request.site," 
                + "request.company," 
                + "request.impact," 
                + "request.urgency,"
                + "request.priority,"
                + "request.state," 
                + "request.contact_method,"
                + "request.approval,"
                + "request.price," 
                + "request.notes,"  // 27
                + "request.desk_notes," 
                + "request.description," 
                + "request.closed_reason,"
                + "request.required_info," 
                + "request.required_info_response,"
                + "request.optional_info," 
                + "request.optional_info_response, " 
                + "service_catalog_item.name, "
                + "service_catalog_item.description "
                + "FROM request INNER JOIN service_catalog_item  ON service_catalog_item.id = request.service_catalog_item_id AND service_catalog_item.catalog_id = request.service_catalog_id AND request.requested_for_id=? ORDER BY request.request_date DESC";
        //System.out.println("?=" + ps_query_string);
        PreparedStatement stmt;
        try 
        {
            ArrayList<String[]> users = db.get_users.all(con);
            ArrayList<String[]> all_group_info = db.get_groups.all(con);
            // Create a Statement Object
            stmt = con.prepareStatement(ps_query_string, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, user_id);
            //System.out.println("q=" + stmt);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                String this_record[] = new String[59];
                this_record[0] = support.string_utils.check_for_null(rs.getString("id"));//id
                this_record[1] = support.string_utils.check_for_null(rs.getString("rev"));//rev
                this_record[2] = rs.getString("rev_date");//rev_date
                this_record[3] = support.string_utils.check_for_null(rs.getString("rev_by_user_id"));//rev_by_user_id
                this_record[4] = support.string_utils.check_for_null(rs.getString("service_catalog_id"));//service_catalog_item_id
                this_record[5] = support.string_utils.check_for_null(rs.getString("service_catalog_item_id"));//service_catalog_item_id
                this_record[6] = rs.getString("create_date");//create_date
                this_record[7] = rs.getString("request_date");//request_date
                this_record[8] = rs.getString("due_date");//due_date
                this_record[9] = rs.getString("resolve_date");//resolve_date
                this_record[10] = rs.getString("closed_date");//closed_date
                this_record[11] = support.string_utils.check_for_null(rs.getString("assigned_group_id"));//assigned_group_id
                this_record[12] = support.string_utils.check_for_null(rs.getString("assigned_to_id"));//assigned_to_id
                this_record[13] = support.string_utils.check_for_null(rs.getString("create_by_id"));//create_by_id
                this_record[14] = support.string_utils.check_for_null(rs.getString("requested_for_id"));//requested_for_id
                this_record[15] = support.string_utils.check_for_null(rs.getString("requested_for_group_id"));//requested_for_group_id
                this_record[16] = support.string_utils.check_for_null(rs.getString("location"));//location
                this_record[17] = support.string_utils.check_for_null(rs.getString("department"));//department
                this_record[18] = support.string_utils.check_for_null(rs.getString("site"));//site
                this_record[19] = support.string_utils.check_for_null(rs.getString("company"));//company
                this_record[20] = support.string_utils.check_for_null(rs.getString("impact"));//impact
                this_record[21] = support.string_utils.check_for_null(rs.getString("urgency"));//urgency
                this_record[22] = support.string_utils.check_for_null(rs.getString("priority"));//urgency
                this_record[23] = support.string_utils.check_for_null(rs.getString("state"));//state
                this_record[24] = support.string_utils.check_for_null(rs.getString("contact_method"));//contact_method
                this_record[25] = support.string_utils.check_for_null(rs.getString("approval"));//approval
                this_record[26] = support.string_utils.check_for_null(rs.getString("price"));//price
                this_record[27] = support.string_utils.check_for_null(rs.getString("notes"));//notes
                this_record[28] = support.string_utils.check_for_null(rs.getString("desk_notes"));//desk_notes
                this_record[29] = support.string_utils.check_for_null(rs.getString("description"));//description
                this_record[30] = support.string_utils.check_for_null(rs.getString("closed_reason"));//closed_reason                
                this_record[31] = support.string_utils.check_for_null(rs.getString("required_info"));//required_info
                this_record[32] = support.string_utils.check_for_null(rs.getString("required_info_response"));//desk_notes
                this_record[33] = support.string_utils.check_for_null(rs.getString("optional_info"));//optional_info
                this_record[34] = support.string_utils.check_for_null(rs.getString("optional_info_response"));//optional_info_response


                //rev_by_user_id
                this_record[35] = "";
                this_record[36] = "";
                this_record[37] = "";
                this_record[38] = "";
                this_record[39] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("rev_by_user_id")))
                    {
                        this_record[35] = support.string_utils.check_for_null(this_user[1]); //username
                        this_record[36] = support.string_utils.check_for_null(this_user[3]); //first
                        this_record[37] = support.string_utils.check_for_null(this_user[4]); //MI
                        this_record[38] = support.string_utils.check_for_null(this_user[5]); //Last
                        this_record[39] = support.string_utils.check_for_null(this_user[17]); //VIP
                        a = users.size();
                    }
                }
                //assigned_group_id
                this_record[40] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("assigned_group_id")))
                    {
                        this_record[40] = support.string_utils.check_for_null(this_group[1]);
                        a = all_group_info.size();
                    }
                }
                //assigned_to_id
                this_record[41] = "";
                this_record[42] = "";
                this_record[43] = "";
                this_record[44] = "";
                this_record[45] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("assigned_to_id")))
                    {
                        this_record[41] = support.string_utils.check_for_null(this_user[1]); //username
                        this_record[42] = support.string_utils.check_for_null(this_user[3]); //first
                        this_record[43] = support.string_utils.check_for_null(this_user[4]); //MI
                        this_record[44] = support.string_utils.check_for_null(this_user[5]); //Last
                        this_record[45] = support.string_utils.check_for_null(this_user[17]); //VIP
                        a = users.size();
                    }
                }
                //create_by_id
                this_record[46] = "";
                this_record[47] = "";
                this_record[48] = "";
                this_record[49] = "";
                this_record[50] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("create_by_id")))
                    {
                        this_record[46] = support.string_utils.check_for_null(this_user[1]); //username
                        this_record[47] = support.string_utils.check_for_null(this_user[3]); //first
                        this_record[48] = support.string_utils.check_for_null(this_user[4]); //MI
                        this_record[49] = support.string_utils.check_for_null(this_user[5]); //Last
                        this_record[50] = support.string_utils.check_for_null(this_user[17]); //VIP
                        a = users.size();
                    }
                }
                //requested_for_id
                this_record[51] = "";
                this_record[52] = "";
                this_record[53] = "";
                this_record[54] = "";
                this_record[55] = "";
                for(int a = 0; a < users.size(); a++)
                {
                    String this_user[] = users.get(a);
                    if(this_user[0].equalsIgnoreCase(rs.getString("requested_for_id")))
                    {
                        this_record[51] = support.string_utils.check_for_null(this_user[1]); //username
                        this_record[52] = support.string_utils.check_for_null(this_user[3]); //first
                        this_record[53] = support.string_utils.check_for_null(this_user[4]); //MI
                        this_record[54] = support.string_utils.check_for_null(this_user[5]); //Last
                        this_record[55] = support.string_utils.check_for_null(this_user[17]); //VIP
                        a = users.size();
                    }
                }
                //requested_for_group_id
                this_record[56] = "";
                for(int a = 0; a < all_group_info.size(); a++)
                {
                    String this_group[] = all_group_info.get(a);
                    if(this_group[0].equalsIgnoreCase(rs.getString("requested_for_group_id")))
                    {
                        this_record[56] = this_group[1];
                        a = all_group_info.size();
                    }
                }
                this_record[57] = rs.getString("service_catalog_item.name");
                this_record[58] = rs.getString("service_catalog_item.description");
                return_arraylist.add(this_record); 
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.my_requests_for_self_help:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.my_requests_for_self_help:=" + exc);
	}
        return return_arraylist;
    }
    public static int all_filtered_by_query_count(Connection con, String query) throws IOException, SQLException
    {
        int count = 0;
        List<String> wheres = new ArrayList<>();
        PreparedStatement stmt;

        try 
        {
            query = "SELECT count(*) as counter FROM request " + ( query.equals("") ? "" : " WHERE " + query);

            stmt = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);

//                System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                count = rs.getInt("counter");
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_assets.all_filtered__by_query_count:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.all_filtered_by_query_count:=" + exc);
	}
        return count;
    }
    public static ArrayList<String[]> all_limited_filtered_by_query(Connection con, int starting_record, int fetch_size, String query, String order_column, String order_dir) throws IOException, SQLException
    {
        ArrayList <String[]> return_array_list = new ArrayList();
        List<String> wheres = new ArrayList<>();
        PreparedStatement stmt;

        try 
        {
            // Create a Statement Object
            query = "SELECT request.*,"
                    + "rev_by_users.username rev_by_user_username,rev_by_users.first rev_by_user_first,rev_by_users.mi rev_by_user_mi,rev_by_users.last rev_by_user_last,rev_by_users.phone_mobile rev_by_user_phone_mobile,"
                    + "assigned_tos.username assigned_to_username,assigned_tos.first assigned_to_first,assigned_tos.mi assigned_to_mi,assigned_tos.last assigned_to_last,assigned_tos.phone_mobile assigned_to_phone_mobile,assigned_tos.avatar assigned_to_avatar,"
                    + "created.username created_by_username,created.first created_by_first,created.mi created_by_mi,created.last created_by_last,created.phone_mobile created_by_phone_mobile,"
                    + "requested.username requester_username,requested.first requester_first,requested.mi requester_mi,requested.last requester_last,requested.phone_mobile requester_phone_mobile,requested.avatar requester_avatar,"
                    + "requester_groups.name requester_group_name,"
                    + "assigned_groups.name assigned_group_name, "
                    + "service_catalog.name service_catalog_name, service_catalog_item.name service_catalog_item_name "
                    + "FROM request "
                    + "LEFT JOIN users as rev_by_users ON request.rev_by_user_id = rev_by_users.id " 
                    + "LEFT JOIN users as assigned_tos ON request.assigned_to_id = assigned_tos.id " 
                    + "LEFT JOIN users as created ON request.create_by_id = created.id " 
                    + "LEFT JOIN users as requested ON request.requested_for_id = requested.id " 
                    + "LEFT JOIN `groups` requester_groups ON request.requested_for_group_id = requester_groups.id " 
                    + "LEFT JOIN `groups` assigned_groups ON request.assigned_group_id = assigned_groups.id " 
                    + "LEFT JOIN `service_catalog` ON request.service_catalog_id = service_catalog.id " 
                    + "LEFT JOIN `service_catalog_item` ON request.service_catalog_item_id = service_catalog_item.id " 
                    + ( query.equals("") ? "" : " WHERE " + query);

            if (!order_column.equals(""))
            {
                query += " ORDER BY " + order_column + " " + order_dir;
            } else {
                query += " ORDER BY id DESC";
            }

            query += " LIMIT ? , ?";

            stmt = con.prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);

            stmt.setInt(1, starting_record);
            stmt.setInt(2, fetch_size);
            System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();
            return_array_list = process_resultset(rs);
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.all_limited_filtered_by_query:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_assets.all_limited_filtered_by_query:=" + exc);
	}
        return return_array_list;
    }
    public static ArrayList<String[]> process_resultset(ResultSet rs)
    {
        ArrayList<String[]> return_arraylist = new ArrayList();
        try
        {
            while(rs.next())
            {
                try
                {
                    String temp[] = new String[61];
                    temp[0] = rs.getString("id");
                    temp[1] = rs.getString("rev");
                    temp[2] = rs.getString("rev_date");
                    temp[3] = rs.getString("rev_by_user_id");
                    temp[4] = rs.getString("service_catalog_id");
                    temp[5] = rs.getString("service_catalog_item_id");
                    temp[6] = rs.getString("create_date");
                    temp[7] = rs.getString("request_date");
                    temp[8] = rs.getString("due_date");
                    temp[9] = rs.getString("resolve_date");
                    temp[10] = rs.getString("closed_date");
                    temp[11] = rs.getString("assigned_group_id");
                    temp[12] = rs.getString("assigned_to_id");
                    temp[13] = rs.getString("create_by_id");
                    temp[14] = rs.getString("requested_for_id");
                    temp[15] = rs.getString("requested_for_group_id");
                    temp[16] = rs.getString("location");
                    temp[17] = rs.getString("department");
                    temp[18] = rs.getString("site");
                    temp[19] = rs.getString("company");
                    temp[20] = rs.getString("impact");
                    temp[21] = rs.getString("urgency");
                    temp[22] = rs.getString("priority");
                    temp[23] = rs.getString("state");
                    temp[24] = rs.getString("contact_method");
                    temp[25] = rs.getString("approval");
                    temp[26] = rs.getString("price");
                    temp[27] = rs.getString("notes");
                    temp[28] = rs.getString("desk_notes");
                    temp[29] = rs.getString("description");
                    temp[30] = rs.getString("closed_reason");

                    temp[31] = rs.getString("required_info");
                    temp[32] = rs.getString("required_info_response");
                    temp[33] = rs.getString("optional_info");
                    temp[34] = rs.getString("optional_info_response");

                    temp[35] = rs.getString("rev_by_user_username");
                    temp[36] = rs.getString("rev_by_user_first");
                    temp[37] = rs.getString("rev_by_user_mi");
                    temp[38] = rs.getString("rev_by_user_last");
                    temp[39] = rs.getString("rev_by_user_phone_mobile");

                    temp[40] = rs.getString("assigned_group_name");

                    temp[41] = rs.getString("assigned_to_username");
                    temp[42] = rs.getString("assigned_to_first");
                    temp[43] = rs.getString("assigned_to_mi");
                    temp[44] = rs.getString("assigned_to_last");
                    temp[45] = rs.getString("assigned_to_phone_mobile");

                    temp[46] = rs.getString("created_by_username");
                    temp[47] = rs.getString("created_by_first");
                    temp[48] = rs.getString("created_by_mi");
                    temp[49] = rs.getString("created_by_last");
                    temp[50] = rs.getString("created_by_phone_mobile");

                    temp[51] = rs.getString("requester_username");
                    temp[52] = rs.getString("requester_first");
                    temp[53] = rs.getString("requester_mi");
                    temp[54] = rs.getString("requester_last");
                    temp[55] = rs.getString("requester_phone_mobile");

                    temp[56] = rs.getString("requester_group_name");
                    temp[57] = rs.getString("service_catalog_name");
                    temp[58] = rs.getString("service_catalog_item_name");
                    
                    temp[59] = rs.getString("assigned_to_avatar");
                    temp[60] = rs.getString("requester_avatar");

                    return_arraylist.add(temp);
                }
                catch (Exception e)
                {
                    System.out.println("Exception in get_requests.process_resultset=" + e);
                }
            }  
        }
        catch(Exception e)
        {
            System.out.println("Exception in get_requests.process_resultset=" + e);
        }
        return return_arraylist;
    }

    public static Map<String, String> requests_count_by_states(Connection con, String user_id) {
        Map<String, String> map = new HashMap<>();
        PreparedStatement stmt;

        try 
        {

            // Create a Statement Object
            String query = "select state,count(id) as requests_count FROM request " + (!user_id.equals("") ? "WHERE assigned_to_id = " + user_id : "") + " GROUP BY state;";
            stmt = con.prepareStatement(query);
//            System.out.println("query=" + stmt.toString());

            ResultSet rs = stmt.executeQuery();
            int total_count = 0;
            while(rs.next())
            {
                map.put(rs.getString("state"), rs.getString("requests_count"));
                total_count = total_count + Integer.parseInt(rs.getString("requests_count"));
            }
            map.put("all", String.valueOf(total_count));

            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_requests.requests_count_by_states:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_requests.requests_count_by_states:=" + exc);
	}
        return map;
    }

}
    
    
    
    
<%@page import="java.time.ZonedDateTime"%>
<%@page import="java.util.TimeZone"%>
<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="org.apache.commons.text.StringEscapeUtils"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String FINANCIAL = session.getAttribute("financial").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String CONTACT = session.getAttribute("contact").toString();
        String CX = session.getAttribute("cx").toString();
        String SURVEY = session.getAttribute("survey").toString();
        String SLA = session.getAttribute("sla").toString();        
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        
        String context_dir = request.getServletContext().getRealPath("");
        String home_page = session.getAttribute("home_page").toString();
        LinkedHashMap props = support.config.get_config(context_dir);
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat display_format = new SimpleDateFormat("HH:mm/dd-MMM-yy");//18:31 / 14-Jan-19
        SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
        java.util.Date now = new java.util.Date();
        String now_display_format = display_format.format(now);
        String now_timestamp_format = timestamp_format.format(now);
        String now_incident_time = date_time_picker_format.format(now);
        String user_tz_name = "UTC";
        String user_tz_time = "+00:00";
        try
        {
            user_tz_name = session.getAttribute("tz_name").toString();
            user_tz_time = session.getAttribute("tz_time").toString();
            if(user_tz_name == null || user_tz_time == null )
            {
                user_tz_name = "UTC";
                user_tz_time = "+00:00";
            }

        }
        catch(Exception e)
        {
            user_tz_name = "UTC";
            user_tz_time = "+00:00";
        }
        date_time_picker_format.setTimeZone(TimeZone.getTimeZone(user_tz_name));

        Connection con = null;
        try
        {
            con = db.db_util.get_contract_connection(context_dir, session); 
            String user_id = session.getAttribute("user_id").toString();
            String id = request.getParameter("id");
            //db log use
            boolean used = db.get_kb.use_article(con, user_id, id);
            //update user sessions
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            //get article text
            String article_info[] = db.get_kb.article_info_for_id(con, id);
            //unescape it 
  
            String unEscapedHTML = StringEscapeUtils.unescapeHtml4(article_info[5]);
            String created_date_display = "";
            String approved_date_display = "";
            String expiration_date_display = "";

            if (!article_info[8].equals("")) {
                java.util.Date created_date = new java.util.Date();            
                created_date = timestamp_format.parse(article_info[8]);  //created_date = filter_format.parse("01/12/2019 12:00 AM");
                ZonedDateTime zdt_created_date = support.date_utils.user_tz_to_utc(user_tz_name, timestamp_format.format(created_date));
                created_date = java.util.Date.from(zdt_created_date.toInstant());
                created_date_display = display_format.format(created_date);
            }

            if (!article_info[10].equals("")) {
                java.util.Date approved_date = new java.util.Date();            
                approved_date = timestamp_format.parse(article_info[10]);  //approved_date = filter_format.parse("01/12/2019 12:00 AM");
                ZonedDateTime zdt_approved_date = support.date_utils.user_tz_to_utc(user_tz_name, timestamp_format.format(approved_date));
                approved_date = java.util.Date.from(zdt_approved_date.toInstant());
                approved_date_display = display_format.format(approved_date);
            }
            if (!article_info[11].equals("")) {
                java.util.Date expiration_date = new java.util.Date();            
                expiration_date = timestamp_format.parse(article_info[11]);  //expiration_date = filter_format.parse("01/12/2019 12:00 AM");
                ZonedDateTime zdt_expiration_date = support.date_utils.user_tz_to_utc(user_tz_name, timestamp_format.format(expiration_date));
                expiration_date = java.util.Date.from(zdt_expiration_date.toInstant());
                expiration_date_display = display_format.format(expiration_date);
            }
            //get session kb feedbacks
            String feedback_left_on_this_article = "";
            try
            {
                String temp = session.getAttribute("feedback_left_article_ids").toString();
                String kb_ids[] = temp.split(",");
                for(int a = 0; a < kb_ids.length;a++)
                {
                    if(kb_ids[a].equalsIgnoreCase(id))
                    {
                        feedback_left_on_this_article = "disabled=\"true\"";
                        break;
                    }                    
                }
            }
            catch(Exception e)
            {
                feedback_left_on_this_article = "";
            }
            
    %>
<form>
        <% if (!request.getHeader("referer").contains("home_self_service")) { %>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel font-weight-bolder">
                        Name
                    </label>
                </div>
                <div class="border-0">
                    <%=article_info[2]%>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel font-weight-bolder">
                        Description
                    </label>
                </div>
                <div class="border-0">
                    <%=article_info[3]%>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel font-weight-bolder">
                        Knowledge Base
                    </label>
                </div>
                <div class="border-0">
                    <%=article_info[12]%>
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel font-weight-bolder">
                        Status
                    </label>
                </div>
                <div class="border-0">
                    <%=article_info[4]%>
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel font-weight-bolder">
                        Author
                    </label>
                </div>
                <div class="border-0">
                    <%=article_info[13] + " " + article_info[14]%>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel font-weight-bolder">
                        Created
                    </label>
                </div>
                <div class="border-0">
                    <%=created_date_display%>
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel font-weight-bolder">
                        Approved
                    </label>
                </div>
                <div class="border-0">
                    <%=approved_date_display%>
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel font-weight-bolder">
                        Expires
                    </label>
                </div>
                <div class="border-0">
                    <%=expiration_date_display%>
                </div>
            </div>
        </div>
        <% } %>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="kba-content clr md px-3">
                    <!--<div class="kba-content w-100 border border-dark p-2 mx-3">-->
                        <%out.println(unEscapedHTML);%>                    
                    <!--</div>-->
                </div>
            </div>
        </div>      
        <div class="row">
            <div class="col-md-12 text-center">
                Was this article helpful? 
                    <ul>
                        <li class="dInlineBlock valign-m pl-15">
                            <button type="button" class="btn md customBtn dInlineBlock btn-wht" onclick="articleHelpfull('<%=id%>','true')">
                                <img style="height:14px;" src="assets/images/svg/up-arrow.svg" alt="" class="mr-2">
                                <strong>
                                    Yes: <span class="useful_counter"><%=article_info[16]%></span>
                                </strong>
                            </button>
                        </li>
                        <li class="dInlineBlock valign-m pl-15">
                            <button type="button" class="btn md customBtn dInlineBlock btn-wht" onclick="articleHelpfull('<%=id%>','false')">
                                <img style="height:14px;" src="assets/images/svg/down-arrow.svg" alt="" class="mr-2">
                                <strong>
                                    No: <span class="useful_counter"><%=article_info[17]%></span>
                                </strong>
                            </button>
                        </li>
                    </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="javascript:void(0)" data-target="article" data-id="<%=id%>" class="mr-5 rightSidePanelOpener2">Suggest an edit to this article</a>
            </div>
        </div>
</form>
    </body>
</html>
<%
        }
        catch(Exception e)
        {
            System.out.println("kb_view.jsp exception=" + e);
        }
    }//end if not logged in
%>
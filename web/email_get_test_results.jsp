<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        if(!session.getAttribute("administration").toString().equalsIgnoreCase("true"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);              
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String status = "";                
                ArrayList<String[]> messages = support.email.get_all_for_test(con, context_dir);               
                
                
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END PAGE LEVEL CSS-->
    
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title">Settings</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a></li>
                                <li class="breadcrumb-item"><a href="admin.jsp">Administration</a></li>
                                <li class="breadcrumb-item"><a href="admin_email.jsp">Email Settings</a></li>
                                <li class="breadcrumb-item"><a href="#">Get Email Test</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div> <!--End breadcrumbs -->
            <div class="row match-height">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form" action="admin_email" method="post">
                                    <p class="card-text">Test Get Email Settings</p>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4>
                                                Get Email Results (Limited to 10)
                                            </h4>
                                            <table id="message_table" class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                    <tr>
                                                        <th class="no-sort">From</th>
                                                        <th>Subject</th>
                                                        <th>To</th>
                                                        <th>Date Sent</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                    
                                                    <%
                                                    for(int a = 0; a < messages.size();a++)
                                                    {
                                                        String message[] = messages.get(a);
                                                        %>
                                                        <tr>
                                                        <td><%=message[0]%></td>
                                                        <td><%=message[1]%></td>
                                                        <td><%=message[2]%></td>
                                                        <td><%=message[3]%></td>
                                                        </tr>
                                                        <%
                                                    }
                                                    
                                                    %>
                                                    
                                                    
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="no-sort">From</th>
                                                        <th>Subject</th>
                                                        <th>To</th>
                                                        <th>Date Sent</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    
                                    <div class="form-actions">
                                        <button type="button" onclick="javascript:location.href='admin_email.jsp'" class="btn btn-warning mr-1">
                                            <i class="ft-x"></i> Go Back to Email Settings
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <!-- END PAGE LEVEL JS-->   
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_user.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_user.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.sql.*" %>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        if (!session.getAttribute("incident").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("true")) 
        {
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            session.setAttribute("home_page", "home_analytics.jsp");
            GregorianCalendar first_of_month = new GregorianCalendar();
            first_of_month.set(Calendar.DAY_OF_MONTH, 1);
            first_of_month.set(Calendar.HOUR_OF_DAY, 0);
            first_of_month.set(Calendar.MINUTE, 0);
            first_of_month.set(Calendar.SECOND, 0);     
            GregorianCalendar now = new GregorianCalendar();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap sys_props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session); 
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	

<!-- BEGIN VENDOR CSS-->
<!--<link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">-->
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
<!-- END VENDOR CSS-->
<!-- BEGIN MODERN CSS-->
<!--<link rel="stylesheet" type="text/css" href="app-assets/css/app.css">-->
<!-- END MODERN CSS-->
<!-- BEGIN Page Level CSS-->
<!--<link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">-->
<!--<link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">-->
<link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">


    <div class="clr whtbg p-10 mb-15 position-relative">
        <div class="float-right options">
            <ul>
                <li class="dInlineBlock dropdown ">
                    <div class="dInlineBlock clr menuHolder">
                        <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                        </a>
                        <div class="dropdown-menu dropdown-menu-right md p-10" x-placement="bottom-end" style="position: absolute; transform: translate3d(100px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                            <h5 class="medium-font boldFont pt-5 mb-10">All Filters</h5>
                            <form>
                                <ul>
                                    <li class="mb-10 clr">
                                        <div class="formField md  mr-0 full-width">
                                            <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="formField md  mr-0 full-width">
                                            <select class="no-border">
                                                <option>Support Group</option>
                                                <option>Support Group2</option>
                                            </select>
                                        </div>
                                    </li>

                                </ul>
                                <div class="text-center pt-5 pb-5">
                                    <button class="btn md2 btn-primary-new customBtn mb-5">Apply Filters</button>
                                    <div class="clr"></div>
                                    <a href="#" class="">Reset Filters</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </li>

            </ul>
        </div>
        <h1 class="large-font boldFont">Analytics</h1>
        <div class="custom-tabs clr ">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#incidents-tab" data-url="incident_home_support.jsp" role="tab" aria-selected="true">Incidents </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#request-tab" role="tab" aria-selected="false">Request</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#contacts-tab" role="tab" aria-selected="false">Contacts</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#csat-survey-tab" role="tab" aria-selected="false">CSAT Surveys</a>
                </li>
            </ul><!-- Tab panes -->
        </div>
    </div>

    <div class=" clr mb-15 text-right ">
        <div class="clr tag dInlineBlock formField md whtbg ml-5">
            <input class="no-border full-width daterange" type="text" name="daterange" value="" placeholder="Select Date Range">
            <img class="icon" src="assets/images/svg/cross-icon.svg" alt="">
        </div>
        <div class="clr tag dInlineBlock formField md whtbg ml-5">
            <strong>Support Group:</strong> Help Desk
            <img class="icon" src="assets/images/svg/cross-icon.svg" alt="">
        </div>
        <!-- <div class="formField md full-width mr-0 float-right whtbg">
            <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015" />
        </div> -->
    </div>

    <div class="tab-content">
        <div class="tab-pane" id="incidents-tab" role="tabpanel">
incidents
        </div>
       <div class="tab-pane" id="request-tab" role="tabpanel">
            <p>Second Panel</p>
        </div>
        <div class="tab-pane" id="contacts-tab" role="tabpanel">

            <div class="group-info-holder clr">
                <div class="holder">
                    <div class=" box mb-20 clr">
                        <h5 class="boldFont basic-font mb-50">Total Incidents</h5>
                        <div class="float-left boldFont basic-font graph-index">
                            <span class="icon mr-5"><img src="assets/images/svg/high-arrow-icon.svg" alt=""></span>-16
                        </div>
                        <div class="float-right xxl-font boldFont">
                            588
                        </div>
                    </div>
                </div>
                <div class="holder">
                    <div class=" box mb-20 clr">
                        <h5 class="boldFont basic-font mb-50">Open Incidents</h5>
                        <div class="float-left boldFont basic-font graph-index">
                            <span class="icon mr-5"><img src="assets/images/svg/down-arrow-icon.svg" alt=""></span>-16
                        </div>
                        <div class="float-right xxl-font boldFont">
                            276
                        </div>
                    </div>
                </div>
                <div class="holder">
                    <div class=" box mb-20 clr">
                        <h5 class="boldFont basic-font mb-50">Closed Incidents</h5>

                        <div class="float-right xxl-font boldFont">
                            588
                        </div>
                    </div>
                </div>
                <div class="holder">
                    <div class=" box mb-20 clr">
                        <h5 class="boldFont basic-font mb-50">Service Compliance</h5>
                        <div class="float-left boldFont basic-font graph-index">
                            <span class="icon mr-5"><img src="assets/images/svg/high-arrow-icon.svg" alt=""></span>0.70%
                        </div>
                        <div class="float-right xxl-font boldFont">
                            67.51%
                        </div>
                    </div>
                </div>
                <div class="holder">
                    <div class=" box mb-20 clr">
                        <h5 class="boldFont basic-font mb-50">CSAT Surveys</h5>

                        <div class="float-right xxl-font boldFont">
                            3.00
                        </div>
                    </div>
                </div>
            </div>
            <div class="formField md dInlineBlock mb-30 whtbg">
                <select class="no-border">
                    <option>Show All</option>
                    <option>Phone Contacts</option>
                    <option>Email Contacts</option>
                    <option>Chat Contacts</option>
                    <option>Voice Mail Contacts</option>
                </select>
            </div>
            <div class="clr"></div>
            <div class="clr mb-30">
                <div class="top clr mb-15">
                    <div class="float-right">
                        <div class="clr searchFieldHolder dInline formField whtbg">
                            <span class="icon"><img src="assets/images/svg/searchIcon.svg" alt=""></span>
                            <input id="" type="text" dir="ltr" placeholder="Find anything...">
                        </div> 
                    </div>
                    <h4 class="boldFont large-font pt-15">Phone Contacts</h4>
                </div>
                <div class="sort-tbl-holder">
                    <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Channel</th>
                                <th>Volume</th>
                                <th>Avg ASA (Sec)</th>
                                <th>Avg. Handle Time(Sec)</th>
                                <th>Avg. Duration(Sec)</th>
                                <th>Avg. Wrap Up Time(Sec) </th>
                                <th>Abandoned Rate</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>          
            </div>
            <div class="clr mb-30">
                <div class="top clr mb-15">
                    <div class="float-right">
                        <div class="clr searchFieldHolder dInline formField whtbg">
                            <span class="icon"><img src="assets/images/svg/searchIcon.svg" alt=""></span>
                            <input id="" type="text" dir="ltr" placeholder="Find anything...">
                        </div> 
                    </div>
                    <h4 class="boldFont large-font pt-15">Email Contacts</h4>
                </div>
                <div class="sort-tbl-holder">
                    <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Channel</th>
                                <th>Volume</th>
                                <th>Avg ASA (Sec)</th>
                                <th>Avg. Handle Time(Sec)</th>
                                <th>Avg. Duration(Sec)</th>
                                <th>Avg. Wrap Up Time(Sec) </th>
                                <th>Abandoned Rate</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>          
            </div>
            <div class="clr mb-30">
                <div class="top clr mb-15">
                    <div class="float-right">
                        <div class="clr searchFieldHolder dInline formField whtbg">
                            <span class="icon"><img src="assets/images/svg/searchIcon.svg" alt=""></span>
                            <input id="" type="text" dir="ltr" placeholder="Find anything...">
                        </div> 
                    </div>
                    <h4 class="boldFont large-font pt-15">Chat Contacts</h4>
                </div>
                <div class="sort-tbl-holder">
                    <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Channel</th>
                                <th>Volume</th>
                                <th>Avg ASA (Sec)</th>
                                <th>Avg. Handle Time(Sec)</th>
                                <th>Avg. Duration(Sec)</th>
                                <th>Avg. Wrap Up Time(Sec) </th>
                                <th>Abandoned Rate</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>          
            </div>
            <div class="clr mb-30">
                <div class="top clr mb-15">
                    <div class="float-right">
                        <div class="clr searchFieldHolder dInline formField whtbg">
                            <span class="icon"><img src="assets/images/svg/searchIcon.svg" alt=""></span>
                            <input id="" type="text" dir="ltr" placeholder="Find anything...">
                        </div> 
                    </div>
                    <h4 class="boldFont large-font pt-15">Voice Mail Contacts</h4>
                </div>
                <div class="sort-tbl-holder">
                    <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Channel</th>
                                <th>Volume</th>
                                <th>Avg ASA (Sec)</th>
                                <th>Avg. Handle Time(Sec)</th>
                                <th>Avg. Duration(Sec)</th>
                                <th>Avg. Wrap Up Time(Sec) </th>
                                <th>Abandoned Rate</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    (225) 555-0118
                                </td>
                                <td>423</td>
                                <td>
                                    177
                                </td>
                                <td>
                                    274
                                </td>
                                <td>
                                    877
                                </td>
                                <td>
                                    429
                                </td>
                                <td>
                                    229%
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>          
            </div>
        </div>
        <div class="tab-pane" id="csat-survey-tab" role="tabpanel">
            <p>Fourth Panel</p>
        </div>
    </div>
    
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <script type="text/javascript">
        $(function() {
            $('input[name="daterange"]').daterangepicker();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                console.log(this.hash,e.target,e.relatedTarget);  // newly activated tab
                // previous active tab
                var url = $(this).attr("data-url");

                if (typeof url !== "undefined") {
                    var pane = $(this), href = this.hash;

                    // ajax load from data-url
                    $(href).load(url,function(result){      
                        pane.tab('show');
                    });
                } else {
                    $(this).tab('show');
                }
            });
            $('.nav-tabs').find("a[href='#incidents-tab']").tab("show");
        });
    </script>

    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>
<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        boolean authorized = support.role.authorized(session, "asset","update"); 
        if(!authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);   
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <!-- END PAGE LEVEL CSS-->
    <div class="clr whtbg p-10 mb-15">
        <div class="float-right options">
            <ul>
                <li class="dInlineBlock">
                    <span data-target="type" class="btn btn-success customBtn lg waves-effect rightSidePanelOpener">
                        <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>Add Type
                    </span>    
                </li>
            </ul>
        </div>

        <h1 class="large-font boldFont">Assets Types/Subtypes</h1>
    </div>    
    
    <div class="sort-tbl-holder">
        <table class="table table-striped custom-sort-table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="20%">Type</th>
                    <th width="72%">Subtype(s)</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <%
                ArrayList<String[]> asset_types = db.get_assets.all_types(con);
                for(int a = 0; a < asset_types.size(); a++)
                {
                    String[] asset_type = asset_types.get(a);
                    %>
                    <tr>
                        <td>
                            <%=asset_type[1]%>
                        </td>
                        <td>
                            <%
                            ArrayList<String[]> subtypes = db.get_assets.all_subtypes_for_type_id(con, asset_type[0]);
                            for(int b = 0; b < subtypes.size(); b++)
                            {
                                String subtype[] = subtypes.get(b);
                                %>
                                <%=subtype[2] + (b < subtypes.size()-1 ? " | " : "")%> 
                                <%
                            }
                            %>
                        </td>
                        <td>
                            <a href="javascript:void(0)" data-target="type" data-type-id="<%=asset_type[0]%>" class="mr-5 rightSidePanelOpener"><img src="assets/images/svg/edit-icon.svg" alt=""></a>
                            <a href="javascript:void(0)" onclick="showDeleteModal(<%=asset_type[0]%>)" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>
                        </td>
                    </tr>
                    <%
                }
                %>                                                  
            </tbody>
        </table>
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>

    <div class="rightPanelHolder">
        <div class="p-15 ticket-content">
            <span class="rightSidePanelCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
            <h4 id="rightPanelTitle" class="mediumFont large-font mb-20"></h4>
            <div id="rightPanelContent" class="clr whtbg p-15">
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="deleteModalLabel">Asset Type Delete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            Are you sure you want to delete this Asset Type with all Subtypes?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-danger" onclick="deleteAssetType()">Delete</button>
          </div>
        </div>
      </div>
    </div>         
    
    <!-- BEGIN PAGE LEVEL JS-->
    <script>
        var delete_asset_type_id = -1;
        
        function showDeleteModal(asset_type_id) 
        {
            delete_asset_type_id = asset_type_id;
            $('#deleteModal').modal();
        }    

        function deleteAssetType() 
        {
            window.location.assign("assets_type_subtype_delete_type?type_id="+delete_asset_type_id);
        }    

        function delete_row(cell) 
        {
            $(cell).closest('tr').remove();
        }
    
        function add_row(data) 
        {
//            document.getElementById("option_table_div").classList.remove("d-none");
            var object_table = document.getElementById("option_table");
            var uniq = Date.now();            
            /*** We insert the row by specifying the current rows length ***/
            var object_row = object_table.insertRow();

            /*** We insert the first row cell ***/
            var checkbox_cell = object_row.insertCell(0);

            var subtype_active = "";
            if (data !== undefined && data.hasOwnProperty("active") && data.active == "true") 
            {
                subtype_active = "checked";
            }    
            var subtype_id = "";
            if (data !== undefined && data.hasOwnProperty("id")) 
            {
                subtype_id = data.id;
            }    
            checkbox_cell.classList.add("text-center");
            checkbox_cell.innerHTML = "<div class='custom-control custom-checkbox dInlineBlock'>" +
                    "<input type='hidden' name='subtype_id_" + uniq + "' value='" + uniq + "'>" +
                    "<input name='subtype_active_" + uniq + "' id='subtyperow_" + uniq + "' class='custom-control-input' type='checkbox' value='true' " + subtype_active + ">" +
                    "<label class='custom-control-label small-font' for='subtyperow_" + uniq + "'></label>" +
                    "</div";

            /*** field ***/
            var field_value_cell = object_row.insertCell(1);
            /*** We  add some text inside the celll ***/
            var field_value = "";
            if (data !== undefined && data.hasOwnProperty("name")) 
            {
                field_value = data.name;
            }    
            field_value_cell.innerHTML = "<input class='form-control' type='text' name='subtype_name_" + uniq + "' value='" + field_value + "'/>";

            var delete_icon_cell = object_row.insertCell(2);
            delete_icon_cell.innerHTML = "<a href='javascript:void(0)' onclick='delete_row(this)' class='mr-5'><img src='assets/images/svg/trash-icon-blk.svg' alt=''></a>";
            
        }

        
        function addRightSidePanelListeners()
        {
            $('.rightSidePanelCloser:not(.listens)').each(function(){
                $(this).addClass('listens');
                this.addEventListener("click", function()
                {
                    $('.rightPanelHolder').removeClass('open');
                    $('.rightSidePanel').removeClass('open');
                    $('html').removeClass('panelOpen');
                }, false);
            });

            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission

            var validation = Array.prototype.filter.call(forms, function(form) 
            {
                $(form).removeClass("needs-validation");
                form.addEventListener('submit', function(event) 
                {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                        $('.rightPanelHolder').animate({scrollTop: $('.form-control:invalid').first().offset().top - 50}, 'fast');
                    }
                    form.classList.add('was-validated');
                }, false);
            });            


            $('.rightSidePanelOpener:not(.listens)').each(function(){
    //            console.log("attaching to ", this);
                $(this).addClass('listens');

                this.addEventListener("click", function()
                {
                    $("#rightPanelContent").html("");

                    var panel_title = "";
                    var ajax = {};
                    ajax.url = "";
                    ajax.type = 'GET';
                    ajax.dataType = "html";
                    var clicked_node = $(event.target).closest(".rightSidePanelOpener");
                    var target = clicked_node.data('target');
                    var type_id = clicked_node.data('typeId');

                    switch (target) 
                    {
                        case "type":
                            if (type_id)
                            {
                                ajax.url = "assets_type_subtype_edit.jsp";
                                ajax.data = {type_id: type_id};
                                panel_title = "Edit Asset Type";
                            } else {
                                ajax.url = "assets_type_subtype_add_type.jsp";
                                panel_title = "Create Asset Type";
                            }
                            break;
                        default:
                            break;
                    }

                    if (ajax.url === "") return false;

                    ajax.error = function(jqXHR, textStatus)
                    {
                        switch (jqXHR.status) {
                            case 401:
                                window.location.assign("<%=props.get("login_url").toString()%>");
                                break;
                            case 403:
                                alert("No permission!");
                                break;
                            default:
                                alert("Error processing request!");
                        }
                    };

                    ajax.success = function (data) 
                    {                    
                        $('.rightPanelHolder').addClass('open');
                        $('.rightSidePanel').addClass('open');
                        $('html').addClass('panelOpen');

                        $("#rightPanelTitle").html(panel_title);
                        $("#rightPanelContent").html(data);
                        addRightSidePanelListeners();
//                        attachFieldListeners($("#rightPanelContent"));        

                    }

                    $.ajax(ajax);
                }, false);
            });

        }

        window.addEventListener("load", (event) => 
        {
            addRightSidePanelListeners();
        });
    </script>
    <!-- END PAGE LEVEL JS-->  
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_systen_select_fields.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_systen_select_fields.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

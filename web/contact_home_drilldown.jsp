<%@page import="db.get_incidents"%>
<%@page import="db.get_chart_data"%>
<!--Copyright 2019 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : incident_home
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>

<!DOCTYPE html>
<%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        if (!session.getAttribute("contact").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            String home_page = session.getAttribute("home_page").toString();
            //if you have the permission to read/modify or Admin then you can see this
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
            //SimpleDateFormat filter_format2 = new SimpleDateFormat("yyyy"); //01/12/2019 12:00 AM
            java.util.Date filter_start = new java.util.Date();
            java.util.Date filter_end = new java.util.Date();
            java.util.Date previous_start_date = new java.util.Date();
            java.util.Date previous_end_date = new java.util.Date();
            String start = "";
            String end = "";
            
            String date_range = "";
            String selected = "";
            try 
            {
                date_range = request.getParameter("date_range");
                if (date_range.equalsIgnoreCase("null") || date_range == null) 
                {
                    date_range = support.filter_dates.past_30_days();
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                date_range = support.filter_dates.past_30_days();
            }
            
            try 
            {
                
                //split the start and end date  //01/12/2019 12:00 AM - 01/12/2019 11:59 PM
                String temp[] = date_range.split("-");
                filter_start = filter_format.parse(temp[0]);
                filter_end = filter_format.parse(temp[1]);                
                start = timestamp.format(filter_start);
                end = timestamp.format(filter_end);

                //get pervious date frame                
                previous_start_date.setTime(filter_start.getTime());
                previous_end_date.setTime(filter_end.getTime());                
                long diff = filter_end.getTime() - filter_start.getTime();
                previous_start_date.setTime(filter_start.getTime() - diff);
                previous_end_date.setTime(filter_end.getTime() - diff);
                
                
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                System.out.println("contact_home.jsp exception=" + e);
            }
            String channel = request.getParameter("channel");
            String media_type = request.getParameter("media_type");
%>
<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>	
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
<!-- END VENDOR CSS-->
<!-- BEGIN MODERN CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
<!-- END MODERN CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
<link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
<link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">

<!-- END Page Level CSS-->
<!-- BEGIN Custom CSS-->
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<!-- END Custom CSS-->
<jsp:include page='menu_service_desk.jsp'>
    <jsp:param name="active_menu" value=""/>
</jsp:include>	
<input type="hidden" name="start" id="start" value="<%=start%>"/>
<input type="hidden" name="end" id="end" value="<%=end%>"/>


<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title"><i class="la la-shopping-cart"></i>&nbsp;Contact</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="contact_home.jsp">Contact Home</a>
                            </li>
                            <li class="breadcrumb-item active">Channel Drilldown 
                            </li>
                        </ol>
                    </div>
                </div>                    
            </div>
        </div>
        <!-- start content here-->
        <div class="row">
            <div class="col-xl-3 col-6">
                <div class="form-group">
                    <label>Date Range</label>
                    <div class='input-group'>
                        <input type='text' id="filter_date_range" name="filter_date_range" value="<%=date_range%>" class="form-control datetime" />
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <span class="la la-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-1 col-2">
                <label>&nbsp;</label>
                <div class="form-actions">
                    <button type="button" onclick="reload_page()" class="btn btn-blue mr-1">
                        <i class="fa-filter"></i> Apply
                    </button>
                </div>
            </div>
        </div>       
                   
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><b>Channel: <%=channel%>&nbsp;&nbsp;&nbsp;Media Type: <%=media_type%></b></h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body pt-0">
                            <table class="table table-striped table-bordered zero-configuration">
                                <thead>
                                    <tr>
                                        <th>Channel</th>
                                        <th>Agent</th>
                                        <th>Avg. ASA (Sec)
                                            <i class="la la-question-circle info" data-toggle="popover" data-content="ASA is a call center metric for the average amount of time it takes for calls to be answered in a call center during a specific time period. This includes the amount of time callers wait in a waiting queue and while the agent's phone rings however does not include the time it takes for callers to navigate through the IVR." data-trigger="hover" data-original-title="Avg. ASA(Sec)"></i>                                            
                                        </th>
                                        <th>Abandoned Time (Sec) 
                                            <i class="la la-question-circle info" data-toggle="popover" data-content="An abandoned call is one where the caller hangs up before being connected to a live agent in the service desk. Call abandonment rate is the number of abandoned calls divided by all calls offered to the service desk. The abandoned time threshold can be set for chat/phone under the Administration -> Setting menu." data-trigger="hover" data-original-title="Abandoned Rate"></i>                                            
                                        </th>
                                        <th>Avg. Handle Time(Sec)
                                            <i class="la la-question-circle info" data-toggle="popover" data-content="Average handle time (AHT) is a call center metric for the average duration of one transaction, measured from the customer's initiation of the call and including any hold time, talk time and related tasks that follow the transaction. AHT is a prime factor when deciding call center staffing levels." data-trigger="hover" data-original-title="Avg. Handle Time(Sec)"></i>                                            
                                        </th>
                                        <th>Avg. Duration(Sec)   
                                            <i class="la la-question-circle info" data-toggle="popover" data-content="Average Call Duration (ACD): A measure of the average call length for an individual agent, useful for measuring worker efficiency. If an agent's ACD is far higher than that of their colleagues, it can indicate a need for further training and coaching. However, ACD becomes a less effective metric at higher levels of support. When a support position is highly detailed, a focus on customer satisfaction and overall agent methodology are more effective indicators." data-trigger="hover" data-original-title="Avg. Duration(Sec)"></i>                                            
                                        </th>
                                        <th>Avg. Wrap Up Time(Sec)   
                                            <i class="la la-question-circle info" data-toggle="popover" data-content="Average wrap up time is the post-call work time an agent spends to complete the call.." data-trigger="hover" data-original-title="Avg. Wrap Up Time(Sec)"></i>                                            
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                    String agent = "";
                                    String asa = "";
                                    String abandoned_time = "";
                                    String handle_time = "";
                                    String duration = "";
                                    String wrap_up = "";
                                    
                                    String table_values[][] = db.get_contact.all_contacts_for_media_type_channel_timeframe(con,media_type, channel, filter_start, filter_end);
                                    for(int a = 0; a < table_values.length; a++)
                                    {
                                        if(table_values[a][8] == null){channel = "--";}else{channel = table_values[a][8];}
                                        if(table_values[a][10] == null){agent = "--";}else{agent = table_values[a][10];}
                                        if(table_values[a][4] == null){asa = "--";}else{asa = table_values[a][4];}
                                        if(table_values[a][6] == null){abandoned_time = "--";}else{abandoned_time = table_values[a][6];}
                                        if(table_values[a][12] == null){handle_time = "--";}else{handle_time = table_values[a][12];}
                                        if(table_values[a][7] == null){duration = "--";}else{duration = table_values[a][7];}
                                        if(table_values[a][11] == null){wrap_up = "--";}else{wrap_up = table_values[a][11];}
                                        %>
                                        <tr>
                                        <td><%=channel%></td>
                                        <td><%=agent%></td>
                                        <td><%=asa%></td>
                                        <td><%=abandoned_time%></td>
                                        <td><%=handle_time%></td>
                                        <td><%=duration%></td>
                                        <td><%=wrap_up%></td>   
                                        </tr>
                                        <%
                                    }
                                    %>                                    
                                </tbody>
                                <%
                                if(table_values.length > 11)
                                {
                                    %>
                                    <tfoot>
                                        <th>Channel</th>
                                        <th>Volume</th>
                                        <th>Avg. ASA (Sec)</th>
                                        <th>Abandoned Time (Sec) </th>
                                        <th>Avg. Handle Time(Sec)</th>
                                        <th>Avg. Duration(Sec)</th>
                                        <th>Avg. Wrap Up Time(Sec)</th>
                                        
                                    </tfoot>
                                    <%
                                }
                                %>
                            </table>
                        </div>
                    </div>
                </div>
            </div>            
        </div>  
        <!-- end content here-->
        
    </div> <!--end  content-wrapper -->       
</div><!--end  app-content content -->
<jsp:include page='footer.jsp'>
    <jsp:param name="parameter" value=""/>
</jsp:include>	
<!-- BEGIN VENDOR JS-->

<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN MODERN JS-->
<script src="app-assets/js/core/app-menu.js"></script>
<script src="app-assets/js/core/app.js"></script>
<!-- END MODERN JS-->
<!-- BEGIN PAGE LEVEL JS-->

<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
<script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
<script src="app-assets/js/scripts/popover/popover.js"></script>
<script>
    function reload_page()
    {
        var filter_date_range = document.getElementById("filter_date_range").value;
        var URL = "contact_home.jsp?date_range=" + filter_date_range;
        window.location.href = URL;
    }
</script>


<!-- END PAGE LEVEL JS-->

</body>
</html>
<%
            con.close();
        }//end if user has permission
        else {
            String RedirectURL = "no_permission.jsp";
            response.sendRedirect(RedirectURL);
        }
    }//end if user is logged in
%>

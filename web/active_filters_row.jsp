<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="java.util.Arrays"%>
<!-- Active filters row -->
<div class="clr ">
<%  
String[] filter_fields = (String[])request.getAttribute("filter_fields");

try
    {
        for(String name : request.getParameterMap().keySet())
        {
            for(String value : request.getParameterValues(name))
            {
                if (!"".equals(value) && !"any".equalsIgnoreCase(value) && Arrays.stream(filter_fields).anyMatch(name::equals))
                {
                    %>
                        <div class="clr tag dInlineBlock formField md whtbg mr-5 mb-15">
                            <h5 class="basic-font mediumFont"><%=StringUtils.capitalize(name).replace("_"," ")%></h5>
                            <span><%=value%></span>
                            <img class="icon" src="assets/images/svg/cross-icon.svg" alt="" onclick="resetFilterField('<%=name%>')">
                        </div>
                    <%
                }
            }
        }
%>
<script type="text/javascript">
    function resetFilterField(field) 
    {
        let url = new URL(location.href);
        url.searchParams.delete(field);
        location.href = url.href;
    }
    function resetFilters()
    {
        let url = new URL(location.href);
        location.href = url.origin + url.pathname;
    }

</script>
<%
    } catch (Exception e)
    {
    }
%>
</div>
<!-- End of Active filters row -->

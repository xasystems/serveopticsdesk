<%@page import="db.get_chart_data"%>
<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : tier0_self_help.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else 
    {
        if (!session.getAttribute("contact").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
            //SimpleDateFormat filter_format2 = new SimpleDateFormat("yyyy"); //01/12/2019 12:00 AM
            java.util.Date filter_start = new java.util.Date();
            java.util.Date filter_end = new java.util.Date();
            java.util.Date previous_start_date = new java.util.Date();
            java.util.Date previous_end_date = new java.util.Date();
            String start = "";
            String end = "";
            
            String date_range = "";
            String selected = "";
            try 
            {
                date_range = request.getParameter("date_range");
                if (date_range.equalsIgnoreCase("null") || date_range == null) 
                {
                    date_range = support.filter_dates.past_30_days();
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                date_range = support.filter_dates.past_30_days();
            }
            
            try 
            {
                
                //split the start and end date  //01/12/2019 12:00 AM - 01/12/2019 11:59 PM
                String temp[] = date_range.split("-");
                filter_start = filter_format.parse(temp[0]);
                filter_end = filter_format.parse(temp[1]);                
                start = timestamp.format(filter_start);
                end = timestamp.format(filter_end);

                //get pervious date frame                
                previous_start_date.setTime(filter_start.getTime());
                previous_end_date.setTime(filter_end.getTime());                
                long diff = filter_end.getTime() - filter_start.getTime();
                previous_start_date.setTime(filter_start.getTime() - diff);
                previous_end_date.setTime(filter_end.getTime() - diff);
                
                
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                System.out.println("contact_home.jsp exception=" + e);
            }
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	
    <!-- BEGIN VENDOR CSS-->
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/app.css">-->
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">

    <script src="assets/js/highcharts/highcharts.js"></script>
    <script src="assets/js/highcharts/data.js"></script>
    <script src="assets/js/highcharts/drilldown.js"></script>
    <script src="assets/js/highcharts/exporting.js"></script>
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <!--<link rel="stylesheet" type="text/css" href="assets/css/style.css">-->
    <!-- END Custom CSS-->
    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	

<div class="clr whtbg p-10 mb-15 position-relative">
    <div class="float-right options">
        <ul>
            <li id="filters_dropdown" class="dInlineBlock dropdown ">
                <div class="dInlineBlock clr menuHolder">
                    <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                    </a>
                    <div class="dropdown-menu dropdown-menu-right md p-10" x-placement="bottom-end" style="position: absolute; transform: translate3d(100px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <h5 class="medium-font boldFont pt-5 mb-10">All Filters</h5>
                        <form id="filter_form">
                            <ul>
                                <li class="mb-10 clr">
                                    <div class="formField md  mr-0 full-width">
                                        <input class="no-border full-width datetime" type="text" name="filter_date_range" id="filter_date_range" value="<%=date_range%>">
                                    </div>
                                </li>
                            </ul>
                            <div class="text-center pt-5 pb-5">
                                <button type="button" class="btn md2 btn-primary-new customBtn mb-5" onclick="reload_page()">Apply Filters</button>
                                <div class="clr"></div>
                                <a href="#" class="">Reset Filters</a>
                            </div>
                        </form>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <h1 class="large-font boldFont">Tier 0 Self Help</h1>
</div>

<div id="active_filters">
</div>
    
            <div class="row">
                <div class="col-md-6">
                    <div class=" box mb-20">
                        <h4 class="boldFont basic-font mb-30">Number of Unique Users per Day</h4>
                        <div class="graph-holder text-center">
                            <div id="unique_chart" class="chart" style="min-width: 250px; height: 180px; margin: 0 auto"></div>
                            <%
                            String unique_chart_categories = "";
                            String unique_chart_values = "";
                            String unique_chart_data[][] = get_chart_data.contact_dashboard_number_of_unique_users_per_day(con, filter_start, filter_end);
                            //12-Dec,y_value,12/12/2018 
                            //build categories string categories: ['12-Dec', '13-Dec', '14-Dec', '15-Dec']
                            for(int b =0; b < unique_chart_data.length; b++)
                            {
                                if(b == 0)
                                {
                                    unique_chart_categories = "'" + unique_chart_data[b][0] + "'";
                                    unique_chart_values = "{y:" + unique_chart_data[b][1] + ", date:'" + unique_chart_data[b][2] + "', date_range:'" + unique_chart_data[b][3] + "'}";
                                }
                                else
                                {
                                    unique_chart_categories = unique_chart_categories + ",'" + unique_chart_data[b][0] + "'";
                                    unique_chart_values = unique_chart_values + ",{y:" + unique_chart_data[b][1] + ", date:'" + unique_chart_data[b][2] + "', date_range:'" + unique_chart_data[b][3] + "'}";
                                }
                            }
                            %>
                            <script>
                                Highcharts.chart('unique_chart', {
                                    chart: {
                                        type: 'spline'
                                    },
                                    title: {
                                        text: null
                                    },
                                    exporting: {
                                        enabled: false
                                    },
                                    credits: {
                                        enabled: false
                                    },
                                    legend: {
                                        enabled: false
                                    },
                                    xAxis: {
                                        categories: [<%=unique_chart_categories%>]
                                    },
                                    yAxis: {
                                        title: {
                                            x: -10,
                                            text: 'Number'
                                        }
                                    },

                                    plotOptions: {
                                        spline: {
                                            marker: {
                                                enabled: false
                                            }
                                        }
                                        ,
                                        series: {
                                            cursor: 'pointer',
                                            point: {
                                                events: {
                                                    click: function (event) {
                                                        //window.location = "tier0_self_help.jsp?date_range=" + event.point.date_range;
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    series: [{
                                            name: "Number of unique users using Self-Help",
                                            color: '#64b5f6',
                                            data: [
                                                <%=unique_chart_values%>
                                            ]
                                        }]
                                });
                            </script>

                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class=" box mb-20">
                        <h4 class="boldFont basic-font mb-30">Number of Viewed Articles per Day</h4>
                        <div class="graph-holder text-center">
                            <div id="number_chart" class="chart" style="min-width: 250px; height: 180px; margin: 0 auto"></div>
                            <%
                            String number_chart_categories = "";
                            String number_chart_values = "";
                            String number_chart_data[][] = get_chart_data.contact_dashboard_number_article_views_per_day(con, filter_start, filter_end);
                            //12-Dec,y_value,12/12/2018 
                            //build categories string categories: ['12-Dec', '13-Dec', '14-Dec', '15-Dec']
                            for(int b =0; b < number_chart_data.length; b++)
                            {
                                if(b == 0)
                                {
                                    number_chart_categories = "'" + number_chart_data[b][0] + "'";
                                    number_chart_values = "{y:" + number_chart_data[b][1] + ", date:'" + number_chart_data[b][2] + "', date_range:'" + number_chart_data[b][3] + "'}";
                                }
                                else
                                {
                                    number_chart_categories = number_chart_categories + ",'" + number_chart_data[b][0] + "'";
                                    number_chart_values = number_chart_values + ",{y:" + number_chart_data[b][1] + ", date:'" + number_chart_data[b][2] + "', date_range:'" + number_chart_data[b][3] + "'}";
                                }
                            }
                            %>
                            <script>
                                Highcharts.chart('number_chart', {
                                    chart: {
                                        type: 'spline'
                                    },
                                    title: {
                                        text: null
                                    },
                                    exporting: {
                                        enabled: false
                                    },
                                    credits: {
                                        enabled: false
                                    },
                                    legend: {
                                        enabled: false
                                    },
                                    xAxis: {
                                        categories: [<%=number_chart_categories%>]
                                    },
                                    yAxis: {
                                        title: {
                                            x: -10,
                                            text: 'Number'
                                        }
                                    },

                                    plotOptions: {
                                        spline: {
                                            marker: {
                                                enabled: false
                                            }
                                        },
                                        series: {
                                            cursor: 'pointer',
                                            point: {
                                                events: {
                                                    click: function (event) {
                                                        //window.location = "tier0_self_help.jsp?date_range=" + event.point.date_range;
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    series: [{
                                            name: "Number of Viewed Articles per Day",
                                            color: '#64b5f6',
                                            data: [
                                                <%=number_chart_values%>
                                            ]
                                        }]
                                });
                            </script>

                        </div>

                    </div>
                </div>

            </div>

    <div class="clr mb-30">
        <div class="top clr mb-15">
            <h4 class="boldFont large-font pt-15">Top 10 Viewed Articles</h4>
        </div>
        <div class="sort-tbl-holder">
            <table class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Article Name</th>
                        <th>Number Of Times Viewed</th>
                        <th>Number Of Positive Feedback</th>
                        <th>Number Of Negative Feedback</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                    String article_views[][] = db.get_kb.get_top_10_view_for_timeframe(con, filter_start, filter_end);
                    if (article_views.length > 0)
                    {
                        for(int a = 0; a < article_views.length; a++)
                        {
                            %>
                            <tr>
                            <td><%=article_views[a][1]%></td>
                            <td><%=article_views[a][2]%></td>
                            <td><%=article_views[a][3]%></td>
                            <td><%=article_views[a][4]%></td>
                            </tr>
                            <%
                        }                        
                    } else {
                        %>
                        <tr><td colspan="4" class="text-center">No data available</td></tr>
                        <%
                    }
                    %>
                </tbody>            
            </table>
        </div>          
    </div>

    <div class="clr mb-30">
        <div class="top clr mb-15">
            <h4 class="boldFont large-font pt-15">Top 10 Least Helpful Articles</h4>
        </div>
        <div class="sort-tbl-holder">
            <table class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Article Name</th>
                        <th>Number Of Times Viewed</th>
                        <th>Number Of Positive Feedback</th>
                        <th>Number Of Negative Feedback</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                    String least_useful_article[][] = db.get_kb.get_top_10_least_helpful_for_timeframe(con, filter_start, filter_end);
                    if (article_views.length > 0)
                    {
                        for(int a = 0; a < least_useful_article.length; a++)
                        {
                            %>
                            <tr>
                            <td><%=least_useful_article[a][1]%></td>
                            <td><%=least_useful_article[a][2]%></td>
                            <td><%=least_useful_article[a][3]%></td>
                            <td><%=least_useful_article[a][4]%></td>
                            </tr>
                            <%
                        }
                    } else {
                        %>
                        <tr><td colspan="4" class="text-center">No data available</td></tr>
                        <%
                    }
                    %>
                </tbody>            
            </table>
        </div>          
    </div>

    <div class="clr mb-30">
        <div class="top clr mb-15">
            <h4 class="boldFont large-font pt-15">Top 10 Most Helpful Articles</h4>
        </div>
        <div class="sort-tbl-holder">
            <table class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Article Name</th>
                        <th>Number Of Times Viewed</th>
                        <th>Number Of Positive Feedback</th>
                        <th>Number Of Negative Feedback</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                    String most_useful_article[][] = db.get_kb.get_top_10_most_helpful_for_timeframe(con, filter_start, filter_end);
                    if (article_views.length > 0)
                    {
                        for(int a = 0; a < most_useful_article.length; a++)
                        {
                            %>
                            <tr>
                            <td><%=most_useful_article[a][1]%></td>
                            <td><%=most_useful_article[a][2]%></td>
                            <td><%=most_useful_article[a][3]%></td>
                            <td><%=most_useful_article[a][4]%></td>
                            </tr>
                            <%
                        }
                    } else {
                        %>
                        <tr><td colspan="4" class="text-center">No data available</td></tr>
                        <%
                    }
                    %>
                </tbody>            
            </table>
        </div>          
    </div>
    <div class="clr mb-30">
        <div class="top clr mb-15">
            <h4 class="boldFont large-font pt-15">All Articles</h4>
        </div>
        <div class="sort-tbl-holder">
            <table class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Article Name</th>
                        <th>Number Of Times Viewed</th>
                        <th>Number Of Positive Feedback</th>
                        <th>Number Of Negative Feedback</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                    String all_article_views[][] = db.get_kb.get_all_articles_view_for_timeframe(con, filter_start, filter_end);
                    if (article_views.length > 0)
                    {
                        for(int a = 0; a < all_article_views.length; a++)
                        {
                            %>
                            <tr>
                            <td><%=all_article_views[a][1]%></td>
                            <td><%=all_article_views[a][2]%></td>
                            <td><%=all_article_views[a][3]%></td>
                            <td><%=all_article_views[a][4]%></td>
                            </tr>
                            <%
                        }
                    } else {
                        %>
                        <tr><td colspan="4" class="text-center">No data available</td></tr>
                        <%
                    }
                    %>
                </tbody>            
            </table>
        </div>          
    </div>

    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN VENDOR JS-->

<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN MODERN JS-->
<!--<script src="app-assets/js/core/app-menu.js"></script>
<script src="app-assets/js/core/app.js"></script>-->
<!-- END MODERN JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
<script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
<script src="app-assets/js/scripts/popover/popover.js"></script>


<script>
    function reload_page()
    {
        var filter_date_range = document.getElementById("filter_date_range").value;
        var URL = "contact_tier0_self_help.jsp?date_range=" + filter_date_range;
        window.location.href = URL;
    }
</script>


<!-- END PAGE LEVEL JS-->

</body>
</html>
<%
            con.close();
        }//end if user has permission
        else {
            String RedirectURL = "no_permission.jsp";
            response.sendRedirect(RedirectURL);
        }
    }//end if user is logged in
%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<!--Copyright 2021 Real Data Technologies, Inc. , All rights reserved.-->
<%-- 
    Document   : problem_new.jsp
    Created on : 18-Jan-2020, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>

<%@ page import="java.time.Instant"%>
<%@ page import="java.time.format.DateTimeFormatter"%>
<%@ page import="java.time.LocalDateTime"%>
<%@ page import="java.time.ZoneId"%>
<%@ page import="java.time.ZonedDateTime"%>
<%@ page import="java.util.TimeZone"%>


    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String FINANCIAL = session.getAttribute("financial").toString();
        String PROBLEM = session.getAttribute("problem").toString();
        String REQUEST = session.getAttribute("request").toString();
        String CONTACT = session.getAttribute("contact").toString();
        String CX = session.getAttribute("cx").toString();
        String SURVEY = session.getAttribute("survey").toString();
        String SLA = session.getAttribute("sla").toString();        
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        
        
        
        if (PROBLEM.contains("create") || ADMINISTRATION.equalsIgnoreCase("true") || MANAGER.equalsIgnoreCase("true")) 
        {
            java.util.Date now = new java.util.Date();
            String context_dir = request.getServletContext().getRealPath("");
            String home_page = session.getAttribute("home_page").toString();
            LinkedHashMap props = support.config.get_config(context_dir);
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
            
            String user_tz_name = "UTC";
            String user_tz_time = "+00:00";
            String incident_id = StringUtils.defaultString(request.getParameter("incident_id"));
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "UTC";
                user_tz_time = "+00:00";
            }
            date_time_picker_format.setTimeZone(TimeZone.getTimeZone(user_tz_name));
            Connection con = null;
            try
            {
                con = db.db_util.get_contract_connection(context_dir, session); 
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);

            }
            catch(Exception e)
            {
                System.out.println("problem_new.jsp exception=" + e);
            }
    %>
    <!-- BEGIN Page Level CSS-->
    <!-- END Page Level CSS-->
    
    <form action="problem_new" method="post" enctype="multipart/form-data">
       
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Problem ID
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input name="id" id="id" type="text" readonly value="" class="form-control" disabled placeholder="Assigned on save"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Problem Time
                    </label>
                </div>
                <div class="formField clr md px-0 border-0 input-group d-flex">
                    <input type='text' id="create_date" name="create_date" value="" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Problem Found By&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="hidden" name="create_by_id" id="create_by_id"/>
                    <input type="text" name="create_by_name" id="create_by_name" value="" class="form-control" placeholder="Enter the creaters username"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Priority
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <%
                    ArrayList<String[]> priority_select = db.get_system_select_field.active_for_table_column(con, "problems", "priority");
                    %>
                    <select name="priority" id="priority" class="form-control">
                        <option value=""></option>
                        <%
                        for(int a = 0; a < priority_select.size(); a++)
                        {
                            String select_option[] = priority_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            %>
                            <option value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Status
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <%
                    ArrayList<String[]> status_select = db.get_system_select_field.active_for_table_column(con, "problems", "status");
                    %>
                    <select name="status" id="status" class="form-control">
                        <%
                         
                        for(int a = 0; a < status_select.size(); a++)
                        {
                            String select_option[] = status_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            %>
                            <option value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Final Status
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <%
                    ArrayList<String[]> final_status_select = db.get_system_select_field.active_for_table_column(con, "problems", "final_status");
                    %>
                    <select name="final_status" id="final_status" class="form-control">
                        <%
                         
                        for(int a = 0; a < final_status_select.size(); a++)
                        {
                            String select_option[] = final_status_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            %>
                            <option value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Name
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" name="name" id="name" value="" class="form-control"/> 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Description
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" name="description" id="description" value="" class="form-control"/> 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Assigned to&nbsp;
                    <i class="la la-search" style="font-size: 14px;"></i>
                    <i class="la la-question-circle" style="font-size: 14px;" title="This field is filtered by the Assigned to Group field. If Group Unassigned is selected, then all users will be available. If a specific group is selected, then only the members of that group will be available. If the Assigned to Group is changed this field will be cleared."></i>
                
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="hidden" name="assigned_to_id" id="assigned_to_id" value="0"/>
                    <input type="text" name="assigned_to_name" id="assigned_to_name" value="" class="form-control" placeholder="Enter the Assigned To group's name"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Assigned to Group&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="hidden" name="assigned_group_id" id="assigned_group_id" value="0"/>
                    <input type="text" name="assigned_group_name" id="assigned_group_name" placeholder="Group Unassigned" class="form-control" />
                </div>
            </div>
        </div>
        <div class="row"><!--Start Custom Fields-->  
            <%
            //get active custom problem fields
            ArrayList <String[]> custom_fields = db.get_custom_fields.active_for_form(con, "problem");
            /*temp[0] = rs.getString("id");
            temp[1] = rs.getString("form");
            temp[2] = rs.getString("index");
            temp[3] = rs.getString("field_name");
            temp[4] = rs.getString("field_label");
            temp[5] = rs.getString("default_text");
            temp[6] = rs.getString("number_cols");
            temp[7] = rs.getString("field_type");
            temp[8] = rs.getString("field_values");
            temp[9] = rs.getString("required");
            temp[10] = rs.getString("active");*/
            for(int a = 0; a < custom_fields.size(); a++)
            {
                String custom_field[] = custom_fields.get(a);
                if(custom_field[7].equalsIgnoreCase("select"))
                {
                    %>
                    <jsp:include page='build_custom_select_field.jsp'>
                        <jsp:param name="field_name" value="<%=custom_field[3]%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="6"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="new_record" value="true"/>
                        <jsp:param name="db_value" value=""/>
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("text"))
                {
                    %>
                    <jsp:include page='build_custom_text_field.jsp'>
                        <jsp:param name="field_name" value="<%=custom_field[3]%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="true"/>
                        <jsp:param name="db_value" value=""/>      
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("textarea"))
                {
                    %>
                    <jsp:include page='build_custom_textarea_field.jsp'>
                        <jsp:param name="field_name" value="<%=custom_field[3]%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="true"/>
                        <jsp:param name="db_value" value=""/>      
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("checkbox"))
                {
                    %>
                    <jsp:include page='build_custom_checkbox_field.jsp'>
                        <jsp:param name="field_name" value="<%=custom_field[3]%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="true"/>
                        <jsp:param name="db_value" value=""/>     
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("people-lookup"))
                {
                    %>
                    <jsp:include page='build_custom_people_field.jsp'>
                        <jsp:param name="field_name" value="<%=custom_field[3]%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="true"/>
                        <jsp:param name="db_value" value=""/>     
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("group-lookup"))
                {
                    %>
                    <jsp:include page='build_custom_group_field.jsp'>
                        <jsp:param name="field_name" value="<%=custom_field[3]%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="true"/>
                        <jsp:param name="db_value" value=""/> 
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("date"))
                {
                    %>
                    <jsp:include page='build_custom_date_field.jsp'>
                        <jsp:param name="field_name" value="<%=custom_field[3]%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="true"/>
                        <jsp:param name="db_value" value=""/>
                    </jsp:include>
                    <%
                }
            }
            %>    
        </div><!--End Custom Fields-->      
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Knowledge Article<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0 input-group d-flex">
                    <input type="hidden" name="knowledge_article_id" id="knowledge_article_id"/>
                    <input type='text' id="knowledge_search_terms" name="knowledge_search_terms" placeholder="" value="" class="form-control" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-question"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>            
        <div class="row">
            <div class="col-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Notes
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <textarea name="notes" id="notes" cols="30" rows="5" class="ckeditor">
                    </textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Attachments
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="file" name="file" multiple>
                        <label class="custom-file-label" for="file">Choose file(s)</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Associated Incidents
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" name="incident_lookup" id="incident_lookup" value="" class="form-control" placeholder="Type an ID or Description to search"/> 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="container-fluid">
                    <div class="sort-tbl-holder container-fluid">
                        <table class="table table-striped custom-sort-table rightPanelDataTable" cellspacing="0" width="100%" data-ordering='false' data-searching='false' data-length-change="false">
                            <thead>
                                <tr>
                                    <th data-data="id">Incident ID</th>
                                    <th data-data="priority">Priority</th>
                                    <th data-data="state">Status</th>
                                    <th data-data="incident_time">Incident Time</th>
                                    <th data-data="description">Description</th>
                                    <th data-data="caller_username">Caller</th>
                                    <th data-data="assigned_to_username">Assigned To</th>
                                    <th data-data="actions">Unlink</th>
                                </tr>
                            </thead>
                            <tbody>
                            <%  
                                String[] incident = {};
                                if (!incident_id.equals(""))
                                {
                                    incident = db.get_incidents.incident_by_id(con, incident_id);
                                    if (incident.length > 0 && !incident[0].equals("--"))
                                    {
                                    %>
                                    <tr data-id="<%=incident[0]%>">
                                        <td><%=incident[0]%></td>                                            
                                        <td><%=incident[14]%></td>                                            
                                        <td><%=incident[19]%></td>                                            
                                        <td><%=incident[2]%></td>                                            
                                        <td><%=incident[15]%></td>                                            
                                        <td><%=incident[32]%></td>                                            
                                        <td><%=incident[39]%></td>                                            
                                        <td><a href="javascript:void(0)" onclick="unlinkIncident('<%=incident[0]%>')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a></td>                                            
                                    </tr>
                                    <%
                                    }
                                }
                                %>
                            </tbody>
                        </table>
                            <input type="hidden" name="incident_ids" value='[<%=(incident.length > 0 && !incident[0].equals("--")? incident_id : "")%>]'>
                    </div>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-12 text-center pt-30 pb-30">
                <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                    Save
                </button>
            </div>
        </div>                                
    </form>
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>
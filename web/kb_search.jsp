<%@page import="org.apache.commons.lang3.StringUtils"%>
<!--Copyright 2020 Real Data Technologies, Inc. , All rights reserved.-->
<%-- 
    Document   : incident_new.jsp
    Created on : 18-Jan-2020, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.sql.*" %>

<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        //session vars
        boolean article_approver = false;
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String FINANCIAL = session.getAttribute("financial").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String CONTACT = session.getAttribute("contact").toString();
        String CX = session.getAttribute("cx").toString();
        String SURVEY = session.getAttribute("survey").toString();
        String SLA = session.getAttribute("sla").toString();        
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        if(ADMINISTRATION.equalsIgnoreCase("true") || MANAGER.equalsIgnoreCase("true"))
        {
            article_approver = true;
        }
        
        
        String context_dir = request.getServletContext().getRealPath("");
        String home_page = session.getAttribute("home_page").toString();
        LinkedHashMap props = support.config.get_config(context_dir);
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
        SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
        java.util.Date now = new java.util.Date();
        String now_display_format = display_format.format(now);
        String now_timestamp_format = timestamp_format.format(now);
        String now_incident_time = date_time_picker_format.format(now);
        Connection con = null;
        try
        {
            con = db.db_util.get_contract_connection(context_dir, session); 
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);            
            String embedded = StringUtils.defaultString(request.getParameter("embedded"));
            String search = StringUtils.defaultString(request.getParameter("search"));
            
    %>
<% if (embedded.equals("")) 
{
%>

    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <!-- Page specific CSS-->
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <link rel="stylesheet" type="text/css" href="assets/css/chat_app.css">
<!--
    End Page specific CSS
     BEGIN VENDOR CSS
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
     END VENDOR CSS
     BEGIN MODERN CSS
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
     END MODERN CSS
     BEGIN Page Level CSS
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
-->    
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css"><!--
    

    <style>
        div.alphabet {
            display: table;
            width: 100%;
            margin-bottom: 1em;
        }

        div.alphabet span {
            display: table-cell;
            color: #3174c7;
            cursor: pointer;
            text-align: center;
            width: 3.5%
        }

        div.alphabet span:hover {
            text-decoration: underline;
        }

        div.alphabet span.active {
            color: black;
        }
        .o-hidden {
            overflow:hidden;
        }

/*        .nav {
            transition: transform 0.4s;
            transform: translateX(50%);
            left: -50%;
        }*/

        .nav.justify-content-end {
            transform: translateX(0);
            left: 0;
        }
        .rightPanelHolderKBA.open {
            right: 0px !important;
        }
        .rightPanelHolder2.open {
            width: calc(100% - 50vw) !important;
        }
    </style>
    <!-- END Page Level CSS-->
<%
}
%>    

    <div class="clr whtbg p-10 mb-15">
        <h1 class="large-font boldFont">Knowledge Base</h1>
        <div class="mb-15">
            <div class="clr searchFieldHolder dInline formField w-100">
                <span class="icon"><img src="assets/images/svg/searchIcon.svg" alt=""></span>
                <input class="autocomplete" id="knowledge_search" type="text" dir="ltr" placeholder="Enter a space separated list of words (3+ chars) to search for">
            </div>
        </div>

    </div>

<div class="clr mb-15">
    <div class="float-left options">
        <ul id="kb_list">
<!--            <li class="dInlineBlock valign-m pl-5">
                <button data-status="open" class="btn btn-wht md customBtn dInlineBlock" style="min-width: 90px">
                    <strong>Open: <span class="status_counter" data-status="open">1</span></strong>
                </button>
            </li>
            <li class="dInlineBlock valign-m pl-5">
                <button data-status="all" class="btn btn-wht md customBtn dInlineBlock" style="min-width: 90px">
                    <strong>All: <span class="status_counter" data-status="all">2986</span></strong>
                </button>
            </li>-->
        </ul>
    </div>
</div>

    <div class="sort-tbl-holder">
        <table id="data_table_kb" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Description</th>
                </tr>
            </thead>
        </table>
    </div>

<% if (embedded.equals("")) 
{
%>
    <div class="rightPanelHolderKBA">
        <div class="p-15 ticket-content">
            <span class="rightSidePanelCloser px-2"><img src="assets/images/svg/close-icon.svg" alt=""></span>
            <span class="rightSidePanelDetacher px-2"><i class="la la-external-link la-lg" title="Detach"></i></span>
            <h4 id="rightPanelTitleKBA" class="mediumFont large-font"></h4>
            <div id="rightPanelContentKBA" class="clr whtbg p-15">
            </div>
        </div>
    </div>

    <div class="rightPanelHolder2">
        <div class="p-15 ticket-content">
            <span class="rightSidePanelCloser2"><img src="assets/images/svg/close-icon.svg" alt=""></span>
            <h4 id="rightPanelTitleChat" class="mediumFont large-font mb-20"></h4>
            <div id="chat_app" class="clr whtbg p-15">
            </div>
        </div>
    </div>

    <div class="modalPreview">
        <div class="modal fade preview" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog preview" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><!--modal_title--></h4>
                <button class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
                <!--modal_body-->
              </div>
              <div class="modal-footer">
                <button class="btn btn-primary-new" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
    </div>

    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <!--<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>-->    
    <script src="app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>    

    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/popover/popover.js"></script>
    <script type="text/javascript">
        var showChat = false;
        var currentUserId = <%=user_id%>;
        var objectType = "";
        var objectId = 0;
        var parentObjectType = "";
        var parentObjectId = 0;
        var serviceDeskEndpoint = "<%=props.get("servicedesk_url")%>";

    </script>
    <script src="assets/js/vue.min.js"></script>
    <script src="assets/js/chat_app.js"></script>

<%
}
%>

    <style>
    div.wrapper {
      position: relative;
    }
    div.scrollmenu {
      white-space: nowrap;
      overflow: hidden;
    margin-left: 20px;
    margin-right: 20px;
    }
    div.scrollmenu a {
      display: inline-block;
      text-align: center;
      padding: 14px;
      text-decoration: none;
    }
    div.scrollmenu a:hover {
    }
    .leftNav {
      display: inline-block;
      font-weight: bolder;
      cursor: pointer;
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      left: 0;
    }
    .rightNav {
      display: inline-block;
      font-weight: bolder;
      cursor: pointer;
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      right: 0.1em;
    }
    </style>    

    <script>
        var _alphabetSearch = '';
        var category_selected = 0;
        var dt_KB;

//        $.fn.dataTable.ext.search.push( function ( settings, searchData ) {
//            if ( ! _alphabetSearch ) {
//                return true;
//            }
//
//            if ( searchData[0].charAt(0) === _alphabetSearch ) {
//                return true;
//            }
//
//            return false;
//        } );

        var $this = $('.scrollmenu');
        var scrollAmount = 300;

        if (typeof moveRight == "undefined") { 
            function moveRight() {
              if ($this[0].scrollWidth - $this.scrollLeft() > $this.outerWidth()) {
                $this.scrollLeft($this.scrollLeft() + scrollAmount);

                }
            }
        }

        if (typeof moveLeft == "undefined") { 
            function moveLeft() {
              if ($this.scrollLeft() > 0) {
                $this.scrollLeft($this.scrollLeft() - scrollAmount);
              }
            }
        }

<% if (embedded.equals("")) 
{
%>
        $(document).ready(function() {
<%
}
%>

if (typeof addRightSidePanelListenersKB == "undefined") { 

    function addRightSidePanelListenersKB()
    {
        $('.rightSidePanelCloser2:not(.listens)').each(function(){
            $(this).addClass('listens');
            this.addEventListener("click", function()
            {
                window.chat_app.showChat = false;
                $('.rightPanelHolder2').removeClass('open');
                $('.rightSidePanel2').removeClass('open');
                $('html').removeClass('panelOpen');
            }, false);
        });

        $('.rightSidePanelDetacher:not(.listens)').each(function(){
            $(this).addClass('listens');
            this.addEventListener("click", function()
            {
//                console.log(this, $(this).closest(".ticket-content"));
                var article_id = $(this).closest(".rightPanelHolderKBA").data('id');
                modaldiv = $('.modalPreview').html();
                modaldiv = $(modaldiv.replace("myModal", "myModal_" + article_id));
//                modaldiv = modaldiv.replace("<!--modal_body-->", $(this).closest(".ticket-content").find(".kba-content").html());
//                modaldiv = modaldiv.replace("<!--modal_title-->", $(this).closest(".ticket-content").find("#rightPanelTitleKBA").html());
                modaldiv.find('.modal-body').html($(this).closest(".ticket-content").find(".kba-content").html());
                modaldiv.find('.modal-title').html($(this).closest(".ticket-content").find("#rightPanelTitleKBA").html());
                $('body').first().prepend(modaldiv);

                // reset modal if it isn't visible
                if (!($('.modal.in').length)) {
                  $('#myModal_' + article_id + ' .modal-dialog').css({
                    top: '50%',
                    left: '40%',

                  });
                }
//                console.log($('#myModal_' + article_id), article_id);
                $('#myModal_' + article_id).modal({
                  backdrop: false,
                  show: true
                });

                $('.modal-dialog').draggable({
                  handle: ".modal-header"
                });

            }, false);
        });

        $('.rightSidePanelOpener2:not(.listens)').each(function(){
            $(this).addClass('listens');
            
            this.addEventListener("click", function()
            {
                $('.rightPanelHolder2').addClass('open');
                $('.rightSidePanel2').addClass('open');
                $('html').addClass('panelOpen');
//                console.log($(this).data('articleId'));
                window.chat_app.objectType = $(this).data('target');
                window.chat_app.objectId = $(this).data('id');
                window.chat_app.parentObjectType = "";
                window.chat_app.parentObjectId = 0;
                window.chat_app.currentUserId = <%=user_id%>;
                window.chat_app.showChat = true;
//                console.log("panel2 opened");
            });
        });
        
        
        $('.rightPanelHolderKBA .rightSidePanelCloser:not(.listens)').each(function(){
            $(this).addClass('listens');
            this.addEventListener("click", function()
            {
                $('.rightPanelHolderKBA').removeClass('open');
                $('.rightSidePanelKBA').removeClass('open');
                $('html').removeClass('panelOpen');
            }, false);
        });

        $('.rightSidePanelOpenerKBA:not(.listens)').each(function(){
            $(this).addClass('listens');
            
            this.addEventListener("click", function()
            {
                $("#rightPanelContentKBA").html("");

                var panel_title = "";
                var ajax = {};
                ajax.type = 'GET';
                ajax.dataType = "html";

                var target = $(event.target).closest(".rightSidePanelOpenerKBA").data("target");
                var KBA_id = $(event.target).closest(".rightSidePanelOpenerKBA").data("id");

                switch (target) 
                {
                    case "KBA":
                        ajax.url = "kb_view.jsp";
                        ajax.data = {id: KBA_id};
                        panel_title = "Knowledge Article";
                        break;
                    default:
                        ajax.url = "";
                        break;
                }

                if (ajax.url === "") return false;

                ajax.error = function(jqXHR, textStatus)
                {
                    switch (jqXHR.status) {
                        case 401:
                            window.location.assign("<%=props.get("login_url").toString()%>");
                            break;
                        case 403:
                            alert("No permission!");
                            break;
                        default:
                            alert("Error processing request!");
                    }
                };

                ajax.success = function (data) 
                {                    
                    $("#rightPanelTitleKBA").html(panel_title);
                    $("#rightPanelContentKBA").html(data);
                    $(".rightPanelHolderKBA").data("id", KBA_id);
                    $('.rightPanelHolderKBA .rightSidePanelCloser').on('click', function(){
                        $('.rightPanelHolderKBA').removeClass('open');
                        $('.rightSidePanelKBA').removeClass('open');
                        $('html').removeClass('panelOpen');
                        ajax_id = "";
                        ajax_type = "";
                    });
                    $('.rightPanelHolderKBA').addClass('open');
                    $('.rightSidePanelKBA').addClass('open');
                    $('html').addClass('panelOpen');

//                    attachFieldListeners($("#rightPanelContent"));
                    addRightSidePanelListenersKB();
                }

                $.ajax(ajax);

            });
        });
        
    }
}
            if (!$.fn.DataTable.isDataTable( dt_KB )) 
            {
                dt_KB = $('#data_table_kb').DataTable({
                    language: {
                        "info":           "Showing _START_ to _END_ of _TOTAL_ articles",
                        "infoEmpty":      "",
                        "infoFiltered":   "(filtered from _MAX_ total articles)",
                        "lengthMenu":     "Show _MENU_ articles",
                        "zeroRecords":    "No matching articles found",
                    },
                    autoWidth: false,
                    "processing": true,
                    "serverSide": true,
                    "ordering": true,
                    'serverMethod': 'post',
                    'searching': false,
                    "lengthChange": false,
                    dom: "<'row'<'col-sm-3'i><'col-sm-3'f><'col-sm-6'p>>" +
                        "<'row'<'col-sm-12'tr>>" +
                        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                    "ajax": {
                        "url": "ajax_lookup_kb",
                        'data': function(data){
                            // Read values
                            if (category_selected != 0) {
                                data['category'] = category_selected;
                            }
                            var search = $("#knowledge_search").val();
                            if (search != "") {
                                data['search'] = search;
                            }
                        }
                    },
                    "pageLength": 50,
                    "order": [],
                    "columnDefs": [
        //                <th></th>
        //                <th>ID</th>
        //                <th>Priority</th>
        //                <th>Status</th>
        //                <th>Final Status</th>
        //                <th>Problem Time</th>
        //                <th>Name</th>
        //                <th>Description</th>
        //                <th>Incidents</th>
        //                <th>Assigned To</th>
        //                <th>Assigned Group</th>
        //                <th>Actions</th>

                        { targets: 0, "width": "30%", data: null, orderable: false, render: function (data, type, row, meta) { return row.article_name;} },
                        { targets: 1, "width": "70%", "data": null, orderable: false,
                            render: function ( data, type, row ) 
                            {
                                return row.article_description;
                            }
                        },

                    ],
                    createdRow: function( row, data, dataIndex ) {
                        $( row )
                            .attr('data-id', data.id)
                            .attr('data-target', "KBA")
                            .addClass('rightSidePanelOpenerKBA')
                            .addClass('pointer')
                    },
                    drawCallback: function( settings ) {
                        var api = this.api();
        //                if ($(api.table().node()).hasClass("d-none"))
        //                {
        //                    var dt_child_response = api.ajax.json();
        //                    $("span.status_counter_" + problem_id).each(function() {
        //                        var status = $(this).attr('data-status')
        //                        if (dt_child_response.statusCounters[status])
        //                        {
        //                            $(this).html(dt_child_response.statusCounters[status]);
        //                        } else {
        //                            $(this).html(0);
        //                        }
        //                    });
        //                    $(api.table().node()).removeClass("d-none");
        //                }
        //
        //                addRightSidePanelListeners();
                        // Output the data for the visible rows to the browser's console
//                        console.log( "drawCallback fired", settings);
                        $(api.table().node()).removeClass("d-none");

                        var dt_response = api.ajax.json();
                        var categories_block = "";


                        dt_response.categories.forEach(function(category) {
                            categories_block += '' +
                                '<li class="dInlineBlock valign-m pl-5" data-id="' + category.id + '" onclick="selectCategory(\'' + category.id + '\')">' +
                                    '<button data-status="new" class="btn ' + (category_selected == category.id ? "btn-primary-new" : "btn-wht") + ' md customBtn dInlineBlock" style="min-width: 90px">' +
                                        '<strong>' + category.name + '<span class="kb_counter" data-status="new">: ' + category.count + '</span></strong>' +
                                    '</button>' +
                                '</li>';
                        });

                        $("#kb_list").html(categories_block);
//                        refresh_categories();
//                        $(".scrollmenu").find(".nav-link").each(function() {
//                            let nav_link = $(this);
//                            nav_link.on("click", function() {
//                                category_selected = $(this).data("id");
//                                dt_KB.draw();
//                            });
//                        });

                        addRightSidePanelListenersKB();

                    },
                    "stripeClasses":[]            
                });
            }

//            var alphabet = 
//            '<div class="wrapper whtbg px-10 mb-10">' +
//                '<div class="leftNav d-none"><i class="la la-arrow-left"></i></div>' +
//                    '<div class="scrollmenu">' +
//                    '</div>' +
//                '<div class="rightNav d-none"><i class="la la-arrow-right"></i></div>' +
//            '</div>';  
//
//            $(alphabet).insertBefore( dt_KB.table().container() );
//
//            $(alphabet).on( 'click', 'span', function () {
//                alphabet.find( '.active' ).removeClass( 'active' );
//                $(this).addClass( 'active' );
//
//                _alphabetSearch = $(this).data('letter');
//                dt_KB.draw();
//            } );
//            
//            $('.leftNav').click(moveLeft);
//            $('.rightNav').click(moveRight);
//            $this = $('.scrollmenu');

            $('input#knowledge_search').keyup( function() {
                var search = this.value.split(" ");
                var do_search = true;
                search.forEach(function(word) {
                    if (word.length < 3) {
                        do_search = false;
                    }
                });

                if (do_search || search == "") {
                    dt_KB.draw();
                }
            });
            addRightSidePanelListenersKB();
            
            <% if (!search.equals("")) 
            {
            %>            
                $('input#knowledge_search').val("<%=search%>");
                dt_KB.draw();
            <%
            }
            %>



<% if (embedded.equals("")) 
{
%>            
        } );
<%
}
%>
        if (typeof selectCategory == "undefined") {  
            function selectCategory(category_id) {
                category_selected = category_id;
                dt_KB.draw();                    
            }
        };

        if (typeof articleHelpfull == "undefined") {  
            function articleHelpfull(id, useful) {
                var status_counter = $(event.target).closest("button").find(".useful_counter");
                $.ajax({url:"kb_instant_feedback?id=" + id + "&useful=" + useful, processData: false})
                .done(function() {
                    status_counter.html(parseInt(status_counter.html()) + 1);
                });
            }
        }

//        if (typeof refresh_categories == "undefined") {  
//            function refresh_categories() {
//                $(".scrollmenu").find(".nav-link").each(function() {
//                    let nav_link = $(this);
//                    if (nav_link.data('id') == category_selected) {
//                        nav_link.addClass("font-weight-bold");
//                    } else {
//                        nav_link.removeClass("font-weight-bold");
//                    }
//                });
//            }
//        }


                
    </script>
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%      }
        catch(Exception e)
        {
            System.out.println("kb_search.jsp exception=" + e);
        }
    }//end if not logged in
%>
<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_users_add.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        boolean authorized = support.role.authorized(session, "asset", "create");
        if (!authorized) {
            response.sendRedirect("no_permission.jsp");
        } 
        else 
        {
            Logger logger = LogManager.getLogger();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try 
            {
                String home_page = session.getAttribute("home_page").toString();
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
%>
<!-- BEGIN PAGE LEVEL CSS-->
<!-- END PAGE LEVEL CSS-->
<h4 class="mediumFont medium-font mb-20">Asset Attributes</h4>
<form action="assets_new" name="in_form" id="in_form" onsubmit="return validateForm()">
    <div class="row">
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Asset Tag
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="asset_tag" name="asset_tag" class="form-control" placeholder=""/>
            </div>
        </div>
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Serial #
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="serial_number" name="serial_number" class="form-control" placeholder="Enter Serial #"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Manufacturer
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="manufacturer" name="manufacturer" class="form-control" placeholder=""/>
            </div>
        </div>
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Model
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="model" name="model" class="form-control" placeholder=""/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Type
                </label>
            </div>
            <div class="formField clr md px-0 border-0">    
                <input type="hidden" name="asset_type" id="asset_type" value=""/>
                <select class="form-control" name="asset_type_id" id="asset_type_id" onchange="update_subtype()">
                    <option value=""></option>
                    <%
                        ArrayList<String[]> all_active_types = db.get_assets.all_active_types(con);
                        for (int a = 0; a < all_active_types.size(); a++) {
                            String type[] = all_active_types.get(a);
                    %>
                    <option value="<%=type[0]%>"><%=type[1]%></option>
                    <%
                        }
                    %>
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Subtype
                </label>
            </div>
            <div class="formField clr md px-0 border-0">                                                
                <select class="form-control" name="asset_subtype" id="asset_subtype">
                    <option value=""></option>
                </select>
            </div>
        </div> 
    </div>
    <div class="separator-new mb-30 mt-30"></div>
    <h4 class="mediumFont medium-font mb-20">Purchase/Warranty/State/Assignment</h4>
    <div class="row">
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Purchase Order
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="purchase_order" name="purchase_order" class="form-control" placeholder=""/>
            </div>
        </div>
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Warranty Expire
                </label>
            </div>
            <div class='formField clr md px-0 border-0 input-group d-flex'>                                                
                <input type='text' id="warranty_expire" name="warranty_expire" value="" class="form-control datetime" />
                <div class="input-group-append">
                    <span class="input-group-text">
                        <span class="la la-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    State
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <select class="form-control" name="state" id="state">
                    <option value=""></option>
                    <option value="Ordered">Ordered</option>
                    <option value="New">New</option>
                    <option value="Delivered">Delivered</option>
                    <option value="Active">Active</option>
                    <option value="Spare">Spare</option>
                    <option value="Broken">Broken</option>
                    <option value="Retired">Retired</option>
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    State Date
                </label>
            </div>
            <div class='formField clr md px-0 border-0 input-group d-flex'>
                <input type='text' id="state_date" name="state_date" value="" class="form-control datetime" />
                <div class="input-group-append">
                    <span class="input-group-text">
                        <span class="la la-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Assigned to Group&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="hidden" name="assigned_group_id" id="assigned_group_id" value="0"/>
                <input type="text" name="assigned_group_name" id="assigned_group_name" value="" class="form-control" />
            </div>
        </div>
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    
                Assigned to&nbsp;
                <i class="la la-search" style="font-size: 14px;"></i>
            
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="hidden" name="assigned_to_id" id="assigned_to_id" value="0"/>
                <input type="text" name="assigned_to_name" id="assigned_to_name" value="" class="form-control" placeholder="Enter the Assigned To username"/>
            </div>
        </div>
    </div>
    <div class="separator-new mb-30 mt-30"></div>
    <h4 class="mediumFont medium-font mb-20">Location</h4>
    <div class="row">
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Department
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="department" name="department" class="form-control" placeholder=""/>
            </div>
        </div>
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Location
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="location" name="location" class="form-control" placeholder=""/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Site
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="site" name="site" class="form-control" placeholder=""/>
            </div>
        </div>
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Company
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="company" name="company" class="form-control" placeholder=""/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    IP
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="ip" name="ip" class="form-control" placeholder=""/>
            </div>
        </div>
    </div>
    <div class="separator-new mb-30 mt-30"></div>
    <h4 class="mediumFont medium-font mb-20">Notes/Log</h4>
    <div class="row">
        <div class="col-md-12">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Notes
                </label>
            </div>
            <div class="formField clr md mb-15 ">
                <textarea class="form-control no-border p-0" rows="4" id="notes" name="notes"></textarea>
            </div>
        </div>
    </div>      
    <div class="row ">
        <div class="col-md-12 text-center pt-30 pb-30">
            <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
            <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                Save
            </button>
        </div>
    </div>
</form>
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->   
<%                con.close();

            } catch (Exception e) {
                System.out.println("Exception in assets_new.jsp: " + e);
                logger.debug("ERROR: Exception in assets_new.jsp:" + e);
            }
        } //end if not admin permission
    } //end if not redirected
%>
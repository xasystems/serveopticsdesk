<%-- 
    Document   : project_list.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="org.apache.commons.codec.digest.HmacAlgorithms"%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.time.ZonedDateTime"%>
<%@ page import="java.time.format.DateTimeFormatter"%>
<%@ page import="org.apache.commons.codec.digest.HmacUtils"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        boolean page_authorized = support.role.authorized(session, "project","read");  
        String user_id = "0";
        try
        {
            user_id = session.getAttribute("user_id").toString(); 
        }
        catch(Exception e)
        {
            user_id = "0";
        }
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            String user_tz_name = "UTC";
            String user_tz_time = "+00:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "UTC";
                user_tz_time = "+00:00";
            }
            boolean project_delete = false;
            boolean project_update = false;
            String date_range = "";
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020            
            String action = StringUtils.defaultString(request.getParameter("action"));
            String predefined = StringUtils.defaultString(request.getParameter("predefined"));
            String project_id = StringUtils.defaultString(request.getParameter("project_id"));
            String task_id = StringUtils.defaultString(request.getParameter("task_id"));

            date_time_picker_format.setTimeZone(TimeZone.getTimeZone(user_tz_name));
            java.util.Date now = new java.util.Date();
            String now_picker_time = date_time_picker_format.format(now);
            String timestamp_string = timestamp_format.format(now);
            
            String status = request.getParameter("status");
            if(status == null || status.equals(""))
            {
                status = "all";
            }
            ArrayList<String[]> projects = new ArrayList();

            if(session.getAttribute("project").toString().equalsIgnoreCase("delete") || session.getAttribute("administration").toString().equalsIgnoreCase("true") || session.getAttribute("manager").toString().equalsIgnoreCase("true"))
            {
                project_delete = true;
                project_update = true;
            }
            else if(session.getAttribute("project").toString().equalsIgnoreCase("update"))
            {
                project_delete = false;
                project_update = true;
            }

            String couchdb_username = timestamp_string;
//            String couchdb_token = HmacUtils(HmacAlgorithms.HMAC_SHA_1, couchdb_username).hmacHex(couchdb_username);
            String couchdb_secret = props.get("couchdb.secret").toString();
            String couchdb_token = HmacUtils.hmacSha1Hex(couchdb_secret, couchdb_username);
            ArrayList<String[]> status_select = db.get_system_select_field.active_for_table_column(con, "projects", "status");
            boolean is_admin = false;
            if (session.getAttribute("administration").toString().equalsIgnoreCase("true") || session.getAttribute("manager").toString().equalsIgnoreCase("true")) 
            {
                is_admin = true;
            }
                                              
    %>
<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>


<jsp:include page='menu_service_desk.jsp'>
    <jsp:param name="active_menu" value=""/>
</jsp:include>	

<!-- BEGIN PAGE LEVEL CSS-->
<link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
<!-- END VENDOR CSS-->
<!-- BEGIN Page Level CSS-->
<!--<link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
-->
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
<!--<link rel="stylesheet" type="text/css" href="assets/css/dashboard.css">-->
<link rel="stylesheet" type="text/css" href="assets/css/chat_app.css">

<!--End Page level css-->



                            
                            <div class="top mb-15" v-if="selectedItem" v-cloak>
                                <div class="float-right">
<!--                                    <a class="btn btn-wht customBtn md waves-effect">
                                        <img src="assets/images/svg/checked-icon.svg" alt="">
                                    </a>
                                    <a class="btn btn-wht customBtn md waves-effect">
                                        <img src="assets/images/svg/trash-icon-blk.svg" alt="">
                                    </a>
                                    <a class="btn btn-wht customBtn md waves-effect">
                                        <img src="assets/images/svg/tag-icon.svg" alt="">
                                    </a>-->
                                    <div class="dInlineBlock clr menuHolder">
                                        <a class="btn btn-wht customBtn md dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                            <img src="assets/images/svg/menu-hamburger-icon.svg" alt="">
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" >
                                            
                                            <a class="dropdown-item" href="#" v-on:click="openKBPanel(selectedItem.description)">
                                                <span class="icon mr-5"><img src="assets/images/svg/searchIcon.svg" alt=""></span>KB
                                            </a>

                                            <a class="dropdown-item" href="#" v-on:click="copyObject(selectedItem.objectType, selectedItem.id)" v-if="selectedItem.objectType == 'Incident'">
                                                <i class="la la-copy pr-1"></i>Copy
                                            </a>
                                            
                                            <a class="dropdown-item" href="#" v-on:click="openRightPanel(selectedItem.objectType, selectedItem.id)">
                                                <span class="icon mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></span>Edit
                                            </a>
                                            <% if (is_admin) 
                                            {
                                            %>
                                                <a class="dropdown-item" href="#" v-on:click="showDeleteModal(selectedItem.objectType, selectedItem.id)">
                                                    <span class="icon mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></span>Delete
                                                </a>
                                            <%
                                            }
                                            %>

                                            <a class="dropdown-item" href="#">
                                                <span class="icon mr-5"><img src="assets/images/svg/print-icon.svg" alt=""></span>Print
                                            </a>
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="float-left">
                                    <span class="badge btn-wht btn customBtn md mr-5">Ticket No. {{ selectedItem.id }}</span>
                                    <span :class="'state-' + selectedItem.state.toLowerCase() + ' btn customBtn md'">{{ selectedItem.state }}</span>
                                    <span v-if="selectedItem.tags && selectedItem.tags['ecommerce']" class="badge badge-secondary btn customBtn md mr-5 basefontcolor"><span class="icon mr-5"><img src="assets/images/svg/tag-icon-secondary.svg"></span>Ecommerce</span>
                                    <span v-if="selectedItem.tags && selectedItem.tags['billing']" class="badge badge-warning btn customBtn md mr-5"><span class="icon mr-5"><img src="assets/images/svg/tag-icon-warning.svg"></span>Billing</span>
                                </div>
                            </div>

                            <h4 class="title xxl-font has_icon boldFont mb-15" v-if="selectedItem" v-cloak>
                                <span v-if="selectedItem.vip && selectedItem.vip == 'true'" class="icon"><img src="assets/images/svg/crown-icon.svg" alt=""></span>
                                <span class="txt">{{ selectedItem.description }}</span>
                            </h4>
                            
                            <div class="meta-listing pb-15 mb-15" v-if="selectedItem" v-cloak>
                                <ul>
                                    <li class="clr">
                                        {{ selectedItem.createDate}}
                                    </li>
                                    <li class="clr">
                                        <strong class="">State:</strong> <span class="bostonBlue">{{ selectedItem.state }}</span>
                                    </li>
                                    <li class="clr" v-if="selectedItem.category && selectedItem.subCategory">
                                        <strong class="">Category:</strong> <span class="bostonBlue"><a href="#">{{ selectedItem.category }}</a> > {{ selectedItem.subCategory }}</span>
                                    </li>
                                    <li class="clr" v-if="selectedItem.assignedTo">
                                        <strong class="">Assigned to:</strong> <span class="bostonBlue"><span class="user-img dInlineBlock mr-5 user-avatar"><img src="assets/images/users/avatar-2.jpg" alt=""></span>{{ selectedItem.assignedTo }}</span>
                                    </li>
                                    <li class="clr" v-if="selectedItem.createdBy">
                                        <strong class="">Created By:</strong> <span class="bostonBlue"><span class="user-img dInlineBlock mr-5 sandy-brown-bg">MW</span>{{ selectedItem.createdBy }}</span>
                                    </li>
                                    <li class="clr" v-if="selectedItem.caller">
                                        <strong class="">Customer Name:</strong> <span class="bostonBlue"><span class="user-img dInlineBlock mr-5 portage-bg">JL</span>{{ selectedItem.caller }}</span>
                                    </li>
                                </ul>
                            </div>

                            <div class="clr comments">
                                <div class="comment clr mb-10" v-if="1 == 0">
                                    <div class="txt float-right">
                                        <p class="basic-font">{{ selectedItem.notes }}</p>
                                    </div>
                                </div>

                                <div class="comment clr mb-10">
                                    <chat-app 
                                        current-user-id="<%=user_id%>"
                                        show-chat="false"
                                        service-desk-endpoint="<%=props.get("servicedesk_url").toString()%>"
                                        ref="chatApp"
                                        no-focus-textarea = "true"
                                        >
                                    </chat-app>
<!--                                    <template>
                                      <vue-advanced-chat/>
                                    </template>-->
                                </div>
                            </div>


<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteModalLabel">Delete Confirmation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="deleteModalBody">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger" v-on:click="deleteEntity" id="modalDeleteButton">Delete</button>
        </div>
      </div>
    </div>
</div>                     

<div class="modalPreview">
    <div class="modal fade preview" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog preview" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"><!--modal_title--></h4>
            <button class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <!--modal_body-->
          </div>
          <div class="modal-footer">
            <button class="btn btn-primary-new" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
</div>

<div class="rightPanelHolder">
    <div class="p-15 ticket-content">
        <span class="rightSidePanelCloser" v-on:click="closeRightPanel"><img src="assets/images/svg/close-icon.svg" alt=""></span>
        <h4 id="rightPanelTitle" class="mediumFont large-font mb-20"></h4>
        <div id="rightPanelContent" class="clr whtbg p-15">
        </div>
    </div>
</div>

<div class="rightPanelHolderKB">
    <div class="p-15 ticket-content">
        <span class="rightSidePanelCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
        <h4 id="rightPanelTitleKB" class="mediumFont large-font mb-20"></h4>
        <div id="rightPanelContentKB" class="clr whtbg p-15">
        </div>
    </div>
</div>

<div class="rightPanelHolderKBA">
    <div class="p-15 ticket-content">
        <span class="rightSidePanelCloser px-2"><img src="assets/images/svg/close-icon.svg" alt=""></span>
        <span class="rightSidePanelDetacher px-2"><img src="assets/images/svg/up-arrow.svg" alt="" title="Detach"></span>
        <h4 id="rightPanelTitleKBA" class="mediumFont large-font mb-20"></h4>
        <div id="rightPanelContentKBA" class="clr whtbg p-15">
        </div>
    </div>
</div>

<jsp:include page='footer.jsp'>
    <jsp:param name="parameter" value=""/>
</jsp:include>	


<style>
    .vac-room-header {
      display: none !important;
    }
    .vac-col-messages .vac-container-scroll {
      margin-top: 0px;
    }
    .dropdown-menu.md {
        width: 500px;
    }
    .dropdown-menu input {
        width: auto !important;
    }
</style>


<!-- BEGIN PAGE LEVEL JS-->
<!-- BEGIN VENDOR JS-->    
<script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!--<script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
--><script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<script src="assets/js/tinymce/tinymce.min.js"></script>
<script src="assets/js/bs-custom-file-input.min.js"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN MODERN JS-->
<!--<script src="app-assets/js/core/app-menu.js"></script>
<script src="app-assets/js/core/app.js"></script>-->
<!-- END MODERN JS-->
<!-- BEGIN PAGE LEVEL JS-->
<!--<script src="app-assets/js/scripts/popover/popover.js"></script>-->

<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>    
<script src="app-assets/js/scripts/tables/datatables-extensions/dataRender/datetime.js"></script>    
<!--

<!-- development version, includes helpful console warnings -->
<script src="assets/js/vue.min.js"></script>
<script src="assets/js/chat_app.js"></script>

<script src="assets/js/axios.min.js"></script>
<script src="assets/js/dayjs.min.js"></script>
<script src="assets/plugins/dayjs/isToday.js"></script>
<script src="assets/plugins/dayjs/isYesterday.js"></script>
<script src="assets/plugins/dayjs/customParseFormat.js"></script>
<!--<script src="https://cdn.jsdelivr.net/npm/vue-advanced-chat@0.8.8/dist/vue-advanced-chat.min.js"></script>-->
<script>dayjs.extend(window.dayjs_plugin_isToday)</script>
<script>dayjs.extend(window.dayjs_plugin_isYesterday)</script>
<script>dayjs.extend(window.dayjs_plugin_customParseFormat)</script>


<!-- production version, optimized for size and speed -->
<!--<script src="https://cdn.jsdelivr.net/npm/vue@2"></script>-->

<script>

    $.holdReady(true); // holding jQuery ready firing until Vue mounted because Vie replacing all attached listeners
    var showChat = false;
    var currentUserId = <%=user_id%>;
    var objectType = "";
    var objectId = 0;
    var parentObjectType = "";
    var parentObjectId = 0;
    var serviceDeskEndpoint = "<%=props.get("servicedesk_url").toString()%>";
//    window.chat_app.showChat = true;
    var collaboration_vue_app;
    
    window.addEventListener("load", (event) => {

        $(document).on("click", '#save_button', function(event) { 
            alert("new link clicked!");
        });
        
        collaboration_vue_app = new Vue({
            el: '#wrapper',
//            components: { 'chat-app': window['chatApp'] },
            data: {
//                rooms: [],
//                messages: [],
//                currentUserId: 1234,
                searchPattern: "",
                selectedFolder: "",
                selectedSubfolder: "",
                folders: {},
                items: {},
                start: 0,
                length: 50,
                view: "team",
                slimScrollOptions: {
                    height: "auto",
                    position: "right",
                    size: "10px",
                    color: "#9ea5ab"                    
                },
                selectedItem: null,
                itemsLoading: false
            },
            methods: {
                showDeleteModal: function(entity_type, delete_entity_id) {
//                    console.log(entity_type, delete_entity_id);
                    switch (entity_type) {
                        case "Problem":
                            $("#deleteModalBody").html("Are you sure you want to delete this Problem?");
                            break;
                        case "Incident":
                            $("#deleteModalBody").html("Are you sure you want to delete this Incident?");
                            break;
                        case "Request":
                            $("#deleteModalBody").html("Are you sure you want to delete this Request?");
                            break;
                        default:
                            break;
                    }
                    $("#modalDeleteButton").prop("data-id", delete_entity_id);
                    $("#modalDeleteButton").prop("data-entity", entity_type);
                    $('#deleteModal').modal();
                },
                deleteEntity: async function() {
                    let entity_type = $("#modalDeleteButton").prop("data-entity");
                    let url = null;
                    switch (entity_type) {
                        case "Problem":
                            url = "problem_delete";
                            break;
                        case "Incident":
                            url = "incident_delete";
                            break;
                        case "Request":
                            url = "request_delete";
                            break;
                        default:
                            break;
                    }
                    let delete_entity_id = $("#modalDeleteButton").prop("data-id");
                    if (url && delete_entity_id) {
                        const self = this;
                        await axios.get(url + "?id=" + delete_entity_id)
                            .then(function (response) {
                                $('#deleteModal').modal('hide');
                                self.doFolderItems(self.selectedFolder, self.selectedSubfolder);
                            })
                            .catch(function (error) {
                                if (error.response) {
                                    console.log("Error code:" + error.response.status);
                                    $("#deleteModalBody").html('<div class="text-danger font-weight-bold">Something went wrong!</div>');
                                }
                            });
                    }
                },
                getFolders: function() {
                    this.start = 0;
                    axios
                        .get('ajax_lookup_collaboration_folders?view=' + this.view)
                        .then(response => {
                            this.folders = response.data;
                            this.doFolderItems("Incident");
                        });
                },
                resetFilters: function(event) {
//                    console.log(event.target);
                    $(event.target).parents('form').trigger('reset');
                    $(event.target).parents('form').find("input[type='hidden']").val('');
                    change_filter_category($('#filter_category_select'));
                    this.doFolderItems(this.selectedFolder, this.selectedSubfolder);
                },
                applyFilters: function() {
                    $('#filters_dropdown_toggle').dropdown('hide');
                    this.doFolderItems(this.selectedFolder, this.selectedSubfolder);
                },
                doFolderItems: function(folder, subfolder = "") {
                    this.itemsLoading = true;
                    this.start = 0;
                    this.selectedFolder = folder;
                    this.items = {};
                    this.fetchMoreItems(folder, subfolder)
                    .then(items => {
                        // console.log(items, Object.keys(items).length === 0);
                        if (Object.keys(items).length !== 0) {
        //                    console.log("after fetchmore", items);
                            this.items = items;

                            Object.entries(items).some(([key, item]) => {
        //                        console.log(item.content[0]);
                                this.selectedItem = item.content[0];
                                return true; // fetching only the first item
                            });

                            window.jQuery.MainApp.initSlimscroll();
                            this.$nextTick(function () {
                                // Code that will run only after the
                                // entire view has been rendered
                                var sa = $("#sidebar3");
                                const self = this;
                                sa.bind('slimscroll', function(e, pos){
                                    if (pos == "bottom") {
                                        self.start = self.start + self.length;
            //                            console.log("reached bottom", folder,subfolder, self.items.length);
                                        self.fetchMoreItems(folder, subfolder).then(items => {
//                                            console.log("reached bottom fetchmore", folder, subfolder);
                                            self.items = {...self.items, ...items};
                                            self.$nextTick(function () {
            //                                    window.jQuery.MainApp.initSlimscroll();
                                            });
                                        });
                                    }
                                });

                            });
                        }
                        this.itemsLoading = false;
                    });
                },
                fetchMoreItems: async function(folder, subfolder = "") {
                    try {

                        var url;
                        var items = {};
                        if (folder == "Incident") {
                            url = "ajax_lookup_incidents";
                        } else if (folder == "Request") {
                            url = "ajax_lookup_requests";                    
                        } else {
                            url = "";
                        }
                        if (subfolder == "All") {
                            subfolder = "";
                        }
                        if (url == "") return items;
    //                    console.log("inside fetchmoreimtes", folder, subfolder, url);
                        const params = new URLSearchParams();
                        params.append('columns[0][data]', 0);
                        params.append('columns[0][name]', 'create_date');
                        params.append('order[0][column]', 0);
                        params.append('order[0][dir]', 'desc');
                        params.append('draw', 0);
                        params.append('start', this.start);
                        params.append('length', this.length);
                        if (subfolder != "" ) {
                            params.append('state', subfolder);
                        }
                        if (this.view == "my" ) {
                            params.append('predefined', "assigned_to_me");
                        }
                        if (this.searchPattern != "" ) {
                            params.append("description", this.searchPattern);
                        }
                        
                        $("#filter_form input,#filter_form select").each(function() {
                            if ($(this).attr("name") && (($(this).attr("type") == "radio" && $(this).is(':checked')) || ($(this).attr("type") != "radio" && this.value && this.value != "" && this.value.toLowerCase() != "any" && this.value.toLowerCase() != "all" && this.value != 0)))
                            {
                                // data[$(this).attr('name')] = this.value;
                                params.append($(this).attr('name'), this.value);
                            }
                        });

                        const config = {
                          headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                          }
                        }

                        let response = await axios.post(url, params, config);
                        const rawItems = response.data.data;
                        var id = 0;

                        for (var item of rawItems) {
//                            if (item.id == "114907") {
//                                item.create_date = "2022-01-29 23:26:00";
//                            }
//                            if (item.id == "114906") {
//                                item.create_date = "2022-01-28 23:26:00";
//                            }

                            var createDate = dayjs(item.create_date, 'YYYY-MM-DD HH:mm:ss', true);
                            var dateGroup = createDate.format('ddd, MMM D');
                            if (createDate.isToday()) {
                                dateGroup = "Today";
                            } else if (createDate.isYesterday()) {
                                dateGroup = "Yesterday";
                            }
                            if (!items.hasOwnProperty(dateGroup)) {
                                items[dateGroup] = {};
                                items[dateGroup]["id"] = this.start + '_' + id;
                                items[dateGroup]["content"] = [];
                                id++;
                            }
                            items[dateGroup]["content"].push({
                                id: item.id,
                                objectType: folder,
                                vip: item.caller_vip,
                                time: createDate.format('h:m A'),
                                createDate: createDate.format('MM/DD/YYYY h:m A'),
                                state: item.state,
                                priority: item.priority,
                                description: item.description,
                                notes: item.notes,
                                category: item.category,
                                subCategory: item.subcategory,
                                assignedTo: item.assigned_to_first || item.assigned_to_last ? item.assigned_to_first + ' ' + item.assigned_to_last : '',
                                createdBy: item.created_by_first || item.created_by_last ? item.created_by_first + ' ' + item.created_by_last : '',
                                caller: item.caller_first || item.caller_last ? item.caller_first + ' ' + item.caller_last : '',
                            });
    //                        console.log(item.create_date, Intl.DateTimeFormat(navigator.language, { weekday: 'long', month: 'short', day: 'numeric' }).format(new Date(item.create_date)), dayjs(item.create_date, 'YYYY-MM-DD HH:mm:ss', true) );
    //                        break;
                        }
    //                    console.log(this.start)
    //                    console.log("inside fetchmoreimtes before return", items);
                        
                        return items;
                    }
                    catch (err) {
                        console.error(err);
                    }                
                },
                filterByName: function(event) {
                    // console.log(event.target.value)
                    if (event.target.value == "") {
                        $(event.target).prev("input[type=hidden]").val("");
                    }
                },
                search: function(event) {
                    const value = event.target.value;
                    this.searchPattern = value;
                    if (value.trim().length > 2 || value.trim().length == 0) {
//                        console.log("searchPattern value: " + this.searchPattern);
                        this.doFolderItems(this.selectedFolder, this.selectedSubfolder);
                    }
                },
                slideToggle: function(event) {
//                    console.log(event, window.jQuery(event.target).parents(".has-sub-menu").children("ul"));
                    window.jQuery(event.target).parents(".has-sub-menu").children("ul").slideToggle("100");
                },
                closeRightPanel: function() {
                    $('.rightPanelHolder').removeClass('open');
                    $('.rightSidePanel').removeClass('open');
                    $('html').removeClass('panelOpen');
                },
                copyObject: async function(target, id) {
                    const self = this;
                    var url = "";
                    switch (target) 
                    {
                        case "Problem":
                            break;
                        case "Incident":
                            if (id)
                            {
                                url = "incident_new";
                            }
                            break;
                        case "Request":
                            break;
                        default:
                            break;
                    }
                    if (url) {
                        await axios.get(url + "?copy=" + id)
                            .then(function (response) {
                                self.doFolderItems(self.selectedFolder, self.selectedSubfolder);
                            })
                            .catch(function (error) {
                                if (error.response) {
                                    console.log("Error code:" + error.response.status);
                                }
                            });                        
                    }
                    
                },
                openRightPanel: function(target, id) {
//                    console.log("clicked ", this);
                    $("#rightPanelContent").html("");

                    var panel_title = "";
                    var ajax = {};
                    ajax.url = "";
                    ajax.type = 'GET';
                    ajax.dataType = "html";
                    if (event.target.nodeName == "IMG")
                    {
                        var clicked_node = $(event.target).closest("a");
                    } else {
                        var clicked_node = $(event.target);
                    }
//                    console.log(target, id);
                    const self = this;

                    switch (target) 
                    {
                        case "Problem":
                            if (id)
                            {
                                ajax.url = "problem.jsp";
                                ajax.data = {id: id};
                                panel_title = "Edit Problem";
                            }
                            break;
                        case "Incident":
                            if (id)
                            {
                                ajax.data = {id: id, force2col: "1"};
                                ajax.url = "incident.jsp";
                                panel_title = "Edit Incident";
                            }
                            break;
                        case "Request":
                            if (id)
                            {
                                ajax.data = {id: id, force2col: "1"};
                                ajax.url = "request.jsp";
                                panel_title = "Edit Request";
                            }
                            break;
                        default:
                            break;
                    }

                    if (ajax.url === "") return false;

                    ajax.error = function(jqXHR, textStatus)
                    {
                        switch (jqXHR.status) {
                            case 401:
                                window.location.assign("<%=props.get("login_url").toString()%>");
                                break;
                            case 403:
                                alert("No permission!");
                                break;
                            default:
                                alert("Error processing request!");
                        }
                    };

                    ajax.success = function (data) 
                    {                    
                        $('.rightPanelHolder').addClass('open');
                        $('.rightSidePanel').addClass('open');
                        $('html').addClass('panelOpen');

                        $("#rightPanelTitle").html(panel_title);
                        $("#rightPanelContent").html(data);
                        attachFieldListeners($("#rightPanelContent"));        
                        $("#rightPanelContent").find("button.save-button").on('click', function(event) {
//                            console.log("save-button clicked");
                            self.submitRightPanel(event);
                        });

                    }

                    $.ajax(ajax);
                    
                },
                openKBPanel: function(description = "") {
                    $("#rightPanelContentKB").html("");

                    var panel_title = "";
                    var ajax = {};
                    ajax.type = 'GET';
                    ajax.dataType = "html";
                    ajax.url = "kb_search.jsp";
                    ajax.data = {embedded: "true", search: description};
                    panel_title = "Knowledge Base Search";

                    if (ajax.url === "") return false;

                    ajax.error = function(jqXHR, textStatus)
                    {
                        switch (jqXHR.status) {
                            case 401:
                                window.location.assign("<%=props.get("login_url").toString()%>");
                                break;
                            case 403:
                                alert("No permission!");
                                break;
                            default:
                                alert("Error processing request!");
                        }
                    };

                    ajax.success = function (data) 
                    {                    
                        $("#rightPanelTitleKB").html(panel_title);
                        $("#rightPanelContentKB").html(data);
                        $('.rightPanelHolderKB .rightSidePanelCloser').on('click', function(){
                            $('.rightPanelHolderKB').removeClass('open');
                            $('.rightSidePanelKB').removeClass('open');
                            $('html').removeClass('panelOpen');
                            dt_KB = null;
                        });
                        $('.rightPanelHolderKB').addClass('open');
                        $('.rightSidePanelKB').addClass('open');
                        $('html').addClass('panelOpen');

                        attachFieldListeners($("#rightPanelContentKB"));
//                        addRightSidePanelListeners();
                    }

                    $.ajax(ajax);        
                    
                },
                openKBA: function(article_id = "") {
                    $("#rightPanelContentKB").html("");

                    var panel_title = "";
                    var ajax = {};
                    ajax.type = 'GET';
                    ajax.dataType = "html";

                    if (article_id == "") {
                        var target = $(event.target).closest(".rightSidePanelOpenerKB").data("target");

                        switch (target) 
                        {
                            case "KB":
                                ajax.url = "kb_search.jsp";
                                ajax.data = {embedded: "true"};
                                panel_title = "Knowledge Base Search";
                                break;
                            default:
                                ajax.url = "";
                                break;
                        }            
                    } else {
                        ajax.url = "kb_view.jsp?id=" + article_id;
                        ajax.data = {embedded: "true"};
                        panel_title = "Knowledge Base Article";            
                    }

                    if (ajax.url === "") return false;

                    ajax.error = function(jqXHR, textStatus)
                    {
                        switch (jqXHR.status) {
                            case 401:
                                window.location.assign("<%=props.get("login_url").toString()%>");
                                break;
                            case 403:
                                alert("No permission!");
                                break;
                            default:
                                alert("Error processing request!");
                        }
                    };

                    ajax.success = function (data) 
                    {                    
                        $("#rightPanelTitleKB").html(panel_title);
                        $("#rightPanelContentKB").html(data);
                        $('.rightPanelHolderKB .rightSidePanelCloser').on('click', function(){
                            $('.rightPanelHolderKB').removeClass('open');
                            $('.rightSidePanelKB').removeClass('open');
                            $('html').removeClass('panelOpen');
                            ajax_id = "";
                            ajax_type = "";
                            dt_KB = null;
                        });
                        $('.rightPanelHolderKB').addClass('open');
                        $('.rightSidePanelKB').addClass('open');
                        $('html').addClass('panelOpen');

                        attachFieldListeners($("#rightPanelContentKB"));
                        addRightSidePanelListeners();
                    }

                    $.ajax(ajax);        
                    
                },
                submitRightPanel: function(event)
                {
//                    console.log(event);
                    const self = this;
                    var form = $(event.target).closest("form").get(0);
                    $.ajax({
                        url: form.action,
                        type: "POST",
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            // setting a timeout
                            $(event.target).prop('disabled', 'disabled');
                        },
                        success: function(){
                            self.closeRightPanel();
                            self.doFolderItems(self.selectedFolder, self.selectedSubfolder);
                        },
                        error: function(jqXHR, textStatus)
                        {
                            switch (jqXHR.status) {
                                case 401:
                                    window.location.assign("<%=props.get("login_url").toString()%>");
                                    break;
                                case 403:
                                    alert("No permission!");
                                    break;
                                default:
                                    alert("Error processing request! " + textStatus);
                            }
                        }
                    });
                },
            },
            watch: {
                selectedSubfolder() {
//                    console.log(this.selectedSubfolder + "selected");
                    this.doFolderItems(this.selectedFolder, this.selectedSubfolder);
                },
                selectedItem() {
//                    console.log(this.selectedItem);
                    var chatApp = this.$refs.chatApp;
                    chatApp.objectType = this.selectedItem.objectType;
                    chatApp.objectId = this.selectedItem.id;
                    chatApp.parentObjectType = "";
                    chatApp.parentObjectId = 0;
                    chatApp.showChat = true;
//                    window.chat_app.showChat = true;
//                    window.showChat = true;
                }
            },
            mounted () {
                this.getFolders();
                window.jQuery.MainApp.initSlimscroll();
                $('.menuHolder').on('hide.bs.dropdown', function (e) { // prevent dropdown close on autocomplete
                    // console.log( $(e.clickEvent.target).parents(), $(e.clickEvent.target).closest('calendar-table').length);
                    if (e.clickEvent && e.clickEvent.target && ($(e.clickEvent.target).parents("form#filter_form").length > 0 || $(e.clickEvent.target).closest('table.table-condensed').length || $(e.clickEvent.target).closest('.daterangepicker').length || (e.clickEvent.target.className == "ui-menu-item-wrapper" || e.clickEvent.target.className == "icon" || e.clickEvent.target.className.indexOf("applyBtn") !== -1) ))   {
                        e.preventDefault();
                    }
                });
                attachFieldListeners($("#filter_form"));

                this.$nextTick(function () {
                    // Code that will run only after the
                    // entire view has been rendered
                    $.holdReady(false);                    
                    this.selectedFolder = "Incident";
                    this.selectedSubfolder = "All";

                });
            }
        });
    });

    function change_filter_category(select)
    {
        $(".filter-category-block").addClass("d-none");
        var category_block = $("#" + $(select).val());
        if (category_block)
        {
            category_block.removeClass("d-none");    
        }
    }
    
    function attachFieldListeners(container)
    {
        tinymce.remove('textarea');
        tinymce.init({
            selector: '.ckeditor',
            setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                });
            },
            height: 250,
            plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });
        bsCustomFileInput.init();
        
        container.find('input[name="problem_lookup"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_problems",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response(
                            $.map(data.data, function (item) {
//                                console.log(item);
                                item.value = item.id;
                                item.label = item.id + " " + item.problem_time + " " + item.description;
                                return item;
                            })
                        )
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
//                console.log(ui.item);
                var problem_ids_input = $("input[name='problem_ids']")
                var problem_ids = JSON.parse(problem_ids_input.val())
                if (!problem_ids.includes(ui.item.id)) {
                    dt_rightPanel_problems.row.add({
                        "id": ui.item.id,
                        "priority": ui.item.priority,
                        "status": ui.item.status,
                        "final_status": ui.item.final_status,
                        "problem_time": ui.item.create_date,
                        "name": ui.item.name,
                        "description": ui.item.description,
                        "incidents": ui.item.incidents,
                        "assigned_to_username": ui.item.assigned_to_username,
                        "assigned_group_name": ui.item.assigned_group_name,
                        "actions": '<a href="javascript:void(0)" onclick="unlinkProblem(\'' + ui.item.id + '\')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>'
                    }).node().dataset.id = ui.item.id;
                    dt_rightPanel_problems.draw();
                    problem_ids.push(ui.item.id);
                    problem_ids_input.val(JSON.stringify(problem_ids));
                }
                $(this).val('');
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });

        //caller
        container.find('input[name="caller_username"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="caller_username"]').val(ui.item.label); // display the selected text
                container.find('input[name="caller_id"]').val(ui.item.value); // save selected id to input
                container.find('input[name="location"]').val(ui.item.location); // save selected id to input
                container.find('input[name="department"]').val(ui.item.department); // save selected id to input
                container.find('input[name="site"]').val(ui.item.site); // save selected id to input
                container.find('input[name="company"]').val(ui.item.company); // save selected id to input
                //if vip = true then check the vip checkbox
                var vip = ui.item.vip;
                if(vip.toUpperCase() === "TRUE")
                {
                    container.find('input[name="vip"]').prop('checked', true);
                }
                else
                {
                    container.find('input[name="vip"]').prop('checked', false);
                }
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });

    //caller_group_name
    container.find( 'input[name="caller_group_name"]' ).autocomplete(
    {
        source: function( request, response ) 
        {
            // Fetch data
            $.ajax({
                url: "ajax_lookup_group",
                type: 'post',
                dataType: "json",
                minLength: 2,
                data: 
                {
                    search: request.term
                },
                success: function( data ) 
                {
                    response( data );
                }
            });
        },
        select: function (event, ui) 
        {
            // Set selection
            container.find('input[name="caller_group_name"]').val(ui.item.label); // display the selected text
            container.find('input[name="caller_group_id"]').val(ui.item.value); // save selected id to input
            //alert("setting is=" + ui.item.value);
            return false;
        }
    }); 

    //create_by
    container.find('input[name="create_by_username"]').autocomplete(
    {
        source: function( request, response ) 
        {
            // Fetch data
            $.ajax({
                url: "ajax_lookup_user",
                type: 'post',
                dataType: "json",
                minLength: 2,
                data: 
                {
                    search: request.term
                },
                success: function( data ) 
                {
                    response( data );
                }
            });
        },
        select: function (event, ui) 
        {
            // Set selection
            container.find('input[name="create_by_username"]').val(ui.item.label); // display the selected text
            container.find('input[name="create_by_id"]').val(ui.item.value); // save selected id to input
            //alert("setting is=" + ui.item.value);
            return false;
        }
    });

//knowledge_search_terms
     container.find('input[name="description"]').autocomplete({

         source: function( request, response ) 
         {
             // Fetch data
             $.ajax({
                 url: "ajax_lookup_incident_knowledge_article_text",
                 type: 'post',
                 dataType: "json",
                 minLength: 2,
                 data: 
                 {
                     search: request.term
                 },
                 success: function( data ) 
                 {
                     response( data );
                 }
             });
         },
        select: function( event, ui ) 
         { 
//            container.find('input[name="knowledge_search_terms"]').val(ui.item.article_description);
//             console.log(ui.item)
             rightSidePanelOpenKB(ui.item.value);
             //window.location.href = ui.item.value;
//             window.open("kb_view.jsp?id=" + ui.item.value, "Knowledge Base Article", "width=600,height=600,top=400,left=400");
             return false;
         }
     });

        //location
        container.find('input[name="location"]' ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user_location",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="location"]').val(ui.item.label); // display the selected text
                //alert("setting is=" + ui.item.value);
                return false;
            }
        }); 
        //department
        container.find('input[name="department"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user_department",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="department"]').val(ui.item.label); // display the selected text
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });
        //site
        container.find('input[name="site"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user_site",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="site"]').val(ui.item.label); // display the selected text
                //alert("setting is=" + ui.item.value);
                return false;
            }
        }); 
        //company
        container.find('input[name="company"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incident_company",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="company"]').val(ui.item.label); // display the selected text
                //alert("setting is=" + ui.item.value);
                return false;
            }
        }); 
        //assigned_group
        container.find('input[name="assigned_group_name"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_group",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="assigned_to_name"]').val(""); // clear field
                container.find('input[name="assigned_to_id"]').val(""); // clear field
                container.find('input[name="assigned_group_name"]').val(ui.item.label); // set name
                container.find('input[name="assigned_group_id"]').val(ui.item.value); // set id
                return false;
            }
        });
        //assigned_to_name
        container.find('input[name="assigned_to_name"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="assigned_to_name"]').val(ui.item.label); // display the selected text
                container.find('input[name="assigned_to_id"]').val(ui.item.value); // save selected id to input
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });

        container.find('input[name="incident_time"]').daterangepicker(
        {
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": 
            {
                "format": "HH:mm MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            }

        });

        container.find('input[name="pending_date"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": 
            {
                "format": "HH:mm MM/DD/YYYY",

                "applyLabel": "Apply",
                "cancelLabel": "Cancel",

                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            }
        });

        container.find('input[name="pending_date"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="pending_date"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        container.find('input[name="closed_date"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": 
            {
                "format": "HH:mm MM/DD/YYYY",

                "applyLabel": "Apply",
                "cancelLabel": "Cancel",

                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            }
        });
        container.find('input[name="closed_date"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="closed_date"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });


        container.find('input[name="incident_time_start"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="incident_time_start"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="incident_time_start"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="create_date_start"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="create_date_start"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="create_date_start"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="pending_date_start"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="pending_date_start"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="pending_date_start"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="state_date_start"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="state_date_start"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="state_date_start"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="closed_date_start"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="closed_date_start"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="closed_date_start"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="incident_time_stop"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="incident_time_stop"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="incident_time_stop"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="create_date_stop"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="create_date_stop"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="create_date_stop"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="pending_date_stop"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="pending_date_stop"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="pending_date_stop"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="state_date_stop"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="state_date_stop"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="state_date_stop"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="closed_date_stop"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="closed_date_stop"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="closed_date_stop"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="caller_name"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user_last_first",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {

                // Set selection
                container.find('input[name="caller_name"]').val(ui.item.label); // set name
                container.find('input[name="caller_id"]').val(ui.item.value); // set id
                return false;
            }
        });

        container.find('input[name="create_by_name"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user_last_first",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="create_by_name"]').val(ui.item.label); // set name
                container.find('input[name="create_by_id"]').val(ui.item.value); // set id
                return false;
            }
        });

        container.find('input[name="priority"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_priority",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="priority"]').val(ui.item.label); // display the selected text
                return false;
            }
        });

        container.find('input[name="urgency"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_urgency",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="urgency"]').val(ui.item.label); // display the selected text
                return false;
            }
        });

        container.find('input[name="impact"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_impact",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="impact"]').val(ui.item.label); // display the selected text
                return false;
            }
        });

        container.find('input[name="category"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_category_search",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="category"]').val(ui.item.label); // display the selected text
                return false;
            }
        });

        container.find('input[name="subcategory"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_subcategory_search",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="subcategory"]').val(ui.item.label); // display the selected text
                return false;
            }
        });

        container.find('input[name="state"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incident_state_search",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="state"]').val(ui.item.label); // display the selected text
                return false;
            }
        });

    }    
    
</script>
    

    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%      con.close();
        }//end if not permission
    }//end if not logged in
%>
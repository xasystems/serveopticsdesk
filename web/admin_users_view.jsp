<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_users_view.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        Logger logger = LogManager.getLogger();
        String home_page = session.getAttribute("home_page").toString();
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        try 
        {
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            String id = request.getParameter("id");
            String user_info[] = db.get_users.by_id(con, id);
            String password = support.encrypt_utils.decrypt(context_dir, user_info[2]);
            /*returnString[0] = rs.getString("id");
            returnString[1] = rs.getString("username");
            returnString[2] = rs.getString("password");
            returnString[3] = check_for_null(rs.getString("first"));
            returnString[4] = check_for_null(rs.getString("mi"));
            returnString[5] = check_for_null(rs.getString("last"));
            returnString[6] = check_for_null(rs.getString("address_1"));
            returnString[7] = check_for_null(rs.getString("address_2"));
            returnString[8] = check_for_null(rs.getString("city"));
            returnString[9] = check_for_null(rs.getString("state"));
            returnString[10] = check_for_null(rs.getString("zip"));
            returnString[11] = check_for_null(rs.getString("location"));
            returnString[12] = check_for_null(rs.getString("department"));
            returnString[13] = check_for_null(rs.getString("site"));
            returnString[14] = check_for_null(rs.getString("company"));
            returnString[15] = check_for_null(rs.getString("email"));
            returnString[16] = check_for_null(rs.getString("phone_office"));
            returnString[17] = check_for_null(rs.getString("phone_mobile"));
            returnString[18] = check_for_null(rs.getString("notes"));
            returnString[19] = check_for_null(rs.getString("vip"));
            returnString[20] = check_for_null(rs.getString("is_admin"));
            returnString[21] = check_for_null(rs.getString("tz_name"));
            returnString[22] = check_for_null(rs.getString("tz_time"));
            returnString[23] = check_for_null(rs.getString("external_id"));*/
%>
<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>	

<jsp:include page='menu_service_desk.jsp'>
    <jsp:param name="active_menu" value=""/>
</jsp:include>
<!-- BEGIN PAGE LEVEL CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
<!-- END PAGE LEVEL CSS-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- start content here-->
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">Users</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a></li>
                            <li class="breadcrumb-item"><a href="admin.jsp">Administration</a></li>
                            <li class="breadcrumb-item"><a href="admin_users.jsp">Users</a></li>
                            <li class="breadcrumb-item"><a href="#"><%=user_info[1]%></a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div> <!--End breadcrumbs -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">User Details</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="first_name">First Name</label>
                                            <input readonly="true" type="text" id="first_name" name="first_name" class="form-control" value="<%=user_info[3]%>" >
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="mi">MI</label>
                                            <input readonly="true" type="text" id="mi" name="mi" class="form-control" value="<%=user_info[4]%>" >
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="last_name">Last Name</label>
                                            <input readonly="true" type="text" id="last_name" name="last_name" class="form-control" value="<%=user_info[5]%>" >
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="username">User Name</label>
                                            <input readonly="true" type="text" id="username" name="username" class="form-control" value="<%=user_info[1]%>" >
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input readonly="true" type="password" id="password" class="form-control" name="password" value="<%=user_info[2]%>" >
                                        </div>
                                    </div>
                                </div><!--end row-->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="address_1">Address <em>(1st Line)</em></label>
                                            <input readonly="true" type="text" id="address_1" name="address_1" class="form-control" value="<%=user_info[6]%>" >
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="address_1">Address <em>(2nd Line)</em></label>
                                            <input readonly="true" type="text" id="address_1" name="address_1" class="form-control" value="<%=user_info[7]%>" >
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="city">City</label>
                                            <input readonly="true" type="text" id="city" name="city" class="form-control" value="<%=user_info[8]%>" >
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="state">State</label>
                                            <input readonly="true" type="text" id="state" name="state" class="form-control" value="<%=user_info[9]%>" >
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="zip">Zip</label>
                                            <input readonly="true" type="text" id="zip" name="zip" class="form-control" value="<%=user_info[10]%>" >
                                        </div>
                                    </div>
                                </div><!--end row-->
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="department">Department</label>
                                            <input readonly="true" type="text" id="department" name="department" class="form-control" value="<%=user_info[12]%>" >
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="location">Location</label>
                                            <input readonly="true" type="text" id="location" name="location" class="form-control" value="<%=user_info[11]%>" >
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input readonly="true" type="text" id="email" name="email" class="form-control" value="<%=user_info[15]%>" >
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="phone_office">Office Phone</label>
                                            <input readonly="true" type="text" id="phone_office" name="phone_office" class="form-control" value="<%=user_info[16]%>" >
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="phone_mobile">Mobile Phone</label>
                                            <input readonly="true" type="text" id="phone_mobile" name="phone_mobile" class="form-control" value="<%=user_info[17]%>" >
                                        </div>
                                    </div>
                                    
                                </div><!--end row-->
                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="vip">VIP</label>
                                            <input readonly="true" type="text" id="vip" name="vip" class="form-control" value="<%=user_info[19]%>" >
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="is_admin">Is Admin?</label>
                                            <input readonly="true" type="text" id="is_admin" name="is_admin" class="form-control" value="<%=user_info[20]%>" >
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="tz">Users Time Zone?</label>
                                            <select disabled="disabled" id="tz" name="tz" class="form-control">
                                                <%
                                                String user_tz_name = user_info[21];
                                                String user_tz_time = user_info[22];
                                                String default_tz = "US/Eastern";
                                                String selected = "";
                                                ArrayList<String[]> timezones = db.get_timezone.all(con);
                                                for(int a = 0; a < timezones.size(); a++)
                                                {
                                                    String timezone[] = timezones.get(a);
                                                    selected = "";
                                                    if(timezone[1].equalsIgnoreCase(user_tz_name))
                                                    {
                                                        selected = "SELECTED";
                                                    }
                                                    %>
                                                    <option <%=selected%> value="<%=timezone[1] + "," + timezone[2]%>">(<%=timezone[2] + ")   " + timezone[1]%></option>
                                                    <%
                                                }
                                                %>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label for="notes">Notes</label>
                                            <input readonly="true" type="text" id="notes" name="notes" class="form-control" value="<%=user_info[18]%>" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12"><hr></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Roles</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <%
                                    ArrayList<String[]> all_roles = db.get_roles.all(con);
                                    ArrayList<String[]> users_assigned_roles = db.get_roles.roles_for_user_id(con, id);
                                    for(int a = 0; a < all_roles.size(); a++ )
                                    {
                                        String role[] = all_roles.get(a);
                                        selected = "";
                                        for(int b = 0; b < users_assigned_roles.size(); b++)
                                        {
                                            String users_role[] = users_assigned_roles.get(b);
                                            if(role[0].equalsIgnoreCase(users_role[1]))
                                            {
                                                selected = "CHECKED";
                                            }
                                        }
                                        %>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input <%=selected%> disabled="disabled" type="checkbox" name="role_id_<%=role[0]%>" id="role_id_<%=role[0]%>" value="<%=role[0]%>"/>&nbsp;<%=role[1]%>
                                            </div>
                                        </div>
                                        <%
                                    }
                                    %>
                                </div><!--end row-->
                            </div>
                            <div class="form-actions">
                                <button type="button" onclick="javascript:location.href='admin_users.jsp'" class="btn btn-warning mr-1">
                                    <i class="ft-rewind"></i> Cancel
                                </button>
                                <button type="button" onclick="javascript:location.href='admin_users_delete.jsp?id=<%=id%>'" class="btn btn-danger mr-1">
                                    <i class="ft-user-x"></i> Delete this User
                                </button>
                                <button type="button" onclick="javascript:location.href='admin_users_edit.jsp?id=<%=id%>'" class="btn btn-info">
                                    &nbsp;&nbsp;<i class="ft-edit"></i> Edit this user&nbsp;&nbsp;
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end content here-->
    </div>        
</div>
<jsp:include page='footer.jsp'>
    <jsp:param name="parameter" value=""/>
</jsp:include>	
<!-- BEGIN PAGE LEVEL JS-->
<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<!-- END PAGE LEVEL JS-->   
        <%
        con.close();
        } 
        catch (Exception e) 
        {
            System.out.println("Exception in admin_user_edit.jsp: " + e);
            logger.debug("ERROR: Exception in admin_user_edit.jsp:" + e);
        }
    } //end if not redirected
%>
</body>
</html>

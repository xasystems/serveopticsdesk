<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : service_catalog_item
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="org.apache.commons.text.StringEscapeUtils"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        if (!session.getAttribute("self_service").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("true") || session.getAttribute("manager").toString().equalsIgnoreCase("true")) 
        {
            //SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   
            //filter_format.setTimeZone(TimeZone.getTimeZone("UTC"));
            session.setAttribute("home_page", "home_service_catalog.jsp");
            String home_page = session.getAttribute("home_page").toString();
            //GregorianCalendar first_of_month = new GregorianCalendar();
            //first_of_month.set(Calendar.DAY_OF_MONTH, 1);
            //first_of_month.set(Calendar.HOUR_OF_DAY, 0);
            //first_of_month.set(Calendar.MINUTE, 0);
            //first_of_month.set(Calendar.SECOND, 0);     
            //GregorianCalendar now = new GregorianCalendar();
            java.util.Date now = new java.util.Date(); 
            String now_string = date_time_picker_format.format(now);
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap sys_props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session); 
            String user_id = session.getAttribute("user_id").toString();
            String user_info[] = db.get_users.by_id(con, user_id);
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            String catalog_item_id = request.getParameter("id");
            String catalog_item_info[] = db.get_service_catalog.service_catalog_items_by_id(con, catalog_item_id);
            
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <form action="home_self_service_sc2" method="post">
                <input type="hidden" name="catalog_item_id" id="catalog_item_id" value="<%=catalog_item_id%>"/>
                <div class="content-header row">
                    <div class="content-header-left col-md-6 col-12 mb-2">
                        <h3 class="content-header-title"><i class="la ft-home"></i>&nbsp;&nbsp;&nbsp;Service Catalog Item <em><%=catalog_item_info[2]%></em> </h3>
                        <div class="row breadcrumbs-top">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="home_self_service.jsp">Home</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <hr>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h1><i class="la  font-large-2 float-left"></i>&nbsp;&nbsp;<%=catalog_item_info[2]%></h1>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <h3><%=catalog_item_info[3]%></h3>
                            <div class="row">
                                <div class="col-12">
                                    <hr>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-2 col-lg-6 col-12">
                                    <label>
                                        <br>
                                        Requesting this Item for&nbsp;&nbsp;<a href="javascript:clear_requested_for()"><small>(clear)</small></a>
                                    </label>
                                    <input type="hidden" name="requested_for_id" id="requested_for_id" value="<%=user_info[0]%>"/>
                                    <input type="hidden" name="request_date" id="request_date" value="<%=now_string%>"/>                                     
                                    <input type="text" name="requested_for_name" id="requested_for_name" value="<%=user_info[1]%>" class="form-control" placeholder="Requested For username"/>

                                    <br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    Delivery Option/Cost
                                    <table id="cost_table" class="table table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th class="no-sort">Priority</th>
                                                <th class="no-sort">Fixed Cost</th>
                                                <th class="no-sort">Recurring Cost</th>
                                                <th class="no-sort">Delivery Time</th>
                                                <th class="no-sort">Delivered By</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <input type="radio" name="priority" value="Critical"/>
                                                    &nbsp;Critical 
                                                </td>
                                                <td>
                                                    $<%=catalog_item_info[7]%>
                                                </td>
                                                <td>
                                                    $<%=catalog_item_info[8]%>&nbsp;<%=catalog_item_info[9]%>
                                                </td>
                                                <td>
                                                    <%=catalog_item_info[10]%>&nbsp;<%=catalog_item_info[11]%>
                                                </td>
                                                <td>
                                                    <%
                                                    String group_info[] = db.get_groups.by_id(con, catalog_item_info[13]);
                                                    out.print(group_info[1]);
                                                    %>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="radio" name="priority" value="High"/>
                                                    &nbsp;High 
                                                </td>
                                                <td>
                                                    $<%=catalog_item_info[14]%>
                                                </td>
                                                <td>
                                                    $<%=catalog_item_info[15]%>&nbsp;<%=catalog_item_info[16]%>
                                                </td>
                                                <td>
                                                    <%=catalog_item_info[17]%>&nbsp;<%=catalog_item_info[18]%>
                                                </td>
                                                <td>
                                                    <%
                                                    group_info = db.get_groups.by_id(con, catalog_item_info[20]);
                                                    out.print(group_info[1]);
                                                    %>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="radio" name="priority" value="Medium"/>
                                                    &nbsp;Medium 
                                                </td>
                                                <td>
                                                    $<%=catalog_item_info[21]%>
                                                </td>
                                                <td>
                                                    $<%=catalog_item_info[22]%>&nbsp;<%=catalog_item_info[23]%>
                                                </td>
                                                <td>
                                                    <%=catalog_item_info[24]%>&nbsp;<%=catalog_item_info[25]%>
                                                </td>
                                                <td>
                                                    <%
                                                    group_info = db.get_groups.by_id(con, catalog_item_info[27]);
                                                    out.print(group_info[1]);
                                                    %>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input checked="true" type="radio" name="priority" value="Low"/>
                                                    &nbsp;Low 
                                                </td>
                                                <td>
                                                    $<%=catalog_item_info[28]%>
                                                </td>
                                                <td>
                                                    $<%=catalog_item_info[29]%>&nbsp;<%=catalog_item_info[30]%>
                                                </td>
                                                <td>
                                                    <%=catalog_item_info[31]%>&nbsp;<%=catalog_item_info[32]%>
                                                </td>
                                                <td>
                                                    <%
                                                    group_info = db.get_groups.by_id(con, catalog_item_info[34]);
                                                    out.print(group_info[1]);
                                                    %>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>             
                            <div class="row match-height">	
                                <div class="col-6 " style="border-style: solid; border-width:thin; border-color: #f0f0f0;" >                    
                                    <div class="bs-callout-danger callout-border-left callout-transparent mt-1 p-1">
                                        <h4 class="danger">Please answer the following question(s) to fullfill this Request.</h4>
                                        <input type="hidden" name="required_info" id="required_info" value="<%=catalog_item_info[35]%>"/>
                                        <%
                                        String unEscaped_required_user_input_text = StringEscapeUtils.unescapeHtml4(catalog_item_info[35]);
                                        out.print(unEscaped_required_user_input_text);
                                        %>
                                        <br>&nbsp;
                                    </div>
                                </div>
                                <div class="col-6" style="border-style: solid; border-width:thin; border-color: #f0f0f0;">                    
                                    <div class="bs-callout-danger callout-border-left callout-transparent mt-1 p-1">
                                        <h4 class="danger">Required Information Response</h4>
                                        <textarea rows="10" name="required_info_response" id="required_info_response" class="form-control" placeholder="If required, Enter Information Here"></textarea>
                                    </div>
                                    <br>&nbsp;
                                </div>  
                            </div>
                            <div class="row match-height">	
                                <div class="col-6 " style="border-style: solid; border-width:thin; border-color: #f0f0f0;" >                    
                                    <div class="bs-callout-info callout-border-left callout-transparent mt-1 p-1">
                                        <h4 class="info">Optional Information</h4>
                                        <input type="hidden" name="optional_info" id="optional_info" value="<%=catalog_item_info[36]%>"/>
                                        <%
                                        String unEscaped_optional_user_input_text = StringEscapeUtils.unescapeHtml4(catalog_item_info[36]);
                                        out.print(unEscaped_optional_user_input_text);
                                        %>
                                        <br>&nbsp;
                                    </div>
                                </div>
                                <div class="col-6" style="border-style: solid; border-width:thin; border-color: #f0f0f0;">                    
                                    <div class="bs-callout-info callout-border-left callout-transparent mt-1 p-1">
                                        <h4 class="info">Optional Information Response</h4>
                                        <textarea rows="3" name="optional_info_response" id="optional_info_response" class="form-control" placeholder="Enter any Ooptional Information Here"></textarea>
                                    </div>
                                    <br>&nbsp;
                                </div>  
                            </div>
                            <div class="row">
                                <div class="col-12 mt-1">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-info" type="submit" name="submit" value="Submit Request"/>
                                </div>
                            </div>                
                        </div>
                    </div>
                </div>  
            </form>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <script src="app-assets/vendors/js/editors/ckeditor/ckeditor.js"></script>
    <script>
    function clear_requested_for() 
    { 
        document.getElementById("requested_for_id").value = "";
        document.getElementById("requested_for_name").value = "";
    }
    </script>
    <script>    
       $( function() 
       {
            //requested_for_id
            $( "#requested_for_name" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#requested_for_name').val(ui.item.label); // display the selected text
                    $('#requested_for_id').val(ui.item.value); // save selected id to input
                    return false;
                }
            });
        });
    </script>
            
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>
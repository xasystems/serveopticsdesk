<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
        <%@ page import="java.text.SimpleDateFormat"%>

        <%
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / MM/dd/yyyy");//18:31 / 14-Jan-19
        SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
        String field_name = request.getParameter("field_name");
        String field_label = request.getParameter("field_label");
        String number_cols = request.getParameter("number_cols");
        String default_text = request.getParameter("default_text");
        String new_record = request.getParameter("new_record"); 
        String db_value = request.getParameter("db_value"); 
        if(new_record.equalsIgnoreCase("true"))
        {
            db_value = "";
        }
        else
        {
            try
            {
                db_value = date_time_picker_format.format(timestamp_format.parse(db_value));
            }
            catch(Exception e)
            {
                db_value = "";
            }
        }
        %>
        <div class="col-<%=number_cols%>">
            <label><br><%=field_label%></label>
            <div class='input-group'>
                <input type='text' id="<%=field_name%>" name="<%=field_name%>" value="<%=db_value%>" class="form-control datetime" />
                <div class="input-group-append">
                    <span class="input-group-text">
                        <span class="la la-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <script>
            $(function()
            {

              $('input[name="<%=field_name%>"]').daterangepicker({
                  "autoUpdateInput": false,
                  "singleDatePicker": true,
                            "showDropdowns": true,
                            "timePicker": true,
                            "timePicker24Hour": true,
                            "locale": {
                            "format": "HH:mm MM/DD/YYYY",

                            "applyLabel": "Apply",
                            "cancelLabel": "Cancel",

                            "weekLabel": "W",
                            "daysOfWeek": [
                                "Su",
                                "Mo",
                                "Tu",
                                "We",
                                "Th",
                                "Fr",
                                "Sa"
                            ],
                            "monthNames": [
                                "January",
                                "February",
                                "March",
                                "April",
                                "May",
                                "June",
                                "July",
                                "August",
                                "September",
                                "October",
                                "November",
                                "December"
                            ],
                            "firstDay": 1
                  }
              });
              $('input[name="<%=field_name%>"]').on('apply.daterangepicker', function(ev, picker) {
                  $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
              });

              $('input[name="<%=field_name%>"]').on('cancel.daterangepicker', function(ev, picker) {
                  $(this).val('');
              });
            });
        </script>

CREATE TABLE IF NOT EXISTS `project_attachements` (
  `id` int NOT NULL AUTO_INCREMENT,
  `project_id` int NOT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  `name_on_disk` text,
  `file_name` text,
  `file_date` timestamp NULL DEFAULT NULL,
  `file_size` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `project_task_attachements` (
  `id` int NOT NULL AUTO_INCREMENT,
  `project_id` int NOT NULL,
  `task_id` int NOT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  `name_on_disk` text,
  `file_name` text,
  `file_date` timestamp NULL DEFAULT NULL,
  `file_size` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- CREATE TABLE IF NOT EXISTS `project_task_messages` (
--   `id` int NOT NULL AUTO_INCREMENT,
--   `project_id` int NOT NULL,
--   `task_id` int NOT NULL,
--   `sender_id` int NOT NULL,
--   `content` text,
--   `date_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
--   `is_system` tinyint DEFAULT '0',
--   `saved` tinyint DEFAULT '0',
--   `distibuted` tinyint DEFAULT '0',
--   `seen` tinyint DEFAULT '0',
--   `deleted` tinyint DEFAULT '0',
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
-- this is transofrmed to a general chat_messages TABLE

CREATE TABLE IF NOT EXISTS `chat_messages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `object_type` varchar(255) NOT NULL,
  `object_id` int NOT NULL,
  `parent_object_type` varchar(255) DEFAULT '',
  `parent_object_id` int DEFAULT '0',
  `sender_id` int NOT NULL,
  `content` text,
  `date_sent` datetime DEFAULT CURRENT_TIMESTAMP,
  `is_system` tinyint DEFAULT '0',
  `saved` tinyint DEFAULT '0',
  `distibuted` tinyint DEFAULT '0',
  `seen` tinyint DEFAULT '0',
  `deleted` tinyint DEFAULT '0',
  `reply_to` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

/* index should be added
(project_id,id) 

*/

DROP PROCEDURE IF EXISTS `alter_temp`;
DELIMITER //
CREATE PROCEDURE `alter_temp`()
BEGIN
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN END;
    ALTER TABLE users ADD password_salt blob NULL AFTER external_id;
    ALTER TABLE users ADD password_hash blob NULL AFTER password_salt;
    ALTER TABLE users ADD avatar varchar(36) NULL;
END //
DELIMITER ;
CALL `alter_temp`();
DROP PROCEDURE `alter_temp`;

CREATE TABLE IF NOT EXISTS `incident_attachements` (
  `id` int NOT NULL AUTO_INCREMENT,
  `incident_id` int DEFAULT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  `name_on_disk` text,
  `file_name` text,
  `file_date` timestamp NULL DEFAULT NULL,
  `file_size` int DEFAULT NULL,
  `rev` varchar(255) DEFAULT NULL,
  `uploaded_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP PROCEDURE IF EXISTS `alter_temp`;
DELIMITER //
CREATE PROCEDURE `alter_temp`()
BEGIN
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN END;
  ALTER TABLE service_catalog ADD image varchar(36) NULL;
END //
DELIMITER ;
CALL `alter_temp`();
DROP PROCEDURE `alter_temp`;

DROP PROCEDURE IF EXISTS `alter_temp`;
DELIMITER //
CREATE PROCEDURE `alter_temp`()
BEGIN
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN END;
  ALTER TABLE service_catalog_item ADD image varchar(36) NULL;
END //
DELIMITER ;
CALL `alter_temp`();
DROP PROCEDURE `alter_temp`;

CREATE TABLE IF NOT EXISTS `problems` (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` varchar(255),
    `description` varchar(255),
    `priority` varchar(56),
    `create_date` timestamp NULL DEFAULT NULL,
    `create_by_id` int DEFAULT NULL,
    `assigned_group_id` int NOT NULL DEFAULT '0',
    `assigned_to_id` int DEFAULT NULL,
    `final_status` varchar(56),
    `status` varchar(56),
    `status_date` timestamp NULL DEFAULT NULL,
    `knowledge_article_id` int DEFAULT NULL,
    `notes` longtext,
    `has_attachments` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `problem_history` (
    `id` int NOT NULL AUTO_INCREMENT,
    `change_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `problem_id` int NOT NULL,
    `priority` varchar(45) DEFAULT NULL,
    `name` varchar(255),
    `description` varchar(255),
    `create_date` timestamp NULL DEFAULT NULL,
    `create_by_id` int DEFAULT NULL,
    `final_status` varchar(56),
    `status` varchar(255) DEFAULT NULL,
    `status_date` timestamp NULL DEFAULT NULL,
    `assigned_group_id` int DEFAULT NULL,
    `assigned_to_id` int DEFAULT NULL,
    `knowledge_article_id` int DEFAULT NULL,
    `notes` longtext,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id_UNIQUE` (`id`),
    KEY `problem_id_key` (`problem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `incident_problem` (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `problem_id` int NOT NULL,
    `incident_id` int NOT NULL,
    UNIQUE KEY `problem_incident` (`problem_id`, `incident_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP PROCEDURE IF EXISTS `alter_temp`;
DELIMITER //
CREATE PROCEDURE `alter_temp`()
BEGIN
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN END;
    ALTER TABLE roles ADD problem varchar(45) DEFAULT 'none' NULL;
END //
DELIMITER ;
CALL `alter_temp`();
DROP PROCEDURE `alter_temp`;

UPDATE roles SET problem='delete' where `name` in ('Administrator', 'Manager', 'Shift Manager', 'Service Desk Manager');
UPDATE roles SET problem='update' where `name`='Agent';

INSERT INTO `system_select_fields` (`table`, `column`, `value`, `label`, `order`, `active`) VALUES ('problems', 'status', 'New', 'New', 1, 'true');
INSERT INTO `system_select_fields` (`table`, `column`, `value`, `label`, `order`, `active`) VALUES ('problems', 'status', 'Open', 'Open', 2, 'true');
INSERT INTO `system_select_fields` (`table`, `column`, `value`, `label`, `order`, `active`) VALUES ('problems', 'status', 'WIP', 'WIP', 3, 'true');
INSERT INTO `system_select_fields` (`table`, `column`, `value`, `label`, `order`, `active`) VALUES ('problems', 'status', 'Pending', 'Pending', 4, 'true');
INSERT INTO `system_select_fields` (`table`, `column`, `value`, `label`, `order`, `active`) VALUES ('problems', 'status', 'RFC', 'RFC', 5, 'true');
INSERT INTO `system_select_fields` (`table`, `column`, `value`, `label`, `order`, `active`) VALUES ('problems', 'status', 'Closed', 'Closed', 6, 'true');

INSERT INTO `system_select_fields` (`table`, `column`, `value`, `label`, `order`, `active`) VALUES ('problems', 'final_status', 'RFC', 'RFC', 1, 'true');
INSERT INTO `system_select_fields` (`table`, `column`, `value`, `label`, `order`, `active`) VALUES ('problems', 'final_status', 'Workaround', 'Workaround', 2, 'true');
INSERT INTO `system_select_fields` (`table`, `column`, `value`, `label`, `order`, `active`) VALUES ('problems', 'final_status', 'Cost Prohibitive', 'Cost Prohibitive', 3, 'true');
INSERT INTO `system_select_fields` (`table`, `column`, `value`, `label`, `order`, `active`) VALUES ('problems', 'final_status', 'No Fix', 'No Fix', 4, 'true');

INSERT INTO `system_select_fields` (`table`, `column`, `value`, `label`, `order`, `active`) VALUES ('problems', 'priority', 'Critical', 'Critical', 1, 'true');
INSERT INTO `system_select_fields` (`table`, `column`, `value`, `label`, `order`, `active`) VALUES ('problems', 'priority', 'High', 'High', 2, 'true');
INSERT INTO `system_select_fields` (`table`, `column`, `value`, `label`, `order`, `active`) VALUES ('problems', 'priority', 'Medium', 'Medium', 3, 'true');
INSERT INTO `system_select_fields` (`table`, `column`, `value`, `label`, `order`, `active`) VALUES ('problems', 'priority', 'Low', 'Low', 4, 'true');
INSERT INTO `system_select_fields` (`table`, `column`, `value`, `label`, `order`, `active`) VALUES ('problems', 'priority', 'Planning', 'Planning', 5, 'true');

CREATE TABLE IF NOT EXISTS `problem_attachements` (
  `id` int NOT NULL AUTO_INCREMENT,
  `problem_id` int DEFAULT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  `name_on_disk` text,
  `file_name` text,
  `file_date` timestamp NULL DEFAULT NULL,
  `file_size` int DEFAULT NULL,
  `rev` varchar(255) DEFAULT NULL,
  `uploaded_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `user_sessions_history` (
  `user_id` int NOT NULL,
  `access_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `dm` (
  `id` int NOT NULL AUTO_INCREMENT,
  `sent_time` timestamp NULL DEFAULT NULL,
  `sender_user_id` int DEFAULT NULL,
  `object_type` varchar(45) DEFAULT NULL,
  `object_id` int DEFAULT NULL,
  `parent_object_type` varchar(45) DEFAULT NULL,
  `parent_object_id` int DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `dm_recipient` (
  `id` int NOT NULL,
  `dm_id` int DEFAULT NULL,
  `recipient_user_id` int DEFAULT NULL,
  `response_timestamp` timestamp NULL DEFAULT NULL,
  `response` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP PROCEDURE IF EXISTS `alter_temp`;
DELIMITER //
CREATE PROCEDURE `alter_temp`()
BEGIN
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN END;
    ALTER TABLE chat_messages ADD reply_to_id int DEFAULT 0 NULL;
    ALTER TABLE chat_messages ADD reply_to_content text;
END //
DELIMITER ;
CALL `alter_temp`();
DROP PROCEDURE `alter_temp`;

CREATE TABLE IF NOT EXISTS `chat_message_reactions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `message_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `reaction` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/* 04-11-2022 */
ALTER TABLE request_sla ADD service_catalog_item_id int NOT NULL AFTER id;
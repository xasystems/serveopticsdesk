<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        if(!session.getAttribute("administration").toString().equalsIgnoreCase("true") && !session.getAttribute("manager").toString().equalsIgnoreCase("true"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            String home_page = session.getAttribute("home_page").toString();
            String user_id = session.getAttribute("user_id").toString();
            Logger logger = LogManager.getLogger();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);    
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
            
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <!-- END PAGE LEVEL CSS-->
    
    <div class="clr whtbg p-10 mb-15 position-relative">
        <h1 class="large-font boldFont">Administration Home</h1>
    </div>

    <div class="row">
        <div class="col-md-3">
                <div class=" box mb-20">
                                    <h4 class="card-title">License</h4>
                                    <p class="card-text">View/Manage the ServeOptics license.</p>
                                    <a href="admin_license.jsp">Edit</a>
                </div>
        </div>
        <div class="col-md-3">
                <div class=" box mb-20">
                                    <h4 class="card-title">Users</h4>
                                    <p class="card-text">View/Manage the Users.</p>
                                    <a href="admin_users.jsp">1. Users</a>
                                    <br>
                                    <a href="admin_groups.jsp">2. Groups</a>
                                    <br>
                                    <a href="admin_users_add_bulk.jsp">3. Bulk User Operations</a>
                </div>
        </div>
        <div class="col-md-3">
                <div class=" box mb-20">
                                    <h4 class="card-title">Roles</h4>
                                    <p class="card-text">View/Manage User Roles</p>
                                    <a href="admin_roles.jsp">Edit</a>
                </div>
        </div>
        <div class="col-md-3">
                <div class=" box mb-20">
                                    <h4 class="card-title">System Schedules</h4>
                                    <p class="card-text">View/Manage System Schedules</p>
                                    <a href="admin_system_schedules.jsp">Edit</a>
            </div>    
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
                <div class=" box mb-20">
                                    <h4 class="card-title">Email Settings</h4>
                                    <p class="card-text">View/Manage Email Settings.</p>
                                    <a href="admin_email.jsp">Edit</a>
                </div>
        </div>
        <div class="col-md-3">
                <div class=" box mb-20">
                                    <h4 class="card-title">ServeOptics URL</h4>
                                    <p class="card-text">Enter the ServeOptics URL.</p>
                                    <a href="admin_url.jsp">Edit</a>
                </div>
        </div>

        <div class="col-md-3">
                <div class=" box mb-20">
                                    <h4 class="card-title">Category/Subcategory</h4>
                                    <p class="card-text">Add/Edit/Delete Incident Categories/Subcategories</p>
                                    <a href="admin_category_subcategory.jsp">Edit</a>
                </div>
        </div>
                   <!-- <div class="col-xl-3 col-md-6 col-sm-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <h4 class="card-title">Form Select Options</h4>
                                    <p class="card-text">Add/Edit/Delete Form Select Options <br><em>Such as Priority, State, etc..</em></p>
                                    <a href="admin_system_select_fields.jsp">Change Options</a>
                                </div>
                            </div>
                        </div>
                    </div>-->
        <div class="col-md-3">
                <div class=" box mb-20">
                                    <h4 class="card-title">Service Catalog</h4>
                                    <p class="card-text">Manage the Service Catalog(s)</p>
                                    <a href="admin_service_catalog.jsp">Edit</a>
                </div>
        </div>    
    </div>
    <div class="row">
        <div class="col-md-3">
                <div class=" box mb-20">
                                    <h4 class="card-title">Custom Fields</h4>
                                    <p class="card-text">View/Manage Custom Fields.</p>
                                    <a href="admin_custom_fields.jsp">Edit</a>
                </div>
        </div>
        <div class="col-md-3">
                <div class=" box mb-20">
                                    <h4 class="card-title">Self Service Options</h4>
                                    <p class="card-text">View/Manage Self Service Portal Options.</p>
                                    <a href="admin_self_service.jsp">Edit</a>
                </div>
        </div>
        <div class="col-md-3">
                <div class=" box mb-20">
                                    <h4 class="card-title">SLA</h4>
                                    <p class="card-text">Service-level Agreement</p>
                                    <a href="admin_sla.jsp">Manage</a>
                </div>
        </div>
        <div class="col-md-3">
                <div class=" box mb-20">
                                    <h4 class="card-title">KB</h4>
                                    <p class="card-text">Knowledge Base</p>
                                    <a href="admin_knowledge_base.jsp">Manage</a>
                </div>
        </div>
    </div>    

    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->   
    <%
                con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception on admin.jsp=" + e);
            }
            
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String FINANCIAL = session.getAttribute("financial").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String CONTACT = session.getAttribute("contact").toString();
        String CX = session.getAttribute("cx").toString();
        String SURVEY = session.getAttribute("survey").toString();
        String SLA = session.getAttribute("sla").toString();        
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        String METRIC = session.getAttribute("metric").toString();
        
        
        if(!ADMINISTRATION.equalsIgnoreCase("true"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);  
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String category_id = request.getParameter("category_id");
                String category_info[] = db.get_category.info_by_category_id(con, category_id);
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END PAGE LEVEL CSS-->
    
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title">Edit Category/Subcategory Options</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a></li>
                                <li class="breadcrumb-item"><a href="admin.jsp">Administration</a></li>
                                <li class="breadcrumb-item"><a href="admin_category_subcategory.jsp">Category/Subcategory</a></li>
                                <li class="breadcrumb-item"><a href="#">Delete</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div> <!--End breadcrumbs -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Delete Category/Subcategory</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <form class="form" action="admin_category_subcategory_delete" method="post">
                                    <input type="hidden" name="category_id" value="<%=category_id%>"/>
                                    <div class="form-body">
                                        <div>
                                            <button type="button" onclick="javascript:location.href='admin_category_subcategory.jsp?category_id=<%=category_id%>'" class="btn btn-warning mr-1">
                                                <i class="ft-x"></i> Go Back
                                            </button>
                                            <button type="submit" class="btn btn-info mr-1">
                                                <i class="ft-save"></i> Delete
                                            </button>
                                        </div>
                                        <br>
                                        <br>
                                        <table id="field_table" name="field_table" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th width="20">Active</th>
                                                    <th>Category</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <%
                                                        String checked = "";
                                                        if(category_info[2].equalsIgnoreCase("true"))
                                                        {
                                                            checked = "CHECKED";
                                                        }
                                                        %>
                                                        <input <%=checked%> disabled type="checkbox" name="category_active" id="category_active"/></div>
                                                    </td>
                                                    <td>                                                        
                                                        <input class="form-control" disabled type="text" name="category_name" id="category_name" value="<%=category_info[1]%>"/>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <hr>
                                        
                                        <table id="field_table" name="field_table" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th width="20"><div class="form-inline">Active</div></th>
                                                    <th>Subcategory</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%
                                                ArrayList<String[]> subcategories = db.get_subcategory.all_for_category_id(con, category_info[0]);
                                                /*temp[0] = rs.getString("id");
                                                temp[1] = rs.getString("category_id");
                                                temp[2] = rs.getString("name");
                                                temp[3] = rs.getString("active");*/
                                                for(int b = 0; b < subcategories.size(); b++)
                                                {
                                                    String subcategory[] = subcategories.get(b);
                                                    checked = "";
                                                    if(subcategory[3].equalsIgnoreCase("true"))
                                                    {
                                                        checked = "CHECKED";
                                                    }
                                                    %>
                                                    <tr>
                                                        <td>
                                                            <input type="hidden" name="subcategory_id_<%=subcategory[0]%>" id="subcategory_id_<%=subcategory[0]%>" value="<%=subcategory[0]%>"/>
                                                            <input <%=checked%> disabled  type="checkbox" name="subcategory_active_<%=subcategory[0]%>" id="subcategory_active_<%=subcategory[0]%>"/>
                                                        </td>
                                                        <td>
                                                            <div class="form-inline">
                                                            <input class="form-control" disabled type="text" name="subcategory_name_<%=subcategory[0]%>" id="subcategory_name_<%=subcategory[0]%>" value="<%=subcategory[2]%>"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <%
                                                }
                                                %>
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>
    
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <!-- END PAGE LEVEL JS-->  
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_systen_select_fields.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_systen_select_fields.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

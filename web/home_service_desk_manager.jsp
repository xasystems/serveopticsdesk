<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.sql.*" %>
<!DOCTYPE html>
    <%
    String context_dir = request.getServletContext().getRealPath("");
    LinkedHashMap props = support.config.get_config(context_dir);
        
    if(session.getAttribute("authenticated")==null)
    {
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        //page authorization
        //session vars
        String administration = session.getAttribute("administration").toString();
        String manager = session.getAttribute("manager").toString();
        
        if(administration.equalsIgnoreCase("false") && manager.equalsIgnoreCase("false"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            session.setAttribute("home_page", "home_service_desk_manager.jsp");
            try
            {
                SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
                GregorianCalendar first_of_month = new GregorianCalendar();
                first_of_month.set(Calendar.DAY_OF_MONTH, 1);
                first_of_month.set(Calendar.HOUR_OF_DAY, 0);
                first_of_month.set(Calendar.MINUTE, 0);
                first_of_month.set(Calendar.SECOND, 0);     
                GregorianCalendar now = new GregorianCalendar();
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                boolean has_a_disply = false;
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title"><i class="la ft-home"></i>&nbsp;&nbsp;&nbsp;Service Desk (Manager View)&nbsp;&nbsp;&nbsp;<input type="button" value="Switch to My View" class="btn-sm btn-info" onclick="location.href='home_service_desk.jsp';"/></h3>
                    <!--<div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="home.jsp">Home</a>
                                </li>
                            </ol>
                        </div>
                    </div>-->
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <hr>
                </div>
            </div>
            <div class="row">
                <%
                //incident CRUD
                boolean incident_authorized = support.role.authorized(session, "incident","read");
                String incident_display = "none";
                if(incident_authorized)
                {
                    incident_display = "inline";
                    has_a_disply = true;
                }
                else
                {
                    incident_display = "none";
                }
                %>
                <div class="col-xl-3 col-lg-6 col-12" style="display: <%=incident_display%>">
                    <div class="card" style="min-height:400px;">
                        <div class="card-header">
                            <h1>
                                <a href="incident_list.jsp?predefined=all" title="All Incidents List"><i class="la la-wrench info font-large-2 float-left"></i>&nbsp;&nbsp;Incidents</a>
                                &nbsp;&nbsp;
                                <a href="incident_new.jsp" class="btn btn-info">Create New</a>
                            </h1>                            
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                 <div class="row justify-content-center">
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="incident_list.jsp?predefined=unassigned">Unassigned</a></h5></div>  
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="incident_list.jsp?predefined=created_today">Created Today</a></h5></div>
                                </div>
                                <div class="row">
                                    <%
                                    int unassigned_count = db.get_incidents.unassigned_incident_count(con);
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="incident_list.jsp?predefined=unassigned"><%=unassigned_count%></a></h3></div>
                                    <%
                                    int create_today_count = db.get_incidents.created_today_incident_count(con);
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="incident_list.jsp?predefined=created_today"><%=create_today_count%></a></h3></div>
                                    
                                </div>
                                
                                <div class="row">
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="incident_list.jsp?predefined=closed_today">Closed Today</a></h5></div>
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="incident_list.jsp?predefined=critical">Critical</a></h5></div>                                    
                                </div>
                                <div class="row">
                                    <%
                                    int closed_today_count = db.get_incidents.closed_today_incident_count(con);
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="incident_list.jsp?predefined=closed_today"><%=closed_today_count%></a></h3></div>
                                    <%
                                    int critical = db.get_incidents.count_by_priority(con,"critical");
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="incident_list.jsp?predefined=critical"><%=critical%></a></h3></div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="incident_list.jsp?predefined=open">Open</a></h5></div>
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="incident_list.jsp?predefined=open_7_14">Open 7-14 Days</a></h5></div>
                                </div>
                                <div class="row">
                                    <%
                                    int open_count = db.get_incidents.open_incident_count(con);
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="incident_list.jsp?predefined=open"><%=open_count%></a></h3></div>
                                    <%
                                    int open_7_14_count = db.get_incidents.incidents_open_7_14_days_count(con);
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="incident_list.jsp?predefined=open_7_14"><%=open_7_14_count%></a></h3></div>
                                </div>
                                <div class="row">
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="incident_list.jsp?predefined=open_14_30">Open 14-30 Days</a></h5></div>
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="incident_list.jsp?predefined=open_30_plus">Open 30+ Days</a></h5></div>
                                </div>
                                <div class="row">
                                    <%
                                    int open_14_30_count = db.get_incidents.incidents_open_14_30_days_count(con);
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="incident_list.jsp?predefined=open_14_30"><%=open_14_30_count%></a></h3></div>
                                    <%
                                    int open_30_plus_days_count = db.get_incidents.incidents_open_30_plus_days_count(con);
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="incident_list.jsp?predefined=open_30_plus"><%=open_30_plus_days_count%></a></h3></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%
                //request CRUD
                boolean request_authorized = support.role.authorized(session, "request","read");
                String request_display = "none";
                if(request_authorized)
                {
                    request_display = "inline";
                    has_a_disply = true;
                }
                else
                {
                    request_display = "none";
                }
                %>
                <div class="col-xl-3 col-lg-6 col-12" style="display: <%=request_display%>">
                    <div class="card" style="min-height:400px;">
                        <div class="card-header">
                            <h1>
                                <a href="request_list.jsp?predefined=all" title="All Requests List"><i class="la la-shopping-cart info font-large-2 float-left"></i>&nbsp;&nbsp;Requests</a>
                                &nbsp;&nbsp;
                                <a href="request_new.jsp" class="btn btn-info">Create New</a>
                            </h1>
                            
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                 <div class="row justify-content-center">
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="request_list.jsp?predefined=unassigned">Unassigned</a></h5></div>                                  
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="request_list.jsp?predefined=created_today">Created Today</a></h5></div>
                                </div>
                                <div class="row">
                                    <%
                                    unassigned_count = db.get_requests.unassigned_request_count(con);
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="request_list.jsp?predefined=unassigned"><%=unassigned_count%></a></h3></div>
                                    <%
                                    create_today_count = db.get_requests.requests_created_today_count(con);
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="request_list.jsp?predefined=created_today"><%=create_today_count%></a></h3></div>
                                </div>
                                
                                <div class="row justify-content-center">
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="request_list.jsp?predefined=closed_today">Closed Today</a></h5></div>                                    
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="request_list.jsp?predefined=open">Open</a></h5></div>
                                </div>
                                <div class="row">
                                    <%
                                    closed_today_count = db.get_requests.requests_closed_today_count(con);
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="request_list.jsp?predefined=closed_today"><%=closed_today_count%></a></h3></div>
                                    <%
                                    open_count = db.get_requests.requests_open_count(con);
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="request_list.jsp?predefined=open"><%=open_count%></a></h3></div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="request_list.jsp?predefined=open_7_14">Open 7-14 Days</a></h5></div>
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="request_list.jsp?predefined=open_14_30">Open 14-30 Days</a></h5></div>
                                </div>
                                <div class="row">
                                    <%
                                    open_7_14_count = db.get_requests.requests_open_7_14_count(con);
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="request_list.jsp?predefined=open_7_14"><%=open_7_14_count%></a></h3></div>
                                    <%
                                    open_count = db.get_requests.requests_open_14_30_count(con);
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="request_list.jsp?predefined=open_14_30"><%=open_count%></a></h3></div>
                                </div>
                                <div class="row justify-content-center">
                                    
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="request_list.jsp?predefined=open_30_plus">Open 30+ Days</a></h5></div>
                                    <div class="col-6"><h5>&nbsp;</h5></div>
                                </div>
                                <div class="row">
                                    <%
                                    int open_30_plus_count = db.get_requests.requests_open_30_plus_days_count(con);
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="request_list.jsp?predefined=open_30_plus"><%=open_30_plus_count%></a></h3></div>
                                    <div class="col-6"><h3>&nbsp;</h3></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%
                //project CRUD
                boolean project_authorized = support.role.authorized(session, "project","read");
                String project_display = "none";
                if(project_authorized)
                {
                    project_display = "inline";
                    has_a_disply = true;
                }
                else
                {
                    project_display = "none";
                }
                %>
                <div class="col-xl-3 col-lg-6 col-12" style="display: <%=project_display%>">
                    <div class="card" style="min-height:400px;">
                        <div class="card-header">
                            <h1>
                                <a href="project_list.jsp?predefined=all" title="All Projects List"><i class="la la-building info font-large-2 float-left"></i>&nbsp;&nbsp;Projects</a>
                                &nbsp;&nbsp;
                                <a href="project_new.jsp" class="btn btn-info">Create New</a>
                            </h1>
                            
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                 <div class="row justify-content-center">
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="project_list.jsp?predefined=open">Open Projects</a></h5></div>
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="project_task_list.jsp?filter=all_open_tasks">Open Tasks</a></h5></div>                                  
                                </div>
                                <div class="row">
                                    <%
                                    ArrayList<String[]> open_projects = db.get_projects.all_open(con);
                                    int open_projects_count = open_projects.size();
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="project_list.jsp?predefined=open"><%=open_projects_count%></a></h3></div>
                                    <%
                                    ArrayList <String[]> open_tasks = db.get_projects.open_project_tasks(con);
                                    int open_tasks_count = open_tasks.size();
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="project_task_list.jsp?filter=all_open_tasks"><%=open_tasks_count%></a></h3></div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-6"><h5>&nbsp;</h5></div>
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="project_task_list.jsp?filter=all_delinquent">Delinquent Tasks</a></h5></div>
                                </div>
                                <div class="row">
                                    <div class="col-6"><h3>&nbsp;</h3></div>
                                    <%
                                    ArrayList <String[]> delinquent_tasks = db.get_projects.delinquent_project_tasks(con);
                                    int delinquent_tasks_count = delinquent_tasks.size();
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="project_task_list.jsp?filter=all_delinquent"><%=delinquent_tasks_count%></a></h3></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%
                //request CRUD
                boolean job_authorized = support.role.authorized(session, "job","read");
                String job_display = "none";
                if(job_authorized)
                {
                    job_display = "inline";
                    has_a_disply = true;
                }
                else
                {
                    job_display = "none";
                }
                %>
                <div class="col-xl-3 col-lg-6 col-12" style="display: <%=job_display%>">
                    <div class="card" style="min-height:400px;">
                        <div class="card-header">
                            <h1>
                                <a href="job_list.jsp?predefined=all"><i class="la la-industry info font-large-2 float-left"></i>&nbsp;&nbsp;Jobs</a>
                                &nbsp;&nbsp;
                                <a href="job_new.jsp" class="btn btn-info">Create New</a>
                            </h1>                            
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                 <div class="row justify-content-center">
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="job_list.jsp?predefined=all_open_jobs">Open Jobs</a></h5></div>
                                    <div class="col-6"><h5><a style="text-decorations:none; color:inherit;" href="job_list.jsp?predefined=all_delinquent_jobs">Delinquent Jobs</a></h5></div>                                  
                                </div>
                                <div class="row">
                                    <%
                                    ArrayList<String[]> all_open_jobs = db.get_jobs.all_open(con);
                                    int all_open_job_count = all_open_jobs.size();
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="job_list.jsp?predefined=all_open_jobs"><%=all_open_job_count%></a></h3></div>
                                    <%
                                    ArrayList<String[]> delinquent_jobs = db.get_jobs.all_delinquent_jobs(con);
                                    int all_delinquent_job_count = delinquent_jobs.size();
                                    %>
                                    <div class="col-6"><h3><a style="text-decorations:none; color:inherit;" href="job_list.jsp?predefined=all_delinquent_jobs"><%=all_delinquent_job_count%></a></h3></div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
                <%
                String no_display = "";
                if(!has_a_disply)
                {
                    no_display = "inline";
                }
                else
                {
                    no_display = "none";
                }
                %>    
                <div class="col-xl-3 col-lg-6 col-12" style="display: <%=no_display%>">
                    <div class="card" style="min-height:400px;">
                        <div class="card-header">
                            <h1>
                                <a href="#"><i class="la la-question info font-large-2 float-left"></i>&nbsp;&nbsp;No Permissions</a>
                                &nbsp;&nbsp;                                
                            </h1>
                            
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                 <div class="row justify-content-center">
                                    <div class="col-12"><h5>Seems your role does not have permission to view anything on this page</h5></div>                                
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%          }
            catch(Exception e)
            {
                System.out.println("Exception on home_service_desk_manager.jsp=" + e);
            }
    } //end if not admin or manager
}//end if not logged in
%>
<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : message_new
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="org.apache.commons.text.StringEscapeUtils"%>
<%@ page import="java.time.ZonedDateTime"%>
<%@ page import="java.time.format.DateTimeFormatter"%>     
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        
        //session vars
        boolean page_authorized = false;
        boolean incident_authorized = support.role.authorized(session, "incident","create");      
        boolean request_authorized = support.role.authorized(session, "request","create");            
        if(!incident_authorized || !request_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            try
            {
                String user_tz_name = "UTC";
                String user_tz_time = "+00:00";
                try
                {
                    user_tz_name = session.getAttribute("tz_name").toString();
                    user_tz_time = session.getAttribute("tz_time").toString();
                    if(user_tz_name == null || user_tz_time == null )
                    {
                        user_tz_name = "UTC";
                        user_tz_time = "+00:00";
                    }

                }
                catch(Exception e)
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }
                String home_page = session.getAttribute("home_page").toString();
                SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
                SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
                SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020            
                SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
                String context_dir = request.getServletContext().getRealPath("");
                LinkedHashMap props = support.config.get_config(context_dir);
                Connection con = db.db_util.get_contract_connection(context_dir, session); 
                ArrayList<String[]> messages = db.get_message.message_by_open_status(con);
                
            
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!--Start Page level css-->
    <link rel="stylesheet" type="text/css" href="assets/css/dashboard.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/colors/palette-tooltip.css">
    <!--End Page level css--><!-- END Custom CSS-->
    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title"><i class="la la-stack-overflow"></i>&nbsp;Messages</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"> 
                                    <a href="<%=home_page%>"><i class="la la-home"></i>&nbsp;Home</a>
                                </li>
                                <li class="breadcrumb-item"> 
                                    <a href="messages.jsp"><i class="la la-stack-overflow"></i>&nbsp;Messages</a>
                                </li>
                                <li class="breadcrumb-item"> 
                                    <a href="#"><i class="la la-stack-overflow"></i>&nbsp;Open Messages</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- start content here-->
            
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <h4>
                                    New Messages
                                </h4>
                                <table id="incident_table" class="table table-striped table-bordered zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>Status</th>
                                            <th class="no-sort">Date Sent</th>
                                            <th>From</th>
                                            <th>Subject</th>
                                            <th>Body</th>
                                            <th>Has Attachment</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                        for(int a = 0; a < messages.size(); a++)
                                        {
                                            String message[] = messages.get(a);
                                            String date_sent = "";
                                            try
                                            {
                                                ZonedDateTime zdt_temp_date = support.date_utils.utc_to_user_tz(user_tz_name, message[3]);
                                                date_sent = DateTimeFormatter.ofPattern("HH:mm MM/dd/yyyy").format(zdt_temp_date);                                                    
                                            }
                                            catch (Exception e)
                                            {
                                                date_sent = "";
                                                System.out.println("Exception on incident_list.jsp db incident_time=" + message[3] + " date_sent=" + e);
                                            }
                                            
                                            
                                            /*
                                            message[0] = rs.getString("id");
                                            message[1] = rs.getString("uuid");
                                            message[2] = support.string_utils.check_for_null(rs.getString("source"));
                                            message[3] = support.string_utils.check_for_null(rs.getString("date_sent"));
                                            message[4] = support.string_utils.check_for_null(rs.getString("from"));                
                                            message[5] = support.string_utils.check_for_null(rs.getString("subject"));  
                                            message[6] = support.string_utils.check_for_null(rs.getString("to"));  
                                            message[7] = support.string_utils.check_for_null(rs.getString("body"));  
                                            message[8] = support.string_utils.check_for_null(rs.getString("mime_type"));  
                                            message[9] = support.string_utils.check_for_null(rs.getString("priority"));  
                                            message[10] = support.string_utils.check_for_null(rs.getString("status"));  
                                            message[11] = support.string_utils.check_for_null(rs.getString("status_date"));  
                                            message[12] = support.string_utils.check_for_null(rs.getString("status_by_user_id"));  
                                            message[13] = support.string_utils.check_for_null(rs.getString("result"));  
                                            message[14] = support.string_utils.check_for_null(rs.getString("has_attachment"));  
                                            message[15] = support.string_utils.check_for_null(rs.getString("description"));  
                                            message[16] = support.string_utils.check_for_null(rs.getString("log"));  
                                            message[17] = support.string_utils.check_for_null(rs.getString("related_to"));     
                                            message[18] = support.string_utils.check_for_null(rs.getString("related_to_id"));  
                                            message[19] = support.string_utils.check_for_null(rs.getString("assigned_to_id"));     
                                            message[20] = support.string_utils.check_for_null(rs.getString("date_assigned"));    
                                            */
                                            String from = message[4];
                                            String subject = message[5];
                                            //String body = StringEscapeUtils.unescapeHtml4(message[7]);;
                                            String body = message[7];
                                            String short_body = body;
                                            String body_mime_type = message[8];
                                            String unescaped_body = message[7];
                                            String status = message[10];
                                            //System.out.println("pre  short_body.length()=" + short_body.length());
                                            if(body.length() > 40)
                                            {
                                                short_body = body.substring(0,39) + "...";
                                            }
                                            //System.out.println("post short_body.length()=" + short_body.length());
                                            String has_attachment = message[14];
                                            //String log = StringEscapeUtils.unescapeHtml4(message[16]);
                                            //String short_log = message[16];
                                            //if(log.length() > 40)
                                            //{
                                            //    short_log = log.substring(0, 39) + "...";
                                            //}
                                            %>
                                            <tr onclick="location.href='message_process.jsp?id=<%=message[0]%>';" style="cursor:pointer;">
                                            <td><%=status%></td>
                                            <td nowrap><%=date_sent%></td>
                                            <td><%=from%></td>
                                            <td><%=subject%></td>
                                            <td>
                                                <span title="<%=StringEscapeUtils.escapeHtml4(body)%>"><%=short_body%></span>
                                            </td>
                                            <td><%=has_attachment%></td>     
                                            </tr>
                                            <%
                                        }                                        
                                        %>                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th class="no-sort">Date Sent</th>
                                            <th>From</th>
                                            <th>Subject</th>
                                            <th>Body</th>
                                            <th>Has Attachment</th>
                                            <th>Log</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end content here-->
            <% 
            }
            catch(Exception e)
            {
                System.out.println("Exception on message_new.jsp=" + e);
            }
            %>
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
                                    
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- BEGIN VENDOR JS-->    
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="app-assets/js/core/app-menu.js"></script>
    <script src="app-assets/js/core/app.js"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/popover/popover.js"></script>

    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>    
    <script src="app-assets/js/scripts/tooltip/tooltip.js"></script>
    
    <script>
    function clear_caller() 
    { 
        document.getElementById("caller_id").value = "";
        document.getElementById("caller_name").value = "";
    }
    function clear_assigned_to() 
    { 
        document.getElementById("assigned_to_id").value = "";
        document.getElementById("assigned_to_name").value = "";
    }
    function clear_create_by() 
    { 
        document.getElementById("create_by_id").value = "";
        document.getElementById("create_by_name").value = "";
    }
    function clear_caller_group() 
    { 
        document.getElementById("caller_group_id").value = "";
        document.getElementById("caller_group_name").value = "";
    }
    function clear_assigned_group() 
    { 
        document.getElementById("assigned_group_id").value = "";
        document.getElementById("assigned_group_name").value = "";
    }
    </script>
    <script>
        function hide_show_div() 
        {
            var x = document.getElementById("selectors");
            if (x.style.display === "none") 
            {
                x.style.display = "block";
            } 
            else 
            {
                x.style.display = "none";
            }
        }        
        
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="incident_time_start"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="incident_time_start"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="incident_time_start"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="create_date_start"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="create_date_start"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="create_date_start"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="pending_date_start"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="pending_date_start"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="pending_date_start"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="state_date_start"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="state_date_start"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="state_date_start"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="closed_date_start"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="closed_date_start"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="closed_date_start"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="incident_time_stop"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="incident_time_stop"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="incident_time_stop"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="create_date_stop"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="create_date_stop"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="create_date_stop"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="pending_date_stop"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="pending_date_stop"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="pending_date_stop"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="state_date_stop"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="state_date_stop"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="state_date_stop"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="closed_date_stop"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="closed_date_stop"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="closed_date_stop"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    
    
    <script>
        //assigned_group
        $( function() 
        {
            $( "#caller_name" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user_last_first",
                        type: 'post',
                        dataType: "json",
                        minLength: 1,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    
                    // Set selection
                    $('#caller_name').val(ui.item.label); // set name
                    $('#caller_id').val(ui.item.value); // set id
                    return false;
                }
            });
     });   
    </script>
    <script>
        //assigned_to
        $( function() 
        {
            $( "#assigned_to_name" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user_last_first",
                        type: 'post',
                        dataType: "json",
                        minLength: 1,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#assigned_to_name').val(ui.item.label); // set name
                    $('#assigned_to_id').val(ui.item.value); // set id
                    return false;
                }
            });
     });   
    </script>
    <script>
        //created_by
        $( function() 
        {
            $( "#create_by_name" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user_last_first",
                        type: 'post',
                        dataType: "json",
                        minLength: 1,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#create_by_name').val(ui.item.label); // set name
                    $('#create_by_id').val(ui.item.value); // set id
                    return false;
                }
            });
     });   
    </script>
    <script>
        $( "#caller_group_name" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_group",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#caller_group_name').val(ui.item.label); // display the selected text
                $('#caller_group_id').val(ui.item.value); // save selected id to input
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });
    
    </script>
    <script>
        $( "#assigned_group_name" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_group",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#assigned_group_name').val(ui.item.label); // display the selected text
                $('#assigned_group_id').val(ui.item.value); // save selected id to input
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });
    
    </script>
    <script>
        $( "#priority" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_priority",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#priority').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        $( "#urgency" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_urgency",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#urgency').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        $( "#impact" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_impact",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#impact').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        $( "#category" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_category_search",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#category').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        $( "#subcategory" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_subcategory_search",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#subcategory').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        $( "#state" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incident_state_search",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#state').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        $( "#location" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incident_location",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#location').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        $( "#department" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incident_department",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#department').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        $( "#site" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incident_site",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#site').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        $( "#company" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incident_company",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#company').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>
    
    <!-- END PAGE LEVEL JS-->
    <script>
        $('#incident_table').dataTable( {
            "order": []
        } );
    </script>
    </body>
</html>
<%     
        }//end if not permission
    }//end if not logged in
%>
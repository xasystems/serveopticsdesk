<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : index
    Created on : Dec 11, 2018, 8:03:00 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.LinkedHashMap"%>
<%
String context_dir = request.getServletContext().getRealPath("");
LinkedHashMap props = support.config.get_config(context_dir);
try
{
    String installation_type = props.get("installation_type").toString().trim(); //#shared or standalone 
    
    String login_url = props.get("login_url").toString(); //http://localhost:8084/2019DeskLogin
    if(installation_type.equalsIgnoreCase("shared"))
    {
        String RedirectURL = login_url;
        response.sendRedirect(RedirectURL);
    }
    else
    {
        boolean license_error = false;
        boolean password_error = false;
        String error = "";
        try
        {
            error = request.getParameter("error"); //License Limit reached!   or   Username/Password incorrect!
            System.out.println("Error=" + error + "=");
            if(error != null)
            {
                if(error.equalsIgnoreCase("License"))
                {
                    license_error = true;
                }
                if(error.equalsIgnoreCase("Password"))
                {
                    password_error = true;
                }
            }
        }
        catch(Exception e)
        {
            error = "";
            System.out.println("Exception=" + e + "=");
        }
%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Login Page - ServeOptics Desk</title>
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.ico">
        <link rel="stylesheet" type="text/css" href="assets/css/fonts_css.css">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/icheck/icheck.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/icheck/custom.css">
        <!-- END VENDOR CSS-->
        <!-- BEGIN MODERN CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
        <!-- END MODERN CSS-->
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/pages/login-register.css">
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <!-- END Custom CSS-->
    </head>
    <body class="horizontal-layout horizontal-menu 1-column   menu-expanded blank-page blank-page" data-open="hover" data-menu="horizontal-menu" data-col="1-column">
        <!-- ////////////////////////////////////////////////////////////////////////////-->
        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <section class="flexbox-container">
                        <div class="col-12 d-flex align-items-center justify-content-center">
                            <div class="col-md-4 col-10 box-shadow-2 p-0">
                                <div class="card border-grey border-lighten-3 m-0">
                                    <div class="card-header border-0">
                                        <div class="card-title text-center">
                                            <div class="p-1"><img src="app-assets/images/RDT_black_100x100.png" alt="rdt logo"></div>
                                        </div>
                                        <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>ServeOptics Desk Login</span></h6>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <form class="form-horizontal form-simple" method="post" action="login">
                                                <%
                                                if(license_error)
                                                {
                                                    %>
                                                    <div class="alert alert-danger alert-dismissible mb-2" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <strong>License limit reached!</strong> License limit reached contact your administrator.
                                                    </div>
                                                    <%
                                                }
                                                if(password_error)
                                                {
                                                    %>
                                                    <div class="alert alert-warning alert-dismissible mb-2" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <strong>Warning!</strong> Username/Password is incorrect!</a>.
                                                    </div>
                                                    <% 
                                                }
                                                %>
                                                
                                                <fieldset class="form-group position-relative has-icon-left mb-0">
                                                    <input type="text" class="form-control form-control-lg input-lg" id="username" name="username" placeholder="Your Username" required>
                                                    <div class="form-control-position">
                                                        <i class="ft-user"></i>
                                                    </div>
                                                </fieldset>
                                                <fieldset class="form-group position-relative has-icon-left">
                                                    <input type="password" class="form-control form-control-lg input-lg" id="password" name="password" placeholder="Enter Password" required>
                                                    <div class="form-control-position">
                                                        <i class="la la-key"></i>
                                                    </div>
                                                </fieldset>
                                                <div class="form-group row">
                                                    <div class="col-md-6 col-12 text-center text-md-left">
                                                        <fieldset>
                                                            <input type="checkbox" id="remember-me" class="chk-remember">
                                                            <label for="remember-me"> Remember Me</label>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-12 ">
                                                        <fieldset>
                                                            <label for="home_selection"> Dashboard:</label>
                                                            <input type="radio" name="home_selection" value="analytics">&nbsp;Analytics
                                                            &nbsp;&nbsp;
                                                            <input checked type="radio" name="home_selection" value="service_desk">&nbsp;Service&nbsp;Desk
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn btn-info btn-lg btn-block"><i class="ft-unlock"></i> Login</button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <!--<div class="">
                                            <p class="float-sm-left text-center m-0"><a href="recover-password.html" class="card-link">Recover password</a></p>
                                            <p class="float-sm-right text-center m-0">New to Moden Admin? <a href="register-simple.html" class="card-link">Sign Up</a></p>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </div>
        <!-- ////////////////////////////////////////////////////////////////////////////-->

        <!-- BEGIN VENDOR JS-->
        <script src="app-assets/vendors/js/vendors.min.js"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="app-assets/vendors/js/charts/jquery.sparkline.min.js"></script>
        <script src="app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
        <script src="app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN MODERN JS-->
        <script src="app-assets/js/core/app-menu.js"></script>
        <script src="app-assets/js/core/app.js"></script>
        <!-- END MODERN JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="app-assets/js/scripts/ui/breadcrumbs-with-stats.js"></script>
        <script src="app-assets/js/scripts/forms/form-login-register.js"></script>
        <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%
    }
}
catch(Exception e)
{
    System.out.println("Exception in index.jsp=" + e);
}
%>

<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->

        <%
        String field_name = request.getParameter("field_name");
        String field_label = request.getParameter("field_label");
        String number_cols = request.getParameter("number_cols");
        String field_values = request.getParameter("field_values");   
        String new_record = request.getParameter("new_record"); 
        String default_text = request.getParameter("default_text"); 
        String db_value = request.getParameter("db_value"); 
        String value = "";
        if(new_record.equalsIgnoreCase("true"))
        {
            value = default_text;
        }
        else
        {
            value = db_value;
        }
        
        %>        
        <div class="col-<%=number_cols%>">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    <%=field_label%>
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="<%=field_name%>" name="<%=field_name%>" value="<%=value%>" class="form-control">
            </div>
        </div>
        
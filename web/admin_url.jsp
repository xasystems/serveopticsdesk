<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        if(!session.getAttribute("administration").toString().equalsIgnoreCase("true"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            String installation_type = props.get("installation_type").toString().trim();
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);     
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String serveoptics_url = db.get_configuration_properties.by_name(con, "serveoptics_url").trim();
                
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <!-- END PAGE LEVEL CSS-->
    

                    
    <div class="clr whtbg p-10 mb-15">
        <h1 class="large-font boldFont">Base URL</h1>
    </div>

    <div class="clr whtbg p-30 mb-15">

        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="admin_url" method="post">
                                <p class="card-text">Base URL</p>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="serveoptics_url">URL</label>
                                            <%
                                            if(installation_type.equalsIgnoreCase("shared"))
                                            {
                                                %>
                                                <input READONLY type="text" id="email_host" name="serveoptics_url" class="form-control" value="<%=serveoptics_url%>">
                                                <%
                                            }
                                            else
                                            {
                                                %>
                                                <input type="text" id="email_host" name="serveoptics_url" class="form-control" value="<%=serveoptics_url%>">
                                                <% 
                                            }
                                            %>
                                        </div>
                                    </div>  
                                </div>                                        
                                <div class="form-actions">
                                    <button type="button" onclick="javascript:location.href='admin.jsp'" class="btn btn-warning mr-1">
                                        <i class="ft-x"></i> Cancel
                                    </button>
                                    <button type="submit" class="btn btn-info">
                                        &nbsp;&nbsp;<i class="ft-save"></i> Save&nbsp;&nbsp;
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- end content here-->
    </div>
                                        
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->   
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_user.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_user.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

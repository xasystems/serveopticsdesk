<%@page import="org.apache.commons.lang3.BooleanUtils"%>
<!--Copyright 2021 Real Data Technologies, Inc. , All rights reserved.-->

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="org.apache.commons.text.StringEscapeUtils"%>
<%@ page import="java.time.format.DateTimeFormatter"%>

<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        //session vars
        boolean page_authorized = false;
        boolean incident_authorized = support.role.authorized(session, "incident","create");      
        boolean request_authorized = support.role.authorized(session, "request","create");   
        if(!incident_authorized || !request_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            String user_tz_name = "UTC";
            String user_tz_time = "+00:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "UTC";
                user_tz_time = "+00:00";
            }
            Boolean embedded = request.getParameter("embedded") != null && BooleanUtils.toBoolean(request.getParameter("embedded"));
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
            date_time_picker_format.setTimeZone(TimeZone.getTimeZone(user_tz_name));
            java.util.Date now = new java.util.Date();
            
            String start_working_on_message_time = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(support.date_utils.now_in_utc());
            String username = session.getAttribute("username").toString();
            String customer_id = session.getAttribute("db").toString();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            Connection con = null;
            try
            {
                con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
            }
            catch(Exception e)
            {
                System.out.println("message_process.jsp exception=" + e);
            }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////            
            String message_id = request.getParameter("id");
            String message_info[] = db.get_message.message_by_id(con, message_id);
            String date_sent = "";
            try
            {
                date_sent = date_time_picker_format.format(timestamp_format.parse(message_info[3]));
            }
            catch(Exception e)
            {
                date_sent = "";
            }
            String status_date = "";
            try
            {
                status_date = date_time_picker_format.format(timestamp_format.parse(message_info[11]));
            }
            catch(Exception e)
            {
                status_date = "";
            }
            String date_assigned = "";
            try
            {
                date_assigned = date_time_picker_format.format(timestamp_format.parse(message_info[20]));
            }
            catch(Exception e)
            {
                date_assigned = "";
            }
            String date_closed = "";
            try
            {
                date_closed = date_time_picker_format.format(timestamp_format.parse(message_info[21]));
            }
            catch(Exception e)
            {
                date_closed = "";
            }
    %>
        <form action="message_process" method="post" onsubmit="return validateForm()" enctype="multipart/form-data">
            <input type="hidden" name="message_id" id="message_id" value="<%=message_id%>"/>
            <input type="hidden" name="referer" id="referer" value="message_process.jsp"/>
            <input type="hidden" name="embedded" id="embedded" value="<%=embedded%>"/>
            <input type="hidden" name="start_working_on_message_time" id="start_working_on_message_time" value="<%=start_working_on_message_time%>"/>
            <!--future fields-->
                            <!--
                            message[0] = rs.getString("id");
                            message[1] = rs.getString("uuid");
                            message[2] = support.string_utils.check_for_null(rs.getString("source"));
                            message[3] = support.string_utils.check_for_null(rs.getString("date_sent"));
                            message[4] = support.string_utils.check_for_null(rs.getString("from"));                
                            message[5] = support.string_utils.check_for_null(rs.getString("subject"));  
                            message[6] = support.string_utils.check_for_null(rs.getString("to"));  
                            message[7] = support.string_utils.check_for_null(rs.getString("body"));  
                            message[8] = support.string_utils.check_for_null(rs.getString("mime_type"));  
                            message[9] = support.string_utils.check_for_null(rs.getString("priority"));  
                            message[10] = support.string_utils.check_for_null(rs.getString("status"));  
                            message[11] = support.string_utils.check_for_null(rs.getString("status_date"));  
                            message[12] = support.string_utils.check_for_null(rs.getString("status_by_user_id"));  
                            message[13] = support.string_utils.check_for_null(rs.getString("result"));  
                            message[14] = support.string_utils.check_for_null(rs.getString("has_attachment"));  
                            message[15] = support.string_utils.check_for_null(rs.getString("description"));  
                            message[16] = support.string_utils.check_for_null(rs.getString("log"));  
                            message[17] = support.string_utils.check_for_null(rs.getString("related_to"));     
                            message[18] = support.string_utils.check_for_null(rs.getString("related_to_id"));  
                            message[19] = support.string_utils.check_for_null(rs.getString("assigned_to_id"));     
                            message[20] = support.string_utils.check_for_null(rs.getString("date_assigned"));      
                            message[21] = support.string_utils.check_for_null(rs.getString("date_closed"));      
                             -->
        <div class="row row-cols-1 row-cols-md-2">
            <div class="col">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Date Sent
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <input READONLY="true" type='text' id="date_sent" name="date_sent" value="<%=date_sent%>" class="form-control datetime" />
                </div>
            </div>
            <%
            String from = message_info[4];
            if(from.contains("<"))
            {
                String t1[] = from.split("<");
                String t2[] = t1[1].split(">");
                from = t2[0];
            }
            boolean user_exists = db.get_users.email_exists(con, from);
            %>  
            <div class="col">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        From
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <input READONLY="true" name="from" id="from" type="text" value="<%=from%>" class="form-control"/>
                    <%
                    if(!user_exists)
                    {
                        %>
                        <input type="hidden" name="user_exists" id="user_exists" value="false"/>
                        <a data-target="user" data-message-id="<%=message_id%>" class="btn btn-success customBtn mt-2 lg waves-effect rightSidePanelUserOpener" href="javascript:void(0)">
                            <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>New User
                        </a>
                        <%
                    }
                    else
                    {
                        %>
                        <input type="hidden" name="user_exists" id="user_exists" value="true"/>
                        <%
                    }
                    %>
                </div>
            </div>


            <div class="col w-100">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Subject
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <input READONLY="true" type="text" name="subject" id="subject" value="<%=message_info[5]%>" class="form-control"/>
                </div>
            </div>
            <div class="col w-100">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Body
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <textarea READONLY="true" class="form-control" name="body" id="body" rows="5"><%=message_info[7]%></textarea>  
                </div>
            </div>
            <div class="w-100"></div>

            <div class="col w-100">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Attachments
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <%
                    if(message_info[14].equalsIgnoreCase("false"))
                    {
                        %>
                        No Attachments
                        <%
                    }
                    else
                    {
                        //get all attachments for uuid 
                        ArrayList<String[]> attachments = db.get_message.all_message_attachment_info_for_uuid(con, message_info[1]);
                        /*email[0] = rs.getString("id");
                        email[1] = rs.getString("uuid");
                        email[2] = support.string_utils.check_for_null(rs.getString("name_on_disk"));
                        email[3] = support.string_utils.check_for_null(rs.getString("file_name"));
                        email[4] = support.string_utils.check_for_null(rs.getString("file_date"));                
                        email[5] = support.string_utils.check_for_null(rs.getString("file_size"));  */
                        for(int a = 0; a < attachments.size(); a++)
                        {
                            String attachment[] = attachments.get(a);
                            //out.print(attachment[3] + "&nbsp;&nbsp;&nbsp;&nbsp;");
                            out.println("<a href='/attachments/" + customer_id + "/" + attachment[2] + "'download='" + attachment[3] + "'  target='_blank'>" + attachment[3] + "</a>");
                        }
                    }
                    %>
                </div>
            </div>
            <div class="col">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Status
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <%
                    String status[] = {"New","Assigned","WIP","Closed"};
                    %>
                    <!--<input name="status" id="status" type="text" value="<%=message_info[10]%>" class="form-control"/>-->
                    <select name="status" id="status" class="form-control">
                        <option value=""></option>
                        <%
                        String selected = "";
                        for(int a = 0; a < status.length; a++)
                        {
                            selected = "";
                            if(message_info[10].equalsIgnoreCase(status[a]))
                            {
                                selected = "SELECTED";
                            }
                            %>
                            <option <%=selected%> value="<%=status[a]%>"><%=status[a]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Status Date
                    </label>
                </div>
                <div class="formField clr md px-0 border-0 input-group d-flex">
                    <input type='text' id="status_date" name="status_date" value="<%=status_date%>" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Assigned To&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                        <%
                        String assigned_to_id = message_info[19];
                        String assigned_to_username = "";
                        if(assigned_to_id == null)
                        {
                            assigned_to_id = "";
                        }
                        else
                        {
                            String caller_id_info[] = db.get_users.by_id(con, assigned_to_id);
                            assigned_to_username = caller_id_info[1];                                                
                        }
                        %>
                        <input type="hidden" name="assigned_to_id" id="assigned_to_id" value="<%=assigned_to_id%>"/>
                        <input type="text" name="assigned_to_username" id="assigned_to_username" value="<%=assigned_to_username%>" class="form-control" placeholder="Enter the Assigned To username"/>
                </div>
            </div>        
            <div class="col">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Assigned Date
                    </label>
                </div>
                <div class="formField clr md px-0 border-0 input-group d-flex">
                    <input type='text' id="date_assigned" name="date_assigned" value="<%=date_assigned%>" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Closed Date
                    </label>
                </div>
                <div class="formField clr md px-0 border-0 input-group d-flex">
                    <input type='text' id="date_closed" name="date_closed" value="<%=date_closed%>" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="w-100"></div>
            <div class="col w-100">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Message Activity Log
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <textarea name="log" id="log" cols="30" rows="100" class="ckeditor">
                        <%
                        out.println("Date:" + date_time_picker_format.format(now) + "     User:" + username);
                        out.println("<br><em>Enter_your_comment_here</em>");
                        out.println(message_info[16]);
                        %>
                    </textarea>                                             
                </div>
            </div>
            <div class="separator-new mb-30 mt-30 w-100"></div>
            <div class="col w-100">
                <h4 class="mediumFont medium-font mb-20">Existing Incidents/Requests for this user</h4>
                <div class="sort-tbl-holder">
                    <table id="item_table" class="table table-striped table-striped-reverse custom-sort-table">
                        <thead>
                            <tr>
                                <th style="width:5%"></th>
                                <th class="no-sort nowrap">ID</th>
                                <th>Description</th>
                                <th>Priority</th>
                                <th>Time</th>
                                <th>State</th>

                            </tr>
                        </thead>
                        <tbody>
                            <%
                            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
                            java.util.Date object_date = new java.util.Date();
                            String clean_email = support.string_utils.clean_up_email_address(message_info[4]);
                            String user_info[] = db.get_users.by_email(con, clean_email);

                            ArrayList <String[]> table_content = db.get_user_incidents_and_requests.for_email(con, clean_email);
                            //ArrayList <String[]> table_content = new ArrayList();
                            if(table_content.size() == 0)
                            {
                                %>
                                <tr>
                                <td></td>    
                                <td>No Incidents or Requests with this users Email address.</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                </tr>
                                <%
                            }
                            else
                            {
                                for(int a = 0; a < table_content.size();a++)
                                {
                                    String date = "";
                                    String row[] = table_content.get(a);
                                    try
                                    {
                                        object_date.setTime(Long.parseLong(row[5]));
                                        date = filter_format.format(object_date);
                                    }
                                    catch(Exception e)
                                    {
                                        date = "--";
                                    }
                                    String icon = "";
                                    String priority = "";
                                    String type = row[1];
                                    String pri = row[4];
                                    if(type.startsWith("INC"))
                                    {
                                        icon = "la la-wrench";
                                    }else if(type.startsWith("REQ"))
                                    {
                                        icon = "la la-shopping-cart";
                                    }else if(type.startsWith("TSK"))
                                    {
                                        icon = "la la-building";
                                    }else if(type.startsWith("JOB"))
                                    {
                                        icon = "la la-industry";
                                    } 
                                    if(pri.equalsIgnoreCase("critical"))
                                    {
                                        priority = "critical";
                                    }else if(pri.equalsIgnoreCase("high"))
                                    {
                                       priority = "high";
                                    }else if(pri.equalsIgnoreCase("medium"))
                                    {
                                        priority = "medium";
                                    }else if(pri.equalsIgnoreCase("low"))
                                    {
                                        priority = "low";
                                    } 
                                    %>
                                    <tr>
                                    <td><input type="radio" onclick="change_options(this);" id="link_to_item" name="link_to_item" value="<%=row[1]%>"></td>
                                    <td onclick="location.href='<%=row[0]%>';" style="cursor:pointer;" nowrap><i class="<%=icon%> <%=priority%>"></i>&nbsp;<%=row[1]%></td>
                                    <td onclick="location.href='<%=row[0]%>';" style="cursor:pointer;"><%=row[3]%></td>
                                    <td onclick="location.href='<%=row[0]%>';" style="cursor:pointer;"><%=row[4]%></td>
                                    <td onclick="location.href='<%=row[0]%>';" style="cursor:pointer;"><%=date%></td>
                                    <td onclick="location.href='<%=row[0]%>';" style="cursor:pointer;"><%=row[6]%></td>
                                    </tr>
                                    <%
                                }
                            }
                            %>
                        </tbody>
                    </table>                                     
                </div>
            </div>
            <div class="col">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Action
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                        <%
                        String values[] = {"none","new_incident","new_request","follow-up_incident","follow-up_request","spam","update"};
                        String names[] = {"Take No Action","Create New Incident and Automatically Close Message","Create New Request and Automatically Close Message","Incident Follow-up and Automatically Close Message","Request Follow-up and Automatically Close Message","Mark as Spam and Automatically Close Message","Update Message"};
                        %>
                        <select name="action" id="action" class="form-control">
                            <%
                            for(int a = 0; a < values.length;a++)
                            {
                                %>
                                <option value="<%=values[a]%>"><%=names[a]%></option>
                                <%
                            }
                            %>                                      
                        </select>
                </div>
            </div>
            <div class="w-100"></div>
            <div class="col w-100 text-center pt-30 pb-30">
<!--                <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                <button type="button" class="btn btn-primary-new customBtn lg waves-effect" onclick="saveMessage()">
                    Submit
                </button>-->
                <input class="btn btn-primary-new customBtn lg waves-effect" type="submit" name="submit" value="Submit"/>
            </div>
        </div>
    </form>
<%
        }//end if not permission
    }//end if not logged in
%>
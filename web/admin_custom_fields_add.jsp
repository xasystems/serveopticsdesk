<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String FINANCIAL = session.getAttribute("financial").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String CONTACT = session.getAttribute("contact").toString();
        String CX = session.getAttribute("cx").toString();
        String SURVEY = session.getAttribute("survey").toString();
        String SLA = session.getAttribute("sla").toString();        
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        String METRIC = session.getAttribute("metric").toString();
        
        
        if(!ADMINISTRATION.equalsIgnoreCase("true"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);   
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END PAGE LEVEL CSS-->
    
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title">Form Select Options</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a></li>
                                <li class="breadcrumb-item"><a href="admin.jsp">Administration</a></li>
                                <li class="breadcrumb-item"><a href="admin_custom_fields.jsp">Custom Fields</a></li>
                                <li class="breadcrumb-item"><a href="#">Add Custom Field</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div> <!--End breadcrumbs -->
            <form action="admin_custom_fields_add" method="POST">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">                                
                                    <div class="row">
                                        <div class="col-12">
                                            Add Custom Field
                                        </div>
                                    </div>
                                </h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-2">
                                                <label>Form  <i class="la la-question" title="On which form should this new custom field be added" style="font-size: 14px;"></i></label>
                                                <div class='input-group'>
                                                    <select name="form" id="form" class="form-control">
                                                        <option value="Incident">Incident</option>
                                                        <option value="Request">Request</option>
                                                        <option value="Change">Change</option>
                                                        <option value="Problem">Problem</option>
                                                        <option value="Known_Error">Known Error</option>
                                                        <option value="Release">Release</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <label>Field Name  <i class="la la-question" title="This is name that will be stored in the database.&#13;&#10;This must be a unique name among the custom fields.&#13;&#10;Do not use spaces in the name." style="font-size: 14px;"></i></label>
                                                <div class='input-group'>
                                                    <input type="text" name="field_name" id="field_name" value="" class="form-control" placeholder="Enter the name of this field"/>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <label>Field Label  <i class="la la-question" title="This is the label shown above the field on the form" style="font-size: 14px;"></i></label>
                                                <div class='input-group'>
                                                    <input type="text" name="field_label" id="field_label" value="" class="form-control" placeholder="Enter the label of this field"/>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <label>Field Type  <i class="la la-question" title="What type of input field? &#13;&#10;Select is a drop down with predefined values for the user to select.&#13;&#10;Text is for small text items like a city name. &#13;&#10;TextArea is for large amount of text like a paragraph describing your weekend.&#13;&#10;Checkbox, just a checkbox.&#13;&#10;People-Lookup is custom ServeOptics field that will lookup users as there name is entered into the field.&#13;&#10;Group-Lookup same as people lookup except for Groups.&#13;&#10;Date is a date/time field. " style="font-size: 14px;"></i></label>
                                                <div class='input-group'>
                                                    <select onchange="add_field_type()" name="field_type" id="field_type" class="form-control">
                                                        <option value="Select">Select</option>
                                                        <option value="Text">Text</option>
                                                        <option value="TextArea">TextArea</option>
                                                        <option value="Checkbox">Checkbox</option>
                                                        <option value="People-Lookup">People-Lookup</option>
                                                        <option value="Group-Lookup">Group-Lookup</option>
                                                        <option value="Date">Date</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <label>Number of Columns  <i class="la la-question" title="Any number 1 thru 12.&#13;&#10;ServeOptics displays are divided into 12 column.&#13;&#10;Two columns is a normal size for most input fields.&#13;&#10;If all the custom fields where set to two columns, then there can be six custom fields in a single row (2 Columns x 6 Custom fields = Total 12 Columns which is one full row).&#13;&#10; if the number of columns was set to 12, then the custom field will fill an entire row on the page." style="font-size: 14px;"></i></label>
                                                <div class='input-group'>
                                                    <select  name="number_cols" id="number_cols" class="form-control">
                                                        <option value="1">1</option>
                                                        <option SELECTED value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-2" id="default_text_div" style="display:none">
                                                <label>Default Text  <i class="la la-question" title="This is the default text that will be pre-populated" style="font-size: 14px;"></i></label>
                                                <div class='input-group'>
                                                    <input type="text" name="default_text" id="default_text" value="" class="form-control" placeholder="Enter the Default Text"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-2">
                                                <label>Display Order<i class="la la-question" title="In what order should the Custom Fields be displayed?&#13;&#10;The value can be 1 thru 99" style="font-size: 14px;"></i></label>
                                                <div class='input-group'>
                                                    <select  name="index" id="index" class="form-control">
                                                        <%
                                                        for(int a = 1; a < 100; a++)
                                                        {
                                                            %>
                                                            <option value="<%=a%>"><%=a%></option>
                                                            <%
                                                        }
                                                        %>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <label>Required<i class="la la-question" title="Is this field required to be filled out" style="font-size: 14px;"></i></label>
                                                <div class='input-group'>
                                                    <input type="checkbox" id="required" name="required" value="on" /> 
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <label>Active<i class="la la-question" title="Is this field displayed on the form" style="font-size: 14px;"></i></label>
                                                <div class='input-group'>
                                                    <input type="checkbox" id="active" name="active" value="on" /> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="option_table_div" style="display:none">        
                                            <div class="col-md-12" id="additional_parameter_list" >
                                                <br>
                                                <button type="button" onclick="delete_row()" class="btn mr-1 mb-1 btn-primary">Delete Selected</button>
                                                <button type="button" onclick="add_row()" class="btn mr-1 mb-1 btn-primary">Add Values</button>
                                                <table class="table" id="option_table" border="0" cellspacing="2" cellpadding="10">
                                                    <tr>
                                                        <td>#</td>
                                                        <td><b>Value</b></td>
                                                        <td><b>Label</b></td>
                                                        <td><b>Selected</b></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        
                                        <div class="form-actions">
                                            <button type="button" onclick="javascript:location.href='admin.jsp'" class="btn btn-warning mr-1">
                                                <i class="ft-x"></i> Cancel
                                            </button>
                                            <button type="submit" class="btn btn-info">
                                                &nbsp;&nbsp;<i class="ft-save"></i> Save&nbsp;&nbsp;
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <script>
        function add_field_type() 
        {
            // code to be executed
            var field_type = document.getElementById("field_type").value; 
            //alert("add field field_type=" + field_type);
            
            if(field_type === "Select" )
            {
                //alert("This field type does have options");
                $(document.getElementById("option_table_div")).show();
                $(document.getElementById("default_text_div")).hide();
            }
            else
            {
                $(document.getElementById("option_table_div")).hide();
                $(document.getElementById("default_text_div")).show();
            }
        }
    </script>
    <script>
        function delete_row() 
        {
            /***We get the table object based on given id ***/
            var objTable = document.getElementById("option_table");
            /*** Get the current row length ***/
            var iRow = objTable.rows.length;
            /*** Initial row counter ***/
            var counter = 0;
            /*** Performing a loop inside the table ***/
            if (objTable.rows.length > 1) 
            {
                for (var i = 0; i < objTable.rows.length; i++) 
                {
                    /*** Get checkbox object ***/
                    var chk = objTable.rows[i].cells[0].childNodes[0];
                    if (chk.checked) 
                    {
                        /*** if checked we del ***/
                        objTable.deleteRow(i);
                        iRow--;
                        i--;
                        counter = counter + 1;
                    }
                }
            }
        }
    </script>
    <script>
        function add_row() 
        {
            var object_table = document.getElementById("option_table");
            var row_number = object_table.rows.length;            
            var field_type = document.getElementById("field_type").value;            
            if(field_type !== "Select" )
            {
                //do nothing
            }
            else
            {
                /*** We insert the row by specifying the current rows length ***/
                var object_row = object_table.insertRow(object_table.rows.length);

                /*** We insert the first row cell ***/
                var checkbox_cell = object_row.insertCell(0);
                /*** We  insert a checkbox object ***/
                var objInputCheckBox = document.createElement("input");
                objInputCheckBox.type = "checkbox";
                checkbox_cell.appendChild(objInputCheckBox);

                /*** field ***/
                var field_value_cell = object_row.insertCell(1);
                /*** We  add some text inside the celll ***/
                field_value_cell.innerHTML = "<input class='form-control' type='text' name='option_value" + "~" + row_number + "' value=''/>";

                /*** Operator ***/
                var field_label_cell = object_row.insertCell(2);
                field_label_cell.innerHTML = "<input class='form-control' type='text' name='option_label" + "~" + row_number + "' value=''/>";
                
                
                var selected_cell = object_row.insertCell(3);
                selected_cell.innerHTML = "<input type='radio' id='selected' name='selected' value='" + row_number + "'>";
            }
        }
    </script>
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <!-- END PAGE LEVEL JS-->   
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_systen_select_fields.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_systen_select_fields.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

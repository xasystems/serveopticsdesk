<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : incident_survey
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.text.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.io.StringReader"%>
<%@ page import="javax.json.Json"%>
<%@ page import="javax.json.JsonArray"%>
<%@ page import="javax.json.JsonObject"%>
<%@ page import="javax.json.JsonReader"%>
<%@ page import="java.lang.*"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else 
    {
        if (!session.getAttribute("incident").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            if(request.getHeader("referer").contains("_survey"))
            {
                session.setAttribute("csat_survey_referer", request.getHeader("referer"));
            }
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("dd-MMM-yy / HH:mm ");//18:31 / 14-Jan-19
            //SimpleDateFormat filter_format2 = new SimpleDateFormat("yyyy"); //01/12/2019 12:00 AM
            java.util.Date filter_start = new java.util.Date();
            java.util.Date filter_end = new java.util.Date();
            String start = "";
            String end = "";
            ArrayList<String[]> groups = db.get_groups.all(con);
            String customer_group_id = "";
            String support_group_id = "";
            String date_range = "";
            String selected = "";
            try 
            {
                date_range = request.getParameter("date_range");
                if (date_range.equalsIgnoreCase("null") || date_range == null) 
                {
                    date_range = support.filter_dates.past_30_days();
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                date_range = support.filter_dates.past_30_days();
            }
            try 
            {
                customer_group_id = request.getParameter("customer_group_id");
                if (customer_group_id.equalsIgnoreCase("null") || customer_group_id == null) 
                {
                    customer_group_id = "all";
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                customer_group_id = "all";
            }
            try 
            {
                support_group_id = request.getParameter("support_group_id");
                if (support_group_id.equalsIgnoreCase("null") || support_group_id == null) 
                {
                    support_group_id = "all";
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                support_group_id = "all";
            }
            try 
            {
                //01/12/2019 12:00 AM - 01/12/2019 11:59 PM&group_id=all
                //System.out.println("date_range=" + date_range);
                String temp[] = date_range.split("-");
                filter_start = filter_format.parse(temp[0]);
                //filter_start = filter_format.parse("01/12/2019 12:00 AM");
                
                filter_end = filter_format.parse(temp[1]);
                //System.out.println("filter_end=" + temp[1].trim() + " ====" + filter_end);

                start = timestamp.format(filter_start);
                end = timestamp.format(filter_end);
            }
            catch(Exception e)
            {
                System.out.println("Exception in incident_survey_detail.jsp:=" + e);
            }
            String survey_id = request.getParameter("survey_id");
            String survey_info[] = db.get_surveys.by_id(con, survey_id);
            String survey_results[][] = db.get_survey_results.for_survey_id_start_date_end_date_support_group_id_customer_group_id(con, survey_id, filter_start, filter_end, support_group_id, customer_group_id);
            double survey_score[] = db.get_survey_results.csat_score_for_survey_results(survey_results);
            String trigger_object = survey_info[8];    
            String survey_questions[][] = db.get_surveys.csat_questions_by_survey_id(con, survey_id);
            
            DecimalFormat two_decimals = new DecimalFormat("0.00");
            DecimalFormat no_decimals = new DecimalFormat("#");
            
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    
    
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/app.css">-->
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <!--<link rel="stylesheet" type="text/css" href="assets/css/style.css">-->
    <!-- END Custom CSS-->
    <input type="hidden" name="survey_id" id="survey_id" value="<%=survey_id%>"/>

    <div class="clr whtbg p-10 mb-15 position-relative">
        <div class="float-right options">
            <ul>
                <li id="filters_dropdown" class="dInlineBlock dropdown ">
                    <div class="dInlineBlock clr menuHolder">
                        <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                        </a>
                        <div class="dropdown-menu dropdown-menu-right md p-10" x-placement="bottom-end" style="position: absolute; transform: translate3d(100px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                            <h5 class="medium-font boldFont pt-5 mb-10">All Filters</h5>
                            <form id="filter_form">
                                <ul>
                                    <li class="mb-10 clr">
                                        <label class="field-label full-width">Date Range</label>
                                        <div class="formField md  mr-0 full-width">
                                            <input class="no-border full-width datetime" type="text" name="filter_date_range" id="filter_date_range" value="<%=date_range%>">
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <label class="field-label full-width">Support Group</label>
                                        <div class="formField md  mr-0 full-width">
                                            <select class="no-border" name="support_group_id" id="support_group_id">
                                                <option value="all">All</option>
                                                <%
                                                for (int a = 0; a < groups.size(); a++) 
                                                {
                                                    String this_group[] = groups.get(a);
                                                    selected = "";
                                                    if (this_group[0].equalsIgnoreCase(support_group_id)) 
                                                    {
                                                        selected = "SELECTED";
                                                    }
                                                    %>
                                                    <option <%=selected%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                    <%
                                                }
                                                %>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <label class="field-label full-width">Customer Group</label>
                                        <div class="formField md  mr-0 full-width">
                                            <select class="no-border" name="customer_group_id" id="customer_group_id">
                                                <option value="all">All</option>
                                                <%
                                                for (int a = 0; a < groups.size(); a++) 
                                                {
                                                    String this_group[] = groups.get(a);
                                                    selected = "";
                                                    if (this_group[0].equalsIgnoreCase(customer_group_id)) 
                                                    {
                                                        selected = "SELECTED";
                                                    }
                                                    %>
                                                    <option <%=selected%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                    <%
                                                }
                                                %>
                                            </select>
                                        </div>
                                    </li>

                                </ul>
                                <div class="text-center pt-5 pb-5">
                                    <button type="button" class="btn md2 btn-primary-new customBtn mb-5" onclick="reload_page()">Apply Filters</button>
                                    <div class="clr"></div>
                                    <a href="#" class="">Reset Filters</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </li>

            </ul>
        </div>
        <h1 class="large-font boldFont">CSAT Survey Detail</h1>
    </div>

    <div class="sort-tbl-holder">
        <div class="top clr mb-15">
            <h4 class="boldFont large-font pt-15">Customer Satisfaction (CSAT)</h4>
        </div>            
        <table id="data_table" class="table table-striped custom-sort-table zero-configuration" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Survey Name</th>
                    <th>Sent</th>
                    <th>Completed</th>
                    <th>Completion Rate</th>
                    <th>Avg. Score</th>
                    <!--<th>Avg. Monthly Score <em>(Past 12 Months)</em></th>-->
                </tr>
            </thead>
                <tbody>
                    <tr>
                        <td><%=survey_info[2]%></td>                                            
                        <td><%=no_decimals.format(survey_score[0])%></td>      
                        <td><%=no_decimals.format(survey_score[1])%></td>
                        <td><%=two_decimals.format(survey_score[2])%>%</td>
                        <td><%=two_decimals.format(survey_score[3])%></td>
                    </tr>
                </tbody>
        </table>
    </div>


    <div class="sort-tbl-holder">
        <div class="top clr mb-15">
            <h4 class="boldFont large-font pt-15">Customer Satisfaction (CSAT)</h4>
        </div>            
        <table id="data_table" class="table table-striped custom-sort-table zero-configuration" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Survey Question</th>
                    <th>Question Type</th>
                    <th>Avg. Score</th>
                    <th>Most Negative</th>
                    <th>Negative</th>
                    <th>Neutral</th>
                    <th>Positive</th>
                    <th>Most Positive</th>
                </tr>

            </thead>
                <tbody>
                <%
                String question_type = "";
                for(int a = 0; a < survey_questions.length;a++)
                {
                    double question_score[] = db.get_survey_results.csat_for_survey_id_survey_question(survey_id,survey_questions[a][0],survey_results);
                    Double avg_score = question_score[0];
                    if(avg_score.isNaN())
                    {
                        question_score[0] = 0.0;
                    }


                    if(survey_questions[a][1].equalsIgnoreCase("rating_scale_5"))
                    {
                        question_type = "Rating Scale (5)";
                        %>
                        <tr>
                        <td><%=survey_questions[a][2]%></td>                                            
                        <td><%=question_type%></td> 
                        <td><%=two_decimals.format(question_score[0])%></th>
                        <td><%=no_decimals.format(question_score[1])%>&nbsp;&nbsp;<small>(<%=survey_questions[a][3]%>)</small></td>
                        <td><%=no_decimals.format(question_score[2])%>&nbsp;&nbsp;<small>(<%=survey_questions[a][4]%>)</small></td>
                        <td><%=no_decimals.format(question_score[3])%>&nbsp;&nbsp;<small>(<%=survey_questions[a][5]%>)</small></td>
                        <td><%=no_decimals.format(question_score[4])%>&nbsp;&nbsp;<small>(<%=survey_questions[a][6]%>)</small></td>
                        <td><%=no_decimals.format(question_score[5])%>&nbsp;&nbsp;<small>(<%=survey_questions[a][7]%>)</small></td>
                        </tr>
                        <%
                    }
                    else
                    {
                        if(survey_questions[a][1].equalsIgnoreCase("true_false"))
                        {
                            question_type = "True/False";
                            %>
                            <tr>
                            <td><%=survey_questions[a][2]%></td>                                            
                            <td><%=question_type%></td> 
                            <td><%=two_decimals.format(question_score[0])%></th>
                            <td><%=no_decimals.format(question_score[1])%>&nbsp;&nbsp;<small>(<%=survey_questions[a][3]%>)</small></td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td><%=no_decimals.format(question_score[5])%>&nbsp;&nbsp;<small>(<%=survey_questions[a][7]%>)</small></td>
                            </tr>
                            <%
                        }
                        else
                        {
                            if(survey_questions[a][1].equalsIgnoreCase("opinion"))
                            {
                                question_type = "Opinion";
                                //level 1 anwers is the number of comments left
                                %>
                                <tr>
                                <td><%=survey_questions[a][2]%></td>                                            
                                <td><%=question_type%></td> 
                                <td>--</th>
                                <td colspan="5">
                                    <%
                                        if(question_score[1] == 0)
                                        {
                                            %>
                                            No user entered text
                                            <%
                                        }
                                        else
                                        {
                                            out.print(no_decimals.format(question_score[1]) + " User Entries");
                                        }
                                    %>
                                </td>
                                </tr>
                                <%
                            }
                        }
                    }
                }
                %>
                </tbody>

        </table>
    </div>

    <div class="sort-tbl-holder">
        <div class="top clr mb-15">
            <h4 class="boldFont large-font pt-15">Customer Satisfaction (CSAT)</h4>
        </div>            
        <table id="data_table" class="table table-striped custom-sort-table zero-configuration" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Date Sent</th>
                    <th>Date Completed</th>
                    <th>User Name</th>
                    <th>Overall score</th>
                    <%
                    if(trigger_object.equalsIgnoreCase("incident"))
                    {
                        %>
                        <th>Incident ID</th>
                        <%
                    }
                    else
                    {
                        if(trigger_object.equalsIgnoreCase("request"))
                        {
                            %>
                            <th>Request ID</th>
                            <%
                        }
                        else
                        {
                            %>
                            <th>Request ID</th>
                            <%
                        }
                    }
                    %>
                </tr>
            </thead>
            <tbody>
                <%
                String table_rows[][] = db.get_survey_results.csat_survey_detail_table(con, survey_results);
                for(int a = 0; a < table_rows.length; a++)
                {
                    java.util.Date sent_date = new java.util.Date();
                    //System.out.println("table_rows[a][0]=" + table_rows[a][0]);
                    sent_date = timestamp_format.parse(table_rows[a][0]);
                    String c_date = "";
                    try
                    {
                        java.util.Date completed_date = new java.util.Date();
                        completed_date = timestamp_format.parse(table_rows[a][1]);
                        c_date = display_format.format(completed_date);
                    }
                    catch(Exception e)
                    {
                        c_date = "";
                    }
                    %>
                    <tr>
                    <td><%=display_format.format(sent_date)%></td>                                            
                    <td><%=c_date%></td> 
                    <td><%=table_rows[a][2]%></th>
                    <td><%=table_rows[a][3]%></td>
                    <td><%=table_rows[a][5]%></td>
                    </tr>
                    <%
                }
                %>
            </tbody>

        </table>
    </div>

    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
    </body>
    <!-- BEGIN VENDOR JS-->
    <!--<script src="app-assets/vendors/js/vendors.min.js"></script>-->
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/charts/jquery.sparkline.min.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <!--<script src="app-assets/js/core/app-menu.js"></script>-->
    <!--<script src="app-assets/js/core/app.js"></script>-->
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!--<script src="app-assets/js/scripts/ui/breadcrumbs-with-stats.js"></script>-->
    <script src="app-assets/js/scripts/tables/datatables/datatable-styling.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
    
    <!-- END PAGE LEVEL JS-->
    
    
    <!-- END PAGE LEVEL JS-->
    <script>
        function reload_page()
        {
            var support_group_id = document.getElementById("support_group_id").value;
            var customer_group_id = document.getElementById("customer_group_id").value;
            var filter_date_range = document.getElementById("filter_date_range").value;
            var survey_id = document.getElementById("survey_id").value;
            var URL = "csat_survey_detail.jsp?survey_id=" + survey_id + "&date_range=" + filter_date_range + "&support_group_id=" + support_group_id + "&customer_group_id=" + customer_group_id;
            window.location.href = URL;
        }
    </script>
</html>
<%
        }//end if not permission
    }//end if not logged in
%>
<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else
    {
        //project CRUD
        boolean page_authorized = support.role.authorized(session, "project","delete");                
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            try
            {
                String context_dir = request.getServletContext().getRealPath("");
                LinkedHashMap props = support.config.get_config(context_dir);
                Connection con = null;
                String selected = "";
                try
                {
                    con = db.db_util.get_contract_connection(context_dir, session);
                    String user_id = session.getAttribute("user_id").toString();
                    boolean update_session_table = db.get_user_sessions.update(con, user_id);
                }
                catch(Exception e)
                {
                    System.out.println("project.jsp exception=" + e);
                }
                String project_id = request.getParameter("project_id");
                String project_info[] = db.get_projects.project_by_id(con, project_id);
                
                //get all tasks for this project
                ArrayList<String[]> project_tasks = db.get_projects.project_tasks_for_project(con, project_id);

    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END PAGE LEVEL CSS-->
    
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title">Project Delete</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a></li>
                                <li class="breadcrumb-item"><a href="projects_list.jsp">Projects</a></li>
                                <li class="breadcrumb-item"><a href="project.jsp?project_id=<%=project_id%>">Project <%=project_info[1]%></a></li>
                                <li class="breadcrumb-item"><a href="#">Delete Project</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div> <!--End breadcrumbs -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Delete Project (<%=project_info[2]%>)</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <div class="alert bg-warning alert-icon-right alert-arrow-right alert-dismissible mb-2" role="alert">
                                    <span class="alert-icon"><i class="la la-warning"></i></span>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Warning!</strong> 
                                    This action can not be undone! 
                                    <br>
                                    This Project has <%=project_tasks.size()%> Task(s).
                                    <br>
                                    All the Tasks will be deleted too.
                                    <br>
                                    <br>
                                    &nbsp;&nbsp;&nbsp;
                                    <button onclick="javascript:location.href='project_delete?project_id=<%=project_id%>'" class="btn btn-info mr-1">
                                        Delete Project
                                    </button>
                                        &nbsp;
                                    <button type="button" onclick="javascript:location.href='project.jsp?project_id=<%=project_id%>'" class="btn btn-blue-grey mr-1">
                                        <i class="ft-x"></i> Cancel and Go Back to Project
                                    </button>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->   
    <%
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_user_delete.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_user_delete.jsp:" + e);
            }
        } //end if not admin permission
    } //end if not redirected
    %>
    </body>
</html>

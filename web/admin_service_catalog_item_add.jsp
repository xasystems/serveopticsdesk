<!--Copyright 2020 Real Data Technologies, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_service_catalog_item_add.jsp
    Created on : 18-Jan-2020, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>

<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String FINANCIAL = session.getAttribute("financial").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String CONTACT = session.getAttribute("contact").toString();
        String CX = session.getAttribute("cx").toString();
        String SURVEY = session.getAttribute("survey").toString();
        String SLA = session.getAttribute("sla").toString();        
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        
        
        
        if (ADMINISTRATION.equalsIgnoreCase("true") || MANAGER.equalsIgnoreCase("true")) 
        {
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
            java.util.Date now = new java.util.Date();
            String now_display_format = display_format.format(now);
            String now_timestamp_format = timestamp_format.format(now);
            String now_incident_time = date_time_picker_format.format(now);
            Connection con = null;
            try
            {
                con = db.db_util.get_contract_connection(context_dir, session); 
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
            }
            catch(Exception e)
            {
                System.out.println("admin_service_catalog_add.jsp exception=" + e);
            }
    %>
    <form action="admin_service_catalog_item_add" method="post" enctype="multipart/form-data" onsubmit="return validateForm()">
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Name
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="name" name="name" value="" class="form-control">
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Description
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="description" name="description" value="" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Catalog
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="catalog_id" id="catalog_id" class="form-control">
                        <%
                        ArrayList <String[]> all_catalogs = db.get_service_catalog.service_catalog_all(con);
                        for(int a = 0; a < all_catalogs.size();a++)
                        {
                            String catalog[] = all_catalogs.get(a);
                            %>
                            <option value="<%=catalog[0]%>"><%=catalog[1]%></option>        
                            <%
                        }
                        %>                                                    
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Category
                    <i class="la la-question" style="font-size: 14px;" title="Category is used to group like items in the Catalog. Like a retail catalog ('Women Shoes','Mens Cloths','Housewares'), the Service Catalog items can be catagorized into like items, such as 'Laptop', 'Desktop','Desktop Software', etc..."></i>
                    &nbsp;
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="category" name="category" value="" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">                    
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Description
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="state" id="state" class="form-control">
                        <%
                        String[] state_names = {"Draft","Awaiting Approval","Published","Superceded","Expired","Invalid","Deleted"};
                        String[] state_values = {"Draft","Waiting_Approval","Published","Superceded","Expired","Invalid","Deleted"};

                        for(int a = 0; a < state_names.length; a++)
                        {
                            %>
                            <option value="<%=state_values[a]%>"><%=state_names[a]%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md">
                        Image
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="clipper_result" name="file" onchange="window.clipper_app.$children[0].upload(event)">
                        <label class="custom-file-label" for="file">Choose file(s)</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row d-none" id="clipper_preview_row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only">
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <div class="clr dInlineBlock formField md whtbg mr-5 mb-15 float-none w-auto">
                        <span>
                            <img width="100px" id="clipper_preview" src="" />
                            <a href="javascript:void(0)" onclick="$(this).closest('div.row').addClass('d-none');$('#clipper_result').val('');event.preventDefault" >
                                <img class="icon pl-2" src="assets/images/svg/cross-icon.svg" alt="">
                            </a>
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="mt-30 mb-30 separator-new darkLighterBg"></div>
        <h4 class="mediumFont large-font mb-20">Cost and SLA By Priority</h4>
        <h4 class="mediumFont basic-font mb-20">Critical</h4>
        <div class="row">  
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Fixed Cost
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="critical_fixed_cost" name="critical_fixed_cost" value="0.00" class="form-control">
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Recurring Cost
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="critical_recurring_cost" name="critical_recurring_cost" value="0.00" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">  
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Recurring Cost Frequency
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="critical_recurring_cost_frequency" id="critical_recurring_cost_frequency" class="form-control">
                        <%
                        String option_name[] = {"Hour","Day","Week","Bi-Monthly","Monthly", "Quarter", "Annual"};
                        for(int a = 0; a < option_name.length; a++)
                        {
                            %>
                            <option value="<%=option_name[a]%>"><%=option_name[a]%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>                                        
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Delivery Time
                    <i class="la la-question" style="font-size: 14px;" title="This time is used to calculate the SLA.  Request time to Delivery"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="critical_delivery_time" name="critical_delivery_time" value="0" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">  
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Time Units
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="critical_delivery_time_unit" id="critical_delivery_time_unit" class="form-control">
                        <%
                        String unit_option_name[] = {"Minutes","Hours","Days"};
                        for(int a = 0; a < unit_option_name.length; a++)
                        {
                            %>
                            <option value="<%=unit_option_name[a]%>"><%=unit_option_name[a]%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>  
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    SLA Schedule
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="critical_schedule_id" id="critical_schedule_id" class="form-control">
                        <%
                        //get all the schedules
                        String schedules[][] = db.get_schedules.all(con);
                        for(int a = 0; a < schedules.length; a++)
                        {
                            %>
                            <option value="<%=schedules[a][0]%>"><%=schedules[a][1]%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">  
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        <br>Delivery Group&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="hidden" name="critical_delivery_group_id" id="critical_delivery_group_id" value="0"/>
                    <input type="text" name="critical_delivery_group_name" id="critical_delivery_group_name" placeholder="Group Unassigned" class="form-control" />
                </div>
            </div>
        </div>
        <div class="mt-30 mb-30 separator-new darkLighterBg"></div>
        <h4 class="mediumFont basic-font mb-20">High</h4>
        <div class="row">  
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Fixed Cost
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="high_fixed_cost" name="high_fixed_cost" value="0.00" class="form-control">
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Recurring Cost
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="high_recurring_cost" name="high_recurring_cost" value="0.00" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">  
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Recurring Cost Frequency
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="high_recurring_cost_frequency" id="high_recurring_cost_frequency" class="form-control">
                        <%
                        for(int a = 0; a < option_name.length; a++)
                        {
                            %>
                            <option value="<%=option_name[a]%>"><%=option_name[a]%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>                                        
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Delivery Time
                    <i class="la la-question" style="font-size: 14px;" title="This time is used to calculate the SLA.  Request time to Delivery"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="high_delivery_time" name="high_delivery_time" value="0" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">  
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Time Units
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="high_delivery_time_unit" id="high_delivery_time_unit" class="form-control">
                        <%
                        for(int a = 0; a < unit_option_name.length; a++)
                        {
                            %>
                            <option value="<%=unit_option_name[a]%>"><%=unit_option_name[a]%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>  
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    SLA Schedule
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="high_schedule_id" id="high_schedule_id" class="form-control">
                        <%
                        //get all the schedules
                        for(int a = 0; a < schedules.length; a++)
                        {
                            %>
                            <option value="<%=schedules[a][0]%>"><%=schedules[a][1]%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">  
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        <br>Delivery Group&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="hidden" name="high_delivery_group_id" id="high_delivery_group_id" value="0"/>
                    <input type="text" name="high_delivery_group_name" id="high_delivery_group_name" placeholder="Group Unassigned" class="form-control" />
                </div>
            </div>
        </div>
        <div class="mt-30 mb-30 separator-new darkLighterBg"></div>
        <h4 class="mediumFont basic-font mb-20">Medium</h4>
        <div class="row">  
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Fixed Cost
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="medium_fixed_cost" name="medium_fixed_cost" value="0.00" class="form-control">
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Recurring Cost
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="medium_recurring_cost" name="medium_recurring_cost" value="0.00" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">  
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Recurring Cost Frequency
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="medium_recurring_cost_frequency" id="medium_recurring_cost_frequency" class="form-control">
                        <%
                        for(int a = 0; a < option_name.length; a++)
                        {
                            %>
                            <option value="<%=option_name[a]%>"><%=option_name[a]%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>                                        
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Delivery Time
                    <i class="la la-question" style="font-size: 14px;" title="This time is used to calculate the SLA.  Request time to Delivery"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="medium_delivery_time" name="medium_delivery_time" value="0" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">  
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Time Units
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="medium_delivery_time_unit" id="medium_delivery_time_unit" class="form-control">
                        <%
                        for(int a = 0; a < unit_option_name.length; a++)
                        {
                            %>
                            <option value="<%=unit_option_name[a]%>"><%=unit_option_name[a]%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div> 
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    SLA Schedule
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="medium_schedule_id" id="medium_schedule_id" class="form-control">
                        <%
                        //get all the schedules
                        for(int a = 0; a < schedules.length; a++)
                        {
                            %>
                            <option value="<%=schedules[a][0]%>"><%=schedules[a][1]%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">  
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        <br>Delivery Group&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="hidden" name="medium_delivery_group_id" id="medium_delivery_group_id" value="0"/>
                    <input type="text" name="medium_delivery_group_name" id="medium_delivery_group_name" placeholder="Group Unassigned" class="form-control" />
                </div>
            </div>
        </div>
        <div class="mt-30 mb-30 separator-new darkLighterBg"></div>
        <h4 class="mediumFont basic-font mb-20">Low</h4>
        <div class="row">  
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Fixed Cost
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="low_fixed_cost" name="low_fixed_cost" value="0.00" class="form-control">
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Recurring Cost
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="low_recurring_cost" name="low_recurring_cost" value="0.00" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">  
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Recurring Cost Frequency
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="low_recurring_cost_frequency" id="low_recurring_cost_frequency" class="form-control">
                        <%
                        for(int a = 0; a < option_name.length; a++)
                        {
                            %>
                            <option value="<%=option_name[a]%>"><%=option_name[a]%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>                                        
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Delivery Time
                    <i class="la la-question" style="font-size: 14px;" title="This time is used to calculate the SLA.  Request time to Delivery"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="low_delivery_time" name="low_delivery_time" value="0" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">  
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Time Units
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="low_delivery_time_unit" id="low_delivery_time_unit" class="form-control">
                        <%
                        for(int a = 0; a < unit_option_name.length; a++)
                        {
                            %>
                            <option value="<%=unit_option_name[a]%>"><%=unit_option_name[a]%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>  
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    SLA Schedule
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="low_schedule_id" id="low_schedule_id" class="form-control">
                        <%
                        //get all the schedules
                        for(int a = 0; a < schedules.length; a++)
                        {
                            %>
                            <option value="<%=schedules[a][0]%>"><%=schedules[a][1]%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">  
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        <br>Delivery Group&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="hidden" name="low_delivery_group_id" id="low_delivery_group_id" value="0"/>
                    <input type="text" name="low_delivery_group_name" id="low_delivery_group_name" placeholder="Group Unassigned" class="form-control" />
                </div>
            </div>
        </div>
        <div class="mt-30 mb-30 separator-new darkLighterBg"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md mb-15">
                        Required Information requested from the User
                    </label>
                </div>
                <div class="formField clr md mb-15 p-0 no-border">
                    <textarea name="required_user_input" id="required_user_input" cols="30" rows="155" class="ckeditor">
                    </textarea>
                </div>
            </div>            
        </div>
        <div class="mt-30 mb-30 separator-new darkLighterBg"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md mb-15">
                        Optional Information requested from the User
                    </label>
                </div>
                <div class="formField clr md mb-15 p-0 no-border">
                    <textarea name="optional_user_input" id="optional_user_input" cols="30" rows="155" class="ckeditor">
                    </textarea>
                </div>
            </div>            
        </div>
        <div class="mt-30 mb-30 separator-new darkLighterBg"></div>
        <h4 class="mediumFont large-font mb-20">Request Approvers</h4>
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <li class="mb-15">
                        <div class="form-check">
                            <input checked type="radio" id="approver_radio" name="approver_radio" value="auto">
                            <label class="form-check-label" for="flexRadioDefault1">
                                Auto (All Request are Approved)
                            </label>
                          </div>
                    </li>
                    <li class="mb-15">
                        <div class="form-check">
                            <input type="radio" id="approver_radio" name="approver_radio" value="manual">
                            <label class="form-check-label" for="flexRadioDefault2">
                                Manual (Approver set by Service Desk)
                            </label>
                          </div>
                    </li>
                    <li class="mb-15">
                        <div class="form-check">
                            <input type="radio" id="approver_radio" name="approver_radio" value="users_group_poc">
                            <label class="form-check-label" for="flexRadioDefault3">
                                Users Group POC
                            </label>
                          </div>
                    </li>
                    <li class="mb-15">
                        <div class="form-check">
                            <input type="radio" id="approver_radio" name="approver_radio" value="group_anybody">
                            <label class="form-check-label" for="flexRadioDefault4">
                                Anyone assigned to these Groups
                            </label>
                        </div>
                        <div class="formField clr md border-0" style="float: left; width: 100%;">
                            <select class="select2 form-control" id="approver_group_ids" name="approver_group_ids" multiple="multiple" placeholder="Select Approver Group ">
                                <%
                                //get all groups
                                ArrayList<String[]> groups = db.get_groups.all(con);
                                String group_id = "";
                                String group_name = "";
                                for(int a = 1; a < groups.size(); a++)
                                {
                                    String this_group[] = groups.get(a);
                                    group_id = this_group[0];
                                    group_name = this_group[1];
                                    %>
                                    <option value="<%=group_id%>"><%=group_name%></option>
                                    <%
                                }
                                %>
                            </select>
                        </div>
                    </li>
                    <li class="mb-15">
                        <div class="form-check">
                            <input type="radio" id="approver_radio" name="approver_radio" value="individual">
                            <label class="form-check-label" for="flexRadioDefault4">
                                Anyone in list below:
                            </label>
                        </div>
                        <div class="clr">
                            <label class="formLabel md">
                                User (Search: Last name <em>space</em> First name
                            </label>
                            <div class="formField clr md px-0 border-0 " style="float: left; width: 100%;">
                                <input type="hidden" name="approver_id" id="approver_id" value=""/>
                                <input type='text' id="approver_name" name="approver_name" value="" placeholder="Enter Last First to search for user" class="form-control" />
                            </div>
                            <div class="clr"></div>
                            <button id="btnAdd" type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                                Add User to Approvers List
                            </button>
                        </div>
                        <div class="clr">
                            <label class="formLabel md">
                                Approvers List:
                            </label>
                            <div class="formField clr md px-0 border-0 " style="float: left; width: 100%;">
                                <select class="form-control" id="approver_list" name="approver_list" multiple>
                                </select>
                            </div>
                            <div class="clr"></div>
                            <button id="btnRemove" type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                                Remove Selected User from Approvers List
                            </button>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-12 text-center pt-30 pb-30">
                <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                    Save
                </button>
            </div>
        </div>
    </form>


    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>
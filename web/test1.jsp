<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="javax.mail.*" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.util.Properties" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    SimpleDateFormat timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    timestamp.setTimeZone(TimeZone.getTimeZone("UTC"));
    String user_id = session.getAttribute("user_id").toString();
   
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <!-- Page specific CSS-->
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <!--End Page specific CSS-->
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END Page Level CSS-->
    
    
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title"><i class="la ft-users"></i>&nbsp;&nbsp;Page Title</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="home_analytics.jsp">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Page Layouts</a>
                                </li>
                                <li class="breadcrumb-item active">Fixed Layout
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <%
            String context_dir = request.getServletContext().getRealPath("");
            String requested_fields[] = {"id","username","password"};
            
            ArrayList<String[]> results = db.get_management_server.run_query(context_dir, "SELECT * FROM users", "GET", requested_fields);
            for(int a = 0; a < results.size(); a++)
            {
                out.println("<br>");
                String result[] = results.get(a);
                for(int b = 0; b < result.length; b++)
                {
                    out.println(requested_fields[b] + ":" + result[b]);    
                }
            }
            

            boolean insert_success = db.get_management_server.run_insert(context_dir, "INSERT INTO contracts(`id`,`license`,`url`,`poc_user_id`,`active`) VALUES ('1000', 'license', 'some_url','0', 'true')");
            System.out.println("insert_success=" + insert_success);
            out.println("<br>insert_success=" + insert_success);
            
            boolean delete_success = db.get_management_server.run_insert(context_dir, "DELETE FROM contracts WHERE id='1000'");
            System.out.println("delete_success=" + insert_success);
            out.println("<br>delete_success=" + delete_success);
            
            boolean update_success = db.get_management_server.run_insert(context_dir, "UPDATE contracts set `active`='false' WHERE id='1000'");
            System.out.println("update_success=" + update_success);
            out.println("<br>update_success=" + update_success);

            %>

            
            
            
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN VENDOR JS-->    
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="app-assets/js/core/app-menu.js"></script>
    <script src="app-assets/js/core/app.js"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/popover/popover.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    </body>
    
    <script>
       $(document).ready(function() {
            $("#autocomplete").autocomplete({
                
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_incident_knowledge_article_text",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function( event, ui ) 
                { 
                    $('#autocomplete').val(ui.item.article_description);
                    //window.location.href = ui.item.value;
                    window.open("kb_view.jsp?id=" + ui.item.value, "Knowledge Base Article", "width=600,height=600,top=400,left=400");
                    return false;
                }
            });
        });
    </script>
</html>
<%
%>
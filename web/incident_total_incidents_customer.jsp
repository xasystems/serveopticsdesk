
<!--Copyright 2019 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : incident_total_incidents
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="db.get_incidents"%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        if (!session.getAttribute("incident").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            if(request.getHeader("referer").contains("incident_home"))
            {
                session.setAttribute("incident_dashboard_referer", request.getHeader("referer"));
            }
            String home_page = session.getAttribute("home_page").toString();
            ArrayList<String[]> incident_info = new ArrayList();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session); 
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
        
            //SimpleDateFormat filter_format2 = new SimpleDateFormat("yyyy"); //01/12/2019 12:00 AM
            java.util.Date filter_start = new java.util.Date();
            java.util.Date filter_end = new java.util.Date();
            String start = "";
            String end = "";
            //System.out.println("format2=" + filter_format2.parse("2016"));
            //System.out.println("format" + filter_format.parse("01/12/2019 12:00 AM"));

            ArrayList<String[]> groups = db.get_groups.all(con);
            String date_range = "";
            String group_id = "";
            String group_type = "";
            String selected = "";
            try {
                date_range = request.getParameter("date_range");
                if (date_range.equalsIgnoreCase("null") || date_range == null) {
                    date_range = support.filter_dates.past_30_days();
                }
            } catch (Exception e) {
                //if not set then default to past 30 days
                date_range = support.filter_dates.past_30_days();
            }
            try {
                group_id = request.getParameter("group_id");
                if (group_id.equalsIgnoreCase("null") || group_id == null) {
                    group_id = "all";
                }
            } catch (Exception e) {
                //if not set then default to past 30 days
                group_id = "all";
            }
            try {
                group_type = request.getParameter("group_type");
                if (group_type.equalsIgnoreCase("null") || group_type == null) {
                    group_type = "support";
                }
            } catch (Exception e) {
                //if not set then default to past 30 days
                group_type = "support";
            }
            if(group_type.equalsIgnoreCase("support"))
            {
                group_type = "Support";
            }
            else
            {
                group_type = "Customer";
            }
            
            try {
                //01/12/2019 12:00 AM - 01/12/2019 11:59 PM&group_id=all
                //System.out.println("date_range=" + date_range);
                String temp[] = date_range.split("-");
                filter_start = filter_format.parse(temp[0]);
                //filter_start = filter_format.parse("01/12/2019 12:00 AM");
                //System.out.println("filter_start=" + temp[0] + " ====" + filter_start);

                filter_end = filter_format.parse(temp[1]);
                //System.out.println("filter_end=" + temp[1].trim() + " ====" + filter_end);

                start = timestamp_format.format(filter_start);
                end = timestamp_format.format(filter_end);
                
                //get the incidents
                incident_info = db.get_incidents.incidents_start_date_stop_date_group_id(con, filter_start, filter_end, group_type, group_id);
                 
            } catch (Exception e) {
                //if not set then default to past 30 days
                System.out.println("Exception on incident_total_incidents.jsp=" + e);
            }
            
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END Custom CSS-->
    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <input type="hidden" name="start" id="start" value="<%=start%>"/>
    <input type="hidden" name="end" id="end" value="<%=end%>"/>
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title"><i class="la la-wrench"></i>&nbsp;Incident</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"> 
                                    <a href="#" onClick="parent.location='<%=session.getAttribute("incident_dashboard_referer")%>'"><i class="la la-angle-left"></i>Back</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- start content here-->
            <div class="row">
                <div class="col-xl-3 col-6">
                    <div class="form-group">
                        <label>Date Range</label>
                        <div class='input-group'>
                            <input type='text' id="filter_date_range" name="filter_date_range" value="<%=date_range%>" class="form-control datetime" />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <span class="la la-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-4">
                    <div class="form-group">
                        <label><%=group_type%> Group</label>
                        <div class='input-group'>
                            <select class="select2-placeholder form-control" name="group_id" id="group_id">
                                <option value="all">All</option>
                                <%
                                for (int a = 0; a < groups.size(); a++) 
                                {
                                    String this_group[] = groups.get(a);
                                    selected = "";
                                    if (this_group[0].equalsIgnoreCase(group_id)) {
                                        selected = "SELECTED";
                                    }
                                %>
                                <option <%=selected%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                <%
                                    }
                                %>
                            </select>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <span class="la la-group"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-1 col-2">
                    <label>&nbsp;</label>
                    <div class="form-actions">
                        <button type="button" onclick="reload_page()" class="btn btn-blue mr-1">
                            <i class="fa-filter"></i> Apply
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <table class="table table-striped table-bordered zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>Incident ID</th>
                                            <th>Priority</th>
                                            <th>Incident Time</th>
                                            <th>State</th>
                                            <th>State Time</th>
                                            <th>Customer Group</th>
                                            <th>Customer</th>
                                            <th>Category</th>
                                            <th>Sub-Category</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                        for(int a = 0; a < incident_info.size(); a++)
                                        {
                                            String this_incident[] = incident_info.get(a);
                                            String incident_time = "";
                                            try
                                            {
                                                incident_time = display_format.format(timestamp_format.parse(this_incident[2]));
                                            }
                                            catch (Exception e)
                                            {
                                                incident_time = "";
                                            }
                                            String state_date = "";
                                            try
                                            {
                                                state_date = display_format.format(timestamp_format.parse(this_incident[20]));
                                            }
                                            catch (Exception e)
                                            {
                                                state_date = "";
                                            }
                                            %>
                                            <tr onclick="location.href='incident.jsp?id=<%=this_incident[0]%>';" style="cursor:pointer;">
                                            <td><%=this_incident[0]%></td>
                                            <td><%=this_incident[14]%></th>
                                            <td><%=incident_time%></td>
                                            <td><%=this_incident[19]%></td>
                                            <td><%=state_date%></td>
                                            <td><%=this_incident[38]%></td>
                                            <td><%=this_incident[33] + " " + this_incident[35]%></td>
                                            <td><%=this_incident[9]%></td>
                                            <td><%=this_incident[10]%></td>
                                            </tr>
                                            <%
                                        }                                        
                                        %>                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Incident ID</th>
                                            <th>Priority</th>
                                            <th>Incident Time</th>
                                            <th>State</th>
                                            <th>State Time</th>
                                            <th>Customer Group</th>
                                            <th>Customer</th>
                                            <th>Category</th>
                                            <th>Sub-Category</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <script>
        function reload_page()
        {
            var group_id = document.getElementById("group_id").value;
            var filter_date_range = document.getElementById("filter_date_range").value;
            var URL = "incident_total_incidents_customer.jsp?date_range=" + filter_date_range + "&group_id=" + group_id;
            window.location.href = URL;
        }
    </script>
    <script>
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>


    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%      con.close();
        }//end if not permission
    }//end if not logged in
%>
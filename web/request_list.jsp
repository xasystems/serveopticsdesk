<%-- 
    Document   : request_total_requests
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.time.ZonedDateTime"%>
<%@ page import="java.time.format.DateTimeFormatter"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {        
        //session vars
        boolean page_authorized = support.role.authorized(session, "request","read");          
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            ArrayList<String[]> request_info = new ArrayList();
            String filter = "all";
            String start = "";
            String end = "";
            String query = "";
            String user_id = "0";
            try
            {
                user_id = session.getAttribute("user_id").toString(); 
            }
            catch(Exception e)
            {
                user_id = "0";
            }
            
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020            
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            
            String user_tz_name = "UTC";
            String user_tz_time = "+00:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }
                
            }
            catch(Exception e)
            {
                user_tz_name = "UTC";
                user_tz_time = "+00:00";
            }
            date_time_picker_format.setTimeZone(TimeZone.getTimeZone(user_tz_name));
            java.util.Date now = new java.util.Date();
            String now_picker_time = date_time_picker_format.format(now);

            String predefined = StringUtils.defaultString(request.getParameter("predefined"));
            String date_range = StringUtils.defaultString(request.getParameter("date_range"));
            String assigned_group_id = StringUtils.defaultString(request.getParameter("assigned_group_id"));
            String caller_group_id = StringUtils.defaultString(request.getParameter("caller_group_id"));
            String view = StringUtils.defaultString(request.getParameter("view"));
            String category = StringUtils.defaultString(request.getParameter("category"));
            String priority = StringUtils.defaultString(request.getParameter("priority"));
            String state = StringUtils.defaultString(request.getParameter("state"));
            String action = StringUtils.defaultString(request.getParameter("action"));
            String id = StringUtils.defaultString(request.getParameter("id"));

            String temp[] = date_range.split("-");
            if (temp.length > 1) {
                java.util.Date filter_start = new java.util.Date();
                java.util.Date filter_end = new java.util.Date();
                filter_start = filter_format.parse(temp[0]);  //filter_start = filter_format.parse("01/12/2019 12:00 AM");
                ZonedDateTime zdt_filter_start = support.date_utils.user_tz_to_utc(user_tz_name, timestamp_format.format(filter_start));
                filter_start = java.util.Date.from(zdt_filter_start.toInstant());  

                filter_end = filter_format.parse(temp[1]);  //System.out.println("filter_end=" + temp[1].trim() + " ====" + filter_end);
                ZonedDateTime zdt_filter_end = support.date_utils.user_tz_to_utc(user_tz_name, timestamp_format.format(filter_end));
                filter_end = java.util.Date.from(zdt_filter_end.toInstant());  

                start = date_time_picker_format.format(filter_start);
                end = date_time_picker_format.format(filter_end);
            }
            
    %>
<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>


<jsp:include page='menu_service_desk.jsp'>
    <jsp:param name="active_menu" value=""/>
</jsp:include>	

<!-- BEGIN PAGE LEVEL CSS-->
<style type="text/css">
    .dropdown-menu.md {
        width: 500px;
    }
    .dropdown-menu input {
        width: auto !important;
    }
</style>
<link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
<!-- END VENDOR CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/dashboard.css">
<link rel="stylesheet" type="text/css" href="app-assets/css/components.css">
<!-- END Page Level CSS-->
<!--End Page level css-->

<div class="clr whtbg p-10 mb-15">
    <div class="float-right options">
                <ul>
            <li id="filters_dropdown" class="dInlineBlock dropdown ">
                <div class="dInlineBlock clr menuHolder">
                    <a id="filters_dropdown_toggle" class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters</a>
                    <div id="filters_dropdown_menu" class="dropdown-menu dropdown-menu-right md p-10" x-placement="bottom-end" style="position: absolute; transform: translate3d(100px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <form id="filter_form" onsubmit="return false">
                            <ul>
                                <div class="row">
                                    <div class="col-md-12">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                Filters Category
                                            </label>
                                            <div class="formField md full-width mr-0">
                                                <select onchange="change_filter_category(this)" class="border-0 full-width">
                                                    <option value="filter_by_dates">Dates</option>
                                                    <option value="filter_by_users">Users/Groups</option>
                                                    <option value="filter_by_state">State/Priority/Category/Description</option>
                                                    <option value="filter_by_other">Other Attributes</option>
                                                    <option value="filter_by_predefined">Predefined Filters</option>
                                                </select>
                                            </div>
                                        </li>
                                    </div>
                                </div>
                                <div id="filter_by_dates" class="filter-category-block">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Request Time (Start)
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="request_date_start" name="request_date_start" value="<%=start%>" placeholder="Both Start and End must be set" class="border-0 full-width datetime">
                                                </div>
                                            </li>
                                        </div>
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Request Date (End)
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="request_date_stop" name="request_date_stop" value="<%=end%>" placeholder="Both Start and End must be set" class="border-0 full-width datetime">
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Create Date (Start)
                                                </label>
                                                <div class="formField md full-width mr-0">    
                                                    <input type="text" id="create_date_start" name="create_date_start" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime">
                                                </div>
                                            </li>
                                        </div>
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Create Date (End)
                                                </label>
                                                <div class="formField md full-width mr-0">                                                
                                                    <input type="text" id="create_date_stop" name="create_date_stop" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime">
                                                </div>
                                            </li> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Pending Date (Start)
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="pending_date_start" name="pending_date_start" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime"/>
                                                </div>
                                            </li>
                                        </div>
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Pending Date (End)
                                                </label>
                                                <div class='formField md full-width mr-0'>                                                
                                                    <input type="text" id="pending_date_stop" name="pending_date_stop" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime"/>
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    State Date (Start)
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="state_date_start" name="state_date_start" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime"/>
                                                </div>
                                            </li>
                                        </div>
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    State Date (End)
                                                </label>
                                                <div class='formField md full-width mr-0'>
                                                    <input type="text" id="state_date_stop" name="state_date_stop" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime"/>
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Closed Date (Start)
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="closed_date_start" name="closed_date_start" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime">
                                                </div>
                                            </li>
                                        </div>
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Closed Date (End)
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="closed_date_stop" name="closed_date_stop" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime">
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                </div>
                                <div id="filter_by_users" class="d-none filter-category-block">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Caller Name (Last First)
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="hidden" name="requested_for_id" id="requested_for_id" value="">
                                                    <input type="text" id="requested_for_name" name="requested_for_name" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                </div>
                                            </li>
                                        </div>
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Caller Group
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="hidden" name="requested_for_group_id" id="requested_for_group_id" value="">
                                                    <input type="text" id="requested_for_group_name" name="requested_for_group_name" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Assigned To (Last First)
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="hidden" name="assigned_to_id" id="assigned_to_id" value="">
                                                    <input type="text" id="assigned_to_name" name="assigned_to_name" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                </div>
                                            </li>
                                        </div>
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Assigned To Group
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="hidden" name="assigned_group_id" id="assigned_group_id" value="<%=assigned_group_id%>">
                                                    <input type="text" id="assigned_group_name" name="assigned_group_name" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Created By (Last First)
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="hidden" name="create_by_id" id="create_by_id" value="">
                                                    <input type="text" id="create_by_name" name="create_by_name" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                </div>
                                <div id="filter_by_state" class="d-none filter-category-block">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Priority
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="priority" name="priority" value="<%=priority%>" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                </div>
                                            </li>
                                        </div>
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Urgency
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="urgency" name="urgency" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Impact
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="impact" name="impact" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                </div>
                                            </li>
                                        </div>
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    State
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="state" name="state" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Category
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="category" name="category" value="<%=category%>" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                </div>
                                            </li>
                                        </div>
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Subcategory
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="subcategory" name="subcategory" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Description
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="description" name="description" value="" placeholder="Any" class="border-0 full-width">
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                </div>
                                <div id="filter_by_other" class="d-none filter-category-block">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    ID
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="id" name="id" value="<%=id%>" placeholder="Any" class="border-0 full-width">
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Location
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="location" name="location" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                </div>
                                            </li>
                                        </div>
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Site
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="site" name="site" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Department
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="department" name="department" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                </div>
                                            </li>
                                        </div>
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Company
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="company" name="company" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                </div>
                                <div id="filter_by_predefined" class="row d-none filter-category-block">
                                    <div class="col-md-12">
                                        <%
                                        Map<String, String> predefined_map = new HashMap<String, String>();
                                        predefined_map.put("all", "All");
                                        predefined_map.put("assigned_to_me", "Assigned to me");
                                        predefined_map.put("unassigned", "Unassigned");
                                        predefined_map.put("created_today", "Created today");
                                        predefined_map.put("closed_today", "Closed today");
                                        predefined_map.put("open", "Open");
                                        predefined_map.put("open_7_14", "Open 7-14 Days");
                                        predefined_map.put("open_14_30", "Open 14-30 Days");
                                        predefined_map.put("open_30_plus", "Open 30+ Days");
                                        predefined_map.put("my_critical", "My Critical");
                                        predefined_map.put("my_high", "My High");
                                        predefined_map.put("my_medium", "My Medium");
                                        predefined_map.put("my_low", "My Low");
                                        predefined_map.put("all_open_critical", "All Open Critical");
                                        predefined_map.put("all_open_high", "All Open High");
                                        predefined_map.put("all_open_medium", "All Open Medium");
                                        predefined_map.put("all_open_low", "All Open Low");
                                        for (Map.Entry<String, String> entry : predefined_map.entrySet())
                                        {
                                            String key = entry.getKey();
                                            String value = entry.getValue();
                                            %>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="predefined" id="<%=key%>" value="<%=key%>" <%=(key.equals(predefined) ? "checked" : "")%>>
                                                <label class="form-check-label" for="<%=key%>"><%=value%></label>
                                            </div>
                                            <%

                                        }
                                        %>
                                    </div>
                                </div>
                            </ul>      
                            <div class="text-center pt-5 pb-5">
                                <button class="btn md2 btn-primary-new customBtn mb-5" onclick="apply_filters();$('#filters_dropdown_toggle').dropdown('toggle');">Apply Filters</button>
                                <div class="clr"></div>
                                <a href="javascript:void(0)" onclick="resetFilters()" class="">Reset Filters</a>
                            </div>
                        </form>
                    </div>
                </div>
            </li>
            <li class="dInlineBlock">
                <a id="new_request_button" data-target="request" data-request-id="0" class="btn btn-success customBtn lg waves-effect rightSidePanelOpener" href="#">
                    <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>New Request
                </a>    
            </li>
        </ul>
    </div>
    <h1 class="large-font boldFont">Requests</h1>
</div>

<%
//        String[] filter_fields = {
//                 "serial_number",
//                 "asset_tag",
//                 "manufacturer",
//                 "model",
//                 "asset_type",
//                 "asset_subtype",
//                 "purchase_order",
//                 "warranty_expire",
//                 "state",
//                 "state_date",
//                 "assigned_to_id",
//                 "assigned_group_id",
//                 "department",
//                 "location",
//                 "site",
//                 "company",
//                 "ip",
//                 "notes"
//        };
//        List<Map<String, String>> filters = new ArrayList<>();
//        for (int i = 0; i < filter_fields.length; i++)
//        {
//            String filter_field = filter_fields[i];
//            String filter_value = request.getParameter(filter_field);
//            if (filter_value != null && !filter_value.isEmpty()) 
//            {
//                Map<String, String> map = new HashMap<String, String>();
//                map.put("key", filter_field);
//                map.put("value", filter_value);
//                filters.add(map);
//            }
//        }
//        request.setAttribute("filter_fields", filter_fields);
    %>
<div id="active_filters">
</div>

<div class="sort-tbl-holder">
    <table id="dataTable" class="table table-striped custom-sort-table d-none" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>ID</th>
                <th>Priority</th>
                <th>Status</th>
                <th>Date/Time</th>
                <th>Description</th>
                <th>Caller</th>
                <th>Assigned To</th>
            </tr>
        </thead>
    </table>
</div>
<jsp:include page='footer.jsp'>
    <jsp:param name="parameter" value=""/>
</jsp:include>	

<div class="rightPanelHolder">
    <div class="p-15 ticket-content">
        <span class="rightSidePanelCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
        <h4 id="rightPanelTitle" class="mediumFont large-font mb-20"></h4>
        <div id="rightPanelContent" class="clr whtbg p-15">
        </div>
    </div>
</div>

<div class="rightPanelHolderUser">
    <div class="p-15 ticket-content">
        <span class="rightSidePanelUserCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
        <h4 class="mediumFont large-font mb-20 panelTitle"></h4>
        <div class="clr whtbg p-15 panelContent">
        </div>
    </div>
</div>

<style>
    .overlay {
        background-color: rgba(0,0,0,0.5);
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        z-index: 999;
    }
    .loader {
        border: 16px solid #f3f3f3; /* Light grey */
        border-top: 16px solid #3498db; /* Blue */
        border-radius: 50%;
        width: 120px;
        height: 120px;
        animation: spin 2s linear infinite;
        margin: 50% auto;
    }
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>

<!-- BEGIN PAGE LEVEL JS-->
<!-- BEGIN VENDOR JS-->    
<script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN MODERN JS-->
<script src="app-assets/js/core/app-menu.js"></script>
<script src="app-assets/js/core/app.js"></script>
<!-- END MODERN JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="app-assets/js/scripts/popover/popover.js"></script>

<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>    
<script src="app-assets/js/scripts/tables/datatables-extensions/dataRender/datetime.js"></script>    
<script src="assets/js/tinymce/tinymce.min.js"></script>
<script>
    var ajax_id = "";
    var ajax_type = "";
    var dt;

    function change_filter_category(select)
    {
        $(".filter-category-block").addClass("d-none");
        var category_block = $("#" + $(select).val());
        if (category_block)
        {
            category_block.removeClass("d-none");    
        }
    }

    function apply_filters()
    {
        $("#filter_form input,#filter_form select").each(function() {

            var field = $(this);
//            console.log("filter_form_field", field);
            var field_name = field.attr('name');
            
            if (!field_name || field.attr('type') == "hidden" || (field.attr('type') == "radio" && !field.prop("checked")))
            {
                return;
            }
            if (field.val() && field.val() != "Any")
            {
                var label = "";
                var filter_label = "";
                if (field.attr("type") == "radio")
                {
                    label = "Predefined";
                    filter_label = field.siblings("label").html();
                } else {
                    label = field.closest("div").prev().html();
                    filter_label = field.val();
                }
                if ($("#active_filter_" + field_name).length > 0) {
                    var filter_block = $("#active_filter_" + field_name);
                    $(filter_block).children("span").html(filter_label);
                } else {
                    $("#active_filters").append(
                        $('' +
                            '<div class="clr tag dInlineBlock formField md whtbg mr-5 mb-15" id="active_filter_' + field_name + '">' +
                                '<h5 class="basic-font mediumFont">' + label + '</h5>' +
                                '<span>' + filter_label + '</span>' +
                                '<img class="icon" src="assets/images/svg/cross-icon.svg" alt="" onclick="resetFilterFieldNew(\'' + field_name + '\')">' +
                            '</div>'
                        )
                    );
                }
                setQueryStringParameter(field_name, field.val());
            } else {
                resetFilterFieldNew(field_name);
            }                
        });
        dt.draw();
    }

    function resetFilterFieldNew(field_name)
    {
        var filter_block = $("#active_filter_" + field_name);
        if (filter_block.length > 0)
        {
            filter_block.remove();
            var filter_field = $("#filter_form input[name='" + field_name + "'],#filter_form select[name='" + field_name + "']");
            if (filter_field.attr("type") != "radio")
            {
                filter_field.val("");
                if (field_name.indexOf("_name") !== false)
                {
                    var filter_id_fields = $("#filter_form input[name='" + field_name.replace("_name","_id") + "'],#filter_form select[name='" + field_name.replace("_name","_id") + "']");
                    if (filter_id_fields.length > 0)
                    {
                        filter_id_fields.val("");
                    }
                }
            } else {
                filter_field.prop('checked', false);
            }
            setQueryStringParameter(field_name, "");
            dt.draw();
        }
        
    }

    function addRightSidePanelListeners()
    {
        $('.rightSidePanelCloser:not(.listens)').each(function(){
            $(this).addClass('listens');
            this.addEventListener("click", function()
            {
                $('.rightPanelHolder').removeClass('open');
                $('.rightSidePanel').removeClass('open');
                $('html').removeClass('panelOpen');
            }, false);
        });
        
        $('.rightSidePanelCloser2:not(.listens)').each(function(){
            $(this).addClass('listens');
            this.addEventListener("click", function()
            {
                window.chat_app.showChat = false;
                $('.rightPanelHolder2').removeClass('open');
                $('.rightSidePanel2').removeClass('open');
                $('html').removeClass('panelOpen');
            }, false);
        });
        
        $('.rightSidePanelUserCloser:not(.listens)').each(function(){
            $(this).addClass('listens');
            this.addEventListener("click", function()
            {
                $('.rightPanelHolderUser').removeClass('open');
                $('.rightSidePanelUser').removeClass('open');
            }, false);
        });


        $('.rightSidePanelOpener2:not(.listens)').each(function(){
            $(this).addClass('listens');
            
            this.addEventListener("click", function()
            {
                $('.rightPanelHolder2').addClass('open');
                $('.rightSidePanel2').addClass('open');
                $('html').addClass('panelOpen');
                console.log($(this).data('requestId'));
                window.chat_app.objectType = "request";
                window.chat_app.objectId = $(this).data('requestId');
                window.chat_app.parentObjectType = "";
                window.chat_app.parentObjectId = 0;
                window.chat_app.currentUserId = <%=user_id%>;
                window.chat_app.showChat = true;
                console.log("panel2 opened");
            });
        });
        
        $('.rightSidePanelOpener:not(.listens)').each(function(){
//            console.log("attaching to ", this);
//            console.log("rightSidePanelOpener attached to", this);
            $(this).addClass('listens');
            
            this.addEventListener("click", function()
            {
                $("#rightPanelContent").html("");

                var panel_title = "";
                var ajax = {};
                ajax.url = "";
                ajax.type = 'GET';
                ajax.dataType = "html";
//                console.log("rightSidePanelOpener fired click ", event);

                var clicked_node = $(event.target).closest(".rightSidePanelOpener");
                var target = clicked_node.data('target');
                var request_id = clicked_node.data('requestId');
//                    console.log(clicked_node, target);

                switch (target) 
                {
                    case "chat":
                        ajax.url = "chat.html";
                        ajax.data = {request_id: request_id};
                        panel_title = "Collaboration by Request";
                        break;
                    case "service-catalog":
                        ajax.url = "service_catalog.jsp";
                        var service_catalog_id = clicked_node.data('catalogId');
                        // todo urlSearchParameters for pass_parameters
                        ajax.data = {id: service_catalog_id, embedded: true};
                        panel_title = "Create a New Request";
                        break;
                    case "item":
                        ajax.url = "service_catalog_item.jsp";
                        var service_catalog_item_id = clicked_node.data('itemId');
                        // todo urlSearchParameters for pass_parameters
                        ajax.data = {id: service_catalog_item_id, embedded: true};
                        panel_title = "Create a New Request";
                        break;
                    case "user":
                        ajax.url = "admin_users_add.jsp";
                        panel_title = "Add New User";
                        break;
                    case "request":
                        if (request_id)
                        {
                            ajax.url = "request.jsp";
                            ajax.data = {request_id: request_id};
                            panel_title = "Edit Request";
                        } else {
                            ajax.url = "home_service_catalog.jsp?embedded=true";
                            panel_title = "Create a New Request";
                        }
                        break;
                    default:
                        console.log("Error! Unknown target for rightPanel");
                        break;
                }
                
                if (ajax.url === "") return false;

                ajax.error = function(jqXHR, textStatus)
                {
                    switch (jqXHR.status) {
                        case 401:
                            window.location.assign("<%=props.get("login_url").toString()%>");
                            break;
                        case 403:
                            alert("No permission!");
                            break;
                        default:
                            alert("Error processing request!");
                    }
                };

                ajax.success = function (data) 
                {                    
                    $('.rightPanelHolder').addClass('open');
                    $('.rightSidePanel').addClass('open');
                    $('html').addClass('panelOpen');

                    $("#rightPanelTitle").html(panel_title);
                    
                    $('.rightPanelHolder').removeClass("service-catalog-page");
                    $("#rightPanelContent").removeClass("catalog-items-listing");
//                    console.log(target);
                    switch (target) 
                    {
                        case "request":
                            $('.rightPanelHolder').addClass("service-catalog-page");
                            break;
                        case "service-catalog":
                            $('.rightPanelHolder').addClass("service-catalog-page");
                            $("#rightPanelContent").addClass("catalog-items-listing");
                            break;
                    }
                    
                    $("#rightPanelContent").html(data);
                    addRightSidePanelListeners();
                    attachFieldListeners($("#rightPanelContent"));        
                    

                    
                }
                
                $.ajax(ajax);
            }, false);
        });

        $('.rightSidePanelUserOpener:not(.listens)').each(function(){
//            console.log("attaching to ", this);
//            console.log("rightSidePanelOpener attached to", this);
            $(this).addClass('listens');
            
            this.addEventListener("click", function()
            {
                $(".rightPanelHolderUser").find(".panelContent").html("");

                var panel_title = "";
                var ajax = {};
                ajax.url = "";
                ajax.type = 'GET';
                ajax.dataType = "html";
//                console.log("rightSidePanelOpener fired click ", event);

                ajax.url = "admin_users_add.jsp";
                panel_title = "Add New User";
                
                ajax.error = function(jqXHR, textStatus)
                {
                    switch (jqXHR.status) {
                        case 401:
                            window.location.assign("<%=props.get("login_url").toString()%>");
                            break;
                        case 403:
                            alert("No permission!");
                            break;
                        default:
                            alert("Error processing request!");
                    }
                };

                ajax.success = function (data) 
                {                    
                   
                    $('.rightPanelHolderUser').addClass('open');
                    $('.rightSidePanelUser').addClass('open');

                    $(".rightPanelHolderUser").find(".panelTitle").html(panel_title);
                    $(".rightPanelHolderUser").find(".panelContent").html(data);
                    
//                    addRightSidePanelListeners();
//                    attachFieldListeners($(".rightPanelHolderUser"));        
                    
                }
                
                $.ajax(ajax);
            }, false);
        });

    }
        
    window.addEventListener("load", (event) => 
    {
        $('.rightSidePanelCloser').on('click', function(){
            $('.rightPanelHolder').removeClass('open');
            $('.rightSidePanel').removeClass('open');
            $('html').removeClass('panelOpen');
            sla_id = "";
            sla_type = "";
        });

        // $('.dropdown-menu div#filter_categories li').on('click', function (e) 
        // { // prevent dropdown close on autocomplete
        //     $(".filter-category-block").addClass("d-none");
        //     var selected_category_name = $(this).children("a").html();
        //     var selected_category_id = $(this).children("a").data('category');
        //     $("#filters_dropdown_toggle").html('<span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>' + selected_category_name);
        //     var category_block = $("#"+selected_category_id);
        //     if (category_block)
        //     {
        //         category_block.removeClass("d-none");
        //         $("#filter_categories").addClass("d-none");
        //         $("#filters_dropdown_menu").css("width", "500px");
        //         $("#filter_actions").removeClass("d-none");
        //     }
           
        //     // if (e.clickEvent && e.clickEvent.target && (e.clickEvent.target.className == "ui-menu-item-wrapper" || e.clickEvent.target.className == "icon")) {
        //     //     e.preventDefault();
        //     // }
        // });

        $('#filters_dropdown').on('hide.bs.dropdown', function (e) 
        { // prevent dropdown close on autocomplete
            console.log("dropdown closed ", e);
            //            console.log(e.clickEvent.target.className);
            if (e.clickEvent && e.clickEvent.target && (e.clickEvent.target.className == "ui-menu-item-wrapper" || e.clickEvent.target.className == "icon" || e.clickEvent.target.className.indexOf("applyBtn") !== -1)) {
                e.preventDefault();
            }
        });

        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
        
        dt = $('#dataTable').DataTable( {
            "processing": true,
            "serverSide": true,
            "ordering": true,
            'serverMethod': 'post',
            'searching': false,
            autoWidth: false,
            "lengthChange": false,
            dom: "<'row'<'col-sm-3'i><'col-sm-3'f><'col-sm-6'p>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            "ajax": {
                "url": "ajax_lookup_requests",
                'data': function(data){
                    // Read values
                    $("#filter_form input,#filter_form select").each(function() {
                        if ($(this).attr("name") && (($(this).attr("type") == "radio" && $(this).is(':checked')) || ($(this).attr("type") != "radio" && this.value && this.value != "" && this.value.toLowerCase() != "any" && this.value.toLowerCase() != "all" && this.value != 0)))
                        {
                            data[$(this).attr('name')] = this.value;
                        }
                    });

                }
            },
            "pageLength": 50,
            "columnDefs": [
                { targets: 0, "width": "1em", defaultContent: '<span class="expand-td"><img src="assets/images/svg/collapse-minus-icon.svg" alt=""></span>', orderable: false },
                { targets: 1, "width": "5em", "data": "id", name: "id" },
                { targets: 2, "width": "5em", "data": null, name: "priority", 
                    render: function ( data, type, row ) 
                    {
                        var indicator_class = "";
                        switch (row.priority) {
                            case "Low":
                                indicator_class = "low";
                                break;
                            case "Medium":
                                indicator_class = "medium";
                                break;
                            case "High":
                                indicator_class = "high";
                                break;
                            case "Critical":
                                indicator_class = "critical";
                                break;
                            default:
                                indicator_class = "unknown";
                                break;
                        }
                        return '<span class="indicator ' + indicator_class + ' "></span> ' + (row.priority ? row.priority : 'Unknown');
                    }
                },
                { targets: 3, "width": "5em", "data": null, name: "state",
                    render: function ( data, type, row ) 
                    {
                        return '<span class="btn sm state-' + row.state.toLowerCase() + ' customBtn">' + row.state + '</span>';
                    }
                },
                { targets: 4, "width": "10em", "data": "request_date", name: "request_date", render: $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss','MM/DD/YYYY hh:mm A') },
                { targets: 5, "data": null, orderable: false,
                    render: function ( data, type, row ) 
                    {
                        return row.description.split(' -- ')[0];
                    }
                },
                { targets: 6, "width": "10em", "data": null, name: "requested_for_id", 
                    render: function ( data, type, row ) 
                    {
                        if (row.requested_for_id)
                        {
                            if (row.requested_for_avatar != "") {
                                return '<span class="user-img dInlineBlock mr-5 user-avatar"><img src="get_avatar?uuid=' + row.requested_for_avatar + '" alt=""></span> ' + row.requested_for_first + ' ' + row.requested_for_last;                        
                            } else {
                                return row.requested_for_first + ' ' + row.requested_for_last;
                            }
                        } else {
                            return '';
                        }
                    }
                },
                { targets: 7, "width": "10em", "data": null, name: "assigned_to_id", 
                    render: function ( data, type, row ) 
                    {
                        if (row.assigned_to_id > 0)
                        {
                            if (row.assigned_to_avatar != "") {
                                return '<span class="user-img dInlineBlock mr-5 user-avatar"><img src="get_avatar?uuid=' + row.assigned_to_avatar + '" alt=""></span> ' + row.assigned_to_first + ' ' + row.assigned_to_last;                        
                            } else {
                                return row.assigned_to_first + ' ' + row.assigned_to_last;
                            }
                        } else {
                            return "";
                        }
                    }
                },
//                { targets: 9, 
//                    render: function (data, type, full, meta) 
//                    {
//                        return '' +
//                            '<a href="javascript:void(0)" onclick="expandRow(' + full.id + ');" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>' +
//                            '<a href="javascript:void(0)" onclick="showDeleteModal(' + full.id + ')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>';
//                    }
//                }
            ],
            "order": [],
            createdRow: function( row, data, dataIndex ) {
                $( row )
                    .attr('data-id', data.id)
//                    // .addClass('tableSortHolder');
            },
            "stripeClasses":[]
        } );


        var detailRows = [];
        
        $('#dataTable tbody').on( 'click', 'tr span.expand-td', function () {
            var tr = $(this).closest('tr');
//            console.log(tr);
            var row = dt.row( tr );
            var row_id = tr.data('id');
            var idx = $.inArray( row_id, detailRows );

            if ( row.child.isShown() ) {
                row.child.hide();

                // Remove from the 'open' array
                detailRows.splice( idx, 1 );
            }
            else {
                if(row.child() && row.child().length)
                {
                    row.child.show();
                } else {
                    row.child( 
                        $(
                            '<tr class="expanded" id="request_details_row_' + row_id + '">' +
                                '<td colspan="9" class="first-row">' +
                                    '<div class="content-holder p-15 mt-10 mb-10"></div>' +
                                '</td>' +
                            '</tr>'
                        )                    
                    ).show();
                    editRequest(row_id);
                }
                // Add to the 'open' array
                if ( idx === -1 ) {
                    detailRows.push( row_id );
                }
            }
        } );

        apply_filters();
        
        dt.on( 'draw', function () {
            $.each( detailRows, function ( i, id ) {
                $('#'+id+' span.expand-td').trigger( 'click' );
            } );
            $(dt.table().node()).removeClass("d-none");
        } );
//        dt.on("preXhr.dt", function (e, settings, data) {
//            $("#dataTable").addClass("d-none");
//        });
        addRightSidePanelListeners();   
        attachFieldListeners($("#filter_form"));

        <% if (action.equals("new"))
        {
        %>    
            document.getElementById("new_request_button").dispatchEvent(new Event('click'));
        <%
        }
        %>

    });

    function expandRow(request_id) {
        console.log(request_id);
        $('tr[data-id="' + request_id + '"] span.expand-td').click();
    }
    function editRequest(id)
    {
        
        var content_holder = $('tr[id="request_details_row_' + id+'"] div.content-holder');
        console.log(id, content_holder);
        if (content_holder.html() == "") 
        {

            var ajax = {};
            ajax.type = 'GET';
            ajax.dataType = "html";

            ajax.data = {id: id};
            ajax.url = "request.jsp";

            ajax.error = function(jqXHR, textStatus)
            {
                switch (jqXHR.status) {
                    case 401:
                        window.location.assign("<%=props.get("login_url").toString()%>");
                        break;
                    case 403:
                        alert("No permission!");
                        break;
                    default:
                        alert("Error processing request!");
                }
            };

            ajax.success = function (data) 
            {
                content_holder.html(data);
                attachFieldListeners(content_holder);

            };

            $.ajax(ajax);        
        }
        
    }
    
    function saveRequest()
    {
        var form = $(event.target.closest("form"));
        if (form != undefined)
        {
            form.append(
                $('' +
                    '<div class="overlay">' +
                        '<div class="loader"></div>' +
                    '</div>'
                )
            );
            var data = form.serialize();
            $.ajax({
                url: "request_update",
                type: "POST",
                data: data,
                success: function(){
                    form.find(".overlay").remove();
                    var tr = form.closest("tr");
                    dt.draw();
                    $('body').animate({scrollTop: tr.offset().top - 50}, 'fast');
                },
                error: function(jqXHR, textStatus)
                {
                    switch (jqXHR.status) {
                        case 401:
                            window.location.assign("<%=props.get("login_url").toString()%>");
                            break;
                        case 403:
                            alert("No permission!");
                            break;
                        default:
                            alert("Error processing request! " + textStatus);
                    }
                    form.find(".overlay").remove();
                }
            });
        } else {
            alert("Saving form error");
        }
    }
    
    function saveUser()
    {
        var form = $(event.target).closest("form").get(0);
        $.ajax({
            url: form.action,
            type: "POST",
            data: new FormData(form),
            processData: false,
            contentType: false,
            success: function(resText){
                var res = jQuery.parseJSON( resText );
                console.log(res);
                if (res.error == "") {
//                        $("input[name='requested_for_name']").autocomplete('search', res.user_name).data("ui-autocomplete").menu.element.children().first().click();;
                    $("input[name='requested_for_name']").val(res.user_name);
                    $("input[name='requested_for_id']").val(res.user_id);
                    $(".rightSidePanelUserCloser").click();
                } else {
                    alert(res.error);
                }
//                window.location.assign("admin_users.jsp");
            },
            error: function(jqXHR, textStatus)
            {
                switch (jqXHR.status) {
                    case 401:
                        window.location.assign("<%=props.get("login_url").toString()%>");
                        break;
                    case 403:
                        alert("No permission!");
                        break;
                    default:
                        alert("Error processing request! " + textStatus);
                }
            }
        });
    }

    function attachFieldListeners(container)
    {
        tinymce.remove('textarea');
        tinymce.init({
            selector: '.ckeditor',
            ui_container: '.content-holder',
            setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                });
            },
            height: 250,
            plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });
        container.find("button.save-button").on('click', function() {
            saveRequest();
        });

        //requested_for
        container.find('input[name="requested_for_username"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="requested_for_username"]').val(ui.item.label); // display the selected text
                container.find('input[name="requested_for_id"]').val(ui.item.value); // save selected id to input
                container.find('input[name="location"]').val(ui.item.location); // save selected id to input
                container.find('input[name="department"]').val(ui.item.department); // save selected id to input
                container.find('input[name="site"]').val(ui.item.site); // save selected id to input
                container.find('input[name="company"]').val(ui.item.company); // save selected id to input
                //if vip = true then check the vip checkbox
                var vip = ui.item.vip;
                if(vip.toUpperCase() === "TRUE")
                {
                    container.find('input[name="vip"]').prop('checked', true);
                }
                else
                {
                    container.find('input[name="vip"]').prop('checked', false);
                }
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });

    //requested_for_group_name
    container.find( 'input[name="requester_group_name"]' ).autocomplete(
    {
        source: function( request, response ) 
        {
            // Fetch data
            $.ajax({
                url: "ajax_lookup_group",
                type: 'post',
                dataType: "json",
                minLength: 2,
                data: 
                {
                    search: request.term
                },
                success: function( data ) 
                {
                    response( data );
                }
            });
        },
        select: function (event, ui) 
        {
            // Set selection
            container.find('input[name="requester_group_name"]').val(ui.item.label); // display the selected text
            container.find('input[name="requested_for_group_id"]').val(ui.item.value); // save selected id to input
            //alert("setting is=" + ui.item.value);
            return false;
        }
    }); 

    //create_by
    container.find('input[name="create_by_username"]').autocomplete(
    {
        source: function( request, response ) 
        {
            // Fetch data
            $.ajax({
                url: "ajax_lookup_user",
                type: 'post',
                dataType: "json",
                minLength: 2,
                data: 
                {
                    search: request.term
                },
                success: function( data ) 
                {
                    response( data );
                }
            });
        },
        select: function (event, ui) 
        {
            // Set selection
            container.find('input[name="create_by_username"]').val(ui.item.label); // display the selected text
            container.find('input[name="create_by_id"]').val(ui.item.value); // save selected id to input
            //alert("setting is=" + ui.item.value);
            return false;
        }
    });

//knowledge_search_terms
     container.find('input[name="knowledge_search_terms"]').autocomplete({

         source: function( request, response ) 
         {
             // Fetch data
             $.ajax({
                 url: "ajax_lookup_request_knowledge_article_text",
                 type: 'post',
                 dataType: "json",
                 minLength: 2,
                 data: 
                 {
                     search: request.term
                 },
                 success: function( data ) 
                 {
                     response( data );
                 }
             });
         },
         select: function( event, ui ) 
         { 
             container.find('input[name="knowledge_search_terms"]').val(ui.item.article_description);
             //window.location.href = ui.item.value;
             window.open("kb_view.jsp?id=" + ui.item.value, "Knowledge Base Article", "width=600,height=600,top=400,left=400");
             return false;
         }
     });

        //location
        container.find('input[name="location"]' ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user_location",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="location"]').val(ui.item.label); // display the selected text
                //alert("setting is=" + ui.item.value);
                return false;
            }
        }); 
        //department
        container.find('input[name="department"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user_department",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="department"]').val(ui.item.label); // display the selected text
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });
        //site
        container.find('input[name="site"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user_site",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="site"]').val(ui.item.label); // display the selected text
                //alert("setting is=" + ui.item.value);
                return false;
            }
        }); 
        //company
        container.find('input[name="company"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user_company",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="company"]').val(ui.item.label); // display the selected text
                //alert("setting is=" + ui.item.value);
                return false;
            }
        }); 
        //assigned_group
        container.find('input[name="assigned_group_name"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_group",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="assigned_to_name"]').val(""); // clear field
                container.find('input[name="assigned_to_id"]').val(""); // clear field
                container.find('input[name="assigned_group_name"]').val(ui.item.label); // set name
                container.find('input[name="assigned_group_id"]').val(ui.item.value); // set id
                return false;
            }
        });
        //assigned_to_name
        container.find('input[name="assigned_to_name"]').autocomplete(
        {
            source: function( request, response ) 
            {
                var var_group_id = container.find('input[name="assigned_group_id"]').val();
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_group_user",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term,
                        group_id: var_group_id                            
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="assigned_to_name"]').val(ui.item.label); // display the selected text
                container.find('input[name="assigned_to_id"]').val(ui.item.value); // save selected id to input
                //alert("assigned to setting is=" + ui.item.value);
                return false;
            }
        });

        container.find('input[name="request_date"]').daterangepicker(
        {
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
                "format": "HH:mm MM/DD/YYYY",

                "applyLabel": "Apply",
                "cancelLabel": "Cancel",

                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            }
        });

        container.find('input[name="pending_date"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": 
            {
                "format": "HH:mm MM/DD/YYYY",

                "applyLabel": "Apply",
                "cancelLabel": "Cancel",

                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            }
        });

        container.find('input[name="pending_date"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="pending_date"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        container.find('input[name="closed_date"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": 
            {
                "format": "HH:mm MM/DD/YYYY",

                "applyLabel": "Apply",
                "cancelLabel": "Cancel",

                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            }
        });
        container.find('input[name="closed_date"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="closed_date"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });


        container.find('input[name="request_date_start"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="request_date_start"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="request_date_start"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="create_date_start"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="create_date_start"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="create_date_start"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="pending_date_start"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="pending_date_start"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="pending_date_start"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="state_date_start"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="state_date_start"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="state_date_start"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="closed_date_start"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="closed_date_start"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="closed_date_start"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="request_date_stop"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="request_date_stop"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="request_date_stop"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="create_date_stop"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="create_date_stop"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="create_date_stop"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="pending_date_stop"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="pending_date_stop"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="pending_date_stop"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="state_date_stop"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="state_date_stop"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="state_date_stop"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="closed_date_stop"]').daterangepicker({
           "autoUpdateInput": false,
           "singleDatePicker": true,
           "showDropdowns": true,
           "timePicker": true,
           "timePicker24Hour": true,
           "locale": {
           "format": "HH:mm MM/DD/YYYY",
           "applyLabel": "Apply",
           "cancelLabel": "Cancel",
           "weekLabel": "W",
           "daysOfWeek": [
               "Su",
               "Mo",
               "Tu",
               "We",
               "Th",
               "Fr",
               "Sa"
           ],
           "monthNames": [
               "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December"
           ],
           "firstDay": 1
            }
        });
        container.find('input[name="closed_date_stop"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="closed_date_stop"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="requester_username"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user_last_first",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {

                // Set selection
                container.find('input[name="requester_username"]').val(ui.item.label); // set name
                container.find('input[name="requested_for_id"]').val(ui.item.value); // set id
                return false;
            }
        });

        container.find('input[name="create_by_name"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user_last_first",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="create_by_name"]').val(ui.item.label); // set name
                container.find('input[name="create_by_id"]').val(ui.item.value); // set id
                return false;
            }
        });

        container.find('input[name="priority"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_priority",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="priority"]').val(ui.item.label); // display the selected text
                return false;
            }
        });

        container.find('input[name="urgency"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_urgency",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="urgency"]').val(ui.item.label); // display the selected text
                return false;
            }
        });

        container.find('input[name="impact"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_impact",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="impact"]').val(ui.item.label); // display the selected text
                return false;
            }
        });

        container.find('input[name="category"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_category_search",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="category"]').val(ui.item.label); // display the selected text
                return false;
            }
        });

        container.find('input[name="subcategory"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_subcategory_search",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="subcategory"]').val(ui.item.label); // display the selected text
                return false;
            }
        });

    }    

    function update_subcategory()
    {
        var var_cat_id = document.getElementById("category_id").value;
        var url = "ajax_lookup_subcategory?category_id=" + var_cat_id;
        var select = document.getElementById('subcategory');
        document.getElementById('subcategory').options.length = 0;
        $.get('ajax_lookup_subcategory?category_id=' + var_cat_id, function(data) 
        {
            for(var i = 0; i < data.length; i++) 
            {
                var obj = data[i];
                var name = obj.name;  
                var opt = document.createElement('option');
                    opt.value = name;
                    opt.innerHTML = name;
                    select.appendChild(opt);
            }            
        });
    }

</script>
    
<script>
function clear_requested_for() 
{ 
    var form = $(event.target.closest("form"));
    if (form.length > 0)
    {
        form.find("#requested_for_id").val("");
        form.find("#requested_for_name").val("");
    }
}
function clear_assigned_to() 
{ 
    document.getElementById("assigned_to_id").value = "";
    document.getElementById("assigned_to_name").value = "";
}
function clear_create_by() 
{ 
    document.getElementById("create_by_id").value = "";
    document.getElementById("create_by_name").value = "";
}
function clear_requested_for_group() 
{ 
    document.getElementById("requested_for_group_id").value = "";
    document.getElementById("requested_for_group_name").value = "";
}
function clear_assigned_group() 
{ 
    document.getElementById("assigned_group_id").value = "";
    document.getElementById("assigned_group_name").value = "";
}
</script>
<script>
    function hide_show_div() 
    {
        var x = document.getElementById("selectors");
        if (x.style.display === "none") 
        {
            x.style.display = "block";
        } 
        else 
        {
            x.style.display = "none";
        }
    }        

</script>
<!-- END PAGE LEVEL JS-->

</body>
</html>
<%     
        }//end if not permission
    }//end if not logged in
%>
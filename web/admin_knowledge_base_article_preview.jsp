<%-- 
    Document   : admin_knowledge_base_article_add.jsp
    Created on : Oct 4 2020, 6:13:59 PM
    Author     : rcampbell
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%> 
<%@page import="java.util.GregorianCalendar"%>
<%@page import="org.apache.commons.text.StringEscapeUtils"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        boolean page_authorized = support.role.authorized(session, "incident","update");         
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            
            boolean article_approver = false;
        
            String ADMINISTRATION = session.getAttribute("administration").toString();
            String MANAGER = session.getAttribute("manager").toString();
            if(ADMINISTRATION.equalsIgnoreCase("true") || MANAGER.equalsIgnoreCase("true"))
            {
                article_approver = true;
            }  
            
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / MM/dd/yyyy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
            java.util.Date now = new java.util.Date();
            String created_date = date_time_picker_format.format(now);
            
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String id = request.getParameter("id");
                String article_info[] = db.get_kb.article_info_for_id(con, id);
    %>
<%=article_info[5]%>
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_knowledge_base_add.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_knowledge_base_add.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
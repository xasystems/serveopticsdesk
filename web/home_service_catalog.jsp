<%@page import="org.apache.commons.lang3.math.NumberUtils"%>
<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : home_service_catalog
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="org.apache.commons.lang3.BooleanUtils" %>
<%@ page import="java.text.*" %>
<%@ page import="java.sql.*" %>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        if (!session.getAttribute("self_service").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("true")) 
        {
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            session.setAttribute("home_page", "home_service_catalog.jsp");
            GregorianCalendar first_of_month = new GregorianCalendar();
            first_of_month.set(Calendar.DAY_OF_MONTH, 1);
            first_of_month.set(Calendar.HOUR_OF_DAY, 0);
            first_of_month.set(Calendar.MINUTE, 0);
            first_of_month.set(Calendar.SECOND, 0);     
            GregorianCalendar now = new GregorianCalendar();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap sys_props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session); 
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            String message_id = request.getParameter("message_id");
            String request_user_id = request.getParameter("user_id");
            String pass_parameters = "";
            Boolean embedded = request.getParameter("embedded") != null && BooleanUtils.toBoolean(request.getParameter("embedded"));
            if(message_id != null)
            {
                pass_parameters = "&message_id=" + message_id;
            }
            if(request_user_id != null)
            {
                pass_parameters = pass_parameters + "&user_id=" + request_user_id;
            }
            ArrayList <String[]> catalogs = db.get_service_catalog.service_catalog_by_state(con, "Published");
            String catalog[];

    %>
    <% if (!embedded) {%>
        <jsp:include page='header.jsp'>
            <jsp:param name="page_type" value=""/>
        </jsp:include>	
        <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">

        <jsp:include page='menu_service_desk.jsp'>
            <jsp:param name="active_menu" value=""/>
        </jsp:include>	
    
    <% } %>

                <h4 id="catalog_title" class="title xxl-font boldFont mb-15 pt-5">
                    <span class="txt">Categories</span>
                </h4>
                <div class="mb-30">
                    <div class="clr searchFieldHolder dInline formField w-100">
                        <span class="icon"><img src="assets/images/svg/searchIcon.svg" alt=""></span>
                        <input class="autocomplete" id="catalog_search" type="text" dir="ltr" placeholder="Find Anything">
                    </div>
                    <small id="catalog_showing" class="pt-15"><em>Showing <%=catalogs.size()%> Items only.</em></small>
                </div>

                <div id="catalog_content" class="row">

                <%
                for(int a = 0; a < catalogs.size();a++)
                {
                    catalog = catalogs.get(a);
                %>
                    <div class="col-md-6 col-lg-6">
                        <!--<a id="<%=catalog[0]%>" class="basic-color" href="service_catalog.jsp?id=<%=catalog[0]%>" <%=(embedded ? "onclick=\"event.preventDefault();openCatalog(" + catalog[0] + ")\"": "")%>>-->
                        <a id="<%=catalog[0]%>" class="basic-color rightSidePanelOpener" data-target="service-catalog" data-catalog-id="<%=catalog[0]%>" href="service_catalog.jsp?id=<%=catalog[0]%>" <%=(embedded ? "onclick=\"event.preventDefault();\"": "")%>>
                            <div class="category clr">
                                <% if (catalog[7] != null && !catalog[7].equals("null")) { %>
                                    <div class="icon placeholder">
                                        <img src="get_attachment?uuid=<%=catalog[7]%>&name=image" alt="">
                                    </div>
                                <% } else { %>
                                    <div class="icon placeholder la <%=catalog[5]%> font-large-4">
                                    </div>
                                <% } %>
                                <div class="txt catalog-category">
                                    <h5 class="mediumFont large-font"><%=catalog[1]%></h5>
                                    <p class="mb-0"><%=catalog[2]%></p>
                                </div>
                            </div>
                        </a>
                    </div>
                <%
                }
                %>
                </div>
    <% if (!embedded) {%>
                <!-- end content here-->
        <jsp:include page='footer.jsp'>
            <jsp:param name="parameter" value=""/>
        </jsp:include>	
        <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>

        <!-- BEGIN PAGE LEVEL JS-->
        <script>
        window.addEventListener("load", (event) => 
        {
            $('#catalog_search').each(function() 
            {
                var $el = $(this);
                var field = $el.prop('name');

                $el.autocomplete({
                    source: function( request, response ) 
                    {
                        $.ajax({
                            url: "ajax_lookup_service_catalog",
                            type: 'post',
                            dataType: "json",
                            data: 
                            {
                                search: request.term,
                                full: 1
                            },
                            success: function( data ) 
                            {
                                response( data );
                            }
                        });
                    },
                    minLength: 2,
                    select: function( event, ui ) 
                    {
                        window.location.href = "service_catalog.jsp?id=" + ui.item.value
                        return false;
                    }
                });
            });
        });

        </script>
        <!-- END PAGE LEVEL JS-->
    <% } %>
    </body>
</html>
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>
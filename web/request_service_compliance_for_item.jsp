<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : request_service_compliance
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="javax.json.Json"%>
<%@ page import="javax.json.JsonArray"%>
<%@ page import="javax.json.JsonObject"%>
<%@ page import="javax.json.JsonReader"%>
<%@ page import="javax.json.JsonStructure"%>
<%@ page import="java.io.StringReader"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        if (!session.getAttribute("request").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            String home_page = session.getAttribute("home_page").toString();
            //if you have the permission to read/modify or Admin then you can see this
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020  
            //SimpleDateFormat filter_format2 = new SimpleDateFormat("yyyy"); //01/12/2019 12:00 AM
            java.util.Date filter_start = new java.util.Date();
            java.util.Date filter_end = new java.util.Date();
            java.util.Date previous_start_date = new java.util.Date();
            java.util.Date previous_end_date = new java.util.Date();
            String start = "";
            String end = "";
            String date_range = "";
            String referer = "";
            String catalog_item_id = request.getParameter("catalog_item_id");
            String catalog_item_info[] = db.get_service_catalog.service_catalog_items_by_id(con, catalog_item_id);
            try 
            {
                date_range = request.getParameter("date_range");
                if (date_range.equalsIgnoreCase("null") || date_range == null) 
                {
                    date_range = support.filter_dates.past_30_days();
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                date_range = support.filter_dates.past_30_days();
            }
            try 
            {
                referer = request.getParameter("referer");
                if (referer.equalsIgnoreCase("null") || referer == null) 
                {
                    referer = "request_home_support.jsp?date_range=" + date_range;
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                referer = "request_home_support.jsp?date_range=" + date_range;
            }
            try 
            {
                //split the start and end date  //01/12/2019 12:00 AM - 01/12/2019 11:59 PM
                String temp[] = date_range.split("-");
                filter_start = filter_format.parse(temp[0]);
                filter_end = filter_format.parse(temp[1]);                
                start = timestamp.format(filter_start);
                end = timestamp.format(filter_end);

                //get pervious date frame                
                previous_start_date.setTime(filter_start.getTime());
                previous_end_date.setTime(filter_end.getTime());                
                long diff = filter_end.getTime() - filter_start.getTime();
                previous_start_date.setTime(filter_start.getTime() - diff);
                previous_end_date.setTime(filter_end.getTime() - diff);
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                System.out.println("request_service_compliance.jsp exception=" + e);
            }
            double sla_score[] = support.sla_request_calc.for_service_catalog_item_id(con, "4", filter_start, filter_end);
            //state=closed_complete
            System.out.println("Good 0=" + sla_score[0] + " Bad 1=" + sla_score[1] + " Score 2=" + sla_score[2]);
            
            
            double drilldown_sla_score[][] = support.sla_request_calc.for_service_catalog_item_id_by_priority(con, catalog_item_id, filter_start, filter_end);
            System.out.println("CRITICAL Good 0=" + drilldown_sla_score[0][0] + " Bad 1=" + drilldown_sla_score[0][1] + " Score 2=" + drilldown_sla_score[0][2]);
            System.out.println("HIGH Good 0=" + drilldown_sla_score[1][0] + " Bad 1=" + drilldown_sla_score[1][1] + " Score 2=" + drilldown_sla_score[1][2]);
            System.out.println("MEDIUM Good 0=" + drilldown_sla_score[2][0] + " Bad 1=" + drilldown_sla_score[2][1] + " Score 2=" + drilldown_sla_score[2][2]);
            System.out.println("LOW Good 0=" + drilldown_sla_score[3][0] + " Bad 1=" + drilldown_sla_score[3][1] + " Score 2=" + drilldown_sla_score[3][2]);
            String data = "{ y: " + drilldown_sla_score[0][2] + ", color: '#8bc34a', catalog_item_id: '1', priority: 'Critical'}," +    
                          "{ y: " + drilldown_sla_score[1][2] + ", color: '#8bc34a', catalog_item_id: '2', priority: 'High'}," +
                          "{ y: " + drilldown_sla_score[2][2] + ", color: '#8bc34a', catalog_item_id: '3', priority: 'Medium'}," +
                          "{ y: " + drilldown_sla_score[3][2] + ", color: '#8bc34a', catalog_item_id: '1', priority: 'Low'}";
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	
    <input type="hidden" name="start" id="start" value="<%=start%>"/>
    <input type="hidden" name="end" id="end" value="<%=end%>"/>
    <!-- BEGIN VENDOR CSS-->
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/app.css">-->
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">-->
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    
    <script src="assets/js/highcharts/highcharts.js"></script>
    <script src="assets/js/highcharts/data.js"></script>
    <script src="assets/js/highcharts/drilldown.js"></script>
    <script src="assets/js/highcharts/exporting.js"></script>
    
    
    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	

    <div class="clr whtbg p-10 mb-15 position-relative">
        <div class="float-right options">
            <ul>
                <li id="filters_dropdown" class="dInlineBlock dropdown ">
                    <div class="dInlineBlock clr menuHolder">
                        <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                        </a>
                        <div class="dropdown-menu dropdown-menu-right md p-10" x-placement="bottom-end" style="position: absolute; transform: translate3d(100px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                            <form id="filter_form">
                                <ul>
                                    <li class="mb-10 clr">
                                        <label class="field-label full-width">
                                            Date Range
                                        </label>
                                        <div class="formField md  mr-0 full-width">
                                            <input class="no-border full-width datetime" type="text" name="date_range" id="date_range" value="<%=date_range%>">
                                        </div>
                                    </li>
                                </ul>
                                <div class="text-center pt-5 pb-5">
                                    <button type="button" class="btn md2 btn-primary-new customBtn mb-5" onclick="reload_page()">Apply Filters</button>
                                    <div class="clr"></div>
                                    <a href="#" class="">Reset Filters</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <h1 class="large-font boldFont">Request SLAs for Service Catalog Item <em><%=catalog_item_info[2]%></em></h1>
    </div>
    <div id="active_filters">
    </div>
    
            <div class="row mb-30">
                <div class="col-12">
                    <div class="card">
                        <h4 class="card-title p-3">
                            SLA by Priority
                        </h4>
                        <div class="card-content collapse show">
                            <div class="card-body pt-0">
                                <div id="sla_chart"></div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <div class="sort-tbl-holder">
            <table id="data_table_email" class="table table-striped custom-sort-table zero-configuration" data-language='{"zeroRecords":"No Requests for this Item/Date Range"}' cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="width:8em">ID</th>
                        <th>Description</th>
                        <th style="width:8em">Priority</th>
                        <th style="width:10em">State</th>
                        <th style="width:10em">Date of Request</th>
                        <th style="width:10em">Due Date</th>
                        <th style="width:10em">Requested For</th>                                            
                    </tr>
                </thead>
                <tbody>
                    <%
                    ArrayList <String[]> requests = db.get_requests.closed_complete_requests_for_sc_item_id_by_date(con, catalog_item_id, filter_start, filter_end);
                    for(int a = 0; a < requests.size(); a++)
                    {
                        String this_request[] = requests.get(a);
                        String request_date = "";
                        try
                        {
                            request_date = display_format.format(timestamp_format.parse(this_request[7]));
                        }
                        catch (Exception e)
                        {
                            request_date = "";
                        }
                        String due_date = "";
                        try
                        {
                            due_date = display_format.format(timestamp_format.parse(this_request[8]));
                        }
                        catch (Exception e)
                        {
                            due_date = "";
                        }
                        %>
                        <tr onclick="location.href='request_list.jsp?id=<%=this_request[0]%>';" style="cursor:pointer;">
                        <%
                        String display_description = this_request[29];
                        %>
                        <td><%=this_request[0]%></td>
                        <td><%=display_description%></td>
                        <td><%=this_request[22]%></td>
                        <td><%=this_request[23]%></td>
                        <td><%=request_date%></td>
                        <td><%=due_date%></td>
                        <td><%=this_request[51]%></td>
                        </tr>
                    <%
                    }                                        
                    %>  
                </tbody>
            </table>
        </div>          


            <%
            //get all the published catalog items
            ArrayList <String[]> catalog_items = db.get_service_catalog.service_catalog_items_by_state(con, "Published");
            //create Arrays to store the results for sortable functions
            String name_array[][] = new String[catalog_items.size()][12];
            
            %>
                            
            <script>                
                Highcharts.chart('sla_chart', {
                    chart: {
                        type: 'bar'
                    },    
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: ['Critical', 'High', 'Medium', 'Low'],
                        title: {
                                text: null
                        }
                    },
                    yAxis: {
                        min: 0,
                        max:100,
                        title: {
                            text: 'Compliance %',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    tooltip: {
                        valueSuffix: '%'
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true,
                                Suffix: '%'
                            }
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: '',
                        data: [
                                <%=data%>
                            ]      
                    }]
                });    
            </script>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN VENDOR JS-->

    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
<!--    <script src="app-assets/js/core/app-menu.js"></script>
    <script src="app-assets/js/core/app.js"></script>-->
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->

    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
    <!--<script src="app-assets/js/scripts/popover/popover.js"></script>-->
    
    <script>
        
        function reload_page()
        {
            var date_range = document.getElementById("date_range").value;
            var URL = "request_service_compliance_for_item.jsp?catalog_item_id=" + getQueryStringParameter("catalog_item_id") + "&referer=<%=referer%>&date_range=" + date_range;
            window.location.href = URL;
        }
    function apply_filters()
    {
        $("#filter_form input,#filter_form select").each(function() {

            var field = $(this);
//            console.log("filter_form_field", field);
            var field_name = field.attr('name');
            
            if (!field_name || field_name == "service_type" || field.attr('type') == "hidden" || (field.attr('type') == "radio" && !field.prop("checked")))
            {
                return;
            }
            if (field.val() && field.val() != "Any")
            {
                var label = "";
                var filter_label = "";
                if (field.attr("type") == "radio")
                {
                    label = "Predefined";
                    filter_label = field.siblings("label").html();
                } else {
                    label = field.closest("div").prev().html();
                    filter_label = field.val();
                }
                if ($("#active_filter_" + field_name).length > 0) {
                    var filter_block = $("#active_filter_" + field_name);
                    $(filter_block).children("span").html(filter_label);
                } else {
                    $("#active_filters").append(
                        $('' +
                            '<div class="clr tag dInlineBlock formField md whtbg mr-5 mb-15" id="active_filter_' + field_name + '">' +
                                '<h5 class="basic-font mediumFont">' + label + '</h5>' +
                                '<span>' + filter_label + '</span>' +
                                '<img class="icon" src="assets/images/svg/cross-icon.svg" alt="" onclick="resetFilterFieldNew(\'' + field_name + '\')">' +
                            '</div>'
                        )
                    );
                }
                setQueryStringParameter(field_name, field.val());
            } else {
                resetFilterFieldNew(field_name);
            }                
        });
    }
    
    function resetFilterFieldNew(field_name)
    {
        var filter_block = $("#active_filter_" + field_name);
        if (filter_block.length > 0)
        {
            filter_block.remove();
            var filter_field = $("#filter_form input[name='" + field_name + "'],#filter_form select[name='" + field_name + "']");
            if (filter_field.attr("type") != "radio")
            {
                filter_field.val("");
                if (field_name.indexOf("_name") !== false)
                {
                    var filter_id_fields = $("#filter_form input[name='" + field_name.replace("_name","_id") + "'],#filter_form select[name='" + field_name.replace("_name","_id") + "']");
                    if (filter_id_fields.length > 0)
                    {
                        filter_id_fields.val("");
                    }
                }
            } else {
                filter_field.prop('checked', false);
            }
            setQueryStringParameter(field_name, "");
            window.location.reload();
        }
    }
    window.addEventListener("load", (event) => 
    {
        apply_filters();
        $('#filters_dropdown').on('hide.bs.dropdown', function (e) 
        { // prevent dropdown close on autocomplete
            console.log("dropdown closed ", e);
            //            console.log(e.clickEvent.target.className);
            if (e.clickEvent && e.clickEvent.target && (e.clickEvent.target.className == "ui-menu-item-wrapper" || e.clickEvent.target.className == "icon" || e.clickEvent.target.className.indexOf("applyBtn") !== -1)) {
                e.preventDefault();
            }
        });

    });
    </script>

    </body>
</html>
<%
        }//end if not permission
    }//end if not logged in
%>
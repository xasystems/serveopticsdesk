<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->

<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        boolean page_authorized = support.role.authorized(session, "USER","create");         
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
%>
<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>	

<jsp:include page='menu_service_desk.jsp'>
    <jsp:param name="active_menu" value=""/>
</jsp:include>
<!-- BEGIN PAGE LEVEL CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/selects/select2.min.css">
<!-- END PAGE LEVEL CSS-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- start content here-->
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">Users</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Administration</a></li>
                            <li class="breadcrumb-item"><a href="admin_users.jsp">Users</a></li>
                            <li class="breadcrumb-item"><a href="#">Bulk Add</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div> <!--End breadcrumbs -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Add User(s) Bulk</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="row">
                                <form method="post" action="import_users" name="in_form" id="in_form" enctype="multipart/form-data" onsubmit="return validateForm()" >
                                    <div class="col-md-12">
                                        <ul>
                                            <li>
                                                Step 1: Export the user records and save the file somewhere safe.
                                                &nbsp;&nbsp;&nbsp;
                                                <button type="button" onclick="javascript:location.href='export_users'" class="btn btn-primary btn-sm">Export Users</button>
                                            </li>
                                            <li>
                                                Step 2: Download the spreadsheet template
                                                &nbsp;&nbsp;&nbsp;
                                                <a href="download_template" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Download Template</a>
                                            </li>
                                            <li>Step 3: Fill out the spreadsheet with all the new users</li>
                                            <li>Step 4: Save the spreadsheet as a csv file</li>
                                            <li>
                                                Step 5: Upload the spreadsheet (in csv format) to ServeOpticsDesk
                                                <input type="file" class="btn btn-primary btn-sm" id="import_file" name="import_file">
                                                &nbsp;&nbsp;&nbsp;
                                                <button type="submit" class="btn btn-primary btn-sm">Submit spreadsheet</button>
                                            </li>
                                            <li>Step 6: Review the upload results for failed user inputs, duplicates, etc....</li>
                                        </ul>
                                    </div>
                                </form>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <hr>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <b>NOTE:</b> The "admin" and "Unassigned" users can not be managed via any bulk operation.
                                    <br>
                                    The spreadsheet has the following required fields:
                                    <ul>
                                        <li>User name: Best practice, use Email address</li>
                                        <li>Password: Blank password will result in the user not being able to use ServeOpticsDesk. Which is fine for users that that will not need to login to ServeOpticsDesk. Passwords will be encrypted once they are loaded into the database.</li>
                                        <li>Email Address: Although not required, it is needed to Email the user updates and status changes to their Incident/Requests.</li>
                                        <li>Role(s): The spreadsheet has two columns for roles. One of those columns is dedicated to Self Service and the other can be any role name defined in your ServeOpticsDesk Instance. If no roles are defined the user will not be able to use ServeOpticsDesk, but their user record can be referenced in Incidents or Requests. They can receive Emails from ServeOpticsDesk.</li>
                                        <li>Overwrite: This is a Global setting. If Overwrite is "true" then any existing user record will be overwritten. If overwrite is "false", then any duplicate user record will be ignored.</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end content here-->
    </div>        
</div>
<jsp:include page='footer.jsp'>
    <jsp:param name="parameter" value=""/>
</jsp:include>	
<!-- BEGIN PAGE LEVEL JS-->
<script>
    function validateForm() 
    {
        var import_file = document.forms["in_form"]["import_file"].value;
        //alert("import_file=" + import_file);
        if (import_file === "") 
        {
            alert("An import file must be selected.");
            return false;
        } 
        else 
        {
            return true;
        }
    }
</script>


<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="app-assets/vendors/js/forms/select/select2.full.min.js"></script>
<script src="app-assets/js/scripts/forms/select/form-select2.js"></script>
<!-- END PAGE LEVEL JS-->   
<%                con.close();
    
            } catch (Exception e) {
                System.out.println("Exception in admin_user_add.jsp: " + e);
                logger.debug("ERROR: Exception in admin_user_add.jsp:" + e);
            }
        } //end if not admin permission
    } //end if not redirected
%>
</body>
</html>

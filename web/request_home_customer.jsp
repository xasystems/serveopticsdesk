<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        if (!session.getAttribute("request").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
            java.util.Date filter_start = new java.util.Date();
            java.util.Date filter_end = new java.util.Date();
            String start = "";
            String end = "";
            String date_range = "";
            String selected = "";
            String support_group_id = "";
            String customer_group_id = "all";
            int total_request_count = 0;
            int total_request_count_previous_period = 0;
            double this_period_csat_survey_score[] = {0,0,0,0};
            double previous_period_csat_survey_score[] = {0,0,0,0};
           
            ArrayList<String[]> groups = db.get_groups.all(con);
            String view = request.getParameter("view");
            if (view == null || view.equalsIgnoreCase("null")) 
            {
                view = "support";
            }

            try 
            {
                date_range = request.getParameter("date_range");
                if (date_range.equalsIgnoreCase("null") || date_range == null) 
                {
                    date_range = support.filter_dates.past_30_days();
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                date_range = support.filter_dates.past_30_days();
            }
            try 
            {
                support_group_id = request.getParameter("support_group_id");
                if (support_group_id.equalsIgnoreCase("null") || support_group_id == null) 
                {
                    support_group_id = "all";
                }
            } 
            catch (Exception e) 
            {
                support_group_id = "all";
            }
            try 
            {
                customer_group_id = request.getParameter("customer_group_id");
                if (customer_group_id.equalsIgnoreCase("null") || customer_group_id == null) 
                {
                    customer_group_id = "all";
                }
            } 
            catch (Exception e) 
            {
                customer_group_id = "all";
            }
            String temp[] = date_range.split("-");
            filter_start = filter_format.parse(temp[0]);
            //filter_start = filter_format.parse("01/12/2019 12:00 AM");

            filter_end = filter_format.parse(temp[1]);
            //System.out.println("filter_end=" + temp[1].trim() + " ====" + filter_end);
            
            total_request_count = db.get_requests.request_count_start_date_stop_date_group_id_support(con, filter_start, filter_end, customer_group_id);
            //get pervious date frame
            java.util.Date previous_start_date = new java.util.Date();
            java.util.Date previous_end_date = new java.util.Date();
            previous_start_date.setTime(filter_start.getTime());
            previous_end_date.setTime(filter_end.getTime());
            long diff = filter_end.getTime() - filter_start.getTime();
            previous_start_date.setTime(filter_start.getTime() - diff);
            previous_end_date.setTime(filter_end.getTime() - diff);
            total_request_count_previous_period = db.get_requests.request_count_start_date_stop_date_group_id_support(con, previous_start_date, previous_end_date, customer_group_id);

            //QUEUE/////////////////////////////////////////////////////////////////////////
            start = timestamp.format(filter_start);
            end = timestamp.format(filter_end);

            //System.out.println("group_id=" + group_id);
            String request_dashboard_queue_by_priority[][] = db.get_chart_data.request_support_dashboard_queue_by_priority_support(con, filter_start, filter_end, customer_group_id);
            String request_dashboard_count_by_priority_data = "";

            for (int a = 0; a < request_dashboard_queue_by_priority.length; a++) 
            {
                if (a == 0) 
                {
                    request_dashboard_count_by_priority_data = "{name: \"" + request_dashboard_queue_by_priority[a][0] + "\","
                            + " y:" + request_dashboard_queue_by_priority[a][1] + ","
                            + "priority: \"" + request_dashboard_queue_by_priority[a][2] + "\","
                            + "color: \"" + request_dashboard_queue_by_priority[a][3] + "\"}";
                } 
                else 
                {
                    request_dashboard_count_by_priority_data = request_dashboard_count_by_priority_data + ",{name: \"" + request_dashboard_queue_by_priority[a][0] + "\","
                            + " y:" + request_dashboard_queue_by_priority[a][1] + ","
                            + "priority: \"" + request_dashboard_queue_by_priority[a][2] + "\","
                            + "color: \"" + request_dashboard_queue_by_priority[a][3] + "\"}";
                }
            }
            ///////////////////////////////////////////////////////////////////////////
            //Number chart data
            String request_dashboard_number_of_request_categories = "";
            String request_dashboard_number_of_requests_data = "";
            
            String request_dashboard_number_of_requests[][] = db.get_chart_data.request_dashboard_number_of_requests_support(con, filter_start, filter_end, customer_group_id);
            //12-Dec,y_value,12/12/2018 
            //build categories string categories: ['12-Dec', '13-Dec', '14-Dec', '15-Dec']
            for(int b =0; b < request_dashboard_number_of_requests.length; b++)
            {
                if(b == 0)
                {
                    request_dashboard_number_of_request_categories = "'" + request_dashboard_number_of_requests[b][0] + "'";
                    request_dashboard_number_of_requests_data = "{y:" + request_dashboard_number_of_requests[b][1] + ", date:'" + request_dashboard_number_of_requests[b][2] + "', date_range:'" + request_dashboard_number_of_requests[b][3] + "'}";
                }
                else
                {
                    request_dashboard_number_of_request_categories = request_dashboard_number_of_request_categories + ",'" + request_dashboard_number_of_requests[b][0] + "'";
                    request_dashboard_number_of_requests_data = request_dashboard_number_of_requests_data + ",{y:" + request_dashboard_number_of_requests[b][1] + ", date:'" + request_dashboard_number_of_requests[b][2] + "', date_range:'" + request_dashboard_number_of_requests[b][3] + "'}";
                }
            }
            //////////////////////////////////////////////////////////////////////////////////
            //Top 5 groups
            String request_dashboard_top_5_categories = "";
            String request_dashboard_top_5_data = "";
            String request_dashboard_top_chart_data[][] = db.get_chart_data.request_dashboard_top_5_categories_customer(con, filter_start, filter_end,customer_group_id);
            for(int c = 0; c < request_dashboard_top_chart_data.length;c++)
            {
                if(c == 0)
                {
                    request_dashboard_top_5_categories = "'" + request_dashboard_top_chart_data[c][0] + "'";  //'1','2','3','4','5'
                    request_dashboard_top_5_data = "{category: '" + request_dashboard_top_chart_data[c][0] + "',group_id: '" + request_dashboard_top_chart_data[c][2] + "',y:" + request_dashboard_top_chart_data[c][1] + "}"; //{y: 7,category: "Cat 1"},
                }
                else
                {
                    request_dashboard_top_5_categories = request_dashboard_top_5_categories +  ",'" + request_dashboard_top_chart_data[c][0] + "'";
                    request_dashboard_top_5_data = request_dashboard_top_5_data + ",{category: '" + request_dashboard_top_chart_data[c][0] + "',group_id: '" + request_dashboard_top_chart_data[c][2] + "',y:" + request_dashboard_top_chart_data[c][1] + "}";
                }
            }
            ///////////////////////////////////////////////////////////////////////////////////
            //Closure Rate 
            String request_dashboard_closure_rate_categories = "";
            String request_dashboard_closure_rate_critical_data = "";
            String request_dashboard_closure_rate_high_data = "";
            String request_dashboard_closure_rate_medium_data = "";
            String request_dashboard_closure_rate_low_data = "";
            String request_dashboard_closure_rate_data[][] = db.get_chart_data.request_dashboard_closure_rate_customer(con, filter_start, filter_end, customer_group_id);
            for(int d = 0; d < request_dashboard_closure_rate_data.length;d++)
            {
                if(d ==0)
                {
                    request_dashboard_closure_rate_categories = "'" + request_dashboard_closure_rate_data[d][0] + "'";
                    request_dashboard_closure_rate_low_data = "{priority:'Low',y:" + request_dashboard_closure_rate_data[d][2] + ",date:'" +  request_dashboard_closure_rate_data[d][1] + "',date_range:'" + request_dashboard_closure_rate_data[d][6] + "'}" ;
                    request_dashboard_closure_rate_medium_data = "{priority:'Medium',y:" + request_dashboard_closure_rate_data[d][3] + ",date:'" +  request_dashboard_closure_rate_data[d][1] + "',date_range:'" + request_dashboard_closure_rate_data[d][6] + "'}" ;
                    request_dashboard_closure_rate_high_data = "{priority:'High',y:" + request_dashboard_closure_rate_data[d][4] + ",date:'" +  request_dashboard_closure_rate_data[d][1] + "',date_range:'" + request_dashboard_closure_rate_data[d][6] + "'}" ;;
                    request_dashboard_closure_rate_critical_data = "{priority:'Critical',y:" + request_dashboard_closure_rate_data[d][5] + ",date:'" +  request_dashboard_closure_rate_data[d][1] + "',date_range:'" + request_dashboard_closure_rate_data[d][6] + "'}" ;
                }
                else
                {
                    request_dashboard_closure_rate_categories = request_dashboard_closure_rate_categories + ",'" + request_dashboard_closure_rate_data[d][0]+ "'";
                    request_dashboard_closure_rate_low_data = request_dashboard_closure_rate_low_data + ",{priority:'Low',y:" + request_dashboard_closure_rate_data[d][2] + ",date:'" +  request_dashboard_closure_rate_data[d][1] + "',date_range:'" + request_dashboard_closure_rate_data[d][6] + "'}" ;
                    request_dashboard_closure_rate_medium_data = request_dashboard_closure_rate_medium_data + ",{priority:'Medium',y:" + request_dashboard_closure_rate_data[d][3] + ",date:'" +  request_dashboard_closure_rate_data[d][1] + "',date_range:'" + request_dashboard_closure_rate_data[d][6] + "'}" ;
                    request_dashboard_closure_rate_high_data = request_dashboard_closure_rate_high_data + ",{priority:'High',y:" + request_dashboard_closure_rate_data[d][4] + ",date:'" +  request_dashboard_closure_rate_data[d][1] + "',date_range:'" + request_dashboard_closure_rate_data[d][6] + "'}" ;
                    request_dashboard_closure_rate_critical_data = request_dashboard_closure_rate_critical_data + ",{priority:'Critical',y:" + request_dashboard_closure_rate_data[d][5] + ",date:'" +  request_dashboard_closure_rate_data[d][1] + "',date_range:'" + request_dashboard_closure_rate_data[d][6] + "'}" ;
                }
            }
            
            //survey trend
            String this_period_survey_results[][] = db.get_survey_results.by_trigger_object_date_range(con, "request","customer", support_group_id, customer_group_id, filter_start, filter_end);
            String previous_period_survey_results[][] = db.get_survey_results.by_trigger_object_date_range(con, "request","customer",support_group_id, customer_group_id , previous_start_date, previous_end_date);
            this_period_csat_survey_score = db.get_survey_results.csat_score_for_survey_results(this_period_survey_results);
            previous_period_csat_survey_score = db.get_survey_results.csat_score_for_survey_results(previous_period_survey_results);
            
            String color = "";
            String arrow = "";
                                            
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>
    <input type="hidden" name="support_group_id" id="support_group_id" value="<%=support_group_id%>"/>
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title">
                        <i class="la la-shopping-cart"></i>
                        &nbsp;Request
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <button onclick="load_customer_page()"  type="button" class="btn mr-1 mb-1 btn-primary ">Customer View</button>
                        &nbsp;&nbsp;
                        <button onclick="load_support_page()" type="button" class="btn btn-outline-primary btn-min-width mr-1 mb-1 ">Support View</button>
                    </h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Request
                                </li>
                            </ol>
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-6">
                    <div class="form-group">
                        <label>Date Range</label>
                        <div class='input-group'>
                            <input type='text' id="filter_date_range" name="filter_date_range" value="<%=date_range%>" class="form-control datetime" />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <span class="la la-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-4">
                    <div class="form-group">
                        <label>Customer Group</label>
                        <div class='input-group'>
                            <select class="select2-placeholder form-control" name="customer_group_id" id="customer_group_id">
                                <option value="all">All</option>
                                <%
                                for (int a = 0; a < groups.size(); a++) 
                                {
                                    String this_group[] = groups.get(a);
                                    selected = "";
                                    if (this_group[0].equalsIgnoreCase(customer_group_id)) 
                                    {
                                        selected = "SELECTED";
                                    }
                                    %>
                                    <option <%=selected%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                    <%
                                }
                                %>
                            </select>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <span class="la la-group"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>                
                <div class="col-xl-1 col-2">
                    <label>&nbsp;</label>
                    <div class="form-actions">
                        <button type="button" onclick="reload_page()" class="btn btn-primary mr-1">
                            <i class="fa-filter"></i> Apply
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">    
                <div class="col-xl-2 col-lg-6 col-12">

                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <table width="100%">
                                    <tr>
                                        <td colspan="3"><h4 class="text-muted" onclick="javascript:location.href='request_home_customer.jsp?date_range=<%=date_range%>&customer_group_id=<%=customer_group_id%>'; return false;" style="cursor: pointer;"><b>Total Requests<b></h4></td>
                                    </tr>
                                    <tr>
                                        <td width="10%">
                                            <%
                                            int diff_stat = total_request_count - total_request_count_previous_period;
                                            if(diff_stat == 0)
                                            {
                                                color = "info";
                                                arrow = "la la-minus";
                                            }
                                            else
                                            {
                                                if(diff_stat > 0)
                                                {
                                                    color = "danger";
                                                    arrow = "ft-chevron-up";
                                                }
                                                else
                                                {
                                                    color = "success";
                                                    arrow = "ft-chevron-down";
                                                }
                                            }
                                            %>
                                            <i class="<%=arrow%> <%=color%> font-large-2" data-toggle="popover" data-content="The Change (+ or -) value is the difference from this period to the previous period." data-trigger="hover" data-original-title="Total Requests"></i>
                                        </td>
                                        <td>
                                            <h5>
                                                <%=total_request_count - total_request_count_previous_period%>   
                                            </h5>
                                        </td>
                                        <td align="right"><h2 onclick="javascript:location.href='request_total_requests.jsp?date_range=<%=date_range%>&group_id=<%=customer_group_id%>&view=<%=view%>'; return false;" style="cursor: pointer;"><%=total_request_count%></h2></td>
                                    </tr>
                                </table>                            
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-6 col-12">
                    <a href="request_service_compliance.jsp?referer=request_home_customer.jsp&date_range=<%=date_range%>">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <table width="100%">
                                        <tr>
                                            <td colspan="3"><h4 class="text-muted"><b>Service Compliance</b></h4></td>
                                        </tr>
                                        <tr>
                                            <%
                                            double previous_period_score = 0;
                                            double this_period_score = 0;

                                            //get all active/reportable contact SLAs
                                            String reportable_request_sla[][] = db.get_sla.all_reportable_request(con);
                                            //get scores for previous period
                                            for(int a = 0; a < reportable_request_sla.length;a++)
                                            {
                                                //System.out.println("FOR previous_period_score=" + previous_period_score);
                                                previous_period_score = previous_period_score + support.sla_request_calc.for_sla_id(con, reportable_request_sla[a][0], previous_start_date, previous_end_date);
                                            }
                                            //get the avg score for all sla's
                                            previous_period_score = previous_period_score / reportable_request_sla.length;
                                            //System.out.println("previous_period_score=" + previous_period_score);
                                            //get scores for this period
                                            for(int a = 0; a < reportable_request_sla.length;a++)
                                            {
                                                this_period_score = this_period_score + support.sla_request_calc.for_sla_id(con, reportable_request_sla[a][0], filter_start, filter_end);
                                            }
                                            //get the avg score for all sla's
                                            this_period_score = this_period_score / reportable_request_sla.length;
                                            //System.out.println("this_period_score=" + this_period_score);
                                            if(this_period_score < previous_period_score)
                                            {
                                                color = "danger";
                                                arrow = "ft-chevron-down";
                                            }
                                            else if(this_period_score > previous_period_score)
                                            {
                                                color = "success";
                                                arrow = "ft-chevron-up";
                                            }
                                            else if(this_period_score == previous_period_score)
                                            {
                                                color = "info";
                                                arrow = "la la-pause";
                                            }
                                            double score_diff = this_period_score - previous_period_score;
                                            %>
                                            <td width="10%"><i class="<%=arrow%> <%=color%> font-large-2" data-toggle="popover" data-content="Service Compliance is the result of meeting service agreement criteria between a service provider and the user community that define measurable targets to be achieved. The displayed 'Value' is the actual Month-To-Date % of target achieved compared to the previous Month-To-Date % achievement." data-trigger="hover" data-original-title="Service Compliance"></i></td>
                                            <td><h5><%=String.valueOf(String.format("%1$,.2f", score_diff))%>%</h5></td>
                                            <td align="right"><h2><%=String.valueOf(String.format("%1$,.2f", this_period_score))%>%</h2></td>
                                        </tr>
                                    </table>                            
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!--<div class="col-xl-2 col-lg-6 col-12">
                    <a href="incident_tier_n_utilization.jsp">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <table width="100%">
                                        <tr>
                                            <td colspan="3"><h4 class="text-muted"><b>Tier N Participation</b></h4></td>
                                        </tr>
                                        <tr>
                                            <td width="10%"><i class="ft-chevron-down info font-large-2" data-toggle="popover" data-content="Tier N Utilization is a measure of the average number of Incidents handled by an analyst or technical support group in a month MULTIPLIED by the average Incident handle time in minutes DIVIDED by the average number of days worked by that analyst in a month MULTIPLIED by the number of hours worked in a day MULTIPLIED by 60 minutes." data-trigger="hover" data-original-title="Tier N Utilization"></i></td>
                                            <td><h5>-1.2%</h5></td>
                                            <td align="right"><h2>43.6%</h2></td>
                                        </tr>
                                    </table>                            
                                </div>
                            </div>
                        </div>
                    </a>
                </div>-->
               <div class="col-xl-2 col-lg-6 col-12">                
                    <div class="card" onclick="javascript:location.href='request_survey.jsp?date_range=<%=date_range%>&support_group_id=all&customer_group_id=<%=customer_group_id%>'; return false;" style="cursor: pointer;">
                        <div class="card-content">
                            <div class="card-body">
                                <%
                                //double return_double[] = {0.0,0.0,0.0,0.0};  //Sent,Completed,Completion Rate,Avg. Score
                                double survey_diff = this_period_csat_survey_score[3] - previous_period_csat_survey_score[3];
                                String survey_color = "danger"; //
                                String total_survey_arrow = "ft-chevron-up";
                                if(survey_diff > 0)
                                {
                                    survey_color = "success";
                                    total_survey_arrow = "ft-chevron-up";
                                }
                                else
                                {
                                    if(survey_diff < 0)
                                    {
                                        survey_color = "danger";
                                        total_survey_arrow = "ft-chevron-down";
                                    }
                                    else
                                    {
                                        if(survey_diff == 0)
                                        {
                                            survey_color = "info";
                                            total_survey_arrow = "la la-pause";
                                        }
                                    }
                                }
                                %>
                                <table width="100%">
                                    <tr>
                                        <td colspan="3"><h4 class="text-muted"><b>CSAT Surveys</b></h4></td>
                                    </tr>
                                    <tr>
                                        <td width="10%"><i class="<%=total_survey_arrow%> <%=survey_color%> font-large-2" data-toggle="popover" data-content="Customer Satisfaction is a measure based on the collection of returned survey responses. The 'Value' is this periods average compared to the last periods average. The maximum survey value is 5." data-trigger="hover" data-original-title="Customer Satisfaction"></i></td>
                                        <td><h5><%=String.format( "%.2f", survey_diff)%></h5></td>
                                        <td align="right"><h2><%=String.format( "%.2f", this_period_csat_survey_score[3] )%></h2></td>
                                    </tr>
                                </table>                           
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title"><b>Request Queue by Priority</b></h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body pt-0">
                                <div id="request_queue_container" class="chart" style="min-width: 250px; height: 240px; margin: 0 auto"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title"><b>Number of Daily Requests</b></h4>                     
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body pt-0">
                                <div id="request_number_container" class="chart" style="min-width: 250px; height: 240px; margin: 0 auto"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>     
            <div class="row">
                <div class="col-xl-6 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title"><b>Top Request Support Groups</b></h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body pt-0">
                                <div id="request_top_5_categories_container" class="chart" style="min-width: 250px; height: 240px; margin: 0 auto"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title"><b>Closure Rate</b></h4>                     
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body pt-0">
                                <div id="request_closure_rate_container" class="chart" style="min-width: 250px; height: 240px; margin: 0 auto"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <script>
        function reload_page()
        {
            var support_group_id = document.getElementById("support_group_id").value;
            var customer_group_id = document.getElementById("customer_group_id").value;
            var filter_date_range = document.getElementById("filter_date_range").value;
            var URL = "request_home_customer.jsp?date_range=" + filter_date_range + "&customer_group_id=" + customer_group_id + "&view=support";
            window.location.href = URL;
        }
        function load_customer_page()
        {
            var support_group_id = document.getElementById("support_group_id").value;
            var customer_group_id = document.getElementById("customer_group_id").value;
            var filter_date_range = document.getElementById("filter_date_range").value;
            var URL = "request_home_customer.jsp?date_range=" + filter_date_range + "&customer_group_id=" + customer_group_id;
            window.location.href = URL;
        }
        function load_support_page()
        {
            var support_group_id = document.getElementById("support_group_id").value;
            var customer_group_id = document.getElementById("customer_group_id").value;
            var filter_date_range = document.getElementById("filter_date_range").value;
            var URL = "request_home_support.jsp?date_range=" + filter_date_range + "&support_group_id=" + support_group_id;
            window.location.href = URL;
        }
    </script>
    
    <script>
        $(function () {
            Highcharts.chart('request_queue_container',
                    {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: null
                        },
                        exporting: {
                            enabled: false
                        },
                        credits: {
                            enabled: false
                        },
                        legend: {
                            enabled: false
                        },
                        xAxis: {
                            type: 'category',
                            minorTickLength: 0,
                            tickLength: 0
                        },
                        yAxis: {
                            title: {
                                x: -10,
                                text: 'Request Count'
                            }
                        },
                        plotOptions: {
                            column: {
                                dataLabels: {
                                    enabled: true
                                }
                            },
                            series: {
                                cursor: 'pointer',
                                point: {
                                    events: {
                                        click: function (event) {
                                            window.location = "request_chart_drilldown_customer.jsp?chart_type=queue&priority=" + event.point.priority + "&date_range=" + document.getElementById("filter_date_range").value + "&customer_group_id=" + document.getElementById("customer_group_id").value;
                                        }
                                    }
                                }
                            }
                        },
                        series: [
                            {
                                name: "Request Count",
                                color: '#64b5f6',
                                data: [
                                    <%=request_dashboard_count_by_priority_data%>
                                ]
                            }
                        ]
                    });
        });
    </script>
    <script>
        $(function () {
            Highcharts.chart('request_number_container', {
            chart: {
                type: 'spline'
            },
            title: {
                text: null
            },
            exporting: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            xAxis: {
                categories: [<%=request_dashboard_number_of_request_categories%>]
            },
            yAxis: {
                title: {
                    x: -10,
                    text: 'Number'
                }
            },

            plotOptions: {
                spline: {
                    marker: {
                        enabled: false
                    }
                },
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (event) {
                                window.location = "request_chart_drilldown_customer.jsp?chart_type=number&priority=all&date_range=" + event.point.date_range + "&customer_group_id=" + document.getElementById("customer_group_id").value;
                            }
                        }
                    }
                }
            },
            series: [{
                    name: "Number of Daily Requests",
                    color: '#64b5f6',
                    data: [
                        <%=request_dashboard_number_of_requests_data%>
                    ]
                }]
        });
    });
    </script>
    <script>
        $(function () {
        Highcharts.chart('request_top_5_categories_container', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: null
                },
                exporting: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                xAxis: {
                    categories: [<%=request_dashboard_top_5_categories%>],
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0
                },
                yAxis: {
                    visible: false
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        },
                        borderWidth: 11,
                        cursor: 'pointer',
                        point: {
                            events:{
                                click: function(event) {
                                    window.location = "request_chart_drilldown_customer.jsp?chart_type=category&priority=all&category=" + event.point.group_id + "&date_range=" + document.getElementById("filter_date_range").value + "&customer_group_id=" + document.getElementById("customer_group_id").value;
                                }
                            }
                        }
                    }
                },
                series: [{
                    name: 'Count',
                    color: '#64b5f6',
                    data: [<%=request_dashboard_top_5_data%>]
                }]
            });
        });
    </script>   
    <script>
        $(function () {
            Highcharts.chart('request_closure_rate_container', {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: null
                },
                exporting: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: [<%=request_dashboard_closure_rate_categories%>]
                },
                yAxis: {
                    title: {
                        x: -10,
                        text: 'Closure Rate'
                    }
                },
                plotOptions: {
                    spline: {
                        marker: {
                            enabled: false
                        }
                    },
                    series: {
                        events: {
                            click: function(event) {
                                window.location = "request_chart_drilldown_customer.jsp?chart_type=closure&priority=" + event.point.priority + "&date_range=" + event.point.date_range + "&customer_group_id=" + document.getElementById("customer_group_id").value;
                            }
                        }
                    }
                },
                series: [
                    {
                        name: "Critical",
                        color: "#ef5350",
                        data:[<%=request_dashboard_closure_rate_critical_data%>]
                    },
                    {
                        name: "High",
                        color: "#ffa726",
                        data:[<%=request_dashboard_closure_rate_high_data%>]
                    },
                    {
                        name: "Medium",
                        color: "#ffee58",
                        data:[<%=request_dashboard_closure_rate_medium_data%>]
                    },
                    {
                        name: "Low",
                        color: '#64b5f6',
                        data:[<%=request_dashboard_closure_rate_low_data%>]
                    }
                ]
            });
        });
    </script>
    
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
    <script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
    <script src="assets/js/highcharts/highcharts.js"></script>
    <script src="assets/js/highcharts/data.js"></script>
    <script src="assets/js/highcharts/drilldown.js"></script>
    <script src="assets/js/highcharts/exporting.js"></script>
    <script src="app-assets/js/scripts/popover/popover.js"></script>
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%
        }//end if not permission
    }//end if not logged in
%>
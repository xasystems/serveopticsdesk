<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="java.net.URLEncoder"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        Logger logger = LogManager.getLogger();
        String home_page = session.getAttribute("home_page").toString();
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        try
        {
            String to_address = request.getParameter("to_address");
            String type = request.getParameter("type");
            String encoded_number = request.getParameter("number");
            String encoded_subject = request.getParameter("subject");
            String encoded_body = request.getParameter("body");

            String decoded_subject = URLDecoder.decode(encoded_subject, "UTF-8");
            String decoded_body = URLDecoder.decode(encoded_body, "UTF-8");

            /*System.out.println("in type=" + encoded_type );
            type = URLDecoder.decode(encoded_type, "UTF-8");
            System.out.println("out type=" + encoded_type );

            System.out.println("in subject=" + encoded_subject );
            subject = URLDecoder.decode(subject, "UTF-8");
            System.out.println("out subject=" + subject );

            System.out.println("in body=" + body);
            body = URLDecoder.decode(body, "UTF-8");
            System.out.println("out body=" + body);
            */
            
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END PAGE LEVEL CSS-->
    
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title">Test Email Results</h3>
                    
                </div>
            </div> <!--End breadcrumbs -->
            <div class="row match-height">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form" action="admin_email" method="post">
                                    <p class="card-text">Test Email Settings</p>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="to_address">To:</label>
                                                <%=to_address%>
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="subject">Subject:</label>
                                                <%=decoded_subject%>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="body">Body:</label>
                                                <%=decoded_body%>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="body">Results:</label>
                                                
                                            </div>
                                        </div> 
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <!-- END PAGE LEVEL JS-->   
    <%
        
        }
        catch(Exception e)
        {
            System.out.println("Exception in admin_user.jsp: " + e); 
            logger.debug("ERROR: Exception in admin_user.jsp:" + e);
        }
    }//end if not redirected
    %>
    </body>
</html>

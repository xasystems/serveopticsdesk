
<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : incident_total_incidents
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="db.get_incidents"%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>

<%@ page import="javax.json.Json"%>
<%@ page import="javax.json.JsonArray"%>
<%@ page import="javax.json.JsonObject"%>
<%@ page import="javax.json.JsonReader"%>
<%@ page import="javax.json.JsonStructure"%>
<%@page import="java.io.StringReader"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        if (!session.getAttribute("request").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            try
            {
                String home_page = session.getAttribute("home_page").toString();
                String context_dir = request.getServletContext().getRealPath("");
                LinkedHashMap props = support.config.get_config(context_dir);
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
                SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
                SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19

                //SimpleDateFormat filter_format2 = new SimpleDateFormat("yyyy"); //01/12/2019 12:00 AM
                java.util.Date filter_start = new java.util.Date();
                java.util.Date filter_end = new java.util.Date();
                String start = "";
                String end = "";
                //System.out.println("format2=" + filter_format2.parse("2016"));
                //System.out.println("format" + filter_format.parse("01/12/2019 12:00 AM"));

                String sla_id = request.getParameter("sla_id");
                String date_range = "";
                String referer = "";
                try 
                {
                    date_range = request.getParameter("date_range");
                    if (date_range.equalsIgnoreCase("null") || date_range == null) 
                    {
                        date_range = support.filter_dates.past_30_days();
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to past 30 days
                    date_range = support.filter_dates.past_30_days();
                }
                try 
                {
                    referer = request.getParameter("referer");
                    if (referer.equalsIgnoreCase("null") || referer == null) 
                    {
                        referer = "request_home_support.jsp?date_range=" + date_range;
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to past 30 days
                    referer = "request_home_support.jsp?date_range=" + date_range;
                }
                try 
                {
                    //01/12/2019 12:00 AM - 01/12/2019 11:59 PM&group_id=all
                    //System.out.println("date_range=" + date_range);
                    String temp[] = date_range.split("-");
                    filter_start = filter_format.parse(temp[0]);
                    //filter_start = filter_format.parse("01/12/2019 12:00 AM");
                    //System.out.println("filter_start=" + temp[0] + " ====" + filter_start);

                    filter_end = filter_format.parse(temp[1]);
                    //System.out.println("filter_end=" + temp[1].trim() + " ====" + filter_end);

                    start = timestamp_format.format(filter_start);
                    end = timestamp_format.format(filter_end);

                } 
                catch (Exception e) 
                {
                    //if not set then default to past 30 days
                    System.out.println("Exception on request_sla_drilldown.jsp=" + e);
                }
                ArrayList<String[]> all_groups = db.get_groups.all(con);

                %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END Custom CSS-->
    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <input type="hidden" name="start" id="start" value="<%=start%>"/>
    <input type="hidden" name="end" id="end" value="<%=end%>"/>
    <input type="hidden" name="sla_id" id="sla_id" value="<%=sla_id%>"/>
    <input type="hidden" name="referer" id="sla_id" value="<%=referer%>"/>
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title"><i class="la la-group"></i>&nbsp;Request SLA</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a>
                                </li>
                                <%
                                if(referer.contains("customer"))
                                {
                                    %>
                                    <li class="breadcrumb-item"><a href="request_home_customer.jsp?date_range=<%=date_range%>">Request Dashboard</a></li>
                                    <%
                                }
                                else
                                {
                                    %>
                                    <li class="breadcrumb-item"><a href="incident_home_support.jsp?date_range=<%=date_range%>">Request Dashboard</a></li>
                                    <%
                                }
                                %> 
                                <li class="breadcrumb-item"><a href="request_service_compliance.jsp?referer=<%=referer%>&date_range=<%=date_range%>">Request SLAs</a></li>
                                <li class="breadcrumb-item active">Request SLA Details</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- start content here-->
            <div class="row">
                <div class="col-xl-3 col-6">
                    <div class="form-group">
                        <label>Date Range</label>
                        <div class='input-group'>
                            <input type='text' id="filter_date_range" name="filter_date_range" value="<%=date_range%>" class="form-control datetime" />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <span class="la la-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-xl-1 col-2">
                    <label>&nbsp;</label>
                    <div class="form-actions">
                        <button type="button" onclick="reload_page()" class="btn btn-blue mr-1">
                            <i class="fa-filter"></i> Apply
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <div class="row">
                                    <div class="col-12">
                                        <%
                                        String compliance_color = "";
                                        String display_threshold = "";
                                        double compliance_value = 0;
                                        double noncompliance_value = 0;
                                        String sla_info[] = db.get_sla.request_by_id(con, sla_id);
                                        String sla_name = sla_info[1];
                                        String sla_description = sla_info[2];
                                        //System.out.println("sla_id=" + sla_id);
                                        
                                        double this_period_score = support.sla_request_calc.for_sla_id(con, sla_id, filter_start, filter_end);
                                        //System.out.println("this_period_score=" + this_period_score);
                                        String request_priority = sla_info[5];
                                        String sla_timeframe = support.string_case_changer.to_camel_case(sla_info[16]);
                                        String sla_target = sla_info[6];
                                        String sla_type = support.string_case_changer.to_camel_case(sla_info[3]);
                                        String sla_threshold_operator = support.string_case_changer.sla_threshold_operator(sla_info[18]);
                                        String sla_threshold_value = sla_info[19];
                                        String sla_threshold_unit = support.string_case_changer.sla_threshold_units(sla_info[20]);
                                        try 
                                        {
                                            JsonReader reader = Json.createReader(new StringReader(sla_info[27]));
                                            JsonObject resultObject = reader.readObject();
                                            JsonArray parameters = (JsonArray) resultObject.getJsonArray("parameters");

                                            for(int b = 0; b < parameters.size(); b++)
                                            {
                                                JsonObject this_record = parameters.getJsonObject(b); //a single jsonrecord
                                                //System.out.println("json=" + this_record);
                                                String field_name = this_record.getJsonString("field_name").toString().replace("\"","");
                                                String operator = this_record.getJsonString("operator").toString().replace("\"","");
                                                JsonArray values = (JsonArray) this_record.getJsonArray("value");
                                                
                                                for(int c = 0; c < values.size(); c++)
                                                {
                                                    String value = values.getString(c);
                                                    if(field_name.contains("group"))
                                                    {
                                                        for(int d = 0; d < all_groups.size();d++)
                                                        {
                                                            String this_group[] = all_groups.get(d);
                                                            if(value.equalsIgnoreCase(this_group[0]))
                                                            {
                                                                value = this_group[1];
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        value = values.getString(c);
                                                    }
                                                    if(c == 0)
                                                    {
                                                        display_threshold = field_name + " " + operator + " '" + value + "'";
                                                    }
                                                    else
                                                    {
                                                        display_threshold = display_threshold + " OR " + field_name + " " + operator + " '" + value + "'";
                                                    }
                                                }              
                                            }
                                        } 
                                        catch (Exception e) 
                                        {
                                            out.println("Exception in record=" + e);
                                        }
                                        String popover_text = sla_threshold_operator + "&nbsp;" + sla_threshold_value + "&nbsp;" + sla_threshold_unit;
                                        if(this_period_score >= Double.parseDouble(sla_target))
                                        {
                                            compliance_color = "#8bc34a";
                                            compliance_value = this_period_score;
                                            noncompliance_value = 100 - this_period_score;
                                        }
                                        else
                                        {
                                            compliance_color = "#ef5350";
                                            compliance_value = this_period_score;
                                            noncompliance_value = 100 - this_period_score;
                                        }
                                        //get incidents that did not comply
                                        ArrayList <String[]> noncompliant_requests = support.sla_request_calc.get_noncompliant_requests_for_sla_id(con, sla_id, filter_start, filter_end);
                                        
                                        %>
                                        <table cellpadding="7">
                                            <thead>
                                                <th>SLA Name&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                <th>SLA Description&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                <th>SLA Type&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                <th>Target&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                <th>Threshold&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                <th>SLA Parameters&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><%=sla_name%>&nbsp;&nbsp;</td>
                                                    <td><%=sla_description%>&nbsp;&nbsp;</td>
                                                    <td><%=sla_type%>&nbsp;&nbsp;</td>
                                                    <td><%=sla_target%>%&nbsp;&nbsp;</td>
                                                    <td><%=popover_text%>&nbsp;&nbsp;</td>
                                                    <td><%=display_threshold%>&nbsp;&nbsp;</td>
                                                    
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <h3>Non-Compliant Request(s)</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">  
                                        <table class="table table-striped table-bordered zero-configuration">
                                            <thead>
                                                <tr>
                                                    <th>Request ID</th>
                                                    <th>Priority</th>
                                                    <th>Date of Request</th>
                                                    <th>Requested By</th>
                                                    <th>Requested For</th>
                                                    <th>Assigned to Group</th>
                                                    <th>Assigned to Analyst</th>
                                                    <th>State</th>
                                                    <th>Description</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%
                                                for(int a = 0; a < noncompliant_requests.size(); a++)
                                                {
                                                    String noncompliant_request[] = noncompliant_requests.get(a);
                                                    String request_date = "";
                                                    try
                                                    {
                                                        request_date = display_format.format(timestamp_format.parse(noncompliant_request[3]));
                                                    }
                                                    catch (Exception e)
                                                    {
                                                        request_date = "";
                                                    }
                                                    %>
                                                    <tr onclick="location.href='request.jsp?id=<%=noncompliant_request[0]%>';" style="cursor:pointer;">
                                                    <td><%=noncompliant_request[0]%></td>
                                                    <td><%=noncompliant_request[17]%></td>
                                                    <td><%=request_date%></td>
                                                    <td><%=noncompliant_request[34] + " " + noncompliant_request[36]%></td>
                                                    <td><%=noncompliant_request[39] + " " + noncompliant_request[41]%></td>
                                                    <td><%=noncompliant_request[27]%></td>
                                                    <td><%=noncompliant_request[29] + " " + noncompliant_request[31]%></td>
                                                    <td><%=noncompliant_request[18]%></td>
                                                    <td title="<%=noncompliant_request[24]%>"><% 
                                                        if(noncompliant_request[24].length() > 20)
                                                        {
                                                            out.print(noncompliant_request[24].substring(0, 19) + "...");
                                                        }
                                                        else
                                                        {
                                                            out.print(noncompliant_request[24]);
                                                        }

                                                        %></td>
                                                    </tr>
                                                    <%
                                                }                                        
                                                %> 
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Request ID</th>
                                                    <th>Priority</th>
                                                    <th>Date of Request</th>
                                                    <th>Requested By</th>
                                                    <th>Requested For</th>
                                                    <th>Assigned to Group</th>
                                                    <th>Assigned to Analyst</th>
                                                    <th>State</th>
                                                    <th>Description</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <script>
        function reload_page()
        {
            var sla_id = document.getElementById("sla_id").value;
            var referer = document.getElementById("referer").value;
            var filter_date_range = document.getElementById("filter_date_range").value;
            var URL = "request_sla_drilldown.jsp?referer=" + referer + "&sla_id=" + sla_id + "&date_range=" + filter_date_range;
            window.location.href = URL;
        }
    </script>
    <script>
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>
    
    
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>


    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <!-- END PAGE LEVEL JS-->
    </body>
</html>

<% 
                con.close();
            }
            catch(Exception e)
            {

            }
        }//end if not permission
    }//end if not logged in
%>
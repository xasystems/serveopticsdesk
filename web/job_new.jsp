<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String JOB = session.getAttribute("job").toString();
        if (JOB.contains("create") || ADMINISTRATION.equalsIgnoreCase("true") || MANAGER.equalsIgnoreCase("true")) 
        {
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
            java.util.Date now = new java.util.Date();
            String now_display_format = display_format.format(now);
            String now_timestamp_format = timestamp_format.format(now);
            Connection con = null;
            try
            {
                con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
            }
            catch(Exception e)
            {
                System.out.println("job_new.jsp exception=" + e);
            }
            String job_id = request.getParameter("job_id");
            String job_info[] = db.get_jobs.job_by_id(con, job_id);
            
    %>
    <!-- Page specific CSS-->
    <!-- END Page Level CSS-->
    
    <form action="job_new" method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Job ID
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" name="job_id" id="job_id" readonly value="" class="form-control" placeholder="Assigned on save"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Job Name
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" name="name" id="name" value="" class="form-control" placeholder="Enter Job Name"/>
                </div>
            </div>
        </div>    
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Assigned To Group&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="hidden" name="assigned_group_id" id="assigned_group_id"/>
                    <input type="text" name="assigned_group_name" id="assigned_group_name" value="" class="form-control" placeholder="Enter the Assigned To Group"/>
                </div>
            </div>                                        
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Assigned To&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="hidden" name="assigned_to_id" id="assigned_to_id" value=""/>
                    <input type="text" name="assigned_to_username" id="assigned_to_username" value="" class="form-control" placeholder="Enter the Assigned To username"/>
                </div>
            </div>
        </div>    
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Priority
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <%
                    ArrayList<String[]> priority_select = db.get_system_select_field.active_for_table_column(con, "jobs", "priority");
                    %>
                    <select name="priority" id="priority" class="form-control">
                        <option value=""></option>
                        <%
                        for(int a = 0; a < priority_select.size(); a++)
                        {
                            String select_option[] = priority_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            %>
                            <option value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Status
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <%
                    ArrayList<String[]> status_select = db.get_system_select_field.active_for_table_column(con, "jobs", "status");
                    %>
                    <select name="status" id="status" class="form-control">
                        <option value=""></option>
                        <%                                                 
                        for(int a = 0; a < status_select.size(); a++)
                        {
                            String select_option[] = status_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            %>
                            <option value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>    
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Scheduled Start Date
                    </label>
                </div>
                <div class="formField clr md border-0 input-group d-flex">
                        <input type='text' id="scheduled_start_date" name="scheduled_start_date" value="" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Actual Start Date
                    </label>
                </div>
                <div class="formField clr md border-0 input-group d-flex">
                        <input type='text' id="actual_start_date" name="actual_start_date" value="" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>    
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Scheduled End Date
                    </label>
                </div>
                <div class="formField clr md border-0 input-group d-flex">
                        <input type='text' id="scheduled_end_date" name="scheduled_end_date" value="" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Actual End Date
                    </label>
                </div>
                <div class="formField clr md border-0 input-group d-flex">
                        <input type='text' id="actual_end_date" name="actual_end_date" value="" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>    
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Estimated Duration <em>(hours)</em>
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" name="estimated_duration" id="estimated_duration" value="" class="form-control" placeholder="Estimated Duration in Hours"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Actual Duration <em>(hours)</em>
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" name="actual_duration" id="actual_duration" value="" class="form-control" placeholder="Actual Duration in Hours"/>
                </div>
            </div>
        </div> 
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Notes
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <textarea class="form-control" name="notes" id="notes" rows="5"></textarea>  
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Description
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <textarea class="form-control" name="description" id="description" rows="5"></textarea>  
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Attachments
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="file" name="file" multiple>
                        <label class="custom-file-label" for="file">Choose file(s)</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-12 text-center pt-30 pb-30">
                <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                <button type="button" class="btn btn-primary-new customBtn lg waves-effect rightSidePanelCloser" role="button" onclick="saveJob()">
                    Save
                </button>
            </div>
        </div>                                

    </form>
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>
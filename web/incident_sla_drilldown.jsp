<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : incident_total_incidents
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="db.get_incidents"%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>

<%@ page import="javax.json.Json"%>
<%@ page import="javax.json.JsonArray"%>
<%@ page import="javax.json.JsonObject"%>
<%@ page import="javax.json.JsonReader"%>
<%@ page import="javax.json.JsonStructure"%>
<%@ page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="java.io.StringReader"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        if (!session.getAttribute("incident").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            try
            {
                String home_page = session.getAttribute("home_page").toString();
                String context_dir = request.getServletContext().getRealPath("");
                LinkedHashMap props = support.config.get_config(context_dir);
                Connection con = db.db_util.get_contract_connection(context_dir, session); 
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
                SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
                SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19

                //SimpleDateFormat filter_format2 = new SimpleDateFormat("yyyy"); //01/12/2019 12:00 AM
                java.util.Date filter_start = new java.util.Date();
                java.util.Date filter_end = new java.util.Date();
                String start = "";
                String end = "";
                //System.out.println("format2=" + filter_format2.parse("2016"));
                //System.out.println("format" + filter_format.parse("01/12/2019 12:00 AM"));

                String compliance = StringUtils.defaultString(request.getParameter("compliance"));
                String sla_id = request.getParameter("sla_id");
                String date_range = "";
                String referer = "";
                try 
                {
                    date_range = request.getParameter("date_range");
                    if (date_range.equalsIgnoreCase("null") || date_range == null) 
                    {
                        date_range = support.filter_dates.past_30_days();
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to past 30 days
                    date_range = support.filter_dates.past_30_days();
                }
                try 
                {
                    referer = request.getParameter("referer");
                    if (referer.equalsIgnoreCase("null") || referer == null) 
                    {
                        referer = "incident_home_support.jsp?date_range=" + date_range;
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to past 30 days
                    referer = "incident_home_support.jsp?date_range=" + date_range;
                }
                try 
                {
                    //01/12/2019 12:00 AM - 01/12/2019 11:59 PM&group_id=all
                    //System.out.println("date_range=" + date_range);
                    String temp[] = date_range.split("-");
                    filter_start = filter_format.parse(temp[0]);
                    //filter_start = filter_format.parse("01/12/2019 12:00 AM");
                    //System.out.println("filter_start=" + temp[0] + " ====" + filter_start);

                    filter_end = filter_format.parse(temp[1]);
                    //System.out.println("filter_end=" + temp[1].trim() + " ====" + filter_end);

                    start = timestamp_format.format(filter_start);
                    end = timestamp_format.format(filter_end);

                    
                } catch (Exception e) {
                    //if not set then default to past 30 days
                    System.out.println("Exception on incident_sla_drilldown.jsp=" + e);
                }
                ArrayList<String[]> groups = db.get_groups.all(con);

                %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	
    <!-- BEGIN VENDOR CSS-->
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/app.css">-->
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <!--<link rel="stylesheet" type="text/css" href="assets/css/style.css">-->
    <style>
        .clickable-row {
            cursor: pointer;
        }
        .btn-primary-new {
            color: #ffffff !important;
        }
    </style>
    <!-- END Custom CSS-->
    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <input type="hidden" name="start" id="start" value="<%=start%>"/>
    <input type="hidden" name="end" id="end" value="<%=end%>"/>
    <input type="hidden" name="sla_id" id="sla_id" value="<%=sla_id%>"/>
    <input type="hidden" name="referer" id="referer" value="<%=referer%>"/>

    <%
    String compliance_color = "";
    String display_threshold = "";
    double compliance_value = 0;
    double noncompliance_value = 0;
    String sla_info[] = db.get_sla.incident_by_id(con, sla_id);
    String sla_name = sla_info[1];
    String sla_description = sla_info[2];
    //System.out.println("sla_id=" + sla_id);

    double this_period_score = support.sla_incident_calc.for_sla_id(con, sla_id, filter_start, filter_end);
    //System.out.println("this_period_score=" + this_period_score);
    String incident_priority = sla_info[5];
    String sla_timeframe = support.string_case_changer.to_camel_case(sla_info[19]);
    String sla_target = sla_info[6];
    String sla_type = support.string_case_changer.to_camel_case(sla_info[3]);
    String sla_threshold_operator = support.string_case_changer.sla_threshold_operator(sla_info[21]);
    String sla_threshold_value = sla_info[22];
    String sla_threshold_unit = support.string_case_changer.sla_threshold_units(sla_info[23]);
    %>

    <div class="clr whtbg p-10 mb-15 position-relative">
        <h1 class="large-font">Service Compliance > <span class="boldFont"><%=sla_name%></span></h1>
    </div>

    <div class="clr mb-30">
        <div class="sort-tbl-holder">
            <%
            try 
            {
                JsonReader reader = Json.createReader(new StringReader(sla_info[30]));
                JsonObject resultObject = reader.readObject();
                JsonArray parameters = (JsonArray) resultObject.getJsonArray("parameters");

                for(int b = 0; b < parameters.size(); b++)
                {
                    JsonObject this_record = parameters.getJsonObject(b); //a single jsonrecord
                    //System.out.println("json=" + this_record);
                    String field_name = this_record.getJsonString("field_name").toString().replace("\"","");
                    String operator = this_record.getJsonString("operator").toString().replace("\"","");
                    JsonArray values = (JsonArray) this_record.getJsonArray("value");

                    for(int c = 0; c < values.size(); c++)
                    {
                        String value = values.getString(c);
                        if(field_name.contains("group"))
                        {
                            for(int d = 0; d < groups.size();d++)
                            {
                                String this_group[] = groups.get(d);
                                if(value.equalsIgnoreCase(this_group[0]))
                                {
                                    value = this_group[1];
                                }
                            }
                        }
                        else
                        {
                            value = values.getString(c);
                        }
                        if(c == 0)
                        {
                            display_threshold = field_name + " " + operator + " '" + value + "'";
                        }
                        else
                        {
                            display_threshold = display_threshold + " OR " + field_name + " " + operator + " '" + value + "'";
                        }
                    }              
                }
            } 
            catch (Exception e) 
            {
                out.println("Exception in record=" + e);
            }
            String popover_text = sla_threshold_operator + "&nbsp;" + sla_threshold_value + "&nbsp;" + sla_threshold_unit;
            if(this_period_score >= Double.parseDouble(sla_target))
            {
                compliance_color = "#8bc34a";
                compliance_value = this_period_score;
                noncompliance_value = 100 - this_period_score;
            }
            else
            {
                compliance_color = "#ef5350";
                compliance_value = this_period_score;
                noncompliance_value = 100 - this_period_score;
            }

            %>
            <table class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                <thead>
                    <th>SLA Name&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>SLA Description&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>SLA Type&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>Target&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>Threshold&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>SLA Parameters&nbsp;&nbsp;&nbsp;&nbsp;</th>
                </thead>
                <tbody>
                    <tr>
                        <td><%=sla_name%>&nbsp;&nbsp;</td>
                        <td><%=sla_description%>&nbsp;&nbsp;</td>
                        <td><%=sla_type%>&nbsp;&nbsp;</td>
                        <td><%=sla_target%>%&nbsp;&nbsp;</td>
                        <td><%=popover_text%>&nbsp;&nbsp;</td>
                        <td><%=display_threshold%>&nbsp;&nbsp;</td>

                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="d-flex flex-row bd-highlight">
      <label>Date Range</label>
    </div>

    <form class="form-inline mb-30">
        <input class="form-control w-25" type="text" name="date_range" id="date_range" value="<%=date_range%>">  
        <button type="button" class="ml-15 form-control btn <%=(compliance.equals("compliant") ? "btn-primary-new" : "btn-wht")%> md customBtn dInlineBlock compliance" data-compliance="compliant">
            <strong>Compliant: <span id="compliant_counter">0</span></strong>
        </button>
        <button type="button" class="ml-15 form-control btn <%=(compliance.equals("non-compliant")||compliance.equals("") ? "btn-primary-new" : "btn-wht")%> md customBtn dInlineBlock compliance" data-compliance="non-compliant">
            <strong>Non-Compliant: <span ompliant" id="non_compliant_counter">0</span></strong>
        </button>
        <button type="button" class="ml-15 form-control btn <%=(compliance.equals("both") ? "btn-primary-new" : "btn-wht")%> md customBtn dInlineBlock compliance" data-compliance="both">
            <strong>Both: <span id="both_counter">0</span></strong>
        </button>
    </form>

    <div class="clr mb-30">
        <div class="top clr mb-15">
            <h4 class="boldFont large-font pt-15">Incidents</h4>
        </div>
        <div class="sort-tbl-holder">
            <table id="data_table" class="table table-striped custom-sort-table" id="table">
                <thead>
                    <tr>
                        <th style="width:3%">ID</th>
                        <th style="width:7%">Priority</th>
                        <th>Incident Time</th>
                        <th style="width:5%">State</th>
                        <th>State Time</th>
                        <th style="width:5%">Group</th>
                        <th style="width:10%">Analyst</th>
                        <th style="width:15%">Category</th>
                        <!--<th>Sub-Category</th>-->
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <script>
        var dt;
        var dt_data;
            
        function reload_page()
        {
            var sla_id = document.getElementById("sla_id").value;
            var referer = document.getElementById("referer").value;
            var filter_date_range = document.getElementById("date_range").value;
            var URL = "incident_sla_drilldown.jsp?referer=" + referer + "&sla_id=" + sla_id + "&date_range=" + filter_date_range;
            window.location.href = URL;
        }

    </script>
    <script>
        window.addEventListener("load", (event) => 
        {
            $('#date_range').daterangepicker({
                timePicker: true,
//                startDate: moment().startOf('hour'),
//                endDate: moment().startOf('hour').add(32, 'hour'),
                locale: {
                    format: 'MM/DD/YYYY h:mm A'
                }
            }).on('apply.daterangepicker', function(ev, picker) {
//                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
                setQueryStringParameter("date_range", picker.startDate.format('MM/DD/YYYY h:mm A') + ' - ' + picker.endDate.format('MM/DD/YYYY h:mm A'));
                dt.clear().draw();
                dt.ajax.reload();
            });
            
            if (getQueryStringParameter("compliance") == null)
            {
                setQueryStringParameter("compliance", "non-compliant");
            }

            $('button.compliance').on('click', function (e) 
            {
                var compliance = $(e.target).closest("button").data('compliance');
                if (getQueryStringParameter("compliance") != compliance)
                {
                    $("button.compliance").each(function() {
                        if ($(this).data('compliance') != compliance) {
                            $(this).removeClass('btn-primary-new');
                            $(this).addClass('btn-wht');
                        } else {
                            $(this).addClass('btn-primary-new');
                            $(this).removeClass('btn-wht');                            
                        }
                    })
                }
                console.log(compliance);
                setQueryStringParameter("compliance", compliance);
                dt.clear();
                if (compliance == "both")
                {
                    dt.rows.add(dt_data['compliant'].concat(dt_data['non-compliant']));
                } else {
                    dt.rows.add(dt_data[compliance]);
                }

                dt.draw();                
            });

            dt = $('#data_table').DataTable( {
                language: {
                    "info":           "Showing _START_ to _END_ of _TOTAL_ rows",
                    "infoEmpty":      "Showing 0 to 0 of 0 rows",
                    "infoFiltered":   "(filtered from _MAX_ total rows)",
                    "lengthMenu":     "Show _MENU_ rows",
                    "zeroRecords":    "No matching rows found",
                },
                autoWidth: false,
                "processing": true,
//                "serverSide": true,
                "ordering": true,
                'serverMethod': 'post',
                'searching': false,
                "lengthChange": false,
                dom: "<'row'<'col-sm-3'i><'col-sm-3'f><'col-sm-6'p>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                "ajax": {
                    "url": "ajax_lookup_sla_incidents",
                    'data': function(data){
                        // Read values
                        data['sla_id'] = '<%=sla_id%>';
                        data['date_range'] = getQueryStringParameter("date_range");
                    }
                },
                order: [[ 0, "desc" ]],
                "pageLength": 50,
                "columnDefs": [
    //                { targets: 0, defaultContent: '<span class="expand-td"><img src="assets/images/svg/collapse-minus-icon.svg" alt=""></span>', orderable: false },
                    { targets: 0, "data": "incident_id" },
                    { targets: 1, "data": null, name: "priority", orderable: false, 
                        render: function ( data, type, row ) 
                        {
                            var indicator_class = "";
                            switch (row.priority) {
                                case "Low":
                                    indicator_class = "low";
                                    break;
                                case "Medium":
                                    indicator_class = "medium";
                                    break;
                                case "High":
                                    indicator_class = "high";
                                    break;
                                case "Critical":
                                    indicator_class = "critical";
                                    break;
                                default:
                                    indicator_class = "unknown";
                                    break;
                            }
                            return '<span class="indicator ' + indicator_class + ' "></span> ' + (row.priority ? row.priority : 'Unknown');
                        }
                    },
                    { targets: 2, "data": "incident_time" },
                    { targets: 3, "data": null, name: "state", orderable: false,
                        render: function ( data, type, row ) 
                        {
                            return '<span class="btn sm state-' + row.state.toLowerCase() + ' customBtn">' + row.state + '</span>';
                        }
                    },
                    { targets: 4, "data": "state_date" },
                    { targets: 5, "data": "caller_group_name" },
                    { targets: 6, "data": null, name: "assigned_to_id", 
                        render: function ( data, type, row ) 
                        {
                            if (row.assigned_to_id)
                            {
                                return '<span class="user-img dInlineBlock mr-5 user-avatar"><img src="assets/images/users/avatar-2.jpg" alt=""></span> ' + row.assigned_to_first + " " + row.assigned_to_last;
                            } else {
                                return '';
                            }
                        }
                    },
                    { targets: 7, "data": null, name: "category", 
                        render: function ( data, type, row ) 
                        {
                            return row.category + '/' + row.subcategory;
                        }
                    },
//                    { targets: 8, 
//                        render: function (data, type, full, meta) 
//                        {
//                            return '' +
//                                '<a href="javascript:void(0)" data-target="user" data-user-id="' + full.id + '" class="mr-5 rightSidePanelOpener"><img src="assets/images/svg/edit-icon.svg" alt=""></a>' +
//                                '<a href="javascript:void(0)" onclick="showDeleteModal(' + full.id + ')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>';
//                        }
//                    }
                ],
                createdRow: function( row, data, dataIndex ) {
                    $( row )
                        .attr('data-id', data.incident_id)
                        .attr('data-href', 'incident_list.jsp?id=' + data.incident_id)
                        .addClass('clickable-row')
                },
                "stripeClasses":[]
            } );

            dt.on( 'draw', function () {
                $(".clickable-row").click(function() {
                    window.location = $(this).data("href");
                });

            } );

            dt.on('xhr.dt', function ( e, settings, json, xhr ) {
                var compliance = getQueryStringParameter("compliance");
                dt_data = json;
                if (compliance == "both")
                {
                    json.data = dt_data['compliant'].concat(dt_data['non-compliant']);
                } else {
                    json.data = dt_data[compliance];
                }
                $("#compliant_counter").html(dt_data['compliant'].length);
                $("#non_compliant_counter").html(dt_data['non-compliant'].length);
                $("#both_counter").html(dt_data['compliant'].length + dt_data['non-compliant'].length);
            // Note no return - manipulate the data directly in the JSON object.

            } )
        });

        function apply_filters()
        {
            $("#filter_form input,#filter_form select").each(function() {

                var field = $(this);
        //            console.log("filter_form_field", field);
                var field_name = field.attr('name');

                if (!field_name || field_name == "service_type" || field.attr('type') == "hidden" || (field.attr('type') == "radio" && !field.prop("checked")))
                {
                    return;
                }
                if (field.val() && field.val() != "Any")
                {
                    var label = "";
                    var filter_label = "";
                    if (field.attr("type") == "radio")
                    {
                        label = "Predefined";
                        filter_label = field.siblings("label").html();
                    } else {
                        label = field.closest("div").prev().html();
                        filter_label = field.val();
                    }
                    if ($("#active_filter_" + field_name).length > 0) {
                        var filter_block = $("#active_filter_" + field_name);
                        $(filter_block).children("span").html(filter_label);
                    } else {
                        $("#active_filters").append(
                            $('' +
                                '<div class="clr tag dInlineBlock formField md whtbg mr-5 mb-15" id="active_filter_' + field_name + '">' +
                                    '<h5 class="basic-font mediumFont">' + label + '</h5>' +
                                    '<span>' + filter_label + '</span>' +
                                    '<img class="icon" src="assets/images/svg/cross-icon.svg" alt="" onclick="resetFilterFieldNew(\'' + field_name + '\')">' +
                                '</div>'
                            )
                        );
                    }
                    setQueryStringParameter(field_name, field.val());
                } else {
                    resetFilterFieldNew(field_name);
                }                
            });
//            window.location.reload();
        }

        function resetFilterFieldNew(field_name)
        {
            var filter_block = $("#active_filter_" + field_name);
            if (filter_block.length > 0)
            {
                filter_block.remove();
                var filter_field = $("#filter_form input[name='" + field_name + "'],#filter_form select[name='" + field_name + "']");
                if (filter_field.attr("type") != "radio")
                {
                    filter_field.val("");
                    if (field_name.indexOf("_name") !== false)
                    {
                        var filter_id_fields = $("#filter_form input[name='" + field_name.replace("_name","_id") + "'],#filter_form select[name='" + field_name.replace("_name","_id") + "']");
                        if (filter_id_fields.length > 0)
                        {
                            filter_id_fields.val("");
                        }
                    }
                } else {
                    filter_field.prop('checked', false);
                }
                setQueryStringParameter(field_name, "");
            }
        }
    
    </script>
    
    
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>


    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <!--<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>-->
    <!-- END PAGE LEVEL JS-->
    </body>
</html>

<% 
                con.close();
            }
            catch(Exception e)
            {

            }
        }//end if not permission
    }//end if not logged in
%>
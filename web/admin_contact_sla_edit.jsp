<!--Copyright 2019 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_sla_add.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="javax.json.Json"%>
<%@page import="javax.json.JsonArray"%>
<%@page import="javax.json.JsonObject"%>
<%@page import="javax.json.JsonReader"%>
<%@page import="javax.json.JsonStructure"%>
<%@page import="java.io.StringReader"%>

<%
if (session.getAttribute("authenticated") == null) 
{
    String context_dir = request.getServletContext().getRealPath("");
    LinkedHashMap props = support.config.get_config(context_dir);
    String login_url = props.get("login_url").toString();
    response.sendRedirect(login_url);
} 
else 
{
    boolean authorized = support.role.authorized(session, "sla","update");
    if(!authorized)
    {
        response.sendRedirect("no_permission.jsp");
    }  
    else 
    {
        Logger logger = LogManager.getLogger();
        String home_page = session.getAttribute("home_page").toString();
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        try 
        {
            Connection con = db.db_util.get_contract_connection(context_dir, session); 
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            String id = request.getParameter("id");
            String sla_info[] = db.get_sla.contact_by_id(con, id);
%>  
<!-- BEGIN PAGE LEVEL CSS-->
<body onload="show_abandoned();">
    <!-- END PAGE LEVEL CSS-->
            <form class="form" action="admin_contact_sla_edit" method="post">
                <input type="hidden" name="id" id="id" value="<%=id%>"/>
                <!-- start content here-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only mb-15">
                                                        SLA Display Name
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0 ">
                                                    <input type="text" id="name" name="name" class="form-control" value="<%=sla_info[1]%>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only mb-15">
                                                        Short Description (Optional)
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0 ">
                                                    <input type="text" id="description" name="description" class="form-control" value="<%=sla_info[2]%>"  />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md mb-15">
                                                        SLA Target
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">
                                                    <input type="text" id="target" name="target" class="form-control" value="<%=sla_info[5]%>"  />                                    
                                                </div>
                                            </div> 
                                            <div class="col-md-6 pt-10">
                                                <div class="custom-control custom-checkbox dInlineBlock mr-15">
                                                    <%
                                                    //12
                                                    if (sla_info[16].equalsIgnoreCase("0")) {
                                                    %>
                                                    <input type="checkbox" class="custom-control-input" id="active" name="active">  
                                                    <%
                                                    } else {
                                                    %>
                                                    <input type="checkbox" class="custom-control-input" CHECKED id="active" name="active">  
                                                    <%
                                                        }
                                                    %>                                          
                                                    <label class="custom-control-label small-font" for="active">Active?</label>
                                                </div>
                                                <div class="custom-control custom-checkbox dInlineBlock mr-15">
                                                    <%
                                                        //12
                                                        if (sla_info[17].equalsIgnoreCase("0")) {
                                                    %>
                                                    <input type="checkbox" class="custom-control-input" id="reportable" name="reportable">  
                                                    <%
                                                    } else {
                                                    %>
                                                    <input type="checkbox" class="custom-control-input" CHECKED id="reportable" name="reportable">  
                                                    <%
                                                        }
                                                    %>                                            
                                                    <label class="custom-control-label small-font" for="reportable">Do you want this SLA included on the Incident Dashboard?</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="field-title">
                                                    <label class="formLabel md mb-15">
                                                        SLA Text Detail (Optional)
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">
                                                    <textarea class="form-control" name="sla_text" id="sla_text" rows="3"><%=sla_info[3]%></textarea>  
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md mb-15">
                                                        SLA Type
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">                                                
                                                    <select onchange="show_abandoned()" id="sla_type" name="sla_type" class="form-control">
                                                        <%
                                                            String selected = "";
                                                            String option_values[] = {"ABANDONED_CONTACT_RATE", "ABANDONED_CONTACT_COUNT", "AVERAGE_POST_PROCESS_TIME", "AVERAGE_SPEED_TO_ANSWER", "AVERAGE_HANDLE_TIME", "AVERAGE_PROCESSING_TIME"};
                                                            String option_names[] = {"Abandoned Contact Rate", "Abandoned Contact Count", "Average Post Process Time", "Average Speed to Answer", "Average Handle Time", "Average Processing Time"};
                                                            for (int a = 0; a < option_values.length; a++) {
                                                                if (sla_info[4].equalsIgnoreCase(option_values[a])) {
                                                                    selected = "SELECTED";
                                                                } else {
                                                                    selected = "";
                                                                }
                                                        %>
                                                        <option <%=selected%> value="<%=option_values[a]%>"><%=option_names[a]%></option>
                                                        <%
                                                            }
                                                        %>
                                                    </select>                                            
                                                </div>
                                            </div> 
                                            <div class="col-md-6" id="abandoned_threshold_div" style="display:none">
                                                <div class="field-title">
                                                    <label class="formLabel md mb-15">
                                                        What is minimum amount of time for an Abandoned call to be counted as Abandoned?
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">                                                
                                                    <input type="text" id="abandoned_threshold" name="abandoned_threshold" class="form-control" value="<%=sla_info[14]%>" />       
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md mb-15">
                                                        SLA Timeframe
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">                                                
                                                    <select id="sla_timeframe" name="sla_timeframe" class="form-control">
                                                        <%
                                                            String timeframes[] = {"DAILY", "WEEKLY", "MONTHLY", "QUARTERLY", "ANNUAL", "CUSTOM"};
                                                            String timeframes_names[] = {"Daily", "Weekly", "Monthly", "Quarterly", "Annual", "Custom"};
                                                            for (int a = 0; a < timeframes.length; a++) {
                                                                if (sla_info[13].equalsIgnoreCase(timeframes[a])) {
                                                        %>
                                                        <option SELECTED value="<%=timeframes[a]%>"><%=timeframes_names[a]%></option>
                                                        <%
                                                        } else {
                                                        %>
                                                        <option value="<%=timeframes[a]%>"><%=timeframes_names[a]%></option>
                                                        <%
                                                                }
                                                            }
                                                        %>
                                                    </select>
                                                </div>
                                            </div>
                                            <!--
                                            <div class="col-md-2">
                                                <label for="schedule_id">Schedule</label>
                                                <select id="schedule_id" name="schedule_id" class="form-control">
                                                    
                                                </select>
                                            </div>
                                            -->
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md mb-15">
                                                        Select all the Channel(s) that this SLA will apply
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">                                                
                                                    <select class="select2 form-control" multiple="multiple" id="channels" name="channels">
                                                        <%
                                                            String channel_parameters[] = new String[0];
                                                            //{"parameters": [{"channels": ["Network Support", "Desktop Support"]}]}
                                                            try 
                                                            {
                                                                JsonReader reader = Json.createReader(new StringReader(sla_info[24]));
                                                                JsonObject resultObject = reader.readObject();
                                                                JsonArray parameters = (JsonArray) resultObject.getJsonArray("parameters");
                                                                JsonObject channel_array_object = parameters.getJsonObject(0); //get the first object
                                                                JsonArray channels = (JsonArray) channel_array_object.getJsonArray("channels");
                                                                System.out.println("channels length=" + channels.size());
                                                                channel_parameters = new String[channels.size()];
                                                                for(int a = 0; a < channels.size(); a++) //for each channel
                                                                {
                                                                    JsonObject this_channel = channels.getJsonObject(a); //a single question
                                                                    System.out.println("channel=" + this_channel.getString("name"));
                                                                    channel_parameters[a] = this_channel.getString("name").replace("\"", "");
                                                                }
                                                            } 
                                                            catch (Exception e) 
                                                            {
                                                                out.println("Exception in record=" + e);
                                                            }

                                                            selected = "";
                                                            String all_channels[] = db.get_contact.all_channels(con);
                                                            for (int a = 0; a < all_channels.length; a++) 
                                                            {
                                                                selected = "";
                                                                for (int b = 0; b < channel_parameters.length; b++) 
                                                                {
                                                                    if (all_channels[a].equals(channel_parameters[b])) 
                                                                    {
                                                                        selected = "selected";
                                                                        b = channel_parameters.length;
                                                                    }
                                                                }
                                                                %>
                                                                <option <%=selected%> value="<%=all_channels[a]%>"><%=all_channels[a]%></option>
                                                                <%
                                                            }
                                                        %>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <h4 class="mediumFont large-font mb-20">Compliance Parameters</h4>
                                            </div>
                                        </div>

                                    <div class="row">
                                        <div class="col-md-6" id="operator_div" style="display:none">
                                            <div class="field-title">
                                                <label class="formLabel md mb-15">
                                                    Operator
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <select id="sla_success_threshold_operator" name="sla_success_threshold_operator" class="form-control">
                                                    <%
                                                    String sla_success_threshold_operator[] = {"greater_than", "greater_than_or_equal", "equal_to", "not_equal_to", "less_than_or_equal", "less_than"};
                                                    String sla_success_threshold_operator_names[] = {"Greater than", "Greater than or equal to", "Equal to", "Not equal to", "Less than or equal", "Less than"};
                                                    for (int a = 0; a < sla_success_threshold_operator.length; a++) 
                                                    {
                                                        if (sla_info[15].equalsIgnoreCase(sla_success_threshold_operator[a])) 
                                                        {
                                                            %>
                                                            <option SELECTED value="<%=sla_success_threshold_operator[a]%>"><%=sla_success_threshold_operator_names[a]%></option>
                                                            <%
                                                        } 
                                                        else 
                                                        {
                                                            %>
                                                            <option value="<%=sla_success_threshold_operator[a]%>"><%=sla_success_threshold_operator_names[a]%></option>
                                                            <%
                                                        }
                                                    }
                                                    %>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6" id="value_div">
                                            <div class="field-title">
                                                <label class="formLabel md mb-15">
                                                    Value
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0" id="value_div_content">
                                                <div id="value_div_content">
                                                    <input type="text" id="sla_success_threshold_value" name="sla_success_threshold_value" class="form-control" value="<%=sla_info[16]%>" />
                                                </div>                                             
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" id="unit_div" style="display:none">
                                            <div class="field-title">
                                                <label class="formLabel md mb-15">
                                                    Unit
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <select id="sla_success_threshold_unit" name="sla_success_threshold_unit" class="form-control">
                                                    <%
                                                    String sla_success_threshold_unit[] = {"seconds", "minutes", "hours", "days"};
                                                    String sla_success_threshold_unit_names[] = {"Seconds", "Minutes", "Hours", "Days"};
                                                    for (int a = 0; a < sla_success_threshold_unit.length; a++) 
                                                    {
                                                        if (sla_info[17].equalsIgnoreCase(sla_success_threshold_unit[a])) 
                                                        {
                                                            %>
                                                            <option SELECTED value="<%=sla_success_threshold_unit[a]%>"><%=sla_success_threshold_unit_names[a]%></option>
                                                            <%
                                                        } 
                                                        else 
                                                        {
                                                            %>
                                                            <option value="<%=sla_success_threshold_unit[a]%>"><%=sla_success_threshold_unit_names[a]%></option>
                                                            <%
                                                        }
                                                    }
                                                    %>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4" id="percent_div" style="display:none">
                                            <div class="field-title">
                                                <label class="formLabel md mb-15">
                                                    Value
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <input type="text" id="sla_success_threshold_value" name="sla_success_threshold_value" class="form-control" value="<%=sla_info[16]%>" />     
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-md-12 text-center pt-30 pb-30">
                                            <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                                            <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                                                Save
                                            </button>
                                        </div>
                                    </div>
                <input type="hidden" id="report_name" name="report_name" value="" />
                <input type="hidden" id="report_description" name="report_description" value="" />
                <input type="hidden" id="report_header" name="report_header" value="" />
                <!--
                <div class="row">
                    <div class="col-12">
                        <div class="card">    
                            <div class="card-header">
                                <h4 class="card-title">Report Parameters </h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="name">Report Name</label>                                        
                                                <input type="text" id="report_name" name="report_name" class="form-control" value="<%=sla_info[25]%>" />  
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="name">Report Description</label>                                        
                                                <input type="text" id="report_description" name="report_description" class="form-control" value="<%=sla_info[26]%>" />  
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="name">Report Header</label>                                        
                                                <input type="text" id="report_header" name="report_header" class="form-control" value="<%=sla_info[27]%>" />  
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-actions">
                                                <button type="button" onclick="javascript:location.href = 'admin_sla.jsp'" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </button>
                                                <button type="submit" class="btn btn-info">
                                                    &nbsp;&nbsp;<i class="ft-save"></i> Save&nbsp;&nbsp;
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>-->
            <!-- end content here-->
            </form>
    <!-- BEGIN PAGE LEVEL JS-->
    
    <!-- END PAGE LEVEL JS-->
    
<%                con.close();

            } catch (Exception e) 
            {
                System.out.println("Exception in admin_sla_edit.jsp: " + e);
                logger.debug("ERROR: Exception in admin_sla_edit.jsp:" + e);
            }
        } //end if not admin permission
    } //end if not redirected
%>
</html>
<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : incident_survey
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.text.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.io.StringReader"%>
<%@ page import="javax.json.Json"%>
<%@ page import="javax.json.JsonArray"%>
<%@ page import="javax.json.JsonObject"%>
<%@ page import="javax.json.JsonReader"%>

<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else 
    {
        if (!session.getAttribute("contact").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            if(request.getHeader("referer").contains("_home_"))
            {
                session.setAttribute("survey_dashboard_referer", request.getHeader("referer"));
                //System.out.println("request survey referer=" + request.getHeader("referer"));
            }
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session); 
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
            //SimpleDateFormat filter_format2 = new SimpleDateFormat("yyyy"); //01/12/2019 12:00 AM
            java.util.Date filter_start = new java.util.Date();
            java.util.Date filter_end = new java.util.Date();
            String start = "";
            String end = "";
            ArrayList<String[]> groups = db.get_groups.all(con);
            String customer_group_id = "";
            String support_group_id = "";
            String date_range = "";
            String selected = "";
            try 
            {
                date_range = request.getParameter("date_range");
                if (date_range.equalsIgnoreCase("null") || date_range == null) 
                {
                    date_range = support.filter_dates.past_30_days();
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                date_range = support.filter_dates.past_30_days();
            }
            try 
            {
                customer_group_id = request.getParameter("customer_group_id");
                if (customer_group_id.equalsIgnoreCase("null") || customer_group_id == null) 
                {
                    customer_group_id = "all";
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                customer_group_id = "all";
            }
            try 
            {
                support_group_id = request.getParameter("support_group_id");
                if (support_group_id.equalsIgnoreCase("null") || support_group_id == null) 
                {
                    support_group_id = "all";
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                support_group_id = "all";
            }
            try 
            {
                //01/12/2019 12:00 AM - 01/12/2019 11:59 PM&group_id=all
                //System.out.println("date_range=" + date_range);
                String temp[] = date_range.split("-");
                filter_start = filter_format.parse(temp[0]);
                //filter_start = filter_format.parse("01/12/2019 12:00 AM");
                
                filter_end = filter_format.parse(temp[1]);
                //System.out.println("filter_end=" + temp[1].trim() + " ====" + filter_end);

                start = timestamp.format(filter_start);
                end = timestamp.format(filter_end);
            }
            catch(Exception e)
            {
                System.out.println("Exception in contact_surveys.jsp:=" + e);
            }
            DecimalFormat two_decimals = new DecimalFormat("0.00");
            DecimalFormat no_decimals = new DecimalFormat("#");
            
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END VENDOR CSS-->

    <div class="clr whtbg p-10 mb-15 position-relative">
        <div class="float-right options">
            <ul>
                <li id="filters_dropdown" class="dInlineBlock dropdown ">
                    <div class="dInlineBlock clr menuHolder">
                        <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                        </a>
                        <div class="dropdown-menu dropdown-menu-right md p-10" x-placement="bottom-end" style="position: absolute; transform: translate3d(100px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                            <h5 class="medium-font boldFont pt-5 mb-10">All Filters</h5>
                            <form id="filter_form">
                                <ul>
                                    <li class="mb-10 clr">
                                        <label class="field-label full-width">Date Range</label>
                                        <div class="formField md  mr-0 full-width">
                                            <input class="no-border full-width datetime" type="text" name="filter_date_range" id="filter_date_range" value="<%=date_range%>">
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <label class="field-label full-width">Support Group</label>
                                        <div class="formField md  mr-0 full-width">
                                            <select class="no-border" name="support_group_id" id="support_group_id">
                                                <option value="all">All</option>
                                                <%
                                                for (int a = 0; a < groups.size(); a++) 
                                                {
                                                    String this_group[] = groups.get(a);
                                                    selected = "";
                                                    if (this_group[0].equalsIgnoreCase(support_group_id)) 
                                                    {
                                                        selected = "SELECTED";
                                                    }
                                                    %>
                                                    <option <%=selected%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                    <%
                                                }
                                                %>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <label class="field-label full-width">Customer Group</label>
                                        <div class="formField md  mr-0 full-width">
                                            <select class="no-border" name="customer_group_id" id="customer_group_id">
                                                <option value="all">All</option>
                                                <%
                                                for (int a = 0; a < groups.size(); a++) 
                                                {
                                                    String this_group[] = groups.get(a);
                                                    selected = "";
                                                    if (this_group[0].equalsIgnoreCase(customer_group_id)) 
                                                    {
                                                        selected = "SELECTED";
                                                    }
                                                    %>
                                                    <option <%=selected%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                    <%
                                                }
                                                %>
                                            </select>
                                        </div>
                                    </li>

                                </ul>
                                <div class="text-center pt-5 pb-5">
                                    <button type="button" class="btn md2 btn-primary-new customBtn mb-5" onclick="reload_page()">Apply Filters</button>
                                    <div class="clr"></div>
                                    <a href="#" class="">Reset Filters</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </li>

            </ul>
        </div>
        <h1 class="large-font boldFont">Contact Surveys</h1>
    </div>

    <div class="sort-tbl-holder">
        <div class="top clr mb-15">
            <h4 class="boldFont large-font pt-15">Customer Satisfaction (CSAT)</h4>
        </div>            
        <table id="data_table" class="table table-striped custom-sort-table zero-configuration" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Survey Name</th>
                    <th>Sent</th>
                    <th>Completed</th>
                    <th>Completion Rate</th>
                    <th>Avg. Score</th>
                    <!--<th>Avg. Monthly Score <em>(Past 12 Months)</em></th>-->
                </tr>
            </thead>
            <tbody>
                <%
                String all_csat_contact_surveys[][] = db.get_surveys.reportable_csat_by_trigger_object(con,"contact");

                for(int a = 0; a < all_csat_contact_surveys.length;a++) //each survey
                {
                    String survey_id = all_csat_contact_surveys[a][0];
                    //get survey results for this survey and filter_start and filter_stop and support_group_id and customer_group_id
                    String survey_results[][] = db.get_survey_results.for_survey_id_start_date_end_date_support_group_id_customer_group_id(con, survey_id, filter_start, filter_end, support_group_id, customer_group_id);
                    double survey_score[] = db.get_survey_results.csat_score_for_survey_results(survey_results);
                    %>
                    <tr onclick="location.href='csat_survey_detail.jsp?date_range=<%=date_range%>&support_group_id=<%=support_group_id%>&customer_group_id=<%=customer_group_id%>&survey_id=<%=survey_id%>';" style="cursor:pointer;">
                    <th><%=all_csat_contact_surveys[a][2]%></th>                                            
                    <td><%=no_decimals.format(survey_score[0])%></td>      
                    <td><%=no_decimals.format(survey_score[1])%></td>
                    <td><%=two_decimals.format(survey_score[2])%></td>
                    <td><%=two_decimals.format(survey_score[3])%></td>
                    <!--<td data-sparkline="3, 3.5, 4.2, 4.8, 4.0, 3.0, 2.8,3, 3.5, 4.2, 4.8, 4.0  ; column"/>-->
                    </tr>

                    <%
                }
                %>
            </tbody>
        </table>
    </div>

    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
    </body>
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
    <script src="assets/js/highcharts/highcharts.js"></script>
    <script src="assets/js/highcharts/data.js"></script>
    <script src="assets/js/highcharts/drilldown.js"></script>
    <script src="assets/js/highcharts/exporting.js"></script>
    <script src="app-assets/js/scripts/popover/popover.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script>
        function reload_page()
        {
            var support_group_id = document.getElementById("support_group_id").value;
            var customer_group_id = document.getElementById("customer_group_id").value;
            var filter_date_range = document.getElementById("filter_date_range").value;
            var URL = "contact_survey.jsp?date_range=" + filter_date_range + "&support_group_id=" + support_group_id + "&customer_group_id=" + customer_group_id;
            window.location.href = URL;
        }
    </script>
    
    <script>
        jQuery.noConflict();
        var example = 'sparkline', theme = 'default';
        (function($)
        { // encapsulate jQuery
					

            /**
             * Create a constructor for sparklines that takes some sensible defaults and merges in the individual
             * chart options. This function is also available from the jQuery plugin as $(element).highcharts('SparkLine').
             */
            Highcharts.SparkLine = function (a, b, c) 
            {
                var hasRenderToArg = typeof a === 'string' || a.nodeName, options = arguments[hasRenderToArg ? 1 : 0],defaultOptions = {
                    chart: {
                        renderTo: (options.chart && options.chart.renderTo) || this,
                        backgroundColor: null,
                        borderWidth: 0,
                        type: 'area',
                        margin: [2, 2, 2, 2],
                        width: 220,
                        height: 30,
                        style: {
                            overflow: 'visible'
                        },

                    // small optimalization, saves 1-2 ms each sparkline
                    skipClone: true
                    },
                    title: {
                        text: ''
                    },
                    exporting: {
                        enabled: false
                    },
                    credits: {
                        enabled: false
                    },
                    xAxis: {
                        labels: {
                            enabled: false
                        },
                        title: {
                            text: null
                        },
                        startOnTick: false,
                        endOnTick: false,
                        tickPositions: []
                    },
                    yAxis: {
                        endOnTick: false,
                        startOnTick: false,
                        labels: {
                            enabled: true
                        },
                        title: {
                            text: null
                        },
                        tickPositions: [0]
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        hideDelay: 0,
                        outside: true,
                        shared: true
                    },
                    plotOptions: {
                        column: {
                            dataLabels: {
                                enabled: true
                            }
                        },
                        series: {
                            animation: false,
                            lineWidth: 1,
                            shadow: false,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            marker: {
                                radius: 1,
                                states: {
                                    hover: {
                                        radius: 2
                                    }
                                }
                            },
                            fillOpacity: 0.25
                        },
                        column: {
                            negativeColor: '#910000',
                            borderColor: 'silver'
                        }
                    }
                };

                options = Highcharts.merge(defaultOptions, options);

                return hasRenderToArg ? new Highcharts.Chart(a, options, c) : new Highcharts.Chart(options, b);
            };

            var start = +new Date(), $tds = $('td[data-sparkline]'), fullLen = $tds.length, n = 0;

            // Creating 153 sparkline charts is quite fast in modern browsers, but IE8 and mobile
            // can take some seconds, so we split the input into chunks and apply them in timeouts
            // in order avoid locking up the browser process and allow interaction.
            function doChunk() 
            {
                var time = +new Date(), i, len = $tds.length, $td, stringdata, arr, data, chart;

                for (i = 0; i < len; i += 1) 
                {
                    $td = $($tds[i]);
                    stringdata = $td.data('sparkline');
                    arr = stringdata.split('; ');
                    data = $.map(arr[0].split(', '), parseFloat);
                    chart = {};
                    if (arr[1]) 
                    {
                        chart.type = arr[1];
                    }
                    $td.highcharts('SparkLine', 
                        {
                            series: [{
                                data: data,
                                pointStart: 1
                            }],
                            tooltip: {
                                headerFormat: '<span style="font-size: 10px">Monthly CSAT Score</span><br/>',
                                pointFormat: '<b>{point.y}</b>'
                            },
                            chart: chart
                        }
                    );

                    n += 1;

                    // If the process takes too much time, run a timeout to allow interaction with the browser
                    if (new Date() - time > 500) {
                        $tds.splice(0, i + 1);
                        setTimeout(doChunk, 0);
                        break;
                    }

                    // Print a feedback on the performance
                    if (n === fullLen) {
                        $('#result').html('Generated ' + fullLen + ' sparklines in ' + (new Date() - start) + ' ms');
                    }
                }
            }
            doChunk();
        }
        )(jQuery);
</script>
    
    
    
</html>
<%
        }//end if not permission
    }//end if not logged in
%>
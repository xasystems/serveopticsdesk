<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_knowledge_base_article_add.jsp
    Created on : Oct 4 2020, 6:13:59 PM
    Author     : rcampbell
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        boolean page_authorized = support.role.authorized(session, "incident","update"); 
        
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            boolean article_approver = false;
        
            String ADMINISTRATION = session.getAttribute("administration").toString();
            String MANAGER = session.getAttribute("manager").toString();
            if(ADMINISTRATION.equalsIgnoreCase("true") || MANAGER.equalsIgnoreCase("true"))
            {
                article_approver = true;
            }            
            
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / MM/dd/yyyy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
            java.util.Date now = new java.util.Date();
            String created_date = date_time_picker_format.format(now);
            
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
        
    %>

    <!-- Page specific CSS-->
    <!-- END Page Level CSS-->
    
    <form action="admin_knowledge_base_article_add" method="POST">
        <input type="hidden" name="created_by_id" id="created_by_id" value="<%=user_id%>"/>
        <input type="hidden" name="created_date" id="created_date" value="<%=created_date%>"/>

        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Name
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input name="article_name" id="article_name" type="text" value="" class="form-control" placeholder="Article Name"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Description
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input name="article_description" id="article_description" type="text" value="" class="form-control" placeholder="Article Description"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Knowledge Base
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="knowledge_base_id" id="knowledge_base_id" class="form-control" >
                        <%
                        ArrayList <String[]> all_kb = db.get_kb.all_kb(con);
                        for(int a = 0; a < all_kb.size(); a++)
                        {
                            String kb[] = all_kb.get(a);
                            %>
                            <option value="<%=kb[0]%>"><%=kb[1]%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Status
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="status" id="status" class="form-control">
                        <%
                        String[] approver_status_names = {"Draft","Awaiting Approval","Published","Superceded","Expired","Invalid","Deleted"};
                        String[] approver_status_values = {"Draft","Waiting_Approval","Published","Superceded","Expired","Invalid","Deleted"};
                        String[] not_approver_status_names = {"Draft","Awaiting Approval"};
                        String[] not_approver_status_values = {"Draft","Waiting_Approval"};
                        if(article_approver)
                        {
                            for(int a = 0; a < approver_status_values.length; a++)
                            {
                                %>
                                <option value="<%=approver_status_values[a]%>"><%=approver_status_names[a]%></option>
                                <%
                            }
                        }
                        else
                        {
                            for(int a = 0; a < not_approver_status_values.length; a++)
                            {
                                %>
                                <option value="<%=not_approver_status_values[a]%>"><%=not_approver_status_names[a]%></option>
                                <%
                            }
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">     
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Valid till
                    </label>
                </div>
                <div class="formField clr md px-0 border-0 input-group d-flex">
                    <input type='text' id="valid_till" name="valid_till" value="" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Keyword
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">               
                    <input type="text" name="keywords" id="keywords" class="form-control" placeholder="Example:  Keyword1,Keyword Two,keyword_3"/>
                </div>
                <div style="display: block; float: left; padding-left: 15px; position: relative; top: -10px;">
                    <em>
                        <small>Comma Separated words that will be used to search for this Article</small>
                    </em>
                </div>
            </div>
        </div>
        <div class="row">     
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Article Text
                    </label>
                </div>
                <div class="formField clr md px-0 border-0 no-border p-0">
                    <textarea name="article_text" id="article_text_new" cols="30" rows="155" class="ckeditor">
                    </textarea>
                </div>
            </div> 
        </div>
        <div class="row ">
            <div class="col-md-12 text-center pt-30 pb-30">
                <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                    Save
                </button>
            </div>
        </div>
    </form>

    <!-- BEGIN PAGE LEVEL JS-->    
    <!-- END PAGE LEVEL JS-->   
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_knowledge_base_add.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_knowledge_base_add.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
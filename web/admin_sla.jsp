<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.apache.commons.lang3.ArrayUtils"%>
<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();        
        String SLA = session.getAttribute("sla").toString();
        if (SLA.contains("create") || ADMINISTRATION.equalsIgnoreCase("true") || MANAGER.equalsIgnoreCase("true")) 
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
%>
<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>	

<jsp:include page='menu_service_desk.jsp'>
    <jsp:param name="active_menu" value=""/>
</jsp:include>
<!-- BEGIN PAGE LEVEL CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/selects/select2.min.css">
<!-- Page specific CSS-->
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <!--End Page specific CSS-->
    <!-- BEGIN VENDOR CSS-->
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/app.css">-->
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
<!--    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <!--<link rel="stylesheet" type="text/css" href="assets/css/style.css">-->

<!-- END PAGE LEVEL CSS-->

<div class="clr whtbg p-10 mb-15 position-relative">
    <div class="float-right options">
        <ul>
            <li class="dInlineBlock dropdown ">
                <div class="dInlineBlock clr menuHolder">
                    <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                    </a>
                    <div class="dropdown-menu dropdown-menu-right md p-10">
                        <h5 class="medium-font boldFont pt-5 mb-10">All Filters</h5>
                        <form>
                            <ul>
<!--                                <li class="mb-10 clr">
                                    <div class="">
                                        <label class="field-label full-width">Category</label>
                                        <div class="formField md full-width mr-0">
                                            <select class="no-border full-width" name="category" >
                                                <%=support.html_utils.generate_select_options(new String[]{"", "Incident", "Request", "Contact"}, request.getParameter("category"))%>
                                            </select>
                                        </div>
                                    </div>
                                </li>-->
                                <li class="mb-10 clr">
                                    <div class="">
                                        <label class="field-label full-width">Company</label>
                                        <div class="formField md full-width mr-0">
                                            <input class="no-border full-width autocomplete" type="text" name="company"  value="<%=(request.getParameter("company") != null?request.getParameter("company"):"")%>">
                                        </div>
                                    </div>
                                </li>
                                <li class="mb-10 clr">
                                    <div class="">
                                        <label class="field-label full-width">Active</label>
                                        <div class="formField md full-width mr-0">
                                            <select class="no-border full-width" name="active" >
                                                <%=support.html_utils.generate_select_options(new String[]{"", "True", "False"}, request.getParameter("active"))%>
                                            </select>
                                        </div>
                                    </div>
                                </li>
                                <li class="mb-10 clr">
                                    <div class="">
                                        <label class="field-label full-width">Priority</label>
                                        <div class="formField md full-width mr-0">
                                            <select class="no-border full-width" name="priority" >
                                                <%=support.html_utils.generate_select_options(new String[]{"", "Low", "Medium", "High", "Critical"}, request.getParameter("priority"))%>
                                            </select>
                                        </div>
                                    </div>
                                </li>
                                <li class="mb-10 clr">
                                    <div class="">
                                        <label class="field-label full-width">SLA Type</label>
                                        <div class="formField md full-width mr-0">
                                            <select class="no-border full-width" name="sla_type" >
                                                <%=support.html_utils.generate_select_options(new String[]{"", "TIME"}, request.getParameter("sla_type"))%>
                                            </select>
                                        </div>
                                    </div>
                                </li>
                                <li class="mb-10 clr">
                                    <div class="">
                                        <label class="field-label full-width">SLA Timeframe</label>
                                        <div class="formField md full-width mr-0">
                                            <select class="no-border full-width" name="sla_timeframe" >
                                                <%=support.html_utils.generate_select_options(new String[]{"", "DAILY", "WEEKLY", "MONTHLY", "QUARTERLY", "ANNUAL", "CUSTOM"}, request.getParameter("sla_timeframe"))%>
                                            </select>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                                <div class="text-center pt-5 pb-5">
                                    <button class="btn md2 btn-primary-new customBtn mb-5">Apply Filters</button>
                                    <div class="clr"></div>
                                    <a href="javascript:void(0)" onclick="resetFilters()" class="">Reset Filters</a>
                                </div>
                        </form>
                    </div>
                </div>
            </li>

        </ul>
    </div>
    <h1 class="large-font boldFont">SLA</h1>
    <div class="custom-tabs clr ">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#all-sla-tab" role="tab">All</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#incidents-sla-tab" role="tab">Incident SLA </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#request-sla-tab" role="tab">Request SLA</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#contact-sla-tab" role="tab">Contact SLA</a>
            </li>

        </ul><!-- Tab panes -->
    </div>
</div>

    <%
        String[] filter_fields = {
//            "category",
            "company",
            "active",
            "priority",
            "sla_type",
            "sla_timeframe",
        };
        List<Map<String, String>> filters = new ArrayList<>();
        for (int i = 0; i < filter_fields.length; i++)
        {
            String filter_field = filter_fields[i];
            String filter_value = request.getParameter(filter_field);
            if (filter_value != null && !filter_value.isEmpty()) 
            {
                Map<String, String> map = new HashMap<String, String>();
                map.put("key", filter_field);
                map.put("value", filter_value);
                filters.add(map);
            }
        }

    %>    

    <%
        request.setAttribute("filter_fields", filter_fields);
    %>
    <jsp:include page="active_filters_row.jsp" />

<div class="tab-content">
    <div class="tab-pane" id="all-sla-tab" role="tabpanel">
        <div class=" clr mb-15 text-right ">
        </div>
        <div class="sort-tbl-holder pb-100">
            <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">

                <tbody>
                    <tr class="bg-transparent">
                        <th colspan="6">Incident SLAs</th>
                    </tr>
                    <tr>
                        <th width="20%">SLA Name</th>
                        <th width="30%">SLA Description</th>
                        <th width="10%">Active</th>
                        <th width="15%">Include on Dashboards</th>
                        <th width="15%">SLA Type</th>
                        <th width="10%" class="text-center">Actions</th>
                    </tr>
                    <%                                                                    
                    String all_incident_sla[][] = db.get_sla.incident_by_filters(con, filters);
                    String all_request_sla[][] = db.get_sla.request_by_filters(con, filters);
                    String all_contact_sla[][] = db.get_sla.contact_by_filters(con, filters);

                    for (int a = 0; a < all_incident_sla.length; a++) 
                    {
                    %>
                    <tr>
                        <td>
                            <%=all_incident_sla[a][1]%>
                        </td>
                        <td><%=all_incident_sla[a][2]%></td>
                        <td>
                            <%
                                if (all_incident_sla[a][16].equalsIgnoreCase("1")) {
                            %>
                            True
                            <%
                            } else {
                            %>
                            False
                            <%
                                }
                            %>
                        </td>
                        <td>
                            <%
                                if (all_incident_sla[a][17].equalsIgnoreCase("1")) {
                            %>
                            True
                            <%
                            } else {
                            %>
                            False
                            <%
                                }
                            %>
                        </td>
                        <td>
                            <%
                                if (all_incident_sla[a][3].equalsIgnoreCase("TIME")) {
                            %>
                            Time
                            <%
                            } else {
                                if (all_incident_sla[a][3].equalsIgnoreCase("COUNT_CONCURRENT")) {
                            %>
                            Count <em>(Concurrent)</em>
                            <%
                            } else {
                                if (all_incident_sla[a][3].equalsIgnoreCase("COUNT_CUMULATIVE")) {
                            %>
                            Count <em>(Cumulative)</em>
                            <%
                            } else {
                                if (all_incident_sla[a][3].equalsIgnoreCase("RATIO")) {
                            %>
                            Ratio
                            <%
                                            }
                                        }
                                    }
                                }
                            %>
                        </td>
                        <!--<td>
                            <%
                                //String schedule_info[] = db.get_schedules.by_id(con, all_incident_sla[a][16]);
                                //out.print(schedule_info[1]);
                            %>
                        </td>-->
                        <td class="text-center">
                            <a href="javascript:void(0)" onclick="editSLA('<%=all_incident_sla[a][0]%>', 'incident');" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>
                            <a href="javascript:void(0)" onclick="showDeleteModal('<%=all_incident_sla[a][0]%>', 'incident')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>
                        </td>
                    </tr>
                    <%
                        }
                        if (all_incident_sla.length == 0) {
                    %>
                    <tr>
                        <td colspan="6">No Incident SLAs</td>
                    </tr>
                    <%
                        }
                    %>
                    <tr class="bg-transparent">
                        <th colspan="6">Request SLAs</th>
                    </tr>
                    <tr>
                        <th width="20%">SLA Name</th>
                        <th width="30%">SLA Description</th>
                        <th width="10%">Active</th>
                        <th width="15%">Include on Dashboards</th>
                        <th width="15%">SLA Type</th>
                        <th width="10%" class="text-center">Actions</th>
                    </tr>
                    <%
                        for (int a = 0; a < all_request_sla.length; a++) {
                    %>
                    <tr>
                        <td>
                            <%=all_request_sla[a][1]%>
                        </td>
                        <td><%=all_request_sla[a][2]%></td>
                        <td>
                            <%
                                if (all_request_sla[a][12].equalsIgnoreCase("1")) {
                            %>
                            True
                            <%
                            } else {
                            %>
                            False
                            <%
                                }
                            %>
                        </td>
                        <td>
                            <%
                                if (all_request_sla[a][13].equalsIgnoreCase("1")) {
                            %>
                            True
                            <%
                            } else {
                            %>
                            False
                            <%
                                }
                            %>
                        </td>
                        <td>
                            <%
                                if (all_request_sla[a][3].equalsIgnoreCase("TIME")) {
                            %>
                            Time
                            <%
                            } else {
                                if (all_request_sla[a][3].equalsIgnoreCase("COUNT_CONCURRENT")) {
                            %>
                            Count <em>(Concurrent)</em>
                            <%
                            } else {
                                if (all_request_sla[a][3].equalsIgnoreCase("COUNT_CUMULATIVE")) {
                            %>
                            Count <em>(Cumulative)</em>
                            <%
                            } else {
                                if (all_request_sla[a][3].equalsIgnoreCase("RATIO")) {
                            %>
                            Count <em>(Cumulative)</em>
                            <%
                                            }
                                        }
                                    }
                                }
                            %>
                        </td>
                        <!--<td>
                            <%
                                //String schedule_info[] = db.get_schedules.by_id(con, all_request_sla[a][16]);
                                //out.print(schedule_info[1]);
                            %>
                        </td>-->
                        <td class="text-center">
                            <a href="javascript:void(0)" onclick="editSLA('<%=all_request_sla[a][0]%>', 'request');" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>
                            <a href="javascript:void(0)" onclick="showDeleteModal('<%=all_request_sla[a][0]%>', 'request')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>
                        </td>

                    </tr>
                    <%
                        }
                        if (all_request_sla.length == 0) {
                    %>
                    <tr>
                        <td colspan="6">No Request SLAs</td>
                    </tr>
                    <%
                        }
                    %>
                    <tr class="bg-transparent">
                        <th colspan="6">Contact SLAs</th>
                    </tr>
                    <tr>
                        <th width="20%">SLA Name</th>
                        <th width="30%">SLA Description</th>
                        <th width="10%">Active</th>
                        <th width="15%">Include on Dashboards</th>
                        <th width="15%">SLA Type</th>
                        <th width="10%" class="text-center">Actions</th>
                    </tr>
                    <%
                        for (int a = 0; a < all_contact_sla.length; a++) {
                    %>
                    <tr>
                        <td>
                            <%=all_contact_sla[a][1]%></td>
                        <td><%=all_contact_sla[a][2]%></td>
                        <td>
                            <%
                            if (all_contact_sla[a][10].equalsIgnoreCase("1")) {
                            %>
                            True
                            <%
                            } else {
                            %>
                            False
                            <%
                                }
                            %>
                        </td>
                        <td>
                            <%
                                if (all_contact_sla[a][11].equalsIgnoreCase("1")) {
                            %>
                            True
                            <%
                            } else {
                            %>
                            False
                            <%
                                }
                            %>
                        </td>
                        <td>
                            <%
                                if (all_contact_sla[a][4].equalsIgnoreCase("ABANDONED_CONTACT_RATE")) {
                            %>
                            Abandoned Contact Rate
                            <%
                            } else {
                                if (all_contact_sla[a][4].equalsIgnoreCase("ABANDONED_CONTACT_COUNT")) {
                            %>
                            Abandoned Contact Count
                            <%
                            } else {
                                if (all_contact_sla[a][4].equalsIgnoreCase("AVERAGE_POST_PROCESS_TIME")) {
                            %>
                            Average Post Process Time
                            <%
                            } else {
                                if (all_contact_sla[a][4].equalsIgnoreCase("AVERAGE_SPEED_TO_ANSWER")) {
                            %>
                            Average Speed to Answer
                            <%
                            } else {
                                if (all_contact_sla[a][4].equalsIgnoreCase("AVERAGE_TOTAL_TIME")) {
                            %>
                            Average Total Time
                            <%
                            } else {
                                if (all_contact_sla[a][4].equalsIgnoreCase("AVERAGE_ABANDONED_TIME")) {
                            %>
                            Average Abandoned Time
                            <%
                            } else {
                                if (all_contact_sla[a][4].equalsIgnoreCase("AVERAGE_HANDLE_TIME")) {
                            %>
                            Average Handle Time
                            <%
                            } else {
                                if (all_contact_sla[a][4].equalsIgnoreCase("AVERAGE_PROCESSING_TIME")) {
                            %>
                            Average Processing Time
                            <%
                            } else {
                                if (all_contact_sla[a][4].equalsIgnoreCase("AVERAGE_TIME_IN_QUEUE")) {
                            %>
                            Average Time in Queue
                            <%
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            %>
                        </td>
                        <!--<td>
                            <%
                                String schedule_info[] = db.get_schedules.by_id(con, all_contact_sla[a][12]);
                                out.print(schedule_info[1]);
                            %>
                        </td>-->
                        <td class="text-center">
                            <a href="javascript:void(0)" onclick="editSLA('<%=all_contact_sla[a][0]%>', 'contact);" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>
                            <a href="javascript:void(0)" onclick="showDeleteModal('<%=all_contact_sla[a][0]%>', 'contact')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>
                        </td>

                    </tr>
                    <%
                        }
                        if (all_contact_sla.length == 0) {
                    %>
                    <tr>
                        <td colspan="6">No Contact SLAs</td>
                    </tr>
                    <%
                        }
                    %>

                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane active" id="incidents-sla-tab" role="tabpanel">
        <div class=" clr mb-15 text-right ">
            <span data-type="incident" class="btn btn-success customBtn lg waves-effect rightSidePanelOpener">
                <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>Add Incident SLA
            </span>
        </div>
        <div class="sort-tbl-holder pb-100">
            <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="20%">SLA Name</th>
                        <th width="30%">SLA Description</th>
                        <th width="10%">Active</th>
                        <th width="15%">Include on Dashboards</th>
                        <th width="15%">SLA Type</th>
                        <th width="10%" class="text-center">Actions</th>
                    </tr>
                </thead>

                <tbody>
                    <%                                                                    
                    for (int a = 0; a < all_incident_sla.length; a++) 
                    {
                    %>
                    <tr>
                        <td>
                            <%=all_incident_sla[a][1]%>
                        </td>
                        <td><%=all_incident_sla[a][2]%></td>
                        <td>
                            <%
                                if (all_incident_sla[a][16].equalsIgnoreCase("1")) {
                            %>
                            True
                            <%
                            } else {
                            %>
                            False
                            <%
                                }
                            %>
                        </td>
                        <td>
                            <%
                                if (all_incident_sla[a][17].equalsIgnoreCase("1")) {
                            %>
                            True
                            <%
                            } else {
                            %>
                            False
                            <%
                                }
                            %>
                        </td>
                        <td>
                            <%
                                if (all_incident_sla[a][3].equalsIgnoreCase("TIME")) {
                            %>
                            Time
                            <%
                            } else {
                                if (all_incident_sla[a][3].equalsIgnoreCase("COUNT_CONCURRENT")) {
                            %>
                            Count <em>(Concurrent)</em>
                            <%
                            } else {
                                if (all_incident_sla[a][3].equalsIgnoreCase("COUNT_CUMULATIVE")) {
                            %>
                            Count <em>(Cumulative)</em>
                            <%
                            } else {
                                if (all_incident_sla[a][3].equalsIgnoreCase("RATIO")) {
                            %>
                            Ratio
                            <%
                                            }
                                        }
                                    }
                                }
                            %>
                        </td>
                        <!--<td>
                            <%
                                //String schedule_info[] = db.get_schedules.by_id(con, all_incident_sla[a][16]);
                                //out.print(schedule_info[1]);
                            %>
                        </td>-->
                        <td class="text-center">
                            <a href="javascript:void(0)" onclick="editSLA('<%=all_incident_sla[a][0]%>', 'incident');" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>
                            <a href="javascript:void(0)" onclick="showDeleteModal('<%=all_incident_sla[a][0]%>', 'incident')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>
                        </td>
                    </tr>
                    <%
                        }
                        if (all_incident_sla.length == 0) {
                    %>
                    <tr>
                        <td colspan="6">No Incident SLAs</td>
                    </tr>
                    <%
                        }
                    %>

                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane" id="request-sla-tab" role="tabpanel">
        <div class=" clr mb-15 text-right ">
            <span data-type="request" class="btn btn-success customBtn lg waves-effect rightSidePanelOpener">
                <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>Add Request SLA
            </span>
        </div>
        <div class="sort-tbl-holder pb-100">
            <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="20%">SLA Name</th>
                        <th width="30%">SLA Description</th>
                        <th width="10%">Active</th>
                        <th width="15%">Include on Dashboards</th>
                        <th width="15%">SLA Type</th>
                        <th width="10%" class="text-center">Actions</th>
                    </tr>
                </thead>

                <tbody>
                    <%
                        for (int a = 0; a < all_request_sla.length; a++) {
                    %>
                    <tr>
                        <td>
                            <%=all_request_sla[a][1]%>
                        </td>
                        <td><%=all_request_sla[a][2]%></td>
                        <td>
                            <%
                                if (all_request_sla[a][12].equalsIgnoreCase("1")) {
                            %>
                            True
                            <%
                            } else {
                            %>
                            False
                            <%
                                }
                            %>
                        </td>
                        <td>
                            <%
                                if (all_request_sla[a][13].equalsIgnoreCase("1")) {
                            %>
                            True
                            <%
                            } else {
                            %>
                            False
                            <%
                                }
                            %>
                        </td>
                        <td>
                            <%
                                if (all_request_sla[a][3].equalsIgnoreCase("TIME")) {
                            %>
                            Time
                            <%
                            } else {
                                if (all_request_sla[a][3].equalsIgnoreCase("COUNT_CONCURRENT")) {
                            %>
                            Count <em>(Concurrent)</em>
                            <%
                            } else {
                                if (all_request_sla[a][3].equalsIgnoreCase("COUNT_CUMULATIVE")) {
                            %>
                            Count <em>(Cumulative)</em>
                            <%
                            } else {
                                if (all_request_sla[a][3].equalsIgnoreCase("RATIO")) {
                            %>
                            Count <em>(Cumulative)</em>
                            <%
                                            }
                                        }
                                    }
                                }
                            %>
                        </td>
                        <!--<td>
                            <%
                                //String schedule_info[] = db.get_schedules.by_id(con, all_request_sla[a][16]);
                                //out.print(schedule_info[1]);
                            %>
                        </td>-->
                        <td class="text-center">
                            <a href="javascript:void(0)" onclick="editSLA('<%=all_request_sla[a][0]%>', 'request');" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>
                            <a href="javascript:void(0)" onclick="showDeleteModal('<%=all_request_sla[a][0]%>', 'request')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>
                        </td>

                    </tr>
                    <%
                        }
                        if (all_request_sla.length == 0) {
                    %>
                    <tr>
                        <td colspan="6">No Request SLAs</td>
                    </tr>
                    <%
                        }
                    %>

                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane" id="contact-sla-tab" role="tabpanel">
        <div class=" clr mb-15 text-right ">
            <span data-type="contact" class="btn btn-success customBtn lg waves-effect rightSidePanelOpener">
                <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>Add Contact SLA
            </span>
        </div>
        <div class="sort-tbl-holder pb-100">
            <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="20%">SLA Name</th>
                        <th width="30%">SLA Description</th>
                        <th width="10%">Active</th>
                        <th width="15%">Include on Dashboards</th>
                        <th width="15%">SLA Type</th>
                        <th width="10%" class="text-center">Actions</th>
                    </tr>
                </thead>

                <tbody>
                    <%
                        for (int a = 0; a < all_contact_sla.length; a++) {
                    %>
                    <tr>
                        <td>
                            <%=all_contact_sla[a][1]%></td>
                        <td><%=all_contact_sla[a][2]%></td>
                        <td>
                            <%
                            if (all_contact_sla[a][10].equalsIgnoreCase("1")) {
                            %>
                            True
                            <%
                            } else {
                            %>
                            False
                            <%
                                }
                            %>
                        </td>
                        <td>
                            <%
                                if (all_contact_sla[a][11].equalsIgnoreCase("1")) {
                            %>
                            True
                            <%
                            } else {
                            %>
                            False
                            <%
                                }
                            %>
                        </td>
                        <td>
                            <%
                                if (all_contact_sla[a][4].equalsIgnoreCase("ABANDONED_CONTACT_RATE")) {
                            %>
                            Abandoned Contact Rate
                            <%
                            } else {
                                if (all_contact_sla[a][4].equalsIgnoreCase("ABANDONED_CONTACT_COUNT")) {
                            %>
                            Abandoned Contact Count
                            <%
                            } else {
                                if (all_contact_sla[a][4].equalsIgnoreCase("AVERAGE_POST_PROCESS_TIME")) {
                            %>
                            Average Post Process Time
                            <%
                            } else {
                                if (all_contact_sla[a][4].equalsIgnoreCase("AVERAGE_SPEED_TO_ANSWER")) {
                            %>
                            Average Speed to Answer
                            <%
                            } else {
                                if (all_contact_sla[a][4].equalsIgnoreCase("AVERAGE_TOTAL_TIME")) {
                            %>
                            Average Total Time
                            <%
                            } else {
                                if (all_contact_sla[a][4].equalsIgnoreCase("AVERAGE_ABANDONED_TIME")) {
                            %>
                            Average Abandoned Time
                            <%
                            } else {
                                if (all_contact_sla[a][4].equalsIgnoreCase("AVERAGE_HANDLE_TIME")) {
                            %>
                            Average Handle Time
                            <%
                            } else {
                                if (all_contact_sla[a][4].equalsIgnoreCase("AVERAGE_PROCESSING_TIME")) {
                            %>
                            Average Processing Time
                            <%
                            } else {
                                if (all_contact_sla[a][4].equalsIgnoreCase("AVERAGE_TIME_IN_QUEUE")) {
                            %>
                            Average Time in Queue
                            <%
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            %>
                        </td>
                        <!--<td>
                            <%
                                String schedule_info[] = db.get_schedules.by_id(con, all_contact_sla[a][12]);
                                out.print(schedule_info[1]);
                            %>
                        </td>-->
                        <td class="text-center">
                            <a href="javascript:void(0)" onclick="editSLA('<%=all_contact_sla[a][0]%>', 'contact');" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>
                            <a href="javascript:void(0)" onclick="showDeleteModal('<%=all_contact_sla[a][0]%>', 'contact')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>
                        </td>

                    </tr>
                    <%
                        }
                        if (all_contact_sla.length == 0) {
                    %>
                    <tr>
                        <td colspan="6">No Contact SLAs</td>
                    </tr>
                    <%
                        }
                    %>

                </tbody>
            </table>
        </div>
    </div>
</div>


    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	

    <div class="rightPanelHolder">
        <div class="p-15 ticket-content">
            <span class="rightSidePanelCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
            <h4 id="rightPanelTitle" class="mediumFont large-font mb-20"></h4>
            <div id="rightPanelContent" class="clr whtbg p-15">
            </div>
        </div>

    </div>
    
    <jsp:include page="delete_confirmation_modal.jsp" />

    <!-- BEGIN PAGE LEVEL JS-->
    <!-- BEGIN VENDOR JS-->    
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="app-assets/js/core/app-menu.js"></script>
    <script src="app-assets/js/core/app.js"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/popover/popover.js"></script>
    <script src="app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="app-assets/js/scripts/forms/select/form-select2.js"></script>

    <script type="text/javascript">
        var sla_id = "";
        var sla_type = "";
        
        function editSLA(id, type)
        {
            sla_id = id;
            sla_type = type;
            $('.rightSidePanelOpener').click();
        }

    
        window.addEventListener("load", (event) => 
        {
            $('.rightSidePanelCloser').on('click', function(){
                $('.rightPanelHolder').removeClass('open');
                $('.rightSidePanel').removeClass('open');
                $('html').removeClass('panelOpen');
                sla_id = "";
                sla_type = "";
            });

            $("input[name='company']" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_incident_company",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $(this).val(ui.item.label); // display the selected text
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            }); 

            $('.dropdown').on('hide.bs.dropdown', function (e) 
            { // prevent dropdown close on autocomplete
                if (e.clickEvent && e.clickEvent.target && e.clickEvent.target.className == "ui-menu-item-wrapper") {
                    e.preventDefault();
                }
            });

            $('.rightSidePanelOpener').on('click', function(e){
                $("#rightPanelContent").html("");

                var ajax = {};
                ajax.type = 'GET';
                ajax.dataType = "html";

                if(e.originalEvent && e.originalEvent.isTrusted)
                {
                    // real click.
                    var url_addon = "_add.jsp";
                    console.log(e, e.target);
                    sla_type = $(e.target).data('type');
                    
                } else {
                    // programmatic click        
                    ajax.data = {id: sla_id};
                    var url_addon = "_edit.jsp";
                }

                switch (sla_type) 
                {
                    case "incident":
                        ajax.url = "admin_incident_sla";
                        break;
                    case "request":
                        ajax.url = "admin_request_sla";
                        break;
                    case "contact":
                        ajax.url = "admin_contact_sla";
                        break;
                    default:
                        ajax.url = "";
                        break;
                }
                if (ajax.url == "") return false;

                ajax.url += url_addon;

                ajax.error = function(jqXHR, textStatus)
                {
                    switch (jqXHR.status) {
                        case 401:
                            window.location.assign("<%=props.get("login_url").toString()%>");
                            break;
                        case 403:
                            alert("No permission!");
                            break;
                        default:
                            alert("Error processing request!");
                    }
                };
                
                ajax.success = function (data) 
                {
                    $("#rightPanelTitle").html(sla_type.charAt(0).toUpperCase() + sla_type.slice(1) + " SLA Parameters");
                    $("#rightPanelContent").html(data);
                    $('.rightSidePanelCloser').on('click', function(){
                        $('.rightPanelHolder').removeClass('open');
                        $('.rightSidePanel').removeClass('open');
                        $('html').removeClass('panelOpen');
                        sla_id = "";
                        sla_type = "";
                    });

                    //company
                    $( "#company" ).autocomplete(
                    {
                        source: function( request, response ) 
                        {
                            // Fetch data
                            $.ajax({
                                url: "ajax_lookup_incident_company",
                                type: 'post',
                                dataType: "json",
                                minLength: 2,
                                data: 
                                {
                                    search: request.term
                                },
                                success: function( data ) 
                                {
                                    response( data );
                                }
                            });
                        },
                        select: function (event, ui) 
                        {
                            // Set selection
                            $('#company').val(ui.item.label); // display the selected text
                            //alert("setting is=" + ui.item.value);
                            return false;
                        }
                    }); 
                    //department
                    $( "#department" ).autocomplete(
                    {
                        source: function( request, response ) 
                        {
                            // Fetch data
                            $.ajax({
                                url: "ajax_lookup_incident_department",
                                type: 'post',
                                dataType: "json",
                                minLength: 2,
                                data: 
                                {
                                    search: request.term
                                },
                                success: function( data ) 
                                {
                                    response( data );
                                }
                            });
                        },
                        select: function (event, ui) 
                        {
                            // Set selection
                            $('#department').val(ui.item.label); // display the selected text
                            //alert("setting is=" + ui.item.value);
                            return false;
                        }
                    }); 
                    //site
                    $( "#site" ).autocomplete(
                    {
                        source: function( request, response ) 
                        {
                            // Fetch data
                            $.ajax({
                                url: "ajax_lookup_incident_site",
                                type: 'post',
                                dataType: "json",
                                minLength: 2,
                                data: 
                                {
                                    search: request.term
                                },
                                success: function( data ) 
                                {
                                    response( data );
                                }
                            });
                        },
                        select: function (event, ui) 
                        {
                            // Set selection
                            $('#site').val(ui.item.label); // display the selected text
                            //alert("setting is=" + ui.item.value);
                            return false;
                        }
                    }); 
                    //location
                    $( "#location" ).autocomplete(
                    {
                        source: function( request, response ) 
                        {
                            // Fetch data
                            $.ajax({
                                url: "ajax_lookup_incident_location",
                                type: 'post',
                                dataType: "json",
                                minLength: 2,
                                data: 
                                {
                                    search: request.term
                                },
                                success: function( data ) 
                                {
                                    response( data );
                                }
                            });
                        },
                        select: function (event, ui) 
                        {
                            // Set selection
                            $('#location').val(ui.item.label); // display the selected text
                            //alert("setting is=" + ui.item.value);
                            return false;
                        }
                    }); 

                };
                
                $.ajax(ajax);
            });
        });

    function checkform() 
    {
        var error = "";
        var target = document.getElementById("target").value;
        if(target === "")
        {
            error = "Target can not be blank";
        }
        else
        {
            if(isNaN(target))
            {
                error = "Target must be a number";
            }
        }
        if(error !== "")
        {
            var errorMsg = "<p>" + error + "</p>"
            document.getElementById("modal_text").innerHTML = errorMsg
            $("#error_modal").modal("show");
            return false;
        }
        else
        {
            return true;
        }
    }    
    function add_measure() {
        //clear field values
        document.getElementById("field_values").options.length = 0;
        //set measure to option 0
        document.getElementById("field_measure").value = "--";
        //hide the add button
        $(document.getElementById("add_button_div")).hide();
        //show the measure field
        $(document.getElementById("field_measure_div")).show();
    }
    function show_add_button() {
        $(document.getElementById("add_button_div")).show();
    }
    function show_select() {
        $(document.getElementById("parameter_selector")).show();
    }
    function hide_select() {
        $(document.getElementById("parameter_selector")).hide();
    }
    function add_parameter_choices() {
        //hide the add button
        $(document.getElementById("add_button_div")).hide();
        $(document.getElementById("field_choices_div")).show();
        $.ajax({
            url: "ajax_lookup_incident_sla_parameters",
            data: {"field_name": $("#field_name").val()},
            success: function (result, status, xhr)
            {
                let dropdown = document.getElementById('field_values');
                const data = JSON.parse(result);
                let option;                
                for (let i = 0; i < data.length; i++)
                {
                    option = document.createElement('option');
                    option.text = data[i].label;
                    option.value = data[i].value;
                    dropdown.add(option);
                }
            }
        });
    }
    function add_request_parameter_choices() {
        //hide the add button
        $(document.getElementById("add_button_div")).hide();
        var field_name = document.getElementById("field_name").value;
        if (field_name == "description")
        {
            $(document.getElementById("field_choices_div")).show();
            $(document.getElementById("contact_method_choices")).hide();
            $(document.getElementById("description_choices")).show();
        } else
        {
            $(document.getElementById("field_choices_div")).show();
            $(document.getElementById("description_choices")).hide();
            $(document.getElementById("contact_method_choices")).show();
        }

        $.ajax({
            url: "ajax_request_sla",
            data: {"field_name": $("#field_name").val()},
            success: function (result, status, xhr)
            {
                let dropdown = document.getElementById('field_values');
                const data = JSON.parse(result);
                let option;
                for (let i = 0; i < data.length; i++)
                {
                    option = document.createElement('option');
                    option.text = data[i].label;
                    option.value = data[i].value;
                    dropdown.add(option);
                }
            }
        });
    }

    function add_department_choices() {
        //clear the existing choices
        document.getElementById('department').options.length = 0;
        
        $.ajax({
            url: "ajax_incident_sla_department",
            data: {"company": $("#company").val()},
            success: function (result, status, xhr)
            {
                let dropdown = document.getElementById('department');
                const data = JSON.parse(result);
                let option;
                option = document.createElement('option');
                option.text = "All";
                option.value = "All";
                dropdown.add(option);
                
                
                for (let i = 0; i < data.length; i++)
                {
                    option = document.createElement('option');
                    option.text = data[i].label;
                    option.value = data[i].value;
                    dropdown.add(option);
                }
            }
        });
    }
    function add_site_choices() {
        //clear the existing choices
        document.getElementById('site').options.length = 0;
        
        $.ajax({
            url: "ajax_incident_sla_site",
            data: {"company": $("#company").val(),"department": $("#department").val()},
            success: function (result, status, xhr)
            {
                let dropdown = document.getElementById('site');
                const data = JSON.parse(result);
                let option;
                option = document.createElement('option');
                option.text = "All";
                option.value = "All";
                dropdown.add(option);
                for (let i = 0; i < data.length; i++)
                {
                    option = document.createElement('option');
                    option.text = data[i].label;
                    option.value = data[i].value;
                    dropdown.add(option);
                }
            }
        });
    }
    function add_location_choices() {
        //clear the existing choices
        document.getElementById('location').options.length = 0;
        
        $.ajax({
            url: "ajax_incident_sla_location",
            data: {"company": $("#company").val(),"department": $("#department").val(),"site": $("#site").val()},
            success: function (result, status, xhr)
            {
                let dropdown = document.getElementById('location');
                const data = JSON.parse(result);
                let option;
                option = document.createElement('option');
                option.text = "All";
                option.value = "All";
                dropdown.add(option);
                for (let i = 0; i < data.length; i++)
                {
                    option = document.createElement('option');
                    option.text = data[i].label;
                    option.value = data[i].value;
                    dropdown.add(option);
                }
            }
        });
    }
    
    
    function add_row() {
        $(document.getElementById("top_hr")).show();
        $(document.getElementById("additional_parameter")).show();
        $(document.getElementById("bottom_hr")).show();
        /*** We get the table object based on given id ***/
        var object_table = document.getElementById("parameter_table");

        var row_number = object_table.rows.length;

        /*** We insert the row by specifying the current rows length ***/
        var object_row = object_table.insertRow(object_table.rows.length);

        /*** We insert the first row cell ***/
        var checkbox_cell = object_row.insertCell(0);

        /*** We  insert a checkbox object ***/
        var objInputCheckBox = document.createElement("input");
        objInputCheckBox.type = "checkbox";
        checkbox_cell.appendChild(objInputCheckBox);

        /*** field ***/
        var field_cell = object_row.insertCell(1);

        /*** We  add some text inside the celll ***/
        var field_name = document.getElementById("field_name").value;
        field_cell.innerHTML = "<input type='hidden' name='field_name" + "~" + row_number + "' value='" + field_name + "'/>" + field_name;

        /*** Operator ***/
        var field_measure_cell = object_row.insertCell(2);
        var measure = document.getElementById("field_measure").value;
        field_measure_cell.innerHTML = "<input type='hidden' name='field_measure" + "~" + row_number + "' value='" + measure + "'/>" + measure;

        /*** values ***/
        var values_cell = object_row.insertCell(3);

        //get field_values
        var values = $('#field_values').val();
        var display_values = "";
        var transfer_values = "";
        for (index = 0; index < values.length; ++index) {
            display_values += values[index] + " , ";
            transfer_values += values[index] + "~";
        }
        values_cell.innerHTML = "<input type='hidden' name='field_values" + "~" + row_number + "' value='" + transfer_values + "'/>" + display_values;

        //hide the add button
        $(document.getElementById("add_button_div")).hide();
        $(document.getElementById("field_choices_div")).hide();
        //hide the measure field
        $(document.getElementById("field_measure_div")).hide();
    }

    function add_request_row() {
        $(document.getElementById("top_hr")).show();
        $(document.getElementById("additional_parameter")).show();
        $(document.getElementById("bottom_hr")).show();
        /*** We get the table object based on given id ***/
        var object_table = document.getElementById("parameter_table");

        var row_number = object_table.rows.length;

        /*** We insert the row by specifying the current rows length ***/
        var object_row = object_table.insertRow(object_table.rows.length);

        /*** We insert the first row cell ***/
        var checkbox_cell = object_row.insertCell(0);

        /*** We  insert a checkbox object ***/
        var objInputCheckBox = document.createElement("input");
        objInputCheckBox.type = "checkbox";
        checkbox_cell.appendChild(objInputCheckBox);

        /*** field ***/
        var field_cell = object_row.insertCell(1);

        /*** We  add some text inside the celll ***/
        var field_name = document.getElementById("field_name").value;
        field_cell.innerHTML = "<input type='hidden' name='field_name" + "~" + row_number + "' value='" + field_name + "'/>" + field_name;

        /*** Operator ***/
        var field_measure_cell = object_row.insertCell(2);
        var measure = document.getElementById("field_measure").value;
        field_measure_cell.innerHTML = "<input type='hidden' name='field_measure" + "~" + row_number + "' value='" + measure + "'/>" + measure;

        /*** values ***/
        var values_cell = object_row.insertCell(3);

        if (field_name == "contact_method" || field_name == "assigned_group_id")
        {
            //get field_values
            var values = $('#field_values').val();
            var display_values = "";
            var transfer_values = "";
            for (index = 0; index < values.length; ++index) {
                display_values += values[index] + " , ";
                transfer_values += values[index] + "~";
            }
            values_cell.innerHTML = "<input type='hidden' name='field_values" + "~" + row_number + "' value='" + transfer_values + "'/>" + display_values;
        } else
        {
            if (field_name == "description")
            {
                //get field_values
                var field_text_choices = document.getElementById("field_text_choices").value;
                values_cell.innerHTML = "<input type='hidden' name='field_values" + "~" + row_number + "' value='" + field_text_choices + "'/>" + field_text_choices;
            }
        }

        //hide the add button
        $(document.getElementById("add_button_div")).hide();
        $(document.getElementById("field_choices_div")).hide();
        //hide the measure field
        $(document.getElementById("field_measure_div")).hide();
    }

    function delete_row() 
    {
        /***We get the table object based on given id ***/
        var objTable = document.getElementById("parameter_table");

        /*** Get the current row length ***/
        var iRow = objTable.rows.length;

        /*** Initial row counter ***/
        var counter = 0;

        /*** Performing a loop inside the table ***/
        if (objTable.rows.length > 1) {
            for (var i = 0; i < objTable.rows.length; i++) {

                /*** Get checkbox object ***/
                var chk = objTable.rows[i].cells[0].childNodes[0];
                if (chk.checked) {
                    /*** if checked we del ***/
                    objTable.deleteRow(i);
                    iRow--;
                    i--;
                    counter = counter + 1;
                }
            }
        }
    }

    function show_abandoned()
    {
        var sla_type = document.getElementById("sla_type").value;
        
        if(sla_type.indexOf("ABANDONED_CONTACT_RATE") !== -1)
        {
            $(document.getElementById("operator_div")).show();
            $(document.getElementById("value_div")).show();
            $(document.getElementById("unit_div")).hide();
            $(document.getElementsByName('sla_success_threshold_value')[0].placeholder='Enter a percent');
            //show the abandoned threshold field
            $(document.getElementById("abandoned_threshold_div")).show();
            document.getElementById("target").value = "100";
            document.getElementById('target').readOnly = true;
        }
        else if(sla_type.indexOf("ABANDONED_CONTACT_COUNT") !== -1)
        {
            $(document.getElementById("operator_div")).show();
            $(document.getElementById("value_div")).show();
            $(document.getElementById("unit_div")).hide();
            $(document.getElementsByName('sla_success_threshold_value')[0].placeholder='Enter a number');
            //show the abandoned threshold field
            $(document.getElementById("abandoned_threshold_div")).show();
            document.getElementById('target').readOnly = false;
        }
        else if(sla_type.indexOf("AVERAGE_POST_PROCESS_TIME") !== -1)
        {
            $(document.getElementById("operator_div")).show();
            $(document.getElementById("value_div")).show();
            $(document.getElementById("unit_div")).show();
            $(document.getElementsByName('sla_success_threshold_value')[0].placeholder='Enter a number');
            //show the abandoned threshold field
            $(document.getElementById("abandoned_threshold_div")).hide();
            document.getElementById('target').readOnly = false;
        }
        else if(sla_type.indexOf("AVERAGE_SPEED_TO_ANSWER") !== -1)
        {
            $(document.getElementById("operator_div")).show();
            $(document.getElementById("value_div")).show();
            $(document.getElementById("unit_div")).show();
            $(document.getElementsByName('sla_success_threshold_value')[0].placeholder='Enter a number');
            //show the abandoned threshold field
            $(document.getElementById("abandoned_threshold_div")).hide();
            document.getElementById('target').readOnly = false;
        }
        else if(sla_type.indexOf("AVERAGE_HANDLE_TIME") !== -1)
        {
            $(document.getElementById("operator_div")).show();
            $(document.getElementById("value_div")).show();
            $(document.getElementById("unit_div")).show();
            $(document.getElementsByName('sla_success_threshold_value')[0].placeholder='Enter a number');
            //show the abandoned threshold field
            $(document.getElementById("abandoned_threshold_div")).hide();
            document.getElementById('target').readOnly = false;
        }
        else if(sla_type.indexOf("AVERAGE_PROCESSING_TIME") !== -1)
        {
            $(document.getElementById("operator_div")).show();
            $(document.getElementById("value_div")).show();
            $(document.getElementById("unit_div")).show();
            $(document.getElementsByName('sla_success_threshold_value')[0].placeholder='Enter a number');
            //show the abandoned threshold field
            $(document.getElementById("abandoned_threshold_div")).hide();
            document.getElementById('target').readOnly = false;
        }        
    }    

    </script>
    <!-- END PAGE LEVEL JS-->   
    <%
                con.close();
            } 
            catch (Exception e) 
            {
                System.out.println("Exception in admin_sla.jsp: " + e);
                logger.debug("ERROR: Exception in admin_sla.jsp:" + e);
            }
        } //end if not admin permission     
                
        
        else 
        {
            response.sendRedirect("no_permission.jsp");
        }
    }
%>
</body>
</html>

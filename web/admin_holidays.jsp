<!--Copyright 2019 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="org.apache.logging.log4j.LogManager"%>
<%@ page import="org.apache.logging.log4j.Logger"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.*" %>
<%@ page import="java.text.SimpleDateFormat"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        if (!session.getAttribute("administration").toString().equalsIgnoreCase("true") && !session.getAttribute("manager").toString().equalsIgnoreCase("true")) 
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session); 
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String all_holidays_info[][] = db.get_holidays.all(con);
                SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
                SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
        
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END PAGE LEVEL CSS-->
    
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title">Holidays</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a></li>
                                <li class="breadcrumb-item"><a href="admin_settings.jsp">Administration</a></li>
                                <li class="breadcrumb-item"><a href="#">Holidays</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div> <!--End breadcrumbs -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Holidays <button type="button" onclick="javascript:location.href='admin_holidays_add.jsp'" class="btn mr-1 mb-1 btn-info"><i class="la la-user-plus"></i> Add Holiday</button></h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <table class="table table-striped table-bordered zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Start</th>
                                            <th>End</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                        String start = "";
                                        String end = "";
                                        for(int a = 0; a < all_holidays_info.length;a++)
                                        {
                                            try
                                            {
                                                start = display_format.format(timestamp_format.parse(all_holidays_info[a][3]));
                                            }
                                            catch (Exception e)
                                            {
                                                start = "";
                                            }
                                            try
                                            {
                                                end = display_format.format(timestamp_format.parse(all_holidays_info[a][4]));
                                            }
                                            catch (Exception e)
                                            {
                                                end = "";
                                            }
                                            %>
                                            <tr>
                                                <td><i class="la la-edit info" onclick="javascript:location.href='admin_holidays_edit.jsp?id=<%=all_holidays_info[a][0]%>'" style="cursor: pointer"></i>&nbsp;<i class="la la-times danger" onclick="javascript:location.href='admin_holidays_delete.jsp?id=<%=all_holidays_info[a][0]%>'" style="cursor: pointer"></i>&nbsp;&nbsp;<%=all_holidays_info[a][1]%></td>
                                                <td><%=all_holidays_info[a][2]%></td>
                                                <td><%=start%></td>
                                                <td><%=end%></td>                                                
                                            </tr>
                                            <%
                                        }
                                        %>
                                    </tbody>
                                    <%
                                    if(all_holidays_info.length > 10)
                                    {
                                        %>
                                        <tfoot>
                                            <tr>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Start</th>
                                            <th>End</th>
                                        </tr>
                                        </tfoot>
                                        <%
                                    }
                                    %>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <!-- END PAGE LEVEL JS-->   
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_schedules.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_schedules.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.apache.commons.lang3.math.NumberUtils"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : menu
    Created on : Dec 13, 2018, 5:55:09 PM
    Author     : server-xc6701
--%>
            <!-- ========== Left Sidebar Start ========== -->
            <%
            String content_addon_class = "";
            String container_addon_class = "";
            String container_addon_style = "";
            int number_of_new_messages = 0;
            boolean new_message = false;
            String sso = session.getAttribute("self_service_only").toString();
            boolean self_service_only = false;
            if(sso.equalsIgnoreCase("true"))
            {
                self_service_only = true;
            }
            else
            {
                self_service_only = false;
            }    

            if (request.getRequestURI().contains("/home_service_catalog.jsp") || request.getRequestURI().contains("/service_catalog.jsp"))
            {
                content_addon_class = "whtbg";
                if (request.getRequestURI().contains("/service_catalog.jsp"))
                {
                    content_addon_class += " catalog-items-listing";
                }
                container_addon_style = "style=\"max-width: 1200px;\"";
                String context_dir = request.getServletContext().getRealPath("");
                LinkedHashMap sys_props = support.config.get_config(context_dir);
                Connection con = db.db_util.get_contract_connection(context_dir, session); 

            %>
            <div class="left sub-side-menu">

                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 949px;"><div class="sidebar-inner slimscrollleft" style="overflow: hidden; width: auto; height: 949px;">
                    <div id="" class="menu">
                        <div class="top p-15 ">
                            <h2 class="float-left mb-15">Categories</h2>
                            <div class="clr"></div>

                        </div>
                        <ul>
                        <%
                        ArrayList <String[]> catalogs = db.get_service_catalog.service_catalog_by_state(con, "Published");
                        int total_items_count = 0;
                        String catalog[];
                        for(int a = 0; a < catalogs.size();a++)
                        {
                            catalog = catalogs.get(a);
                            int catalog_items_count = NumberUtils.toInt(catalog[6], 0);
                            total_items_count += catalog_items_count;
                        }
                        %>
                            <li>
                                <a href="<%=session.getAttribute("home_page")%>" class="waves-effect">
                                    <span class="icon la la-home font-weight-bold""></span>
                                    <span class="txt">Home</span>
                                    <span class="count"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="waves-effect">
                                    <span class="icon la la-desktop font-weight-bold""></span>
                                    <span class="txt">View All                              </span>
                                    <span class="count"><%=total_items_count%></span>
                                </a>
                            </li>

                        <%
                        for(int a = 0; a < catalogs.size();a++)
                        {
                            catalog = catalogs.get(a);
                            int catalog_items_count = NumberUtils.toInt(catalog[6], 0);
                            ArrayList <String[]> categories = db.get_service_catalog.categories_by_catalog_id_and_state(con, catalog[0], "Published");                    
                            if (categories.size() > 0) {
                            %>
                                <li class="has-sub-menu">
                                    <a href="#" class="waves-effect">
                                        <span class="icon la <%=catalog[5]%> font-weight-bold"></span>
                                        <span class="txt"><%=catalog[1]%></span>
                                        <span class="count"><%=catalog_items_count%></span>
                                    </a>
                                    <ul>
                                        <% 
                                        for (int i = 0; i < categories.size(); i++) {
                                            String category[] = categories.get(i);
                                        %>
                                            <li>
                                                <a href="service_catalog.jsp?id=<%=catalog[0]%>&category=<%=category[0]%>" class="waves-effect">
                                                    <span class="txt"><%=category[0]%></span>
                                                    <span class="count"><%=category[1]%></span>
                                                </a>
                                            </li>

                                        <%
                                        }
                                        %>
                                    </ul>
                                </li>
                            <%
                            } else {
                                %>
                                    <li>
                                        <a href="#" class="waves-effect">
                                            <span class="icon la <%=catalog[5]%> font-weight-bold"></span>
                                            <span class="txt"><%=catalog[1]%></span>
                                            <span class="count"><%=catalog_items_count%></span>
                                        </a>
                                    </li>

                                <%

                            }
                        }
                        %>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div><div class="slimScrollBar" style="background: rgb(158, 165, 171); width: 10px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 949px;"></div><div class="slimScrollRail" style="width: 10px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div> <!-- end sidebarinner -->
            </div>
            <% } else { 
                if(!self_service_only)
                {  
                    try
                    {
                        String context_dir = request.getServletContext().getRealPath("");
                        Connection con = db.db_util.get_contract_connection(context_dir, session);
                        ///////////////////////////////////////////////////////////////////////////////////////////
                        //get emails
                        number_of_new_messages = db.get_message.message_count_for_status(con, "New");

                        if(number_of_new_messages > 0)
                        {
                            new_message = true;
                        }
                        con.close();
                    }
                    catch(Exception e)
                    {
                        System.out.println("Exception on menu_service_desk=" + e);
                    }
                }
                Boolean is_SLA = false;
                if (request.getRequestURI().contains("/incident_service_compliance.jsp") || 
                    request.getRequestURI().contains("/request_service_compliance.jsp") || 
                    request.getRequestURI().contains("/contact_service_compliance.jsp")) 
                {
                    is_SLA = true;
                }

            %>

            <div class="left side-menu">

                <div class="sidebar-inner slimscrollleft">
                    <div id="sidebar-menu">
                        <ul>
                            <li>
                                <a href="<%=session.getAttribute("home_page")%>" class="waves-effect">
                                    <span class="icon"><i class="la la-lg la-home"></i></span>
                                    <span class="txt">Home</span>
                                    
                                </a>
                            </li>
                            <li>
                                <a href="message_list.jsp?status=new" class="waves-effect">
                                    <span class="icon"><i class="la la-lg la-lg la-inbox"></i></span>
                                    <span class="txt">
                                        Messages 
                                        <%
                                        if(new_message)
                                        {
                                            %>
                                            <img src="app-assets/images/red_flag.png" alt="Red Flag" width="16" height="16">
                                            <%
                                        }
                                        %>
                                    </span>                                    
                                </a>
                            </li>
                            <li>
                                <a href="collaboration.jsp" class="waves-effect">
                                    <span class="icon"><i class="la la-lg la-people-carry"></i></span>
                                    <span class="txt">Collaboration</span>
                                    
                                </a>
                            </li>
                            <li class="">
                                <a href="incident_list.jsp" class="waves-effect ">
                                    <span class="icon"><i class="la la-lg la-tools"></i></span>
                                    <span class="txt">Incidents</span>
                                </a>
                            </li>
                            <li>
                                <a href="request_list.jsp" class="waves-effect">
                                    <span class="icon"><i class="la la-lg la-shopping-cart"></i></span>
                                    <span class="txt">Requests</span>
                                </a>
                            </li>
                            <li>
                                <a href="problem_list.jsp" class="waves-effect">
                                    <span class="icon"><i class="la la-lg la-stethoscope"></i></span>
                                    <span class="txt">Problems</span>
                                </a>
                            </li>
                            <li>
                                <a href="project_list.jsp" class="waves-effect">
                                    <span class="icon"><i class="la la-lg la-project-diagram"></i></span>
                                    <span class="txt">Projects</span>
                                </a>
                            </li>
                            <li>
                                <a href="job_list.jsp" class="waves-effect">
                                    <span class="icon"><i class="la la-lg la-list-ol"></i></span>
                                    <span class="txt">Other Tasks</span>
                                </a>
                            </li>
                            <li>
                                <a href="assets.jsp" class="waves-effect">
                                    <span class="icon"><i class="la la-lg la-laptop"></i></span>
                                    <span class="txt">Assets</span>
                                </a>
                            </li>
                            <li>
                                <a href="#sla_submenu" data-toggle="collapse" role="button" class="waves-effect <%=(is_SLA == true? "" : "collapsed")%>">
                                    <span class="icon"><i class="la la-lg la-chart-pie"></i></span>
                                    <span class="txt">SLA</span>
                                </a>
                            </li>
                            <div class="collapse <%=(is_SLA == true? "show" : "")%>" id="sla_submenu">
                            <li>
                                <a href="incident_service_compliance.jsp" class="waves-effect">
                                    <span class="icon"><i class="la la-lg la-blank"></i></span>
                                    <span class="txt">Incidents</span>
                                </a>
                            </li>
                            <li>
                                <a href="request_service_compliance.jsp" class="waves-effect">
                                    <span class="icon"><i class="la la-lg la-blank"></i></span>
                                    <span class="txt">Requests</span>
                                </a>
                            </li>
                            <li>
                                <a href="contact_service_compliance.jsp" class="waves-effect">
                                    <span class="icon"><i class="la la-lg la-blank"></i></span>
                                    <span class="txt">Contacts</span>
                                </a>
                            </li>
                            </div>
                            <li>
                                <a href="kb_search.jsp" class="waves-effect">
                                    <span class="icon"><i class="la la-lg la-book-reader"></i></span>
                                    <span class="txt">Knowledge</span>
                                </a>
                            </li>    
                            <li>
                                <a href="home_self_service.jsp" class="waves-effect">
                                    <span class="icon"><i class="la la-lg la-user-cog"></i></span>
                                    <span class="txt">Self-Service</span>
                                </a>
                            </li>    
                            <li>
                                <a href="home_service_catalog.jsp" class="waves-effect">
                                    <span class="icon"><i class="la la-lg la-book-open"></i></span>
                                    <span class="txt">Catalog</span>
                                </a>
                            </li>    
                            <li>
                                <a href="admin.jsp" class="waves-effect">
                                    <span class="icon"><i class="la la-lg la-cog"></i></span>
                                    <span class="txt">Admin</span>
                                </a>
                            </li>
                            <li>
                                <a href="help_page.jsp" class="waves-effect">
                                    <span class="icon"><i class="la la-lg la-list-ol"></i></span>
                                    <span class="txt">Help</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div> <!-- end sidebarinner -->
            </div>
            <%
            }
            %>
            <!-- Left Sidebar End -->

            <%

            if (request.getRequestURI().contains("collaboration"))
            {
            %>

            <div class="left sub-side-menu">

                <div class="sidebar-inner slimscrollleft">
                    <div id="" class="menu">
                        <div class="top p-15 ">
                            <div class="float-right mb-15">
                                <a class="btn btn-success customBtn md waves-effect" href="#" role="button" a="">
                                    <span class="btn-label -icon"><img src="assets/images/svg/plus-icon.svg" alt=""></span>
                                    New
                                </a>
                            </div>
                            <h2 class="float-left mb-15">Collaboration</h2>
                            <div class="clr"></div>
                            <a :class="'btn ' + (view == 'team' ? 'boston-blue-bg' : 'btn-wht') + ' customBtn md waves-effect float-left'" href="#" v-on:click="view = 'team'; getFolders()">
                                <span class="btn-label mr-5"><img src="assets/images/svg/team-icon.svg" alt=""></span>Team View
                            </a>
                            <a :class="'btn ' + (view == 'my' ? 'boston-blue-bg' : 'btn-wht') + ' customBtn md waves-effect float-right'" href="#" v-on:click="view = 'my'; getFolders()">
                                <span class="btn-label mr-5"><img src="assets/images/svg/team-view-icon.svg" alt=""></span>My View
                            </a>
                        </div>
                        <ul v-cloak>
                            <li v-for="folder in folders" v-bind:class="{'has-sub-menu': folder.hasOwnProperty('subfolders')}">
                                <a href="javascript:void(0)" class="waves-effect" v-on:click="slideToggle">
                                    <span class="icon"><img class="" :src="folder.icon" alt=""></span>
                                    <span class="txt">{{ folder.txt }}</span> 
                                    <!-- <span class="count">{{ folder.count }}</span> -->
                                </a>
                                <ul v-if="folder.subfolders">
                                    <li v-for="subfolder in folder.subfolders">
                                        <a href="#" :class="(folder.objectType == selectedFolder && selectedSubfolder == subfolder.txt ? ' active' : '')" v-on:click="selectedFolder = folder.objectType; selectedSubfolder = subfolder.txt">
                                            <span class="txt">{{ subfolder.txt }}</span>
                                            <span class="count">{{ subfolder.count }}</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div> <!-- end sidebarinner -->
            </div>
            <!-- Left Sub Sidebar End -->

            <div class="left ticket-listing">

                <div id="sidebar3" class="sidebar-inner">
                    <div id="" class="listing">
                        <div class="top">
                            <div class="float-right">
                                <div class="dInlineBlock clr menuHolder">
                                    <a id="filters_dropdown_toggle" class="btn btn-wht customBtn md waves-effect dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                        <span class="btn-label -icon"><img src="assets/images/svg/filter-icon.svg" alt=""></span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right md p-10" x-placement="bottom-end" style="position: absolute; transform: translate3d(-25px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                                    <form id="filter_form" onsubmit="return false">
                                        <!--<h5 class="medium-font boldFont pt-5 mb-10">All Filters</h5>-->
                                        <ul>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <li class="mb-10 clr">
                                                        <label class="field-label full-width">
                                                            <h5 class="medium-font boldFont pt-5 mb-10">Filters Category</h5>
                                                        </label>
                                                        <div class="formField md full-width mr-0">
                                                            <select id="filter_category_select" onchange="change_filter_category(this)" class="border-0 full-width">
                                                                <option value="filter_by_dates">Dates</option>
                                                                <option value="filter_by_users">Users/Groups</option>
                                                                <option value="filter_by_state">State/Priority/Category/Description</option>
                                                                <option value="filter_by_other">Other Attributes</option>
                                                            </select>
                                                        </div>
                                                    </li>
                                                </div>
                                            </div>
                                            <div id="filter_by_dates" class="filter-category-block">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Incident Time (Start)
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="text" id="incident_time_start" name="incident_time_start" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime">
                                                            </div>
                                                        </li>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Incident Date (End)
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="text" id="incident_time_stop" name="incident_time_stop" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime">
                                                            </div>
                                                        </li>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Create Date (Start)
                                                            </label>
                                                            <div class="formField md full-width mr-0">    
                                                                <input type="text" id="create_date_start" name="create_date_start" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime">
                                                            </div>
                                                        </li>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Create Date (End)
                                                            </label>
                                                            <div class="formField md full-width mr-0">                                                
                                                                <input type="text" id="create_date_stop" name="create_date_stop" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime">
                                                            </div>
                                                        </li> 
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Pending Date (Start)
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="text" id="pending_date_start" name="pending_date_start" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime"/>
                                                            </div>
                                                        </li>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Pending Date (End)
                                                            </label>
                                                            <div class='formField md full-width mr-0'>                                                
                                                                <input type="text" id="pending_date_stop" name="pending_date_stop" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime"/>
                                                            </div>
                                                        </li>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                State Date (Start)
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="text" id="state_date_start" name="state_date_start" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime"/>
                                                            </div>
                                                        </li>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                State Date (End)
                                                            </label>
                                                            <div class='formField md full-width mr-0'>
                                                                <input type="text" id="state_date_stop" name="state_date_stop" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime"/>
                                                            </div>
                                                        </li>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Closed Date (Start)
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="text" id="closed_date_start" name="closed_date_start" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime">
                                                            </div>
                                                        </li>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Closed Date (End)
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="text" id="closed_date_stop" name="closed_date_stop" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime">
                                                            </div>
                                                        </li>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="filter_by_users" class="d-none filter-category-block">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Caller Name (Last First)
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="hidden" id="caller_id" name="caller_id" value="">
                                                                <input type="text" id="caller_name" name="caller_name" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off" @change="filterByName">
                                                            </div>
                                                        </li>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Callers Group
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="hidden" id="caller_group_id" name="caller_group_id" value="">
                                                                <input type="text" id="caller_group_name" name="caller_group_name" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                            </div>
                                                        </li>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Assigned To (Last First)
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="hidden" id="assigned_to_id" name="assigned_to_id" value="">
                                                                <input type="text" id="assigned_to_name" name="assigned_to_name" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                            </div>
                                                        </li>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Assigned To Group
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="hidden" id="assigned_group_id" name="assigned_group_id" value="">
                                                                <input type="text" id="assigned_group_name" name="assigned_group_name" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                            </div>
                                                        </li>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Created By (Last First)
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="hidden" id="create_by_id" name="create_by_id" value="">
                                                                <input type="text" id="create_by_name" name="create_by_name" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                            </div>
                                                        </li>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="filter_by_state" class="d-none filter-category-block">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Priority
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="text" id="priority" name="priority" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                            </div>
                                                        </li>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Urgency
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="text" id="urgency" name="urgency" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                            </div>
                                                        </li>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Impact
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="text" id="impact" name="impact" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                            </div>
                                                        </li>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                State
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="text" id="state" name="state" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                            </div>
                                                        </li>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Category
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="text" id="category" name="category" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                            </div>
                                                        </li>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Subcategory
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="text" id="subcategory" name="subcategory" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                            </div>
                                                        </li>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Description
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="text" id="description" name="description" value="" placeholder="Any" class="border-0 full-width">
                                                            </div>
                                                        </li>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="filter_by_other" class="d-none filter-category-block">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                ID
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="text" id="id" name="id" value="" placeholder="Any" class="border-0 full-width">
                                                            </div>
                                                        </li>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Location
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="text" id="location" name="location" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                            </div>
                                                        </li>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Site
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="text" id="site" name="site" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                            </div>
                                                        </li>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Department
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="text" id="department" name="department" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                            </div>
                                                        </li>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <li class="mb-10 clr">
                                                            <label class="field-label full-width">
                                                                Company
                                                            </label>
                                                            <div class="formField md full-width mr-0">
                                                                <input type="text" id="company" name="company" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                            </div>
                                                        </li>
                                                    </div>
                                                </div>
                                            </div>
                                        </ul>      
                                        <div class="text-center pt-5 pb-5">
                                            <button id="submitFiltersButton" class="btn md2 btn-primary-new customBtn mb-5" v-on:click="applyFilters">Apply Filters</button>
                                            <div class="clr"></div>
                                            <a href="javascript:void(0)" @click="resetFilters" class="">Reset Filters</a>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <div class="float-left">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb" v-cloak>
                                        <li class="breadcrumb-item"><a href="#">{{ selectedFolder }}s</a></li>
                                        <li><input id="" type="text" dir="ltr" placeholder="Find anything..." class="form-control d-inline" @input="search" :value="searchPattern"></li>
                                    </ol>
                                </nav>
                                
                            </div>
                        </div>
                        <div id="sidebar3" class="custom-accordian slimscrollleft">
                            <div class="card" v-cloak v-if="!itemsLoading && Object.keys(items).length === 0">
                                <div class="text-center pt-5 pb-5">
                                    No items found
                                </div>
                            </div>
                            <div class="card" v-cloak v-for="(group, index) in items">
                                <div class="card-header" id="headingOne">
                                    <span class="dBlock txt font-weight-bold" data-toggle="collapse" :data-target="'#collapse' + group.id" aria-expanded="true" :aria-controls="'collapse' + group.id">
                                        {{ index }}
                                        <label class="count font-weight-bold">{{ group.content.length }}</label>
                                    </span>
                                </div>
                                <div :id="'collapse' + group.id" class="collapse show" :aria-labelledby="'heading' + group.id" data-parent="#accordion">
                                    <div class="card-body">
                                        <ul>
                                            <li :class="'ticket' + ( selectedItem && selectedItem.id == item.id ? ' unread' : '')" v-for="item in group.content" v-on:click="selectedItem = item">
                                                <a href="#">
                                                    <div class="top no-border mb-0">
                                                        <div class="time float-right">
                                                            {{ item.time }} <span :class="'indicator ' + item.priority.toLowerCase()"></span>
                                                        </div>
                                                        <h4>
                                                            <span class="icon mr-5" v-if="item.flag || item.vip == 'true'">
                                                                <img src="assets/images/svg/red-flag-icon.svg" alt="" v-if="item.flag">
                                                                <img src="assets/images/svg/crown-icon.svg" alt="" v-if="item.vip == 'true'">
                                                            </span>
                                                            {{ item.id }}
                                                            <span class="ml-5 count potrtage success" v-if="item.messages">16</span>
                                                        </h4>
                                                    </div>
                                                    <div class="content">
                                                        <h5 class="truncate">{{ item.description }}</h5>
                                                        <p class="truncate4">{{ item.notes }}</p>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                          </div>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- end sidebarinner -->
            </div>


            <% 
                content_addon_class = "whtbg";
                container_addon_class = "ticket-content";
            }
            %>

            <div class="content-page <%=(request.getRequestURI().contains("collaboration") || request.getRequestURI().contains("/home_service_catalog") || request.getRequestURI().contains("/service_catalog") ? "" : "full")%>">
                <!-- Start content -->
                <div class="content <%=content_addon_class%>">

                    <!-- ==================
                         PAGE CONTENT START
                         ================== -->

                    <div class="page-content-wrapper">
                        <div class="container-fluid <%=container_addon_class%>" <%=container_addon_style%>>

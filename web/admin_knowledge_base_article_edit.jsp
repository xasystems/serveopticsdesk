<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_knowledge_base_article_add.jsp
    Created on : Oct 4 2020, 6:13:59 PM
    Author     : rcampbell
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%> 
<%@page import="java.util.GregorianCalendar"%>
<%@page import="org.apache.commons.text.StringEscapeUtils"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        boolean page_authorized = support.role.authorized(session, "incident","update");         
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            
            boolean article_approver = false;
        
            String ADMINISTRATION = session.getAttribute("administration").toString();
            String MANAGER = session.getAttribute("manager").toString();
            if(ADMINISTRATION.equalsIgnoreCase("true") || MANAGER.equalsIgnoreCase("true"))
            {
                article_approver = true;
            }  
            
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / MM/dd/yyyy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
            java.util.Date now = new java.util.Date();
            String created_date = date_time_picker_format.format(now);
            
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String id = request.getParameter("id");
                String article_info[] = db.get_kb.article_info_for_id(con, id);
    %>
    <!-- Page specific CSS-->
    <!-- END Page Level CSS-->
    
    <form action="admin_knowledge_base_article_edit" method="POST" id="article_edit_form_<%=article_info[0]%>">
        <input type="hidden" name="id" id="id" value="<%=id%>"/>
        <input type="hidden" name="approver" id="approver" value="<%=String.valueOf(article_approver)%>"/>
        <div class="row">
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Knowledge Base
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="knowledge_base_id" id="knowledge_base_id" class="form-control" >
                        <%
                        ArrayList <String[]> all_kb = db.get_kb.all_kb(con);
                        String selected = "";
                        for(int a = 0; a < all_kb.size(); a++)
                        {
                            String kb[] = all_kb.get(a);
                            selected = "";
                            if(article_info[1].equalsIgnoreCase(kb[0]))
                            {
                                selected = "SELECTED";
                            }
                            %>
                            <option <%=selected%> value="<%=kb[0]%>"><%=kb[1]%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Name
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input name="article_name" id="article_name" type="text" value="<%=article_info[2]%>" class="form-control" placeholder="Article Name"/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Status
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <%
                    System.out.println("article_approver=" + article_approver);
                    String status_disabled = "";
                    if(article_approver)
                    {
                        status_disabled = "";
                    }
                    else
                    {
                        status_disabled = "disabled='true'";
                    }
                    %>
                    <select name="status" id="status" class="form-control">
                    <%
                    String[] approver_status_names = {"Draft","Awaiting Approval","Published","Superceded","Expired","Invalid","Deleted"};
                    String[] approver_status_values = {"Draft","Waiting_Approval","Published","Superceded","Expired","Invalid","Deleted"};
                    String[] not_approver_status_names = {"Draft","Awaiting Approval"};
                    String[] not_approver_status_values = {"Draft","Waiting_Approval"};

                    if(article_approver)
                    {
                        for(int a = 0; a < approver_status_values.length; a++)
                        {
                            selected = "";
                            if(article_info[4].equalsIgnoreCase(approver_status_values[a]))
                            {
                                selected = "SELECTED";
                            }
                            %>
                            <option <%=selected%> value="<%=approver_status_values[a]%>"><%=approver_status_names[a]%></option>
                            <%                                                                
                        }
                    }
                    else //not an approver
                    {
                        for(int a = 0; a < not_approver_status_values.length; a++)
                        {
                            selected = "";
                            if(article_info[4].equalsIgnoreCase(not_approver_status_values[a]))
                            {
                                selected = "SELECTED";
                            }
                            %>
                            <option <%=selected%> value="<%=not_approver_status_values[a]%>"><%=not_approver_status_names[a]%></option>
                            <%                                                                
                        }
                    }
                    %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Description
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input name="article_description" id="article_description" type="text" value="<%=article_info[3]%>" class="form-control" placeholder="Article Description"/>
                </div>
            </div>            
        </div>
        <div class="row">     
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Valid till
                    </label>
                </div>
                <div class="formField clr md px-0 border-0 input-group d-flex">
                    <%
                    String valid_till_value = "";
                    try
                    {
                        Calendar valid_till_calendar = new GregorianCalendar();
                        valid_till_calendar.setTime(timestamp_format.parse(article_info[11]));
                        valid_till_value = date_time_picker_format.format(valid_till_calendar.getTime());                                                    
                    }
                    catch(Exception e)
                    {
                        System.out.println("Exception on admin_knowledge_base_article_edit.jsp getting valid_till date:=" + e);
                    }
                    %>
                    <input type='text' id="valid_till" name="valid_till" value="<%=valid_till_value%>" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Keyword
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" name="keywords" id="keywords" class="form-control" value="<%=article_info[6]%>" placeholder="Example:  Keyword1,Keyword Two,keyword_3"/>
                </div>
                <div style="display: block; float: left; padding-left: 15px; position: relative; top: -10px;">
                    <em>
                        <small>Comma Separated words that will be used to search for this Article</small>
                    </em>
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Created By&nbsp;
                        <i class="la la-search" style="font-size: 14px;"></i>
                        <i class="la la-question-circle" style="font-size: 14px;" title="This field is a database lookup for all users."></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0 input-group d-flex">
                    <%
                    String created_by_id = article_info[7];
                    String created_by_id_info[] = db.get_users.by_id(con, created_by_id);
                    if(created_by_id.equalsIgnoreCase("") || created_by_id == null)
                    {
                        created_by_id = "";
                        created_by_id_info[1] = "";
                    }
                    %>
                    <input type="hidden" name="created_by_id" id="created_by_id" value="<%=created_by_id%>"/>
                    <%
                    if(article_approver)
                    {
                        %>
                        <input type="text" name="created_by_name" id="created_by_name" value="<%=created_by_id_info[1]%>" class="form-control" placeholder="Enter the creaters username"/>
                        <%
                    }
                    else
                    {
                        %>
                        <input type="text" readonly="true" name="created_by_name" id="created_by_name" value="<%=created_by_id_info[1]%>" class="form-control" placeholder="Enter the creaters username"/>
                        <%
                    }
                    %>
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <a href="javascript:clear_created_by(<%=article_info[0]%>)"><span class="la la-close"></span></a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">     
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Date Created
                    </label>
                </div>
                <div class="formField clr md px-0 border-0 input-group d-flex">
                    <%
                    String created_date_value = "";
                    try
                    {
                        Calendar created_date_calendar = new GregorianCalendar();
                        created_date_calendar.setTime(timestamp_format.parse(article_info[8]));
                        created_date_value = date_time_picker_format.format(created_date_calendar.getTime());                                                    
                    }
                    catch(Exception e)
                    {
                        System.out.println("Exception on admin_knowledge_base_article_edit.jsp getting created_date_value date:=" + e);
                    }
                    if(article_approver)
                    {
                        %>
                        <input type='text' id="created_date" name="created_date" value="<%=created_date_value%>" class="form-control datetime" />
                        <%
                    }
                    else
                    {
                        %>
                        <input type='text' readonly="true" id="created_date" name="created_date" value="<%=created_date_value%>" class="form-control datetime" />
                        <%
                    }
                    %>
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>          
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Approved By&nbsp;
                        <i class="la la-search" style="font-size: 14px;"></i>
                        <i class="la la-question-circle" style="font-size: 14px;" title="This field is a database lookup for all users."></i>                        
                    </label>
                </div>
                <div class="formField clr md px-0 border-0 input-group d-flex">
                    <%
                    String approved_by_id = article_info[9];
                    String approved_by_id_info[] = db.get_users.by_id(con, approved_by_id);
                    if(approved_by_id.equalsIgnoreCase("") || approved_by_id == null)
                    {
                        approved_by_id = "";
                        approved_by_id_info[1] = "";
                    }
                    %>
                    <input type="hidden" name="approved_by_id" id="approved_by_id" value="<%=approved_by_id%>"/>
                    <%
                    if(article_approver)
                    {
                        %>
                        <input type="text" name="approved_by_name" id="approved_by_name" value="<%=approved_by_id_info[1]%>" class="form-control" placeholder="Enter the approvers username"/>
                        <%
                    }
                    else
                    {
                        %>
                        <input type="text" readonly="true" name="approved_by_name" id="approved_by_name" value="<%=approved_by_id_info[1]%>" class="form-control" placeholder="Enter the approvers username"/>
                        <%
                    }
                    %>
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <a href="javascript:clear_approved_by(<%=article_info[0]%>)"><span class="la la-close"></span></a>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Date Approved
                    </label>
                </div>
                <div class="formField clr md px-0 border-0 input-group d-flex">
                    <%
                    String approved_date_value = "";
                    try
                    {
                        Calendar approved_date_calendar = new GregorianCalendar();
                        approved_date_calendar.setTime(timestamp_format.parse(article_info[10]));
                        approved_date_value = date_time_picker_format.format(approved_date_calendar.getTime());                                                    
                    }
                    catch(Exception e)
                    {
                        System.out.println("Exception on admin_knowledge_base_article_edit.jsp getting approved_date_value date:=" + e);
                    }
                    if(article_approver)
                    {
                        %>
                        <input type='text' id="approved_date" name="approved_date" value="<%=approved_date_value%>" class="form-control datetime" />
                        <%
                    }
                    else
                    {
                        %>
                        <input type='text' readonly="true" id="approved_date" name="approved_date" value="<%=approved_date_value%>" class="form-control datetime" />
                        <%
                    }
                    %>
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div> 
        </div>    

        <div class="row">     
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Article Text
                    </label>
                </div>
                <div class="formField clr md px-0 border-0 no-border p-0">
                    <%
                    String unEscaped_article_text = StringEscapeUtils.unescapeHtml4(article_info[5]);
                    %>
                    <textarea name="article_text" id="article_text_<%=article_info[0]%>" cols="30" rows="155">
                        <%=unEscaped_article_text%>
                    </textarea>
                </div>
            </div> 
        </div>

        <div class="row ">
            <div class="col-md-12 text-center pt-30 pb-30">
                <button type="button" class="btn btn-outline-primary customBtn lg waves-effect">Preview</button>
                <button type="submit" <%=(article_approver || article_info[4].equalsIgnoreCase("Draft")?"":"disabled")%> class="btn btn-primary-new customBtn lg waves-effect" role="button" name="submit" value="Submit">
                    Save
                </button>
            </div>
        </div>

    </form>
    <!-- BEGIN VENDOR JS-->    
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE LEVEL JS-->   
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_knowledge_base_add.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_knowledge_base_add.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
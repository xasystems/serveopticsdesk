<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String FINANCIAL = session.getAttribute("financial").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String CONTACT = session.getAttribute("contact").toString();
        String CX = session.getAttribute("cx").toString();
        String SURVEY = session.getAttribute("survey").toString();
        String SLA = session.getAttribute("sla").toString();        
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        String METRIC = session.getAttribute("metric").toString();
        
        
        if(!ADMINISTRATION.equalsIgnoreCase("true"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);   
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->

    <!-- END PAGE LEVEL CSS-->
    <div class="clr whtbg p-10 mb-15">
        <div class="float-right options">
            <ul>
                <li class="dInlineBlock dropdown ">
                    <div class="dInlineBlock clr menuHolder">
                        <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                        </a>
                        <div class="dropdown-menu dropdown-menu-right md p-10">
                            <h5 class="medium-font boldFont pt-5 mb-10">All Filters</h5>
                            <form>
                                <ul>
                                    <li class="mb-10 clr">
                                        <div class="formField md  mr-0 full-width">
                                            <select class="form-control">
                                                <option>Select Filters by Dates</option>
                                                <option>Select Filters by Dates</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Incident Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>
                                        <!-- <div class="row">
                                            <div class="col-md-12">
                                                <label class="field-label full-width">Incident Date (Start)</label>
                                                <div class="formField md full-width mr-0">

                                                    <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="field-label full-width">Incident Date (End)</label>
                                                <div class="formField md full-width mr-0">
                                                    <input class="no-border full-width" type="date" value="">
                                                </div>
                                            </div>
                                        </div> -->
                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Create Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>

                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Pending Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>

                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">State Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>

                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Close Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>

                                    </li>
                                </ul>
                                <div class="text-center pt-5 pb-5">
                                    <button class="btn md2 btn-primary-new customBtn mb-5">Apply Filters</button>
                                    <div class="clr"></div>
                                    <a href="#" class="">Reset Filters</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </li>
                <li class="dInlineBlock">
                    <span class="btn btn-success customBtn lg waves-effect rightSidePanelOpener">
                        <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>Add Custom Field
                    </span>    
                </li>
            </ul>
        </div>
        <h1 class="large-font boldFont">Custom Fields</h1>
    </div>    

    <div class="sort-tbl-holder">
        <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Form (Field Name)</th>
                    <th>Field Label</th>
                    <th>Field Type</th>
                    <th>Number of Columns</th>
                    <th>Required</th>
                    <th>Active</th>
                    <th>Actions</th> 
                </tr>
            </thead>

            <tbody>
                <%
                ArrayList<String[]> custom_fields = db.get_custom_fields.all(con);

                for(int a = 0; a < custom_fields.size(); a++)
                {
                    String custom_field[] = custom_fields.get(a);
                    String id = custom_field[0];
                    String form = custom_field[1];
                    String index = custom_field[2];
                    String field_name = custom_field[3];
                    String field_label = custom_field[4];
                    String default_text = custom_field[5];
                    String number_cols = custom_field[6];
                    String field_type = custom_field[7];
                    String field_values = custom_field[8];
                    String required = custom_field[9];
                    String active = custom_field[10];

                    %>
                    <tr>
                        <td><%=form + " (" + field_name + ")"%></td>
                        <td><%=field_label%></td>
                        <td><%=field_type%></td>
                        <td><%=number_cols%></td>
                        <td><%=required%></td>
                        <td><%=active%></td>
                        <td>
                            <a href="javascript:void(0)" onclick="event.stopPropagation(); editField(<%=id%>);" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>
                            <a href="javascript:void(0)" onclick="event.stopPropagation(); showDeleteModal(<%=id%>)" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>
                        </td>           

                    </tr>
                    <%
                    }                                        
                    %>                                                

                </tr>
            </tbody>
        </table>
    </div>
    
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <div class="rightPanelHolder">
        <div class="p-15 ticket-content">
            <span class="rightSidePanelCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
            <h4 id="right_panel_title" class="mediumFont large-font mb-20">Add Custom Field</h4>
            <div class="clr whtbg p-15 ">
                <form id="add_form" action="admin_custom_fields_add" class="add-project-form needs-validation" novalidate>  
                    <input type="hidden" name="id" value=""/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Form 
                                </label>
                            </div>
                            <div class="formField clr md px-0 border-0 ">
                                <select name="form" id="form" class="form-control" title="On which form should this new custom field be added" >
                                    <option value="Incident">Incident</option>
                                    <option value="Request">Request</option>
                                    <option value="Change">Change</option>
                                    <option value="Problem">Problem</option>
                                    <option value="Known_Error">Known Error</option>
                                    <option value="Release">Release</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Field Name
                                </label>
                            </div>
                            <div class="formField clr md px-0 border-0 ">
                                <input type="text" name="field_name" value="" class="form-control" placeholder="Enter the name of this field">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Field Label 
                                </label>
                            </div>
                            <div class="formField clr md px-0 border-0 ">
                                <input type="text" name="field_label" value="" class="form-control" placeholder="Enter the label of this field" title="This is name that will be stored in the database.&#13;&#10;This must be a unique name among the custom fields.&#13;&#10;Do not use spaces in the name.">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Field Type
                                </label>
                            </div>
                            <div class="formField clr md px-0 border-0 ">
                                <select onchange="add_field_type()" name="field_type" id="field_type" class="form-control" title="What type of input field? &#13;&#10;Select is a drop down with predefined values for the user to select.&#13;&#10;Text is for small text items like a city name. &#13;&#10;TextArea is for large amount of text like a paragraph describing your weekend.&#13;&#10;Checkbox, just a checkbox.&#13;&#10;People-Lookup is custom ServeOptics field that will lookup users as there name is entered into the field.&#13;&#10;Group-Lookup same as people lookup except for Groups.&#13;&#10;Date is a date/time field. ">
                                    <option value="Select">Select</option>
                                    <option value="Text">Text</option>
                                    <option value="TextArea">TextArea</option>
                                    <option value="Checkbox">Checkbox</option>
                                    <option value="People-Lookup">People-Lookup</option>
                                    <option value="Group-Lookup">Group-Lookup</option>
                                    <option value="Date">Date</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Number of Columns
                                </label>
                            </div>
                            <div class="formField clr md px-0 border-0 ">
                                <select name="number_cols" id="number_cols" class="form-control" title="Any number 1 thru 12.&#13;&#10;ServeOptics displays are divided into 12 column.&#13;&#10;Two columns is a normal size for most input fields.&#13;&#10;If all the custom fields where set to two columns, then there can be six custom fields in a single row (2 Columns x 6 Custom fields = Total 12 Columns which is one full row).&#13;&#10; if the number of columns was set to 12, then the custom field will fill an entire row on the page." >
                                    <option value="1">1</option>
                                    <option selected="" value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Display Order
                                </label>
                            </div>
                            <div class="formField clr md px-0 border-0 ">
                                <select name="index" id="index" class="form-control" title="In what order should the Custom Fields be displayed?&#13;&#10;The value can be 1 thru 99" >
                                    <%
                                    for(int a = 1; a < 100; a++)
                                    {
                                        %>
                                        <option value="<%=a%>"><%=a%></option>
                                        <%
                                    }
                                    %>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Required
                                </label>
                            </div>
                            <div class="formField clr md px-0 border-0 pl-0">
                                <div class="custom-control custom-checkbox dInlineBlock">
                                    <input type="checkbox" class="custom-control-input form-control" name="required" value="on" id="listingCheck1" title="Is this field required to be filled out" >
                                    <label class="custom-control-label small-font" for="listingCheck1"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Active
                                </label>
                            </div>
                            <div class="formField clr md px-0 border-0 pl-0">
                                <div class="custom-control custom-checkbox dInlineBlock">
                                    <input type="checkbox" class="custom-control-input form-control" name="active" value="on" id="listingCheck2" title="Is this field displayed on the form" >
                                    <label class="custom-control-label small-font" for="listingCheck2"></label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- dynamic part -->
                    <div id="default_text_div" class="row d-none">
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Default text
                                </label>
                            </div>
                            <div class="formField clr md px-0 border-0 ">
                                <input type="text" name="default_text" id="default_text" value="" class="form-control" placeholder="Enter the Default Text"/>
                            </div>
                        </div>
                    </div>

                    <div class="row d-none" id="option_table_div">
                        <div class="col-md-12">
                            <button type="button" onclick="delete_row()" class="btn mr-1 mb-1 btn-primary">Delete Selected</button>
                            <button type="button" onclick="add_row()" class="btn mr-1 mb-1 btn-primary">Add Values</button>
                            <table class="table" id="option_table" border="0" cellspacing="2" cellpadding="10">
                                <tr>
                                    <td>#</td>
                                    <td><b>Value</b></td>
                                    <td><b>Label</b></td>
                                    <td><b>Selected</b></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- end of dynamic part -->
                    <div class="row ">
                        <div class="col-md-12 text-center pt-30 pb-30">
                            <button type="button" class="btn btn-outline-primary customBtn lg waves-effect ">Cancel</button>
                            <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

    // delete confirmation modal
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="deleteModalLabel">Field deletion</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            Are you sure you want to delete this field? Deleting this Custom Field will permanently delete all the data saved under this Custom Field. 
            This action can not be undone!
            Consider de-activating the field, which will remove it from the form but keep all the saved data.
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-danger" onclick="deleteField()">Delete</button>
          </div>
        </div>
      </div>
    </div>         

    <!-- BEGIN PAGE LEVEL JS-->
    <script type="text/javascript">
    
    var delete_field_id = -1;
    
    function editField(field_id) 
    {
        $("form#add_form").get(0).classList.remove('was-validated');
        document.getElementById('add_form').reset();
        $("#option_table > tbody > tr").slice(1).remove();

        $.ajax({
            url: "ajax_lookup_custom_field_data",
            type: 'post',
            dataType: "json",
            data: 
            {
                id: field_id
            },
            error: function(jqXHR, textStatus) 
            {
                switch (jqXHR.status) {
                    case 401:
                        window.location.assign("<%=props.get("login_url").toString()%>");
                        break;
                    case 403:
                        alert("You don't have rights enough!");
                        break;
                    default:
                        alert("Error processing request!");
                }
            },
            success: function( data ) 
            {
                console.log(data);
                if (Object.keys(data).length > 0) {
                    $('.rightPanelHolder').addClass('open');
                    $('.rightSidePanel').addClass('open');
                    $('html').addClass('panelOpen');
                    $('#right_panel_title').html('Edit custom field');

                    document.getElementsByName("id")[0].value=data[0];
                    document.querySelectorAll('select[name="form"]')[0].value = data[1];
                    document.querySelectorAll('select[name="index"]')[0].value = data[2];
                    document.querySelectorAll('input[name="field_name"]')[0].value = data[3];
                    document.querySelectorAll('input[name="field_label"]')[0].value = data[4];
                    document.querySelectorAll('input[name="default_text"]')[0].value = data[5];
                    document.querySelectorAll('select[name="number_cols"]')[0].value = data[6];
                    document.querySelectorAll('select[name="field_type"]')[0].value = data[7];
                    document.getElementsByName("required")[0].checked = (data[9] === "true");
                    document.getElementsByName("active")[0].checked = (data[10] === "true");

                    const field_values_string = data[8].replaceAll("&quot;","\"");
                    console.log(field_values_string);        
                    if (field_values_string !== "")
                    {
                        /*{
                            "elements": {
                              "element": [
                                {
                                  "id": "1",
                                  "value": "Tom",
                                  "label": "Tom",
                                  "selected": "false"
                                },
                                {
                                  "id": "2",
                                  "value": "Frank",
                                  "label": "Frank",
                                  "selected": "false"
                                },
                                {

                                  "id": "3",
                                  "value": "Mary",
                                  "label": "Mary",
                                  "selected": "false"
                                }
                              ]
                            }
                          }*/

                        const field_values_obj = JSON.parse(field_values_string);
//                      console.log(field_values_obj);
                        if (Object.keys(field_values_obj).length > 0) {        
                            console.log(field_values_obj.elements.element);        
                            for (let key in field_values_obj.elements.element) 
                            {
                                console.log(key, field_values_obj.elements.element[key]);
                                add_row(field_values_obj.elements.element[key]);
                            }
                        }
                        
                    }
                    add_field_type();
                    
//                        console.log(data[0]);

                } else {
                    alert("Error processing request!");
                }
            }
        });            
    }
    
    function showDeleteModal(field_id) 
    {
        delete_field_id = field_id;
        $('#deleteModal').modal();
    }    

    function deleteField() 
    {
        window.location.assign("custom_field_delete?id="+delete_field_id);
    }    
    
    window.addEventListener("load", (event) => 
    {
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        
        var validation = Array.prototype.filter.call(forms, function(form) 
        {
            
            form.addEventListener('submit', function(event) 
            {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                    $('.rightPanelHolder').animate({scrollTop: $('.form-control:invalid').first().offset().top - 50}, 'fast');
                }
                form.classList.add('was-validated');
                var field_id = document.getElementsByName("id")[0].value;
                if (field_id !== "") 
                {
                    form.action = "admin_custom_fields_edit";
                    form.method = "POST";
                } else {
                    form.action = "admin_custom_fields_add";
                    form.method = "POST";
                }
            }, false);
        });            
        
        $('.rightSidePanelOpener').on('click', function(){
            $('#right_panel_title').html('Add custom field');
            $("form#add_form").get(0).classList.remove('was-validated');
            document.getElementsByName("id")[0].value = "";    
            document.getElementById('add_form').reset();
            $("#option_table > tbody > tr").slice(1).remove();
            add_field_type();
        });

    });
    
        function add_field_type() 
        {
            // code to be executed
            var field_type = document.getElementById("field_type").value; 
            //alert("add field field_type=" + field_type);
            
            if(field_type === "Select" )
            {
                //alert("This field type does have options");
                document.getElementById("option_table_div").classList.remove('d-none');
                document.getElementById("default_text_div").classList.add('d-none');
            }
            else
            {
                document.getElementById("option_table_div").classList.add('d-none');
                document.getElementById("default_text_div").classList.remove('d-none');
            }
        }
    
        function delete_row() 
        {
            /***We get the table object based on given id ***/
            var objTable = document.getElementById("option_table");
            /*** Get the current row length ***/
            var iRow = objTable.rows.length;
            /*** Initial row counter ***/
            var counter = 0;
            /*** Performing a loop inside the table ***/
            if (objTable.rows.length > 1) 
            {
                for (var i = 0; i < objTable.rows.length; i++) 
                {
                    /*** Get checkbox object ***/
                    var chk = objTable.rows[i].cells[0].childNodes[0];
                    if (chk.checked) 
                    {
                        /*** if checked we del ***/
                        objTable.deleteRow(i);
                        iRow--;
                        i--;
                        counter = counter + 1;
                    }
                }
            }
        }
    
        function add_row(data) 
        {
            var object_table = document.getElementById("option_table");
            var row_number = object_table.rows.length;            
            var field_type = document.getElementById("field_type").value;            
            if(field_type !== "Select" )
            {
                //do nothing
            }
            else
            {
                /*** We insert the row by specifying the current rows length ***/
                var object_row = object_table.insertRow(object_table.rows.length);

                /*** We insert the first row cell ***/
                var checkbox_cell = object_row.insertCell(0);
                /*** We  insert a checkbox object ***/
                var objInputCheckBox = document.createElement("input");
                objInputCheckBox.type = "checkbox";
                checkbox_cell.appendChild(objInputCheckBox);

                /*** field ***/
                var field_value_cell = object_row.insertCell(1);
                /*** We  add some text inside the celll ***/
                var field_value = "";
                if (data !== undefined && data.hasOwnProperty("value")) 
                {
                    field_value = data.value;
                }    
                field_value_cell.innerHTML = "<input class='form-control' type='text' name='option_value" + "~" + row_number + "' value='" + field_value + "'/>";
                

                /*** Operator ***/
                var field_label_cell = object_row.insertCell(2);
                var field_label = "";
                if (data !== undefined && data.hasOwnProperty("label")) 
                {
                    field_label = data.label;
                }    

                field_label_cell.innerHTML = "<input class='form-control' type='text' name='option_label" + "~" + row_number + "' value='" + field_label + "'/>";
                
                
                var selected_cell = object_row.insertCell(3);
                
                var field_selected = "";
                if (data !== undefined && data.hasOwnProperty("selected") && data.selected == "true") 
                {
                    field_selected = "checked";
                }    
                
                selected_cell.innerHTML = "<input type='radio' id='selected' name='selected' value='" + row_number + "' " + field_selected + ">";
            }
        }
    </script>

    <!-- END PAGE LEVEL JS-->   
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_systen_select_fields.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_systen_select_fields.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

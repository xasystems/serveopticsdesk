<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_users_add.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%> 
<%@page import="java.util.GregorianCalendar"%>
<%@ page import="java.time.ZonedDateTime"%>
<%@ page import="java.time.format.DateTimeFormatter"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        boolean authorized = support.role.authorized(session, "asset", "update");
        if (!authorized) {
            response.sendRedirect("no_permission.jsp");
        } 
        else 
        {
            Logger logger = LogManager.getLogger();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try 
            {
                String user_tz_name = "UTC";
                String user_tz_time = "+00:00";
                try
                {
                    user_tz_name = session.getAttribute("tz_name").toString();
                    user_tz_time = session.getAttribute("tz_time").toString();
                    if(user_tz_name == null || user_tz_time == null )
                    {
                        user_tz_name = "UTC";
                        user_tz_time = "+00:00";
                    }
                }
                catch(Exception e)
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }
                String home_page = session.getAttribute("home_page").toString();
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String id = request.getParameter("id");
                String asset_info[] = db.get_assets.for_id(con, id);
                String type_id = "";
                
%>
<!-- BEGIN PAGE LEVEL CSS-->
<!-- END PAGE LEVEL CSS-->

<form action="assets_edit" method="post" name="in_form" id="in_form">
    <input type="hidden" name="id" id="id" value="<%=id%>"/>
    <h4 class="mediumFont medium-font mb-20">Asset Attributes</h4>
    <div class="row">
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Serial #
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="serial_number" name="serial_number" class="form-control" value="<%=asset_info[1]%>"/>
            </div>
        </div>
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Asset Tag
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="asset_tag" name="asset_tag" class="form-control" value="<%=asset_info[2]%>"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Manufacturer
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="manufacturer" name="manufacturer" class="form-control" value="<%=asset_info[3]%>"/>
            </div>
        </div>
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Model
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="model" name="model" class="form-control" value="<%=asset_info[4]%>"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Type
                </label>
            </div>
            <div class="formField clr md px-0 border-0">    
                <select class="form-control" name="asset_type_id" id="asset_type_id" onchange="update_subtype(this)">
                    <option value=""></option>
                    <%
                    ArrayList<String[]> all_active_types = db.get_assets.all_active_types(con);
                    String asset_type_name = "";
                    for (int a = 0; a < all_active_types.size(); a++) 
                    {
                        String type[] = all_active_types.get(a);
                        if(asset_info[5].equalsIgnoreCase(type[1]))
                        {
                            type_id = type[0];
                            asset_type_name = type[1];
                            %><option SELECTED value="<%=type[0]%>"><%=type[1]%></option><%
                        }
                        else
                        {
                            %><option value="<%=type[0]%>"><%=type[1]%></option><%
                        }
                    }
                    %>
                </select>
                <input type="hidden" name="asset_type" id="asset_type" value="<%=asset_type_name%>"/>
            </div>
        </div>
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Subtype
                </label>
            </div>
            <div class="formField clr md px-0 border-0">                                                
                <select class="form-control" name="asset_subtype" id="asset_subtype">
                    <option value=""></option>
                    <%
                    ArrayList <String[]> subtypes_for_type = db.get_assets.all_subtypes_for_type_id(con, type_id);
                    for(int a = 0; a < subtypes_for_type.size();a++)
                    {
                        String subtype[] = subtypes_for_type.get(a);
                        if(asset_info[6].equalsIgnoreCase(subtype[2]))
                        {
                            %><option SELECTED value="<%=subtype[2]%>"><%=subtype[2]%></option><%
                        }
                        else
                        {
                            %><option value="<%=subtype[2]%>"><%=subtype[2]%></option><%
                        }
                    }
                    %>
                    
                </select>
            </div>
        </div> 
    </div>
    <div class="separator-new mb-30 mt-30"></div>
    <h4 class="mediumFont medium-font mb-20">Purchase/Warranty/State/Assignment</h4>
    <div class="row">
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Purchase Order
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="purchase_order" name="purchase_order" class="form-control" value="<%=asset_info[7]%>"/>
            </div>
        </div>
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Warranty Expire
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <%
                String warranty_expire = "";
                if(asset_info[8] == null || asset_info[8].equalsIgnoreCase("null"))
                {
                    
                }
                else
                {
                    
                    try
                    {
                        ZonedDateTime zdt_temp_date = support.date_utils.utc_to_user_tz(user_tz_name, asset_info[8]);
                        warranty_expire = DateTimeFormatter.ofPattern("HH:mm MM/dd/yyyy").format(zdt_temp_date);
                    }
                    catch (Exception e)
                    {
                        warranty_expire = "";
                        System.out.println("Exception on warranty_expire=" + e);
                    }
                }
                %>
                <input type="text" id="warranty_expire" name="warranty_expire" class="form-control" value="<%=warranty_expire%>" placeholder=""/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    State
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <select class="form-control" name="state" id="state">
                    <%
                    String state_option[] = {"","Ordered","New","Delivered","Active","Spare","Broken","Retired"};
                    for(int a = 0; a < state_option.length; a++)
                    {
                        if(state_option[a].equalsIgnoreCase(asset_info[9]))
                        {
                            %><option SELECTED value="<%=state_option[a]%>"><%=state_option[a]%></option><%
                        }
                        else
                        {
                            %><option value="<%=state_option[a]%>"><%=state_option[a]%></option><%
                        }
                    }
                    %>
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    State Date
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <%
                String state_date = "";
                if(asset_info[10] == null || asset_info[10].equalsIgnoreCase("null"))
                {
                    
                }
                else
                {                                                    
                    try
                    {
                        ZonedDateTime zdt_temp_date = support.date_utils.utc_to_user_tz(user_tz_name, asset_info[10]);
                        state_date = DateTimeFormatter.ofPattern("HH:mm MM/dd/yyyy").format(zdt_temp_date);                                                    
                    }
                    catch (Exception e)
                    {
                        state_date = "";
                        System.out.println("Exception on state_date=" + e);
                    }
                }
                %>
                <input type="text" id="state_date" name="state_date" class="form-control" value="<%=state_date%>" placeholder=""/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Assigned to Group&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <%
                String group_info[] = db.get_groups.by_id(con,asset_info[12]);                                            
                %>
                <input type="hidden" name="assigned_group_id" id="assigned_group_id" value="<%=asset_info[12]%>"/>
                <input type="text" name="assigned_group_name" id="assigned_group_name" value="<%=group_info[1]%>" class="form-control" />
            </div>
        </div>
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    
                Assigned to&nbsp;
                <i class="la la-search" style="font-size: 14px;"></i>
            
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <%
                String user_info[] = db.get_users.by_id(con,asset_info[11]);
                %>
                <input type="hidden" name="assigned_to_id" id="assigned_to_id" value="<%=asset_info[11]%>"/>
                <input type="text" name="assigned_to_name" id="assigned_to_name" value="<%=user_info[1]%>" class="form-control" placeholder="Enter the Assigned To username"/>
            </div>
        </div>
    </div>
    <div class="separator-new mb-30 mt-30"></div>
    <h4 class="mediumFont medium-font mb-20">Location</h4>
    <div class="row">
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Department
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="department" name="department" class="form-control" value="<%=asset_info[13]%>"/>
            </div>
        </div>
    </div><!--end row-->
    <div class="row">
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Location
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="location" name="location" class="form-control" value="<%=asset_info[14]%>"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Site
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="site" name="site" class="form-control" value="<%=asset_info[15]%>"/>
            </div>
        </div>
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Company
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="company" name="company" class="form-control" value="<%=asset_info[16]%>"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    IP
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <input type="text" id="ip" name="ip" class="form-control" value="<%=asset_info[17]%>"/>
            </div>
        </div>
    </div>
    <div class="separator-new mb-30 mt-30"></div>
    <h4 class="mediumFont medium-font mb-20">Notes/Log</h4>
    <div class="row">
        <div class="col-md-12">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Notes
                </label>
            </div>
            <div class="formField clr md mb-15 ">
                <textarea class="form-control no-border p-0" rows="4" id="notes" name="notes"><%=asset_info[18]%></textarea>
            </div>
        </div>
    </div>      
    <div class="row ">
        <div class="col-md-12 text-center pt-30 pb-30">
            <button type="button" class="btn btn-outline-primary customBtn lg waves-effect">Cancel</button>
            <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                Save
            </button>
        </div>
    </div>
</form>


<!-- BEGIN PAGE LEVEL JS-->
<!-- END PAGE LEVEL JS-->   
<%                con.close();

            } catch (Exception e) {
                System.out.println("Exception in assets_edit.jsp: " + e);
                logger.debug("ERROR: Exception in assets_edit.jsp:" + e);
            }
        } //end if not admin permission
    } //end if not redirected
%>
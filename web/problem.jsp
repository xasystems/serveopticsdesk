<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="java.util.Arrays"%>
<!--Copyright 2021 Real Data Technologies, Inc. , All rights reserved.-->
<%-- 
    Document   : problem.jsp
    Created on : 18-Jan-2020, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>

<%@ page import="java.time.Instant"%>
<%@ page import="java.time.format.DateTimeFormatter"%>
<%@ page import="java.time.LocalDateTime"%>
<%@ page import="java.time.ZoneId"%>
<%@ page import="java.time.ZonedDateTime"%>
<%@ page import="java.util.TimeZone"%>
<%@ page import="org.apache.logging.log4j.LogManager"%>
<%@ page import="org.apache.logging.log4j.Logger"%>


    <%
    String context_dir = request.getServletContext().getRealPath("");
    Logger logger = LogManager.getLogger();
    SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy"); //10:42 / 24-Jan-20   

    if(session.getAttribute("authenticated")==null)
    {
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        if (!session.getAttribute("problem").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            String user_tz_name = "UTC";
            String user_tz_time = "+00:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }                
            }
            catch(Exception e)
            {
                user_tz_name = "UTC";
                user_tz_time = "+00:00";
            }            
            
            String home_page = session.getAttribute("home_page").toString();
            boolean problem_not_found = false;
            
            java.util.Date now = new java.util.Date();
            //String now_display_format = display_format.format(now);
            //String now_timestamp_format = timestamp_format.format(now);
            //String now_problem_time = date_time_picker_format.format(now);
            Connection con = null;
            String selected = "";
            String category_id = "";
            String create_date = "";
            String problem_info[] = {"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};
            try
            {
                con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
            }
            catch(Exception e)
            {
                System.out.println("problem.jsp exception=" + e);
            }
            //get problem info
            String id = "0";
            try
            {
                id = request.getParameter("id");
                problem_info = db.get_problems.problem_by_id(con, id);
                
                if(problem_info[0].equalsIgnoreCase("--"))
                {
                    problem_not_found = true;
                }
                create_date = problem_info[4];
            }
            catch (Exception e)
            {
                System.out.println("problem.jsp problem_id " + id + " not found. exception=" + e);
                problem_not_found = true;
                id = "0";
            }
            boolean is_admin = false;
            if (session.getAttribute("administration").toString().equalsIgnoreCase("true") || session.getAttribute("manager").toString().equalsIgnoreCase("true")) 
            {
                is_admin = true;
            }
    %>
    <!-- BEGIN Page Level CSS-->
    <!-- END Page Level CSS-->
    
    <form action="problem_update" method="post" enctype="multipart/form-data">
       <input type="hidden" name="id" id="id" value="<%=id%>"/>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Problem ID
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input name="id" id="id" type="text" readonly value="" class="form-control" disabled placeholder="<%=problem_info[0]%>"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        <%
                        ZonedDateTime zdt_incident_time = support.date_utils.utc_to_user_tz(user_tz_name, problem_info[4]);
                        DateTimeFormatter date_picker_formatter = DateTimeFormatter.ofPattern("HH:mm MM/dd/yyyy");
                        String problem_time = zdt_incident_time.format(date_picker_formatter);
                        %>                                                
                        Problem Time
                    </label>
                </div>
                <div class="formField clr md px-0 border-0 input-group d-flex">
                    <input type='text' id="create_date" name="create_date" value="<%=problem_time%>" class="form-control datetime" disabled/>
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Problem Found By&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <%
                    String created_by_id = problem_info[5];
                    String created_by_name = problem_info[17];
                    %>
                    <input type="hidden" name="create_by_id" id="create_by_id" value="<%=created_by_id%>"/>
                    <input type="text" name="create_by_name" id="create_by_name" value="<%=created_by_name%>" class="form-control" placeholder="Enter the creaters username" disabled/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Priority
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <%
                    ArrayList<String[]> priority_select = db.get_system_select_field.active_for_table_column(con, "problems", "priority");
                    %>
                    <select name="priority" id="priority" class="form-control">
                        <option value=""></option>
                        <%
                        for(int a = 0; a < priority_select.size(); a++)
                        {
                            String select_option[] = priority_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            if(problem_info[1].equalsIgnoreCase(select_option[3]))
                            {
                                selected = "SELECTED";
                            }
                            else
                            {
                                selected = "";
                            }
                            //select_option[3] = value    select_option[4] = label
                            %>
                            <option <%=selected%> value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Status
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <%
                    ArrayList<String[]> status_select = db.get_system_select_field.active_for_table_column(con, "problems", "status");
                    %>
                    <select name="status" id="status" class="form-control">
                        <%
                        for(int a = 0; a < status_select.size(); a++)
                        {
                            String select_option[] = status_select.get(a);
                            if(problem_info[6].equalsIgnoreCase(select_option[3]))
                            {
                                selected = "SELECTED";
                            }
                            else
                            {
                                selected = "";
                            }
                            //select_option[3] = value    select_option[4] = label
                            %>
                            <option <%=selected%> value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Final Status
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <%
                    ArrayList<String[]> final_status_select = db.get_system_select_field.active_for_table_column(con, "problems", "final_status");
                    %>
                    <select name="final_status" id="final_status" class="form-control">
                        <%
//                        logger.info(Arrays.toString(problem_info));
                        for(int a = 0; a < final_status_select.size(); a++)
                        {
                            String select_option[] = status_select.get(a);
                            if(problem_info[22] != null && problem_info[22].equalsIgnoreCase(select_option[3]))
                            {
                                selected = "SELECTED";
                            }
                            else
                            {
                                selected = "";
                            }
                            //select_option[3] = value    select_option[4] = label
                            %>
                            <option <%=selected%> value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row d-none" id="close_linked_incidents_row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Close linked Incidents?
                    </label>
                </div>
                <div class="formField clr md mb-15 no-border px-0">
                    <div class="custom-control custom-checkbox dInlineBlock ">
                        <input type="checkbox" class="custom-control-input" name="close_linked_incidents" id="listingCheck3">
                        <label class="custom-control-label small-font" for="listingCheck3">Yes</label>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Closed Reason
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" name="closed_reason" id="closed_reason" value="" class="form-control"/> 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Name
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" name="name" id="name" value="<%=problem_info[2]%>" class="form-control"/> 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Description
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" name="description" id="description" value="<%=problem_info[3]%>" class="form-control"/> 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Assigned to&nbsp;
                    <i class="la la-search" style="font-size: 14px;"></i>
                    <i class="la la-question-circle" style="font-size: 14px;" title="This field is filtered by the Assigned to Group field. If Group Unassigned is selected, then all users will be available. If a specific group is selected, then only the members of that group will be available. If the Assigned to Group is changed this field will be cleared."></i>
                
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <%
                    String assigned_to_id = problem_info[9];
                    String assigned_to_name = problem_info[12];
                    %>
                    <input type="hidden" name="assigned_to_id" id="assigned_to_id" value="<%=assigned_to_id%>"/>
                    <input type="text" name="assigned_to_name" id="assigned_to_name" value="<%=assigned_to_name%>" class="form-control" placeholder="Enter the Assigned To group's name"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Assigned to Group&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <%
                    String assigned_group_id = problem_info[8];
                    String assigned_group_name = problem_info[11];
                    %>
                    <input type="hidden" name="assigned_group_id" id="assigned_group_id" value="<%=assigned_group_id%>"/>
                    <input type="text" name="assigned_group_name" id="assigned_group_name" value="<%=assigned_group_name%>" placeholder="Group Unassigned" class="form-control" />
                </div>
            </div>
        </div>
        <div class="row"><!--Start Custom Fields-->  
            <%
            //get active custom problem fields
            ArrayList <String[]> custom_fields = db.get_custom_fields.active_for_form(con, "problem");
            ArrayList<String[]> db_values = db.get_custom_field_data.active_by_form(con, "problem",id);

            for(int a = 0; a < custom_fields.size(); a++)
            {
                String custom_field[] = custom_fields.get(a);
                String field_name = custom_field[3];
                String db_value = "";
                //find the saved value if any
                for(int b = 0; b < db_values.size(); b++)
                {
                    String temp[] = db_values.get(b);
                    if(temp[2].equalsIgnoreCase(field_name))
                    {
                        db_value = temp[3];
                    }
                }

                //String db_value = "Bill";
                if(custom_field[7].equalsIgnoreCase("select"))
                {
                    %>
                    <jsp:include page='build_custom_select_field.jsp'>
                        <jsp:param name="field_name" value="<%=field_name%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="new_record" value="false"/>
                        <jsp:param name="db_value" value="<%=db_value%>"/>                                                
                    </jsp:include>
                    <%   
                }
                else if(custom_field[7].equalsIgnoreCase("text"))
                {
                    %>
                    <jsp:include page='build_custom_text_field.jsp'>
                        <jsp:param name="field_name" value="<%=field_name%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="false"/>
                        <jsp:param name="db_value" value="<%=db_value%>"/>         
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("textarea"))
                {
                    %>
                    <jsp:include page='build_custom_textarea_field.jsp'>
                        <jsp:param name="field_name" value="<%=field_name%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="false"/>
                        <jsp:param name="db_value" value="<%=db_value%>"/>    
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("checkbox"))
                {
                    %>
                    <jsp:include page='build_custom_checkbox_field.jsp'>
                        <jsp:param name="field_name" value="<%=field_name%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="false"/>
                        <jsp:param name="db_value" value="<%=db_value%>"/>    
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("people-lookup"))
                {
                    String user_info[] = db.get_users.by_id(con, db_value);
                    String user_name = user_info[1];
                    %>
                    <jsp:include page='build_custom_people_field.jsp'>
                        <jsp:param name="field_name" value="<%=field_name%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="false"/>
                        <jsp:param name="db_value" value="<%=db_value%>"/>   
                        <jsp:param name="user_name" value="<%=user_name%>"/>   
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("group-lookup"))
                {
                    String group_info[] = db.get_groups.by_id(con, db_value);
                    String group_name = group_info[1];
                    %>
                    <jsp:include page='build_custom_group_field.jsp'>
                        <jsp:param name="field_name" value="<%=field_name%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="false"/>
                        <jsp:param name="db_value" value="<%=db_value%>"/>   
                        <jsp:param name="group_name" value="<%=group_name%>"/>   
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("date"))
                {
                    %>
                    <jsp:include page='build_custom_date_field.jsp'>
                        <jsp:param name="field_name" value="<%=field_name%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="false"/>
                        <jsp:param name="db_value" value="<%=db_value%>"/>   
                    </jsp:include>
                    <%
                }
            }
            %>    
        </div><!--End Custom Fields-->      
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Knowledge Article<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0 input-group d-flex">
                    <input type="hidden" name="knowledge_article_id" id="knowledge_article_id"/>
                    <input type='text' id="knowledge_search_terms" name="knowledge_search_terms" placeholder="" value="" class="form-control" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-question"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>            
        <div class="row">
            <div class="col-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Notes
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <textarea name="notes" id="notes" cols="30" rows="5" class="ckeditor"><%=problem_info[10]%>
                    </textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Attachments
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="file" name="file" multiple>
                        <label class="custom-file-label" for="file">Choose file(s)</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="formField clr md px-0 border-0">
                    <% 
                    ArrayList<String[]> attachments = new ArrayList();
                    attachments = db.get_problems.get_attachments_by_problem_id(con, id);
                    for (int i = 0; i < attachments.size(); i++)
                    {
                        String[] attachment = attachments.get(i);
                    %>
                    <div class="clr tag dInlineBlock formField md whtbg mr-5 mb-15 w-auto float-none">
                        <span>
                            <a title="Uploaded by <%=attachment[9]%> on <%=attachment[5]%>" href="javascript:void(0)" onclick="openAttachment();event.preventDefault" data-file-name="<%=attachment[4]%>" data-link="<%=attachment[3]%>"><%=attachment[4]%></a>
                        </span>
                        <a data-id="<%=attachment[0]%>" href="javascript:void(0)" onclick="deleteAttachment()"><img class="icon" src="assets/images/svg/cross-icon.svg" alt=""></a>
                    </div>                    
                    <%
                    }
                    %>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Associated Incidents
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" name="incident_lookup" id="incident_lookup" value="" class="form-control" placeholder="Type an ID or Description to search"/> 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="container-fluid">
                    <div class="sort-tbl-holder container-fluid">
                        <table class="table table-striped custom-sort-table rightPanelDataTable" cellspacing="0" width="100%" data-ordering='false' data-searching='false' data-length-change="false">
                            <thead>
                                <tr>
                                    <th data-data="id">Incident ID</th>
                                    <th data-data="priority">Priority</th>
                                    <th data-data="state">Status</th>
                                    <th data-data="incident_time">Incident Time</th>
                                    <th data-data="description">Description</th>
                                    <th data-data="caller_username">Caller</th>
                                    <th data-data="assigned_to_username">Assigned To</th>
                                    <th data-data="actions">Unlink</th>
                                </tr>
                            </thead>
                            <tbody>
                            <%  ArrayList<String[]> incidents = db.get_problems.problem_incidents_for_problem(con, id, null);
                                ArrayList<String> incident_ids = new ArrayList<>();
                                for(int a = 0; a < incidents.size();a++)
                                {
                                    String incident_record[] = incidents.get(a);
                                    incident_ids.add("\"" + incident_record[0] + "\""); 
                            %>
                                <tr data-id="<%=incident_record[0]%>">
                                    <td><%=incident_record[0]%></td>                                            
                                    <td><%=incident_record[14]%></td>                                            
                                    <td><%=incident_record[19]%></td>                                            
                                    <td><%=incident_record[2]%></td>                                            
                                    <td><%=incident_record[15]%></td>                                            
                                    <td><%=incident_record[32]%></td>                                            
                                    <td><%=incident_record[39]%></td>                                            
                                    <td><a href="javascript:void(0)" onclick="unlinkIncident('<%=incident_record[0]%>')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a></td>                                            
                                </tr>
                            <%  } %>
                            </tbody>
                        </table>
                            <input type="hidden" name="incident_ids" value='[<%=(incidents != null ? StringUtils.join(incident_ids, ",") : "")%>]'>
                    </div>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-12 text-center pt-30 pb-30">
                <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                    Save
                </button>
            </div>
        </div>                                
    </form>
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<!--Copyright 2019 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_request_sla_add.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
    <%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        boolean authorized = support.role.authorized(session, "sla","create");
        if(!authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }    
        else 
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session); 
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);

    %>  
    <!-- BEGIN PAGE LEVEL CSS-->

    <!-- END PAGE LEVEL CSS-->
        <form class="form" action="admin_request_sla_add" method="post">
            <!-- start content here-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only mb-15">
                                                        Catalog Item
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0 ">
                                                    <select name="service_catalog_item_id" id="service_catalog_item_id" class="form-control">
                                                    <%
                                                    ArrayList <String[]> catalog_items = db.get_service_catalog.service_catalog_items_by_state(con, "Published");
                                                    Map<String, String> item_names = new HashMap<String, String>();

                                                    Map<String, List<String>> catalog = new HashMap<String, List<String>>();
                                                    for(int a = 0; a < catalog_items.size(); a++)
                                                    {
                                                        String catalog_item[] = catalog_items.get(a);
                                                        item_names.put(catalog_item[0], catalog_item[2]);
                                                        
                                                        if (catalog.containsKey(catalog_item[37])) {
                                                            catalog.get(catalog_item[37]).add(catalog_item[0]);
                                                        } else {
                                                            List<String> valueList = new ArrayList<>();
                                                            valueList.add(catalog_item[0]);
                                                            // and put both into the Map
                                                            catalog.put(catalog_item[37], valueList);
                                                        }
                                                    }
                                                    for (String key : catalog.keySet()) {
                                                        %>
                                                            <optgroup label="<%=key%>">
                                                        <%
                                                        List<String> value = catalog.get(key);
                                                        for (String val : value) {
                                                            %>
                                                                <option value="<%=val%>"><%=item_names.get(val)%></option>
                                                            <%
                                                        }
                                                    }
                                                    %>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only mb-15">
                                                        Request Priority
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">                                                
                                                    <select id="request_priority" name="request_priority" class="form-control">
                                                        <option value="Low">Low</option>
                                                        <option value="Medium">Medium</option>
                                                        <option value="High">High</option>
                                                        <option value="Critical">Critical</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only mb-15">
                                                        SLA Display Name
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0 ">
                                                    <input type="text" id="name" name="name" class="form-control" placeholder="Name Example: Company or Department - Time to Close" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only mb-15">
                                                        Short Description (Optional)
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0 ">
                                                    <input type="text" id="description" name="description" class="form-control" placeholder="Description" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only mb-15">
                                                        SLA Target
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">
                                                    <input type="text" id="target" name="target" class="form-control" placeholder="SLA Target in percentage 0-100%" />                                    
                                                </div>
                                            </div> 
                                            <div class="col-md-6 pt-10">
                                                <div class="custom-control custom-checkbox dInlineBlock mr-15">
                                                    <input type="checkbox" class="custom-control-input" id="active" name="active">
                                                    <label class="custom-control-label small-font" for="active">Active?</label>
                                                </div>
                                                <div class="custom-control custom-checkbox dInlineBlock mr-15">
                                                    <input type="checkbox" class="custom-control-input" id="reportable" name="reportable">
                                                    <label class="custom-control-label small-font" for="reportable">Do you want this SLA included on the Incident Dashboard?</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only mb-15">
                                                        SLA Text Detail (Optional)
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">
                                                    <textarea class="form-control" name="sla_text" id="sla_text" rows="3" placeholder="Enter the SLA text here"></textarea>  
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only mb-15">
                                                        SLA Type
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">                                                
                                                    <select id="sla_type" name="sla_type" class="form-control">
                                                        <option value="TIME">Time</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only mb-15">
                                                        SLA Timeframe
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">                                                
                                                    <select id="sla_timeframe" name="sla_timeframe" class="form-control">
                                                        <option value="DAILY">Daily</option>
                                                        <option value="WEEKLY">Weekly</option>
                                                        <option value="MONTHLY">Monthly</option>
                                                        <option value="QUARTERLY">Quarterly</option>
                                                        <option value="ANNUAL">Annual</option>
                                                        <option value="CUSTOM">Custom</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!--<div class="col-md-3">
                                                <label for="schedule_id">Schedule</label>
                                                <select id="schedule_id" name="schedule_id" class="form-control">
                                                    <%
                                                        String all_schedules[][] = db.get_schedules.all(con);
                                                        for (int a = 0; a < all_schedules.length; a++) {
                                                    %>
                                                    <option value="<%=all_schedules[a][0]%>"><%=all_schedules[a][1]%></option>
                                                    <%
                                                        }
                                                    %>
                                                </select>
                                            </div>-->

                                    </div>
                                    <div class="row ">
                                        <div class="col-md-12 pt-15">
                                            <button type="button" onclick="show_select()" class="btn mr-1 mb-1 btn-primary">Add Additional Parameters</button>
                                            <p>
                                                <em>
                                                    Do you need to add additional parameters to further define records that will be part of this SLA?
                                                </em>
                                            </p>
                                            <div class="row  col-md-12" id="additional_parameters">
                                            </div>
                                        </div>                                        
                                    </div>
                                    <div class="row" id="parameter_selector" style="display:none">
                                        <div class="col-md-12">
                                            <div class="field-title">
                                                <label class="formLabel md has-txt-only mb-15">
                                                    Select a Field
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <select onchange="add_measure()" id="field_name" name="field_name" class="form-control">
                                                    <option value="--"></option>
                                                    <option value="description">Description</option>
                                                    <option value="contact_method">Contact Method</option>
                                                    <option value="assigned_group_id">Assigned to Group</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row"  id="field_measure_div" style="display:none">
                                        <div class="col-md-12">
                                            <div class="field-title">
                                                <label class="formLabel md has-txt-only mb-15">
                                                    Select a Measure
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <select onchange="add_request_parameter_choices()" id="field_measure" name="field_measure" class="form-control">
                                                    <option value="--"></option>
                                                    <option value="equal">Equal</option>
                                                    <option value="not_equal">Not Equal</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" id="field_choices_div" style="display:none">
                                            <div id="contact_method_choices" style="display:none">
                                                <label for="field_select_choices">Select all the values that apply</label>
                                                <select onchange="show_add_button()" class="select2 form-control" multiple="multiple" id="field_values" name="field_values">

                                                </select>
                                            </div>
                                            <div id="description_choices" style="display:none">
                                                <label for="field_text_choices">Enter text that will match the text in the Request Description</label>
                                                <input onfocus="show_add_button()" type="text" id="field_text_choices" name="field_text_choices" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-3" id="add_button_div" style="display:none">
                                        <div class="col-md-6">
                                            <button type="button" onclick="add_row()" class="btn mr-1 mb-1 btn-primary">Add >></button>
                                        </div>
                                    </div>
                                    <div class="row" id="top_hr" style="display:none">
                                        <div class="col-md-12">
                                            <hr/>
                                        </div>
                                    </div>
                                    <div class="row" id="additional_parameter" style="display:none">        
                                        <div class="col-md-12" id="additional_parameter_list" >
                                            <button type="button" onclick="delete_row()" class="btn mr-1 mb-1 btn-primary">Delete Selected</button>
                                            <table class="table" id="parameter_table" border="0" cellspacing="2" cellpadding="10">
                                                <tr>
                                                    <td>#</td>
                                                    <td><b>Field</b></td>
                                                    <td><b>Operator</b></td>
                                                    <td><b>Value(s)</b></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row" id="bottom_hr" style="display:none">
                                        <div class="col-md-12">
                                            <hr/>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4 class="card-title">Compliance Parameters </h4>
                                        </div>
                                    </div>
                                            
                                    <div class="row">                                    
                                        <div class="col-md-12">
                                            <div class="field-title">
                                                <label class="formLabel md has-txt-only mb-15">
                                                    Which State defines the starting point of the Incident record?
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">

                                                <select id="start_condition" name="start_condition" class="form-control">
                                                    <option value="--"></option>
                                                    <option value="create_date">Create Date</option>
                                                    <option value="request_date">Request Date</option>
                                                    <option value="resolve_date">Resolved Date</option>
                                                    <option value="closed_date">Closed Date</option>                                            
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="field-title">
                                                <label class="formLabel md has-txt-only mb-15">
                                                    Which State defines the stopping point of the Incident record?
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <select id="stop_condition" name="stop_condition" class="form-control">
                                                    <option value="--"></option>
                                                    <option value="create_date">Create Date</option>
                                                    <option value="request_date">Request Date</option>
                                                    <option value="resolve_date">Resolved Date</option>
                                                    <option value="closed_date">Closed Date</option>  
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="field-title">
                                                <label class="formLabel md has-txt-only mb-15">
                                                    Stopping state
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <select id="stop_state" name="stop_state" class="form-control">
                                                    <option value="pending_approval">Pending Approval</option>
                                                    <option value="approved">Approved</option>
                                                    <option value="closed_complete">Closed Complete</option>
                                                    <option value="closed_incomplete">Closed Incomplete</option>
                                                    <option value="closed_cancelled">Closed Cancelled</option>
                                                    <option selected value="closed_rejected">Closed Rejected</option>
                                                    <option SELECTED value=""></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="field-title">
                                                <label class="formLabel md has-txt-only mb-15">
                                                    Operator
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <select id="sla_success_threshold_operator" name="sla_success_threshold_operator" class="form-control">
                                                    <option value="greater_than">Greater than</option>
                                                    <option value="greater_than_or_equal">Greater than or equal to</option>
                                                    <option value="equal_to">Equal to</option>
                                                    <option value="not_equal_to">Not equal to</option>
                                                    <option value="less_than_or_equal">Less than or equal</option>
                                                    <option selected value="less_than">Less than</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="field-title">
                                                <label class="formLabel md has-txt-only mb-15">
                                                    Value
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <input type="text" id="sla_success_threshold_value" name="sla_success_threshold_value" class="form-control" placeholder="Enter a whole number" />     
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="field-title">
                                                <label class="formLabel md has-txt-only mb-15">
                                                    Unit
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <select id="sla_success_threshold_unit" name="sla_success_threshold_unit" class="form-control">
                                                    <option value="seconds">Seconds</option>
                                                    <option value="minutes">Minutes</option>
                                                    <option value="hours">Hours</option>
                                                    <option value="days">Days</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-md-12 text-center pt-30 pb-30">
                                            <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                                            <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                                                Save
                                            </button>
                                        </div>
                                    </div>

            <input type="hidden" id="report_name" name="report_name" value="" />
            <input type="hidden" id="report_description" name="report_description" value="" />
            <input type="hidden" id="report_header" name="report_header" value="" />
            <!--<div class="row">
                <div class="col-12">   
                    <div class="card">    
                        <div class="card-header">
                            <h4 class="card-title">Report Parameters </h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name">Report Name</label>                                        
                                            <input type="text" id="report_name" name="report_name" class="form-control" placeholder="Report Name" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name">Report Description</label>                                        
                                            <input type="text" id="report_description" name="report_description" class="form-control" placeholder="Report Description" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name">Report Header</label>                                        
                                            <input type="text" id="report_header" name="report_header" class="form-control" placeholder="Report Header" />
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>   
                    </div>
                </div>
            </div>-->
            <!-- end content here-->
        </form>   
<!-- BEGIN PAGE LEVEL JS-->
<!-- END PAGE LEVEL JS-->   
<%                con.close();

            } catch (Exception e) {
                System.out.println("Exception in admin_sla_add.jsp: " + e);
                logger.debug("ERROR: Exception in admin_sla_add.jsp:" + e);
            }
        } //end if not admin permission
    } //end if not redirected
%>

<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : project_list.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        boolean page_authorized = support.role.authorized(session, "project","read");  
        String user_id = "0";
        try
        {
            user_id = session.getAttribute("user_id").toString(); 
        }
        catch(Exception e)
        {
            user_id = "0";
        }
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            boolean project_delete = false;
            boolean project_update = false;
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            
            String status = request.getParameter("status");
            if(status == null || status.equals(""))
            {
                status = "all";
            }
            ArrayList<String[]> projects = new ArrayList();
            ArrayList<String[]> project_tasks = new ArrayList();
            
            if(session.getAttribute("project").toString().equalsIgnoreCase("delete") || session.getAttribute("administration").toString().equalsIgnoreCase("true") || session.getAttribute("manager").toString().equalsIgnoreCase("true"))
            {
                project_delete = true;
                project_update = true;
            }
            else if(session.getAttribute("project").toString().equalsIgnoreCase("update"))
            {
                project_delete = false;
                project_update = true;
            }
        
            
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END Custom CSS-->
    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title"><i class="la la-building"></i>&nbsp;My&nbsp;Projects</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"> 
                                    <a href="<%=home_page%>"><i class="la la-home"></i>&nbsp;Home</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- start content here-->
            <%
            projects = db.get_projects.all_open_assigned_to_id(con, user_id);
            project_tasks = db.get_projects.open_project_tasks_for_user_id(con, user_id);
            %>
            <div class="row">
                <div class="col-12">
                    <hr>
                </div>
            </div>
            
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="heading-labels">My Projects</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <table id="request_table" class="table table-striped table-bordered zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Status</th>
                                            <th>Scheduled Start</th>
                                            <th>Scheduled End</th>
                                            <th>Manager</th>
                                            <th>Owner Group</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                        for(int a = 0; a < projects.size(); a++)
                                        {
                                            String project_info[] = projects.get(a);
                                            String scheduled_start = "";
                                            try
                                            {
                                                scheduled_start = filter_format.format(timestamp_format.parse(project_info[7]));
                                            }
                                            catch (Exception e)
                                            {
                                                scheduled_start = "";
                                            }

                                            String scheduled_end = "";
                                            try
                                            {
                                                scheduled_end = filter_format.format(timestamp_format.parse(project_info[9]));
                                            }
                                            catch (Exception e)
                                            {
                                                scheduled_end = "";
                                            }

                                            %>

                                            <tr>
                                                <td>    <%
                                                    if(project_update)
                                                    {
                                                        %>
                                                        <a href="project.jsp?project_id=<%=project_info[0]%>"><i class="la la-edit info"></i></a>&nbsp;
                                                        <%
                                                    }
                                                    if(project_delete)
                                                    {
                                                        %>
                                                        <a href="project_delete.jsp?project_id=<%=project_info[0]%>"><i class="la la-times danger" style="cursor: pointer"></i></a>&nbsp;
                                                        <%
                                                    }                                                        
                                                    %>
                                                    &nbsp;<%=project_info[1]%>
                                                </td>
                                                <td><%=project_info[2]%></td>
                                                <td><%=project_info[11]%></td>
                                                <td><%=scheduled_start%></td>
                                                <td><%=scheduled_end%></td>
                                                <td><%=project_info[21] + " " + project_info[23]%></td>
                                                <td><%=project_info[14]%></td>

                                            </tr>
                                            <%
                                        }                                        
                                        %>                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Status</th>
                                            <th>Scheduled Start</th>
                                            <th>Scheduled End</th>
                                            <th>Manager</th>
                                            <th>Owner Group</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="heading-labels">My Tasks</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <table id="request_table" class="table table-striped table-bordered zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>Project Name</th>
                                            <th>Task Name</th>
                                            <th>Description</th>
                                            <th>Status</th>
                                            <th>Scheduled Start</th>
                                            <th>Scheduled End</th>
                                            <th>Assigned Group</th>
                                            <th>Assigned To</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                        for(int a = 0; a < project_tasks.size(); a++)
                                        {
                                            String task_info[] = project_tasks.get(a);


                                            String scheduled_start = "";
                                            try
                                            {
                                                scheduled_start = filter_format.format(timestamp_format.parse(task_info[8]));
                                            }
                                            catch (Exception e)
                                            {
                                                scheduled_start = "";
                                            }

                                            String scheduled_end = "";
                                            try
                                            {
                                                scheduled_end = filter_format.format(timestamp_format.parse(task_info[10]));
                                            }
                                            catch (Exception e)
                                            {
                                                scheduled_end = "";
                                            }

                                            %>
                                            <tr onclick="location.href='project_task_edit.jsp?project_id=<%=task_info[1]%>&task_id=<%=task_info[0]%>';" style="cursor:pointer;">
                                            <td><%=task_info[21]%></td>
                                            <td><%=task_info[2]%></td>
                                            <td><%=task_info[3]%></td>
                                            <td><%=task_info[7]%></td>
                                            <td><%=scheduled_start%></td>
                                            <td><%=scheduled_end%></td>
                                            <td><%=task_info[15]%></td>
                                            <td><%=task_info[17] + " " + task_info[19]%></td>

                                            </tr>
                                            <%
                                        }                                        
                                        %>                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Project Name</th>
                                            <th>Task Name</th>
                                            <th>Description</th>
                                            <th>Status</th>
                                            <th>Scheduled Start</th>
                                            <th>Scheduled End</th>
                                            <th>Assigned Group</th>
                                            <th>Assigned To</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <script>
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>


    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script>
        $('#request_table').dataTable( {
            "order": []
        } );
    </script>
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%      con.close();
        }//end if not permission
    }//end if not logged in
%>
<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_knowledge_base_add.jsp
    Created on : Oct 4 2020, 6:13:59 PM
    Author     : rcampbell
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        boolean page_authorized = support.role.authorized(session, "ADMINISTRATION","read"); 
        
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String id = request.getParameter("id");
                String kb_info[] = db.get_kb.kb_info_for_id(con, id);
                
    %>
    <!-- BEGIN PAGE LEVEL CSS-->
    <!-- END PAGE LEVEL CSS-->
    
    <form action="admin_knowledge_base_edit" method="POST">
        <input type="hidden" name="id" id="id" value="<%=id%>"/>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Name
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input name="name" id="name" type="text" class="form-control" value="<%=kb_info[1]%>"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Description
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input name="description" id="description" type="text" class="form-control" value="<%=kb_info[2]%>"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        State
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="state" id="state" class="form-control">
                        <%
                        String state_names[] = {"Draft","Awaiting Approval","Published","Superceded","Expired","Invalid","Deleted"};
                        String state_values[] = {"Draft","Waiting_Approval","Published","Superceded","Expired","Invalid","Deleted"};
                        for(int a = 0; a < state_values.length; a++)
                        {
                            String selected = "";
                            if(kb_info[3].equalsIgnoreCase(state_values[a]))
                            {
                                selected = "SELECTED";
                            }
                            %>
                            <option <%=selected%> value="<%=state_values[a]%>"><%=state_names[a]%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Type
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="type" id="type" class="form-control">
                        <%
                        String type_names[] = {"Service Desk","Self Service"};
                        String type_values[] = {"Service_Desk","Self_Service"};
                        for(int a = 0; a < type_values.length; a++)
                        {
                            String selected = "";
                            if(kb_info[4].equalsIgnoreCase(type_values[a]))
                            {
                                selected = "SELECTED";
                            }
                            %>
                            <option <%=selected%> value="<%=type_values[a]%>"><%=type_names[a]%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-12 text-center pt-30 pb-30">
                <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                    Save
                </button>
            </div>
        </div>

    </form>
            
    <!-- END PAGE LEVEL JS-->   
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_knowledge_base_edit.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_knowledge_base_edit.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
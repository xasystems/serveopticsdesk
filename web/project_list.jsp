<%-- 
    Document   : project_list.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="org.apache.commons.codec.digest.HmacAlgorithms"%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.time.ZonedDateTime"%>
<%@ page import="java.time.format.DateTimeFormatter"%>
<%@ page import="org.apache.commons.codec.digest.HmacUtils"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        boolean page_authorized = support.role.authorized(session, "project","read");  
        String user_id = "0";
        try
        {
            user_id = session.getAttribute("user_id").toString(); 
        }
        catch(Exception e)
        {
            user_id = "0";
        }
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            String user_tz_name = "UTC";
            String user_tz_time = "+00:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "UTC";
                user_tz_time = "+00:00";
            }
            boolean project_delete = false;
            boolean project_update = false;
            String date_range = "";
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020            
            String action = StringUtils.defaultString(request.getParameter("action"));
            String predefined = StringUtils.defaultString(request.getParameter("predefined"));
            String project_id = StringUtils.defaultString(request.getParameter("project_id"));
            String task_id = StringUtils.defaultString(request.getParameter("task_id"));

            date_time_picker_format.setTimeZone(TimeZone.getTimeZone(user_tz_name));
            java.util.Date now = new java.util.Date();
            String now_picker_time = date_time_picker_format.format(now);
            String timestamp_string = timestamp_format.format(now);
            
            String status = request.getParameter("status");
            if(status == null || status.equals(""))
            {
                status = "all";
            }
            ArrayList<String[]> projects = new ArrayList();

            if(session.getAttribute("project").toString().equalsIgnoreCase("delete") || session.getAttribute("administration").toString().equalsIgnoreCase("true") || session.getAttribute("manager").toString().equalsIgnoreCase("true"))
            {
                project_delete = true;
                project_update = true;
            }
            else if(session.getAttribute("project").toString().equalsIgnoreCase("update"))
            {
                project_delete = false;
                project_update = true;
            }

            String couchdb_username = timestamp_string;
//            String couchdb_token = HmacUtils(HmacAlgorithms.HMAC_SHA_1, couchdb_username).hmacHex(couchdb_username);
            String couchdb_secret = props.get("couchdb.secret").toString();
            String couchdb_token = HmacUtils.hmacSha1Hex(couchdb_secret, couchdb_username);
            ArrayList<String[]> status_select = db.get_system_select_field.active_for_table_column(con, "projects", "status");

                                              
    %>
<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>


<jsp:include page='menu_service_desk.jsp'>
    <jsp:param name="active_menu" value=""/>
</jsp:include>	

<!-- BEGIN PAGE LEVEL CSS-->
<style type="text/css">
    .dropdown-menu.md {
        width: 500px;
    }
    .dropdown-menu input {
        width: auto !important;
    }
</style>
<link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
<!-- END VENDOR CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
<!--<link rel="stylesheet" type="text/css" href="assets/css/dashboard.css">-->
<link rel="stylesheet" type="text/css" href="assets/css/chat_app.css">

<!--End Page level css-->

<div class="clr whtbg p-10 mb-15">
    <div class="float-right options">
                <ul>
            <li id="filters_dropdown" class="dInlineBlock dropdown ">
                <div class="dInlineBlock clr menuHolder">
                    <a id="filters_dropdown_toggle" class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters</a>
                    <div id="filters_dropdown_menu" class="dropdown-menu dropdown-menu-right md p-10" x-placement="bottom-end" style="position: absolute; transform: translate3d(100px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <form id="filter_form" onsubmit="return false">
                            <ul>
                                <div class="row">
                                    <div class="col-md-12">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                Filters Category
                                            </label>
                                            <div class="formField md full-width mr-0">
                                                <select onchange="change_filter_category(this)" class="border-0 full-width">
                                                    <option value="filter_by_params">Params</option>
                                                    <option value="filter_by_predefined">Predefined Filters</option>
                                                </select>
                                            </div>
                                        </li>
                                    </div>
                                </div>
                                <div id="filter_by_params" class="filter-category-block">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Project Id
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="project_id" name="project_id" value="<%=project_id%>" class="border-0 full-width">
                                                </div>
                                            </li>
                                        </div>
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Task Id
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="task_id" name="task_id" value="<%=task_id%>" class="border-0 full-width">
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Scheduled Start Date
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="scheduled_start_date" name="scheduled_start_date" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime">
                                                </div>
                                            </li>
                                        </div>
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Scheduled End Date
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="scheduled_end_date" name="scheduled_end_date" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime">
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Actual Start Date
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="actual_start_date" name="actual_start_date" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime">
                                                </div>
                                            </li>
                                        </div>
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Actual End Date
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="text" id="actual_end_date" name="actual_end_date" value="" placeholder="Both Start and End must be set" class="border-0 full-width datetime">
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Owner
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="hidden" name="owner_id" id="owner_id" value="">
                                                    <input type="text" id="owner_username" name="owner_username" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                </div>
                                            </li>
                                        </div>
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Owner Group
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <input type="hidden" name="owner_group_id" id="owner_group_id" value="">
                                                    <input type="text" id="owner_group_name" name="owner_group_name" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Priority
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <%
                                                    ArrayList<String[]> priority_select = db.get_system_select_field.active_for_table_column(con, "projects", "priority");
                                                    %>
                                                    <select name="priority" id="priority" class="border-0">
                                                        <option value=""></option>
                                                        <%
                                                        for(int a = 0; a < priority_select.size(); a++)
                                                        {
                                                            String select_option[] = priority_select.get(a);
                                                            //select_option[3] = value    select_option[4] = label
                                                            %>
                                                            <option value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                                                            <%
                                                        }
                                                        %>
                                                    </select>
                                                </div>
                                            </li>
                                        </div>
                                        <div class="col-md-6">
                                            <li class="mb-10 clr">
                                                <label class="field-label full-width">
                                                    Status
                                                </label>
                                                <div class="formField md full-width mr-0">
                                                    <select name="status" id="status" class="border-0">
                                                        <option value=""></option>
                                                        <%
                                                        for(int a = 0; a < status_select.size(); a++)
                                                        {
                                                            String select_option[] = status_select.get(a);
                                                            //select_option[3] = value    select_option[4] = label
                                                            %>
                                                            <option value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                                                            <%
                                                        }
                                                        %>
                                                    </select>
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                </div>
                                <div id="filter_by_predefined" class="row d-none filter-category-block">
                                    <div class="col-md-12">
                                        <%
                                        Map<String, String> predefined_map = new HashMap<String, String>();
                                        predefined_map.put("all", "All");
                                        predefined_map.put("assigned_to_me", "Assigned to me");
                                        predefined_map.put("unassigned", "Unassigned");
                                        predefined_map.put("created_today", "Created today");
                                        predefined_map.put("closed_today", "Closed today");
                                        predefined_map.put("open", "Open");
                                        predefined_map.put("open_7_14", "Open 7-14 Days");
                                        predefined_map.put("open_14_30", "Open 14-30 Days");
                                        predefined_map.put("open_30_plus", "Open 30+ Days");
                                        predefined_map.put("my_critical", "My Critical");
                                        predefined_map.put("my_high", "My High");
                                        predefined_map.put("my_medium", "My Medium");
                                        predefined_map.put("my_low", "My Low");
                                        predefined_map.put("all_open_critical", "All Open Critical");
                                        predefined_map.put("all_open_high", "All Open High");
                                        predefined_map.put("all_open_medium", "All Open Medium");
                                        predefined_map.put("all_open_low", "All Open Low");
                                        for (Map.Entry<String, String> entry : predefined_map.entrySet())
                                        {
                                            String key = entry.getKey();
                                            String value = entry.getValue();
                                            %>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="predefined" id="<%=key%>" value="<%=key%>" <%=(key.equals(predefined) ? "checked" : "")%>>
                                                <label class="form-check-label" for="<%=key%>"><%=value%></label>
                                            </div>
                                            <%

                                        }
                                        %>
                                    </div>
                                </div>
                            </ul>      
                            <div class="text-center pt-5 pb-5">
                                <button class="btn md2 btn-primary-new customBtn mb-5" onclick="apply_filters()">Apply Filters</button>
                                <div class="clr"></div>
                                <a href="javascript:void(0)" onclick="resetFilters()" class="">Reset Filters</a>
                            </div>
                        </form>
                    </div>
                </div>
            </li>
            <li class="dInlineBlock">
                <a id="new_project_button" data-target="project" data-project-id="0" class="btn btn-success customBtn lg waves-effect rightSidePanelOpener" href="#">
                    <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>New Project
                </a>    
            </li>
        </ul>
    </div>
    <h1 class="large-font boldFont">Projects</h1>
</div>

<%
        String[] filter_fields = {
                 "serial_number",
                 "asset_tag",
                 "manufacturer",
                 "model",
                 "asset_type",
                 "asset_subtype",
                 "purchase_order",
                 "warranty_expire",
                 "state",
                 "state_date",
                 "assigned_to_id",
                 "assigned_group_id",
                 "department",
                 "location",
                 "site",
                 "company",
                 "ip",
                 "notes"
        };
        List<Map<String, String>> filters = new ArrayList<>();
        for (int i = 0; i < filter_fields.length; i++)
        {
            String filter_field = filter_fields[i];
            String filter_value = request.getParameter(filter_field);
            if (filter_value != null && !filter_value.isEmpty()) 
            {
                Map<String, String> map = new HashMap<String, String>();
                map.put("key", filter_field);
                map.put("value", filter_value);
                filters.add(map);
            }
        }
        request.setAttribute("filter_fields", filter_fields);

    %>
<div id="active_filters">
    <jsp:include page="active_filters_row.jsp" />
</div>

<div class="sort-tbl-holder">
    <table id="data_table" class="table table-striped custom-sort-table d-none" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>Project Title</th>
                <th>Priority</th>
                <th>Status</th>
                <th>Scheduled Start</th>
                <th>Scheduled End</th>
                <th>Project Timeline</th>
<!--                <th>Manager</th>
                <th>Owner Group</th>-->
                <!--<th>Actions</th>-->
            </tr>
        </thead>
    </table>
</div>
<jsp:include page='footer.jsp'>
    <jsp:param name="parameter" value=""/>
</jsp:include>	

<div class="rightPanelHolder">
    <div class="p-15 ticket-content">
        <span class="rightSidePanelCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
        <h4 id="rightPanelTitle" class="mediumFont large-font mb-20"></h4>
        <div id="rightPanelContent" class="clr whtbg p-15">
        </div>
    </div>
</div>

<div class="rightPanelHolder2">
    <div class="p-15 ticket-content">
        <span class="rightSidePanelCloser2"><img src="assets/images/svg/close-icon.svg" alt=""></span>
        <h4 id="rightPanelTitleChat" class="mediumFont large-font mb-20"></h4>
        <chat-app current-user-id="<%=user_id%>" show-chat="false" service-desk-endpoint="<%=props.get("servicedesk_url").toString()%>"></chat-app>
    </div>
</div>

<div class="modal fade" id="deleteAttachmentModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteModalLabel">Attachment delete confirmation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary-new" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger" onclick="delete_attachment()">Delete</button>
        </div>
      </div>
    </div>
</div>        
             
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteModalLabel">Delete Confirmation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="deleteModalBody">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger" onclick="delete_entity()" id="modalDeleteButton">Delete</button>
        </div>
      </div>
    </div>
</div>                     

<script type="text/javascript">
    function showAttachmentDeleteModal() 
    {
        var file_name = $(event.target).closest("span").html();
        var id = $(event.target).closest("a").data('id');
        var modal = $('#deleteAttachmentModal');
        modal.find("#deleteModalLalbel").html("");
        modal.modal();
    }
    function deleteAttachment()
    {
        var id = $(event.target).closest("a").data('id');
        var div = $(event.target).closest("div").replaceWith('<input type="hidden" name="attachment_delete_id" value="' + id +'"/>');
    }
    
    function showDeleteModal(delete_entity_id, entity_type) 
    {
        switch (entity_type) {
            case "catalog":
                $("#deleteModalBody").html("Are you sure you want to delete this Catalog");
                break;
            case "catalog_item":
                $("#deleteModalBody").html("Are you sure you want to delete this Catalog Item");
                break;
            default:
                break;
        }
        $("#modalDeleteButton").prop("data-id", delete_entity_id);
        $("#modalDeleteButton").prop("data-entity", entity_type);
        $('#deleteModal').modal();
    }    
    function delete_entity() 
    {
        let entity_type = $("#modalDeleteButton").prop("data-entity");
        let url = null;
        switch (entity_type) {
            case "catalog":
                url = "admin_service_catalog_delete";
                break;
            case "catalog_item":
                url = "admin_service_catalog_item_delete";
                break;
            default:
                break;
        }
        let delete_entity_id = $("#modalDeleteButton").prop("data-id");
        if (url && delete_entity_id) {
            window.location.assign(url + "?id=" + delete_entity_id);
        }
    }    

</script>

<!-- BEGIN PAGE LEVEL JS-->
<!-- BEGIN VENDOR JS-->    
<script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN MODERN JS-->
<script src="app-assets/js/core/app-menu.js"></script>
<script src="app-assets/js/core/app.js"></script>
<!-- END MODERN JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="app-assets/js/scripts/popover/popover.js"></script>

<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<!--<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>-->    
<script src="app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>    
<script src="app-assets/js/scripts/tables/datatables-extensions/dataRender/datetime.js"></script>    
<script src="assets/js/tinymce/tinymce.min.js"></script>
<script src="assets/js/bs-custom-file-input.min.js"></script>
<script src="assets/js/axios.min.js"></script>
<script src="assets/js/vue.min.js"></script>

<!--<script src='https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js'></script>-->
<!--<script src="https://cdn.jsdelivr.net/npm/vue-advanced-chat@0.8.8/dist/vue-advanced-chat.umd.min.js"></script>-->

<!--<script src='https://cdn.jsdelivr.net/npm/babel-polyfill/dist/polyfill.min.js'></script>
<script src='https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js'></script>-->

<script>
    var dt;
    var dt_childs = new Array();
    var taskDetailRows = [];    
//    var showChat = false;
//    var currentUserId = <%=user_id%>;
//    var objectType = "";
//    var objectId = 0;
//    var parentObjectType = "";
//    var parentObjectId = 0;
    
    function change_filter_category(select)
    {
        $(".filter-category-block").addClass("d-none");
        var category_block = $("#" + $(select).val());
        if (category_block)
        {
            category_block.removeClass("d-none");    
        }
    }

    function apply_filters(draw = true)
    {
        $("#filter_form input,#filter_form select").each(function() {

            var field = $(this);
//            console.log("filter_form_field", field);
            var field_name = field.attr('name');
            
            if (!field_name || field.attr('type') == "hidden" || (field.attr('type') == "radio" && !field.prop("checked")))
            {
                return;
            }
            if (field.val() && field.val() != "Any")
            {
                var label = "";
                var filter_value = "";
                if (field.attr("type") == "radio")
                {
                    label = "Predefined";
                    filter_value = field.siblings("label").html();
                } else {
                    label = field.closest("div").prev().html();
                    filter_value = field.val();
                }
                if ($("#active_filter_" + field_name).length > 0) {
                    var filter_block = $("#active_filter_" + field_name);
                    $(filter_block).children("span").html(filter_value);
                } else {
                    $("#active_filters").append(
                        $('' +
                            '<div class="clr tag dInlineBlock formField md whtbg mr-5 mb-15" id="active_filter_' + field_name + '">' +
                                '<h5 class="basic-font mediumFont">' + label + '</h5>' +
                                '<span>' + filter_value + '</span>' +
                                '<img class="icon" src="assets/images/svg/cross-icon.svg" alt="" onclick="resetFilterFieldNew(\'' + field_name + '\')">' +
                            '</div>'
                        )
                    );
                }
                setQueryStringParameter(field_name, field.val());
            } else {
                resetFilterFieldNew(field_name, false);
            }                
        });
        if(draw)
        {
            console.log("calling draw from apply_filters")
            dt.draw();
        }
    }

    function resetFilterFieldNew(field_name, draw = true)
    {
        var filter_block = $("#active_filter_" + field_name);
        if (filter_block.length > 0)
        {
            filter_block.remove();
            var filter_field = $("#filter_form input[name='" + field_name + "'],#filter_form select[name='" + field_name + "']");
            if (filter_field.attr("type") != "radio")
            {
                filter_field.val("");
            }
            if (field_name == "owner_username" || field_name == "owner_group_name")
            {
                filter_field.trigger('change');
            }
            if (draw) {
                console.log("calling draw from resetfilter")
                dt.draw();
            }
            setQueryStringParameter(field_name, "");
        }
    }

    function addRightSidePanelListeners()
    {
        $('.rightSidePanelCloser:not(.listens)').each(function(){
            $(this).addClass('listens');
            this.addEventListener("click", function()
            {
                $('.rightPanelHolder').removeClass('open');
                $('.rightSidePanel').removeClass('open');
                $('html').removeClass('panelOpen');
            }, false);
        });
        
        $('.rightSidePanelCloser2:not(.listens)').each(function(){
            $(this).addClass('listens');
            this.addEventListener("click", function()
            {
                document.querySelector('chat-app').showChat = false;
                $('.rightPanelHolder2').removeClass('open');
                $('.rightSidePanel2').removeClass('open');
                $('html').removeClass('panelOpen');
            }, false);
        });

        $('.rightSidePanelOpener2:not(.listens)').each(function(){
            $(this).addClass('listens');
            
            this.addEventListener("click", function()
            {
                $('.rightPanelHolder2').addClass('open');
                $('.rightSidePanel2').addClass('open');
                $('html').addClass('panelOpen');
                
                var chatApp = document.querySelector('chat-app');
                switch ($(this).data('target'))
                {
                    case "project":
                        chatApp.objectType = "project";
                        chatApp.objectId = $(this).data('projectId');
                        chatApp.parentObjectType = "";
                        chatApp.parentObjectId = 0;
                        break;
                    case "task":
                        chatApp.objectType = "task";
                        chatApp.objectId = $(this).data('taskId');
                        chatApp.parentObjectType = "project";
                        chatApp.parentObjectId = $(this).data('projectId');
                        break;
                }
//                console.log($(this).data('projectId'));
//                chatApp.currentUserId = 0;
                chatApp.showChat = true;
                console.log("panel2 opened");
            });
        });
        
        $('.rightSidePanelOpener:not(.listens)').each(function(){
//            console.log("attaching to ", this);
            $(this).addClass('listens');
            
            this.addEventListener("click", function()
            {
                $("#rightPanelContent").html("");

                var panel_title = "";
                var ajax = {};
                ajax.url = "";
                ajax.type = 'GET';
                ajax.dataType = "html";
                if (event.target.nodeName == "IMG")
                {
                    var clicked_node = $(event.target).closest("a");
                } else {
                    var clicked_node = $(event.target);
                }
                var target = clicked_node.data('target');
                var project_id = clicked_node.data('projectId');
                var task_id = clicked_node.data('taskId');
                console.log(clicked_node, target, project_id, task_id);

                switch (target) 
                {
                    case "chat":
                        if (project_id)
                        {
                            ajax.url = "chat.html";
                            ajax.data = {project_id: project_id};
                            panel_title = "Collaboration by Project";
                        } else {
                            ajax.url = "chat.html";
                            panel_title = "Collaboration by Task";
                        }
                        break;
                    case "project":
                        if (project_id)
                        {
                            ajax.url = "project.jsp";
                            ajax.data = {project_id: project_id};
                            panel_title = "Edit Project";
                        } else {
                            ajax.url = "project_new.jsp";
                            panel_title = "Create a New Project";
                        }
                        break;
                    case "task":
                        if (task_id)
                        {
                            ajax.data = {project_id: project_id, task_id: task_id};
                            ajax.url = "project_task_edit.jsp";
                            panel_title = "Edit Project Task";
                        } else {
                            ajax.data = {project_id: project_id};
                            ajax.url = "project_task_add.jsp";
                            panel_title = "Create a New Project Task";
                        }
                        break;
                    default:
                        break;
                }
                
                if (ajax.url === "") return false;

                ajax.error = function(jqXHR, textStatus)
                {
                    switch (jqXHR.status) {
                        case 401:
                            window.location.assign("<%=props.get("login_url").toString()%>");
                            break;
                        case 403:
                            alert("No permission!");
                            break;
                        default:
                            alert("Error processing request!");
                    }
                };

                ajax.success = function (data) 
                {                    
                    $('.rightPanelHolder').addClass('open');
                    $('.rightSidePanel').addClass('open');
                    $('html').addClass('panelOpen');

                    $("#rightPanelTitle").html(panel_title);
                    $("#rightPanelContent").html(data);
                    addRightSidePanelListeners();
                    attachFieldListeners($("#rightPanelContent"));        


                }
                
                $.ajax(ajax);
            }, false);
        });

    }
    
    window.addEventListener("load", (event) => 
    {
        addRightSidePanelListeners();
        // $('.dropdown-menu div#filter_categories li').on('click', function (e) 
        // { // prevent dropdown close on autocomplete
        //     $(".filter-category-block").addClass("d-none");
        //     var selected_category_name = $(this).children("a").html();
        //     var selected_category_id = $(this).children("a").data('category');
        //     $("#filters_dropdown_toggle").html('<span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>' + selected_category_name);
        //     var category_block = $("#"+selected_category_id);
        //     if (category_block)
        //     {
        //         category_block.removeClass("d-none");
        //         $("#filter_categories").addClass("d-none");
        //         $("#filters_dropdown_menu").css("width", "500px");
        //         $("#filter_actions").removeClass("d-none");
        //     }
           
        //     // if (e.clickEvent && e.clickEvent.target && (e.clickEvent.target.className == "ui-menu-item-wrapper" || e.clickEvent.target.className == "icon")) {
        //     //     e.preventDefault();
        //     // }
        // });

        $('#filters_dropdown').on('hide.bs.dropdown', function (e) 
        { // prevent dropdown close on autocomplete
            console.log("dropdown closed ", e);
            //            console.log(e.clickEvent.target.className);
            if (e.clickEvent && e.clickEvent.target && (e.clickEvent.target.className == "ui-menu-item-wrapper" || e.clickEvent.target.className == "icon" || e.clickEvent.target.className.indexOf("applyBtn") !== -1)) {
                e.preventDefault();
            }
        });

        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
        
        dt = $('#data_table').DataTable( {
            language: {
                "info":           "Showing _START_ to _END_ of _TOTAL_ projects",
                "infoEmpty":      "Showing 0 to 0 of 0 projects",
                "infoFiltered":   "(filtered from _MAX_ total projects)",
                "lengthMenu":     "Show _MENU_ projects",
                "zeroRecords":    "No matching projects found",
            },
            autoWidth: false,
            "processing": true,
            "serverSide": true,
            "ordering": true,
            'serverMethod': 'post',
            'searching': false,
            "lengthChange": false,
            dom: "<'row'<'col-sm-3'i><'col-sm-3'f><'col-sm-6'p>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            "ajax": {
                "url": "ajax_lookup_projects",
                'data': function(data){
                    // Read values
                    var filters = {};
                    $("#filter_form input,#filter_form select").each(function() {
                        if ($(this).attr("name") && (($(this).attr("type") == "radio" && $(this).is(':checked')) || ($(this).attr("type") != "radio" && this.value && this.value != "" && this.value.toLowerCase() != "any" && this.value.toLowerCase() != "all" && this.value != 0)))
                        {
                            data[$(this).attr('name')] = this.value;
                        }
                    });

                    data['filters'] = JSON.stringify(filters);
                }
            },
            "pageLength": 50,
            "columnDefs": [
//                <th></th>
//                <th>Project Title</th>
//                <th>Priority</th>
//                <th>Status</th>
//                <th>Schedule Start</th>
//                <th>Schedule End</th>
//                <th>Project Timeline</th>
//                <th>Manager</th>
//                <th>Owner Group</th>
//                <th>Actions</th>

                { targets: 0, "width": "1%", defaultContent: '<span class="expand-td"><img src="assets/images/svg/collapse-minus-icon.svg" alt=""></span>', orderable: false },
//                { targets: 1, "data": "id" },
                { targets: 1, "width": "30%", "data": null, name: "name",
                    render: function ( data, type, row ) 
                    {
                        if (row.timeline_overdue != "") {
                            return row.name + '<span class="icon mr-5 text-danger"><i class="las la-exclamation-circle la-2x" title="' + row.timeline_overdue + '"></i></span>';
                        } else {
                            return row.name;
                        }

                    }                    
                },
                { targets: 2, "width": "5%", name: "priority",  "data": null, 
                    render: function ( data, type, row ) 
                    {
                        var indicator_class = "";
                        switch (row.priority) {
                            case "Low":
                                indicator_class = "low";
                                break;
                            case "Medium":
                                indicator_class = "medium";
                                break;
                            case "High":
                                indicator_class = "high";
                                break;
                            case "Critical":
                                indicator_class = "critical";
                                break;
                            default:
                                indicator_class = "unknown";
                                break;
                        }
                        return '<span class="indicator ' + indicator_class + ' "></span> ' + (row.priority ? row.priority : 'Unknown');
                    }
                },
                { targets: 3, "width": "5%", "data": null, orderable: false,
                    render: function ( data, type, row ) 
                    {
                        return '<span class="btn sm state-' + row.status.toLowerCase() + ' customBtn">' + row.status + '</span>';
                    }
                },
                { targets: 4, "width": "10%", name: "scheduled_start_date", "data": "scheduled_start_date", render: $.fn.dataTable.render.moment('YYYYMMDDHHmmss','MM/DD/YYYYY') },
                { targets: 5, "width": "10%", name: "scheduled_end_date", "data": "scheduled_end_date", render: $.fn.dataTable.render.moment('YYYYMMDDHHmmss','MM/DD/YYYYY') },
                { targets: 6, "width": "20%", "data": null, orderable: false, 
                    render: function ( data, type, row ) 
                    {
                        if (row.timeline_percent > 0) {
                            return '' +
                                '<div class="progress-content clr ">' +
    //                                '<div class="txt dInlineBlock p-10 mr-5">' + row.timeline+ '</div> ' +
                                    '<div class="dInlineBlock progress w-100">' +
//                                        '<div class="progress-bar ' + (row.timeline_overdue ? "progress-bar-overdue" : "")  + '" role="progressbar" style="width: ' + row.timeline_percent + '%" aria-valuenow="' + row.timeline_percent + '" aria-valuemin="0" aria-valuemax="100"></div>' +
                                        '<div class="progress-bar" role="progressbar" style="width: ' + row.timeline_percent + '%" aria-valuenow="' + row.timeline_percent + '" aria-valuemin="0" aria-valuemax="100"></div>' +
                                    '</div>' +
                                '</div>';
                        } else {
                            return '<span class="icon mr-5 text-danger"><i class="las la-exclamation-circle la-2x" title="No Tasks Completed"></i></span>';
                        }

                    }
                },
//                { targets: 7, "data": null, 
//                    render: function ( data, type, row ) 
//                    {
//                        return '<span class="user-img dInlineBlock mr-5 user-avatar"><img src="assets/images/users/avatar-2.jpg" alt=""></span> ' + row.manager_first + ' ' + row.manager_last;
//                    }
//                },
//                { targets: 8, "data": "owner_group_name"},
//                { targets: 7, "width": "5%", orderable: false,
//                    render: function (data, type, full, meta) 
//                    {
//                        return '' +
//                            '<a href="javascript:void(0)" data-target="project" data-project-id="' + full.id + '" class="mr-5 rightSidePanelOpener2"><img src="assets/images/svg/team-icon.svg" alt=""></a>' +
//                            '<a href="javascript:void(0)" data-target="project" data-project-id="' + full.id + '" class="mr-5 rightSidePanelOpener"><img src="assets/images/svg/edit-icon.svg" alt=""></a>' +
//                            '<a href="javascript:void(0)" onclick="showDeleteModal(' + full.id + ',\'project\')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>';
//                    }
//                }
            ],
            "order": [],
            createdRow: function( row, data, dataIndex ) {
                $( row )
                    .attr('data-id', data.id)
                    // .addClass('tableSortHolder')
                    .addClass('project-row');
            },
            drawCallback: function( settings ) {
                var api = this.api();
//                if ($(api.table().node()).hasClass("d-none"))
//                {
//                    var dt_child_response = api.ajax.json();
//                    $("span.status_counter_" + project_id).each(function() {
//                        var status = $(this).attr('data-status')
//                        if (dt_child_response.statusCounters[status])
//                        {
//                            $(this).html(dt_child_response.statusCounters[status]);
//                        } else {
//                            $(this).html(0);
//                        }
//                    });
//                    $(api.table().node()).removeClass("d-none");
//                }
//
//                addRightSidePanelListeners();
                // Output the data for the visible rows to the browser's console
                    console.log( "drawCallback fired", settings);
                $(api.table().node()).removeClass("d-none");
                if (getQueryStringParameter("project_id"))
                {
                    $(".project-row[data-id='" + getQueryStringParameter("project_id") + "'] span.expand-td").trigger('click');
                }

                addRightSidePanelListeners();

            },
            "stripeClasses":[]
        } );


        var detailRows = [];
        
        $('#data_table tbody').on( 'click', 'tr.project-row span.expand-td', function (e) {
            e.stopPropagation();
            var tr = $(this).closest('tr');
//            console.log(tr);
            var row = dt.row( tr );
            var data = row.data();
            var row_id = tr.data('id');
            var idx = $.inArray( row_id, detailRows );

            if ( row.child.isShown() ) {
                row.child.hide();

                // Remove from the 'open' array
                detailRows.splice( idx, 1 );
            }
            else {
                if(row.child() && row.child().length)
                {
                    row.child.show();
                } else {
                    var out_tr =                             
                            '<tr class="expanded" id="project_details_row_' + row_id + '">' +
                                '<td colspan="7" class="first-row">' +
                                    '<div class="content-holder p-15 mt-10 mb-10">' +
                                        '<div class="row mb-30 justify-content-between">' +
                                            '<div class="col-md-5">' +
                                                '<div class="row">' +
                                                    '<div class="col-12 mb-30">' +
                                                        '<h4 class="boldFont xlarge-font mb-10">' + data.description+ '</h4>' +
                                                        '<p>' + data.notes + '</p>' +
                                                    '</div>' +
                                                '</div>' +
                                                '<div class="row">' +
                                                    '<div class="col-12">' +
                                                        '<div class="clr mb-15">' +
                                                            '<h5 class="pb-5 boldFont">Project Attachments</h5>' +
                                                            '' +
                                                         '</div>';
                                                        for (var i in data.attachments) 
                                                        {
                                                            out_tr += 
                                                            '<div class="clr tag dInlineBlock formField md whtbg mr-5 mb-15">' +
                                                                '<span>' +
                                                                    '<a title="Uploaded by ' + data.attachments[i].uploaded_by_name + ' on ' + data.attachments[i].file_date + '" href="javascript:void(0)" onclick="openAttachment();event.preventDefault" data-file-name="' + data.attachments[i].file_name +'" data-link="' + data.attachments[i].name_on_disk + '">' + data.attachments[i].file_name + '<img class="icon" src="assets/images/svg/attachment-icon.svg" alt=""></a>' +
                                                                    '</span>' +
                                                            '</div>';
                                                        }
                                                        out_tr +=
                                                    '</div>' +
                                                '</div>' +
                                            '</div>' +
                                            '<div class="col-md-6">' +
                                                '<div class="row mb-30">' +
                                                    '<div class="col-4">' +
                                                        '<label class="font-weight-bold">' +
                                                            'Project ID' +
                                                        '</label>' +
                                                        '<div>' + data.id + '</div>' +
                                                    '</div>' +
                                                    '<div class="col-4">' +
                                                        '<label class="font-weight-bold">' +
                                                            'Project Owner' +
                                                        '</label>' +
                                                        '<div><span class="user-img dInlineBlock mr-5 user-avatar"><img src="assets/images/users/avatar-2.jpg" alt=""></span>' + data.owner_first + ' ' + data.owner_last + '</div>' +
                                                    '</div>' +
                                                    '<div class="col-4">' +
                                                        '<label class="font-weight-bold">' +
                                                            'Project Owner Group' +
                                                        '</label>' +
                                                        '<div>' + data.owner_group_name + '</div>' +
                                                    '</div>' +
                                                '</div>' +
                                                '<div class="row">' +
                                                    '<div class="col-4">' +
                                                        '<label class="font-weight-bold">' +
                                                            'Project Manager' +
                                                        '</label>' +
                                                        '<div><span class="user-img dInlineBlock mr-5 user-avatar"><img src="assets/images/users/avatar-2.jpg" alt=""></span>' + data.manager_first + ' ' + data.manager_last + '</div>' +
                                                    '</div>' +
                                                    '<div class="col-4">' +
                                                        '<label class="font-weight-bold">' +
                                                            'Tech Lead' +
                                                        '</label>' +
                                                        '<div><span class="user-img dInlineBlock mr-5 user-avatar"><img src="assets/images/users/avatar-2.jpg" alt=""></span>' + data.tech_lead_first + ' ' + data.tech_lead_last + '</div>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</div>' +
                                            '<div class="col" style="max-width: 1em">' +
                                                '<div class="float-right">' +
                                                    '<a href="javascript:void(0)" data-target="project" data-project-id="' + data.id + '" class="mr-5 rightSidePanelOpener2"><img src="assets/images/svg/team-icon.svg" alt=""></a>' +
                                                    '<a href="javascript:void(0)" data-target="project" data-project-id="' + data.id + '" class="mr-5 rightSidePanelOpener"><img src="assets/images/svg/edit-icon.svg" alt=""></a>' +
                                                    '<a href="javascript:void(0)" onclick="showDeleteModal(' + data.id + ',\'project\')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>' +
                                                '</div>' +                                            
                                            '</div>' +                                            
                                        '</div>' +
                                        '<div class="mb-30 separator-new darkLighterBg"></div>' +
                                        '<div class="clr mb-30">' +
                                            '<div class="float-right text-right">' +
                                                '<ul>' +
                                                <%
                                                for(int a = 0; a < status_select.size(); a++)
                                                {
                                                    String select_option[] = status_select.get(a);
                                                    %>
                                                    '<li class="dInlineBlock valign-m pl-15">' +
                                                        '<button class="btn btn-wht md customBtn dInlineBlock" onclick="filter_tasks(this)">' +
                                                            '<strong><%=select_option[4]%>: <span class="status_counter_' + row_id + '" data-active="0" data-project-id="' + row_id + '" data-status="<%=select_option[3]%>"></span></strong>' +
                                                        '</button>' +
                                                    '</li>' +
                                                    <%
                                                }
                                                %>
                                                    '<li class="dInlineBlock valign-m pl-15">' +
                                                        '<a data-target="task" data-project-id="' + row_id + '" data-task-id="0" class="btn btn-success customBtn lg waves-effect rightSidePanelOpener" href="#">' +
                                                            '<span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>New Task' +
                                                        '</a>' +
                                                    '</li>' +
                                                '</ul>' +
                                            '</div>' +
                                            '<!-- <h5 class="pb-5 boldFont"><span class="icon mr-5"><img src="assets/images/svg/folders-icon-blk.svg" alt="s"></span>Project Title</h5>' +
                                            '<p>Our improved business 2118 Thornridge Cir. Syracuse, Connecticut 35624</p> -->' +
                                            '<h5 class="pb-5 boldFont"><span class="icon mr-5"><i class="la la-lg la-tasks"></i></span>Tasks</h5>' +
                                            '<!-- <p>Our improved business 2118 Thornridge Cir. Syracuse, Connecticut 35624</p> -->' +
                                        '</div>' +
                                        '<table id="data_table_tasks_' + row_id + '" class="table table-striped custom-sort-table d-none" cellspacing="0" width="100%">' +
                                            '<thead>' +
                                                '<tr>' +
                                                    '<th></th>' +
                                                    '<th>Task Name</th>' +
                                                    '<th>Priority</th>' +
                                                    '<th>Assigned to</th>' +
                                                    '<th>Status</th>' +
                                                    '<th>Due Date</th>' +
                                                    '<th class="text-center">Attachments</th>' +
                                                    '<th class="text-center">Notes</th>' +
//                                                    '<th class="text-center">Actions</th>' +
                                                '</tr>' +
                                            '</thead>' +
                                        '</table>' +
                                    '</div>' +
                                '</td>' +
                            '</tr>';

                    row.child( 
                        $( out_tr )                    
                    ).show();
                    showTasks(row_id);
                    // editProject(row_id);
                }
                // Add to the 'open' array
                if ( idx === -1 ) {
                    detailRows.push( row_id );
                }
            }
        } );

        apply_filters(false);
        
//        dt.on("preXhr.dt", function (e, settings, data) {
//            $("#data_table").addClass("d-none");
//        });
        
        attachFieldListeners($("#filter_form"));

        <% if (action.equals("new"))
        {
        %>    
            document.getElementById("new_project_button").dispatchEvent(new Event('click'));
        <%
        }
        %>

        <% if (!project_id.equals(""))
        {
        %>    
//            setTimeout(function()
//            { 
//                console.log(".project-row[data-id='<%=project_id%>'] span.expand-td")
//                $(".project-row[data-id='<%=project_id%>'] span.expand-td").trigger('click');                
//            }, 500);
        <%
        }
        %>
    });

    function filter_tasks(btn_clicked)
    {
        var span = $(btn_clicked).find('span[class^="status_counter"]');
        var project_id = span.data('projectId');
        var status = span.data('status');
        if (project_id && status)
        {
            if (span.data('active') == "1")
            {
                span.data('active', "0");
                $(btn_clicked).removeClass("btn-primary-new");
                $(btn_clicked).addClass("btn-wht");
            } else {
                span.data('active', "1");
                $(btn_clicked).removeClass("btn-wht");
                $(btn_clicked).addClass("btn-primary-new");
            }
        }
        dt_childs[project_id].draw();
        
    }
    
    function showTasks(project_id)
    {
        dt_childs[project_id] = 
            $('#data_table_tasks_' + project_id).DataTable( {
                autoWidth: false,
                "processing": true,
                "serverSide": true,
                "ordering": false,
                'serverMethod': 'post',
                'searching': false,
                "lengthChange": false,
                dom: "<'row'<'col-sm-12'tr>>",
                "ajax": {
                    "url": "ajax_lookup_project_tasks",
                    'data': function(data){
                        data['project_id'] = project_id;
                        var status_filters = [];
                        $("#project_details_row_" + project_id).find('span[class^="status_counter"]').each( function()
                            {
                                if ($(this).data('active') == "1")
                                {
                                    status_filters.push($(this).data('status'));
                                }
                            }
                        );
                        if (status_filters.length > 0)
                        {
                            data['status_filters'] = status_filters;
                        }
                    }
                },
                "pageLength": 50,
                "columnDefs": [
                    { targets: 0, defaultContent: '<span class="expand-td"><img src="assets/images/svg/collapse-minus-icon.svg" alt=""></span>', orderable: false },
    //                { targets: 1, "data": "id" },
                    { targets: 1, "data": "name" },
                    { targets: 2, "data": null, 
                        render: function ( data, type, row ) 
                        {
                            var indicator_class = "";
                            switch (row.priority) {
                                case "Low":
                                    indicator_class = "low";
                                    break;
                                case "Medium":
                                    indicator_class = "medium";
                                    break;
                                case "High":
                                    indicator_class = "high";
                                    break;
                                case "Critical":
                                    indicator_class = "critical";
                                    break;
                                default:
                                    indicator_class = "unknown";
                                    break;
                            }
                            return '<span class="indicator ' + indicator_class + ' "></span> ' + (row.priority ? row.priority : 'Unknown');
                        }
                    },
                    { targets: 3, "data": null, 
                        render: function ( data, type, row ) 
                        {
                            return '<span class="user-img dInlineBlock mr-5 user-avatar"><img src="assets/images/users/avatar-2.jpg" alt=""></span> ' + row.assigned_to_first + ' ' + row.assigned_to_last;
                        }
                    },
                    { targets: 4, "data": null, 
                        render: function ( data, type, row ) 
                        {
                            return '<span class="btn sm state-' + (row.status ? row.status.toLowerCase() : 'unknown') + ' customBtn">' + (row.status ? row.status : 'Unknown') + '</span>';
                        }
                    },
                    { targets: 5, "data": "scheduled_end_date", render: $.fn.dataTable.render.moment('YYYYMMDDHHmmss','MM/DD/YYYYY') },
                    { targets: 6, "data": null, className: 'text-center',
                        render: function ( data, type, row ) 
                        {
                            return '<span class="icon mr-5"><img src="assets/images/svg/attachment-icon.svg" alt=""></span> ' + row.attachments.length;
                        }
                    },
                    { targets: 7, "data": null, className: 'text-center',
                        render: function ( data, type, row ) 
                        {
                            return '<span class="icon mr-5"><img src="assets/images/svg/file-icon.svg" alt=""></span> ' + row.notes_count;
                        }
                    },
//                    { targets: 8, className: 'text-center',
//                        render: function (data, type, full, meta) 
//                        {
//                            return '' +
//                                '<a href="javascript:void(0)" data-target="task" data-project-id="' + project_id + '" data-task-id="' + full.id + '" class="mr-5 rightSidePanelOpener2"><img src="assets/images/svg/team-icon.svg" alt=""></a>' +
//                                '<a href="javascript:void(0)" data-target="task" data-project-id="' + project_id + '" data-task-id="' + full.id + '" class="mr-5 rightSidePanelOpener"><img src="assets/images/svg/edit-icon.svg" alt=""></a>' +
//                                '<a href="javascript:void(0)" onclick="showDeleteModal(' + full.id + ',\'task\')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>';
//                        }
//                    }
                ],
                "order": [],
                createdRow: function( row, data, dataIndex ) {
                    $( row )
                        .attr('data-id', data.id)
                        // .addClass('tableSortHolder')
                        .addClass('task-row');
                },
                drawCallback: function( settings ) {
                    var api = this.api();
                    if ($(api.table().node()).hasClass("d-none"))
                    {
                        var dt_child_response = api.ajax.json();
                        $("span.status_counter_" + project_id).each(function() {
                            var status = $(this).attr('data-status')
                            if (dt_child_response.statusCounters[status])
                            {
                                $(this).html(dt_child_response.statusCounters[status]);
                            } else {
                                $(this).html(0);
                            }
                        });
                        $(api.table().node()).removeClass("d-none");
                    }
                    if (getQueryStringParameter("project_id") && getQueryStringParameter("task_id"))
                    {
                        $("#data_table_tasks_" + getQueryStringParameter("project_id") + " .task-row[data-id='" + getQueryStringParameter("task_id") + "'] span.expand-td").trigger('click');                
                    }
                    addRightSidePanelListeners();
                    // Output the data for the visible rows to the browser's console
//                    console.log( api.rows( {page:'current'} ).data() );
                    
                },
                "stripeClasses":[]
            } );

//        dt_childs[project_id].on( 'draw', function () {
//            console.log(dt_childs[project_id]);
//            $(dt_childs[project_id].table().node()).removeClass("d-none");
//            console.log(this.api());
//        } );

        $('#data_table_tasks_' + project_id + ' tbody').on( 'click', 'tr.task-row span.expand-td', function () {
            var tr = $(this).closest('tr');

            var row = dt_childs[project_id].row( tr );
            var data = row.data();
            var row_id = tr.data('id');
            var idx = $.inArray( row_id, taskDetailRows );
            
            if ( row.child.isShown() ) {
                row.child.hide();

                // Remove from the 'open' array
                taskDetailRows.splice( idx, 1 );
            }
            else {
                if(row.child() && row.child().length)
                {
                    row.child.show();
                } else {
                    var out_tr =  
                        '<tr class="expanded" id="project_details_row_' + row_id + '">' +
                            '<td colspan="8" class="first-row">' +
                                '<div class="content-holder p-15 mt-10 mb-10">' +
                                    '<div class="row mb-30 justify-content-between">' +
                                        '<div class="col-md-5">' +
                                            '<h4 class="boldFont xlarge-font mb-10">' + data.description+ '</h4>' +
                                            '<p>' + data.notes + '</p>' +
                                        '</div>' +
                                        '<div class="col-md-6">';
                                            for (var i in data.attachments) 
                                            {
                                                out_tr += 
                                                '<div class="clr tag dInlineBlock formField md whtbg mr-5 mb-15">' +
                                                    '<span>' +
                                                        '<a title="Uploaded by ' + data.attachments[i].uploaded_by_name + ' on ' + data.attachments[i].file_date + '" href="javascript:void(0)" onclick="openAttachment();event.preventDefault" data-file-name="' + data.attachments[i].file_name +'" data-link="' + data.attachments[i].name_on_disk + '">' + data.attachments[i].file_name + '<img class="icon" src="assets/images/svg/attachment-icon.svg" alt=""></a>' +
                                                    '</span>' +
                                                '</div>';
                                            }
                                            out_tr +=                                            
                                        '</div>' +
                                        '<div class="col" style="max-width: 1em">' +
                                            '<div class="float-right">' +
                                                '<a href="javascript:void(0)" data-target="task" data-project-id="' + project_id + '" data-task-id="' + data.id + '" class="mr-5 rightSidePanelOpener2"><img src="assets/images/svg/team-icon.svg" alt=""></a>' +
                                                '<a href="javascript:void(0)" data-target="task" data-project-id="' + project_id + '" data-task-id="' + data.id + '" class="mr-5 rightSidePanelOpener"><img src="assets/images/svg/edit-icon.svg" alt=""></a>' +
                                                '<a href="javascript:void(0)" onclick="showDeleteModal(' + data.id + ',\'task\')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>' +
                                            '</div>' +
                                        '</div>' +                                                                                    
                                    '</div>' +
                                '</div>' +
                            '</td>' +
                        '</tr>';
                    row.child($(out_tr)                    
                    ).show();
                    // editProject(row_id);
                    addRightSidePanelListeners();
                }
                // Add to the 'open' array
                if ( idx === -1 ) {
                    taskDetailRows.push( row_id );
                }
            }
        } );

        dt_childs[project_id].on( 'draw', function () {
            $.each( taskDetailRows, function ( i, id ) {
                $('#'+id+' span.expand-td').trigger( 'click' );
            } );
        } );

    }
    
    const axiosInstance = axios.create();
    axiosInstance.interceptors.request.use(function (config) {
        // Do something before request is sent
        if (config.url != 'ajax_get_jwt')
        {
            config.responseType = 'arraybuffer'
        }
        return config;
    }, function (error) {
        // Do something with request error
        return Promise.reject(error);
    });
    
    axiosInstance.interceptors.response.use((response) => {
        return response
    },  function (error) {
            const originalRequest = error.config;
            if (error.response)
            {
                if (error.response.status === 401 && originalRequest.url.indexOf('ajax_get_jwt') !== -1 ) {
                    router.push('/login');
                    return Promise.reject(error);
                }

                if (error.response.status === 401 && !originalRequest._retry) {
                    originalRequest._retry = true;
                    return axiosInstance.get('ajax_get_jwt')
                        .then(res => {
                            if (res.status === 200) {
                                axiosInstance.defaults.headers.common['Authorization'] = 'Bearer ' + res.data.token;
                                return axiosInstance(originalRequest);
                            }
                        })
                }
            } else {
                console.log("axiosInstance error", error);
            }
            return Promise.reject(error);
    });
    
    function openAttachment(url, file_name)
    {
        let file = ''
        if (typeof url == 'undefined' || typeof file_name == 'undefined')
        {
            let name_on_disk = event.target.getAttribute('data-link');
            file_name = event.target.getAttribute('data-file-name');
            file = '<%=props.get("couchdb.attachments_host")%>/<%=props.get("couchdb.name")%>/' + name_on_disk;
        } else {
            file = url
        }        
        if (file != '')
        {            
            axiosInstance.get(file).then((response) => {
                console.log(response)

                let blob = new Blob([response.data], { type: response.headers['content-type'] })
                let link = document.createElement('a')
                link.href = window.URL.createObjectURL(blob)
                link.download = file_name
                link.click()
                link.remove(); 
                window.URL.revokeObjectURL(link);

            });
        }
    }
        
    function saveProject()
    {
        var form = $(event.target).closest("form").get(0);
        $.ajax({
            url: form.action,
            type: "POST",
            data: new FormData(form),
            processData: false,
            contentType: false,
            success: function(){
                dt.draw();
            },
            error: function(jqXHR, textStatus)
            {
                switch (jqXHR.status) {
                    case 401:
                        window.location.assign("<%=props.get("login_url").toString()%>");
                        break;
                    case 403:
                        alert("No permission!");
                        break;
                    default:
                        alert("Error processing request! " + textStatus);
                }
            }
        });
    }

    function saveProjectTask()
    {
        var form = $(event.target).closest("form").get(0);
        var project_id = form.elements['project_id'].value;
        $.ajax({
            url: form.action,
            type: "POST",
            data: new FormData(form),
            processData: false,
            contentType: false,
            success: function(){
                dt_childs[project_id].draw();
            },
            error: function(jqXHR, textStatus)
            {
                switch (jqXHR.status) {
                    case 401:
                        window.location.assign("<%=props.get("login_url").toString()%>");
                        break;
                    case 403:
                        alert("No permission!");
                        break;
                    default:
                        alert("Error processing request! " + textStatus);
                }
            }
        });
    }

    function attachFieldListeners(container)
    {
        tinymce.remove('textarea');
        tinymce.init({
            selector: '.ckeditor',
            setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                });
            },
            height: 250,
            plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });

        bsCustomFileInput.init();
        
          container.find('input[name="scheduled_start_date"]').daterangepicker({
              "autoUpdateInput": false,
              "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
          container.find('input[name="scheduled_start_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });
          container.find('input[name="scheduled_start_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });

          container.find('input[name="actual_start_date"]').daterangepicker({
              "autoUpdateInput": false,
              "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
          container.find('input[name="actual_start_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });
          container.find('input[name="actual_start_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });

          container.find('input[name="actual_end_date"]').daterangepicker({
              "autoUpdateInput": false,
              "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
          container.find('input[name="actual_end_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });
          container.find('input[name="actual_end_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });

          container.find('input[name="scheduled_end_date"]').daterangepicker({
              "autoUpdateInput": false,
              "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
          container.find('input[name="scheduled_end_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });

          container.find('input[name="scheduled_end_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });

        //owner
        container.find('input[name="owner_username"]').on('change', function() {
            if (this.value == "") {
                container.find('input[name="owner_id"]').val("");
            }
        });

        container.find('input[name="owner_group_name"]').on('change', function() {
            if (this.value == "") {
                container.find('input[name="owner_group_id"]').val("");
            }
        });
        
        container.find('input[name="owner_username"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="owner_username"]').val(ui.item.label); // display the selected text
                container.find('input[name="owner_id"]').val(ui.item.value); // save selected id to input
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });
        //caller_group_name
        container.find('input[name="owner_group_name"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_group",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="owner_group_name"]').val(ui.item.label); // display the selected text
                container.find('input[name="owner_group_id"]').val(ui.item.value); // save selected id to input
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });
        //manager
        container.find('input[name="manager_username"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="manager_username"]').val(ui.item.label); // display the selected text
                container.find('input[name="manager_id"]').val(ui.item.value); // save selected id to input
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });
        
        container.find('input[name="assigned_to_id_username"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="assigned_to_id_username"]').val(ui.item.label); // display the selected text
                container.find('input[name="assigned_to_id"]').val(ui.item.value); // save selected id to input
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });
        //caller_group_name
        container.find('input[name="assigned_group_name"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_group",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="assigned_group_name"]').val(ui.item.label); // display the selected text
                container.find('input[name="assigned_group_id"]').val(ui.item.value); // save selected id to input
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });
        
        //manager
        container.find('input[name="tech_lead_username"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="tech_lead_username"]').val(ui.item.label); // display the selected text
                container.find('input[name="tech_lead_id"]').val(ui.item.value); // save selected id to input
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });

    }    

    function update_subcategory()
    {
        var var_cat_id = document.getElementById("category_id").value;
        var url = "ajax_lookup_subcategory?category_id=" + var_cat_id;
        var select = document.getElementById('subcategory');
        document.getElementById('subcategory').options.length = 0;
        $.get('ajax_lookup_subcategory?category_id=' + var_cat_id, function(data) 
        {
            for(var i = 0; i < data.length; i++) 
            {
                var obj = data[i];
                var name = obj.name;  
                var opt = document.createElement('option');
                    opt.value = name;
                    opt.innerHTML = name;
                    select.appendChild(opt);
            }            
        });
    }

</script>
    
<script>
function clear_caller() 
{ 
    document.getElementById("caller_id").value = "";
    document.getElementById("caller_name").value = "";
}
function clear_assigned_to() 
{ 
    document.getElementById("assigned_to_id").value = "";
    document.getElementById("assigned_to_name").value = "";
}
function clear_create_by() 
{ 
    document.getElementById("create_by_id").value = "";
    document.getElementById("create_by_name").value = "";
}
function clear_caller_group() 
{ 
    document.getElementById("caller_group_id").value = "";
    document.getElementById("caller_group_name").value = "";
}
function clear_assigned_group() 
{ 
    document.getElementById("assigned_group_id").value = "";
    document.getElementById("assigned_group_name").value = "";
}
</script>
<script>
    function hide_show_div() 
    {
        var x = document.getElementById("selectors");
        if (x.style.display === "none") 
        {
            x.style.display = "block";
        } 
        else 
        {
            x.style.display = "none";
        }
    }        

</script>
<script src="assets/js/chat_app.js"></script>

    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%      con.close();
        }//end if not permission
    }//end if not logged in
%>
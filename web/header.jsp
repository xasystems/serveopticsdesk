<%@page import="org.apache.commons.io.FileUtils"%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.util.List"%>
<%@page import="java.util.stream.Collectors"%>
<%@page import="java.nio.file.Paths"%>
<%@page import="java.nio.file.Files"%>
<%@page import="java.util.stream.Stream"%>
<%@page import="java.nio.file.Path"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="org.apache.commons.codec.binary.Base64"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.File"%>
<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : header
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.LinkedHashMap"%>

<% 
Logger logger = LogManager.getLogger();
            
String session_username = "";
String session_user_first = "";
String session_user_last = "";
String session_user_id = "";
String session_self_service_only = "";
try
{
    session_username = session.getAttribute("username").toString();
    session_user_id = session.getAttribute("user_id").toString();
    session_user_first = session.getAttribute("user_first").toString();
    session_user_last = session.getAttribute("user_last").toString();
    session_self_service_only = session.getAttribute("self_service_only").toString();
    if (session_user_first.equals("")) 
    {
        session_user_first = " ";
    }
    if (session_user_last.equals("")) 
    {
        session_user_last = " ";
    }
}
catch(Exception e)
{
    session_username = "";
}    
%>    
<html>

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Serve Optics</title>
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App Icons -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- Basic Css files -->
        <link href="assets/css/app_theme.css" rel="stylesheet" type="text/css">
        <link href="assets/css/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="app-assets/fonts/line-awesome/css/line-awesome.min.css">
        <!--<link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.css">-->

        <!-- Daterange -->
        <link rel="stylesheet" type="text/css" media="all" href="assets/plugins/daterange/daterangepicker.css" />

    </head>

    <%
        String body_addon_class = "";
        if (request.getRequestURI().contains("/home_service_catalog.jsp") || request.getRequestURI().contains("/service_catalog"))
        {
            body_addon_class = "service-catalog-page";
        } 


    %>
    <body class="fixed-left <%=body_addon_class%>" style="/*overflow: visible;*/">



        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">
                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="">
                        <a href="<%=session.getAttribute("home_page")%>" class="logo text-center"><img src="assets/images/serveoptics-logo.png" alt=""></a>
                        <!--<a href="index.html" class="logo"><img src="assets/images/logo.png" height="14" alt="logo"></a>-->
                    </div>
                </div>

                <nav class="navbar-custom">


                    <ul class="list-inline float-right mb-0">
                        <!-- User-->
                        <li class="list-inline-item dropdown notification-list">
                            <div class="dInlineBlock menuHolder">
                                <a class="nav-link dropdown-toggle waves-effect nav-user " data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <div class="img-holder rounded-circle">
                                        <% 
                                        if (session.getAttribute("avatar") != null && !session.getAttribute("avatar").equals(""))
                                        {
                                        %>
                                            <img src="<%=session.getAttribute("avatar")%>" alt="user" class="rounded-circle">                                        
                                        <%
                                        } else {
                                        %>
                                            <span class="nick"><%=(Character.toString(session_user_first.charAt(0))+Character.toString(session_user_last.charAt(0)))%></span>            
                                        <%
                                        } 
                                        %>
                                    </div>
                                    <span><%=session_username%></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-dropdown right--15">
                                    <% if (session_self_service_only.equals("false")) { %>
                                        <a class="dropdown-item" href="edit_profile.jsp">Profile</a>
                                        <div class="dropdown-divider"></div>
                                    <% } %>
                                    <a class="dropdown-item" href="logout">Logout</a>
                                </div>
                            </div>

                        </li>
                    </ul>

                    <!-- Menu Collapse Button -->
                    <button type="button" class="button-menu-mobile open-left waves-effect">
                        <img src="assets/images/svg/sidebar-icon-new.svg" alt="">
                    </button>
                    <!-- <button type="button" class="rightSidebarOpener open-left waves-effect">
                        <img src="assets/images/svg/sidebar-icon-new.svg" alt="">
                    </button> -->
                    <% if (session_self_service_only.equals("false")) { %>
                        <div class="clr searchFieldHolder dInline formField">
                            <span class="icon"><img src="assets/images/svg/searchIcon.svg" alt=""></span>
                            <input id="" type="text" dir="ltr" placeholder="Find anything...">
                        </div>
                    <% } %>
                    <div class="clearfix"></div>
                </nav>

            </div>
            <!-- Top Bar End -->


<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String FINANCIAL = session.getAttribute("financial").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String CONTACT = session.getAttribute("contact").toString();
        String CX = session.getAttribute("cx").toString();
        String SURVEY = session.getAttribute("survey").toString();
        String SLA = session.getAttribute("sla").toString();        
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        String METRIC = session.getAttribute("metric").toString();
        String ASSET = session.getAttribute("asset").toString();
        String USER = session.getAttribute("user").toString();
        
        if(!ADMINISTRATION.equalsIgnoreCase("true") && !MANAGER.equalsIgnoreCase("true"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);  
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
    %>
    <jsp:include page='header.jsp' />

    <jsp:include page='menu_service_desk.jsp' />

    <!-- BEGIN PAGE LEVEL CSS-->
    <style>
    .dropdown-menu.md {
        width: 500px;
    }
    </style>
    <!-- END PAGE LEVEL CSS-->
  
    <div class="clr whtbg p-10 mb-15">
        <div class="float-right options">
            <ul>
                <li class="dInlineBlock dropdown ">
                    <div class="dInlineBlock clr menuHolder">
                        <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                        </a>
                        <div class="dropdown-menu dropdown-menu-right md p-10">
                            <h5 class="medium-font boldFont pt-5 mb-10">All Filters</h5>
                            <form id="filters_form">
                                <ul>
                                <div class="row">
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <div class="">
                                                <label class="field-label full-width">Name</label>
                                                <div class="formField md full-width mr-0">
                                                    <input class="no-border full-width" type="text" name="name" value="<%=(request.getParameter("name") != null?request.getParameter("name"):"")%>">
                                                </div>
                                            </div>
                                        </li>
                                    </div>
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <div class="">
                                                <label class="field-label full-width">Description</label>
                                                <div class="formField md full-width mr-0">
                                                    <input class="no-border full-width" type="text" name="description" value="<%=(request.getParameter("description") != null?request.getParameter("description"):"")%>">
                                                </div>
                                            </div>
                                        </li>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Administration</label>
                                            <div class="formField md full-width mr-0">
                                                <select class="no-border full-width" name="administration" >
                                                    <%=support.html_utils.generate_select_options(new String[]{"", "false", "true"}, request.getParameter("administration"))%>
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                                    <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Manager</label>
                                            <div class="formField md full-width mr-0">
                                                <select class="no-border full-width" name="manager" >
                                                    <%=support.html_utils.generate_select_options(new String[]{"", "false", "true"}, request.getParameter("manager"))%>
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>
                                <div class="row">
                                    <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Financial</label>
                                            <div class="formField md full-width mr-0">
                                                <select class="no-border full-width" name="financial" >
                                                    <%=support.html_utils.generate_select_options(new String[]{"", "none", "read", "create", "update", "delete"}, request.getParameter("financial"))%>        
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                                    <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Incident</label>
                                            <div class="formField md full-width mr-0">
                                                <select class="no-border full-width" name="incident" >
                                                    <%=support.html_utils.generate_select_options(new String[]{"", "none", "read", "create", "update", "delete"}, request.getParameter("incident"))%>           
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>
                                <div class="row">
                                    <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Request</label>
                                            <div class="formField md full-width mr-0">
                                                <select class="no-border full-width" name="request" >
                                                    <%=support.html_utils.generate_select_options(new String[]{"", "none", "read", "create", "update", "delete"}, request.getParameter("request"))%>           
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                                <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Contact</label>
                                            <div class="formField md full-width mr-0">
                                                <select class="no-border full-width" name="contact" >
                                                    <%=support.html_utils.generate_select_options(new String[]{"", "none", "read", "create", "update", "delete"}, request.getParameter("contact"))%>           
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>
                                <div class="row">
                                    <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">CX</label>
                                            <div class="formField md full-width mr-0">
                                                <select class="no-border full-width" name="cx" >
                                                    <%=support.html_utils.generate_select_options(new String[]{"", "none", "read", "create", "update", "delete"}, request.getParameter("cx"))%>           
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                                    <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Survey</label>
                                            <div class="formField md full-width mr-0">
                                                <select class="no-border full-width" name="survey" >
                                                    <%=support.html_utils.generate_select_options(new String[]{"", "none", "read", "create", "update", "delete"}, request.getParameter("survey"))%>           
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>
                                <div class="row">
                                    <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">SLA</label>
                                            <div class="formField md full-width mr-0">
                                                <select class="no-border full-width" name="sla" >
                                                    <%=support.html_utils.generate_select_options(new String[]{"", "none", "read", "create", "update", "delete"}, request.getParameter("sla"))%>           
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                                    <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Project</label>
                                            <div class="formField md full-width mr-0">
                                                <select class="no-border full-width" name="project" >
                                                    <%=support.html_utils.generate_select_options(new String[]{"", "none", "read", "create", "update", "delete"}, request.getParameter("project"))%>           
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>
                                <div class="row">
                                    <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Job</label>
                                            <div class="formField md full-width mr-0">
                                                <select class="no-border full-width" name="job" >
                                                    <%=support.html_utils.generate_select_options(new String[]{"", "none", "read", "create", "update", "delete"}, request.getParameter("job"))%>           
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                                    <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Metric</label>
                                            <div class="formField md full-width mr-0">
                                                <select class="no-border full-width" name="metric" >
                                                    <%=support.html_utils.generate_select_options(new String[]{"", "none", "read", "create", "update", "delete"}, request.getParameter("metric"))%>           
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>
                                <div class="row">
                                    <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Asset</label>
                                            <div class="formField md full-width mr-0">
                                                <select class="no-border full-width" name="asset" >
                                                    <%=support.html_utils.generate_select_options(new String[]{"", "none", "read", "create", "update", "delete"}, request.getParameter("asset"))%>           
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                                    <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Users</label>
                                            <div class="formField md full-width mr-0">
                                                <select class="no-border full-width" name="user" >
                                                    <%=support.html_utils.generate_select_options(new String[]{"", "none", "read", "create", "update", "delete"}, request.getParameter("user"))%>           
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>
                                <div class="row">
                                    <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Self service</label>
                                            <div class="formField md full-width mr-0">
                                                <select class="no-border full-width" name="self_service" >
                                                    <%=support.html_utils.generate_select_options(new String[]{"", "none", "read", "create", "update", "delete"}, request.getParameter("self_service"))%>           
                                                </select>
                                            </div>
                                        </div>
                                    </li>                                    
                                </div>
                            </div>
                                </ul>
                                <div class="text-center pt-5 pb-5">
                                    <button class="btn md2 btn-primary-new customBtn mb-5">Apply Filters</button>
                                    <div class="clr"></div>
                                    <a href="javascript:void(0)" onclick="resetFilters()" class="">Reset Filters</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </li>
                <li class="dInlineBlock">
                    <span class="btn btn-success customBtn lg waves-effect rightSidePanelOpener">
                        <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>Add Role
                    </span>    
                </li>
            </ul>
        </div>
        <h1 class="large-font boldFont">User Roles</h1>
    </div>

    <%
    if (session.getAttribute("alert") != null) 
    {
        String alert = session.getAttribute("alert").toString();
        session.removeAttribute("alert");

        String alert_class = "alert-info";
        if (session.getAttribute("alert-class") != null) 
        {
            alert_class = session.getAttribute("alert-class").toString();
            session.removeAttribute("alert-class");
        }

    %>
    <div id="session_alert" class="clr whtbg p-10 mb-15">
        <div class="alert <%=alert_class%>" role="alert">
            <%=alert%>
        </div>
    </div>    
    <script type="text/javascript">
        setTimeout(function(){ document.getElementById('session_alert').classList.add("d-none"); }, 3000);    
    </script>
    <%
    }
    %>

    <%
        String[] filter_fields = {
            "name",
            "description",
            "administration",
            "manager",
            "financial",
            "incident",
            "request",
            "contact",
            "cx",
            "survey",
            "sla",
            "project",
            "job",
            "metric",
            "asset",
            "user",
            "self_service",
        };
        List<Map<String, String>> filters = new ArrayList<>();
        for (int i = 0; i < filter_fields.length; i++)
        {
            String filter_field = filter_fields[i];
            String filter_value = request.getParameter(filter_field);
            if (filter_value != null && !filter_value.isEmpty()) 
            {
                Map<String, String> map = new HashMap<String, String>();
                map.put("key", filter_field);
                map.put("value", filter_value);
                filters.add(map);
            }
        }

    %>    

    <%
        request.setAttribute("filter_fields", filter_fields);
    %>
    <jsp:include page="active_filters_row.jsp" />

    <!-- start content here-->
    <div class="sort-tbl-holder">
        <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
            <%
            ArrayList <String[]> all_system_roles =  db.get_roles.all_filtered(con, filters);
            for(int a = 0; a < all_system_roles.size();a++)
            {
                String value[] = all_system_roles.get(a);
                //id=0 name=1 description=2 created=3 created_by_id=4 administration=5 manager=6 financial=7 incident=8 request=9 contact=10 cx=11 survey=12 sla=13 project=14 job=15 
                %>

                <tr data-toggle="collapse" data-target="#role_data_<%=a%>" class="tableSortHolder accordion-toggle" aria-expanded="true">
                    <td width="10%">
                        <span class="expand-td">
                            <img src="assets/images/svg/collapse-minus-icon.svg" alt="">
                        </span>
                    </td>
                    <td width="30%">
                        <%=value[1]%>
                    </td>
                    <td width="50%">
                        <%=value[2]%>
                    </td>
                    <%
                    if(value[0].equalsIgnoreCase("0") || value[0].equalsIgnoreCase("1") || value[0].equalsIgnoreCase("2") || value[0].equalsIgnoreCase("3"))
                    {
                    %>
                        <td width="10%">
                            <a href="javascript:void(0)" onclick="event.stopPropagation();" class="mr-5 disabled" title="System Role can not edit"><img src="assets/images/svg/edit-icon.svg" alt=""></a>
                            <a href="javascript:void(0)" onclick="event.stopPropagation();" class="mr-5 disabled" title="System Role can not delete"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>
                        </td>
                    <% } else { %>
                        <td width="10%">
                            <a href="javascript:void(0)" onclick="event.stopPropagation(); editRole(<%=value[0]%>);" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>
                            <a href="javascript:void(0)" onclick="event.stopPropagation(); showDeleteModal(<%=value[0]%>)" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>
                        </td>           
                    <% }%>
                    
                </tr>
                <tr class="expanded accordian-body -collapse <%=(a==0?"show":"collapse")%>" id="role_data_<%=a%>" aria-expanded="false" style="height: 0px;">
                    <td colspan="4" class="first-row hiddenRow">
                        <div class="content-holder p-15 mt-10 mb-10  ">
                            <h5 class="mb-15 boldFont">User Role</h5>
                            <form>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="field-title">
                                            <label class="formLabel md has-txt-only mb-15">
                                                Administration
                                            </label>
                                        </div>
                                        <div class="formField clr md mb-15">
                                            <input type="text" value="<%=value[5]%>" class="no-border" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="field-title">
                                            <label class="formLabel md has-txt-only mb-15">
                                                Manager
                                            </label>
                                        </div>
                                        <div class="formField clr md mb-15">
                                            <input type="text" value="<%=value[6]%>" class="no-border" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="field-title">
                                            <label class="formLabel md has-txt-only mb-15">
                                                Financial
                                            </label>
                                        </div>
                                        <div class="formField clr md mb-15">
                                            <input type="text" value="<%=value[7]%>" class="no-border" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="field-title">
                                            <label class="formLabel md has-txt-only mb-15">
                                                Incident
                                            </label>
                                        </div>
                                        <div class="formField clr md mb-15">
                                            <input type="text" value="<%=value[8]%>" class="no-border" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="field-title">
                                            <label class="formLabel md has-txt-only mb-15">
                                                Request
                                            </label>
                                        </div>
                                        <div class="formField clr md mb-15">
                                            <input type="text" value="<%=value[9]%>" class="no-border" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="field-title">
                                            <label class="formLabel md has-txt-only mb-15">
                                                Contact
                                            </label>
                                        </div>
                                        <div class="formField clr md mb-15">
                                            <input type="text" value="<%=value[10]%>" class="no-border" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="field-title">
                                            <label class="formLabel md has-txt-only mb-15">
                                                CX
                                            </label>
                                        </div>
                                        <div class="formField clr md mb-15">
                                            <input type="text" value="<%=value[11]%>" class="no-border" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="field-title">
                                            <label class="formLabel md has-txt-only mb-15">
                                                Survey
                                            </label>
                                        </div>
                                        <div class="formField clr md mb-15">
                                            <input type="text" value="<%=value[12]%>" class="no-border" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="field-title">
                                            <label class="formLabel md has-txt-only mb-15">
                                                SLA
                                            </label>
                                        </div>
                                        <div class="formField clr md mb-15">
                                            <input type="text" value="<%=value[13]%>" class="no-border" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="field-title">
                                            <label class="formLabel md has-txt-only mb-15">
                                                Project
                                            </label>
                                        </div>
                                        <div class="formField clr md mb-15">
                                            <input type="text" value="<%=value[14]%>" class="no-border" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="field-title">
                                            <label class="formLabel md has-txt-only mb-15">
                                                Job
                                            </label>
                                        </div>
                                        <div class="formField clr md mb-15">
                                            <input type="text" value="<%=value[15]%>" class="no-border" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="field-title">
                                            <label class="formLabel md has-txt-only mb-15">
                                                Metric
                                            </label>
                                        </div>
                                        <div class="formField clr md mb-15">
                                            <input type="text" value="<%=value[16]%>" class="no-border" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="field-title">
                                            <label class="formLabel md has-txt-only mb-15">
                                                Asset
                                            </label>
                                        </div>
                                        <div class="formField clr md mb-15">
                                            <input type="text" value="<%=value[17]%>" class="no-border" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="field-title">
                                            <label class="formLabel md has-txt-only mb-15">
                                                Users
                                            </label>
                                        </div>
                                        <div class="formField clr md mb-15">
                                            <input type="text" value="<%=value[18]%>" class="no-border" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="field-title">
                                            <label class="formLabel md has-txt-only mb-15">
                                                Self Service
                                            </label>
                                        </div>
                                        <div class="formField clr md mb-15">
                                            <input type="text" value="<%=value[19]%>" class="no-border" placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </td>
                </tr>
                <%
                }                                    
                %>

            </tbody>
        </table>
    </div>
                                    
    <div class="pt-15 whtbg p-15">
        <h5 class="basic-font boldFont mb-10">Role permissions:</h5>
        <p>Administration permission grants full access to everything. Overrides all other permissions.<br>
            Manager permission has full access to everything Except Administration functions.</p>
        <ul>
            <li class="pb-5"><strong>None</strong> = No Access to the data</li>
            <li class="pb-5"><strong>Read</strong> = Able to view the data but can not control any data functions</li>
            <li class="pb-5"><strong>Create</strong> = Able to create new records, inherits Read permission</li>
            <li class="pb-5"><strong>Update</strong> = Able to update records, inherits Read and Create permission</li>
            <li class="pb-5"><strong>Delete</strong> = Able to delete records, inherits Read, Create, and Update permission</li>
        </ul>

    </div>
    <!-- end content here-->
    <jsp:include page='footer.jsp' />
    
    <div class="rightPanelHolder">
        <div class="p-15 ticket-content">
            <span class="rightSidePanelCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
            <h4 id="right_panel_title" class="mediumFont large-font mb-20">Add User Role</h4>

            <div class="clr whtbg p-15 ">
                <form action="admin_roles_add" name="add_form" id="add_form" class="add-project-form needs-validation" novalidate>  
                    <input type="hidden" name="id" value=""/>
                    <h4 class="mediumFont large-font mb-20">Enter User Role Details</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    User Role Name
                                </label>
                            </div>
                            <div class="formField border-0 clr md px-0">
                                <input type="text" value="" class="form-control" placeholder="Name" name="name" required="">
                                <div class="invalid-feedback">Please fill this field</div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Description
                                </label>
                            </div>
                            <div class="formField border-0 clr md px-0">
                                <input type="text" value="" class="form-control" placeholder="Description" name="description" required>
                                <div class="invalid-feedback">Please fill this field</div>
                            </div>
                        </div>

                    </div>
                    <div class="mt-30 mb-30 separator-new darkLighterBg"></div>
                    <h4 class="mediumFont large-font mb-20">Select Permissions for this User Role</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Administration
                                </label>
                            </div>
                            <div class="formField clr md mb-15">
                                <select class="no-border" name="administration">
                                    <option>false</option>
                                    <option>true</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Manager
                                </label>
                            </div>
                            <div class="formField clr md mb-15">
                                <select class="no-border" name="manager">
                                    <option>true</option>
                                    <option>false</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Financial
                                </label>
                            </div>
                            <div class="formField clr md mb-15">
                                <select name="financial" class="no-border">
                                    <option selected="" value="none">None</option>
                                    <option value="read">Read</option>
                                    <option value="create">Create</option>
                                    <option value="update">Update</option>
                                    <option value="delete">Delete</option>                                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Incident
                                </label>
                            </div>
                            <div class="formField clr md mb-15">
                                <select name="incident" class="no-border">
                                    <option selected="" value="none">None</option>
                                    <option value="read">Read</option>
                                    <option value="create">Create</option>
                                    <option value="update">Update</option>
                                    <option value="delete">Delete</option>                                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Request
                                </label>
                            </div>
                            <div class="formField clr md mb-15">
                                <select name="param_request"  class="no-border">
                                    <option selected="" value="none">None</option>
                                    <option value="read">Read</option>
                                    <option value="create">Create</option>
                                    <option value="update">Update</option>
                                    <option value="delete">Delete</option>                                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Contact
                                </label>
                            </div>
                            <div class="formField clr md mb-15">
                                <select name="contact" class="no-border">
                                    <option selected="" value="none">None</option>
                                    <option value="read">Read</option>
                                    <option value="create">Create</option>
                                    <option value="update">Update</option>
                                    <option value="delete">Delete</option>                                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    CX
                                </label>
                            </div>
                            <div class="formField clr md mb-15">
                                <select name="cx" class="no-border">
                                    <option selected="" value="none">None</option>
                                    <option value="read">Read</option>
                                    <option value="create">Create</option>
                                    <option value="update">Update</option>
                                    <option value="delete">Delete</option>                                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Survey
                                </label>
                            </div>
                            <div class="formField clr md mb-15">
                                <select name="survey" class="no-border">
                                    <option selected="" value="none">None</option>
                                    <option value="read">Read</option>
                                    <option value="create">Create</option>
                                    <option value="update">Update</option>
                                    <option value="delete">Delete</option>                                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    SLA
                                </label>
                            </div>
                            <div class="formField clr md mb-15">
                                <select name="sla" class="no-border">
                                    <option selected="" value="none">None</option>
                                    <option value="read">Read</option>
                                    <option value="create">Create</option>
                                    <option value="update">Update</option>
                                    <option value="delete">Delete</option>                                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Project
                                </label>
                            </div>
                            <div class="formField clr md mb-15">
                                <select name="project" class="no-border">
                                    <option selected="" value="none">None</option>
                                    <option value="read">Read</option>
                                    <option value="create">Create</option>
                                    <option value="update">Update</option>
                                    <option value="delete">Delete</option>                                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Job
                                </label>
                            </div>
                            <div class="formField clr md mb-15">
                                <select name="job" class="no-border">
                                    <option selected="" value="none">None</option>
                                    <option value="read">Read</option>
                                    <option value="create">Create</option>
                                    <option value="update">Update</option>
                                    <option value="delete">Delete</option>                                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Metric
                                </label>
                            </div>
                            <div class="formField clr md mb-15">
                                <select name="metric" class="no-border">
                                    <option selected="" value="none">None</option>
                                    <option value="read">Read</option>
                                    <option value="create">Create</option>
                                    <option value="update">Update</option>
                                    <option value="delete">Delete</option>                                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Asset
                                </label>
                            </div>
                            <div class="formField clr md mb-15">
                                <select name="asset" class="no-border">
                                    <option selected="" value="none">None</option>
                                    <option value="read">Read</option>
                                    <option value="create">Create</option>
                                    <option value="update">Update</option>
                                    <option value="delete">Delete</option>                                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Users
                                </label>
                            </div>
                            <div class="formField clr md mb-15">
                                <select name="user" class="no-border">
                                    <option selected="" value="none">None</option>
                                    <option value="read">Read</option>
                                    <option value="create">Create</option>
                                    <option value="update">Update</option>
                                    <option value="delete">Delete</option>                                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Self Service
                                </label>
                            </div>
                            <div class="formField clr md mb-15">
                                <select name="self_service" class="no-border">
                                    <option selected="" value="none">None</option>
                                    <option value="read">Read</option>
                                    <option value="create">Create</option>
                                    <option value="update">Update</option>
                                    <option value="delete">Delete</option>                                                    
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="row ">
                        <div class="col-md-12 text-center pt-30 pb-30">
                            <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                            <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                                Submit
                            </button>
                        </div>
                    </div>


                    <div class="row">
                        <div class="pt-15 whtbg p-15 col-md-12">
                            <h5 class="basic-font boldFont mb-10">Role permissions:</h5>
                            <p>Administration permission grants full access to everything. Overrides all other permissions.<br>
                                Manager permission has full access to everything Except Administration functions.</p>
                            <ul>
                                <li class="pb-5"><strong>None</strong> = No Access to the data</li>
                                <li class="pb-5"><strong>Read</strong> = Able to view the data but can not control any data functions</li>
                                <li class="pb-5"><strong>Create</strong> = Able to create new records, inherits Read permission</li>
                                <li class="pb-5"><strong>Update</strong> = Able to update records, inherits Read and Create permission</li>
                                <li class="pb-5"><strong>Delete</strong> = Able to delete records, inherits Read, Create, and Update permission</li>
                            </ul>

                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
    
    <jsp:include page="delete_confirmation_modal.jsp" />
    
    <!-- BEGIN PAGE LEVEL JS-->
    <script type="text/javascript">
    
    function resetFilters()
    {
        let url = new URL(location.href);
        location.href = url.origin + url.pathname;
    }
    function editRole(role_id) 
    {
        $("form#add_form").get(0).classList.remove('was-validated');
        document.getElementById('add_form').reset();

        $.ajax({
            url: "ajax_lookup_role_data",
            type: 'post',
            dataType: "json",
            data: 
            {
                role_id: role_id
            },
            error: function(jqXHR, textStatus) 
            {
                switch (jqXHR.status) {
                    case 401:
                        window.location.assign("<%=props.get("login_url").toString()%>");
                        break;
                    case 403:
                        alert("You don't have rights enough!");
                        break;
                    default:
                        alert("Error processing request!");
                }
            },
            success: function( data ) 
            {
                console.log(data);
                if (Object.keys(data).length > 0) {
                    $('.rightPanelHolder').addClass('open');
                    $('.rightSidePanel').addClass('open');
                    $('html').addClass('panelOpen');
                    $('#right_panel_title').html('Edit role');

                    document.getElementsByName("id")[0].value=data[0];
                    document.querySelectorAll('input[name="name"]')[0].value = data[1];
                    document.querySelectorAll('input[name="description"]')[0].value = data[2];
                    document.getElementsByName("administration")[0].value = data[5];
                    document.getElementsByName("manager")[0].value = data[6];
                    document.getElementsByName("financial")[0].value = data[7];
                    document.getElementsByName("incident")[0].value = data[8];
                    document.getElementsByName("param_request")[0].value = data[9];
                    document.getElementsByName("contact")[0].value = data[10];
                    document.getElementsByName("cx")[0].value = data[11];
                    document.getElementsByName("survey")[0].value = data[12];
                    document.getElementsByName("sla")[0].value = data[13];
                    document.getElementsByName("project")[0].value = data[14];
                    document.getElementsByName("job")[0].value = data[15];
                    document.getElementsByName("metric")[0].value = data[16];
                    document.getElementsByName("asset")[0].value = data[17];
                    document.getElementsByName("user")[0].value = data[18];
                    document.getElementsByName("self_service")[0].value = data[19];

//                        console.log(data[0]);

                } else {
                    alert("Error processing request!");
                }
            }
        });            
    }
    
    window.addEventListener("load", (event) => 
    {
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        
        var validation = Array.prototype.filter.call(forms, function(form) 
        {
            
            form.addEventListener('submit', function(event) 
            {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                    $('.rightPanelHolder').animate({scrollTop: $('.form-control:invalid').first().offset().top - 50}, 'fast');
                }
                form.classList.add('was-validated');
                var role_id = document.getElementsByName("id")[0].value;
                if (role_id !== "") 
                {
                    form.action = "admin_roles_edit";
                    form.method = "POST";
                } else {
                    form.action = "admin_roles_add";
                    form.method = "POST";
                }
            }, false);
        });            
        
        $('.rightSidePanelOpener').on('click', function(){
            $('#right_panel_title').html('Add user role');
            $("form#add_form").get(0).classList.remove('was-validated');
            document.getElementById('add_form').reset();
        });

    });

    </script>
    <!-- END PAGE LEVEL JS-->   
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_roles.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_roles.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

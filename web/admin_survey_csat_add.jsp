<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_survey_csat_add.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        if (!session.getAttribute("administration").toString().equalsIgnoreCase("true") && !session.getAttribute("manager").toString().equalsIgnoreCase("true")) 
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String unique_cat_sub[] = db.get_incidents.unique_category_subcategory(con);
                //ArrayList<String[]> all_groups = db.get_groups.all(con);

%>
<!-- BEGIN PAGE LEVEL CSS-->
<!-- END PAGE LEVEL CSS-->
<div class="p-15 ticket-content">
    <span class="rightSidePanelCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
    <h4 class="mediumFont large-font mb-20">Add CSAT Survey</h4>
    <div class="clr whtbg p-15 ">
        <div class="row">
            <div class="col-12">
                <input type="hidden" name="question_type_added" id="question_type_added"/>
                <form class="form" action="admin_survey_csat_add" onsubmit="return validateForm()"  method="post">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="field-title">
                                    <label class="formLabel md has-txt-only mb-15">
                                        Name
                                    </label>
                                </div>
                                <div class="formField clr md px-0 border-0 ">
                                    <input type="text" id="name" name="name" class="form-control" placeholder="Survey Name" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="field-title">
                                    <label class="formLabel md mb-15">
                                        Description
                                    </label>
                                </div>
                                <div class="formField clr md px-0 border-0">
                                    <input type="text" id="description" name="description" class="form-control" placeholder="Description" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="field-title">
                                    <label class="formLabel md mb-15">
                                        Version
                                    </label>
                                </div>
                                <div class="formField clr md px-0 border-0">
                                    <input type="text" id="version" name="version" class="form-control" placeholder="Version x.xx.xx" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="field-title">
                                    <label class="formLabel md has-txt-only mb-15">
                                        Status
                                    </label>
                                </div>
                                <div class="formField clr md px-0 border-0  ">
                                    <select id="status" name="status" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Set the status of the Survey">
                                        <option value="Draft">Draft</option>
                                        <option value="Active">Active</option>
                                        <option value="Inactive">Inactive</option>
                                        <option value="Retired">Retired</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="field-title">
                                    <label class="formLabel md has-txt-only mb-15">
                                        Contact Method
                                    </label>
                                </div>
                                <div class="formField clr md px-0 border-0  ">
                                    <select id="contact_method" name="contact_method" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Select a contact method that will trigger this Survey">
                                        <option value="Any">Any</option>
                                        <option value="Chat">Chat</option>
                                        <option value="Email">Email</option>
                                        <option value="Phone">Phone</option>
                                        <option value="Self-Service">Self-Service</option>
                                        <option value="Walk-in">Walk-in</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="field-title">
                                    <label class="formLabel md has-txt-only mb-15">
                                        Ticket Type
                                    </label>
                                </div>
                                <div class="formField clr md px-0 border-0  ">
                                    <select id="trigger_object" onchange="populate_ticket_state()" name="trigger_object" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Select a ticket type that will trigger this Survey">
                                        <option value=""></option>
                                        <option value="Incident">Incident</option>
                                        <option value="Request">Request</option>
                                        <option value="Contact">Contact</option>
                                        <option value="Manual">Manual</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">                                        
                            <div class="col-md-6">
                                <div class="field-title">
                                    <label class="formLabel md has-txt-only mb-15">
                                        Ticket State
                                    </label>
                                </div>
                                <div class="formField clr md px-0 border-0  ">
                                    <select id="trigger_state" name="trigger_state" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Select a ticket state that will trigger this Survey">

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="field-title">
                                    <label class="formLabel md has-txt-only mb-15">
                                        Trigger Interval
                                    </label>
                                </div>
                                <div class="formField clr md px-0 border-0  ">
                                    <select id="trigger_interval" name="trigger_interval" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Trigger a Survey on every Nth ticket">
                                        <%
                                        for(int n = 1; n < 1001; n++)
                                        {
                                            %>
                                            <option value="<%=n%>"><%=n%></option>
                                            <%
                                        }
                                        %>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">                                        
                            <div class="col-md-6">
                                <div class="field-title">
                                    <label class="formLabel md has-txt-only mb-15">
                                        Max # Survey per User (30 day period)
                                    </label>
                                </div>
                                <div class="formField clr md px-0 border-0  ">
                                    <select id="max_per_person" name="max_per_person" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Maximum number of Surveys a user should get per 30 days">
                                        <option value="0">Unlimited</option>
                                        <%
                                        for(int m = 1; m < 31; m++)
                                        {
                                            %>
                                            <option value="<%=m%>"><%=m%></option>
                                            <%
                                        }
                                        %>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="field-title">
                                    <label class="formLabel md has-txt-only mb-15">
                                        Lifespan
                                    </label>
                                </div>
                                <div class="formField clr md px-0 border-0  ">
                                    <input type="text" id="lifespan" name="lifespan" class="form-control" placeholder="The number of hours before the Survey is declared unanswered." >
                                </div>
                            </div>        
                        </div><!--end row-->

                        <div class="row" id="survey_trigger_div" style="display:none"> 


                        </div>

                        <div class="row">                                        
                            <div class="col-md-12">
                                <div class="field-title">
                                    <label class="formLabel md has-txt-only mb-15">
                                        Email Subject
                                    </label>
                                </div>
                                <div class="formField clr md px-0 border-0  ">
                                    <input type="text" id="email_subject" name="email_subject" class="form-control" placeholder="Enter the email subject line that will be sent to the users" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="field-title">
                                    <label class="formLabel md has-txt-only mb-15">
                                        Email Body
                                    </label>
                                </div>
                                <div class="formField clr md px-0 border-0  ">
                                    <textarea class="form-control" name="email_body" id="email_body" rows="3" placeholder="Enter the email body text"></textarea>  
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="field-title">
                                    <label class="formLabel md has-txt-only mb-15">
                                        Send Reminder Email
                                    </label>
                                </div>
                                <div class="formField clr md px-0 border-0">
                                    <select onchange="show_reminder_email()" id="email_reminder_schedule" name="email_reminder_schedule" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Maximum number of Surveys a user should get per 30 days">
                                        <option value="0">No Reminders</option>
                                        <option value="48">Every 2 Days</option>
                                        <option value="72">Every 3 Days</option>
                                        <option value="168">Once a Week</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="email_reminder_subject_div" style="display:none">                                        
                            <div class="col-md-12">
                                <div class="field-title">
                                    <label class="formLabel md has-txt-only mb-15">
                                        Reminder Email Subject
                                    </label>
                                </div>
                                <div class="formField clr md px-0 border-0">
                                    <input type="text" id="email_reminder_subject" name="email_reminder_subject" class="form-control" placeholder="Enter the reminder email subject line that will be sent to the users" >
                                </div>
                            </div>
                        </div>
                        <div class="row" id="email_reminder_body_div" style="display:none">
                            <div class="col-12">
                                <div class="field-title">
                                    <label class="formLabel md has-txt-only mb-15">
                                        Reminder Email Body
                                    </label>
                                </div>
                                <div class="formField clr md px-0 border-0">
                                    <textarea class="form-control" name="email_reminder_body" id="email_reminder_body" rows="3" placeholder="Enter the reminder email body text"></textarea>  
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="field-title">
                                    <label class="formLabel md has-txt-only mb-15">
                                        Select the type of question that you want added to the Survey
                                    </label>
                                </div>
                                <div class="formField clr md px-0 border-0">
                                    <button type="button" onclick="add_true_false()" class="btn mr-1 mb-1 btn-primary "><i class="la la-plus"></i> True/False</button>
                                    &nbsp;
                                    <button type="button" onclick="add_rating_scale_5()" class="btn mr-1 mb-1 btn-primary "><i class="la la-plus"></i> Rate Scale (1-5)</button>
                                    &nbsp;
                                    <button type="button" onclick="add_opinion()" class="btn mr-1 mb-1 btn-primary "><i class="la la-plus"></i> Opinion</button>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="true_false_question_div" style="display:none">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="t_f_question">Question</label>
                                            <input type="text" id="t_f_question" name="t_f_question" class="form-control" placeholder="Enter a true/false type question" >
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="t_f_answer_positive">Positive Answer</label>
                                            <select id="t_f_answer_positive" onchange="toogle_t_f('positive')" name="t_f_answer_positive" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="What is the value of a positive answer?">
                                                <option selected value="True">True</option>
                                                <option value="False">False</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="t_f_answer_negative">Negative Answer</label>
                                            <select id="t_f_answer_negative" disabled name="t_f_answer_negative" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="What is the value of a negative answer?">
                                                <option value="True">True</option>
                                                <option selected value="False">False</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <button type="button" onclick="add_row('true_false')" class="btn mr-1 mb-1 btn-primary ">Add</button>
                                            <button type="button" class="btn mr-1 mb-1 btn-warning ">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="rating_scale_5_question_div1" style="display:none">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="rating_scale_5_question">Question</label>
                                            <input type="text" id="rating_scale_5_question" name="rating_scale_5_question" class="form-control" placeholder="Enter an rating scale type question" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="rating_scale_5_1">Most Negative Answer(1)</label>
                                            <input type="text" id="rating_scale_5_1" name="rating_scale_5_1" class="form-control" placeholder="Enter the most negative answer" value="Not Satisfied"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="rating_scale_5_2">Negative Answer(2)</label>
                                            <input type="text" id="rating_scale_5_2" name="rating_scale_5_2" class="form-control" placeholder="Enter the negative answer" value="Somewhat Satisfied"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="rating_scale_5_3">Neutral Answer(3)</label>
                                            <input type="text" id="rating_scale_5_3" name="rating_scale_5_3" class="form-control" placeholder="Enter the neutral answer" value="Mostly Satisfied"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="rating_scale_5_4">Positive Answer(4)</label>
                                            <input type="text" id="rating_scale_5_4" name="rating_scale_5_4" class="form-control" placeholder="Enter the positive answer" value="Satisfied"/>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="rating_scale_5_5">Most Positive Answer(5)</label>
                                            <input type="text" id="rating_scale_5_5" name="rating_scale_5_5" class="form-control" placeholder="Enter the most positive answer" value="Very satisfied"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                            <button type="button" onclick="add_row('rating_scale_5')" class="btn mr-1 mb-1 btn-primary ">&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;</button>
                                            <button type="button" onclick="cancel_rating_scale_5()" class="btn mr-1 mb-1 btn-warning ">&nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="opinion_question_div1" style="display:none">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="opinion_question">Question</label>
                                            <input type="text" id="opinion_question" name="opinion_question" class="form-control" placeholder="Enter an opinion type question" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                            <button type="button" onclick="add_row('opinion')" class="btn mr-1 mb-1 btn-primary ">Add</button>
                                            <button type="button" onclick="cancel_opinion()" class="btn mr-1 mb-1 btn-warning ">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="top_hr" style="display:none">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="row" id="questions_list" style="display:none">        
                            <div class="col-md-12">
                                <button type="button" onclick="delete_row()" class="btn mr-1 mb-1 btn-primary ">Delete Selected</button>
                                <table class="table" id="question_table" border="0" cellspacing="2" cellpadding="10">
                                    <tr>
                                        <td>#</td>
                                        <td>Question Number</td>
                                        <td><b>Question Type</b></td>
                                        <td><b>Question</b></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row" id="bottom_hr" style="display:none">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>                       
                    </div>
                    <div class="row ">
                        <div class="col-md-12 text-center pt-30 pb-30">
                            <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                            <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                                Submit
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- BEGIN PAGE LEVEL JS-->
<script>
    function validateForm() 
    {
        var error = "";
        var lifespan = document.getElementById("lifespan").value;

        if (lifespan === "" || isNaN(lifespan)) 
        {
            error = "Lifespan must be filled out and be a number";
        }
        if(error !== "")
        {
            var errorMsg = "<p>" + error + "</p>"
            document.getElementById("modal_text").innerHTML = errorMsg
            $("#error_modal").modal("show");
            //alert(error);
            return false;
        }
        else
        {
            return true;
        }
    }
</script>



<script>
    function toogle_t_f(p_n)
    {
        if(p_n == "positive")
        {
            if(document.getElementById('t_f_answer_positive').value == 'True')
            {
                document.getElementById("t_f_answer_negative").value = 'False';
            }
            else
            {
                document.getElementById("t_f_answer_negative").value = 'True';
            }
        }
        
    }
    function delete_row() 
    {
        /***We get the table object based on given id ***/
        var objTable = document.getElementById("question_table");
        /*** Get the current row length ***/
        var iRow = objTable.rows.length;
        /*** Initial row counter ***/
        var counter = 0;
        /*** Performing a loop inside the table ***/
        if (objTable.rows.length > 1) 
        {
            for (var i = 0; i < objTable.rows.length; i++) 
            {
                /*** Get checkbox object ***/
                var chk = objTable.rows[i].cells[0].childNodes[0];
                if (chk.checked) 
                {
                    /*** if checked we del ***/
                    objTable.deleteRow(i);
                    iRow--;
                    i--;
                    counter = counter + 1;
                }                
            }
        }
        var t_f_string = "True/False";
        
        //re-number rows
        var table = document.getElementById("question_table");
        //start at 1 ignore header row
        for (var i = 1, row; row = table.rows[i]; i++) 
        {
            var row_number = i + 1;
            var question_type_cell = row.cells[2];
            if(question_type_cell.innerText)
            {
                alert(question_type_cell.innerText);
            }
            
    

            //string.indexOf(substring) !== -1
            
            //iterate through rows
            //rows would be accessed using the "row" variable assigned in the for loop
            var question_number_cell = row.cells[0];
            question_number_cell.innerHTML = "<input type='hidden' name='question_number" + "~" + row_number + "' value='" + row_number + "'/>" + row_number;
           
           
           
           for (var j = 0, col; col = row.cells[j]; j++) 
           {
             //iterate through columns
             //columns would be accessed using the "col" variable assigned in the for loop
           }  
        }
        
        
    }
    function add_row(question_type) 
    {
        $(document.getElementById("top_hr")).show();
        $(document.getElementById("questions_list")).show();
        $(document.getElementById("bottom_hr")).show();
        
        /*** We get the table object based on given id ***/
        var object_table = document.getElementById("question_table");
        var row_number = object_table.rows.length;
        /*** We insert the row by specifying the current rows length ***/
        var object_row = object_table.insertRow(object_table.rows.length);
        /*** We  add some text inside the celll ***/
        /*** We insert the first row cell ***/
        var checkbox_cell = object_row.insertCell(0);
        /*** We  insert a checkbox object ***/
        var objInputCheckBox = document.createElement("input");
        var question_number_cell = object_row.insertCell(1);
        var question_type_cell = object_row.insertCell(2);
        var question_cell = object_row.insertCell(3);
            
        if(question_type == "true_false")
        {
            
            objInputCheckBox.type = "checkbox";
            checkbox_cell.appendChild(objInputCheckBox);

            /*** question number ***/            
            question_number_cell.innerHTML = "<input type='hidden' name='question_number" + "~" + row_number + "' value='" + row_number + "'/>" + row_number;
            /*** question type ***/            
            question_type_cell.innerHTML = "<input type='hidden' name='question_type" + "~" + row_number + "' value='true_false'/>" + "True/False";

            /*** question and answers ***/
            
            var t_f_question = document.getElementById("t_f_question").value;
            var hidden_t_f_question = "<input type='hidden' name='question" + "~" + row_number + "' value='" + t_f_question + "'/>"
            var t_f_answer_positive = document.getElementById("t_f_answer_positive").value;
            var hidden_t_f_answer_positive = "<input type='hidden' name='value_5" + "~" + row_number + "' value='" + t_f_answer_positive + "'/>"
            var t_f_answer_negative = document.getElementById("t_f_answer_negative").value;
            var hidden_t_f_answer_negative = "<input type='hidden' name='value_1" + "~" + row_number + "' value='" + t_f_answer_negative + "'/>"   
            //alert(hidden_question_type + hidden_t_f_question + hidden_t_f_question_true + hidden_t_f_question_false + t_f_question);
            question_cell.innerHTML = hidden_t_f_question + hidden_t_f_answer_positive + hidden_t_f_answer_negative + t_f_question;            
            
            //clear question after save
            document.getElementById("t_f_question").value = "";
            $(document.getElementById("true_false_question_div")).hide();
        
        }
        else
        {
            if(question_type == "rating_scale_5")
            {
                objInputCheckBox.type = "checkbox";
                checkbox_cell.appendChild(objInputCheckBox);
                /*** question number ***/            
                question_number_cell.innerHTML = row_number;
                question_type_cell.innerHTML = "Rating Scale 5";
                var hidden_question_number = "<input type='hidden' name='question_number" + "~" + row_number + "' value='" + row_number + "'/>"
                var hidden_question_type = "<input type='hidden' name='question_type" + "~" + row_number + "' value='rating_scale_5'/>"
                var rating_scale_5_question = document.getElementById("rating_scale_5_question").value;
                var hidden_opinion_question = "<input type='hidden' name='question" + "~" + row_number + "' value='" + rating_scale_5_question + "'/>"
                var rating_scale_5_5 = document.getElementById("rating_scale_5_5").value;
                var hidden_rating_scale_5 = "<input type='hidden' name='value_5" + "~" + row_number + "' value='" + rating_scale_5_5 + "'/>"
                var rating_scale_5_4 = document.getElementById("rating_scale_5_4").value;
                var hidden_rating_scale_4 = "<input type='hidden' name='value_4" + "~" + row_number + "' value='" + rating_scale_5_4 + "'/>"
                var rating_scale_5_3 = document.getElementById("rating_scale_5_3").value;
                var hidden_rating_scale_3 = "<input type='hidden' name='value_3" + "~" + row_number + "' value='" + rating_scale_5_3 + "'/>"
                var rating_scale_5_2 = document.getElementById("rating_scale_5_2").value;
                var hidden_rating_scale_2 = "<input type='hidden' name='value_2" + "~" + row_number + "' value='" + rating_scale_5_2 + "'/>"
                var rating_scale_5_1 = document.getElementById("rating_scale_5_1").value;
                var hidden_rating_scale_1 = "<input type='hidden' name='value_1" + "~" + row_number + "' value='" + rating_scale_5_1 + "'/>"
                question_cell.innerHTML = hidden_question_number + hidden_question_type + hidden_opinion_question + hidden_rating_scale_5 + hidden_rating_scale_4 + hidden_rating_scale_3 + hidden_rating_scale_2 + hidden_rating_scale_1 + rating_scale_5_question;
                //alert(hidden_question_type + hidden_opinion_question + hidden_opinion_question_5 + hidden_opinion_question_4 + hidden_opinion_question_3 + hidden_opinion_question_2 + hidden_opinion_question_1 + opinion_question);
                //clear question after save
                document.getElementById("rating_scale_5_question").value = "";
                $(document.getElementById("rating_scale_5_question_div1")).hide();
                $(document.getElementById("rating_scale_5_question_div2")).hide();
            }
            else
            {
                if(question_type == "opinion")
                {
                    objInputCheckBox.type = "checkbox";
                    checkbox_cell.appendChild(objInputCheckBox);
                    /*** question number ***/            
                    question_number_cell.innerHTML = row_number;                    
                    question_type_cell.innerHTML = "Opinion";
                    var hidden_question_number = "<input type='hidden' name='question_number" + "~" + row_number + "' value='" + row_number + "'/>"
                    var hidden_question_type = "<input type='hidden' name='question_type" + "~" + row_number + "' value='opinion'/>"
                    var opinion_question = document.getElementById("opinion_question").value;
                    var hidden_opinion_question = "<input type='hidden' name='question" + "~" + row_number + "' value='" + opinion_question + "'/>"
                    question_cell.innerHTML = hidden_question_number + hidden_question_type + hidden_opinion_question + opinion_question;
                    document.getElementById("opinion_question").value = "";
                    $(document.getElementById("opinion_question_div1")).hide();
                }
            }
        }
        document.getElementById("question_type_added").value = "";
    }
    
    function clear_ticket_state()
    {
        var selectElement = document.getElementById("trigger_state");
        while (selectElement.length > 0) 
        {
            selectElement.remove(0);
        }
    }
    function populate_ticket_state()
    {
        //clear the original opts first
        clear_ticket_state();
                
        var trigger_object = document.getElementById("trigger_object").value;   
        
        if(trigger_object == "Incident")
        {
            let dropdown = document.getElementById('trigger_state');
            let option;            
            option = document.createElement('option');
            option.text = "Create";
            option.value = "Create";
            dropdown.add(option);            
            option = document.createElement('option');
            option.text = "Resolved";
            option.value = "Resolved";
            dropdown.add(option);
            option = document.createElement('option');
            option.text = "Closed";
            option.value = "Closed";
            dropdown.add(option);
        }
        else
        {
            if(trigger_object == "Request")
            {
                let dropdown = document.getElementById('trigger_state');
                let option;            
                option = document.createElement('option');
                option.text = "Create";
                option.value = "Create";
                dropdown.add(option);            
                option = document.createElement('option');
                option.text = "Resolved";
                option.value = "Resolved";
                dropdown.add(option);
                option = document.createElement('option');
                option.text = "Closed";
                option.value = "Closed";
                dropdown.add(option);
            }
            else
            {
                if(trigger_object == "Contact")
                {
                    let dropdown = document.getElementById('trigger_state');
                    let option;            
                    option = document.createElement('option');
                    option.text = "Complete";
                    option.value = "Complete";
                    dropdown.add(option);  
                }
            }
        }
    }
    function show_reminder_email()
    {
        var email_reminder_schedule = document.getElementById("email_reminder_schedule").value;   
        if(email_reminder_schedule !== "0")
        {
            $(document.getElementById("email_reminder_subject_div")).show();
            $(document.getElementById("email_reminder_body_div")).show();
        }
        else
        {
            $(document.getElementById("email_reminder_subject_div")).hide();
            $(document.getElementById("email_reminder_body_div")).hide();
        }
    }
    function add_true_false()
    {
        $(document.getElementById("true_false_question_div")).show();
        document.getElementById("question_type_added").value = "true_false";
    }
    function cancel_true_false()
    {
        $(document.getElementById("true_false_question_div")).hide();
        document.getElementById("question_type_added").value = "";
    }
    function cancel_rating_scale_5()
    {
        $(document.getElementById("rating_scale_5_question_div1")).hide();
        $(document.getElementById("rating_scale_5_question_div2")).hide();
        document.getElementById("question_type_added").value = "";
    }
    function add_rating_scale_5()
    {
        $(document.getElementById("rating_scale_5_question_div1")).show();
        $(document.getElementById("rating_scale_5_question_div2")).show();
        document.getElementById("question_type_added").value = "rating_scale_5";
    }
    function add_opinion()
    {
        $(document.getElementById("opinion_question_div1")).show();
        document.getElementById("question_type_added").value = "opinion";
    }
    function cancel_opinion()
    {
        $(document.getElementById("opinion_question_div1")).hide();
        document.getElementById("question_type_added").value = "";
    }
    
    
    
</script>
<!-- END PAGE LEVEL JS-->   
<%                con.close();
    
            } catch (Exception e) {
                System.out.println("Exception in admin_survey_csat_add.jsp: " + e);
                logger.debug("ERROR: Exception in admin_survey_csat_add.jsp:" + e);
            }
        } //end if not admin permission
    } //end if not redirected
%>
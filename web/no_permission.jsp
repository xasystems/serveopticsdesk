<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else
    {
        String home_page = session.getAttribute("home_page").toString();
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);

    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <!-- BEGIN PAGE LEVEL CSS-->
    <!-- END PAGE LEVEL CSS-->
    <div class="content-page full ml-0">
        <!-- Start content -->
        <div class="content ">

            <!-- ==================
                 PAGE CONTENT START
                 ================== -->

            <div class="page-content-wrapper">
                <div class="container-fluid " >
                    <div class="clr whtbg p-10 mb-15">
                        <div class="alert bg-warning alert-icon-right alert-arrow-right mb-2" role="alert">
                            <span class="alert-icon"><i class="la la-warning"></i></span>

                            <strong>Not Authorized</strong>
                            <br>
                            <br>
                            <h3>Your current role does not allow access to this resource!</h3> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->   
    <%
    } //end if not redirected
    %>
    </body>
</html>

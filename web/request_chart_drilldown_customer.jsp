<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : incident_total_incidents
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        if (!session.getAttribute("request").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("true")) 
        {
            //create the go back url
            if(request.getHeader("referer").contains("_home"))
            {
                session.setAttribute("request_drilldown_referer", request.getHeader("referer"));
            }
            String user_tz_name = "US/Eastern";
            String user_tz_time = "-05:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "US/Eastern";
                    user_tz_time = "-05:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "US/Eastern";
                user_tz_time = "-05:00";
            }  
            String home_page = session.getAttribute("home_page").toString();
            String chart_type = request.getParameter("chart_type");
            String date_range = "";
            String category = "";
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            ArrayList <String[]> requests = new ArrayList();
            String priority = "";
            java.util.Date start_date = new java.util.Date();
            java.util.Date end_date = new java.util.Date();
            ArrayList<String[]> groups = db.get_groups.all(con);
            String selected = "";
            String customer_group_id = "";
            String date = "";
            try 
            {
                customer_group_id = request.getParameter("customer_group_id");
                if (customer_group_id.equalsIgnoreCase("null") || customer_group_id == null) 
                {
                    customer_group_id = "all";
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                customer_group_id = "all";
            }
            //if date_range is set override start and end dates
            if(request.getParameter("date_range") != null) //filter has been updated and applied so override the dates
            {
                //if date_range is set override the start and end
                try 
                {
                    date_range = request.getParameter("date_range");
                    if (date_range.equalsIgnoreCase("null") || date_range == null) 
                    {
                        date_range = support.filter_dates.past_30_days();
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to past 30 days
                    date_range = support.filter_dates.past_30_days();
                }
                String temp[] = date_range.split("-");
                start_date = filter_format.parse(temp[0]);
                //filter_start = filter_format.parse("01/12/2019 12:00 AM");
                //System.out.println("filter_start=" + temp[0] + " ====" + start_date);

                end_date = filter_format.parse(temp[1]);
                //System.out.println("filter_end=" + temp[1].trim() + " ====" + end_date);
            }
            else
            {
                date_range = support.filter_dates.past_30_days();
            }
            
            //what kind of page 
            if(chart_type.equalsIgnoreCase("queue"))
            {
                priority = request.getParameter("priority");    
                //get the incidents
                requests = db.get_requests.request_chart_drilldown_queue_customer(con, priority, start_date, end_date, customer_group_id, user_tz_time); 
            }
            else
            {
                if(chart_type.equalsIgnoreCase("number")) //number
                {
                    date = request.getParameter("date");  //12/25/2018
                    requests = db.get_requests.request_chart_drilldown_number_customer(con, start_date, end_date, customer_group_id);
                }
                else
                {
                    if(chart_type.equalsIgnoreCase("category")) //category
                    {
                        //?chart_type=category&category=Desktop%20Software&start_date=20181219071500&stop_date=20190119071500&group_id=all
                        category = request.getParameter("category");
                        requests = db.get_requests.request_chart_drilldown_requestor_customer(con, category, start_date, end_date);  //cat is assigned_group_id
                    }
                    else
                    {
                        if(chart_type.equalsIgnoreCase("closure")) //closure
                        {
                            //?chart_type=closure&priority=Medium&date=12/22/2018&group_id=all
                            priority = request.getParameter("priority");
                            requests = db.get_requests.request_chart_drilldown_closure_customer(con, priority, start_date, end_date, customer_group_id);
                        }
                    }
                }
            }
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END Custom CSS-->
    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <input type="hidden" name="date" id="date" value="<%=date%>"/>
    <input type="hidden" name="chart_type" id="chart_type" value="<%=chart_type%>"/>
    <input type="hidden" name="priority" id="priority" value="<%=priority%>"/>
    <input type="hidden" name="category" id="category" value="<%=category%>"/>
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title"><i class="la la-wrench"></i>&nbsp;Requests</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="#" onClick="parent.location='<%=session.getAttribute("request_drilldown_referer")%>'"><i class="la la-angle-left"></i>Back</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- start content here-->
            <div class="row">
                <div class="col-xl-3 col-6">
                    <div class="form-group">
                        <label>Date Range</label>
                        <div class='input-group'>
                            <input type='text' id="filter_date_range" name="filter_date_range" value="<%=date_range%>" class="form-control datetime" />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <span class="la la-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                
                    <%
                    if(chart_type.equalsIgnoreCase("category")) //category
                    {
                        
                        %>
                        <input type="hidden" name="group_id" id="group_id" value="<%=customer_group_id%>"/>
                        
                        <%
                    }
                    else
                    {
                        %>
                        <div class="col-xl-2 col-4">
                        
                        <div class="form-group">
                            <label>Customer Group</label>
                            <div class='input-group'>
                                <select class="select2-placeholder form-control" name="customer_group_id" id="customer_group_id">
                                    <option value="all">All</option>
                                    <%
                                    for (int a = 0; a < groups.size(); a++) 
                                    {
                                        String this_group[] = groups.get(a);
                                        selected = "";
                                        if (this_group[0].equalsIgnoreCase(customer_group_id)) 
                                        {
                                            selected = "SELECTED";
                                        }
                                    %>
                                    <option <%=selected%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                    <%
                                    }
                                    %>
                                </select>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <span class="la la-group"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                                </div>
                        <%
                    }
                    %>                    
                
                <div class="col-xl-1 col-2">
                    <label>&nbsp;</label>
                    <div class="form-actions">
                        <button type="button" onclick="reload_page()" class="btn btn-blue mr-1">
                            <i class="fa-filter"></i> Apply
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <table class="table table-striped table-bordered zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>Request ID</th>
                                            <th>Priority</th>
                                            <th>Date of Request</th>
                                            <th>Requested By</th>
                                            <th>Requested For</th>
                                            <th>Assigned to Group</th>
                                            <th>Assigned to Analyst</th>
                                            <th>State</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                        for(int a = 0; a < requests.size(); a++)
                                        {
                                            String req[] = requests.get(a);
                                            String request_date = "";
                                            try
                                            {
                                                request_date = display_format.format(timestamp_format.parse(req[7]));
                                            }
                                            catch (Exception e)
                                            {
                                                request_date = "";
                                            }
                                            %>
                                            <tr onclick="location.href='request.jsp?id=<%=req[0]%>';" style="cursor:pointer;">
                                            <td><%=req[0]%></td>
                                            <td><%=req[22]%></td>
                                            <td><%=request_date%></td>
                                            <td><%=req[47] + " " + req[49]%></td>
                                            <td><%=req[52] + " " + req[54]%></td>
                                            <td><%=req[40]%></td>
                                            <td><%=req[42] + " " + req[44]%></td>
                                            <td><%=req[23]%></td>
                                                <% 
                                                String desc = "";
                                                if(req[29].length() > 20)
                                                {
                                                    desc = req[29].substring(0, 19) + "...";
                                                }
                                                else
                                                {
                                                    desc = req[29];
                                                }                                                
                                                %>
                                                <td> <%=desc%> <a href="#" title="<%=req[29]%>">(view)</a></td>
                                            </tr>
                                            <%
                                        }                                        
                                        %>                                          
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Request ID</th>
                                            <th>Priority</th>
                                            <th>Date of Request</th>
                                            <th>Requested By</th>
                                            <th>Requested For</th>
                                            <th>Assigned to Group</th>
                                            <th>Assigned to Analyst</th>
                                            <th>State</th>
                                            <th>Description</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <script>
        function reload_page()
        {
            var chart_type = document.getElementById("chart_type").value;
            var category = document.getElementById("category").value;
            var priority = document.getElementById("priority").value;
            var filter_date_range = document.getElementById("filter_date_range").value;
            if(chart_type == "category")
            {
                var customer_group_id = category;
                var URL = "request_chart_drilldown_customer.jsp?chart_type=" + chart_type + "&category=" + category + "&priority=" + priority + "&date_range=" + filter_date_range + "&customer_group_id=" + category;
            }
            else
            {
                var customer_group_id = document.getElementById("group_id").value;
                var URL = "request_chart_drilldown_customer.jsp?chart_type=" + chart_type + "&priority=" + priority + "&date_range=" + filter_date_range + "&customer_group_id=" + customer_group_id;
            }            
            window.location.href = URL;            
        }
    </script>
    <script>
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>


    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%      con.close();
        }//end if not permission
    }//end if not logged in
%>
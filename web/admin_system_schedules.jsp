<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page import="java.text.SimpleDateFormat"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    String context_dir = request.getServletContext().getRealPath("");
    LinkedHashMap props = support.config.get_config(context_dir);
    if(session.getAttribute("authenticated")==null)
    {
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        if(!session.getAttribute("administration").toString().equalsIgnoreCase("true"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session); 
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_incident_sla_delete.jsp=" + e);
            }
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
   
    <!-- END PAGE LEVEL CSS-->
                    <!-- ==================
                         PAGE CONTENT START
                         ================== -->

                    <div class="page-content-wrapper">
                        <div class="container-fluid">
                            <div class="clr whtbg p-10 mb-15">
                                <h1 class="large-font boldFont">System Schedules</h1>
                            </div>
<form class="form" action="admin_system_schedules_update" method="post">
                            <div class="sort-tbl-holder">
                                <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Task Name</th>
                                            <th>Run Interval (minutes)</th>
                                            <th>Last Run</th>
                                        </tr>
                                    </thead>
                            
                                    <tbody>
                                        <tr>
                                            <td>
                                                Send Surveys <input type="hidden" id="send_surveys" name="send_surveys" value="send_surveys">
                                            </td>
                                            <%
                                                    String send_survey_info[] = db.system_schedules.get_by_name(con, "send_surveys");
                                                    String send_survey_last_run = "";
                                                    try
                                                    {
                                                        java.util.Date d = new java.util.Date();
                                                        d = timestamp.parse(send_survey_info[2]);
                                                        send_survey_last_run = filter_format.format(d);
                                                    }
                                                    catch(Exception e)
                                                    {
                                                        send_survey_last_run = "No last run time";
                                                    }
                                            %>
                                            <td>
                                                <div class="formField clr md dInlineBlock">
                                                    <input type="number" class="no-border" placeholder="" id="send_surveys_interval" name="send_surveys_interval" value="<%=send_survey_info[1]%>">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="formField clr md dInlineBlock">
                                                    <input type="text" class="no-border" placeholder="" id="basicInput" value="<%=send_survey_last_run%>">
                                                </div>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                                    <%
                                                    String send_email_info[] = db.system_schedules.get_by_name(con, "send_email");
                                                    String send_email_last_run = "";
                                                    try
                                                    {
                                                        java.util.Date d = new java.util.Date();
                                                        d = timestamp.parse(send_survey_info[2]);
                                                        send_email_last_run = filter_format.format(d);
                                                    }
                                                    catch(Exception e)
                                                    {
                                                        send_email_last_run = "No last run time";
                                                    }
                                                    %>
                        
                                            <td>
                                                Send Email <input type="hidden" id="send_email" name="send_email" value="send_email">
                                            </td>
                                            <td>
                                                <div class="formField clr md dInlineBlock">
                                                    <input type="number" class="no-border" placeholder="" id="send_email_interval" name="send_email_interval" value="<%=send_email_info[1]%>">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="formField clr md dInlineBlock">
                                                    <input type="text" class="no-border" placeholder="" id="basicInput" value="<%=send_email_last_run%>">
                                                </div>
                                            </td>
                                            
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="col-md-12 text-center pt-30 pb-30">
                                    <button type="button" class="btn btn-wht customBtn xlg waves-effect ml-5 mr-5" role="button" onclick="javascript:location.href='admin_settings.jsp'">
                                        Cancel
                                    </button>
                                    <button type="submit" class="btn btn-primary-new customBtn xlg waves-effect ml-5 mr-5" role="button">
                                        Save
                                    </button>
                                </div>
                            </div>
</form>
                        </div>
                    </div> <!-- Page content Wrapper -->

    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->   
    <%
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

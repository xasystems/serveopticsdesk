<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="org.apache.commons.lang3.math.NumberUtils"%>
<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : home_service_catalog
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="org.apache.commons.lang3.BooleanUtils" %>
<%@ page import="java.text.*" %>
<%@ page import="java.sql.*" %>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        if (!session.getAttribute("self_service").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("true")) 
        {
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            session.setAttribute("home_page", "home_service_catalog.jsp");
            String home_page = session.getAttribute("home_page").toString();
            GregorianCalendar first_of_month = new GregorianCalendar();
            first_of_month.set(Calendar.DAY_OF_MONTH, 1);
            first_of_month.set(Calendar.HOUR_OF_DAY, 0);
            first_of_month.set(Calendar.MINUTE, 0);
            first_of_month.set(Calendar.SECOND, 0);     
            GregorianCalendar now = new GregorianCalendar();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session); 
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            String catalog_id = request.getParameter("id");
            String category = StringUtils.defaultString(request.getParameter("category"));
            String catalog_info[] = db.get_service_catalog.service_catalog_by_id(con, catalog_id);
            ArrayList <String[]> catalog_items;
            if (category.equals(""))
            {
                catalog_items = db.get_service_catalog.service_catalog_items_in_catalog_by_state(con, catalog_id, "Published");
            } else {
                catalog_items = db.get_service_catalog.service_catalog_items_in_catalog_by_state_and_category(con, catalog_id, "Published", category);
            }
            String message_id = request.getParameter("message_id");
            String request_user_id = request.getParameter("user_id");
            Boolean embedded = request.getParameter("embedded") != null && BooleanUtils.toBoolean(request.getParameter("embedded"));
            String pass_parameters = "";
            if(message_id != null)
            {
                pass_parameters = "&message_id=" + message_id;
            }
            if(request_user_id != null)
            {
                pass_parameters = pass_parameters + "&user_id=" + request_user_id;
            }
            
    %>
    <% if (!embedded) {%>
        <jsp:include page='header.jsp'>
            <jsp:param name="page_type" value=""/>
        </jsp:include>	

        <jsp:include page='menu_service_desk.jsp'>
            <jsp:param name="active_menu" value=""/>
        </jsp:include>	
        <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
        <!--<link rel="stylesheet" href="app-assets/css/colors.css">-->

    <% } %>

                <% if (category.equals(""))
                {
                %>
                    <ol class="breadcrumb mt-0">
                        <li class="breadcrumb-item"><a href="home_service_catalog.jsp">Catalog Home</a></li>
                        <li class="breadcrumb-item active pl-0" aria-current="page"><%=catalog_info[1]%></li>
                    </ol>
                <%
                } else {
                %>
                    <ol class="breadcrumb mt-0">
                        <li class="breadcrumb-item"><a href="#"><%=catalog_info[1]%></a></li>
                        <li class="breadcrumb-item active pl-0" aria-current="page"><%=category%></li>
                    </ol>
                    <h4 id="catalog_title" class="title xxl-font boldFont mb-15 pt-5">
                        <span class="txt"><%=category%></span>
                    </h4>
                <%
                }
                %>
                <div class="mb-30">
                    <div class="clr searchFieldHolder dInline formField" style="width:100%">
                        <span class="icon"><img src="assets/images/svg/searchIcon.svg" alt=""></span>
                        <input class="autocomplete" id="catalog_items_search" type="text" dir="ltr" placeholder="Find Anything">
                    </div>
                    <small id="catalog_showing" class="pt-15"><em>Showing <%=catalog_items.size()%> Items only.</em></small>
                </div>

                <div id="catalog_content" class="row">
                    <%
                    //get all published items in this catalog
                    for(int a = 0; a < catalog_items.size();a++)
                    {
                        String catalog_item[] = catalog_items.get(a);
                        %>

                        <div class="col-md-6 col-lg-6">
                            <div data-target="item" data-item-id="<%=catalog_item[0]%>" class="category clr rightSidePanelOpener">
                                <div class="icon">
                                <% if (catalog_item[38] != null && !catalog_item[38].equals("null")) { %>
                                    <div class="icon">
                                        <img src="get_attachment?uuid=<%=catalog_item[38]%>&name=image" alt="">
                                    </div>
                                <% } %>
                                </div>
                                <div class="txt catalog-item">
                                    <h5 class="mediumFont large-font mb-5"><%=catalog_item[2]%></h5>
                                    <p class=""><%=catalog_item[3]%></p>
                                    <div class="item-price clr">
                                        <div class="float-left">
                                            <a href="#" class="mr-10 underline" style="position: relative; top: 2px;">Item Details</a>
                                            <a href="#" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>
                                            <a href="#" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>

                                        </div>
                                        <!--<span class="float-right boldFont price">$<%=catalog_item[28]%> - $<%=catalog_item[7]%></span>-->
                                        <span class="float-right boldFont price">$<%=catalog_item[28]%></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <%
                    }
                    if(catalog_items.size() == 0)
                    {
                        %>
                        <div class="row">
                            <div class="col-12">
                                This Catalog seems to be empty.
                            </div>
                        </div>
                        <%
                    }
                    %>
    <% if (!embedded) {%>
                <!-- end content here-->
        <jsp:include page='footer.jsp'>
            <jsp:param name="parameter" value=""/>
        </jsp:include>	
        <!-- BEGIN PAGE LEVEL JS-->
        <!-- END PAGE LEVEL JS-->

    <div class="rightPanelHolder">
        <div class="p-15 ticket-content">
            <span class="rightSidePanelCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
            <h4 id="rightPanelTitle" class="mediumFont large-font mb-20"></h4>
            <div id="rightPanelContent" class="clr whtbg p-15">
            </div>
        </div>
    </div>

    <div class="rightPanelHolderUser">
        <div class="p-15 ticket-content">
            <span class="rightSidePanelUserCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
            <h4 class="mediumFont large-font mb-20 panelTitle"></h4>
            <div class="clr whtbg p-15 panelContent">
            </div>
        </div>
    </div>

    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <script src="app-assets/vendors/js/editors/ckeditor/ckeditor.js"></script>

    <script>

        function addRightSidePanelListeners()
        {
            $('.rightSidePanelCloser:not(.listens)').each(function(){
                $(this).addClass('listens');
                this.addEventListener("click", function()
                {
                    $('.rightPanelHolder').removeClass('open');
                    $('.rightSidePanel').removeClass('open');
                    $('html').removeClass('panelOpen');
                }, false);
            });
            
            $('.rightSidePanelUserCloser:not(.listens)').each(function(){
                $(this).addClass('listens');
                this.addEventListener("click", function()
                {
                    $('.rightPanelHolderUser').removeClass('open');
                    $('.rightSidePanelUser').removeClass('open');
                }, false);
            });

            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission

            var validation = Array.prototype.filter.call(forms, function(form) 
            {
                $(form).removeClass("needs-validation");
                form.addEventListener('submit', function(event) 
                {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                        $('.rightPanelHolder').animate({scrollTop: $('.form-control:invalid').first().offset().top - 50}, 'fast');
                    }
                    form.classList.add('was-validated');
                }, false);
            });            


            $('.rightSidePanelOpener:not(.listens)').each(function(){
    //            console.log("attaching to ", this);
                $(this).addClass('listens');

                this.addEventListener("click", function()
                {
                    $("#rightPanelContent").html("");

                    var panel_title = "";
                    var ajax = {};
                    ajax.url = "";
                    ajax.type = 'GET';
                    ajax.dataType = "html";
                    var clicked_node = $(event.target).closest(".rightSidePanelOpener");
                    var target = clicked_node.data('target');
                    var item_id = clicked_node.data('itemId');

                    switch (target) 
                    {
                        case "item":
                            ajax.url = "service_catalog_item.jsp";
                            ajax.data = {embedded: "true", id: item_id};
                            panel_title = "";
                            break;
                        default:
                            break;
                    }

                    if (ajax.url === "") return false;

                    ajax.error = function(jqXHR, textStatus)
                    {
                        switch (jqXHR.status) {
                            case 401:
                                window.location.assign("<%=props.get("login_url").toString()%>");
                                break;
                            case 403:
                                alert("No permission!");
                                break;
                            default:
                                alert("Error processing request!");
                        }
                    };

                    ajax.success = function (data) 
                    {                    
                        $('.rightPanelHolder').addClass('open');
                        $('.rightSidePanel').addClass('open');
                        $('html').addClass('panelOpen');

                        $("#rightPanelTitle").html(panel_title);
                        $("#rightPanelContent").html(data);
                        addRightSidePanelListeners();
                        attachFieldListeners($("#rightPanelContent"));        

                    }

                    $.ajax(ajax);
                }, false);
            });
            
            $('.rightSidePanelUserOpener:not(.listens)').each(function(){
                console.log("attaching to ", this);
    //            console.log("rightSidePanelOpener attached to", this);
                $(this).addClass('listens');

                this.addEventListener("click", function()
                {
                    $(".rightPanelHolderUser").find(".panelContent").html("");

                    var panel_title = "";
                    var ajax = {};
                    ajax.url = "";
                    ajax.type = 'GET';
                    ajax.dataType = "html";
    //                console.log("rightSidePanelOpener fired click ", event);

                    ajax.url = "admin_users_add.jsp";
                    panel_title = "Add New User";

                    ajax.error = function(jqXHR, textStatus)
                    {
                        switch (jqXHR.status) {
                            case 401:
                                window.location.assign("<%=props.get("login_url").toString()%>");
                                break;
                            case 403:
                                alert("No permission!");
                                break;
                            default:
                                alert("Error processing request!");
                        }
                    };

                    ajax.success = function (data) 
                    {                    

                        $('.rightPanelHolderUser').addClass('open');
                        $('.rightSidePanelUser').addClass('open');
                        $('html').addClass('panelOpen');

                        $(".rightPanelHolderUser").find(".panelTitle").html(panel_title);
                        $(".rightPanelHolderUser").find(".panelContent").html(data);

    //                    addRightSidePanelListeners();
    //                    attachFieldListeners($(".rightPanelHolderUser"));        

                    }

                    $.ajax(ajax);
                }, false);
            });

        }

        function attachFieldListeners(container)
        {
            container.find("input[name='requested_for_name']" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    container.find("input[name='requested_for_name']").val(ui.item.label); // display the selected text
                    container.find("input[name='requested_for_id']").val(ui.item.value); // save selected id to input
                    return false;
                }
            });
        }
        
        window.addEventListener("load", (event) => 
        {
            addRightSidePanelListeners();
            $('#catalog_items_search').each(function() 
            {
                var $el = $(this);
                var field = $el.prop('name');

                $el.autocomplete({
                    source: function( request, response ) 
                    {
                        $.ajax({
                            url: "ajax_lookup_service_catalog_item",
                            type: 'post',
                            dataType: "json",
                            data: 
                            {
                                search: request.term,
                                full: 1
                            },
                            success: function( data ) 
                            {
                                response( data );
                            }
                        });
                    },
                    minLength: 2,
                    select: function( event, ui ) 
                    {
//                        console.log(ui.item.value);
                        $("div.rightSidePanelOpener[data-item-id='" + ui.item.value + "']").click();
                        return false;
                    }
                });
            });
            
        });

        function saveUser()
        {
            var form = $(event.target).closest("form").get(0);
            $.ajax({
                url: form.action,
                type: "POST",
                data: new FormData(form),
                processData: false,
                contentType: false,
                success: function(resText){
                    var res = jQuery.parseJSON( resText );
                    console.log(res);
                    if (res.error == "") {
//                        $("input[name='requested_for_name']").autocomplete('search', res.user_name).data("ui-autocomplete").menu.element.children().first().click();;
                        $("input[name='requested_for_name']").val(res.user_name);
                        $("input[name='requested_for_id']").val(res.user_id);
                        $(".rightSidePanelUserCloser").click();
                    } else {
                        alert(res.error);
                    }
    //                window.location.assign("admin_users.jsp");
                },
                error: function(jqXHR, textStatus)
                {
                    switch (jqXHR.status) {
                        case 401:
                            window.location.assign("<%=props.get("login_url").toString()%>");
                            break;
                        case 403:
                            alert("No permission!");
                            break;
                        default:
                            alert("Error processing request! " + textStatus);
                    }
                }
            });
        }

    </script>
    <% } %>

    </body>
</html>

<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>
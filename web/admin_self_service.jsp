<!--Copyright 2019 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String FINANCIAL = session.getAttribute("financial").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String CONTACT = session.getAttribute("contact").toString();
        String CX = session.getAttribute("cx").toString();
        String SURVEY = session.getAttribute("survey").toString();
        String SLA = session.getAttribute("sla").toString();        
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        String METRIC = session.getAttribute("metric").toString();
        
        
        if(!ADMINISTRATION.equalsIgnoreCase("true"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);  
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                //get self_service_enabled
                String self_service_enabled = "false";   
                try
                {
                    self_service_enabled = db.get_configuration_properties.by_name(con, "self_service_enabled").trim(); 
                    if(self_service_enabled == null || self_service_enabled.equalsIgnoreCase(""))
                    {
                        self_service_enabled = "false";
                    }
                    else if(self_service_enabled.equalsIgnoreCase("true"))
                    {
                        self_service_enabled = "true";
                    }                        
                }
                catch (Exception e)
                {
                    self_service_enabled = "false";
                }
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <!-- END PAGE LEVEL CSS-->

<div class="clr whtbg p-10 mb-15">
    <h1 class="large-font boldFont">Self Service Portal Options</h1>
</div>

<jsp:include page="alerts.jsp" />

<div class="sort-tbl-holder">
            <form class="form" action="admin_self_service" method="post">
                                    <div class="row">
                                        <div class="col-xl-3 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="self_service_enabled">Enabled</label>
                                                <br>
                                                <%
                                                String checked = "";
                                                if(self_service_enabled.equalsIgnoreCase("true"))
                                                {
                                                    checked = "CHECKED";
                                                }
                                                %>
                                                <input <%=checked%> type="checkbox" id="self_service_enabled" name="self_service_enabled" value="true"/>
                                            </div>
                                        </div>
                                        <!--
                                        <div class="col-xl-3 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="self_service_login_required">Login Required</label>
                                                <br>
                                                <input <%=checked%> type="checkbox" id="self_service_login_required" name="self_service_login_required" value="ON"/>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="self_service_enable_captcha">CAPTCHA Required</label>
                                                <br>
                                                <input <%=checked%> type="checkbox" id="self_service_enable_captcha" name="self_service_enable_captcha" value="ON"/>
                                            </div>
                                        </div>
                                        -->
                                    </div>
                                    <div class="row ">
                                        <div class="col-md-12 pt-30 pb-30">
                                            <button type="button" onclick="javascript:location.href='admin.jsp'"  class="btn btn-outline-primary customBtn lg waves-effect">Cancel</button>
                                            <button type="submit" class="btn btn-primary-new customBtn lg waves-effect">
                                                Save
                                            </button>
                                        </div>
                                    </div>                                
            </form>                                
</div>

    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->   
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_self_service.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_self_service.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

<%@page import="org.apache.commons.lang3.StringUtils"%>
<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_users_add.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        Logger logger = LogManager.getLogger();
        String home_page = session.getAttribute("home_page").toString();
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        try 
        {
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            String user_info[] = db.get_users.by_id(con, user_id);
            String password = support.encrypt_utils.decrypt(context_dir, user_info[2]);
            ArrayList<String[]> all_groups = db.get_groups.all(con);
            String force2col = StringUtils.defaultString(request.getParameter("force2col"));

            
            /*returnString[0] = rs.getString("id");
            returnString[1] = rs.getString("username");
            returnString[2] = rs.getString("password");
            returnString[3] = check_for_null(rs.getString("first"));
            returnString[4] = check_for_null(rs.getString("mi"));
            returnString[5] = check_for_null(rs.getString("last"));
            returnString[6] = check_for_null(rs.getString("address_1"));
            returnString[7] = check_for_null(rs.getString("address_2"));
            returnString[8] = check_for_null(rs.getString("city"));
            returnString[9] = check_for_null(rs.getString("state"));
            returnString[10] = check_for_null(rs.getString("zip"));
            returnString[11] = check_for_null(rs.getString("location"));
            returnString[12] = check_for_null(rs.getString("department"));
            returnString[13] = check_for_null(rs.getString("site"));
            returnString[14] = check_for_null(rs.getString("company"));
            returnString[15] = check_for_null(rs.getString("email"));
            returnString[16] = check_for_null(rs.getString("phone_office"));
            returnString[17] = check_for_null(rs.getString("phone_mobile"));
            returnString[18] = check_for_null(rs.getString("notes"));
            returnString[19] = check_for_null(rs.getString("vip"));
            returnString[20] = check_for_null(rs.getString("is_admin"));
            returnString[21] = check_for_null(rs.getString("tz_name"));
            returnString[22] = check_for_null(rs.getString("tz_time"));
            returnString[23] = check_for_null(rs.getString("external_id"));*/
%>
<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>	

<jsp:include page='menu_service_desk.jsp'>
    <jsp:param name="active_menu" value=""/>
</jsp:include>
<!-- Page specific CSS-->

<!--    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    End Page specific CSS
     BEGIN VENDOR CSS
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
     END VENDOR CSS
     BEGIN MODERN CSS
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
     END MODERN CSS
     BEGIN Page Level CSS
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
     END Page Level CSS-->

<div class="clr whtbg p-10 mb-15 position-relative">
    <h1 class="large-font boldFont">&nbsp;</h1>
    <div class="custom-tabs clr ">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="user_profile" data-toggle="tab" href="#tab1" aria-controls="activeIcon12" aria-expanded="true"></i>User Profile</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="notification_tab" data-toggle="tab" href="#tab2" aria-controls="activeIcon12" aria-expanded="false">Email Notifications</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="mail_tab" data-toggle="tab" href="#tab3" aria-controls="activeIcon12" aria-expanded="false">Personal Email Formats</a>
            </li>
        </ul><!-- Tab panes -->
    </div>
</div>

                            <div class="tab-content px-1 pt-1">
                                <div role="tabpanel" class="tab-pane active" id="tab1" aria-labelledby="user_profile" aria-expanded="true">
                                    <form class="form" action="edit_profile" method="post" enctype="multipart/form-data" onsubmit="return validateForm()">
                                        <input type="hidden" name="id" id="id" value="<%=user_id%>"/>
                                            <div class="row row-cols-1 row-cols-md-2<%=(force2col.equals("") ? " row-cols-xl-3 row-cols-xxl-4" : "")%>">
                                                <div class="col">
                                                    <div class="field-title">
                                                        <label class="formLabel md has-txt-only mb-15" for="first">First Name</label>
                                                    </div>
                                                    <div class="formField clr md border-0 px-0">
                                                        <input type="text" id="first" name="first" class="form-control" value="<%=user_info[3]%>" />
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="field-title">
                                                        <label class="formLabel md has-txt-only mb-15" for="mi">MI</label>
                                                    </div>
                                                    <div class="formField clr md border-0 px-0">
                                                        <input type="text" id="mi" name="mi" class="form-control" value="<%=user_info[4]%>" />
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="field-title">
                                                        <label class="formLabel md has-txt-only mb-15" for="last">Last Name</label>
                                                    </div>
                                                    <div class="formField clr md border-0 px-0">
                                                        <input type="text" id="last" name="last" class="form-control" value="<%=user_info[5]%>" />
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="field-title">
                                                        <label class="formLabel md has-txt-only mb-15" for="username">User Name</label>
                                                    </div>
                                                    <div class="formField clr md border-0 px-0">
                                                        <input READONLY type="text" id="username" name="username" class="form-control" value="<%=user_info[1]%>"/>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="field-title">
                                                        <label class="formLabel md has-txt-only mb-15" for="password">Password</label>
                                                    </div>
                                                    <div class="formField clr md border-0 px-0">
                                                        <input type="password" id="password" class="form-control" name="password" value="<%=password%>" />
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="field-title">
                                                        <label class="formLabel md has-txt-only mb-15" for="address_1">Address <em>(1st Line)</em></label>
                                                    </div>
                                                    <div class="formField clr md border-0 px-0">
                                                        <input type="text" id="address_1" name="address_1" class="form-control" value="<%=user_info[6]%>"/>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="field-title">
                                                        <label class="formLabel md has-txt-only mb-15" for="address_2">Address <em>(2nd Line)</em></label>
                                                    </div>
                                                    <div class="formField clr md border-0 px-0">
                                                        <input type="text" id="address_2" name="address_2" class="form-control" value="<%=user_info[7]%>"/>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="field-title">
                                                        <label class="formLabel md has-txt-only mb-15" for="city">City</label>
                                                    </div>
                                                    <div class="formField clr md border-0 px-0">
                                                        <input type="text" id="city" name="city" class="form-control" value="<%=user_info[8]%>"/>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="field-title">
                                                        <label class="formLabel md has-txt-only mb-15" for="state">State</label>
                                                    </div>
                                                    <div class="formField clr md border-0 px-0">
                                                        <input type="text" id="state" name="state" class="form-control" value="<%=user_info[9]%>"/>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="field-title">
                                                        <label class="formLabel md has-txt-only mb-15" for="zip">Zip</label>
                                                    </div>
                                                    <div class="formField clr md border-0 px-0">
                                                        <input type="text" id="zip" name="zip" class="form-control" value="<%=user_info[10]%>"/>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="field-title">
                                                        <label class="formLabel md has-txt-only mb-15" for="department">Department<i class="la la-search" style="font-size: 14px;"></i></label>
                                                    </div>
                                                    <div class="formField clr md border-0 px-0">
                                                        <input type="text" id="department" name="department" class="form-control" value="<%=user_info[12]%>"/>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="field-title">
                                                        <label class="formLabel md has-txt-only mb-15" for="location">Location<i class="la la-search" style="font-size: 14px;"></i></label>
                                                    </div>
                                                    <div class="formField clr md border-0 px-0">
                                                        <input type="text" id="location" name="location" class="form-control" value="<%=user_info[11]%>"/>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="field-title">
                                                        <label class="formLabel md has-txt-only mb-15" for="site">Site<i class="la la-search" style="font-size: 14px;"></i></label>
                                                    </div>
                                                    <div class="formField clr md border-0 px-0">
                                                        <input type="text" id="site" name="site" class="form-control" value="<%=user_info[13]%>"/>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="field-title">
                                                        <label class="formLabel md has-txt-only mb-15" for="company">Company<i class="la la-search" style="font-size: 14px;"></i></label>
                                                    </div>
                                                    <div class="formField clr md border-0 px-0">
                                                        <input type="text" id="company" name="company" class="form-control" value="<%=user_info[14]%>"/>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="field-title">
                                                        <label class="formLabel md has-txt-only mb-15" for="phone_office">Office Phone</label>
                                                    </div>
                                                    <div class="formField clr md border-0 px-0">
                                                        <input type="text" id="phone_office" name="phone_office" class="form-control" value="<%=user_info[16]%>"/>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="field-title">
                                                        <label class="formLabel md has-txt-only mb-15" for="phone_mobile">Mobile Phone</label>
                                                    </div>
                                                    <div class="formField clr md border-0 px-0">
                                                        <input type="text" id="phone_mobile" name="phone_mobile" class="form-control" value="<%=user_info[17]%>"/>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="field-title">
                                                        <label class="formLabel md has-txt-only mb-15" for="email">Email</label>
                                                    </div>
                                                    <div class="formField clr md border-0 px-0">
                                                        <input type="text" id="email" name="email" class="form-control" value="<%=user_info[15]%>"/>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="field-title">
                                                        <label class="formLabel md has-txt-only mb-15" for="tz">Users Time Zone?</label>
                                                    </div>
                                                    <div class="formField clr md border-0 px-0">
                                                        <select id="tz" name="tz" class="form-control">
                                                            <%
                                                            String user_tz_name = user_info[21];
                                                            String user_tz_time = user_info[22];
                                                            String default_tz = "US/Eastern";
                                                            String selected = "";
                                                            ArrayList<String[]> timezones = db.get_timezone.all(con);
                                                            for(int a = 0; a < timezones.size(); a++)
                                                            {
                                                                String timezone[] = timezones.get(a);
                                                                selected = "";
                                                                if(timezone[1].equalsIgnoreCase(user_tz_name))
                                                                {
                                                                    selected = "SELECTED";
                                                                }
                                                                %>
                                                                <option <%=selected%> value="<%=timezone[1] + "," + timezone[2]%>">(<%=timezone[2] + ")   " + timezone[1]%></option>
                                                                <%
                                                            }
                                                            %>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="w-100"></div>
                                                <div class="col" style="max-width: 50% !important;flex: 0 0 50%;">
                                                    <div class="field-title">
                                                        <label class="formLabel md has-txt-only mb-15">
                                                            Avatar
                                                        </label>
                                                    </div>
                                                    <div class="formField clr md border-0 custom-file">
                                                        <input type="file" class="custom-file-input" id="file" name="file" accept=".gif,.jpg,.jpeg,.png">
                                                        <label class="custom-file-label" for="file">Choose file</label>
                                                    </div>
                                                </div>
                                                <div class="w-100"></div>
                                                <div class="col">
                                                    <div class="formField clr md border-0">
                                                        <% 
                                                        if (user_info[24] != null && !user_info[24].equals(""))
                                                        {
                                                            %>
                                                            <div class="clr tag dInlineBlock formField md whtbg mb-15 w-auto float-none">
                                                                <div class="img-holder rounded-circle">
                                                                    <img src="<%=props.get("couchdb.attachments_host") + "/" + props.get("couchdb.avatars_db") + "/" + user_info[24] + "/avatar"%>" width="100px" alt="user" class="rounded-circle">                                        
                                                                </div>
                                                                <!--<span><%=user_info[24]%></span>-->
                                                                <a data-id="<%=user_info[24]%>" href="javascript:void(0)" onclick="deleteAttachment()"><img class="icon" src="assets/images/svg/cross-icon.svg" alt=""></a>
                                                            </div>                    
                                                            <%
                                                        }
                                                        %>
                                                    </div>
                                                </div>
                                            </div>

                                        <div class="form-actions">
                                            <button type="button" onclick="javascript:window.history.back()" class="btn btn-warning mr-1">
                                                <i class="ft-x"></i> Cancel
                                            </button>
                                            <button type="submit" class="btn btn-info">
                                                &nbsp;&nbsp;<i class="ft-save"></i> Save&nbsp;&nbsp;
                                            </button>
                                        </div>
                                    </form>
                                </div> <!--End tab 1-->
                                <div role="tabpanel" class="tab-pane"  id="tab2" aria-labelledby="notification_tab" aria-expanded="false">
                                    <form class="form" action="edit_profile_notifications" method="post" onsubmit="return validateForm()">
                                        <%
                                        ArrayList<String[]> this_users_user_notifications = db.get_notifications.user_notifications_for_user(con, user_id);
                                        ArrayList<String[]> this_users_notifications = db.get_notifications.notifications_for_user(con, user_id);
                                        String checked = "";
                                        %>
                                        <div class="row">
                                            <div class="col-12">
                                                <h3>Send a Notification on the following Incident Events</h3>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-4 col-lg-6 col-12">
                                                <fieldset class="checkbox">
                                                    <label>
                                                        <%
                                                        for(int a = 0; a < this_users_user_notifications.size();a++)
                                                        {
                                                            String this_notification[] = this_users_user_notifications.get(a);
                                                            if(this_notification[2].equalsIgnoreCase("incident_create_assigned_to_me") && this_notification[3].equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                        }
                                                        %>
                                                        <input <%=checked%> type='checkbox' id="incident_create_assigned_to_me" name="incident_create_assigned_to_me" value="true"/>
                                                        Incident "Create" and "Assigned To Me"
                                                    </label>
                                                </fieldset>                                                
                                            </div>
                                            <div class="col-xl-8 col-lg-8 col-12">
                                                <fieldset class="checkbox">
                                                    <label>
                                                        <%
                                                        for(int a = 0; a < this_users_user_notifications.size();a++)
                                                        {
                                                            String this_notification[] = this_users_user_notifications.get(a);
                                                            if(this_notification[2].equalsIgnoreCase("incident_create_assigned_to_group") && this_notification[3].equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                        }
                                                        %>
                                                        <input <%=checked%> type='checkbox' id="incident_create_assigned_to_group" name="incident_create_assigned_to_group" value="true"/>
                                                        Incident "Create" and "Assigned To Group(s)"
                                                    </label>
                                                    <br>
                                                    <label for="incident_create_assigned_to_group_select">Group(s)</label>
                                                        <select id="incident_create_assigned_to_group_select" name="incident_create_assigned_to_group_select" class="select2 form-control" multiple="multiple">
                                                            <%
                                                            for(int a = 0; a < all_groups.size(); a++)
                                                            {
                                                                checked = "";
                                                                String this_group[] = all_groups.get(a);
                                                                for(int b = 0; b < this_users_notifications.size(); b++)
                                                                {
                                                                    String this_users_notification[] = this_users_notifications.get(b);
                                                                    if(this_users_notification[1].equalsIgnoreCase("incident_create_assigned_to_group") && this_group[0].equalsIgnoreCase(this_users_notification[3]))
                                                                    {
                                                                        checked = "SELECTED";
                                                                    }
                                                                }
                                                                %>
                                                                <option <%=checked%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                                <%
                                                            }
                                                            %>
                                                        </select>
                                                </fieldset> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12"><hr></div>
                                        </div>
                                            
                                        <div class="row">
                                            <div class="col-xl-4 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <%
                                                    checked = "";
                                                    for(int a = 0; a < this_users_user_notifications.size();a++)
                                                    {
                                                        String this_notification[] = this_users_user_notifications.get(a);
                                                        if(this_notification[2].equalsIgnoreCase("incident_update_assigned_to_me") && this_notification[3].equalsIgnoreCase("true"))
                                                        {
                                                            checked = "CHECKED";
                                                        }
                                                    }
                                                    %>
                                                    <input <%=checked%> type='checkbox' id="incident_update_assigned_to_me" name="incident_update_assigned_to_me" value="true"/>&nbsp;<label>Incident "Update" and "Assigned To Me"</label>
                                                </div>
                                            </div>
                                            <div class="col-xl-8 col-lg-8 col-12">
                                                <fieldset class="checkbox">
                                                    <label>
                                                        <%
                                                        checked = "";
                                                        for(int a = 0; a < this_users_user_notifications.size();a++)
                                                        {
                                                            String this_notification[] = this_users_user_notifications.get(a);
                                                            if(this_notification[2].equalsIgnoreCase("incident_update_assigned_to_group") && this_notification[3].equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                        }
                                                        %>
                                                        <input <%=checked%> type='checkbox' id="incident_update_assigned_to_group" name="incident_update_assigned_to_group" value="true"/>
                                                        Incident "Update" and "Assigned To Group(s)"
                                                    </label>
                                                    <br>
                                                    <label for="incident_update_assigned_to_group_select">Group(s)</label>
                                                        <select id="incident_update_assigned_to_group_select" name="incident_update_assigned_to_group_select" class="select2 form-control" multiple="multiple">
                                                            <%
                                                            for(int a = 0; a < all_groups.size(); a++)
                                                            {
                                                                checked = "";
                                                                String this_group[] = all_groups.get(a);
                                                                for(int b = 0; b < this_users_notifications.size(); b++)
                                                                {
                                                                    String this_users_notification[] = this_users_notifications.get(b);
                                                                    if(this_users_notification[1].equalsIgnoreCase("incident_update_assigned_to_group") && this_group[0].equalsIgnoreCase(this_users_notification[3]))
                                                                    {
                                                                        checked = "SELECTED";
                                                                    }
                                                                }
                                                                %>
                                                                <option <%=checked%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                                <%
                                                            }
                                                            %>
                                                        </select>
                                                </fieldset> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12"><hr></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-4 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <%
                                                    checked = "";
                                                    for(int a = 0; a < this_users_user_notifications.size();a++)
                                                    {
                                                        String this_notification[] = this_users_user_notifications.get(a);
                                                        if(this_notification[2].equalsIgnoreCase("incident_resolve_assigned_to_me") && this_notification[3].equalsIgnoreCase("true"))
                                                        {
                                                            checked = "CHECKED";
                                                        }
                                                    }
                                                    %>
                                                    <input <%=checked%> type='checkbox' id="incident_resolve_assigned_to_me" name="incident_resolve_assigned_to_me" value="true"/>&nbsp;<label>Incident "Resolve" and "Assigned To Me"</label>
                                                </div>
                                            </div>
                                            <div class="col-xl-8 col-lg-8 col-12">
                                                <fieldset class="checkbox">
                                                    <label>
                                                        <%
                                                        checked = "";
                                                        for(int a = 0; a < this_users_user_notifications.size();a++)
                                                        {
                                                            String this_notification[] = this_users_user_notifications.get(a);
                                                            if(this_notification[2].equalsIgnoreCase("incident_resolve_assigned_to_group") && this_notification[3].equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                        }
                                                        %>
                                                        <input <%=checked%> type='checkbox' id="incident_resolve_assigned_to_group" name="incident_resolve_assigned_to_group" value="true"/>
                                                        Incident "Resolved" and "Assigned To Group(s)"
                                                    </label>
                                                    <br>
                                                    <label for="incident_resolve_assigned_to_group_select">Group(s)</label>
                                                        <select id="incident_resolve_assigned_to_group_select" name="incident_resolve_assigned_to_group_select" class="select2 form-control" multiple="multiple">
                                                            <%
                                                            for(int a = 0; a < all_groups.size(); a++)
                                                            {
                                                                checked = "";
                                                                String this_group[] = all_groups.get(a);
                                                                for(int b = 0; b < this_users_notifications.size(); b++)
                                                                {
                                                                    String this_users_notification[] = this_users_notifications.get(b);
                                                                    if(this_users_notification[1].equalsIgnoreCase("incident_resolve_assigned_to_group") && this_group[0].equalsIgnoreCase(this_users_notification[3]))
                                                                    {
                                                                        checked = "SELECTED";
                                                                    }
                                                                }
                                                                %>
                                                                <option <%=checked%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                                <%
                                                            }
                                                            %>
                                                        </select>
                                                </fieldset> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12"><hr></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-4 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <%
                                                    checked = "";
                                                    for(int a = 0; a < this_users_user_notifications.size();a++)
                                                    {
                                                        String this_notification[] = this_users_user_notifications.get(a);
                                                        if(this_notification[2].equalsIgnoreCase("incident_close_assigned_to_me") && this_notification[3].equalsIgnoreCase("true"))
                                                        {
                                                            checked = "CHECKED";
                                                        }
                                                    }
                                                    %>
                                                    <input <%=checked%> type='checkbox' id="incident_close_assigned_to_me" name="incident_close_assigned_to_me" value="true"/>&nbsp;<label>Incident "Close" and "Assigned To Me"</label>
                                                </div>
                                            </div>
                                            <div class="col-xl-8 col-lg-8 col-12">
                                                <fieldset class="checkbox">
                                                    <label>
                                                        <%
                                                        checked = "";
                                                        for(int a = 0; a < this_users_user_notifications.size();a++)
                                                        {
                                                            String this_notification[] = this_users_user_notifications.get(a);
                                                            if(this_notification[2].equalsIgnoreCase("incident_close_assigned_to_group") && this_notification[3].equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                        }
                                                        %>
                                                        <input <%=checked%> type='checkbox' id="incident_close_assigned_to_group" name="incident_close_assigned_to_group" value="true"/>
                                                        Incident "Close" and "Assigned To Group(s)"
                                                    </label>
                                                    <br>
                                                    <label for="incident_closed_assigned_to_group_select">Group(s)</label>
                                                        <select id="incident_close_assigned_to_group_select" name="incident_close_assigned_to_group_select" class="select2 form-control" multiple="multiple">
                                                            <%
                                                            for(int a = 0; a < all_groups.size(); a++)
                                                            {
                                                                checked = "";
                                                                String this_group[] = all_groups.get(a);
                                                                for(int b = 0; b < this_users_notifications.size(); b++)
                                                                {
                                                                    String this_users_notification[] = this_users_notifications.get(b);
                                                                    if(this_users_notification[1].equalsIgnoreCase("incident_close_assigned_to_group") && this_group[0].equalsIgnoreCase(this_users_notification[3]))
                                                                    {
                                                                        checked = "SELECTED";
                                                                    }
                                                                }
                                                                %>
                                                                <option <%=checked%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                                <%
                                                            }
                                                            %>
                                                        </select>
                                                </fieldset> 
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-12"><hr size="4"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <h3>Send a Notification on the following Request Events</h3>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-4 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <%
                                                    checked = "";
                                                    for(int a = 0; a < this_users_user_notifications.size();a++)
                                                    {
                                                        String this_notification[] = this_users_user_notifications.get(a);
                                                        if(this_notification[2].equalsIgnoreCase("request_create_assigned_to_me") && this_notification[3].equalsIgnoreCase("true"))
                                                        {
                                                            checked = "CHECKED";
                                                        }
                                                    }
                                                    %>
                                                    <input <%=checked%> type='checkbox' id="request_create_assigned_to_me" name="request_create_assigned_to_me" value="true"/>&nbsp;<label>Request "Create" and "Assigned To Me"</label>
                                                </div>
                                            </div>
                                            <div class="col-xl-8 col-lg-8 col-12">
                                                <fieldset class="checkbox">
                                                    <label>
                                                        <%
                                                        checked = "";
                                                        for(int a = 0; a < this_users_user_notifications.size();a++)
                                                        {
                                                            String this_notification[] = this_users_user_notifications.get(a);
                                                            if(this_notification[2].equalsIgnoreCase("request_create_assigned_to_group") && this_notification[3].equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                        }
                                                        %>
                                                        <input <%=checked%> type='checkbox' id="request_create_assigned_to_group" name="request_create_assigned_to_group" value="true"/>
                                                        Request "Create" and "Assigned To Group(s)"
                                                    </label>
                                                    <br>
                                                    <label for="request_create_assigned_to_group_select">Group(s)</label>
                                                        <select id="request_create_assigned_to_group_select" name="request_create_assigned_to_group_select" class="select2 form-control" multiple="multiple">
                                                            <%
                                                            for(int a = 0; a < all_groups.size(); a++)
                                                            {
                                                                checked = "";
                                                                String this_group[] = all_groups.get(a);
                                                                for(int b = 0; b < this_users_notifications.size(); b++)
                                                                {
                                                                    String this_users_notification[] = this_users_notifications.get(b);
                                                                    if(this_users_notification[1].equalsIgnoreCase("request_create_assigned_to_group") && this_group[0].equalsIgnoreCase(this_users_notification[3]))
                                                                    {
                                                                        checked = "SELECTED";
                                                                    }
                                                                }
                                                                %>
                                                                <option <%=checked%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                                <%
                                                            }
                                                            %>
                                                        </select>
                                                </fieldset> 
                                            </div>
                                        </div>                
                                        <div class="row">
                                            <div class="col-12"><hr size="4"></div>
                                        </div>                
                                        <div class="row">
                                            <div class="col-xl-4 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <%
                                                    checked = "";
                                                    for(int a = 0; a < this_users_user_notifications.size();a++)
                                                    {
                                                        String this_notification[] = this_users_user_notifications.get(a);
                                                        if(this_notification[2].equalsIgnoreCase("request_update_assigned_to_me") && this_notification[3].equalsIgnoreCase("true"))
                                                        {
                                                            checked = "CHECKED";
                                                        }
                                                    }
                                                    %>
                                                    <input <%=checked%> type='checkbox' id="request_update_assigned_to_me" name="request_update_assigned_to_me" value="true"/>&nbsp;<label>Request "Update" and "Assigned To Me"</label>
                                                </div>
                                            </div>
                                            <div class="col-xl-8 col-lg-8 col-12">
                                                <fieldset class="checkbox">
                                                    <label>
                                                        <%
                                                        checked = "";
                                                        for(int a = 0; a < this_users_user_notifications.size();a++)
                                                        {
                                                            String this_notification[] = this_users_user_notifications.get(a);
                                                            if(this_notification[2].equalsIgnoreCase("request_update_assigned_to_group") && this_notification[3].equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                        }
                                                        %>
                                                        <input <%=checked%> type='checkbox' id="request_update_assigned_to_group" name="request_update_assigned_to_group" value="true"/>
                                                        Request "Update" and "Assigned To Group(s)"
                                                    </label>
                                                    <br>
                                                    <label for="request_update_assigned_to_group_select">Group(s)</label>
                                                        <select id="request_update_assigned_to_group_select" name="request_update_assigned_to_group_select" class="select2 form-control" multiple="multiple">
                                                            <%
                                                            for(int a = 0; a < all_groups.size(); a++)
                                                            {
                                                                checked = "";
                                                                String this_group[] = all_groups.get(a);
                                                                for(int b = 0; b < this_users_notifications.size(); b++)
                                                                {
                                                                    String this_users_notification[] = this_users_notifications.get(b);
                                                                    if(this_users_notification[1].equalsIgnoreCase("request_update_assigned_to_group") && this_group[0].equalsIgnoreCase(this_users_notification[3]))
                                                                    {
                                                                        checked = "SELECTED";
                                                                    }
                                                                }
                                                                %>
                                                                <option <%=checked%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                                <%
                                                            }
                                                            %>
                                                        </select>
                                                </fieldset> 
                                            </div>
                                        </div>                
                                        <div class="row">
                                            <div class="col-12"><hr size="4"></div>
                                        </div>                
                                        <div class="row">
                                            <div class="col-xl-4 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <%
                                                    checked = "";
                                                    for(int a = 0; a < this_users_user_notifications.size();a++)
                                                    {
                                                        String this_notification[] = this_users_user_notifications.get(a);
                                                        if(this_notification[2].equalsIgnoreCase("request_resolve_assigned_to_me") && this_notification[3].equalsIgnoreCase("true"))
                                                        {
                                                            checked = "CHECKED";
                                                        }
                                                    }
                                                    %>
                                                    <input <%=checked%> type='checkbox' id="request_resolve_assigned_to_me" name="request_resolve_assigned_to_me" value="true"/>&nbsp;<label>Request "Resolve" and "Assigned To Me"</label>
                                                </div>
                                            </div>
                                            <div class="col-xl-8 col-lg-8 col-12">
                                                <fieldset class="checkbox">
                                                    <label>
                                                        <%
                                                        checked = "";
                                                        for(int a = 0; a < this_users_user_notifications.size();a++)
                                                        {
                                                            String this_notification[] = this_users_user_notifications.get(a);
                                                            if(this_notification[2].equalsIgnoreCase("request_resolve_assigned_to_group") && this_notification[3].equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                        }
                                                        %>
                                                        <input <%=checked%> type='checkbox' id="request_resolve_assigned_to_group" name="request_resolve_assigned_to_group" value="true"/>
                                                        Request "Resolved" and "Assigned To Group(s)"
                                                    </label>
                                                    <br>
                                                    <label for="request_resolve_assigned_to_group_select">Group(s)</label>
                                                        <select id="request_resolve_assigned_to_group_select" name="request_resolve_assigned_to_group_select" class="select2 form-control" multiple="multiple">
                                                            <%
                                                            for(int a = 0; a < all_groups.size(); a++)
                                                            {
                                                                checked = "";
                                                                String this_group[] = all_groups.get(a);
                                                                for(int b = 0; b < this_users_notifications.size(); b++)
                                                                {
                                                                    String this_users_notification[] = this_users_notifications.get(b);
                                                                    if(this_users_notification[1].equalsIgnoreCase("request_resolve_assigned_to_group") && this_group[0].equalsIgnoreCase(this_users_notification[3]))
                                                                    {
                                                                        checked = "SELECTED";
                                                                    }
                                                                }
                                                                %>
                                                                <option <%=checked%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                                <%
                                                            }
                                                            %>
                                                        </select>
                                                </fieldset> 
                                            </div>
                                        </div>                
                                        <div class="row">
                                            <div class="col-12"><hr size="4"></div>
                                        </div>                 
                                        <div class="row">
                                            <div class="col-xl-4 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <%
                                                    checked = "";
                                                    for(int a = 0; a < this_users_user_notifications.size();a++)
                                                    {
                                                        String this_notification[] = this_users_user_notifications.get(a);
                                                        if(this_notification[2].equalsIgnoreCase("request_close_assigned_to_me") && this_notification[3].equalsIgnoreCase("true"))
                                                        {
                                                            checked = "CHECKED";
                                                        }
                                                    }
                                                    %>
                                                    <input <%=checked%> type='checkbox' id="request_close_assigned_to_me" name="request_close_assigned_to_me" value="true"/>&nbsp;<label>Request "Close" and "Assigned To Me"</label>
                                                </div>
                                            </div>
                                            <div class="col-xl-8 col-lg-8 col-12">
                                                <fieldset class="checkbox">
                                                    <label>
                                                        <%
                                                        checked = "";
                                                        for(int a = 0; a < this_users_user_notifications.size();a++)
                                                        {
                                                            String this_notification[] = this_users_user_notifications.get(a);
                                                            if(this_notification[2].equalsIgnoreCase("request_close_assigned_to_group") && this_notification[3].equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                        }
                                                        %>
                                                        <input <%=checked%> type='checkbox' id="request_close_assigned_to_group" name="request_close_assigned_to_group" value="true"/>
                                                        Request "Close" and "Assigned To Group(s)"
                                                    </label>
                                                    <br>
                                                    <label for="request_close_assigned_to_group_select">Group(s)</label>
                                                        <select id="request_close_assigned_to_group_select" name="request_close_assigned_to_group_select" class="select2 form-control" multiple="multiple">
                                                            <%
                                                            for(int a = 0; a < all_groups.size(); a++)
                                                            {
                                                                checked = "";
                                                                String this_group[] = all_groups.get(a);
                                                                for(int b = 0; b < this_users_notifications.size(); b++)
                                                                {
                                                                    String this_users_notification[] = this_users_notifications.get(b);
                                                                    if(this_users_notification[1].equalsIgnoreCase("request_close_assigned_to_group") && this_group[0].equalsIgnoreCase(this_users_notification[3]))
                                                                    {
                                                                        checked = "SELECTED";
                                                                    }
                                                                }
                                                                %>
                                                                <option <%=checked%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                                <%
                                                            }
                                                            %>
                                                        </select>
                                                </fieldset> 
                                            </div>
                                        </div>                
                                        <div class="row">
                                            <div class="col-12"><hr size="4"></div>
                                        </div>  
                                        <div class="row">
                                            <div class="col-12">
                                                <h3>Send a Notification on the following Job Events</h3>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-4 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <%
                                                    checked = "";
                                                    for(int a = 0; a < this_users_user_notifications.size();a++)
                                                    {
                                                        String this_notification[] = this_users_user_notifications.get(a);
                                                        if(this_notification[2].equalsIgnoreCase("job_create_assigned_to_me") && this_notification[3].equalsIgnoreCase("true"))
                                                        {
                                                            checked = "CHECKED";
                                                        }
                                                    }
                                                    %>
                                                    <input <%=checked%> type='checkbox' id="job_create_assigned_to_me" name="job_create_assigned_to_me" value="true"/>&nbsp;<label>Job "Create" and "Assigned To Me"</label>
                                                </div>
                                            </div>
                                            <div class="col-xl-8 col-lg-8 col-12">
                                                <fieldset class="checkbox">
                                                    <label>
                                                        <%
                                                        checked = "";
                                                        for(int a = 0; a < this_users_user_notifications.size();a++)
                                                        {
                                                            String this_notification[] = this_users_user_notifications.get(a);
                                                            if(this_notification[2].equalsIgnoreCase("job_create_assigned_to_group") && this_notification[3].equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                        }
                                                        %>
                                                        <input <%=checked%> type='checkbox' id="job_create_assigned_to_group" name="job_create_assigned_to_group" value="true"/>
                                                        Job "Create" and "Assigned To Group(s)"
                                                    </label>
                                                    <br>
                                                    <label for="job_create_assigned_to_group_select">Group(s)</label>
                                                        <select id="job_create_assigned_to_group_select" name="job_create_assigned_to_group_select" class="select2 form-control" multiple="multiple">
                                                            <%
                                                            for(int a = 0; a < all_groups.size(); a++)
                                                            {
                                                                checked = "";
                                                                String this_group[] = all_groups.get(a);
                                                                for(int b = 0; b < this_users_notifications.size(); b++)
                                                                {
                                                                    String this_users_notification[] = this_users_notifications.get(b);
                                                                    if(this_users_notification[1].equalsIgnoreCase("job_create_assigned_to_group") && this_group[0].equalsIgnoreCase(this_users_notification[3]))
                                                                    {
                                                                        checked = "SELECTED";
                                                                    }
                                                                }
                                                                %>
                                                                <option <%=checked%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                                <%
                                                            }
                                                            %>
                                                        </select>
                                                </fieldset> 
                                            </div>
                                        </div>                
                                        <div class="row">
                                            <div class="col-12"><hr size="4"></div>
                                        </div>                 
                                        
                                        <div class="row">
                                            <div class="col-xl-4 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <%
                                                    checked = "";
                                                    for(int a = 0; a < this_users_user_notifications.size();a++)
                                                    {
                                                        String this_notification[] = this_users_user_notifications.get(a);
                                                        if(this_notification[2].equalsIgnoreCase("job_update_assigned_to_me") && this_notification[3].equalsIgnoreCase("true"))
                                                        {
                                                            checked = "CHECKED";
                                                        }
                                                    }
                                                    %>
                                                    <input <%=checked%> type='checkbox' id="job_update_assigned_to_me" name="job_update_assigned_to_me" value="true"/>&nbsp;<label>Job "Update" and "Assigned To Me"</label>
                                                </div>
                                            </div>
                                            <div class="col-xl-8 col-lg-8 col-12">
                                                <fieldset class="checkbox">
                                                    <label>
                                                        <%
                                                        checked = "";
                                                        for(int a = 0; a < this_users_user_notifications.size();a++)
                                                        {
                                                            String this_notification[] = this_users_user_notifications.get(a);
                                                            if(this_notification[2].equalsIgnoreCase("job_update_assigned_to_group") && this_notification[3].equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                        }
                                                        %>
                                                        <input <%=checked%> type='checkbox' id="job_update_assigned_to_group" name="job_update_assigned_to_group" value="true"/>
                                                        Job "Update" and "Assigned To Group(s)"
                                                    </label>
                                                    <br>
                                                    <label for="job_update_assigned_to_group_select">Group(s)</label>
                                                        <select id="job_update_assigned_to_group_select" name="job_update_assigned_to_group_select" class="select2 form-control" multiple="multiple">
                                                            <%
                                                            for(int a = 0; a < all_groups.size(); a++)
                                                            {
                                                                checked = "";
                                                                String this_group[] = all_groups.get(a);
                                                                for(int b = 0; b < this_users_notifications.size(); b++)
                                                                {
                                                                    String this_users_notification[] = this_users_notifications.get(b);
                                                                    if(this_users_notification[1].equalsIgnoreCase("job_update_assigned_to_group") && this_group[0].equalsIgnoreCase(this_users_notification[3]))
                                                                    {
                                                                        checked = "SELECTED";
                                                                    }
                                                                }
                                                                %>
                                                                <option <%=checked%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                                <%
                                                            }
                                                            %>
                                                        </select>
                                                </fieldset> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12"><hr size="4"></div>
                                        </div>  
                                        <div class="row">
                                            <div class="col-xl-4 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <%
                                                    checked = "";
                                                    for(int a = 0; a < this_users_user_notifications.size();a++)
                                                    {
                                                        String this_notification[] = this_users_user_notifications.get(a);
                                                        if(this_notification[2].equalsIgnoreCase("job_close_assigned_to_me") && this_notification[3].equalsIgnoreCase("true"))
                                                        {
                                                            checked = "CHECKED";
                                                        }
                                                    }
                                                    %>
                                                    <input <%=checked%> type='checkbox' id="job_close_assigned_to_me" name="job_close_assigned_to_me" value="true"/>&nbsp;<label>Job "Close" and "Assigned To Me"</label>
                                                </div>
                                            </div>
                                            <div class="col-xl-8 col-lg-8 col-12">
                                                <fieldset class="checkbox">
                                                    <label>
                                                        <%
                                                        checked = "";
                                                        for(int a = 0; a < this_users_user_notifications.size();a++)
                                                        {
                                                            String this_notification[] = this_users_user_notifications.get(a);
                                                            if(this_notification[2].equalsIgnoreCase("job_close_assigned_to_group") && this_notification[3].equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                        }
                                                        %>
                                                        <input <%=checked%> type='checkbox' id="job_close_assigned_to_group" name="job_close_assigned_to_group" value="true"/>
                                                        Job "Close" and "Assigned To Group(s)"
                                                    </label>
                                                    <br>
                                                    <label for="job_close_assigned_to_group_select">Group(s)</label>
                                                        <select id="job_close_assigned_to_group_select" name="job_close_assigned_to_group_select" class="select2 form-control" multiple="multiple">
                                                            <%
                                                            for(int a = 0; a < all_groups.size(); a++)
                                                            {
                                                                checked = "";
                                                                String this_group[] = all_groups.get(a);
                                                                for(int b = 0; b < this_users_notifications.size(); b++)
                                                                {
                                                                    String this_users_notification[] = this_users_notifications.get(b);
                                                                    if(this_users_notification[1].equalsIgnoreCase("job_close_assigned_to_group") && this_group[0].equalsIgnoreCase(this_users_notification[3]))
                                                                    {
                                                                        checked = "SELECTED";
                                                                    }
                                                                }
                                                                %>
                                                                <option <%=checked%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                                <%
                                                            }
                                                            %>
                                                        </select>
                                                </fieldset> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12"><hr size="4"></div>
                                        </div>  
                                        <div class="row">
                                            <div class="col-12">
                                                <h3>Send a Notification on the following Project Task Events</h3>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-4 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <%
                                                    checked = "";
                                                    for(int a = 0; a < this_users_user_notifications.size();a++)
                                                    {
                                                        String this_notification[] = this_users_user_notifications.get(a);
                                                        if(this_notification[2].equalsIgnoreCase("task_create_assigned_to_me") && this_notification[3].equalsIgnoreCase("true"))
                                                        {
                                                            checked = "CHECKED";
                                                        }
                                                    }
                                                    %>
                                                    <input <%=checked%> type='checkbox' id="task_create_assigned_to_me" name="task_create_assigned_to_me" value="true"/>&nbsp;<label>Task "Create" and "Assigned To Me"</label>
                                                </div>
                                            </div>
                                            <div class="col-xl-8 col-lg-8 col-12">
                                                <fieldset class="checkbox">
                                                    <label>
                                                        <%
                                                        checked = "";
                                                        for(int a = 0; a < this_users_user_notifications.size();a++)
                                                        {
                                                            String this_notification[] = this_users_user_notifications.get(a);
                                                            if(this_notification[2].equalsIgnoreCase("task_create_assigned_to_group") && this_notification[3].equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                        }
                                                        %>
                                                        <input <%=checked%> type='checkbox' id="task_create_assigned_to_group" name="task_create_assigned_to_group" value="true"/>
                                                        Task "Create" and "Assigned To Group(s)"
                                                    </label>
                                                    <br>
                                                    <label for="task_create_assigned_to_group_select">Group(s)</label>
                                                        <select id="task_create_assigned_to_group_select" name="task_create_assigned_to_group_select" class="select2 form-control" multiple="multiple">
                                                            <%
                                                            for(int a = 0; a < all_groups.size(); a++)
                                                            {
                                                                checked = "";
                                                                String this_group[] = all_groups.get(a);
                                                                for(int b = 0; b < this_users_notifications.size(); b++)
                                                                {
                                                                    String this_users_notification[] = this_users_notifications.get(b);
                                                                    if(this_users_notification[1].equalsIgnoreCase("task_create_assigned_to_group") && this_group[0].equalsIgnoreCase(this_users_notification[3]))
                                                                    {
                                                                        checked = "SELECTED";
                                                                    }
                                                                }
                                                                %>
                                                                <option <%=checked%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                                <%
                                                            }
                                                            %>
                                                        </select>
                                                </fieldset> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12"><hr size="4"></div>
                                        </div>  
                                        <div class="row">
                                            <div class="col-xl-4 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <%
                                                    checked = "";
                                                    for(int a = 0; a < this_users_user_notifications.size();a++)
                                                    {
                                                        String this_notification[] = this_users_user_notifications.get(a);
                                                        if(this_notification[2].equalsIgnoreCase("task_update_assigned_to_me") && this_notification[3].equalsIgnoreCase("true"))
                                                        {
                                                            checked = "CHECKED";
                                                        }
                                                    }
                                                    %>
                                                    <input <%=checked%> type='checkbox' id="task_update_assigned_to_me" name="task_update_assigned_to_me" value="true"/>&nbsp;<label>Task "Update" and "Assigned To Me"</label>
                                                </div>
                                            </div>
                                            <div class="col-xl-8 col-lg-8 col-12">
                                                <fieldset class="checkbox">
                                                    <label>
                                                        <%
                                                        checked = "";
                                                        for(int a = 0; a < this_users_user_notifications.size();a++)
                                                        {
                                                            String this_notification[] = this_users_user_notifications.get(a);
                                                            if(this_notification[2].equalsIgnoreCase("task_update_assigned_to_group") && this_notification[3].equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                        }
                                                        %>
                                                        <input <%=checked%> type='checkbox' id="task_update_assigned_to_group" name="task_update_assigned_to_group" value="true"/>
                                                        Task "Update" and "Assigned To Group(s)"
                                                    </label>
                                                    <br>
                                                    <label for="task_update_assigned_to_group_select">Group(s)</label>
                                                        <select id="task_update_assigned_to_group_select" name="task_update_assigned_to_group_select" class="select2 form-control" multiple="multiple">
                                                            <%
                                                            for(int a = 0; a < all_groups.size(); a++)
                                                            {
                                                                checked = "";
                                                                String this_group[] = all_groups.get(a);
                                                                for(int b = 0; b < this_users_notifications.size(); b++)
                                                                {
                                                                    String this_users_notification[] = this_users_notifications.get(b);
                                                                    if(this_users_notification[1].equalsIgnoreCase("task_update_assigned_to_group") && this_group[0].equalsIgnoreCase(this_users_notification[3]))
                                                                    {
                                                                        checked = "SELECTED";
                                                                    }
                                                                }
                                                                %>
                                                                <option <%=checked%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                                <%
                                                            }
                                                            %>
                                                        </select>
                                                </fieldset> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12"><hr size="4"></div>
                                        </div> 
                                        <div class="row">
                                            <div class="col-xl-4 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <%
                                                    checked = "";
                                                    for(int a = 0; a < this_users_user_notifications.size();a++)
                                                    {
                                                        String this_notification[] = this_users_user_notifications.get(a);
                                                        if(this_notification[2].equalsIgnoreCase("task_close_assigned_to_me") && this_notification[3].equalsIgnoreCase("true"))
                                                        {
                                                            checked = "CHECKED";
                                                        }
                                                    }
                                                    %>
                                                    <input <%=checked%> type='checkbox' id="task_close_assigned_to_me" name="task_close_assigned_to_me" value="true"/>&nbsp;<label>Task "Closed" and "Assigned To Me"</label>
                                                </div>
                                            </div>
                                        
                                            <div class="col-xl-8 col-lg-8 col-12">
                                                <fieldset class="checkbox">
                                                    <label>
                                                        <%
                                                        checked = "";
                                                        for(int a = 0; a < this_users_user_notifications.size();a++)
                                                        {
                                                            String this_notification[] = this_users_user_notifications.get(a);
                                                            if(this_notification[2].equalsIgnoreCase("task_close_assigned_to_group") && this_notification[3].equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                        }
                                                        %>
                                                        <input <%=checked%> type='checkbox' id="task_close_assigned_to_group" name="task_close_assigned_to_group" value="true"/>
                                                        Task "Closed" and "Assigned To Group(s)"
                                                    </label>
                                                    <br>
                                                    <label for="task_close_assigned_to_group_select">Group(s)</label>
                                                        <select id="task_close_assigned_to_group_select" name="task_close_assigned_to_group_select" class="select2 form-control" multiple="multiple">
                                                            <%
                                                            for(int a = 0; a < all_groups.size(); a++)
                                                            {
                                                                checked = "";
                                                                String this_group[] = all_groups.get(a);
                                                                for(int b = 0; b < this_users_notifications.size(); b++)
                                                                {
                                                                    String this_users_notification[] = this_users_notifications.get(b);
                                                                    if(this_users_notification[1].equalsIgnoreCase("task_close_assigned_to_group") && this_group[0].equalsIgnoreCase(this_users_notification[3]))
                                                                    {
                                                                        checked = "SELECTED";
                                                                    }
                                                                }
                                                                %>
                                                                <option <%=checked%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                                <%
                                                            }
                                                            %>
                                                        </select>
                                                </fieldset> 
                                            </div>
                                        </div> 
                                        <div class="form-actions">
                                            <button type="button" onclick="javascript:window.history.back()" class="btn btn-warning mr-1">
                                                <i class="ft-x"></i> Cancel
                                            </button>
                                            <button type="submit" class="btn btn-info">
                                                &nbsp;&nbsp;<i class="ft-save"></i> Save&nbsp;&nbsp;
                                            </button>
                                        </div>
                                    </form>
                                </div> <!--End tab 2-->
                                
                                
                                
                                
                                <div role="tabpanel" class="tab-pane"  id="tab3" aria-labelledby="mail_tab" aria-expanded="false">
                                    <form class="form" action="edit_profile_notifications_format" method="post">
                                        <%
                                        ArrayList<String[]> all_personal_notifications = db.get_notifications.user_notifications_for_user(con,user_id);
                                        System.out.println("all_personal_notifications=" + all_personal_notifications.size());
                                        %>
                                        <div class="row">
                                            <div class="col-12">
                                                <h3>Incident Email Notification Formats</h3>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-8 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>
                                                        Create Incident Email Subject
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <button type="button" onclick="test_email('incident','incident_create_subject','incident_create_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                    </label>

                                                    <div class='input-group'>
                                                        <%
                                                        String value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("incident_create_assigned_to_me"))
                                                            {
                                                                value = this_setting[4];
                                                            }
                                                        }
                                                        %>
                                                        <input type='text' class="form-control" id="incident_create_subject" name="incident_create_subject" value="<%=value%>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Subject Incident Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="incident_create_subject_fields" id="incident_create_subject_fields">
                                                            <option value="<$incident_id$>">Incident ID</option>
                                                            <option value="<$incident_time$>">Incident Time</option>
                                                            <option value="<$caller$>">Caller</option>
                                                            <option value="<$caller_group$>">Caller Group</option>
                                                            <option value="<$location$>">Location</option>
                                                            <option value="<$department$>">Department</option>
                                                            <option value="<$site$>">Site</option>
                                                            <option value="<$company$>">Company</option>
                                                            <option value="<$category$>">Category</option>
                                                            <option value="<$subcategory$>">Subcategory</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$create_date$>">Create Date</option>
                                                            <option value="<$create_by$>">Created By</option>
                                                            <option value="<$contact_method$>">Contact Method</option>
                                                            <option value="<$state$>">State</option>
                                                            <option value="<$state_date$>">State Date</option>
                                                            <option value="<$assigned_group$>">Assigned Group</option>
                                                            <option value="<$assigned_to$>">Assigned To</option>
                                                            <option value="<$notes$>">Notes</option>
                                                            <option value="<$desk_notes$>">Desk Notes</option>
                                                            <option value="<$closed_date$>">Closed Date</option>
                                                            <option value="<$closed_reason$>">Closed Reason</option>
                                                            <option value="<$pending_date$>">Pending Date</option>
                                                            <option value="<$pending_reason$>">Pending Reason</option>                                                                
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('incident_create_subject_fields','incident_create_subject')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">    
                                            <div class="col-xl-8 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <label>Create Incident Email Body</label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("incident_create_assigned_to_me"))
                                                            {
                                                                value = this_setting[5];
                                                            }
                                                        }
                                                        %>
                                                        <textarea rows="5" class="form-control" id="incident_create_body" name="incident_create_body" ><%=value%></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Body Incident Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="incident_create_body_fields" id="incident_create_body_fields">
                                                            <option value="<$incident_id$>">Incident ID</option>
                                                            <option value="<$incident_time$>">Incident Time</option>
                                                            <option value="<$caller$>">Caller</option>
                                                            <option value="<$caller_group$>">Caller Group</option>
                                                            <option value="<$location$>">Location</option>
                                                            <option value="<$department$>">Department</option>
                                                            <option value="<$site$>">Site</option>
                                                            <option value="<$company$>">Company</option>
                                                            <option value="<$category$>">Category</option>
                                                            <option value="<$subcategory$>">Subcategory</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$create_date$>">Create Date</option>
                                                            <option value="<$create_by$>">Created By</option>
                                                            <option value="<$contact_method$>">Contact Method</option>
                                                            <option value="<$state$>">State</option>
                                                            <option value="<$state_date$>">State Date</option>
                                                            <option value="<$assigned_group$>">Assigned Group</option>
                                                            <option value="<$assigned_to$>">Assigned To</option>
                                                            <option value="<$notes$>">Notes</option>
                                                            <option value="<$desk_notes$>">Desk Notes</option>
                                                            <option value="<$closed_date$>">Closed Date</option>
                                                            <option value="<$closed_reason$>">Closed Reason</option>
                                                            <option value="<$pending_date$>">Pending Date</option>
                                                            <option value="<$pending_reason$>">Pending Reason</option>                                                                
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('incident_create_body_fields','incident_create_body')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12"><hr></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xl-8 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>
                                                        Update Incident Email Subject
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <button type="button" onclick="test_email('incident','incident_update_subject','incident_update_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                    </label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("incident_update_assigned_to_me"))
                                                            {
                                                                value = this_setting[4];
                                                            }
                                                        }
                                                        %>
                                                        <input type='text' class="form-control" id="incident_update_subject" name="incident_update_subject" value="<%=value%>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Subject Incident Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="incident_update_subject_fields" id="incident_update_subject_fields">
                                                            <option value="<$incident_id$>">Incident ID</option>
                                                            <option value="<$incident_time$>">Incident Time</option>
                                                            <option value="<$caller$>">Caller</option>
                                                            <option value="<$caller_group$>">Caller Group</option>
                                                            <option value="<$location$>">Location</option>
                                                            <option value="<$department$>">Department</option>
                                                            <option value="<$site$>">Site</option>
                                                            <option value="<$company$>">Company</option>
                                                            <option value="<$category$>">Category</option>
                                                            <option value="<$subcategory$>">Subcategory</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$create_date$>">Create Date</option>
                                                            <option value="<$create_by$>">Created By</option>
                                                            <option value="<$contact_method$>">Contact Method</option>
                                                            <option value="<$state$>">State</option>
                                                            <option value="<$state_date$>">State Date</option>
                                                            <option value="<$assigned_group$>">Assigned Group</option>
                                                            <option value="<$assigned_to$>">Assigned To</option>
                                                            <option value="<$notes$>">Notes</option>
                                                            <option value="<$desk_notes$>">Desk Notes</option>
                                                            <option value="<$closed_date$>">Closed Date</option>
                                                            <option value="<$closed_reason$>">Closed Reason</option>
                                                            <option value="<$pending_date$>">Pending Date</option>
                                                            <option value="<$pending_reason$>">Pending Reason</option>                                                                
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('incident_update_subject_fields','incident_update_subject')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">    
                                            <div class="col-xl-8 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <label>Update Incident Email Body</label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("incident_update_assigned_to_me"))
                                                            {
                                                                value = this_setting[5];
                                                            }
                                                        }
                                                        %>
                                                        <textarea rows="5" class="form-control" id="incident_update_body" name="incident_update_body" ><%=value%></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Body Incident Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="incident_update_body_fields" id="incident_update_body_fields">
                                                            <option value="<$incident_id$>">Incident ID</option>
                                                            <option value="<$incident_time$>">Incident Time</option>
                                                            <option value="<$caller$>">Caller</option>
                                                            <option value="<$caller_group$>">Caller Group</option>
                                                            <option value="<$location$>">Location</option>
                                                            <option value="<$department$>">Department</option>
                                                            <option value="<$site$>">Site</option>
                                                            <option value="<$company$>">Company</option>
                                                            <option value="<$category$>">Category</option>
                                                            <option value="<$subcategory$>">Subcategory</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$create_date$>">Create Date</option>
                                                            <option value="<$create_by$>">Created By</option>
                                                            <option value="<$contact_method$>">Contact Method</option>
                                                            <option value="<$state$>">State</option>
                                                            <option value="<$state_date$>">State Date</option>
                                                            <option value="<$assigned_group$>">Assigned Group</option>
                                                            <option value="<$assigned_to$>">Assigned To</option>
                                                            <option value="<$notes$>">Notes</option>
                                                            <option value="<$desk_notes$>">Desk Notes</option>
                                                            <option value="<$closed_date$>">Closed Date</option>
                                                            <option value="<$closed_reason$>">Closed Reason</option>
                                                            <option value="<$pending_date$>">Pending Date</option>
                                                            <option value="<$pending_reason$>">Pending Reason</option>                                                                
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('incident_update_body_fields','incident_update_body')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12"><hr></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-8 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>
                                                        Resolve Incident Email Subject
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <button type="button" onclick="test_email('incident','incident_resolve_subject','incident_resolve_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                    </label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("incident_resolve_assigned_to_me"))
                                                            {
                                                                value = this_setting[4];
                                                            }
                                                        }
                                                        %>
                                                        <input type='text' class="form-control" id="incident_resolve_subject" name="incident_resolve_subject" value="<%=value%>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Subject Incident Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="incident_resolve_subject_fields" id="incident_resolve_subject_fields">
                                                            <option value="<$incident_id$>">Incident ID</option>
                                                            <option value="<$incident_time$>">Incident Time</option>
                                                            <option value="<$caller$>">Caller</option>
                                                            <option value="<$caller_group$>">Caller Group</option>
                                                            <option value="<$location$>">Location</option>
                                                            <option value="<$department$>">Department</option>
                                                            <option value="<$site$>">Site</option>
                                                            <option value="<$company$>">Company</option>
                                                            <option value="<$category$>">Category</option>
                                                            <option value="<$subcategory$>">Subcategory</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$create_date$>">Create Date</option>
                                                            <option value="<$create_by$>">Created By</option>
                                                            <option value="<$contact_method$>">Contact Method</option>
                                                            <option value="<$state$>">State</option>
                                                            <option value="<$state_date$>">State Date</option>
                                                            <option value="<$assigned_group$>">Assigned Group</option>
                                                            <option value="<$assigned_to$>">Assigned To</option>
                                                            <option value="<$notes$>">Notes</option>
                                                            <option value="<$desk_notes$>">Desk Notes</option>
                                                            <option value="<$closed_date$>">Closed Date</option>
                                                            <option value="<$closed_reason$>">Closed Reason</option>
                                                            <option value="<$pending_date$>">Pending Date</option>
                                                            <option value="<$pending_reason$>">Pending Reason</option>                                                                
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('incident_resolve_subject_fields','incident_resolve_subject')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">    
                                            <div class="col-xl-8 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <label>Resolve Incident Email Body</label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("incident_resolve_assigned_to_me"))
                                                            {
                                                                value = this_setting[5];
                                                            }
                                                        }
                                                        %>
                                                        <textarea rows="5" class="form-control" id="incident_resolve_body" name="incident_resolve_body" ><%=value%></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Body Incident Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="incident_resolve_body_fields" id="incident_resolve_body_fields">
                                                            <option value="<$incident_id$>">Incident ID</option>
                                                            <option value="<$incident_time$>">Incident Time</option>
                                                            <option value="<$caller$>">Caller</option>
                                                            <option value="<$caller_group$>">Caller Group</option>
                                                            <option value="<$location$>">Location</option>
                                                            <option value="<$department$>">Department</option>
                                                            <option value="<$site$>">Site</option>
                                                            <option value="<$company$>">Company</option>
                                                            <option value="<$category$>">Category</option>
                                                            <option value="<$subcategory$>">Subcategory</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$create_date$>">Create Date</option>
                                                            <option value="<$create_by$>">Created By</option>
                                                            <option value="<$contact_method$>">Contact Method</option>
                                                            <option value="<$state$>">State</option>
                                                            <option value="<$state_date$>">State Date</option>
                                                            <option value="<$assigned_group$>">Assigned Group</option>
                                                            <option value="<$assigned_to$>">Assigned To</option>
                                                            <option value="<$notes$>">Notes</option>
                                                            <option value="<$desk_notes$>">Desk Notes</option>
                                                            <option value="<$closed_date$>">Closed Date</option>
                                                            <option value="<$closed_reason$>">Closed Reason</option>
                                                            <option value="<$pending_date$>">Pending Date</option>
                                                            <option value="<$pending_reason$>">Pending Reason</option>                                                                
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('incident_resolve_body_fields','incident_resolve_body')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12"><hr></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xl-8 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>
                                                        Close Incident Email Subject
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <button type="button" onclick="test_email('incident','incident_close_subject','incident_close_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                    </label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("incident_close_assigned_to_me"))
                                                            {
                                                                value = this_setting[4];
                                                            }
                                                        }
                                                        %>
                                                        <input type='text' class="form-control" id="incident_close_subject" name="incident_close_subject" value="<%=value%>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Subject Incident Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="incident_close_subject_fields" id="incident_close_subject_fields">
                                                            <option value="<$incident_id$>">Incident ID</option>
                                                            <option value="<$incident_time$>">Incident Time</option>
                                                            <option value="<$caller$>">Caller</option>
                                                            <option value="<$caller_group$>">Caller Group</option>
                                                            <option value="<$location$>">Location</option>
                                                            <option value="<$department$>">Department</option>
                                                            <option value="<$site$>">Site</option>
                                                            <option value="<$company$>">Company</option>
                                                            <option value="<$category$>">Category</option>
                                                            <option value="<$subcategory$>">Subcategory</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$create_date$>">Create Date</option>
                                                            <option value="<$create_by$>">Created By</option>
                                                            <option value="<$contact_method$>">Contact Method</option>
                                                            <option value="<$state$>">State</option>
                                                            <option value="<$state_date$>">State Date</option>
                                                            <option value="<$assigned_group$>">Assigned Group</option>
                                                            <option value="<$assigned_to$>">Assigned To</option>
                                                            <option value="<$notes$>">Notes</option>
                                                            <option value="<$desk_notes$>">Desk Notes</option>
                                                            <option value="<$closed_date$>">Closed Date</option>
                                                            <option value="<$closed_reason$>">Closed Reason</option>
                                                            <option value="<$pending_date$>">Pending Date</option>
                                                            <option value="<$pending_reason$>">Pending Reason</option>                                                                
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('incident_close_subject_fields','incident_close_subject')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">    
                                            <div class="col-xl-8 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <label>Close Incident Email Body</label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("incident_close_assigned_to_me"))
                                                            {
                                                                value = this_setting[5];
                                                            }
                                                        }
                                                        %>
                                                        <textarea rows="5" class="form-control" id="incident_close_body" name="incident_close_body" ><%=value%></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Body Incident Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="incident_close_body_fields" id="incident_close_body_fields">
                                                            <option value="<$incident_id$>">Incident ID</option>
                                                            <option value="<$incident_time$>">Incident Time</option>
                                                            <option value="<$caller$>">Caller</option>
                                                            <option value="<$caller_group$>">Caller Group</option>
                                                            <option value="<$location$>">Location</option>
                                                            <option value="<$department$>">Department</option>
                                                            <option value="<$site$>">Site</option>
                                                            <option value="<$company$>">Company</option>
                                                            <option value="<$category$>">Category</option>
                                                            <option value="<$subcategory$>">Subcategory</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$create_date$>">Create Date</option>
                                                            <option value="<$create_by$>">Created By</option>
                                                            <option value="<$contact_method$>">Contact Method</option>
                                                            <option value="<$state$>">State</option>
                                                            <option value="<$state_date$>">State Date</option>
                                                            <option value="<$assigned_group$>">Assigned Group</option>
                                                            <option value="<$assigned_to$>">Assigned To</option>
                                                            <option value="<$notes$>">Notes</option>
                                                            <option value="<$desk_notes$>">Desk Notes</option>
                                                            <option value="<$closed_date$>">Closed Date</option>
                                                            <option value="<$closed_reason$>">Closed Reason</option>
                                                            <option value="<$pending_date$>">Pending Date</option>
                                                            <option value="<$pending_reason$>">Pending Reason</option>                                                                
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('incident_close_body_fields','incident_close_body')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12"><hr></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-12">
                                                <h3>Request Email Notification Formats</h3>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-8 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>
                                                        Create Request Email Subject
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <button type="button" onclick="test_email('request','request_create_subject','request_create_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                    </label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("request_create_assigned_to_me"))
                                                            {
                                                                value = this_setting[4];
                                                            }
                                                        }
                                                        %>
                                                        <input type='text' class="form-control" id="request_create_subject" name="request_create_subject" value="<%=value%>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Subject Request Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="request_create_subject_fields" id="request_create_subject_fields">
                                                            <option value="<$request_id$>">Request ID</option>
                                                            <option value="<$rev$>">Revision Number</option>
                                                            <option value="<$rev_date$>">Revision Date</option>
                                                            <option value="<$rev_by_user_id$>">Revision by</option>
                                                            <option value="<$service_catalog$>">Service Catalog</option>
                                                            <option value="<$service_catalog_item$>">Service Catalog Item</option>
                                                            <option value="<$create_date$>">Create Date</option>
                                                            <option value="<$request_date$>">Request Date</option>
                                                            <option value="<$due_date$>">Due Date</option>
                                                            <option value="<$resolve_date$>">Resolved Date</option>
                                                            <option value="<$closed_date$>">Closed Date</option>
                                                            <option value="<$assigned_group$>">Assigned Group</option>
                                                            <option value="<$assigned_to$>">Assigned To</option>
                                                            <option value="<$create_by$>">Created By</option>
                                                            <option value="<$requested_for_id$>">Requested For</option>
                                                            <option value="<$requested_for_group$>">Requested For Group</option>
                                                            <option value="<$location$>">Location</option>
                                                            <option value="<$department$>">Department</option>
                                                            <option value="<$site$>">Site</option>
                                                            <option value="<$company$>">Company</option>
                                                            <option value="<$impact$>">Impact</option>
                                                            <option value="<$urgency$>">Urgency</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$state$>">State</option>
                                                            <option value="<$contact_method$>">Contact Method</option>
                                                            <option value="<$approval$>">Approval</option>
                                                            <option value="<$price$>">Price</option>
                                                            <option value="<$notes$>">Notes</option>
                                                            <option value="<$desk_notes$>">Desk Notes</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$closed_reason$>">Closed Reason</option>
                                                            <option value="<$required_info$>">Required Info</option>
                                                            <option value="<$required_info_response$>">Required Info Response</option>
                                                            <option value="<$optional_info$>">Optional Info</option>
                                                            <option value="<$optional_info_response$>">Optional Info Response</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('request_create_subject_fields','request_create_subject')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">    
                                            <div class="col-xl-8 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <label>Create Request Email Body</label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("request_create_assigned_to_me"))
                                                            {
                                                                value = this_setting[5];
                                                            }
                                                        }
                                                        %>
                                                        <textarea rows="5" class="form-control" id="request_create_body" name="request_create_body" ><%=value%></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Body Request Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="request_create_body_fields" id="request_create_body_fields">
                                                            <option value="<$request_id$>">Request ID</option>
                                                            <option value="<$rev$>">Revision Number</option>
                                                            <option value="<$rev_date$>">Revision Date</option>
                                                            <option value="<$rev_by_user_id$>">Revision by</option>
                                                            <option value="<$service_catalog$>">Service Catalog</option>
                                                            <option value="<$service_catalog_item$>">Service Catalog Item</option>
                                                            <option value="<$create_date$>">Create Date</option>
                                                            <option value="<$request_date$>">Request Date</option>
                                                            <option value="<$due_date$>">Due Date</option>
                                                            <option value="<$resolve_date$>">Resolved Date</option>
                                                            <option value="<$closed_date$>">Closed Date</option>
                                                            <option value="<$assigned_group$>">Assigned Group</option>
                                                            <option value="<$assigned_to$>">Assigned To</option>
                                                            <option value="<$create_by$>">Created By</option>
                                                            <option value="<$requested_for_id$>">Requested For</option>
                                                            <option value="<$requested_for_group$>">Requested For Group</option>
                                                            <option value="<$location$>">Location</option>
                                                            <option value="<$department$>">Department</option>
                                                            <option value="<$site$>">Site</option>
                                                            <option value="<$company$>">Company</option>
                                                            <option value="<$impact$>">Impact</option>
                                                            <option value="<$urgency$>">Urgency</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$state$>">State</option>
                                                            <option value="<$contact_method$>">Contact Method</option>
                                                            <option value="<$approval$>">Approval</option>
                                                            <option value="<$price$>">Price</option>
                                                            <option value="<$notes$>">Notes</option>
                                                            <option value="<$desk_notes$>">Desk Notes</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$closed_reason$>">Closed Reason</option>
                                                            <option value="<$required_info$>">Required Info</option>
                                                            <option value="<$required_info_response$>">Required Info Response</option>
                                                            <option value="<$optional_info$>">Optional Info</option>
                                                            <option value="<$optional_info_response$>">Optional Info Response</option>                                                             
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('request_create_body_fields','request_create_body')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12"><hr></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xl-8 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>
                                                        Update Request Email Subject
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <button type="button" onclick="test_email('request','request_update_subject','request_update_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                    </label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("request_update_assigned_to_me"))
                                                            {
                                                                value = this_setting[4];
                                                            }
                                                        }
                                                        %>
                                                        <input type='text' class="form-control" id="request_update_subject" name="request_update_subject" value="<%=value%>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Subject Request Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="request_update_subject_fields" id="request_update_subject_fields">
                                                            <option value="<$request_id$>">Request ID</option>
                                                            <option value="<$rev$>">Revision Number</option>
                                                            <option value="<$rev_date$>">Revision Date</option>
                                                            <option value="<$rev_by_user_id$>">Revision by</option>
                                                            <option value="<$service_catalog$>">Service Catalog</option>
                                                            <option value="<$service_catalog_item$>">Service Catalog Item</option>
                                                            <option value="<$create_date$>">Create Date</option>
                                                            <option value="<$request_date$>">Request Date</option>
                                                            <option value="<$due_date$>">Due Date</option>
                                                            <option value="<$resolve_date$>">Resolved Date</option>
                                                            <option value="<$closed_date$>">Closed Date</option>
                                                            <option value="<$assigned_group$>">Assigned Group</option>
                                                            <option value="<$assigned_to$>">Assigned To</option>
                                                            <option value="<$create_by$>">Created By</option>
                                                            <option value="<$requested_for_id$>">Requested For</option>
                                                            <option value="<$requested_for_group$>">Requested For Group</option>
                                                            <option value="<$location$>">Location</option>
                                                            <option value="<$department$>">Department</option>
                                                            <option value="<$site$>">Site</option>
                                                            <option value="<$company$>">Company</option>
                                                            <option value="<$impact$>">Impact</option>
                                                            <option value="<$urgency$>">Urgency</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$state$>">State</option>
                                                            <option value="<$contact_method$>">Contact Method</option>
                                                            <option value="<$approval$>">Approval</option>
                                                            <option value="<$price$>">Price</option>
                                                            <option value="<$notes$>">Notes</option>
                                                            <option value="<$desk_notes$>">Desk Notes</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$closed_reason$>">Closed Reason</option>
                                                            <option value="<$required_info$>">Required Info</option>
                                                            <option value="<$required_info_response$>">Required Info Response</option>
                                                            <option value="<$optional_info$>">Optional Info</option>
                                                            <option value="<$optional_info_response$>">Optional Info Response</option>                                                               
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('request_update_subject_fields','request_update_subject')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">    
                                            <div class="col-xl-8 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <label>Update Request Email Body</label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("request_update_assigned_to_me"))
                                                            {
                                                                value = this_setting[5];
                                                            }
                                                        }
                                                        %>
                                                        <textarea rows="5" class="form-control" id="request_update_body" name="request_update_body" ><%=value%></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Body Request Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="request_update_body_fields" id="request_update_body_fields">
                                                            <option value="<$request_id$>">Request ID</option>
                                                            <option value="<$rev$>">Revision Number</option>
                                                            <option value="<$rev_date$>">Revision Date</option>
                                                            <option value="<$rev_by_user_id$>">Revision by</option>
                                                            <option value="<$service_catalog$>">Service Catalog</option>
                                                            <option value="<$service_catalog_item$>">Service Catalog Item</option>
                                                            <option value="<$create_date$>">Create Date</option>
                                                            <option value="<$request_date$>">Request Date</option>
                                                            <option value="<$due_date$>">Due Date</option>
                                                            <option value="<$resolve_date$>">Resolved Date</option>
                                                            <option value="<$closed_date$>">Closed Date</option>
                                                            <option value="<$assigned_group$>">Assigned Group</option>
                                                            <option value="<$assigned_to$>">Assigned To</option>
                                                            <option value="<$create_by$>">Created By</option>
                                                            <option value="<$requested_for_id$>">Requested For</option>
                                                            <option value="<$requested_for_group$>">Requested For Group</option>
                                                            <option value="<$location$>">Location</option>
                                                            <option value="<$department$>">Department</option>
                                                            <option value="<$site$>">Site</option>
                                                            <option value="<$company$>">Company</option>
                                                            <option value="<$impact$>">Impact</option>
                                                            <option value="<$urgency$>">Urgency</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$state$>">State</option>
                                                            <option value="<$contact_method$>">Contact Method</option>
                                                            <option value="<$approval$>">Approval</option>
                                                            <option value="<$price$>">Price</option>
                                                            <option value="<$notes$>">Notes</option>
                                                            <option value="<$desk_notes$>">Desk Notes</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$closed_reason$>">Closed Reason</option>
                                                            <option value="<$required_info$>">Required Info</option>
                                                            <option value="<$required_info_response$>">Required Info Response</option>
                                                            <option value="<$optional_info$>">Optional Info</option>
                                                            <option value="<$optional_info_response$>">Optional Info Response</option>                                                               
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('request_update_body_fields','request_update_body')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12"><hr></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-8 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>
                                                        Resolve Request Email Subject
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <button type="button" onclick="test_email('request','request_resolve_subject','request_resolve_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                    </label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("request_resolve_assigned_to_me"))
                                                            {
                                                                value = this_setting[4];
                                                            }
                                                        }
                                                        %>
                                                        <input type='text' class="form-control" id="request_resolve_subject" name="request_resolve_subject" value="<%=value%>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Subject Request Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="request_resolve_subject_fields" id="request_resolve_subject_fields">
                                                            <option value="<$request_id$>">Request ID</option>
                                                            <option value="<$rev$>">Revision Number</option>
                                                            <option value="<$rev_date$>">Revision Date</option>
                                                            <option value="<$rev_by_user_id$>">Revision by</option>
                                                            <option value="<$service_catalog$>">Service Catalog</option>
                                                            <option value="<$service_catalog_item$>">Service Catalog Item</option>
                                                            <option value="<$create_date$>">Create Date</option>
                                                            <option value="<$request_date$>">Request Date</option>
                                                            <option value="<$due_date$>">Due Date</option>
                                                            <option value="<$resolve_date$>">Resolved Date</option>
                                                            <option value="<$closed_date$>">Closed Date</option>
                                                            <option value="<$assigned_group$>">Assigned Group</option>
                                                            <option value="<$assigned_to$>">Assigned To</option>
                                                            <option value="<$create_by$>">Created By</option>
                                                            <option value="<$requested_for_id$>">Requested For</option>
                                                            <option value="<$requested_for_group$>">Requested For Group</option>
                                                            <option value="<$location$>">Location</option>
                                                            <option value="<$department$>">Department</option>
                                                            <option value="<$site$>">Site</option>
                                                            <option value="<$company$>">Company</option>
                                                            <option value="<$impact$>">Impact</option>
                                                            <option value="<$urgency$>">Urgency</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$state$>">State</option>
                                                            <option value="<$contact_method$>">Contact Method</option>
                                                            <option value="<$approval$>">Approval</option>
                                                            <option value="<$price$>">Price</option>
                                                            <option value="<$notes$>">Notes</option>
                                                            <option value="<$desk_notes$>">Desk Notes</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$closed_reason$>">Closed Reason</option>
                                                            <option value="<$required_info$>">Required Info</option>
                                                            <option value="<$required_info_response$>">Required Info Response</option>
                                                            <option value="<$optional_info$>">Optional Info</option>
                                                            <option value="<$optional_info_response$>">Optional Info Response</option>                                                               
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('request_resolve_subject_fields','request_resolve_subject')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">    
                                            <div class="col-xl-8 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <label>Resolve Request Email Body</label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("request_resolve_assigned_to_me"))
                                                            {
                                                                value = this_setting[5];
                                                            }
                                                        }
                                                        %>
                                                        <textarea rows="5" class="form-control" id="request_resolve_body" name="request_resolve_body" ><%=value%></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Body Request Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="request_resolve_body_fields" id="request_resolve_body_fields">
                                                            <option value="<$request_id$>">Request ID</option>
                                                            <option value="<$rev$>">Revision Number</option>
                                                            <option value="<$rev_date$>">Revision Date</option>
                                                            <option value="<$rev_by_user_id$>">Revision by</option>
                                                            <option value="<$service_catalog$>">Service Catalog</option>
                                                            <option value="<$service_catalog_item$>">Service Catalog Item</option>
                                                            <option value="<$create_date$>">Create Date</option>
                                                            <option value="<$request_date$>">Request Date</option>
                                                            <option value="<$due_date$>">Due Date</option>
                                                            <option value="<$resolve_date$>">Resolved Date</option>
                                                            <option value="<$closed_date$>">Closed Date</option>
                                                            <option value="<$assigned_group$>">Assigned Group</option>
                                                            <option value="<$assigned_to$>">Assigned To</option>
                                                            <option value="<$create_by$>">Created By</option>
                                                            <option value="<$requested_for_id$>">Requested For</option>
                                                            <option value="<$requested_for_group$>">Requested For Group</option>
                                                            <option value="<$location$>">Location</option>
                                                            <option value="<$department$>">Department</option>
                                                            <option value="<$site$>">Site</option>
                                                            <option value="<$company$>">Company</option>
                                                            <option value="<$impact$>">Impact</option>
                                                            <option value="<$urgency$>">Urgency</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$state$>">State</option>
                                                            <option value="<$contact_method$>">Contact Method</option>
                                                            <option value="<$approval$>">Approval</option>
                                                            <option value="<$price$>">Price</option>
                                                            <option value="<$notes$>">Notes</option>
                                                            <option value="<$desk_notes$>">Desk Notes</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$closed_reason$>">Closed Reason</option>
                                                            <option value="<$required_info$>">Required Info</option>
                                                            <option value="<$required_info_response$>">Required Info Response</option>
                                                            <option value="<$optional_info$>">Optional Info</option>
                                                            <option value="<$optional_info_response$>">Optional Info Response</option>                                                               
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('request_resolve_body_fields','request_resolve_body')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>    

                                        <div class="row">
                                            <div class="col-12"><hr></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-8 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>
                                                        Close Request Email Subject
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <button type="button" onclick="test_email('request','request_close_subject','request_close_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                    </label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("request_close_assigned_to_me"))
                                                            {
                                                                value = this_setting[4];
                                                            }
                                                        }
                                                        %>
                                                        <input type='text' class="form-control" id="request_close_subject" name="request_close_subject" value="<%=value%>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Subject Request Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="request_close_subject_fields" id="request_close_subject_fields">
                                                            <option value="<$request_id$>">Request ID</option>
                                                            <option value="<$rev$>">Revision Number</option>
                                                            <option value="<$rev_date$>">Revision Date</option>
                                                            <option value="<$rev_by_user_id$>">Revision by</option>
                                                            <option value="<$service_catalog$>">Service Catalog</option>
                                                            <option value="<$service_catalog_item$>">Service Catalog Item</option>
                                                            <option value="<$create_date$>">Create Date</option>
                                                            <option value="<$request_date$>">Request Date</option>
                                                            <option value="<$due_date$>">Due Date</option>
                                                            <option value="<$resolve_date$>">Resolved Date</option>
                                                            <option value="<$closed_date$>">Closed Date</option>
                                                            <option value="<$assigned_group$>">Assigned Group</option>
                                                            <option value="<$assigned_to$>">Assigned To</option>
                                                            <option value="<$create_by$>">Created By</option>
                                                            <option value="<$requested_for_id$>">Requested For</option>
                                                            <option value="<$requested_for_group$>">Requested For Group</option>
                                                            <option value="<$location$>">Location</option>
                                                            <option value="<$department$>">Department</option>
                                                            <option value="<$site$>">Site</option>
                                                            <option value="<$company$>">Company</option>
                                                            <option value="<$impact$>">Impact</option>
                                                            <option value="<$urgency$>">Urgency</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$state$>">State</option>
                                                            <option value="<$contact_method$>">Contact Method</option>
                                                            <option value="<$approval$>">Approval</option>
                                                            <option value="<$price$>">Price</option>
                                                            <option value="<$notes$>">Notes</option>
                                                            <option value="<$desk_notes$>">Desk Notes</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$closed_reason$>">Closed Reason</option>
                                                            <option value="<$required_info$>">Required Info</option>
                                                            <option value="<$required_info_response$>">Required Info Response</option>
                                                            <option value="<$optional_info$>">Optional Info</option>
                                                            <option value="<$optional_info_response$>">Optional Info Response</option>                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('request_close_subject_fields','request_close_subject')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">    
                                            <div class="col-xl-8 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <label>Close Request Email Body</label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("request_close_assigned_to_me"))
                                                            {
                                                                value = this_setting[5];
                                                            }
                                                        }
                                                        %>
                                                        <textarea rows="5" class="form-control" id="request_close_body" name="request_close_body" ><%=value%></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Body Request Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="request_close_body_fields" id="request_close_body_fields">
                                                            <option value="<$request_id$>">Request ID</option>
                                                            <option value="<$rev$>">Revision Number</option>
                                                            <option value="<$rev_date$>">Revision Date</option>
                                                            <option value="<$rev_by_user_id$>">Revision by</option>
                                                            <option value="<$service_catalog$>">Service Catalog</option>
                                                            <option value="<$service_catalog_item$>">Service Catalog Item</option>
                                                            <option value="<$create_date$>">Create Date</option>
                                                            <option value="<$request_date$>">Request Date</option>
                                                            <option value="<$due_date$>">Due Date</option>
                                                            <option value="<$resolve_date$>">Resolved Date</option>
                                                            <option value="<$closed_date$>">Closed Date</option>
                                                            <option value="<$assigned_group$>">Assigned Group</option>
                                                            <option value="<$assigned_to$>">Assigned To</option>
                                                            <option value="<$create_by$>">Created By</option>
                                                            <option value="<$requested_for_id$>">Requested For</option>
                                                            <option value="<$requested_for_group$>">Requested For Group</option>
                                                            <option value="<$location$>">Location</option>
                                                            <option value="<$department$>">Department</option>
                                                            <option value="<$site$>">Site</option>
                                                            <option value="<$company$>">Company</option>
                                                            <option value="<$impact$>">Impact</option>
                                                            <option value="<$urgency$>">Urgency</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$state$>">State</option>
                                                            <option value="<$contact_method$>">Contact Method</option>
                                                            <option value="<$approval$>">Approval</option>
                                                            <option value="<$price$>">Price</option>
                                                            <option value="<$notes$>">Notes</option>
                                                            <option value="<$desk_notes$>">Desk Notes</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$closed_reason$>">Closed Reason</option>
                                                            <option value="<$required_info$>">Required Info</option>
                                                            <option value="<$required_info_response$>">Required Info Response</option>
                                                            <option value="<$optional_info$>">Optional Info</option>
                                                            <option value="<$optional_info_response$>">Optional Info Response</option>                                                              
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('request_close_body_fields','request_close_body')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="row">
                                            <div class="col-12"><hr></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-12">
                                                <h3>Project/Task Email Notification Formats</h3>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-8 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>
                                                        Create Task Email Subject
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <button type="button" onclick="test_email('task','task_create_subject','task_create_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                    </label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("task_create_assigned_to_me"))
                                                            {
                                                                value = this_setting[4];
                                                            }
                                                        }
                                                        %>
                                                        <input type='text' class="form-control" id="task_create_subject" name="task_create_subject" value="<%=value%>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Subject Task Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="task_create_subject_fields" id="task_create_subject_fields">
                                                            <option value="<$task_id$>">Task ID</option>
                                                            <option value="<$project_id_name$>">Project Name</option>
                                                            <option value="<$name$>">Name</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$assigned_group_id_name$>">Assigned Group</option>
                                                            <option value="<$assigned_to_id_username$>">Assigned to username</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$status$>">Status</option>
                                                            <option value="<$scheduled_start_date$>">Scheduled Start Date</option>
                                                            <option value="<$actual_start_date$>">Actual Start Date</option>
                                                            <option value="<$scheduled_end_date$>">Scheduled End Date</option>
                                                            <option value="<$actual_end_date$>">Actual End Date</option>
                                                            <option value="<$estimated_duration$>">Estimated Duration</option>
                                                            <option value="<$actual_duration$>">Actual Duration</option>
                                                            <option value="<$notes$>">Notes</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('task_create_subject_fields','task_create_subject')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">    
                                            <div class="col-xl-8 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <label>Create Task Email Body</label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("task_create_assigned_to_me"))
                                                            {
                                                                value = this_setting[5];
                                                            }
                                                        }
                                                        %>
                                                        <textarea rows="5" class="form-control" id="task_create_body" name="task_create_body" ><%=value%></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Body Task Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="task_create_body_fields" id="task_create_body_fields">
                                                            <option value="<$task_id$>">Task ID</option>
                                                            <option value="<$project_id_name$>">Project Name</option>
                                                            <option value="<$name$>">Name</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$assigned_group_id_name$>">Assigned Group</option>
                                                            <option value="<$assigned_to_id_username$>">Assigned to username</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$status$>">Status</option>
                                                            <option value="<$scheduled_start_date$>">Scheduled Start Date</option>
                                                            <option value="<$actual_start_date$>">Actual Start Date</option>
                                                            <option value="<$scheduled_end_date$>">Scheduled End Date</option>
                                                            <option value="<$actual_end_date$>">Actual End Date</option>
                                                            <option value="<$estimated_duration$>">Estimated Duration</option>
                                                            <option value="<$actual_duration$>">Actual Duration</option>
                                                            <option value="<$notes$>">Notes</option>                                                           
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('task_create_body_fields','task_create_body')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12"><hr></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xl-8 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>
                                                        Update Task Email Subject
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <button type="button" onclick="test_email('task','task_update_subject','task_update_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                    </label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("task_update_assigned_to_me"))
                                                            {
                                                                value = this_setting[4];
                                                            }
                                                        }
                                                        %>
                                                        <input type='text' class="form-control" id="task_update_subject" name="task_update_subject" value="<%=value%>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Subject Task Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="task_update_subject_fields" id="task_update_subject_fields">
                                                            <option value="<$task_id$>">Task ID</option>
                                                            <option value="<$project_id_name$>">Project Name</option>
                                                            <option value="<$name$>">Name</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$assigned_group_id_name$>">Assigned Group</option>
                                                            <option value="<$assigned_to_id_username$>">Assigned to username</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$status$>">Status</option>
                                                            <option value="<$scheduled_start_date$>">Scheduled Start Date</option>
                                                            <option value="<$actual_start_date$>">Actual Start Date</option>
                                                            <option value="<$scheduled_end_date$>">Scheduled End Date</option>
                                                            <option value="<$actual_end_date$>">Actual End Date</option>
                                                            <option value="<$estimated_duration$>">Estimated Duration</option>
                                                            <option value="<$actual_duration$>">Actual Duration</option>
                                                            <option value="<$notes$>">Notes</option>                                                             
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('task_update_subject_fields','task_update_subject')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">    
                                            <div class="col-xl-8 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <label>Update Task Email Body</label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("task_update_assigned_to_me"))
                                                            {
                                                                value = this_setting[5];
                                                            }
                                                        }
                                                        %>
                                                        <textarea rows="5" class="form-control" id="task_update_body" name="task_update_body" ><%=value%></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Body Task Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="task_update_body_fields" id="task_update_body_fields">
                                                            <option value="<$task_id$>">Task ID</option>
                                                            <option value="<$project_id_name$>">Project Name</option>
                                                            <option value="<$name$>">Name</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$assigned_group_id_name$>">Assigned Group</option>
                                                            <option value="<$assigned_to_id_username$>">Assigned to username</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$status$>">Status</option>
                                                            <option value="<$scheduled_start_date$>">Scheduled Start Date</option>
                                                            <option value="<$actual_start_date$>">Actual Start Date</option>
                                                            <option value="<$scheduled_end_date$>">Scheduled End Date</option>
                                                            <option value="<$actual_end_date$>">Actual End Date</option>
                                                            <option value="<$estimated_duration$>">Estimated Duration</option>
                                                            <option value="<$actual_duration$>">Actual Duration</option>
                                                            <option value="<$notes$>">Notes</option>                                                              
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('task_update_body_fields','task_update_body')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <!--
                                        <div class="row">
                                            <div class="col-12"><hr></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-8 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>
                                                        Resolve Task Email Subject
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <button type="button" onclick="test_email('task','task_resolve_subject','task_resolve_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                    </label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("task_resolve_assigned_to_me"))
                                                            {
                                                                value = this_setting[4];
                                                            }
                                                        }
                                                        %>
                                                        <input type='text' class="form-control" id="task_resolve_subject" name="task_resolve_subject" value="<%=value%>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Subject Task Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="task_resolve_subject_fields" id="task_resolve_subject_fields">
                                                            <option value="<$task_id$>">Task ID</option>
                                                            <option value="<$project_id_name$>">Project Name</option>
                                                            <option value="<$name$>">Name</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$assigned_group_id_name$>">Assigned Group</option>
                                                            <option value="<$assigned_to_id_username$>">Assigned to username</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$status$>">Status</option>
                                                            <option value="<$scheduled_start_date$>">Scheduled Start Date</option>
                                                            <option value="<$actual_start_date$>">Actual Start Date</option>
                                                            <option value="<$scheduled_end_date$>">Scheduled End Date</option>
                                                            <option value="<$actual_end_date$>">Actual End Date</option>
                                                            <option value="<$estimated_duration$>">Estimated Duration</option>
                                                            <option value="<$actual_duration$>">Actual Duration</option>
                                                            <option value="<$notes$>">Notes</option>                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('task_resolve_subject_fields','task_resolve_subject')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">    
                                            <div class="col-xl-8 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <label>Resolve Task Email Body</label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("task_resolve_assigned_to_me"))
                                                            {
                                                                value = this_setting[5];
                                                            }
                                                        }
                                                        %>
                                                        <textarea rows="5" class="form-control" id="task_resolve_body" name="task_resolve_body" ><%=value%></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Body Task Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="task_resolve_body_fields" id="task_resolve_body_fields">
                                                            <option value="<$task_id$>">Task ID</option>
                                                            <option value="<$project_id_name$>">Project Name</option>
                                                            <option value="<$name$>">Name</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$assigned_group_id_name$>">Assigned Group</option>
                                                            <option value="<$assigned_to_id_username$>">Assigned to username</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$status$>">Status</option>
                                                            <option value="<$scheduled_start_date$>">Scheduled Start Date</option>
                                                            <option value="<$actual_start_date$>">Actual Start Date</option>
                                                            <option value="<$scheduled_end_date$>">Scheduled End Date</option>
                                                            <option value="<$actual_end_date$>">Actual End Date</option>
                                                            <option value="<$estimated_duration$>">Estimated Duration</option>
                                                            <option value="<$actual_duration$>">Actual Duration</option>
                                                            <option value="<$notes$>">Notes</option>                                                              
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('task_resolve_body_fields','task_resolve_body')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>    
                                        -->
                                        <div class="row">
                                            <div class="col-12"><hr></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-8 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>
                                                        Close Task Email Subject
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <button type="button" onclick="test_email('task','task_close_subject','task_close_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                    </label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("task_close_assigned_to_me"))
                                                            {
                                                                value = this_setting[4];
                                                            }
                                                        }
                                                        %>
                                                        <input type='text' class="form-control" id="task_close_subject" name="task_close_subject" value="<%=value%>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Subject Task Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="task_close_subject_fields" id="task_close_subject_fields">
                                                            <option value="<$task_id$>">Task ID</option>
                                                            <option value="<$project_id_name$>">Project Name</option>
                                                            <option value="<$name$>">Name</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$assigned_group_id_name$>">Assigned Group</option>
                                                            <option value="<$assigned_to_id_username$>">Assigned to username</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$status$>">Status</option>
                                                            <option value="<$scheduled_start_date$>">Scheduled Start Date</option>
                                                            <option value="<$actual_start_date$>">Actual Start Date</option>
                                                            <option value="<$scheduled_end_date$>">Scheduled End Date</option>
                                                            <option value="<$actual_end_date$>">Actual End Date</option>
                                                            <option value="<$estimated_duration$>">Estimated Duration</option>
                                                            <option value="<$actual_duration$>">Actual Duration</option>
                                                            <option value="<$notes$>">Notes</option>                                                           
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('task_close_subject_fields','task_close_subject')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">    
                                            <div class="col-xl-8 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <label>Close Task Email Body</label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("task_close_assigned_to_me"))
                                                            {
                                                                value = this_setting[5];
                                                            }
                                                        }
                                                        %>
                                                        <textarea rows="5" class="form-control" id="task_close_body" name="task_close_body" ><%=value%></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Body Task Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="task_close_body_fields" id="task_close_body_fields">
                                                            <option value="<$task_id$>">Task ID</option>
                                                            <option value="<$project_id_name$>">Project Name</option>
                                                            <option value="<$name$>">Name</option>
                                                            <option value="<$description$>">Description</option>
                                                            <option value="<$assigned_group_id_name$>">Assigned Group</option>
                                                            <option value="<$assigned_to_id_username$>">Assigned to username</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$status$>">Status</option>
                                                            <option value="<$scheduled_start_date$>">Scheduled Start Date</option>
                                                            <option value="<$actual_start_date$>">Actual Start Date</option>
                                                            <option value="<$scheduled_end_date$>">Scheduled End Date</option>
                                                            <option value="<$actual_end_date$>">Actual End Date</option>
                                                            <option value="<$estimated_duration$>">Estimated Duration</option>
                                                            <option value="<$actual_duration$>">Actual Duration</option>
                                                            <option value="<$notes$>">Notes</option>                                                          
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('task_close_body_fields','task_close_body')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>            
                                        <div class="row">
                                            <div class="col-12"><hr></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <h3>Job Email Notification Formats</h3>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-8 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>
                                                        Create Job Email Subject
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <button type="button" onclick="test_email('job','job_create_subject','job_create_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                    </label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("job_create_assigned_to_me"))
                                                            {
                                                                value = this_setting[4];
                                                            }
                                                        }
                                                        %>
                                                        <input type='text' class="form-control" id="job_create_subject" name="job_create_subject" value="<%=value%>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Subject Job Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="job_create_subject_fields" id="job_create_subject_fields">
                                                            <option value="<$job_id$>">Job ID</option>
                                                            <option value="<$name$>">Name</option>
                                                            <option value="<$description$>">description</option>
                                                            <option value="<$assigned_group_id_name$>">Assigned Group Name</option>
                                                            <option value="<$assigned_to_id_username$>">Assigned To</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$status$>">Status</option>
                                                            <option value="<$scheduled_start_date$>">Scheduled Start Date</option>
                                                            <option value="<$actual_start_date$>">Actual Start Date</option>
                                                            <option value="<$scheduled_end_date$>">Scheduled End Date</option>
                                                            <option value="<$actual_end_date$>">Actual End Date</option>
                                                            <option value="<$estimated_duration$>">Estimated Duration</option>
                                                            <option value="<actual_duration>">Actual Duration</option>
                                                            <option value="<$notes$>">Notes</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('job_create_subject_fields','job_create_subject')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">    
                                            <div class="col-xl-8 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <label>Create Job Email Body</label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("job_create_assigned_to_me"))
                                                            {
                                                                value = this_setting[5];
                                                            }
                                                        }
                                                        %>
                                                        <textarea rows="5" class="form-control" id="job_create_body" name="job_create_body" ><%=value%></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Body Job Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="job_create_body_fields" id="job_create_body_fields">
                                                            <option value="<$job_id$>">Job ID</option>
                                                            <option value="<$name$>">Name</option>
                                                            <option value="<$description$>">description</option>
                                                            <option value="<$assigned_group_id_name$>">Assigned Group Name</option>
                                                            <option value="<$assigned_to_id_username$>">Assigned To</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$status$>">Status</option>
                                                            <option value="<$scheduled_start_date$>">Scheduled Start Date</option>
                                                            <option value="<$actual_start_date$>">Actual Start Date</option>
                                                            <option value="<$scheduled_end_date$>">Scheduled End Date</option>
                                                            <option value="<$actual_end_date$>">Actual End Date</option>
                                                            <option value="<$estimated_duration$>">Estimated Duration</option>
                                                            <option value="<actual_duration>">Actual Duration</option>
                                                            <option value="<$notes$>">Notes</option>                                                           
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('job_create_body_fields','job_create_body')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12"><hr></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xl-8 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>
                                                        Update Job Email Subject
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <button type="button" onclick="test_email('job','job_update_subject','job_update_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                    </label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("job_update_assigned_to_me"))
                                                            {
                                                                value = this_setting[4];
                                                            }
                                                        }
                                                        %>
                                                        <input type='text' class="form-control" id="job_update_subject" name="job_update_subject" value="<%=value%>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Subject Job Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="job_update_subject_fields" id="job_update_subject_fields">
                                                            <option value="<$job_id$>">Job ID</option>
                                                            <option value="<$name$>">Name</option>
                                                            <option value="<$description$>">description</option>
                                                            <option value="<$assigned_group_id_name$>">Assigned Group Name</option>
                                                            <option value="<$assigned_to_id_username$>">Assigned To</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$status$>">Status</option>
                                                            <option value="<$scheduled_start_date$>">Scheduled Start Date</option>
                                                            <option value="<$actual_start_date$>">Actual Start Date</option>
                                                            <option value="<$scheduled_end_date$>">Scheduled End Date</option>
                                                            <option value="<$actual_end_date$>">Actual End Date</option>
                                                            <option value="<$estimated_duration$>">Estimated Duration</option>
                                                            <option value="<actual_duration>">Actual Duration</option>
                                                            <option value="<$notes$>">Notes</option>                                                           
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('job_update_subject_fields','job_update_subject')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">    
                                            <div class="col-xl-8 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <label>Update Job Email Body</label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("job_update_assigned_to_me"))
                                                            {
                                                                value = this_setting[5];
                                                            }
                                                        }
                                                        %>
                                                        <textarea rows="5" class="form-control" id="job_update_body" name="job_update_body" ><%=value%></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Body Job Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="job_update_body_fields" id="job_update_body_fields">
                                                            <option value="<$job_id$>">Job ID</option>
                                                            <option value="<$name$>">Name</option>
                                                            <option value="<$description$>">description</option>
                                                            <option value="<$assigned_group_id_name$>">Assigned Group Name</option>
                                                            <option value="<$assigned_to_id_username$>">Assigned To</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$status$>">Status</option>
                                                            <option value="<$scheduled_start_date$>">Scheduled Start Date</option>
                                                            <option value="<$actual_start_date$>">Actual Start Date</option>
                                                            <option value="<$scheduled_end_date$>">Scheduled End Date</option>
                                                            <option value="<$actual_end_date$>">Actual End Date</option>
                                                            <option value="<$estimated_duration$>">Estimated Duration</option>
                                                            <option value="<actual_duration>">Actual Duration</option>
                                                            <option value="<$notes$>">Notes</option>                                                              
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('job_update_body_fields','job_update_body')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <!--
                                        <div class="row">
                                            <div class="col-12"><hr></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-8 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>
                                                        Resolve Job Email Subject
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <button type="button" onclick="test_email('job','job_resolve_subject','job_resolve_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                    </label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("job_resolve_assigned_to_me"))
                                                            {
                                                                value = this_setting[4];
                                                            }
                                                        }
                                                        %>
                                                        <input type='text' class="form-control" id="job_resolve_subject" name="job_resolve_subject" value="<%=value%>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Subject Job Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="job_resolve_subject_fields" id="job_resolve_subject_fields">
                                                            <option value="<$job_id$>">Job ID</option>
                                                            <option value="<$name$>">Name</option>
                                                            <option value="<$description$>">description</option>
                                                            <option value="<$assigned_group_id_name$>">Assigned Group Name</option>
                                                            <option value="<$assigned_to_id_username$>">Assigned To</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$status$>">Status</option>
                                                            <option value="<$scheduled_start_date$>">Scheduled Start Date</option>
                                                            <option value="<$actual_start_date$>">Actual Start Date</option>
                                                            <option value="<$scheduled_end_date$>">Scheduled End Date</option>
                                                            <option value="<$actual_end_date$>">Actual End Date</option>
                                                            <option value="<$estimated_duration$>">Estimated Duration</option>
                                                            <option value="<actual_duration>">Actual Duration</option>
                                                            <option value="<$notes$>">Notes</option>                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('job_resolve_subject_fields','job_resolve_subject')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        -->
                                        <!--            
                                        <div class="row">    
                                            <div class="col-xl-8 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <label>Resolve Job Email Body</label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("job_resolve_assigned_to_me"))
                                                            {
                                                                value = this_setting[5];
                                                            }
                                                        }
                                                        %>
                                                        <textarea rows="5" class="form-control" id="job_resolve_body" name="job_resolve_body" ><%=value%></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Body Job Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="job_resolve_body_fields" id="job_resolve_body_fields">
                                                            <option value="<$job_id$>">Job ID</option>
                                                            <option value="<$name$>">Name</option>
                                                            <option value="<$description$>">description</option>
                                                            <option value="<$assigned_group_id_name$>">Assigned Group Name</option>
                                                            <option value="<$assigned_to_id_username$>">Assigned To</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$status$>">Status</option>
                                                            <option value="<$scheduled_start_date$>">Scheduled Start Date</option>
                                                            <option value="<$actual_start_date$>">Actual Start Date</option>
                                                            <option value="<$scheduled_end_date$>">Scheduled End Date</option>
                                                            <option value="<$actual_end_date$>">Actual End Date</option>
                                                            <option value="<$estimated_duration$>">Estimated Duration</option>
                                                            <option value="<actual_duration>">Actual Duration</option>
                                                            <option value="<$notes$>">Notes</option>                                                             
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('job_resolve_body_fields','job_resolve_body')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>    
                                        -->
                                        <div class="row">
                                            <div class="col-12"><hr></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-8 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>
                                                        Close Job Email Subject
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <button type="button" onclick="test_email('job','job_close_subject','job_close_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                    </label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("job_close_assigned_to_me"))
                                                            {
                                                                value = this_setting[4];
                                                            }
                                                        }
                                                        %>
                                                        <input type='text' class="form-control" id="job_close_subject" name="job_close_subject" value="<%=value%>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Subject Job Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="job_close_subject_fields" id="job_close_subject_fields">
                                                            <option value="<$job_id$>">Job ID</option>
                                                            <option value="<$name$>">Name</option>
                                                            <option value="<$description$>">description</option>
                                                            <option value="<$assigned_group_id_name$>">Assigned Group Name</option>
                                                            <option value="<$assigned_to_id_username$>">Assigned To</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$status$>">Status</option>
                                                            <option value="<$scheduled_start_date$>">Scheduled Start Date</option>
                                                            <option value="<$actual_start_date$>">Actual Start Date</option>
                                                            <option value="<$scheduled_end_date$>">Scheduled End Date</option>
                                                            <option value="<$actual_end_date$>">Actual End Date</option>
                                                            <option value="<$estimated_duration$>">Estimated Duration</option>
                                                            <option value="<actual_duration>">Actual Duration</option>
                                                            <option value="<$notes$>">Notes</option>                                                           
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('job_close_subject_fields','job_close_subject')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">    
                                            <div class="col-xl-8 col-lg-6 col-12">
                                                <div class='input-group'>
                                                    <label>Close Job Email Body</label>
                                                    <div class='input-group'>
                                                        <%
                                                        value = "";
                                                        for(int a = 0; a < all_personal_notifications.size();a++)
                                                        {
                                                            String this_setting[] = all_personal_notifications.get(a);
                                                            if(this_setting[2].equalsIgnoreCase("job_close_assigned_to_me"))
                                                            {
                                                                value = this_setting[5];
                                                            }
                                                        }
                                                        %>
                                                        <textarea rows="5" class="form-control" id="job_close_body" name="job_close_body" ><%=value%></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-5 col-12">
                                                <div class='input-group'>
                                                    <label>Insert Body Job Record Value</label>
                                                    <div class='input-group'>
                                                        <select class="form-control" name="job_close_body_fields" id="job_close_body_fields">
                                                            <option value="<$job_id$>">Job ID</option>
                                                            <option value="<$name$>">Name</option>
                                                            <option value="<$description$>">description</option>
                                                            <option value="<$assigned_group_id_name$>">Assigned Group Name</option>
                                                            <option value="<$assigned_to_id_username$>">Assigned To</option>
                                                            <option value="<$priority$>">Priority</option>
                                                            <option value="<$status$>">Status</option>
                                                            <option value="<$scheduled_start_date$>">Scheduled Start Date</option>
                                                            <option value="<$actual_start_date$>">Actual Start Date</option>
                                                            <option value="<$scheduled_end_date$>">Scheduled End Date</option>
                                                            <option value="<$actual_end_date$>">Actual End Date</option>
                                                            <option value="<$estimated_duration$>">Estimated Duration</option>
                                                            <option value="<actual_duration>">Actual Duration</option>
                                                            <option value="<$notes$>">Notes</option>                                                           
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-2 col-12">
                                                <div class='input-group'>
                                                    <label>&nbsp;</label>
                                                    <button type="button" onclick="add_get_select_value('job_close_body_fields','job_close_body')" class="btn btn-primary mr-1">
                                                        <i class="ft-plus"></i> Add Field
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                                    
                                                    
                                        <div class="form-actions">
                                            <button type="button" onclick="javascript:location.href='admin.jsp'" class="btn btn-warning mr-1">
                                                <i class="ft-x"></i> Cancel
                                            </button>
                                            <button type="submit" class="btn btn-info">
                                                &nbsp;&nbsp;<i class="ft-save"></i> Save&nbsp;&nbsp;
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
<jsp:include page='footer.jsp'>
    <jsp:param name="parameter" value=""/>
</jsp:include>	
<script src="assets/js/bs-custom-file-input.min.js"></script>

<!-- BEGIN PAGE LEVEL JS-->
<!-- BEGIN VENDOR JS-->    
<!--    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
     BEGIN VENDOR JS
     BEGIN PAGE VENDOR JS
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
     END PAGE VENDOR JS
     BEGIN MODERN JS
    <script src="app-assets/js/core/app-menu.js"></script>
    <script src="app-assets/js/core/app.js"></script>
     END MODERN JS
     BEGIN PAGE LEVEL JS
    <script src="app-assets/js/scripts/popover/popover.js"></script>-->
<script>
    function validateForm() 
    {
        var password = document.getElementById('password').value;
        if (document.getElementById('role_id_0').checked && password == "") 
        {
            alert("If Login Role is checked, then Password must be filled in.");
            return false;
        } 
        else 
        {
            return true;
        }
    }    
</script>
<script>
    function test_email(type, subject, body)
    {
        var subject_element = document.getElementById(subject).value;
        var body_element = document.getElementById(body).value;
        window.open ("email_test_notification.jsp?type=" + type + "&subject=" + subject_element + "&body=" + body_element , '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
    }
</script>
<script>        
    function add_get_select_value(source_id, target_id) 
    {
        var source_value = document.getElementById(source_id).value;
        var current_target_value = document.getElementById(target_id).value;
        var new_target_value = current_target_value + source_value;
        document.getElementById(target_id).value = new_target_value;
    }
    function deleteAttachment()
    {
        var id = $(event.target).closest("a").data('id');
        var div = $(event.target).closest("div").replaceWith('<input type="hidden" name="attachment_delete_id" value="' + id +'"/>');
    }
    bsCustomFileInput.init();
</script>




<!--<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="app-assets/vendors/js/forms/select/select2.full.min.js"></script>
<script src="app-assets/js/scripts/forms/select/form-select2.js"></script>-->
<!-- END PAGE LEVEL JS-->   
        <%
        con.close();
        } 
        catch (Exception e) 
        {
            System.out.println("Exception in admin_user_edit.jsp: " + e);
            logger.debug("ERROR: Exception in admin_user_edit.jsp:" + e);
        }
    } //end if not redirected
%>
</body>
</html>

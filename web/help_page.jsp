<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : help_page
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="org.apache.commons.text.StringEscapeUtils"%>

<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        response.sendRedirect("session_timeout.jsp");
    }
    else 
    {
        
        //session vars
        boolean page_authorized = support.role.authorized(session, "incident","read");          
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap sys_props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session); 
            ArrayList<String[]> help_contents = db.get_help.all(con);
            
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>
    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL CSS-->
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!--Start Page level css-->
    <link rel="stylesheet" type="text/css" href="assets/css/dashboard.css">
    <!--End Page level css--><!-- END Custom CSS-->
    
    	
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title"><i class="fa fa-question-circle"></i>&nbsp;Help</h3>
                    
                </div>
            </div>
            <!-- start content here-->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <h4>
                                    <a name="top"></a>Help Topics
                                </h4>
                                <table id="incident_table" class="table table-striped table-bordered zero-configuration">
                                    <thead>
                                        <tr>
                                            <th class="no-sort">Topic</th>
                                            <th>URL</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                        for(int a = 0; a < help_contents.size(); a++)
                                        {
                                            String help_content[] = help_contents.get(a);
                                            %>
                                            <tr onclick="location.href='#<%=help_content[2]%>';" style="cursor:pointer;">
                                                <td><%=help_content[1]%></td>
                                                <td><%=help_content[3]%></td>
                                                <td><%=help_content[4]%></td>
                                            </tr>
                                            <%
                                        }
                                        %>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Topic</th>
                                            <th>URL</th>
                                            <th>Description</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                                    
                                    
            <%
            for(int a = 0; a < help_contents.size(); a++)
            {
                String help_content[] = help_contents.get(a);
                String id = help_content[0];
                String topic = help_content[1];
                String anchor = help_content[2];
                String url = help_content[3];
                String description = help_content[4];
                String html_content = help_content[5];
                
                %>
                <div class="row">
                    <div class="col-12">
                        <div class="card">                        
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <h4><a name="<%=anchor%>"></a><%=topic%>&nbsp;&nbsp;&nbsp;&nbsp;<small><a href="#top" style="cursor:pointer;"><i class="ft ft-chevrons-up"  ></i>Back to the Top</a></small></h4>
                                    <%=StringEscapeUtils.unescapeHtml4(html_content)%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%
            }
            %>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
                                    
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- BEGIN VENDOR JS-->    
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="app-assets/js/core/app-menu.js"></script>
    <script src="app-assets/js/core/app.js"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/popover/popover.js"></script>

    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>    
    
    
    <script>
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>
    
    <!-- END PAGE LEVEL JS-->
    <script>
        $('#incident_table').dataTable( {
            "order": []
        } );
    </script>
    </body>
</html>
<%     
    }//end if not logged in
}
%>
<!--Copyright 2019 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        if (!session.getAttribute("administration").toString().equalsIgnoreCase("true") && !session.getAttribute("manager").toString().equalsIgnoreCase("true")) 
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
        
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                String home_page = session.getAttribute("home_page").toString();
                Connection con = db.db_util.get_contract_connection(context_dir, session); 
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String abandoned_time_chat = db.get_configuration_properties.by_name(con, "abandoned_time_chat").trim();
                String abandoned_time_phone = db.get_configuration_properties.by_name(con, "abandoned_time_phone").trim();               
                
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <!-- END PAGE LEVEL CSS-->

<div class="clr whtbg p-10 mb-15">
    <h1 class="large-font boldFont">Abandoned Contact Thresholds</h1>
</div>

<jsp:include page="alerts.jsp" />

<div class="sort-tbl-holder">

    <form class="form" action="admin_abandoned_thresholds" method="post">    
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="abandoned_time_phone">Phone Abandoned Time (Seconds)</label>
                    <input type="text" id="abandoned_time_phone" name="abandoned_time_phone" class="form-control" value="<%=abandoned_time_phone%>">
                </div>
            </div>  
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="abandoned_time_chat">Chat Abandoned Time (Seconds)</label>
                    <input type="text" id="abandoned_time_chat" name="abandoned_time_chat" class="form-control" value="<%=abandoned_time_chat%>">
                </div>
            </div>  
        </div>
        <div class="row ">
            <div class="col-md-12 pt-30 pb-30">
                <button type="button" onclick="javascript:location.href='admin_settings.jsp'"  class="btn btn-outline-primary customBtn lg waves-effect">Cancel</button>
                <button type="submit" class="btn btn-primary-new customBtn lg waves-effect">
                    Save
                </button>
            </div>
        </div>                                
    </form>
</div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->   
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_user.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_user.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

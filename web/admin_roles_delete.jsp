<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String FINANCIAL = session.getAttribute("financial").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String CONTACT = session.getAttribute("contact").toString();
        String CX = session.getAttribute("cx").toString();
        String SURVEY = session.getAttribute("survey").toString();
        String SLA = session.getAttribute("sla").toString();        
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        String ASSET = session.getAttribute("asset").toString();
        
        
        if(!ADMINISTRATION.equalsIgnoreCase("true") && !MANAGER.equalsIgnoreCase("true"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String role_id = request.getParameter("role_id");
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END PAGE LEVEL CSS-->
    
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title">Roles</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a></li>
                                <li class="breadcrumb-item"><a href="#">Administration</a></li>
                                <li class="breadcrumb-item"><a href="admin_roles.jsp">User Roles</a></li>
                                <li class="breadcrumb-item"><a href="#">Delete Role</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div> <!--End breadcrumbs -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Delete ServeOptics Role</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <%
                                ArrayList<String[]> users_with_role = db.get_users.users_with_role(con, role_id);
                                if(users_with_role.size() > 0)
                                {
                                    %>
                                    The following users have this role. Deleting this Role will remove their access to ServeOptics.
                                    <table class="table table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>Username</th>
                                                <th>First</th>
                                                <th>Last</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%    
                                            for(int a = 0; a < users_with_role.size();a++)
                                            {
                                                String user[] = users_with_role.get(a);
                                                %>
                                                <tr>
                                                    <td><i class="la la-edit info" onclick="javascript:location.href='admin_users_edit.jsp?id=<%=user[0]%>'" style="cursor: pointer"></i>&nbsp;<i class="la la-user-times danger" onclick="javascript:location.href='admin_users_delete.jsp?id=<%=user[0]%>'" style="cursor: pointer"></i>&nbsp;&nbsp;<%=user[1]%></td>
                                                    <td><%=user[3]%></td>
                                                    <td><%=user[5]%></td>
                                                    <td><%=user[9]%></td>
                                                </tr>
                                                <%
                                            }                                          
                                            %>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Username</th>
                                                <th>First</th>
                                                <th>Last</th>
                                                <th>Email</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <%
                                }
                                %>
                                
                                <div class="alert bg-warning alert-icon-right alert-arrow-right alert-dismissible mb-2" role="alert">
                                    <span class="alert-icon"><i class="la la-warning"></i></span>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Warning!</strong> This action can not be undone! 
                                    <button type="button" onclick="javascript:location.href='admin_role_delete?id=<%=role_id%>'" class="btn btn-info mr-1">
                                        <i class="ft-x"></i> Delete Role
                                    </button>
                                </div>
                                <div class="form-actions">
                                    <button type="button" onclick="javascript:location.href='admin_roles.jsp'" class="btn btn-blue-grey mr-1">
                                        <i class="ft-x"></i> Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->   
    <%
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_user_delete.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_user_delete.jsp:" + e);
            }
        } //end if not admin permission
    } //end if not redirected
    %>
    </body>
</html>

<!--Copyright 2019 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_sla_add.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>

<%
if (session.getAttribute("authenticated") == null) 
{
    String context_dir = request.getServletContext().getRealPath("");
    LinkedHashMap props = support.config.get_config(context_dir);
    String login_url = props.get("login_url").toString();
    response.sendRedirect(login_url);
} 
else 
{
    boolean authorized = support.role.authorized(session, "sla","create");
    if(!authorized)
    {
        response.sendRedirect("no_permission.jsp");
    }  
    else 
    {
        Logger logger = LogManager.getLogger();
        String home_page = session.getAttribute("home_page").toString();
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        try 
        {
            Connection con = db.db_util.get_contract_connection(context_dir, session); 
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
%>  
<!-- BEGIN PAGE LEVEL CSS-->
<!-- END PAGE LEVEL CSS-->
            <form class="form" action="admin_contact_sla_add" method="post">
                <!-- start content here-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only mb-15">
                                                        SLA Display Name
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0 ">
                                                    <input type="text" id="name" name="name" class="form-control" placeholder="SLA Name" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only mb-15">
                                                        Short Description (Optional)
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0 ">
                                                    <input type="text" id="description" name="description" class="form-control" placeholder="Description" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md mb-15">
                                                        SLA Target
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">
                                                    <input type="text" id="target" name="target" class="form-control" placeholder="SLA Target in percentage 0-100%" />                                    
                                                </div>
                                            </div> 
                                            <div class="col-md-6 pt-10">
                                                <div class="custom-control custom-checkbox dInlineBlock mr-15">
                                                    <input type="checkbox" class="custom-control-input" id="active" name="active">
                                                    <label class="custom-control-label small-font" for="active">Active?</label>
                                                </div>
                                                <div class="custom-control custom-checkbox dInlineBlock mr-15">
                                                    <input type="checkbox" class="custom-control-input" id="reportable" name="reportable">
                                                    <label class="custom-control-label small-font" for="reportable">Do you want this SLA included on the Incident Dashboard?</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="field-title">
                                                    <label class="formLabel md mb-15">
                                                        SLA Text Detail (Optional)
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">
                                                    <textarea class="form-control" name="sla_text" id="sla_text" rows="3" placeholder="Enter the SLA text here"></textarea>  
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md mb-15">
                                                        SLA Type
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">                                                
                                                    <select onchange="show_abandoned()" id="sla_type" name="sla_type" class="form-control">
                                                        <option value="--"></option>
                                                        <option value="ABANDONED_CONTACT_RATE">Abandoned Contact Rate</option>
                                                        <option value="ABANDONED_CONTACT_COUNT">Abandoned Contact Count</option>
                                                        <option value="AVERAGE_POST_PROCESS_TIME">Average Post Process Time</option>
                                                        <option value="AVERAGE_SPEED_TO_ANSWER">Average Speed to Answer</option>
                                                        <option value="AVERAGE_HANDLE_TIME">Average Handle Time</option>
                                                        <option value="AVERAGE_PROCESSING_TIME">Average Processing Time</option>
                                                    </select>                                            
                                                </div>
                                            </div> 
                                            <div class="col-md-6" id="abandoned_threshold_div" style="display:none">
                                                <div class="field-title">
                                                    <label class="formLabel md mb-15">
                                                        What is minimum amount of time for an Abandoned call to be counted as Abandoned?
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">                                                
                                                    <input type="text" id="abandoned_threshold" name="abandoned_threshold" class="form-control" placeholder="Abandoned time in seconds" />       
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md mb-15">
                                                        SLA Timeframe
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">                                                
                                                    <select id="sla_timeframe" name="sla_timeframe" class="form-control">
                                                        <option value="DAILY">Daily</option>
                                                        <option value="WEEKLY">Weekly</option>
                                                        <option value="MONTHLY">Monthly</option>
                                                        <option value="QUARTERLY">Quarterly</option>
                                                        <option value="ANNUAL">Annual</option>
                                                        <option value="CUSTOM">Custom</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md mb-15">
                                                        Select all the Channel(s) that this SLA will apply
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">                                                
                                                    <select class="select2 form-control" multiple="multiple" id="channels" name="channels">
                                                        <%
                                                        String all_channels[] = db.get_contact.all_channels(con);
                                                        for(int a = 0; a < all_channels.length;a++)
                                                        {
                                                            %>
                                                            <option value="<%=all_channels[a]%>"><%=all_channels[a]%></option>
                                                            <%
                                                        }
                                                        %>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <h4 class="mediumFont large-font mb-20">Compliance Parameters</h4>
                                            </div>
                                        </div>

                                    <div class="row">
                                        <div class="col-md-6" id="operator_div" style="display:none">
                                            <div class="field-title">
                                                <label class="formLabel md mb-15">
                                                    Operator
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <select id="sla_success_threshold_operator" name="sla_success_threshold_operator" class="form-control">
                                                    <option value="greater_than">Greater than</option>
                                                    <option value="greater_than_or_equal">Greater than or equal to</option>
                                                    <option value="equal_to">Equal to</option>
                                                    <option value="not_equal_to">Not equal to</option>
                                                    <option value="less_than_or_equal">Less than or equal</option>
                                                    <option selected value="less_than">Less than</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6" id="value_div">
                                            <div class="field-title">
                                                <label class="formLabel md mb-15">
                                                    Value
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0" id="value_div_content">
                                                <input type="text" id="sla_success_threshold_value" name="sla_success_threshold_value" class="form-control" placeholder="Enter a whole number" />
                                            </div>
                                        </div>     
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" id="unit_div" style="display:none">
                                            <div class="field-title">
                                                <label class="formLabel md mb-15">
                                                    Unit
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <select id="sla_success_threshold_unit" name="sla_success_threshold_unit" class="form-control">
                                                    <option value="seconds">Seconds</option>
                                                    <option value="minutes">Minutes</option>
                                                    <option value="hours">Hours</option>
                                                    <option value="days">Days</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-md-12 text-center pt-30 pb-30">
                                            <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                                            <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                                                Save
                                            </button>
                                        </div>
                                    </div>
                <input type="hidden" id="report_name" name="report_name" value="" />
                <input type="hidden" id="report_description" name="report_description" value="" />
                <input type="hidden" id="report_header" name="report_header" value="" />
            </form>
    
    <!-- BEGIN PAGE LEVEL JS-->
    
    <!-- END PAGE LEVEL JS-->
    
<%                con.close();

            } catch (Exception e) 
            {
                System.out.println("Exception in admin_sla_add.jsp: " + e);
                logger.debug("ERROR: Exception in admin_sla_add.jsp:" + e);
            }
        } //end if not admin permission
    } //end if not redirected
%>
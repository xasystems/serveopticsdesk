<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->

        <%
        String field_name = request.getParameter("field_name");
        String field_label = request.getParameter("field_label");
        String number_cols = request.getParameter("number_cols");
        String field_values = request.getParameter("field_values");   
        String new_record = request.getParameter("new_record"); 
        String default_text = request.getParameter("default_text"); 
        String db_value = request.getParameter("db_value"); 
        String value = "";
        if(new_record.equalsIgnoreCase("true"))
        {
            value = default_text;
        }
        else
        {
            value = db_value;
        }
        
        %>
        <div class="col-<%=number_cols%>">
            <label><br><%=field_label%></label>
            <textarea class="form-control" name="<%=field_name%>" id="<%=field_name%>" rows="5"><%=value%></textarea>  
        </div>
        
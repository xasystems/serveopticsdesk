<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    String context_dir = request.getServletContext().getRealPath("");
    LinkedHashMap props = support.config.get_config(context_dir);
    if(session.getAttribute("authenticated")==null)
    {
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        try
        {
            Connection con = db.db_util.get_contract_connection(context_dir, session); 
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            con.close();
        }
        catch(Exception e)
        {
            System.out.println("Exception in admin_incident_sla_delete.jsp=" + e);
        }
        String home_page = session.getAttribute("home_page").toString();
        String status = "";
        String text = "";
        String reason = "";
        String source = "";
        try{status = request.getParameter("status");}catch(Exception e){}
        try{text = request.getParameter("text");}catch(Exception e){}
        try{reason = request.getParameter("reason");}catch(Exception e){}
        try{source = request.getParameter("source");}catch(Exception e){}
        
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <!-- END PAGE LEVEL CSS-->
    
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title">System Error</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div> <!--End breadcrumbs -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <div class="alert bg-warning alert-icon-right alert-arrow-right mb-2" role="alert">
                                    <span class="alert-icon"><i class="la la-warning"></i></span>
                                    
                                    <strong>A System Error has occurred!</strong>
                                    <br>
                                    <br>
                                    <h3>Source: <%=source%></h3> 
                                    <br>
                                    <h3>Status: <%=status%></h3> 
                                    <br>
                                    <h3>Text: <%=text%></h3> 
                                    <br>
                                    <h3>Reason: <%=reason%></h3> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->   
    <%
    } //end if not redirected
    %>
    </body>
</html>

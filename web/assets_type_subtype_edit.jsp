<!--Copyright 2019 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        boolean authorized = support.role.authorized(session, "asset","update"); 
        if(!authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);  
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String type_id = request.getParameter("type_id");
                String type_info[] = db.get_assets.info_by_type_id(con, type_id);
    %>
    <!-- BEGIN PAGE LEVEL CSS-->
    <!-- END PAGE LEVEL CSS-->
    
                                <form class="form" action="assets_type_subtype_edit" method="post">
                                    <input type="hidden" name="type_id" value="<%=type_id%>"/>
                                    <div class="sort-tbl-holder">
                                        <table class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th width="20%" class="text-center">Active</th>
                                                    <th>Type</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-center">
                                                        <%
                                                        String checked = "";
                                                        if(type_info[2].equalsIgnoreCase("true"))
                                                        {
                                                            checked = "CHECKED";
                                                        }
                                                        %>
                                                        <input <%=checked%> type="checkbox" name="type_active" id="category_active"/></div>
                                                    </td>
                                                    <td>                                                        
                                                        <input class="form-control"  type="text" name="type_name" id="type_name" value="<%=type_info[1]%>"/>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="row mb-15">
                                        <div class="col-md-12">
                                            <span onclick="add_row()" class="btn btn-primary-new customBtn lg waves-effect float-right">
                                                <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>Add Subtype
                                            </span>
                                        </div>
                                    </div>

                                    <div class="sort-tbl-holder" >
                                        <table class="table table-striped custom-sort-table" id="option_table" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="text-center" width="20%"><b>Active</b></th>
                                                    <th width="74%"><b>Subtype</b></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%
                                                ArrayList<String[]> subtypes = db.get_assets.all_subtypes_for_type_id(con, type_info[0]);
                                                for(int b = 0; b < subtypes.size(); b++)
                                                {
                                                    String subtype[] = subtypes.get(b);
                                                    checked = "";
                                                    if(subtype[3].equalsIgnoreCase("true"))
                                                    {
                                                        checked = "CHECKED";
                                                    }
                                                    %>
                                                    <tr>
                                                        <td class="text-center">
                                                            <div class="custom-control custom-checkbox dInlineBlock">
                                                                <input type="hidden" name="subtype_id_<%=subtype[0]%>" value="<%=subtype[0]%>"">
                                                                <input name="subtype_active_<%=subtype[0]%>" id="subtyperow_<%=subtype[0]%>" class="custom-control-input" type="checkbox" value="true" <%=checked%>>
                                                                <label class="custom-control-label small-font" for="subtyperow_<%=subtype[0]%>"></label>
                                                            </div
                                                        </td>
                                                        <td>
                                                            <input class="form-control" type="text" name="subtype_name_<%=subtype[0]%>" id="subcategory_name_<%=subtype[0]%>" value="<%=subtype[2]%>"/>
                                                        </td>
                                                        <td>
                                                            <a href='javascript:void(0)' onclick='delete_row(this)' class='mr-5'><img src='assets/images/svg/trash-icon-blk.svg' alt=''></a>
                                                        </td>
                                                    </tr>
                                                    <%
                                                }
                                                %>
                                            </tbody>

                                        </table>
                                    </div>

                                        <div class="row ">
                                            <div class="col-md-12 text-center pt-30 pb-30">
                                                <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                                                <button type="submit" class="btn btn-primary-new customBtn lg waves-effect">
                                                    Save
                                                </button>
                                            </div>
                                        </div>                                
                                </form>
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->  
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_systen_select_fields.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_systen_select_fields.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

<%-- 
    Document   : problem_list.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="org.apache.commons.codec.digest.HmacAlgorithms"%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.time.ZonedDateTime"%>
<%@ page import="java.time.format.DateTimeFormatter"%>
<%@ page import="org.apache.commons.codec.digest.HmacUtils"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        boolean page_authorized = support.role.authorized(session, "problem","read");  
        String user_id = "0";
        try
        {
            user_id = session.getAttribute("user_id").toString(); 
        }
        catch(Exception e)
        {
            user_id = "0";
        }
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            String user_tz_name = "UTC";
            String user_tz_time = "+00:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "UTC";
                user_tz_time = "+00:00";
            }
            boolean problem_delete = false;
            boolean problem_update = false;
            String date_range = "";
            try 
            {
                date_range = request.getParameter("date_range");
                if (date_range.equalsIgnoreCase("null") || date_range == null) 
                {
                    date_range = support.filter_dates.past_30_days();
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                date_range = support.filter_dates.past_30_days();
            }

            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020            
            String action = StringUtils.defaultString(request.getParameter("action"));
            String predefined = StringUtils.defaultString(request.getParameter("predefined"));
            String problem_id = StringUtils.defaultString(request.getParameter("problem_id"));
            String incident_id = StringUtils.defaultString(request.getParameter("incident_id"));

            date_time_picker_format.setTimeZone(TimeZone.getTimeZone(user_tz_name));
            java.util.Date now = new java.util.Date();
            String now_picker_time = date_time_picker_format.format(now);
            String timestamp_string = timestamp_format.format(now);
            
            ArrayList<String[]> problems = new ArrayList();

            if(session.getAttribute("problem").toString().equalsIgnoreCase("delete") || session.getAttribute("administration").toString().equalsIgnoreCase("true") || session.getAttribute("manager").toString().equalsIgnoreCase("true"))
            {
                problem_delete = true;
                problem_update = true;
            }
            else if(session.getAttribute("problem").toString().equalsIgnoreCase("update"))
            {
                problem_delete = false;
                problem_update = true;
            }

            String couchdb_username = timestamp_string;
//            String couchdb_token = HmacUtils(HmacAlgorithms.HMAC_SHA_1, couchdb_username).hmacHex(couchdb_username);
            String couchdb_secret = props.get("couchdb.secret").toString();
            String couchdb_token = HmacUtils.hmacSha1Hex(couchdb_secret, couchdb_username);
            ArrayList<String[]> status_select = db.get_system_select_field.active_for_table_column(con, "problems", "status");
            ArrayList<String[]> final_status_select = db.get_system_select_field.active_for_table_column(con, "problems", "final_status");
            ArrayList<String[]> priority_select = db.get_system_select_field.active_for_table_column(con, "problems", "priority");
            String status = StringUtils.defaultString(request.getParameter("status"));
            String final_status = StringUtils.defaultString(request.getParameter("final_status"));
            String priority = StringUtils.defaultString(request.getParameter("priority"));

                                              
    %>
<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>


<jsp:include page='menu_service_desk.jsp'>
    <jsp:param name="active_menu" value=""/>
</jsp:include>	

<!-- BEGIN PAGE LEVEL CSS-->
<style type="text/css">
    .dropdown-menu.md {
        width: 500px;
    }
    .dropdown-menu input {
        width: auto !important;
    }
</style>
<link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
<!-- END VENDOR CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
<!--<link rel="stylesheet" type="text/css" href="assets/css/dashboard.css">-->
<link rel="stylesheet" type="text/css" href="assets/css/chat_app.css">
<style>
.sort-tbl-holder {
    background-color: #F7F8FC !important;
}
</style>
<!--End Page level css-->

<div class="clr whtbg p-10 mb-15">
    <div class="float-right options">
        <ul>
            <li id="filters_dropdown" class="dInlineBlock dropdown ">
                <div class="dInlineBlock clr menuHolder">
                    <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                    </a>
                    <div class="dropdown-menu dropdown-menu-right md p-10" x-placement="bottom-end" style="position: absolute; transform: translate3d(100px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <form id="filter_form">
                            <ul>
                                <li class="mb-10 clr">
                                    <label class="field-label full-width">
                                        Problem Date
                                    </label>
                                    <div class="formField md  mr-0 full-width">
                                        <input class="no-border full-width datetime" type="text" name="date_range" id="filter_date_range" value="<%=date_range%>">
                                    </div>
                                </li>
                                <li class="mb-10 clr">
                                    <label class="field-label full-width">
                                        Status
                                    </label>
                                    <div class="formField md full-width mr-0">
                                        <%
                                        %>
                                        <select name="status" class="form-control border-0 h-auto px-0 mb-0">
                                            <option value="Any">Any</option>        
                                            <%
                                            String selected;
                                            for(int a = 0; a < status_select.size(); a++)
                                            {
                                                String select_option[] = status_select.get(a);
                                                if(status.equalsIgnoreCase(select_option[3]))
                                                {
                                                    selected = "SELECTED";
                                                }
                                                else
                                                {
                                                    selected = "";
                                                }
                                                //select_option[3] = value    select_option[4] = label
                                                %>
                                                <option <%=selected%> value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                                                <%
                                            }
                                            %>
                                        </select>
                                    </div>
                                </li>
                                <li class="mb-10 clr">
                                    <label class="field-label full-width">
                                        Final Status
                                    </label>
                                    <div class="formField md full-width mr-0">
                                        <%
                                        %>
                                        <select name="final_status" class="form-control border-0 h-auto px-0 mb-0">
                                            <option value="Any">Any</option>        
                                            <%
                                            String final_status_selected;
                                            for(int a = 0; a < final_status_select.size(); a++)
                                            {
                                                String select_option[] = final_status_select.get(a);
                                                if(final_status.equalsIgnoreCase(select_option[3]))
                                                {
                                                    final_status_selected = "SELECTED";
                                                }
                                                else
                                                {
                                                    final_status_selected = "";
                                                }
                                                //select_option[3] = value    select_option[4] = label
                                                %>
                                                <option <%=final_status_selected%> value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                                                <%
                                            }
                                            %>
                                        </select>
                                    </div>
                                </li>
                                <li class="mb-10 clr">
                                    <label class="field-label full-width">
                                        Priority
                                    </label>
                                    <div class="formField md full-width mr-0">
                                        <%
                                        %>
                                        <select name="priority" class="form-control border-0 h-auto px-0 mb-0">
                                            <option value="Any">Any</option>        
                                            <%
                                            String priority_selected;
                                            for(int a = 0; a < priority_select.size(); a++)
                                            {
                                                String select_option[] = priority_select.get(a);
                                                if(priority.equalsIgnoreCase(select_option[3]))
                                                {
                                                    priority_selected = "SELECTED";
                                                }
                                                else
                                                {
                                                    priority_selected = "";
                                                }
                                                //select_option[3] = value    select_option[4] = label
                                                %>
                                                <option <%=priority_selected%> value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                                                <%
                                            }
                                            %>
                                        </select>
                                    </div>
                                </li>
                                <li class="mb-10 clr">
                                    <label class="field-label full-width">
                                        Number of Incidents
                                    </label>
                                    <div class="formField md full-width mr-0">
                                        <input type="text" name="number_incidents" value="" placeholder="Any" class="border-0 full-width" >
                                    </div>
                                </li>                                
                                <li class="mb-10 clr">
                                    <label class="field-label full-width">
                                        Assigned To (Last First)
                                    </label>
                                    <div class="formField md full-width mr-0">
                                        <input type="hidden" name="assigned_to_id" id="assigned_to_id" value="">
                                        <input type="text" id="assigned_to_name" name="assigned_to_name" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                    </div>
                                </li>
                            </ul>
                            <div class="text-center pt-5 pb-5">
                                <button type="button" class="btn md2 btn-primary-new customBtn mb-5" onclick="apply_filters()">Apply Filters</button>
                                <div class="clr"></div>
                                <a href="#" class="">Reset Filters</a>
                            </div>
                        </form>
                    </div>
                </div>
            </li>
            <li class="dInlineBlock">
                <a id="new_problem_button" data-target="problem" data-problem-id="0" class="btn btn-success customBtn lg waves-effect rightSidePanelOpener" href="#">
                    <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>New Problem
                </a>    
            </li>
        </ul>
    </div>
    <h1 class="large-font boldFont">Problems</h1>
</div>

<%
        String[] filter_fields = {
                 "serial_number",
                 "asset_tag",
                 "manufacturer",
                 "model",
                 "asset_type",
                 "asset_subtype",
                 "purchase_order",
                 "warranty_expire",
                 "state",
                 "state_date",
                 "assigned_to_id",
                 "assigned_group_id",
                 "department",
                 "location",
                 "site",
                 "company",
                 "ip",
                 "notes"
        };
        List<Map<String, String>> filters = new ArrayList<>();
        for (int i = 0; i < filter_fields.length; i++)
        {
            String filter_field = filter_fields[i];
            String filter_value = request.getParameter(filter_field);
            if (filter_value != null && !filter_value.isEmpty()) 
            {
                Map<String, String> map = new HashMap<String, String>();
                map.put("key", filter_field);
                map.put("value", filter_value);
                filters.add(map);
            }
        }
        request.setAttribute("filter_fields", filter_fields);

    %>
<div id="active_filters">
    <jsp:include page="active_filters_row.jsp" />
</div>

<div class="sort-tbl-holder">
    <table id="data_table" class="table table-striped custom-sort-table d-none" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>ID</th>
                <th>Priority</th>
                <th>Status</th>
                <th>Final Status</th>
                <th>Problem Time</th>
                <th>Name</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
        </thead>
    </table>
</div>
<jsp:include page='footer.jsp'>
    <jsp:param name="parameter" value=""/>
</jsp:include>	

<div class="rightPanelHolder">
    <div class="p-15 ticket-content">
        <span class="rightSidePanelCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
        <h4 id="rightPanelTitle" class="mediumFont large-font mb-20"></h4>
        <div id="rightPanelContent" class="clr whtbg p-15">
        </div>
    </div>
</div>

<div class="rightPanelHolder2">
    <div class="p-15 ticket-content">
        <span class="rightSidePanelCloser2"><img src="assets/images/svg/close-icon.svg" alt=""></span>
        <h4 id="rightPanelTitleChat" class="mediumFont large-font mb-20"></h4>
        <div id="chat_app" class="clr whtbg p-15">
        </div>
    </div>
</div>

<div class="modal fade" id="deleteAttachmentModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteModalLabel">Attachment delete confirmation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary-new" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger" onclick="delete_attachment()">Delete</button>
        </div>
      </div>
    </div>
</div>                     

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteModalLabel">Delete Confirmation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="deleteModalBody">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger" onclick="delete_entity()" id="modalDeleteButton">Delete</button>
        </div>
      </div>
    </div>
</div>                     

<script type="text/javascript">
    function showAttachmentDeleteModal() 
    {
        var file_name = $(event.target).closest("span").html();
        var id = $(event.target).closest("a").data('id');
        var modal = $('#deleteAttachmentModal');
        modal.find("#deleteModalLalbel").html("");
        modal.modal();
    }
    function deleteAttachment()
    {
        var id = $(event.target).closest("a").data('id');
        var div = $(event.target).closest("div").replaceWith('<input type="hidden" name="attachment_delete_id" value="' + id +'"/>');
    }
    function showDeleteModal(delete_entity_id, entity_type) 
    {
        switch (entity_type) {
            case "problem":
                $("#deleteModalBody").html("Are you sure you want to delete this Problem?");
                break;
            case "incident":
                $("#deleteModalBody").html("Are you sure you want to delete this Incident?");
                break;
            default:
                break;
        }
        $("#modalDeleteButton").prop("data-id", delete_entity_id);
        $("#modalDeleteButton").prop("data-entity", entity_type);
        $('#deleteModal').modal();
    }    
    function delete_entity() 
    {
        let entity_type = $("#modalDeleteButton").prop("data-entity");
        let url = null;
        switch (entity_type) {
            case "problem":
                url = "problem_delete";
                break;
            case "incident":
                url = "incident_delete";
                break;
            default:
                break;
        }
        let delete_entity_id = $("#modalDeleteButton").prop("data-id");
        if (url && delete_entity_id) {
            window.location.assign(url + "?id=" + delete_entity_id);
        }
    }    

</script>

<!-- BEGIN PAGE LEVEL JS-->
<!-- BEGIN VENDOR JS-->    
<script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN MODERN JS-->
<script src="app-assets/js/core/app-menu.js"></script>
<script src="app-assets/js/core/app.js"></script>
<!-- END MODERN JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
<script src="app-assets/js/scripts/popover/popover.js"></script>

<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<!--<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>-->    
<script src="app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>    
<script src="app-assets/js/scripts/tables/datatables-extensions/dataRender/datetime.js"></script>    
<script src="assets/js/tinymce/tinymce.min.js"></script>
<script src="assets/js/bs-custom-file-input.min.js"></script>
<script src="assets/js/axios.min.js"></script>

<!--<script src='https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js'></script>-->
<!--<script src="https://cdn.jsdelivr.net/npm/vue-advanced-chat@0.8.8/dist/vue-advanced-chat.umd.min.js"></script>-->

<!--<script src='https://cdn.jsdelivr.net/npm/babel-polyfill/dist/polyfill.min.js'></script>
<script src='https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js'></script>-->

<script>
    var dt;
    var dt_childs = new Array();
    var dt_rightPanel_incidents;
    var incidentDetailRows = [];    
    var showChat = false;
    var currentUserId = <%=user_id%>;
    var objectType = "";
    var objectId = 0;
    var parentObjectType = "";
    var parentObjectId = 0;
    var serviceDeskEndpoint = "<%=props.get("servicedesk_url").toString()%>";

    function change_filter_category(select)
    {
        $(".filter-category-block").addClass("d-none");
        var category_block = $("#" + $(select).val());
        if (category_block)
        {
            category_block.removeClass("d-none");    
        }
    }

    function apply_filters(draw = true)
    {
        $("#filter_form input,#filter_form select").each(function() {

            var field = $(this);
//            console.log("filter_form_field", field);
            var field_name = field.attr('name');
            
            if (!field_name || field.attr('type') == "hidden" || (field.attr('type') == "radio" && !field.prop("checked")))
            {
                return;
            }
            if (field.val() && field.val() != "Any")
            {
                var label = "";
                var filter_value = "";
                if (field.attr("type") == "radio")
                {
                    label = "Predefined";
                    filter_value = field.siblings("label").html();
                } else {
                    label = field.closest("div").prev().html();
                    filter_value = field.val();
                }
                if ($("#active_filter_" + field_name).length > 0) {
                    var filter_block = $("#active_filter_" + field_name);
                    $(filter_block).children("span").html(filter_value);
                } else {
                    $("#active_filters").append(
                        $('' +
                            '<div class="clr tag dInlineBlock formField md whtbg mr-5 mb-15" id="active_filter_' + field_name + '">' +
                                '<h5 class="basic-font mediumFont">' + label + '</h5>' +
                                '<span>' + filter_value + '</span>' +
                                '<img class="icon" src="assets/images/svg/cross-icon.svg" alt="" onclick="resetFilterFieldNew(\'' + field_name + '\')">' +
                            '</div>'
                        )
                    );
                }
                setQueryStringParameter(field_name, field.val());
            } else {
                resetFilterFieldNew(field_name, false);
            }                
        });
        if(draw)
        {
            console.log("calling draw from apply_filters")
            dt.draw();
        }
    }

    function resetFilterFieldNew(field_name, draw = true)
    {
        var filter_block = $("#active_filter_" + field_name);
        if (filter_block.length > 0)
        {
            filter_block.remove();
            var filter_field = $("#filter_form input[name='" + field_name + "'],#filter_form select[name='" + field_name + "']");
            if (filter_field.attr("type") != "radio")
            {
                filter_field.val("");
            }
            if (field_name == "owner_username" || field_name == "owner_group_name")
            {
                filter_field.trigger('change');
            }
            if (draw) {
                console.log("calling draw from resetfilter")
                dt.draw();
            }
            setQueryStringParameter(field_name, "");
        }
    }

    function addRightSidePanelListeners()
    {
        $('.rightSidePanelCloser:not(.listens)').each(function(){
            $(this).addClass('listens');
            this.addEventListener("click", function()
            {
                $('.rightPanelHolder').removeClass('open');
                $('.rightSidePanel').removeClass('open');
                $('html').removeClass('panelOpen');
            }, false);
        });
        
        $('.rightSidePanelCloser2:not(.listens)').each(function(){
            $(this).addClass('listens');
            this.addEventListener("click", function()
            {
                window.chat_app.showChat = false;
                $('.rightPanelHolder2').removeClass('open');
                $('.rightSidePanel2').removeClass('open');
                $('html').removeClass('panelOpen');
            }, false);
        });

        $('.rightSidePanelOpener2:not(.listens)').each(function(){
            $(this).addClass('listens');
            
            this.addEventListener("click", function()
            {
                $('.rightPanelHolder2').addClass('open');
                $('.rightSidePanel2').addClass('open');
                $('html').addClass('panelOpen');
                
                switch ($(this).data('target'))
                {
                    case "problem":
                        window.chat_app.objectType = "problem";
                        window.chat_app.objectId = $(this).data('problemId');
                        window.chat_app.parentObjectType = "";
                        window.chat_app.parentObjectId = 0;
                        break;
                    case "incident":
                        window.chat_app.objectType = "incident";
                        window.chat_app.objectId = $(this).data('incidentId');
                        window.chat_app.parentObjectType = "problem";
                        window.chat_app.parentObjectId = $(this).data('problemId');
                        break;
                }
//                console.log($(this).data('problemId'));
                window.chat_app.currentUserId = <%=user_id%>;
                window.chat_app.showChat = true;
                console.log("panel2 opened");
            });
        });
        
        $('.rightSidePanelOpener:not(.listens)').each(function(){
//            console.log("attaching to ", this);
            $(this).addClass('listens');
            
            this.addEventListener("click", function()
            {
                $("#rightPanelContent").html("");

                var panel_title = "";
                var ajax = {};
                ajax.url = "";
                ajax.type = 'GET';
                ajax.dataType = "html";
                if (event.target.nodeName == "IMG")
                {
                    var clicked_node = $(event.target).closest("a");
                } else {
                    var clicked_node = $(event.target);
                }
                var target = clicked_node.data('target');
                var problem_id = clicked_node.data('problemId');
                var incident_id = clicked_node.data('incidentId');
                console.log(clicked_node, target, problem_id, incident_id);

                switch (target) 
                {
                    case "chat":
                        if (problem_id)
                        {
                            ajax.url = "chat.html";
                            ajax.data = {problem_id: problem_id};
                            panel_title = "Collaboration by Problem";
                        } else {
                            ajax.url = "chat.html";
                            panel_title = "Collaboration by Incident";
                        }
                        break;
                    case "problem":
                        if (problem_id)
                        {
                            ajax.url = "problem.jsp";
                            ajax.data = {id: problem_id};
                            panel_title = "Edit Problem";
                        } else {
                            ajax.url = "problem_new.jsp<%=(incident_id.equals("") ? "" : "?incident_id=" + incident_id )%>";
                            panel_title = "Create a New Problem";
                        }
                        break;
                    case "incident":
                        if (incident_id)
                        {
                            ajax.data = {id: incident_id};
                            ajax.url = "incident.jsp";
                            panel_title = "Edit Problem Incident";
                        } else {
                            ajax.data = {problem_id: problem_id};
                            ajax.url = "problem_incident_add.jsp";
                            panel_title = "Create a New Problem Incident";
                        }
                        break;
                    default:
                        break;
                }
                
                if (ajax.url === "") return false;

                ajax.error = function(jqXHR, textStatus)
                {
                    switch (jqXHR.status) {
                        case 401:
                            window.location.assign("<%=props.get("login_url").toString()%>");
                            break;
                        case 403:
                            alert("No permission!");
                            break;
                        default:
                            alert("Error processing request!");
                    }
                };

                ajax.success = function (data) 
                {                    
                    $('.rightPanelHolder').addClass('open');
                    $('.rightSidePanel').addClass('open');
                    $('html').addClass('panelOpen');

                    $("#rightPanelTitle").html(panel_title);
                    $("#rightPanelContent").html(data);
                    addRightSidePanelListeners();
                    attachFieldListeners($("#rightPanelContent"));        


                }
                
                $.ajax(ajax);
            }, false);
        });

    }
    
    window.addEventListener("load", (event) => 
    {
        addRightSidePanelListeners();
        // $('.dropdown-menu div#filter_categories li').on('click', function (e) 
        // { // prevent dropdown close on autocomplete
        //     $(".filter-category-block").addClass("d-none");
        //     var selected_category_name = $(this).children("a").html();
        //     var selected_category_id = $(this).children("a").data('category');
        //     $("#filters_dropdown_toggle").html('<span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>' + selected_category_name);
        //     var category_block = $("#"+selected_category_id);
        //     if (category_block)
        //     {
        //         category_block.removeClass("d-none");
        //         $("#filter_categories").addClass("d-none");
        //         $("#filters_dropdown_menu").css("width", "500px");
        //         $("#filter_actions").removeClass("d-none");
        //     }
           
        //     // if (e.clickEvent && e.clickEvent.target && (e.clickEvent.target.className == "ui-menu-item-wrapper" || e.clickEvent.target.className == "icon")) {
        //     //     e.preventDefault();
        //     // }
        // });

        $('#filters_dropdown').on('hide.bs.dropdown', function (e) 
        { // prevent dropdown close on autocomplete
            console.log("dropdown closed ", e);
            //            console.log(e.clickEvent.target.className);
            if (e.clickEvent && e.clickEvent.target && (e.clickEvent.target.className == "ui-menu-item-wrapper" || e.clickEvent.target.className == "icon" || e.clickEvent.target.className.indexOf("applyBtn") !== -1)) {
                e.preventDefault();
            }
        });

        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
        
        dt = $('#data_table').DataTable( {
            language: {
                "info":           "Showing _START_ to _END_ of _TOTAL_ problems",
                "infoEmpty":      "Showing 0 to 0 of 0 problems",
                "infoFiltered":   "(filtered from _MAX_ total problems)",
                "lengthMenu":     "Show _MENU_ problems",
                "zeroRecords":    "No matching problems found",
            },
            autoWidth: false,
            "processing": true,
            "serverSide": true,
            "ordering": true,
            'serverMethod': 'post',
            'searching': false,
            "lengthChange": false,
            dom: "<'row'<'col-sm-3'i><'col-sm-3'f><'col-sm-6'p>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            "ajax": {
                "url": "ajax_lookup_problems",
                'data': function(data){
                    // Read values
                    var filters = {};
                    $("#filter_form input,#filter_form select").each(function() {
                        if ($(this).attr("name") && (($(this).attr("type") == "radio" && $(this).is(':checked')) || ($(this).attr("type") != "radio" && this.value && this.value != "" && this.value.toLowerCase() != "any" && this.value.toLowerCase() != "all" && this.value != 0)))
                        {
                            data[$(this).attr('name')] = this.value;
                        }
                    });

                    data['filters'] = JSON.stringify(filters);
                }
            },
            "pageLength": 50,
            "columnDefs": [
//                <th></th>
//                <th>ID</th>
//                <th>Priority</th>
//                <th>Status</th>
//                <th>Final Status</th>
//                <th>Problem Time</th>
//                <th>Name</th>
//                <th>Description</th>
//                <th>Incidents</th>
//                <th>Assigned To</th>
//                <th>Assigned Group</th>
//                <th>Actions</th>

                { targets: 0, "width": "2%", defaultContent: '<span class="expand-td"><img src="assets/images/svg/collapse-minus-icon.svg" alt=""></span>', orderable: false },
                { targets: 1, "width": "5%", "data": "id" },
                { targets: 2, "width": "8%", name: "priority",  "data": null, 
                    render: function ( data, type, row ) 
                    {
                        var indicator_class = "";
                        switch (row.priority) {
                            case "Low":
                                indicator_class = "low";
                                break;
                            case "Medium":
                                indicator_class = "medium";
                                break;
                            case "High":
                                indicator_class = "high";
                                break;
                            case "Critical":
                                indicator_class = "critical";
                                break;
                            default:
                                indicator_class = "unknown";
                                break;
                        }
                        return '<span class="indicator ' + indicator_class + ' "></span> ' + (row.priority ? row.priority : 'Unknown');
                    }
                },
                { targets: 3, "width": "8%", "data": null, orderable: false,
                    render: function ( data, type, row ) 
                    {
                        return '<span class="btn sm state-' + row.status.toLowerCase() + ' customBtn">' + row.status + '</span>';
                    }
                },
                { targets: 4, "width": "8%", "data": null, orderable: false,
                    render: function ( data, type, row ) 
                    {
                        return '<span class="btn sm state-' + row.final_status.toLowerCase() + ' customBtn">' + row.final_status + '</span>';
                    }
                },
                { targets: 5, "width": "15%", name: "create_date", "data": "create_date", render: $.fn.dataTable.render.moment('YYYYMMDDHHmmss','MM/DD/YYYYY') },
                { targets: 6, "width": "15%", "data": "name", name: "name" },
                { targets: 7, "width": "15%", "data": "description", name: "description" },
                { targets: 8, "width": "10%", orderable: false,
                    render: function (data, type, full, meta) 
                    {
                        return '' +
                            '<a href="javascript:void(0)" data-target="problem" data-problem-id="' + full.id + '" class="mr-5 rightSidePanelOpener2"><img src="assets/images/svg/team-icon.svg" alt=""></a>' +
                            '<a href="javascript:void(0)" data-target="problem" data-problem-id="' + full.id + '" class="mr-5 rightSidePanelOpener"><img src="assets/images/svg/edit-icon.svg" alt=""></a>' +
                            '<a href="javascript:void(0)" onclick="showDeleteModal(' + full.id + ',\'problem\')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>';
                    }
                }
            ],
            "order": [],
            createdRow: function( row, data, dataIndex ) {
                $( row )
                    .attr('data-id', data.id)
                    // .addClass('tableSortHolder')
                    .addClass('problem-row');
            },
            drawCallback: function( settings ) {
                var api = this.api();
//                if ($(api.table().node()).hasClass("d-none"))
//                {
//                    var dt_child_response = api.ajax.json();
//                    $("span.status_counter_" + problem_id).each(function() {
//                        var status = $(this).attr('data-status')
//                        if (dt_child_response.statusCounters[status])
//                        {
//                            $(this).html(dt_child_response.statusCounters[status]);
//                        } else {
//                            $(this).html(0);
//                        }
//                    });
//                    $(api.table().node()).removeClass("d-none");
//                }
//
//                addRightSidePanelListeners();
                // Output the data for the visible rows to the browser's console
                    console.log( "drawCallback fired", settings);
                $(api.table().node()).removeClass("d-none");
                if (getQueryStringParameter("problem_id"))
                {
                    $(".problem-row[data-id='" + getQueryStringParameter("problem_id") + "'] span.expand-td").trigger('click');
                }

                addRightSidePanelListeners();

            },
            "stripeClasses":[]
        } );


        var detailRows = [];
        
        $('#data_table tbody').on( 'click', 'tr.problem-row span.expand-td', function (e) {
            e.stopPropagation();
            var tr = $(this).closest('tr');
//            console.log(tr);
            var row = dt.row( tr );
            var data = row.data();
            var row_id = tr.data('id');
            var idx = $.inArray( row_id, detailRows );

            if ( row.child.isShown() ) {
                row.child.hide();

                // Remove from the 'open' array
                detailRows.splice( idx, 1 );
            }
            else {
                if(row.child() && row.child().length)
                {
                    row.child.show();
                } else {
                    var out_tr =                             
                            '<tr class="expanded" id="problem_details_row_' + row_id + '">' +
                                '<td colspan="9" class="first-row">' +
                                    '<div class="content-holder p-15 mt-10 mb-10">' +
                                        '<div class="row mb-30">' +
                                            '<div class="col-md-6">' +
                                                '<div class="row">' +
                                                    '<div class="col-12 mb-30">' +
                                                        '<h4 class="boldFont xlarge-font mb-10">Notes</h4>' +
                                                        '<p>' + data.notes + '</p>' +
                                                    '</div>' +
                                                '</div>' +
                                                '<div class="row">' +
                                                    '<div class="col-12">';
                                                    if (data.attachments) {
                                                        out_tr += 
                                                        '<div class="clr mb-15">' +
                                                            '<h5 class="pb-5 boldFont">Problem Attachments</h5>' +
                                                            '' +
                                                         '</div>';
                                                        for (var i in data.attachments) 
                                                        {
                                                            out_tr += 
                                                            '<div class="clr tag dInlineBlock formField md whtbg mr-5 mb-15">' +
                                                                '<span>' +
                                                                    '<a title="Uploaded by ' + data.attachments[i].uploaded_by_name + ' on ' + data.attachments[i].file_date + '" href="javascript:void(0)" onclick="openAttachment();event.preventDefault" data-file-name="' + data.attachments[i].file_name +'" data-link="' + data.attachments[i].name_on_disk + '">' + data.attachments[i].file_name + '<img class="icon" src="assets/images/svg/attachment-icon.svg" alt=""></a>' +
                                                                    '</span>' +
                                                            '</div>';
                                                        }
                                                    }
                                                    out_tr +=
                                                    '</div>' +
                                                '</div>' +
                                            '</div>' +
                                            '<div class="col-md-6">' +
                                                '<div class="row mb-30">' +
                                                    '<div class="col-4">' +
                                                        '<label class="font-weight-bold">' +
                                                            'Problem ID' +
                                                        '</label>' +
                                                        '<div>' + data.id + '</div>' +
                                                    '</div>' +
                                                    '<div class="col-4">' +
                                                        '<label class="font-weight-bold">' +
                                                            'Assigned To' +
                                                        '</label>' +
                                                        '<div><span class="user-img dInlineBlock mr-5 user-avatar"><img src="assets/images/users/avatar-2.jpg" alt=""></span>' + data.assigned_to_first + ' ' + data.assigned_to_last + '</div>' +
                                                    '</div>' +
                                                    '<div class="col-4">' +
                                                        '<label class="font-weight-bold">' +
                                                            'Assigned To Group' +
                                                        '</label>' +
                                                        '<div>' + data.assigned_group_name + '</div>' +
                                                    '</div>' +
                                                '</div>' +
                                                '<div class="row">' +
                                                    '<div class="col-4">' +
                                                        '<label class="font-weight-bold">' +
                                                            'Created By' +
                                                        '</label>' +
                                                        '<div><span class="user-img dInlineBlock mr-5 user-avatar"><img src="assets/images/users/avatar-2.jpg" alt=""></span>' + data.created_by_first + ' ' + data.created_by_last + '</div>' +
                                                    '</div>' +
                                                    '<div class="col-4">' +
                                                        '<label class="font-weight-bold">' +
                                                            'Knowledge Article' +
                                                        '</label>' +
                                                        '<div><a href="javascript:void()" onclick="open_article(' + data.knowledge_article_id + ')">' + data.knowledge_article_name + '</a></div>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</div>' +
                                        '</div>' +
                                        '<div class="mb-30 separator-new darkLighterBg"></div>' +
                                        '<div class="clr mb-30">' +
                                            '<div class="float-right text-right">' +
                                                '<ul>' +
                                                <%
                                                for(int a = 0; a < status_select.size(); a++)
                                                {
                                                    String select_option[] = status_select.get(a);
                                                    %>
                                                    '<li class="dInlineBlock valign-m pl-15">' +
                                                        '<button class="btn btn-wht md customBtn dInlineBlock" onclick="filter_incidents(this)">' +
                                                            '<strong><%=select_option[4]%>: <span class="status_counter_' + row_id + '" data-active="0" data-problem-id="' + row_id + '" data-status="<%=select_option[3]%>"></span></strong>' +
                                                        '</button>' +
                                                    '</li>' +
                                                    <%
                                                }
                                                %>
                                                '</ul>' +
                                            '</div>' +
                                            '<h5 class="pb-5 boldFont"><span class="icon mr-5"><i class="la la-lg la-tools"></i></span>Incidents</h5>' +
                                            '<!-- <p>Our improved business 2118 Thornridge Cir. Syracuse, Connecticut 35624</p> -->' +
                                        '</div>' +
                                        '<table id="data_table_incidents_' + row_id + '" class="table table-striped custom-sort-table d-none" cellspacing="0" width="100%">' +
                                            '<thead>' +
                                                '<tr>' +
                                                    '<th></th>' +
                                                    '<th>Id</th>' +
                                                    '<th>Priority</th>' +
                                                    '<th>State</th>' +
                                                    '<th>Incident Time</th>' +
                                                    '<th>Description</th>' +
                                                    '<th>Caller</th>' +
                                                    '<th>Assigned To</th>' +
                                                    '<th>Category</th>' +
                                                '</tr>' +
                                            '</thead>' +
                                        '</table>' +
                                    '</div>' +
                                '</td>' +
                            '</tr>';

                    row.child( 
                        $( out_tr )                    
                    ).show();
                    showIncidents(row_id);
                    // editProblem(row_id);
                }
                // Add to the 'open' array
                if ( idx === -1 ) {
                    detailRows.push( row_id );
                }
            }
        } );

        apply_filters(false);
        
//        dt.on("preXhr.dt", function (e, settings, data) {
//            $("#data_table").addClass("d-none");
//        });
        
        attachFieldListeners($("#filter_form"));

        <% if (action.equals("new"))
        {
        %>    
            document.getElementById("new_problem_button").dispatchEvent(new Event('click'));
        <%
        }
        %>

        <% if (!problem_id.equals(""))
        {
        %>    
//            setTimeout(function()
//            { 
//                console.log(".problem-row[data-id='<%=problem_id%>'] span.expand-td")
//                $(".problem-row[data-id='<%=problem_id%>'] span.expand-td").trigger('click');                
//            }, 500);
        <%
        }
        %>
    });

    function filter_incidents(btn_clicked)
    {
        var span = $(btn_clicked).find('span[class^="status_counter"]');
        var problem_id = span.data('problemId');
        var status = span.data('status');
        if (problem_id && status)
        {
            if (span.data('active') == "1")
            {
                span.data('active', "0");
                $(btn_clicked).removeClass("btn-primary-new");
                $(btn_clicked).addClass("btn-wht");
            } else {
                span.data('active', "1");
                $(btn_clicked).removeClass("btn-wht");
                $(btn_clicked).addClass("btn-primary-new");
            }
        }
        dt_childs[problem_id].draw();
        
    }
    
    function showIncidents(problem_id)
    {
        dt_childs[problem_id] = 
            $('#data_table_incidents_' + problem_id).DataTable( {
                autoWidth: false,
                "processing": true,
                "serverSide": true,
                "ordering": false,
                'serverMethod': 'post',
                'searching': false,
                "lengthChange": false,
                dom: "<'row'<'col-sm-12'tr>>",
                "ajax": {
                    "url": "ajax_lookup_problem_incidents",
                    'data': function(data){
                        data['problem_id'] = problem_id;
                        var status_filters = [];
                        $("#problem_details_row_" + problem_id).find('span[class^="status_counter"]').each( function()
                            {
                                if ($(this).data('active') == "1")
                                {
                                    status_filters.push($(this).data('status'));
                                }
                            }
                        );
                        if (status_filters.length > 0)
                        {
                            data['status_filters'] = status_filters;
                        }
                    }
                },
                "pageLength": 50,
                "columnDefs": [
                    { targets: 0, defaultContent: '<span class="expand-td"><img src="assets/images/svg/collapse-minus-icon.svg" alt=""></span>', orderable: false },
                    { targets: 1, "data": "id", name: "id" },
                    { targets: 2, "data": null, name: "priority",  
                        render: function ( data, type, row ) 
                        {
                            var indicator_class = "";
                            switch (row.priority) {
                                case "Low":
                                    indicator_class = "low";
                                    break;
                                case "Medium":
                                    indicator_class = "medium";
                                    break;
                                case "High":
                                    indicator_class = "high";
                                    break;
                                case "Critical":
                                    indicator_class = "critical";
                                    break;
                                default:
                                    indicator_class = "unknown";
                                    break;
                            }
                            return '<span class="indicator ' + indicator_class + ' "></span> ' + (row.priority ? row.priority : 'Unknown');
                        }
                    },
                    { targets: 3, "data": null, name: "state", 
                        render: function ( data, type, row ) 
                        {
                            return '<span class="btn sm state-' + row.state.toLowerCase() + ' customBtn">' + row.state + '</span>';
                        }
                    },
                    { targets: 4, "data": "incident_time", name: "incident_time", render: $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss','MM/DD/YYYYY hh:mm A') },
                    { targets: 5, "data": "description", name: "description", orderable: false  },
                    { targets: 6, "data": null, name: "caller_id", 
                        render: function ( data, type, row ) 
                        {
                            return row.caller_first + ' ' + row.caller_last;
                        }
                    },
                    { targets: 7, "data": null, name: "assigned_to_id", 
                        render: function ( data, type, row ) 
                        {
                            return row.assigned_to_first + ' ' + row.assigned_to_last;
                        }
                    },
                    { targets: 8, "data": "category", name: "category",  orderable: false},
    //                { targets: 9, 
    //                    render: function (data, type, full, meta) 
    //                    {
    //                        return '' +
    //                            '<a href="javascript:void(0)" onclick="expandRow(' + full.id + ');" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>' +
    //                            '<a href="javascript:void(0)" onclick="showDeleteModal(' + full.id + ',\'incident\')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>';
    //                    }
    //                }
                
                ],
                "order": [],
                createdRow: function( row, data, dataIndex ) {
                    $( row )
                        .attr('data-id', data.id)
                        // .addClass('tableSortHolder')
                        .addClass('incident-row');
                },
                drawCallback: function( settings ) {
                    var api = this.api();
                    if ($(api.table().node()).hasClass("d-none"))
                    {
                        var dt_child_response = api.ajax.json();
                        $("span.status_counter_" + problem_id).each(function() {
                            var status = $(this).attr('data-status')
                            if (dt_child_response.statusCounters[status])
                            {
                                $(this).html(dt_child_response.statusCounters[status]);
                            } else {
                                $(this).html(0);
                            }
                        });
                        $(api.table().node()).removeClass("d-none");
                    }
                    if (getQueryStringParameter("problem_id") && getQueryStringParameter("incident_id"))
                    {
                        $("#data_table_incidents_" + getQueryStringParameter("problem_id") + " .incident-row[data-id='" + getQueryStringParameter("incident_id") + "'] span.expand-td").trigger('click');                
                    }
                    addRightSidePanelListeners();
                    // Output the data for the visible rows to the browser's console
//                    console.log( api.rows( {page:'current'} ).data() );
                    
                },
                "stripeClasses":[]
            } );

//        dt_childs[problem_id].on( 'draw', function () {
//            console.log(dt_childs[problem_id]);
//            $(dt_childs[problem_id].table().node()).removeClass("d-none");
//            console.log(this.api());
//        } );

        $('#data_table_incidents_' + problem_id + ' tbody').on( 'click', 'tr.incident-row span.expand-td', function () {
            var tr = $(this).closest('tr');

            var row = dt_childs[problem_id].row( tr );
            var data = row.data();
            var row_id = tr.data('id');
            var idx = $.inArray( row_id, incidentDetailRows );
            
            if ( row.child.isShown() ) {
                row.child.hide();

                // Remove from the 'open' array
                incidentDetailRows.splice( idx, 1 );
            }
            else {
                if(row.child() && row.child().length)
                {
                    row.child.show();
                } else {
                    var out_tr =  
                        '<tr class="expanded" id="problem_details_row_' + row_id + '">' +
                            '<td colspan="10" class="first-row">' +
                                '<div class="content-holder p-15 mt-10 mb-10">' +
                                    '<div class="row mb-30">' +
                                        '<div class="col-md-6">' +
                                            '<h4 class="boldFont xlarge-font mb-10">' + data.description+ '</h4>' +
                                            '<p>' + data.notes + '</p>' +
                                        '</div>' +
                                        '<div class="col-md-6">';
                                            for (var i in data.attachments) 
                                            {
                                                out_tr += 
                                                '<div class="clr tag dInlineBlock formField md whtbg mr-5 mb-15">' +
                                                    '<span>' +
                                                        '<a title="Uploaded by ' + data.attachments[i].uploaded_by_name + ' on ' + data.attachments[i].file_date + '" href="javascript:void(0)" onclick="openAttachment();event.preventDefault" data-file-name="' + data.attachments[i].file_name +'" data-link="' + data.attachments[i].name_on_disk + '">' + data.attachments[i].file_name + '<img class="icon" src="assets/images/svg/attachment-icon.svg" alt=""></a>' +
                                                    '</span>' +
                                                '</div>';
                                            }
                                            out_tr +=                                            
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</td>' +
                        '</tr>';
                    row.child($(out_tr)                    
                    ).show();
                    // editProblem(row_id);
                }
                // Add to the 'open' array
                if ( idx === -1 ) {
                    incidentDetailRows.push( row_id );
                }
            }
        } );

        dt_childs[problem_id].on( 'draw', function () {
            $.each( incidentDetailRows, function ( i, id ) {
                $('#'+id+' span.expand-td').trigger( 'click' );
            } );
        } );

    }
    
    const axiosInstance = axios.create();
    axiosInstance.interceptors.request.use(function (config) {
        // Do something before request is sent
        if (config.url != 'ajax_get_jwt')
        {
            config.responseType = 'arraybuffer'
        }
        return config;
    }, function (error) {
        // Do something with request error
        return Promise.reject(error);
    });
    
    axiosInstance.interceptors.response.use((response) => {
        return response
    },  function (error) {
            const originalRequest = error.config;
            if (error.response)
            {
                if (error.response.status === 401 && originalRequest.url.indexOf('ajax_get_jwt') !== -1 ) {
                    router.push('/login');
                    return Promise.reject(error);
                }

                if (error.response.status === 401 && !originalRequest._retry) {
                    originalRequest._retry = true;
                    return axiosInstance.get('ajax_get_jwt')
                        .then(res => {
                            if (res.status === 200) {
                                axiosInstance.defaults.headers.common['Authorization'] = 'Bearer ' + res.data.token;
                                return axiosInstance(originalRequest);
                            }
                        })
                }
            } else {
                console.log("axiosInstance error", error);
            }
            return Promise.reject(error);
    });
    
    function openAttachment(url, file_name)
    {
        let file = ''
        if (typeof url == 'undefined' || typeof file_name == 'undefined')
        {
            let name_on_disk = event.target.getAttribute('data-link');
            file_name = event.target.getAttribute('data-file-name');
            file = '<%=props.get("couchdb.attachments_host")%>/<%=props.get("couchdb.name")%>/' + name_on_disk;
        } else {
            file = url
        }        
        if (file != '')
        {            
            axiosInstance.get(file).then((response) => {
                console.log(response)

                let blob = new Blob([response.data], { type: response.headers['content-type'] })
                let link = document.createElement('a')
                link.href = window.URL.createObjectURL(blob)
                link.download = file_name
                link.click()
                link.remove(); 
                window.URL.revokeObjectURL(link);

            });
        }
    }
        
    function saveProblem()
    {
        var form = $(event.target).closest("form").get(0);
        $.ajax({
            url: form.action,
            type: "POST",
            data: new FormData(form),
            processData: false,
            contentType: false,
            success: function(){
                dt.draw();
            },
            error: function(jqXHR, textStatus)
            {
                switch (jqXHR.status) {
                    case 401:
                        window.location.assign("<%=props.get("login_url").toString()%>");
                        break;
                    case 403:
                        alert("No permission!");
                        break;
                    default:
                        alert("Error processing request! " + textStatus);
                }
            }
        });
    }

    function saveProblemIncident()
    {
        var form = $(event.target).closest("form").get(0);
        var problem_id = form.elements['problem_id'].value;
        $.ajax({
            url: form.action,
            type: "POST",
            data: new FormData(form),
            processData: false,
            contentType: false,
            success: function(){
                dt_childs[problem_id].draw();
            },
            error: function(jqXHR, textStatus)
            {
                switch (jqXHR.status) {
                    case 401:
                        window.location.assign("<%=props.get("login_url").toString()%>");
                        break;
                    case 403:
                        alert("No permission!");
                        break;
                    default:
                        alert("Error processing request! " + textStatus);
                }
            }
        });
    }

    function unlinkIncident() {
        var tr = $(event.target).closest("tr");
        var incident_ids_input = $("input[name='incident_ids']")
        var incident_ids = JSON.parse(incident_ids_input.val())
        incident_ids = incident_ids.filter(e => e !== "" + tr.data('id'));
        incident_ids_input.val(JSON.stringify(incident_ids));
        dt_rightPanel_incidents.row(tr).remove().draw();
        //        dt_rightPanel_incidents.row('#'+id).remove().draw();
    }

    function attachFieldListeners(container)
    {
        tinymce.remove('textarea');
        tinymce.init({
            selector: '.ckeditor',
            setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                });
            },
            height: 250,
            plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });

        var rightPanel_incidents_table = container.find('table.rightPanelDataTable');
        if (!$.fn.DataTable.isDataTable( rightPanel_incidents_table ))
        {
            dt_rightPanel_incidents = rightPanel_incidents_table.DataTable();
        }

        bsCustomFileInput.init();

            container.find('input[name="create_date"]').daterangepicker({
                "startDate": "<%=now_picker_time%>",
                "autoUpdateInput": true,
                "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
        
        container.find('input[name="incident_lookup"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incidents",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response(
                            $.map(data.data, function (item) {
//                                console.log(item);
                                item.value = item.id;
                                item.label = item.id + " " + item.incident_time + " " + item.description;
                                return item;
                            })
                        )
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                console.log(ui.item);
                var incident_ids_input = $("input[name='incident_ids']")
                var incident_ids = JSON.parse(incident_ids_input.val())
                if (!incident_ids.includes(ui.item.id)) {
                    dt_rightPanel_incidents.row.add({
                        "id": ui.item.id,
                        "priority" : ui.item.priority,
                        "state" : ui.item.state,
                        "incident_time" : ui.item.incident_time,
                        "description" : ui.item.description,
                        "caller_username" : ui.item.caller_username,
                        "assigned_to_username" : ui.item.assigned_to_username,
                        "actions": '<a href="javascript:void(0)" onclick="unlinkIncident(\'' + ui.item.id + '\')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>'
                    }).node().dataset.id = ui.item.id;
                    dt_rightPanel_incidents.draw();
                    incident_ids.push(ui.item.id);
                    incident_ids_input.val(JSON.stringify(incident_ids));
                }
                $(this).val('');
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });
        container.find('input[name="assigned_to_name"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="assigned_to_name"]').val(ui.item.label); // display the selected text
                container.find('input[name="assigned_to_id"]').val(ui.item.value); // save selected id to input
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });
        //caller_group_name
        container.find('input[name="assigned_group_name"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_group",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="assigned_group_name"]').val(ui.item.label); // display the selected text
                container.find('input[name="assigned_group_id"]').val(ui.item.value); // save selected id to input
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });
        
        container.find('input[name="knowledge_search_terms"]').autocomplete({

            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incident_knowledge_article_text",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function( event, ui ) 
            { 
                container.find('input[name="knowledge_search_terms"]').val(ui.item.article_description);
                //window.location.href = ui.item.value;
//                window.open("kb_view.jsp?id=" + ui.item.value, "Knowledge Base Article", "width=600,height=600,top=400,left=400");
                container.find('input[name="knowledge_search_terms"]').val(ui.item.label); // display the selected text
                container.find('input[name="knowledge_article_id"]').val(ui.item.value); // save selected id to input
                
                return false;
            }
        });

        container.find('input[name="create_by_name"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user_last_first",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="create_by_name"]').val(ui.item.label); // set name
                container.find('input[name="create_by_id"]').val(ui.item.value); // set id
                return false;
            }
        });
        container.find('select[name="status"]').on( "change", function() {
            if ( $( this ).val() == "Closed") {
                $("#close_linked_incidents_row").removeClass("d-none")
            } else {
                $("#close_linked_incidents_row").addClass("d-none")
            }
        });

    }    
    function open_article(id) {
        window.open("kb_view.jsp?id=" + id, "Knowledge Base Article", "width=600,height=600,top=400,left=400");
    }

    function update_subcategory()
    {
        var var_cat_id = document.getElementById("category_id").value;
        var url = "ajax_lookup_subcategory?category_id=" + var_cat_id;
        var select = document.getElementById('subcategory');
        document.getElementById('subcategory').options.length = 0;
        $.get('ajax_lookup_subcategory?category_id=' + var_cat_id, function(data) 
        {
            for(var i = 0; i < data.length; i++) 
            {
                var obj = data[i];
                var name = obj.name;  
                var opt = document.createElement('option');
                    opt.value = name;
                    opt.innerHTML = name;
                    select.appendChild(opt);
            }            
        });
    }


</script>
    
<script>
function clear_caller() 
{ 
    document.getElementById("caller_id").value = "";
    document.getElementById("caller_name").value = "";
}
function clear_assigned_to() 
{ 
    document.getElementById("assigned_to_id").value = "";
    document.getElementById("assigned_to_name").value = "";
}
function clear_create_by() 
{ 
    document.getElementById("create_by_id").value = "";
    document.getElementById("create_by_name").value = "";
}
function clear_caller_group() 
{ 
    document.getElementById("caller_group_id").value = "";
    document.getElementById("caller_group_name").value = "";
}
function clear_assigned_group() 
{ 
    document.getElementById("assigned_group_id").value = "";
    document.getElementById("assigned_group_name").value = "";
}
</script>
<script>
    function hide_show_div() 
    {
        var x = document.getElementById("selectors");
        if (x.style.display === "none") 
        {
            x.style.display = "block";
        } 
        else 
        {
            x.style.display = "none";
        }
    }        

</script>
<script src="assets/js/chat_app.js"></script>

    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%      con.close();
        }//end if not permission
    }//end if not logged in
%>
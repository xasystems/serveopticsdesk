<!--Copyright 2021 Real Data Technologies, Inc. , All rights reserved.-->
<%-- 
    Document   : request.jsp
    Created on : 18-Jan-2020, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Calendar"%> 
<%@ page import="java.util.GregorianCalendar"%>
<%@ page import="org.apache.commons.text.StringEscapeUtils"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        if (!REQUEST.equalsIgnoreCase("none") || MANAGER.equalsIgnoreCase("true") || ADMINISTRATION.equalsIgnoreCase("true") ) 
        {
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            SimpleDateFormat display_format2 = new SimpleDateFormat("HH:mm:ss / dd-MMM-yy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
            java.util.Date now = new java.util.Date();
            String now_display_format = display_format.format(now);
            String now_timestamp_format = timestamp_format.format(now);
            String now_incident_time = date_time_picker_format.format(now);
            Connection con = null;
            String request_info[] = new String[0];
            String selected = "";
            boolean request_not_found = false;
            String user_id = session.getAttribute("user_id").toString();
            
            try
            {
                con = db.db_util.get_contract_connection(context_dir, session);                
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
            }
            catch(Exception e)
            {
                System.out.println("request.jsp exception=" + e);
            }
            String id = request.getParameter("id");
            try
            {
                id = request.getParameter("id");
                request_info = db.get_requests.request_by_id(con, id);
                if(request_info[0].equalsIgnoreCase(""))
                {
                    request_not_found = true;
                }
            }
            catch (Exception e)
            {
                request_not_found = true;
                id = "0";
            }
            //set can edit
            boolean can_edit = false;
            
            if(REQUEST.equalsIgnoreCase("update") || MANAGER.equalsIgnoreCase("true") || ADMINISTRATION.equalsIgnoreCase("true") ) 
            {
                can_edit = true;
            }
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <!-- Page specific CSS-->
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <!--End Page specific CSS-->
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END Page Level CSS-->
    
    
    <div class="app-content content">
        <form action="request_update" method="post">
            <!--future fields-->            
            <input type="hidden" name="impact" id="impact" value=""/>
            <input type="hidden" name="urgency" id="urgency" value=""/>
            <input type="hidden" name="id" id="id" value="<%=id%>"/>
            <input type="hidden" name="catalog_item_id" id="catalog_item_id" value="<%=request_info[5]%>"/>
            
            
            <div class="content-wrapper">
                <!-- start content here-->
                <div class="content-header row">
                    <div class="content-header-left col-md-6 col-12 mb-2">
                        <h3 class="content-header-title"><i class="la la-shopping-cart"></i>&nbsp;Request</h3>
                        <div class="row breadcrumbs-top">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a></li>
                                    <li class="breadcrumb-item active">Request</li>
                                </ol>                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                    <%
                                    String rev_time = "";
                                    try
                                    {
                                        java.util.Date rev_date = timestamp_format.parse(request_info[2]);
                                        rev_time = display_format2.format(rev_date);
                                    }
                                    catch(Exception e)
                                    {
                                        rev_time = "";
                                    }
                                    %>
                                    Request <em>REQ<%=request_info[0]%></em>&nbsp;&nbsp;<small><em>(Revision.<%=request_info[1]%> Date:<%=rev_time%>)</em></small>
                                    <%
                                    if(request_not_found)
                                    {
                                        %>
                                        &nbsp;&nbsp;<font color="red">REQUEST NOT FOUND</font>
                                        <%
                                    }
                                    %>
                                </h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>                                    
                                </div>
                            </div> 
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Request ID</label>
                                            <input name="id" id="id" type="text" readonly="true" value="<%=request_info[0]%>" class="form-control" disabled/>
                                        </div>
                                        
                                        <div class="col-xl-10 col-lg-6 col-12">
                                            <label><br>Description</label>
                                            <input readonly="true" type="text" name="description" id="description" value="<%=request_info[29]%>" class="form-control"/> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Request Date</label>
                                            <div class='input-group'>
                                                <%
                                                String request_date_value = "";
                                                try
                                                {
                                                    GregorianCalendar request_date_calendar = new GregorianCalendar();
                                                    request_date_calendar.setTime(timestamp_format.parse(request_info[7]));
                                                    if(request_info[7] == null || request_info[7].equalsIgnoreCase("null") || request_info[7].equalsIgnoreCase(""))
                                                    {
                                                        request_date_value = "";
                                                    }
                                                    else
                                                    {
                                                        request_date_value = date_time_picker_format.format(request_date_calendar.getTime());
                                                    }
                                                }
                                                catch(Exception e)
                                                {
                                                    System.out.println("Exception on request.jsp getting request_date:=" + e);
                                                }
                                                //String incident_time = date_time_picker_format.format(timestamp_format.parse(incident_info[2]).getTime());
                                                %>
                                                <input readonly="true" type="text" id="request_date" name="request_date" value="<%=request_date_value%>" class="form-control datetime" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <span class="la la-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Due Date <i class="la la-question" title="This field is calculated by the system based on 'Priority','Request Date' and the Delivery values set in the Catalog Item definition. Due Date will be updated after clicking on 'Update' button" style="font-size: 14px;"></i></label>
                                            <div class='input-group'>
                                                <%
                                                String due_date_value = "";
                                                try
                                                {
                                                    GregorianCalendar due_date_calendar = new GregorianCalendar();
                                                    due_date_calendar.setTime(timestamp_format.parse(request_info[8]));
                                                    if(request_info[8] == null || request_info[8].equalsIgnoreCase("null") || request_info[8].equalsIgnoreCase(""))
                                                    {
                                                        due_date_value = "";
                                                    }
                                                    else
                                                    {
                                                        due_date_value = date_time_picker_format.format(due_date_calendar.getTime());
                                                    }
                                                    
                                                }
                                                catch(Exception e)
                                                {
                                                    System.out.println("Exception on request.jsp getting due_date_value:=" + e);
                                                }
                                                //String incident_time = date_time_picker_format.format(timestamp_format.parse(incident_info[2]).getTime());
                                                %>
                                                <input readonly="true" type='text' id="due_date" name="due_date" value="<%=due_date_value%>" class="form-control datetime" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <span class="la la-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Request Create By&nbsp;<i class="la la-search" title="This is a lookup field. To start the lookup enter a least two characters of the users 'User Name' then click on one of the available options that popup. Manually entered values will be ignored. If the user is not available in the list, then add the user via the 'Administration' 'Users' function." style="font-size: 14px;"></i></label>
                                            <input type="hidden" name="create_by_id" id="create_by_id" value="<%=request_info[13]%>"/>
                                            <input readonly="true" type="text" name="create_by_username" id="create_by_username" value="<%=request_info[46]%>" class="form-control" placeholder="Enter the creaters username"/>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Contact Method</label>
                                            <%
                                            ArrayList<String[]> contact_method_select = db.get_system_select_field.active_for_table_column(con, "request", "contact_method");
                                            String contact_method = "";
                                            for(int a = 0; a < contact_method_select.size(); a++)
                                            {
                                                String select_option[] = contact_method_select.get(a);
                                                //select_option[3] = value    select_option[4] = label
                                                if(request_info[24].equalsIgnoreCase(select_option[3]))
                                                {
                                                    contact_method = select_option[4];
                                                }
                                            }
                                            %>
                                            <input readonly="true" type="text" name="contact_method" id="contact_method" value="<%=contact_method%>" class="form-control"/>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>State</label>
                                            <%
                                            ArrayList<String[]> state_select = db.get_system_select_field.active_for_table_column(con, "request", "state");
                                            String state = "";
                                            for(int a = 0; a < state_select.size(); a++)
                                            {
                                                String select_option[] = state_select.get(a);
                                                //select_option[3] = value    select_option[4] = label
                                                if(request_info[23].equalsIgnoreCase(select_option[3]))
                                                {
                                                    state = select_option[4];
                                                }
                                            }
                                            %>
                                            <input readonly="true" type="text" name="state" id="state" value="<%=state%>" class="form-control"/>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Approval</label>
                                            <%
                                            ArrayList<String[]> approval_select = db.get_system_select_field.active_for_table_column(con, "request", "approval");
                                            String approval = ""; 
                                            for(int a = 0; a < approval_select.size(); a++)
                                            {
                                                String select_option[] = approval_select.get(a);
                                                //select_option[3] = value    select_option[4] = label
                                                if(request_info[25].equalsIgnoreCase(select_option[3]))
                                                {
                                                    approval = select_option[4];
                                                }
                                                else
                                                {
                                                    selected = "";
                                                }
                                            }
                                            %>
                                            <input readonly="true" type="text" name="approval" id="approval" value="<%=approval%>" class="form-control"/>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Priority</label>
                                            <%
                                            ArrayList<String[]> priority_select = db.get_system_select_field.active_for_table_column(con, "request", "priority");
                                            String priority = "";
                                            for(int a = 0; a < priority_select.size(); a++)
                                            {
                                                String select_option[] = priority_select.get(a);
                                                //select_option[3] = value    select_option[4] = label
                                                if(request_info[22].equalsIgnoreCase(select_option[3]))
                                                {
                                                    priority = select_option[4];
                                                }
                                            }
                                            %>
                                            <input readonly="true" type="text" name="priority" id="priority" value="<%=priority%>" class="form-control"/>
                                        </div>                                           
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Requester&nbsp;<i class="la la-search" title="This is a lookup field. To start the lookup enter a least two characters of the users 'User Name' then click on one of the available options that popup. Manually entered values will be ignored. If the user is not available in the list, then add the user via the 'Administration' 'Users' function." style="font-size: 14px;"></i></label>
                                            <input type="hidden" name="requested_for_id" id="requested_for_id" value="<%=request_info[14]%>"/>
                                            <input readonly="true" type="text" name="requester_username" id="requester_username" value="<%=request_info[51]%>" class="form-control" placeholder="Enter the requesters username"/>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Requester VIP?</label>
                                            <br>
                                            <%
                                            if(request_info[55].equalsIgnoreCase("true"))
                                            {
                                                %>
                                                <input name="vip" id="vip" CHECKED type="checkbox" disabled=""/>&nbsp;<em>Set by Requester's profile</em>
                                                <%
                                            }
                                            else
                                            {
                                                %>
                                                <input name="vip" id="vip" type="checkbox" disabled=""/>&nbsp;<em>Set by Requester's profile</em>
                                                <%
                                            }
                                            %>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Requesters Group&nbsp;<i class="la la-search" title="This is a lookup field. Manually enter values will be ignored, use the lookup feature to select a valid option. Enter at least two characters of the Group and click on one of the popup options. If the Group is not available, use the 'Administration' 'Groups' function to add the Group." style="font-size: 14px;"></i></label>
                                            <input type="hidden" name="requested_for_group_id" id="requested_for_group_id" value="<%=request_info[15]%>"/>
                                            <input readonly="true" type="text" name="requester_group_name" id="requester_group_name" value="<%=request_info[56]%>" class="form-control" placeholder="Enter the requesters Group"/>
                                        </div>
                                        
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Assigned to Group&nbsp;<i class="la la-search" title="This is a lookup field. Manually enter values will be ignored, use the lookup feature to select a valid option. Enter at least two characters of the Group and click on one of the popup options. If the Group is not available, use the 'Administration' 'Groups' function to add the Group." style="font-size: 14px;"></i></label>
                                            <input type="hidden" name="assigned_group_id" id="assigned_group_id" value="<%=request_info[11]%>"/>
                                            <input readonly="true" type="text" name="assigned_group_name" id="assigned_group_name" value="<%=request_info[40]%>" class="form-control" />
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label>
                                                <br>
                                                Assigned to&nbsp;
                                                <i class="la la-search" style="font-size: 14px;"></i>
                                                <i class="la la-question-circle" style="font-size: 14px;" title="This field is filtered by the Assigned to Group field. If Group Unassigned is selected, then all users will be available. If a specific group is selected, then only the members of that group will be available. If the Assigned to Group is changed this field will be cleared."></i>
                                            </label>
                                            <input type="hidden" name="assigned_to_id" id="assigned_to_id" value="<%=request_info[12]%>"/>
                                            <input readonly="true" type="text" name="assigned_to_name" id="assigned_to_name" value="<%=request_info[41]%>" class="form-control" placeholder="Enter the Assigned To group's name"/>
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Location&nbsp;<i class="la la-search" title="This is a lookup field. Type in at least two characters for popup options. It should be populated when the 'Requested for' user is selected. If not, then it can be set manually. Any text value will be accepted."  style="font-size: 14px;"></i></label>                                            
                                            <input readonly="true" type="text" id="location" name="location" value="<%=request_info[16]%>" class="form-control">
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Department&nbsp;<i class="la la-search" title="This is a lookup field. Type in at least two characters for popup options. It should be populated when the 'Requested for' user is selected. If not, then it can be set manually. Any text value will be accepted." style="font-size: 14px;"></i></label>
                                            <input readonly="true" type="text" id="department" name="department" value="<%=request_info[17]%>" class="form-control">
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Site&nbsp;<i class="la la-search" title="This is a lookup field. Type in at least two characters for popup options. It should be populated when the 'Requested for' user is selected. If not, then it can be set manually. Any text value will be accepted." style="font-size: 14px;"></i></label>
                                            <input readonly="true" type="text" id="site" name="site" value="<%=request_info[18]%>" class="form-control">
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Company&nbsp;<i class="la la-search" title="This is a lookup field. Type in at least two characters for popup options. It should be populated when the 'Requested for' user is selected. If not, then it can be set manually. Any text value will be accepted." style="font-size: 14px;"></i></label>
                                            <input readonly="true" type="text" id="company" name="company" value="<%=request_info[19]%>" class="form-control">
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            &nbsp;
                                        </div>
                                    </div>
                                    <div class="row match-height" >	
                                        <div class="col-6 " style="border-style: solid; border-width:thin; border-color: #f7f7f7;height: 180px; overflow-y: scroll;" >                    
                                            <div class="bs-callout-danger callout-border-left callout-transparent mt-1 p-1">
                                                <h4 class="danger">Required Information</h4>
                                                <input type="hidden" name="required_info" id="required_info" value="<%=request_info[31]%>"/> 
                                                <%
                                                String unEscaped_required_user_input_text = StringEscapeUtils.unescapeHtml4(request_info[31]);
                                                out.print(unEscaped_required_user_input_text);
                                                %>
                                                <br>&nbsp;
                                            </div>
                                        </div>
                                        <div class="col-6 " style="border-style: solid; border-width:thin; border-color: #f7f7f7;height: 180px; overflow-y: scroll;" >                    
                                            <div class="bs-callout-danger callout-border-left callout-transparent mt-1 p-1">
                                                <h4 class="danger">Required Information Response <i class="la la-question-circle" title="Only Users with 'Administrator' or 'Manager' Role can edit this field." style="font-size: 14px;"> </i> </h4>
                                                <%
                                                if(MANAGER.equalsIgnoreCase("true") || ADMINISTRATION.equalsIgnoreCase("true"))
                                                {
                                                    %>
                                                    <textarea readonly="true" rows="10" name="required_info_response" id="required_info_response" class="form-control" placeholder="If required, Enter Information Here"><%=request_info[32]%></textarea>
                                                    <%
                                                }
                                                else
                                                {
                                                    %>
                                                    <textarea readonly="true" rows="10" name="required_info_response" id="required_info_response" class="form-control" placeholder="If required, Enter Information Here"><%=request_info[32]%></textarea>
                                                    <%
                                                }
                                                %>
                                                
                                            </div>
                                            <br>&nbsp;
                                        </div>  
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            &nbsp;
                                        </div>
                                    </div>
                                    <div class="row match-height">	
                                        <div class="col-6 " style="border-style: solid; border-width:thin; border-color: #f7f7f7;height: 180px; overflow-y: scroll;" >                    
                                            <div class="bs-callout-info callout-border-left callout-transparent mt-1 p-1">
                                                <h4 class="info">Optional Information </h4>
                                                <%
                                                String unEscaped_optional_user_input_text = StringEscapeUtils.unescapeHtml4(request_info[33]);
                                                out.print(unEscaped_optional_user_input_text);
                                                %>
                                                <br>&nbsp;
                                            </div>
                                        </div>
                                        <div class="col-6 " style="border-style: solid; border-width:thin; border-color: #f7f7f7;height: 180px; overflow-y: scroll;" >                    
                                            <div class="bs-callout-info callout-border-left callout-transparent mt-1 p-1">
                                                <h4 class="info">Optional Information Response <i class="la la-question-circle" title="Only Users with 'Administrator' or 'Manager' Role can edit this field." style="font-size: 14px;"> </i></h4>
                                                <%
                                                if(MANAGER.equalsIgnoreCase("true") || ADMINISTRATION.equalsIgnoreCase("true"))
                                                {
                                                    %>
                                                    <textarea readonly="true" rows="3" name="optional_info_response" id="optional_info_response" class="form-control" placeholder="Enter any Ooptional Information Here"><%=request_info[34]%></textarea>
                                                    <%
                                                }
                                                else
                                                {
                                                    %>
                                                    <textarea readonly="" rows="3" name="optional_info_response" id="optional_info_response" class="form-control" placeholder="Enter any Ooptional Information Here"><%=request_info[34]%></textarea>
                                                    <%
                                                }
                                                %>
                                                
                                            </div>
                                            <br>&nbsp;
                                        </div>  
                                    </div>   
                                    <div class="row">
                                        <div class="col-6">
                                            <br>
                                            <p class="text-muted">Notes <em>(Customer viewable)</em></p>
                                            <textarea readonly="true" class="form-control" name="notes" id="notes" rows="5"><%=request_info[27]%></textarea>  
                                        </div>
                                        <div class="col-6">
                                            <br>
                                            <p class="text-muted">Notes <em>(Service Desk)</em></p>
                                            <textarea readonly="true" class="form-control" name="desk_notes" id="desk_notes" rows="5"><%=request_info[28]%></textarea>  
                                        </div>
                                    </div> 
                                    <div class="row">
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Resolved Date</label>
                                            <div class='input-group'>
                                                <%
                                                String resolve_date_value = "";
                                                try
                                                {
                                                    GregorianCalendar resolve_date_calendar = new GregorianCalendar();
                                                    resolve_date_calendar.setTime(timestamp_format.parse(request_info[9]));
                                                    if(request_info[9] == null || request_info[9].equalsIgnoreCase("null") || request_info[9].equalsIgnoreCase(""))
                                                    {
                                                        resolve_date_value = "";
                                                    }
                                                    else
                                                    {
                                                        resolve_date_value = date_time_picker_format.format(resolve_date_calendar.getTime());
                                                    }
                                                }
                                                catch(Exception e)
                                                {
                                                    System.out.println("Exception on request.jsp getting resolve_date=" + e);
                                                }
                                                //String incident_time = date_time_picker_format.format(timestamp_format.parse(incident_info[2]).getTime());
                                                %>
                                                <input readonly="true" type='text' id="resolve_date" name="resolve_date" value="<%=resolve_date_value%>" class="form-control datetime" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <span class="la la-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Closure Date</label>
                                            <div class='input-group'>
                                                <%
                                                String closed_date_value = "";
                                                try
                                                {
                                                    GregorianCalendar closed_date_calendar = new GregorianCalendar();
                                                    closed_date_calendar.setTime(timestamp_format.parse(request_info[10]));
                                                    if(request_info[10] == null || request_info[10].equalsIgnoreCase("null") || request_info[10].equalsIgnoreCase(""))
                                                    {
                                                        closed_date_value = "";
                                                    }
                                                    else
                                                    {
                                                        closed_date_value = date_time_picker_format.format(closed_date_calendar.getTime());
                                                    }
                                                }
                                                catch(Exception e)
                                                {
                                                    System.out.println("Exception on request.jsp getting closed_date=" + e);
                                                }
                                                //String incident_time = date_time_picker_format.format(timestamp_format.parse(incident_info[2]).getTime());
                                                %>
                                                <input readonly="true" type='text' id="closed_date" name="closed_date" value="<%=closed_date_value%>" class="form-control datetime" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <span class="la la-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Closure Reason</label>
                                            <%
                                            ArrayList<String[]> closed_reason_select = db.get_system_select_field.active_for_table_column(con, "request", "closed_reason");
                                            String closed_reason = "";
                                            try
                                            {
                                                if(request_info[30] == null)
                                                {
                                                    closed_reason = "";
                                                }
                                                else
                                                {
                                                    closed_reason = request_info[30];
                                                }
                                            }
                                            catch(Exception e)
                                            {
                                                closed_reason = "";
                                            }
                                            %>
                                            <input readonly="true" type='text' id="closed_reason" name="closed_reason" value="<%=closed_reason%>" class="form-control" />
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-12 mt-1">
                                            <hr>
                                        </div>
                                    </div>   
                                    <div class="row">
                                        <div class="col-12 mt-1">
                                            &nbsp;
                                        </div>
                                    </div>  
                                    <div class="row">
                                        <div class="col-12 mt-1">
                                            &nbsp;
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>
                <!-- end content here-->
            </div> 
        </form>
    </div>
    <br>&nbsp;
    <br>&nbsp;
    <br>&nbsp;
    <br>&nbsp;
    
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN VENDOR JS-->    
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="app-assets/js/core/app-menu.js"></script>
    <script src="app-assets/js/core/app.js"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/popover/popover.js"></script>
    
    
    
    <script>     
       $( function() 
       {
            //caller
            $( "#requester_username" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#requester_username').val(ui.item.label); // display the selected text
                    $('#requested_for_id').val(ui.item.value); // save selected id to input
                    $('#location').val(ui.item.location); // save selected id to input
                    $('#department').val(ui.item.department); // save selected id to input
                    $('#site').val(ui.item.site); // save selected id to input
                    $('#company').val(ui.item.company); // save selected id to input
                    //if vip = true then check the vip checkbox
                    var vip = ui.item.vip;
                    if(vip.toLowerCase() === "true")
                    {
                        document.getElementById("vip").checked = true;
                    }
                    else
                    {
                        document.getElementById("vip").checked = false;
                    }
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            });
            //caller_group_name
            $( "#requester_group_name" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_group",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#requester_group_name').val(ui.item.label); // display the selected text
                    $('#requested_for_group_id').val(ui.item.value); // save selected id to input
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            });
            
            //create_by
            $( "#create_by_username" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#create_by_username').val(ui.item.label); // display the selected text
                    $('#create_by_id').val(ui.item.value); // save selected id to input
                    return false;
                }
            });
            //location
            $( "#location" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user_location",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#location').val(ui.item.label); // display the selected text
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            }); 
            //department
            $( "#department" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user_department",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#department').val(ui.item.label); // display the selected text
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            });
            //site
            $( "#site" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user_site",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#site').val(ui.item.label); // display the selected text
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            }); 
            //company
            $( "#company" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user_company",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#company').val(ui.item.label); // display the selected text
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            }); 
            //assigned_group
            $( "#assigned_group_name" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_group",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#assigned_to_name').val(""); // clear field
                    $('#assigned_to_id').val(""); // clear field
                    $('#assigned_group_name').val(ui.item.label); // set name
                    $('#assigned_group_id').val(ui.item.value); // set id
                    return false;
                }
            });
            //assigned_to_name
            $( "#assigned_to_name" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    var var_group_id = $('#assigned_group_id').val();
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_group_user",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term,
                            group_id: var_group_id                            
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#assigned_to_name').val(ui.item.label); // display the selected text
                    $('#assigned_to_id').val(ui.item.value); // save selected id to input
                    //alert("assigned to setting is=" + ui.item.value);
                    return false;
                }
            });
        });
    </script>
    <script>
        $(function() 
        {
          $('input[name="due_date"]').daterangepicker({
                "autoUpdateInput": false,
                "singleDatePicker": true,
                "showDropdowns": true,
                "timePicker": true,
                "timePicker24Hour": true,
                "locale": {
                    "format": "HH:mm MM/DD/YYYY",

                    "applyLabel": "Apply",
                    "cancelLabel": "Cancel",

                    "weekLabel": "W",
                    "daysOfWeek": [
                        "Su",
                        "Mo",
                        "Tu",
                        "We",
                        "Th",
                        "Fr",
                        "Sa"
                    ],
                    "monthNames": [
                        "January",
                        "February",
                        "March",
                        "April",
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",
                        "December"
                    ],
                    "firstDay": 1
              }
          });

          $('input[name="due_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });

          $('input[name="due_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });

        });
    </script>
    <script>
        $(function() 
        {
          $('input[name="request_date"]').daterangepicker({
                "autoUpdateInput": false,
                "singleDatePicker": true,
                "showDropdowns": true,
                "timePicker": true,
                "timePicker24Hour": true,
                "locale": {
                    "format": "HH:mm MM/DD/YYYY",

                    "applyLabel": "Apply",
                    "cancelLabel": "Cancel",

                    "weekLabel": "W",
                    "daysOfWeek": [
                        "Su",
                        "Mo",
                        "Tu",
                        "We",
                        "Th",
                        "Fr",
                        "Sa"
                    ],
                    "monthNames": [
                        "January",
                        "February",
                        "March",
                        "April",
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",
                        "December"
                    ],
                    "firstDay": 1
              }
          });

          $('input[name="request_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });

          $('input[name="request_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });

        });
    </script>
    <script type="text/javascript">
        $(function()
        {
          $('input[name="resolve_date"]').daterangepicker({
              "autoUpdateInput": false,
              "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
          $('input[name="resolve_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });

          $('input[name="resolve_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });
        });
    </script>
    
    <script>
        $(function()
        {
          $('input[name="closed_date"]').daterangepicker({
              "autoUpdateInput": false,
              "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
          $('input[name="closed_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });

          $('input[name="closed_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });
        });
    </script>
    
    
    
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>
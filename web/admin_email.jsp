<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        if(!session.getAttribute("administration").toString().equalsIgnoreCase("true"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);   
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String send_email_host = db.get_configuration_properties.by_name(con, "send_email_host").trim();
                //options SSL,TLS,PLAIN
                String send_email_encryption = db.get_configuration_properties.by_name(con, "send_email_encryption").trim();
                String send_email_smtp_port = db.get_configuration_properties.by_name(con, "send_email_smtp_port").trim();
                String send_email_user = db.get_configuration_properties.by_name(con, "send_email_user").trim();
                String send_email_password = db.get_configuration_properties.by_name(con, "send_email_password").trim();    
                send_email_password = support.encrypt_utils.decrypt(context_dir, send_email_password);
                
                String send_email_enable = "false";   
                try
                {
                    send_email_enable = db.get_configuration_properties.by_name(con, "send_email_enable").trim(); 
                    if(send_email_enable == null || send_email_enable.equalsIgnoreCase(""))
                    {
                        send_email_enable = "false";
                    }
                    else if(send_email_enable.equalsIgnoreCase("true"))
                    {
                        send_email_enable = "true";
                    }                        
                }
                catch (Exception e)
                {
                    send_email_enable = "false";
                }
                //get email settings
                String get_email_host = db.get_configuration_properties.by_name(con, "get_email_host").trim();
                String get_email_user = db.get_configuration_properties.by_name(con, "get_email_user").trim();
                String get_email_password = db.get_configuration_properties.by_name(con, "get_email_password").trim();   
                get_email_password = support.encrypt_utils.decrypt(context_dir, get_email_password);
                String get_email_enable = "false";   
                try
                {
                    get_email_enable = db.get_configuration_properties.by_name(con, "get_email_enable").trim(); 
                    if(get_email_enable == null || get_email_enable.equalsIgnoreCase(""))
                    {
                        get_email_enable = "false";
                    }
                    else if(get_email_enable.equalsIgnoreCase("true"))
                    {
                        get_email_enable = "true";
                    }                        
                }
                catch (Exception e)
                {
                    get_email_enable = "false";
                }
                String get_email_store_protocol = db.get_configuration_properties.by_name(con, "get_email_store_protocol").trim(); //imap or pop3
                //get pop3
                String mail_pop3_port = db.get_configuration_properties.by_name(con, "mail.pop3.port").trim();   
                String mail_pop3_starttls_enable = "false";
                try
                {
                    mail_pop3_starttls_enable = db.get_configuration_properties.by_name(con, "mail.pop3.starttls.enable").trim(); 
                    if(mail_pop3_starttls_enable == null || mail_pop3_starttls_enable.equalsIgnoreCase(""))
                    {
                        mail_pop3_starttls_enable = "false";
                    }
                    else if(mail_pop3_starttls_enable.equalsIgnoreCase("true"))
                    {
                        mail_pop3_starttls_enable = "true";
                    }                        
                }
                catch (Exception e)
                {
                    mail_pop3_starttls_enable = "false";
                }
                String mail_pop3_ssl_enable = "false";   
                try
                {
                    mail_pop3_ssl_enable = db.get_configuration_properties.by_name(con, "mail.pop3.ssl.enable").trim(); 
                    if(mail_pop3_ssl_enable == null || mail_pop3_ssl_enable.equalsIgnoreCase(""))
                    {
                        mail_pop3_ssl_enable = "false";
                    }
                    else if(mail_pop3_ssl_enable.equalsIgnoreCase("true"))
                    {
                        mail_pop3_ssl_enable = "true";
                    }                        
                }
                catch (Exception e)
                {
                    mail_pop3_ssl_enable = "false";
                }
                String mail_pop3_socketFactory_fallback = "false";   
                try
                {
                    mail_pop3_socketFactory_fallback = db.get_configuration_properties.by_name(con, "mail.pop3.socketFactory.fallback").trim(); 
                    if(mail_pop3_socketFactory_fallback == null || mail_pop3_socketFactory_fallback.equalsIgnoreCase(""))
                    {
                        mail_pop3_socketFactory_fallback = "false";
                    }
                    else if(mail_pop3_socketFactory_fallback.equalsIgnoreCase("true"))
                    {
                        mail_pop3_socketFactory_fallback = "true";
                    }                        
                }
                catch (Exception e)
                {
                    mail_pop3_socketFactory_fallback = "false";
                }
                //get imap
                String mail_imap_port = db.get_configuration_properties.by_name(con, "mail.imap.port").trim();   
                String mail_imap_starttls_enable = "false";
                try
                {
                    mail_imap_starttls_enable = db.get_configuration_properties.by_name(con, "mail.imap.starttls.enable").trim(); 
                    if(mail_imap_starttls_enable == null || mail_imap_starttls_enable.equalsIgnoreCase(""))
                    {
                        mail_imap_starttls_enable = "false";
                    }
                    else if(mail_imap_starttls_enable.equalsIgnoreCase("true"))
                    {
                        mail_imap_starttls_enable = "true";
                    }                        
                }
                catch (Exception e)
                {
                    mail_imap_starttls_enable = "false";
                }
                String mail_imap_ssl_enable = "false";   
                try
                {
                    mail_imap_ssl_enable = db.get_configuration_properties.by_name(con, "mail.imap.ssl.enable").trim(); 
                    if(mail_imap_ssl_enable == null || mail_pop3_ssl_enable.equalsIgnoreCase(""))
                    {
                        mail_imap_ssl_enable = "false";
                    }
                    else if(mail_imap_ssl_enable.equalsIgnoreCase("true"))
                    {
                        mail_imap_ssl_enable = "true";
                    }                        
                }
                catch (Exception e)
                {
                    mail_imap_ssl_enable = "false";
                }
                String mail_imap_socketFactory_fallback = "false";   
                try
                {
                    mail_imap_socketFactory_fallback = db.get_configuration_properties.by_name(con, "mail.imap.socketFactory.fallback").trim(); 
                    if(mail_imap_socketFactory_fallback == null || mail_imap_socketFactory_fallback.equalsIgnoreCase(""))
                    {
                        mail_imap_socketFactory_fallback = "false";
                    }
                    else if(mail_imap_socketFactory_fallback.equalsIgnoreCase("true"))
                    {
                        mail_imap_socketFactory_fallback = "true";
                    }                        
                }
                catch (Exception e)
                {
                    mail_imap_socketFactory_fallback = "false";
                }



    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END PAGE LEVEL CSS-->

<div class="clr whtbg p-10 mb-15 position-relative">
    <h1 class="large-font boldFont">Email Settings</h1>
    <div class="custom-tabs clr ">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="send_server_tab" data-toggle="tab" href="#tab1" aria-controls="activeIcon12" aria-expanded="true"><i class="la la-cogs"></i>Send Email Server Settings</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="receive_server_tab" data-toggle="tab" href="#tab2" aria-controls="activeIcon12" aria-expanded="false"><i class="la la-cogs"></i>Receive Email Server Settings</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="notification_tab" data-toggle="tab" href="#tab3" aria-controls="activeIcon12" aria-expanded="false"><i class="ft-users"></i>Global Email Notifications</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="mail_tab" data-toggle="tab" href="#tab4" aria-controls="activeIcon12" aria-expanded="false"><i class="ft-mail"></i>Global Email Formats</a>
            </li>
        </ul><!-- Tab panes -->
    </div>
</div>

    
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="tab1" aria-labelledby="send_server_tab" aria-expanded="true">
                                        <form class="form" action="admin_email_send" method="post">
                                            <h3>Send Email Settings</h3>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="send_email_enable">Enable Send Email</label>
                                                        <br>
                                                        <%
                                                        String checked = "";
                                                        if(send_email_enable.equalsIgnoreCase("true"))
                                                        {
                                                            checked = "CHECKED";
                                                        }
                                                        %>
                                                        <input <%=checked%> type="checkbox" id="send_email_enable" name="send_email_enable" value="ON"/>

                                                    </div>
                                                </div>
                                            
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="send_email_host">Email Host</label>
                                                        <input type="text" id="send_email_host" name="send_email_host" class="form-control" value="<%=send_email_host%>">
                                                    </div>
                                                </div>  
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="send_email_smtp_port">SMTP Port</label>
                                                        <input type="text" id="send_email_smtp_port" name="send_email_smtp_port" class="form-control" value="<%=send_email_smtp_port%>" >
                                                    </div>
                                                </div> 
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="send_email_encryption">Encryption</label>
                                                        <select id="send_email_encryption" name="send_email_encryption" class="form-control">
                                                        <%
                                                        String send_option_values[] = {"","plain","tls","ssl"};
                                                        String send_option_names[] = {"","Plain","TLS","SSL"};
                                                        for(int a = 0; a < send_option_values.length;a++)
                                                        {
                                                            if(send_email_encryption.equalsIgnoreCase(send_option_values[a]))
                                                            {
                                                                %>
                                                                <option SELECTED value="<%=send_option_values[a]%>"><%=send_option_names[a]%></option>
                                                                <%
                                                            }
                                                            else
                                                            {
                                                                %>
                                                                <option value="<%=send_option_values[a]%>"><%=send_option_names[a]%></option>
                                                                <%
                                                            }
                                                        }
                                                        %>
                                                        </select>
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="send_email_user">User Name</label>
                                                        <input type="text" id="send_email_user" name="send_email_user" class="form-control" value="<%=send_email_user%>" >
                                                    </div>
                                                </div> 
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="send_email_password">Password</label>
                                                        <input type="password" id="send_email_password" name="send_email_password" class="form-control" value="<%=send_email_password%>" >
                                                    </div>
                                                </div> 
                                                <div class="col-md-3">
                                                    <div class="form-group">

                                                    </div>
                                                </div>
                                                    <div class="col-md-3">
                                                    <div class="form-group">

                                                    </div>
                                                </div>
                                            </div>
                                            <!--        
                                            Name	Type	Description
                                            mail.smtp.user	String	Default user name for SMTP.
                                            mail.smtp.host	String	The SMTP server to connect to.
                                            mail.smtp.port	int	The SMTP server port to connect to, if the connect() method doesn't explicitly specify one. Defaults to 25.
                                            mail.smtp.connectiontimeout	int	Socket connection timeout value in milliseconds. This timeout is implemented by java.net.Socket. Default is infinite timeout.
                                            mail.smtp.timeout	int	Socket read timeout value in milliseconds. This timeout is implemented by java.net.Socket. Default is infinite timeout.
                                            mail.smtp.writetimeout	int	Socket write timeout value in milliseconds. This timeout is implemented by using a java.util.concurrent.ScheduledExecutorService per connection that schedules a thread to close the socket if the timeout expires. Thus, the overhead of using this timeout is one thread per connection. Default is infinite timeout.
                                            mail.smtp.from	String	Email address to use for SMTP MAIL command. This sets the envelope return address. Defaults to msg.getFrom() or InternetAddress.getLocalAddress(). NOTE: mail.smtp.user was previously used for this.
                                            mail.smtp.localhost	String	Local host name used in the SMTP HELO or EHLO command. Defaults to InetAddress.getLocalHost().getHostName(). Should not normally need to be set if your JDK and your name service are configured properly.
                                            mail.smtp.localaddress	String	Local address (host name) to bind to when creating the SMTP socket. Defaults to the address picked by the Socket class. Should not normally need to be set, but useful with multi-homed hosts where it's important to pick a particular local address to bind to.
                                            mail.smtp.localport	int	Local port number to bind to when creating the SMTP socket. Defaults to the port number picked by the Socket class.
                                            mail.smtp.ehlo	boolean	If false, do not attempt to sign on with the EHLO command. Defaults to true. Normally failure of the EHLO command will fallback to the HELO command; this property exists only for servers that don't fail EHLO properly or don't implement EHLO properly.
                                            mail.smtp.auth	boolean	If true, attempt to authenticate the user using the AUTH command. Defaults to false.
                                            mail.smtp.auth.mechanisms	String	If set, lists the authentication mechanisms to consider, and the order in which to consider them. Only mechanisms supported by the server and supported by the current implementation will be used. The default is "LOGIN PLAIN DIGEST-MD5 NTLM", which includes all the authentication mechanisms supported by the current implementation except XOAUTH2.
                                            mail.smtp.auth.login.disable	boolean	If true, prevents use of the AUTH LOGIN command. Default is false.
                                            mail.smtp.auth.plain.disable	boolean	If true, prevents use of the AUTH PLAIN command. Default is false.
                                            mail.smtp.auth.digest-md5.disable	boolean	If true, prevents use of the AUTH DIGEST-MD5 command. Default is false.
                                            mail.smtp.auth.ntlm.disable	boolean	If true, prevents use of the AUTH NTLM command. Default is false.
                                            mail.smtp.auth.ntlm.domain	String	The NTLM authentication domain.
                                            mail.smtp.auth.ntlm.flags	int	NTLM protocol-specific flags. See http://curl.haxx.se/rfc/ntlm.html#theNtlmFlags for details.
                                            mail.smtp.auth.xoauth2.disable	boolean	If true, prevents use of the AUTHENTICATE XOAUTH2 command. Because the OAuth 2.0 protocol requires a special access token instead of a password, this mechanism is disabled by default. Enable it by explicitly setting this property to "false" or by setting the "mail.smtp.auth.mechanisms" property to "XOAUTH2".
                                            mail.smtp.submitter	String	The submitter to use in the AUTH tag in the MAIL FROM command. Typically used by a mail relay to pass along information about the original submitter of the message. See also the setSubmitter method of SMTPMessage. Mail clients typically do not use this.
                                            mail.smtp.dsn.notify	String	The NOTIFY option to the RCPT command. Either NEVER, or some combination of SUCCESS, FAILURE, and DELAY (separated by commas).
                                            mail.smtp.dsn.ret	String	The RET option to the MAIL command. Either FULL or HDRS.
                                            mail.smtp.allow8bitmime	boolean	If set to true, and the server supports the 8BITMIME extension, text parts of messages that use the "quoted-printable" or "base64" encodings are converted to use "8bit" encoding if they follow the RFC2045 rules for 8bit text.
                                            mail.smtp.sendpartial	boolean	If set to true, and a message has some valid and some invalid addresses, send the message anyway, reporting the partial failure with a SendFailedException. If set to false (the default), the message is not sent to any of the recipients if there is an invalid recipient address.
                                            mail.smtp.sasl.enable	boolean	If set to true, attempt to use the javax.security.sasl package to choose an authentication mechanism for login. Defaults to false.
                                            mail.smtp.sasl.mechanisms	String	A space or comma separated list of SASL mechanism names to try to use.
                                            mail.smtp.sasl.authorizationid	String	The authorization ID to use in the SASL authentication. If not set, the authentication ID (user name) is used.
                                            mail.smtp.sasl.realm	String	The realm to use with DIGEST-MD5 authentication.
                                            mail.smtp.sasl.usecanonicalhostname	boolean	If set to true, the canonical host name returned by InetAddress.getCanonicalHostName is passed to the SASL mechanism, instead of the host name used to connect. Defaults to false.
                                            mail.smtp.quitwait	boolean	If set to false, the QUIT command is sent and the connection is immediately closed. If set to true (the default), causes the transport to wait for the response to the QUIT command.
                                            mail.smtp.reportsuccess	boolean	If set to true, causes the transport to include an SMTPAddressSucceededException for each address that is successful. Note also that this will cause a SendFailedException to be thrown from the sendMessage method of SMTPTransport even if all addresses were correct and the message was sent successfully.
                                            mail.smtp.socketFactory	SocketFactory	If set to a class that implements the javax.net.SocketFactory interface, this class will be used to create SMTP sockets. Note that this is an instance of a class, not a name, and must be set using the put method, not the setProperty method.
                                            mail.smtp.socketFactory.class	String	If set, specifies the name of a class that implements the javax.net.SocketFactory interface. This class will be used to create SMTP sockets.
                                            mail.smtp.socketFactory.fallback	boolean	If set to true, failure to create a socket using the specified socket factory class will cause the socket to be created using the java.net.Socket class. Defaults to true.
                                            mail.smtp.socketFactory.port	int	Specifies the port to connect to when using the specified socket factory. If not set, the default port will be used.
                                            mail.smtp.ssl.enable	boolean	If set to true, use SSL to connect and use the SSL port by default. Defaults to false for the "smtp" protocol and true for the "smtps" protocol.
                                            mail.smtp.ssl.checkserveridentity	boolean	If set to true, check the server identity as specified by RFC 2595. These additional checks based on the content of the server's certificate are intended to prevent man-in-the-middle attacks. Defaults to false.
                                            mail.smtp.ssl.trust	String	If set, and a socket factory hasn't been specified, enables use of a MailSSLSocketFactory. If set to "*", all hosts are trusted. If set to a whitespace separated list of hosts, those hosts are trusted. Otherwise, trust depends on the certificate the server presents.
                                            mail.smtp.ssl.socketFactory	SSLSocketFactory	If set to a class that extends the javax.net.ssl.SSLSocketFactory class, this class will be used to create SMTP SSL sockets. Note that this is an instance of a class, not a name, and must be set using the put method, not the setProperty method.
                                            mail.smtp.ssl.socketFactory.class	String	If set, specifies the name of a class that extends the javax.net.ssl.SSLSocketFactory class. This class will be used to create SMTP SSL sockets.
                                            mail.smtp.ssl.socketFactory.port	int	Specifies the port to connect to when using the specified socket factory. If not set, the default port will be used.
                                            mail.smtp.ssl.protocols	string	Specifies the SSL protocols that will be enabled for SSL connections. The property value is a whitespace separated list of tokens acceptable to the javax.net.ssl.SSLSocket.setEnabledProtocols method.
                                            mail.smtp.ssl.ciphersuites	string	Specifies the SSL cipher suites that will be enabled for SSL connections. The property value is a whitespace separated list of tokens acceptable to the javax.net.ssl.SSLSocket.setEnabledCipherSuites method.
                                            mail.smtp.starttls.enable	boolean	If true, enables the use of the STARTTLS command (if supported by the server) to switch the connection to a TLS-protected connection before issuing any login commands. If the server does not support STARTTLS, the connection continues without the use of TLS; see the mail.smtp.starttls.required property to fail if STARTTLS isn't supported. Note that an appropriate trust store must configured so that the client will trust the server's certificate. Defaults to false.
                                            mail.smtp.starttls.required	boolean	If true, requires the use of the STARTTLS command. If the server doesn't support the STARTTLS command, or the command fails, the connect method will fail. Defaults to false.
                                            mail.smtp.proxy.host	string	Specifies the host name of an HTTP web proxy server that will be used for connections to the mail server.
                                            mail.smtp.proxy.port	string	Specifies the port number for the HTTP web proxy server. Defaults to port 80.
                                            mail.smtp.proxy.user	string	Specifies the user name to use to authenticate with the HTTP web proxy server. By default, no authentication is done.
                                            mail.smtp.proxy.password	string	Specifies the password to use to authenticate with the HTTP web proxy server. By default, no authentication is done.
                                            mail.smtp.socks.host	string	Specifies the host name of a SOCKS5 proxy server that will be used for connections to the mail server.
                                            mail.smtp.socks.port	string	Specifies the port number for the SOCKS5 proxy server. This should only need to be used if the proxy server is not using the standard port number of 1080.
                                            mail.smtp.mailextension	String	Extension string to append to the MAIL command. The extension string can be used to specify standard SMTP service extensions as well as vendor-specific extensions. Typically the application should use the SMTPTransport method supportsExtension to verify that the server supports the desired service extension. See RFC 1869 and other RFCs that define specific extensions.
                                            mail.smtp.userset	boolean	If set to true, use the RSET command instead of the NOOP command in the isConnected method. In some cases sendmail will respond slowly after many NOOP commands; use of RSET avoids this sendmail issue. Defaults to false.
                                            mail.smtp.noop.strict	boolean	If set to true (the default), insist on a 250 response code from the NOOP command to indicate success. The NOOP command is used by the isConnected method to determine if the connection is still alive. Some older servers return the wrong response code on success, some servers don't implement the NOOP command at all and so always return a failure code. Set this property to false to handle servers that are broken in this way. Normally, when a server times out a connection, it will send a 421 response code, which the client will see as the response to the next command it issues. Some servers send the wrong failure response code when timing out a connection. Do not set this property to false when dealing with servers that are broken in this way.
                                            -->

                                            <div class="form-actions">
                                                <button type="button" onclick="javascript:location.href='admin.jsp'" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </button>
                                                <button type="submit" class="btn btn-info">
                                                    &nbsp;&nbsp;<i class="ft-save"></i> Save&nbsp;&nbsp;
                                                </button>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <button type="button" onclick="javascript:location.href='admin_send_email_test.jsp'" class="btn btn-primary mr-1">
                                                    <i class="ft-mail"></i> Send a Test Email
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                    
                                    <div role="tabpanel" class="tab-pane" id="tab2" aria-labelledby="receive_server_tab" aria-expanded="false">
                                        <form class="form" action="admin_email_receive" method="post">
                                            <h3>Receive Email Settings</h3>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="send_email_enable">Enable Receive Email</label>
                                                        <br>
                                                        <%
                                                        checked = "";
                                                        if(get_email_enable.equalsIgnoreCase("true"))
                                                        {
                                                            checked = "CHECKED";
                                                        }
                                                        %>
                                                        <input <%=checked%> type="checkbox" id="get_email_enable" name="get_email_enable" value="ON"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="get_email_host">Email Host</label>
                                                        <input type="text" id="get_email_host" name="get_email_host" class="form-control" value="<%=get_email_host%>">
                                                    </div>
                                                </div>  
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="get_email_user">User Name</label>
                                                        <input type="text" id="get_email_user" name="get_email_user" class="form-control" value="<%=get_email_user%>" >
                                                    </div>
                                                </div> 
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="get_email_password">Password</label>
                                                        <input type="password" id="get_email_password" name="get_email_password" class="form-control" value="<%=get_email_password%>" >
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="row">    
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="mail_store_protocol">Store Protocol</label>
                                                        <select onchange="show(this.value)" id="get_email_store_protocol" name="get_email_store_protocol" class="form-control">
                                                        <%
                                                        String get_email_store_protocol_option_values[] = {"","pop3","pop3s","imap"};
                                                        String get_email_store_protocol_option_names[] = {"","POP3","POP3S", "IMAP"};
                                                        for(int a = 0; a < get_email_store_protocol_option_values.length;a++)
                                                        {
                                                            if(get_email_store_protocol.equalsIgnoreCase(get_email_store_protocol_option_values[a]))
                                                            {
                                                                %>
                                                                <option SELECTED value="<%=get_email_store_protocol_option_values[a]%>"><%=get_email_store_protocol_option_names[a]%></option>
                                                                <%
                                                            }
                                                            else
                                                            {
                                                                %>
                                                                <option value="<%=get_email_store_protocol_option_values[a]%>"><%=get_email_store_protocol_option_names[a]%></option>
                                                                <%
                                                            }
                                                        }
                                                        %>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                                 
                                            <div id="pop3" style="display:none">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="mail_pop3_port">POP3 Port</label>
                                                            <input type="text" id="mail_pop3_port" name="mail_pop3_port" class="form-control" value="<%=mail_pop3_port%>"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="mail_pop3_starttls_enable">POP3 Enable StartTLS</label>
                                                            <br>
                                                            <%
                                                            checked = "";
                                                            if(mail_pop3_starttls_enable.equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                            %>
                                                            <input <%=checked%> type="checkbox" id="mail_pop3_starttls_enable" name="mail_pop3_starttls_enable" value="ON"/>
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="mail_pop3_ssl_enable">POP3 Enable SSL</label>
                                                            <br>
                                                            <%
                                                            checked = "";
                                                            if(mail_pop3_ssl_enable.equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                            %>
                                                            <input <%=checked%> type="checkbox" id="mail_pop3_ssl_enable" name="mail_pop3_ssl_enable" value=""/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="mail_pop3_socketFactory_fallback">POP3 Enable SSL Fallback</label>
                                                            <br>
                                                            <%
                                                            checked = "";
                                                            if(mail_pop3_socketFactory_fallback.equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                            %>
                                                            <input <%=checked%> type="checkbox" id="mail_pop3_socketFactory_fallback" name="mail_pop3_socketFactory_fallback" value=""/>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div id="imap" style="display:none">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="mail_imap_port">IMAP Port</label>
                                                            <input type="text" id="mail_imap_port" name="mail_imap_port" class="form-control" value="<%=mail_imap_port%>"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="mail_imap_starttls_enable">IMAP Enable StartTLS</label>
                                                            <br>
                                                            <%
                                                            checked = "";
                                                            if(mail_imap_starttls_enable.equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                            %>
                                                            <input <%=checked%> type="checkbox" id="mail_imap_starttls_enable" name="mail_imap_starttls_enable" value="ON"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="mail_imap_ssl_enable">IMAP Enable SSL</label>
                                                            <br>
                                                            <%
                                                            checked = "";
                                                            if(mail_imap_ssl_enable.equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                            %>
                                                            <input <%=checked%> type="checkbox" id="mail_imap_ssl_enable" name="mail_imap_ssl_enable" value="ON"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="mail_imap_socketFactory_fallback">IMAP Enable SSL Fallback</label>
                                                            <br>
                                                            <%
                                                            checked = "";
                                                            if(mail_imap_socketFactory_fallback.equalsIgnoreCase("true"))
                                                            {
                                                                checked = "CHECKED";
                                                            }
                                                            %>
                                                            <input <%=checked%> type="checkbox" id="mail_imap_socketFactory_fallback" name="mail_imap_socketFactory_fallback" value="ON"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>           
                                                      
                                            
                                                <!--
                                                Name	Type	Description
                                                mail.pop3.user	String	Default user name for POP3.
                                                mail.pop3.host	String	The POP3 server to connect to.
                                                mail.pop3.port	int	The POP3 server port to connect to, if the connect() method doesn't explicitly specify one. Defaults to 110.
                                                mail.pop3.connectiontimeout	int	Socket connection timeout value in milliseconds. This timeout is implemented by java.net.Socket. Default is infinite timeout.
                                                mail.pop3.timeout	int	Socket read timeout value in milliseconds. This timeout is implemented by java.net.Socket. Default is infinite timeout.
                                                mail.pop3.writetimeout	int	Socket write timeout value in milliseconds. This timeout is implemented by using a java.util.concurrent.ScheduledExecutorService per connection that schedules a thread to close the socket if the timeout expires. Thus, the overhead of using this timeout is one thread per connection. Default is infinite timeout.
                                                mail.pop3.rsetbeforequit	boolean	Send a POP3 RSET command when closing the folder, before sending the QUIT command. Useful with POP3 servers that implicitly mark all messages that are read as "deleted"; this will prevent such messages from being deleted and expunged unless the client requests so. Default is false.
                                                mail.pop3.message.class	String	Class name of a subclass of com.sun.mail.pop3.POP3Message. The subclass can be used to handle (for example) non-standard Content-Type headers. The subclass must have a public constructor of the form MyPOP3Message(Folder f, int msgno) throws MessagingException.
                                                mail.pop3.localaddress	String	Local address (host name) to bind to when creating the POP3 socket. Defaults to the address picked by the Socket class. Should not normally need to be set, but useful with multi-homed hosts where it's important to pick a particular local address to bind to.
                                                mail.pop3.localport	int	Local port number to bind to when creating the POP3 socket. Defaults to the port number picked by the Socket class.
                                                mail.pop3.apop.enable	boolean	If set to true, use APOP instead of USER/PASS to login to the POP3 server, if the POP3 server supports APOP. APOP sends a digest of the password rather than the clear text password. Defaults to false.
                                                mail.pop3.socketFactory	SocketFactory	If set to a class that implements the javax.net.SocketFactory interface, this class will be used to create POP3 sockets. Note that this is an instance of a class, not a name, and must be set using the put method, not the setProperty method.
                                                mail.pop3.socketFactory.class	String	If set, specifies the name of a class that implements the javax.net.SocketFactory interface. This class will be used to create POP3 sockets.
                                                mail.pop3.socketFactory.fallback	boolean	If set to true, failure to create a socket using the specified socket factory class will cause the socket to be created using the java.net.Socket class. Defaults to true.
                                                mail.pop3.socketFactory.port	int	Specifies the port to connect to when using the specified socket factory. If not set, the default port will be used.
                                                mail.pop3.ssl.enable	boolean	If set to true, use SSL to connect and use the SSL port by default. Defaults to false for the "pop3" protocol and true for the "pop3s" protocol.
                                                mail.pop3.ssl.checkserveridentity	boolean	If set to true, check the server identity as specified by RFC 2595. These additional checks based on the content of the server's certificate are intended to prevent man-in-the-middle attacks. Defaults to false.
                                                mail.pop3.ssl.trust	String	If set, and a socket factory hasn't been specified, enables use of a MailSSLSocketFactory. If set to "*", all hosts are trusted. If set to a whitespace separated list of hosts, those hosts are trusted. Otherwise, trust depends on the certificate the server presents.
                                                mail.pop3.ssl.socketFactory	SSLSocketFactory	If set to a class that extends the javax.net.ssl.SSLSocketFactory class, this class will be used to create POP3 SSL sockets. Note that this is an instance of a class, not a name, and must be set using the put method, not the setProperty method.
                                                mail.pop3.ssl.socketFactory.class	String	If set, specifies the name of a class that extends the javax.net.ssl.SSLSocketFactory class. This class will be used to create POP3 SSL sockets.
                                                mail.pop3.ssl.socketFactory.port	int	Specifies the port to connect to when using the specified socket factory. If not set, the default port will be used.
                                                mail.pop3.ssl.protocols	string	Specifies the SSL protocols that will be enabled for SSL connections. The property value is a whitespace separated list of tokens acceptable to the javax.net.ssl.SSLSocket.setEnabledProtocols method.
                                                mail.pop3.ssl.ciphersuites	string	Specifies the SSL cipher suites that will be enabled for SSL connections. The property value is a whitespace separated list of tokens acceptable to the javax.net.ssl.SSLSocket.setEnabledCipherSuites method.
                                                mail.pop3.starttls.enable	boolean	If true, enables the use of the STLS command (if supported by the server) to switch the connection to a TLS-protected connection before issuing any login commands. If the server does not support STARTTLS, the connection continues without the use of TLS; see the mail.pop3.starttls.required property to fail if STARTTLS isn't supported. Note that an appropriate trust store must configured so that the client will trust the server's certificate. Defaults to false.
                                                mail.pop3.starttls.required	boolean	If true, requires the use of the STLS command. If the server doesn't support the STLS command, or the command fails, the connect method will fail. Defaults to false.
                                                mail.pop3.proxy.host	string	Specifies the host name of an HTTP web proxy server that will be used for connections to the mail server.
                                                mail.pop3.proxy.port	string	Specifies the port number for the HTTP web proxy server. Defaults to port 80.
                                                mail.pop3.proxy.user	string	Specifies the user name to use to authenticate with the HTTP web proxy server. By default, no authentication is done.
                                                mail.pop3.proxy.password	string	Specifies the password to use to authenticate with the HTTP web proxy server. By default, no authentication is done.
                                                mail.pop3.socks.host	string	Specifies the host name of a SOCKS5 proxy server that will be used for connections to the mail server.
                                                mail.pop3.socks.port	string	Specifies the port number for the SOCKS5 proxy server. This should only need to be used if the proxy server is not using the standard port number of 1080.
                                                mail.pop3.disabletop	boolean	If set to true, the POP3 TOP command will not be used to fetch message headers. This is useful for POP3 servers that don't properly implement the TOP command, or that provide incorrect information in the TOP command results. Defaults to false.
                                                mail.pop3.disablecapa	boolean	If set to true, the POP3 CAPA command will not be used to fetch server capabilities. This is useful for POP3 servers that don't properly implement the CAPA command, or that provide incorrect information in the CAPA command results. Defaults to false.
                                                mail.pop3.forgettopheaders	boolean	If set to true, the headers that might have been retrieved using the POP3 TOP command will be forgotten and replaced by headers retrieved as part of the POP3 RETR command. Some servers, such as some versions of Microsft Exchange and IBM Lotus Notes, will return slightly different headers each time the TOP or RETR command is used. To allow the POP3 provider to properly parse the message content returned from the RETR command, the headers also returned by the RETR command must be used. Setting this property to true will cause these headers to be used, even if they differ from the headers returned previously as a result of using the TOP command. Defaults to false.
                                                mail.pop3.filecache.enable	boolean	If set to true, the POP3 provider will cache message data in a temporary file rather than in memory. Messages are only added to the cache when accessing the message content. Message headers are always cached in memory (on demand). The file cache is removed when the folder is closed or the JVM terminates. Defaults to false.
                                                mail.pop3.filecache.dir	String	If the file cache is enabled, this property can be used to override the default directory used by the JDK for temporary files.
                                                mail.pop3.cachewriteto	boolean	Controls the behavior of the writeTo method on a POP3 message object. If set to true, and the message content hasn't yet been cached, and ignoreList is null, the message is cached before being written. Otherwise, the message is streamed directly to the output stream without being cached. Defaults to false.
                                                mail.pop3.keepmessagecontent	boolean	The content of a message is cached when it is first fetched. Normally this cache uses a SoftReference to refer to the cached content. This allows the cached content to be purged if memory is low, in which case the content will be fetched again if it's needed. If this property is set to true, a hard reference to the cached content will be kept, preventing the memory from being reused until the folder is closed or the cached content is explicitly invalidated (using the invalidate method). (This was the behavior in previous versions of JavaMail.) Defaults to false.
                                                mail.pop3.finalizecleanclose	boolean	When the finalizer for POP3Store or POP3Folder is called, should the connection to the server be closed cleanly, as if the application called the close method? Or should the connection to the server be closed without sending any commands to the server? Defaults to false, the connection is not closed cleanly.
                                                -->

                                            <div class="form-actions">
                                                <button type="button" onclick="javascript:location.href='admin.jsp'" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </button>
                                                <button type="submit" class="btn btn-info">
                                                    &nbsp;&nbsp;<i class="ft-save"></i> Save&nbsp;&nbsp;
                                                </button>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <button type="button" onclick="javascript:location.href='email_get_test_results.jsp'" class="btn btn-primary mr-1">
                                                    <i class="ft-mail"></i> Test Getting Emails From This Account
                                                </button>
                                            </div>
                                        </form>
                                    </div>                
                                                    
                                              
                                    <div role="tabpanel" class="tab-pane"  id="tab3" aria-labelledby="notification_tab" aria-expanded="false">
                                        <form class="form" action="admin_email_global_notifications" method="post">
                                            <%
                                            ArrayList<String[]> all_global_notifications = db.get_notifications.all_global(con);
                                            checked = "";                                                            
                                            %>
                                            <div class="row">
                                                <div class="col-12">
                                                    <h3>Send the "Caller" a Notification on the following Incident Events</h3>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <fieldset class="checkbox">
                                                        <label>
                                                            <%
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("caller_incident_create") && this_setting[2].equalsIgnoreCase("true"))
                                                                {
                                                                    checked = "CHECKED";
                                                                }
                                                            }
                                                            %>
                                                            <input <%=checked%> type='checkbox' id="caller_incident_create" name="caller_incident_create" value="ON"/>&nbsp;<label>Incident Create</label>
                                                        </label>
                                                    </fieldset>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <fieldset class="checkbox">
                                                        <label>
                                                            <%
                                                            checked = ""; 
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("caller_incident_update") && this_setting[2].equalsIgnoreCase("true"))
                                                                {
                                                                    checked = "CHECKED";
                                                                }
                                                            }
                                                            %>
                                                            <input <%=checked%> type='checkbox' id="caller_incident_update" name="caller_incident_update" value="ON"/>&nbsp;<label>Incident Update</label>
                                                        </label>
                                                    </fieldset>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <fieldset class="checkbox">
                                                        <label>
                                                            <%
                                                            checked = ""; 
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("caller_incident_resolve") && this_setting[2].equalsIgnoreCase("true"))
                                                                {
                                                                    checked = "CHECKED";
                                                                }
                                                            }
                                                            %>
                                                            <input <%=checked%> type='checkbox' id="caller_incident_resolve" name="caller_incident_resolve" value="ON"/>&nbsp;<label>Incident Resolved</label>
                                                        </label>
                                                    </fieldset>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <fieldset class="checkbox">
                                                        <label>
                                                            <%
                                                            checked = ""; 
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("caller_incident_close") && this_setting[2].equalsIgnoreCase("true"))
                                                                {
                                                                    checked = "CHECKED";
                                                                }
                                                            }
                                                            %>
                                                            <input <%=checked%> type='checkbox' id="caller_incident_close" name="caller_incident_close" value="ON"/>&nbsp;<label>Incident Close</label>
                                                        </label>
                                                    </fieldset>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    &nbsp;
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    &nbsp;
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12"><hr></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <h3>Send the "Request Create By" a Notification on the following Request Events</h3>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <fieldset class="checkbox">
                                                        <label>
                                                            <%
                                                            checked = ""; 
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("request_created_by_create") && this_setting[2].equalsIgnoreCase("true"))
                                                                {
                                                                    checked = "CHECKED";
                                                                }
                                                            }
                                                            %>
                                                            <input <%=checked%> type='checkbox' id="request_created_by_create" name="request_created_by_create" value="ON"/>&nbsp;<label>Request Create</label>
                                                        </label>
                                                    </fieldset>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <fieldset class="checkbox">
                                                        <label>
                                                            <%
                                                            checked = ""; 
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("request_created_by_update") && this_setting[2].equalsIgnoreCase("true"))
                                                                {
                                                                    checked = "CHECKED";
                                                                }
                                                            }
                                                            %>
                                                            <input <%=checked%> type='checkbox' id="request_created_by_update" name="request_created_by_update" value="ON"/>&nbsp;<label>Request Update</label>
                                                        </label>
                                                    </fieldset>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <fieldset class="checkbox">
                                                        <label>
                                                            <%
                                                            checked = ""; 
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("request_created_by_resolve") && this_setting[2].equalsIgnoreCase("true"))
                                                                {
                                                                    checked = "CHECKED";
                                                                }
                                                            }
                                                            %>
                                                            <input <%=checked%> type='checkbox' id="request_created_by_resolve" name="request_created_by_resolve" value="ON"/>&nbsp;<label>Request Resolved</label>
                                                        </label>
                                                    </fieldset>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <fieldset class="checkbox">
                                                        <label>
                                                            <%
                                                            checked = ""; 
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("request_created_by_close") && this_setting[2].equalsIgnoreCase("true"))
                                                                {
                                                                    checked = "CHECKED";
                                                                }
                                                            }
                                                            %>
                                                            <input <%=checked%> type='checkbox' id="request_created_by_close" name="request_created_by_close" value="ON"/>&nbsp;<label>Request Close</label>
                                                        </label>
                                                    </fieldset>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    &nbsp;
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    &nbsp;
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12"><hr></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <h3>Send the "Requester" a Notification on the following Request Events</h3>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <fieldset class="checkbox">
                                                        <label>
                                                            <%
                                                            checked = ""; 
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("request_requester_create") && this_setting[2].equalsIgnoreCase("true"))
                                                                {
                                                                    checked = "CHECKED";
                                                                }
                                                            }
                                                            %>
                                                            <input <%=checked%> type='checkbox' id="request_requester_create" name="request_requester_create" value="ON"/>&nbsp;<label>Request Create</label>
                                                        </label>
                                                    </fieldset>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <fieldset class="checkbox">
                                                        <label>
                                                            <%
                                                            checked = ""; 
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("request_requester_update") && this_setting[2].equalsIgnoreCase("true"))
                                                                {
                                                                    checked = "CHECKED";
                                                                }
                                                            }
                                                            %>
                                                            <input <%=checked%> type='checkbox' id="request_requester_update" name="request_requester_update" value="ON"/>&nbsp;<label>Request Update</label>
                                                        </label>
                                                    </fieldset>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <fieldset class="checkbox">
                                                        <label>
                                                            <%
                                                            checked = ""; 
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("request_requester_resolve") && this_setting[2].equalsIgnoreCase("true"))
                                                                {
                                                                    checked = "CHECKED";
                                                                }
                                                            }
                                                            %>
                                                            <input <%=checked%> type='checkbox' id="request_requester_resolve" name="request_requester_resolve" value="ON"/>&nbsp;<label>Request Resolved</label>
                                                        </label>
                                                    </fieldset>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    <fieldset class="checkbox">
                                                        <label>
                                                            <%
                                                            checked = ""; 
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("request_requester_close") && this_setting[2].equalsIgnoreCase("true"))
                                                                {
                                                                    checked = "CHECKED";
                                                                }
                                                            }
                                                            %>
                                                            <input <%=checked%> type='checkbox' id="request_requester_close" name="request_requester_close" value="ON"/>&nbsp;<label>Request Close</label>
                                                        </label>
                                                    </fieldset>
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    &nbsp;
                                                </div>
                                                <div class="col-xl-2 col-lg-6 col-12">
                                                    &nbsp;
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <button type="button" onclick="javascript:location.href='admin.jsp'" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </button>
                                                <button type="submit" class="btn btn-info">
                                                    &nbsp;&nbsp;<i class="ft-save"></i> Save&nbsp;&nbsp;
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                    <div role="tabpanel" class="tab-pane"  id="tab4" aria-labelledby="mail_tab" aria-expanded="false">
                                        <form class="form" action="admin_email_global_notifications_format" method="post">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h3>Incident Email Notification Formats</h3>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-8 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>
                                                            Create Incident Email Subject
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <button type="button" onclick="test_email('incident','incident_create_subject','incident_create_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                        </label>
                                                        
                                                        <div class='input-group'>
                                                            <%
                                                            String value = "";
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("caller_incident_create"))
                                                                {
                                                                    value = this_setting[3];
                                                                }
                                                            }
                                                            %>
                                                            <input type='text' class="form-control" id="incident_create_subject" name="incident_create_subject" value="<%=value%>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>Insert Subject Incident Record Value</label>
                                                        <div class='input-group'>
                                                            <select class="form-control" name="create_incident_subject_fields" id="create_incident_subject_fields">
                                                                <option value="<$incident_id$>">Incident ID</option>
                                                                <option value="<$incident_time$>">Incident Time</option>
                                                                <option value="<$caller$>">Caller</option>
                                                                <option value="<$caller_group$>">Caller Group</option>
                                                                <option value="<$location$>">Location</option>
                                                                <option value="<$department$>">Department</option>
                                                                <option value="<$site$>">Site</option>
                                                                <option value="<$company$>">Company</option>
                                                                <option value="<$category$>">Category</option>
                                                                <option value="<$subcategory$>">Subcategory</option>
                                                                <option value="<$priority$>">Priority</option>
                                                                <option value="<$description$>">Description</option>
                                                                <option value="<$create_date$>">Create Date</option>
                                                                <option value="<$create_by$>">Created By</option>
                                                                <option value="<$contact_method$>">Contact Method</option>
                                                                <option value="<$state$>">State</option>
                                                                <option value="<$state_date$>">State Date</option>
                                                                <option value="<$assigned_group$>">Assigned Group</option>
                                                                <option value="<$assigned_to$>">Assigned To</option>
                                                                <option value="<$notes$>">Notes</option>
                                                                <option value="<$desk_notes$>">Desk Notes</option>
                                                                <option value="<$closed_date$>">Closed Date</option>
                                                                <option value="<$closed_reason$>">Closed Reason</option>
                                                                <option value="<$pending_date$>">Pending Date</option>
                                                                <option value="<$pending_reason$>">Pending Reason</option>                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-1 col-lg-2 col-12">
                                                    <div class='input-group'>
                                                        <label>&nbsp;</label>
                                                        <button type="button" onclick="add_get_select_value('create_incident_subject_fields','incident_create_subject')" class="btn btn-primary mr-1">
                                                            <i class="ft-plus"></i> Add Field
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                                
                                            <div class="row">    
                                                <div class="col-xl-8 col-lg-6 col-12">
                                                    <div class='input-group'>
                                                        <label>Create Incident Email Body</label>
                                                        <div class='input-group'>
                                                            <%
                                                            value = "";
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("caller_incident_create"))
                                                                {
                                                                    value = this_setting[4];
                                                                }
                                                            }
                                                            %>
                                                            <textarea rows="5" class="form-control" id="incident_create_body" name="incident_create_body" ><%=value%></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>Insert Body Incident Record Value</label>
                                                        <div class='input-group'>
                                                            <select class="form-control" name="create_incident_body_fields" id="create_incident_body_fields">
                                                                <option value="<$incident_id$>">Incident ID</option>
                                                                <option value="<$incident_time$>">Incident Time</option>
                                                                <option value="<$caller$>">Caller</option>
                                                                <option value="<$caller_group$>">Caller Group</option>
                                                                <option value="<$location$>">Location</option>
                                                                <option value="<$department$>">Department</option>
                                                                <option value="<$site$>">Site</option>
                                                                <option value="<$company$>">Company</option>
                                                                <option value="<$category$>">Category</option>
                                                                <option value="<$subcategory$>">Subcategory</option>
                                                                <option value="<$priority$>">Priority</option>
                                                                <option value="<$description$>">Description</option>
                                                                <option value="<$create_date$>">Create Date</option>
                                                                <option value="<$create_by$>">Created By</option>
                                                                <option value="<$contact_method$>">Contact Method</option>
                                                                <option value="<$state$>">State</option>
                                                                <option value="<$state_date$>">State Date</option>
                                                                <option value="<$assigned_group$>">Assigned Group</option>
                                                                <option value="<$assigned_to$>">Assigned To</option>
                                                                <option value="<$notes$>">Notes</option>
                                                                <option value="<$desk_notes$>">Desk Notes</option>
                                                                <option value="<$closed_date$>">Closed Date</option>
                                                                <option value="<$closed_reason$>">Closed Reason</option>
                                                                <option value="<$pending_date$>">Pending Date</option>
                                                                <option value="<$pending_reason$>">Pending Reason</option>                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-1 col-lg-2 col-12">
                                                    <div class='input-group'>
                                                        <label>&nbsp;</label>
                                                        <button type="button" onclick="add_get_select_value('create_incident_body_fields','incident_create_body')" class="btn btn-primary mr-1">
                                                            <i class="ft-plus"></i> Add Field
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12"><hr></div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-xl-8 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>
                                                            Update Incident Email Subject
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <button type="button" onclick="test_email('incident','incident_update_subject','incident_update_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                        </label>
                                                        <div class='input-group'>
                                                            <%
                                                            value = "";
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("caller_incident_update"))
                                                                {
                                                                    value = this_setting[3];
                                                                }
                                                            }
                                                            %>
                                                            <input type='text' class="form-control" id="incident_update_subject" name="incident_update_subject" value="<%=value%>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>Insert Subject Incident Record Value</label>
                                                        <div class='input-group'>
                                                            <select class="form-control" name="update_incident_subject_fields" id="update_incident_subject_fields">
                                                                <option value="<$incident_id$>">Incident ID</option>
                                                                <option value="<$incident_time$>">Incident Time</option>
                                                                <option value="<$caller$>">Caller</option>
                                                                <option value="<$caller_group$>">Caller Group</option>
                                                                <option value="<$location$>">Location</option>
                                                                <option value="<$department$>">Department</option>
                                                                <option value="<$site$>">Site</option>
                                                                <option value="<$company$>">Company</option>
                                                                <option value="<$category$>">Category</option>
                                                                <option value="<$subcategory$>">Subcategory</option>
                                                                <option value="<$priority$>">Priority</option>
                                                                <option value="<$description$>">Description</option>
                                                                <option value="<$create_date$>">Create Date</option>
                                                                <option value="<$create_by$>">Created By</option>
                                                                <option value="<$contact_method$>">Contact Method</option>
                                                                <option value="<$state$>">State</option>
                                                                <option value="<$state_date$>">State Date</option>
                                                                <option value="<$assigned_group$>">Assigned Group</option>
                                                                <option value="<$assigned_to$>">Assigned To</option>
                                                                <option value="<$notes$>">Notes</option>
                                                                <option value="<$desk_notes$>">Desk Notes</option>
                                                                <option value="<$closed_date$>">Closed Date</option>
                                                                <option value="<$closed_reason$>">Closed Reason</option>
                                                                <option value="<$pending_date$>">Pending Date</option>
                                                                <option value="<$pending_reason$>">Pending Reason</option>                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-1 col-lg-2 col-12">
                                                    <div class='input-group'>
                                                        <label>&nbsp;</label>
                                                        <button type="button" onclick="add_get_select_value('update_incident_subject_fields','incident_update_subject')" class="btn btn-primary mr-1">
                                                            <i class="ft-plus"></i> Add Field
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                                
                                            <div class="row">    
                                                <div class="col-xl-8 col-lg-6 col-12">
                                                    <div class='input-group'>
                                                        <label>Update Incident Email Body</label>
                                                        <div class='input-group'>
                                                            <%
                                                            value = "";
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("caller_incident_update"))
                                                                {
                                                                    value = this_setting[4];
                                                                }
                                                            }
                                                            %>
                                                            <textarea rows="5" class="form-control" id="incident_update_body" name="incident_update_body" ><%=value%></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>Insert Body Incident Record Value</label>
                                                        <div class='input-group'>
                                                            <select class="form-control" name="update_incident_body_fields" id="update_incident_body_fields">
                                                                <option value="<$incident_id$>">Incident ID</option>
                                                                <option value="<$incident_time$>">Incident Time</option>
                                                                <option value="<$caller$>">Caller</option>
                                                                <option value="<$caller_group$>">Caller Group</option>
                                                                <option value="<$location$>">Location</option>
                                                                <option value="<$department$>">Department</option>
                                                                <option value="<$site$>">Site</option>
                                                                <option value="<$company$>">Company</option>
                                                                <option value="<$category$>">Category</option>
                                                                <option value="<$subcategory$>">Subcategory</option>
                                                                <option value="<$priority$>">Priority</option>
                                                                <option value="<$description$>">Description</option>
                                                                <option value="<$create_date$>">Create Date</option>
                                                                <option value="<$create_by$>">Created By</option>
                                                                <option value="<$contact_method$>">Contact Method</option>
                                                                <option value="<$state$>">State</option>
                                                                <option value="<$state_date$>">State Date</option>
                                                                <option value="<$assigned_group$>">Assigned Group</option>
                                                                <option value="<$assigned_to$>">Assigned To</option>
                                                                <option value="<$notes$>">Notes</option>
                                                                <option value="<$desk_notes$>">Desk Notes</option>
                                                                <option value="<$closed_date$>">Closed Date</option>
                                                                <option value="<$closed_reason$>">Closed Reason</option>
                                                                <option value="<$pending_date$>">Pending Date</option>
                                                                <option value="<$pending_reason$>">Pending Reason</option>                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-1 col-lg-2 col-12">
                                                    <div class='input-group'>
                                                        <label>&nbsp;</label>
                                                        <button type="button" onclick="add_get_select_value('update_incident_body_fields','incident_update_body')" class="btn btn-primary mr-1">
                                                            <i class="ft-plus"></i> Add Field
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12"><hr></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-8 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>
                                                            Resolve Incident Email Subject
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <button type="button" onclick="test_email('incident','incident_resolve_subject','incident_resolve_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                        </label>
                                                        <div class='input-group'>
                                                            <%
                                                            value = "";
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("caller_incident_resolve"))
                                                                {
                                                                    value = this_setting[3];
                                                                }
                                                            }
                                                            %>
                                                            <input type='text' class="form-control" id="incident_resolve_subject" name="incident_resolve_subject" value="<%=value%>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>Insert Subject Incident Record Value</label>
                                                        <div class='input-group'>
                                                            <select class="form-control" name="resolve_incident_subject_fields" id="resolve_incident_subject_fields">
                                                                <option value="<$incident_id$>">Incident ID</option>
                                                                <option value="<$incident_time$>">Incident Time</option>
                                                                <option value="<$caller$>">Caller</option>
                                                                <option value="<$caller_group$>">Caller Group</option>
                                                                <option value="<$location$>">Location</option>
                                                                <option value="<$department$>">Department</option>
                                                                <option value="<$site$>">Site</option>
                                                                <option value="<$company$>">Company</option>
                                                                <option value="<$category$>">Category</option>
                                                                <option value="<$subcategory$>">Subcategory</option>
                                                                <option value="<$priority$>">Priority</option>
                                                                <option value="<$description$>">Description</option>
                                                                <option value="<$create_date$>">Create Date</option>
                                                                <option value="<$create_by$>">Created By</option>
                                                                <option value="<$contact_method$>">Contact Method</option>
                                                                <option value="<$state$>">State</option>
                                                                <option value="<$state_date$>">State Date</option>
                                                                <option value="<$assigned_group$>">Assigned Group</option>
                                                                <option value="<$assigned_to$>">Assigned To</option>
                                                                <option value="<$notes$>">Notes</option>
                                                                <option value="<$desk_notes$>">Desk Notes</option>
                                                                <option value="<$closed_date$>">Closed Date</option>
                                                                <option value="<$closed_reason$>">Closed Reason</option>
                                                                <option value="<$pending_date$>">Pending Date</option>
                                                                <option value="<$pending_reason$>">Pending Reason</option>                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-1 col-lg-2 col-12">
                                                    <div class='input-group'>
                                                        <label>&nbsp;</label>
                                                        <button type="button" onclick="add_get_select_value('resolve_incident_subject_fields','incident_resolve_subject')" class="btn btn-primary mr-1">
                                                            <i class="ft-plus"></i> Add Field
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                                
                                            <div class="row">    
                                                <div class="col-xl-8 col-lg-6 col-12">
                                                    <div class='input-group'>
                                                        <label>Resolve Incident Email Body</label>
                                                        <div class='input-group'>
                                                            <%
                                                            value = "";
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("caller_incident_resolve"))
                                                                {
                                                                    value = this_setting[4];
                                                                }
                                                            }
                                                            %>
                                                            <textarea rows="5" class="form-control" id="incident_resolve_body" name="incident_resolve_body" ><%=value%></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>Insert Body Incident Record Value</label>
                                                        <div class='input-group'>
                                                            <select class="form-control" name="resolve_incident_body_fields" id="resolve_incident_body_fields">
                                                                <option value="<$incident_id$>">Incident ID</option>
                                                                <option value="<$incident_time$>">Incident Time</option>
                                                                <option value="<$caller$>">Caller</option>
                                                                <option value="<$caller_group$>">Caller Group</option>
                                                                <option value="<$location$>">Location</option>
                                                                <option value="<$department$>">Department</option>
                                                                <option value="<$site$>">Site</option>
                                                                <option value="<$company$>">Company</option>
                                                                <option value="<$category$>">Category</option>
                                                                <option value="<$subcategory$>">Subcategory</option>
                                                                <option value="<$priority$>">Priority</option>
                                                                <option value="<$description$>">Description</option>
                                                                <option value="<$create_date$>">Create Date</option>
                                                                <option value="<$create_by$>">Created By</option>
                                                                <option value="<$contact_method$>">Contact Method</option>
                                                                <option value="<$state$>">State</option>
                                                                <option value="<$state_date$>">State Date</option>
                                                                <option value="<$assigned_group$>">Assigned Group</option>
                                                                <option value="<$assigned_to$>">Assigned To</option>
                                                                <option value="<$notes$>">Notes</option>
                                                                <option value="<$desk_notes$>">Desk Notes</option>
                                                                <option value="<$closed_date$>">Closed Date</option>
                                                                <option value="<$closed_reason$>">Closed Reason</option>
                                                                <option value="<$pending_date$>">Pending Date</option>
                                                                <option value="<$pending_reason$>">Pending Reason</option>                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-1 col-lg-2 col-12">
                                                    <div class='input-group'>
                                                        <label>&nbsp;</label>
                                                        <button type="button" onclick="add_get_select_value('resolve_incident_body_fields','incident_resolve_body')" class="btn btn-primary mr-1">
                                                            <i class="ft-plus"></i> Add Field
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12"><hr></div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-xl-8 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>
                                                            Close Incident Email Subject
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <button type="button" onclick="test_email('incident','incident_close_subject','incident_close_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                        </label>
                                                        <div class='input-group'>
                                                            <%
                                                            value = "";
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("caller_incident_close"))
                                                                {
                                                                    value = this_setting[3];
                                                                }
                                                            }
                                                            %>
                                                            <input type='text' class="form-control" id="incident_close_subject" name="incident_close_subject" value="<%=value%>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>Insert Subject Incident Record Value</label>
                                                        <div class='input-group'>
                                                            <select class="form-control" name="close_incident_subject_fields" id="close_incident_subject_fields">
                                                                <option value="<$incident_id$>">Incident ID</option>
                                                                <option value="<$incident_time$>">Incident Time</option>
                                                                <option value="<$caller$>">Caller</option>
                                                                <option value="<$caller_group$>">Caller Group</option>
                                                                <option value="<$location$>">Location</option>
                                                                <option value="<$department$>">Department</option>
                                                                <option value="<$site$>">Site</option>
                                                                <option value="<$company$>">Company</option>
                                                                <option value="<$category$>">Category</option>
                                                                <option value="<$subcategory$>">Subcategory</option>
                                                                <option value="<$priority$>">Priority</option>
                                                                <option value="<$description$>">Description</option>
                                                                <option value="<$create_date$>">Create Date</option>
                                                                <option value="<$create_by$>">Created By</option>
                                                                <option value="<$contact_method$>">Contact Method</option>
                                                                <option value="<$state$>">State</option>
                                                                <option value="<$state_date$>">State Date</option>
                                                                <option value="<$assigned_group$>">Assigned Group</option>
                                                                <option value="<$assigned_to$>">Assigned To</option>
                                                                <option value="<$notes$>">Notes</option>
                                                                <option value="<$desk_notes$>">Desk Notes</option>
                                                                <option value="<$closed_date$>">Closed Date</option>
                                                                <option value="<$closed_reason$>">Closed Reason</option>
                                                                <option value="<$pending_date$>">Pending Date</option>
                                                                <option value="<$pending_reason$>">Pending Reason</option>                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-1 col-lg-2 col-12">
                                                    <div class='input-group'>
                                                        <label>&nbsp;</label>
                                                        <button type="button" onclick="add_get_select_value('close_incident_subject_fields','incident_close_subject')" class="btn btn-primary mr-1">
                                                            <i class="ft-plus"></i> Add Field
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                                
                                            <div class="row">    
                                                <div class="col-xl-8 col-lg-6 col-12">
                                                    <div class='input-group'>
                                                        <label>Close Incident Email Body</label>
                                                        <div class='input-group'>
                                                            <%
                                                            value = "";
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("caller_incident_close"))
                                                                {
                                                                    value = this_setting[4];
                                                                }
                                                            }
                                                            %>
                                                            <textarea rows="5" class="form-control" id="incident_close_body" name="incident_close_body" ><%=value%></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>Insert Body Incident Record Value</label>
                                                        <div class='input-group'>
                                                            <select class="form-control" name="close_incident_body_fields" id="close_incident_body_fields">
                                                                <option value="<$incident_id$>">Incident ID</option>
                                                                <option value="<$incident_time$>">Incident Time</option>
                                                                <option value="<$caller$>">Caller</option>
                                                                <option value="<$caller_group$>">Caller Group</option>
                                                                <option value="<$location$>">Location</option>
                                                                <option value="<$department$>">Department</option>
                                                                <option value="<$site$>">Site</option>
                                                                <option value="<$company$>">Company</option>
                                                                <option value="<$category$>">Category</option>
                                                                <option value="<$subcategory$>">Subcategory</option>
                                                                <option value="<$priority$>">Priority</option>
                                                                <option value="<$description$>">Description</option>
                                                                <option value="<$create_date$>">Create Date</option>
                                                                <option value="<$create_by$>">Created By</option>
                                                                <option value="<$contact_method$>">Contact Method</option>
                                                                <option value="<$state$>">State</option>
                                                                <option value="<$state_date$>">State Date</option>
                                                                <option value="<$assigned_group$>">Assigned Group</option>
                                                                <option value="<$assigned_to$>">Assigned To</option>
                                                                <option value="<$notes$>">Notes</option>
                                                                <option value="<$desk_notes$>">Desk Notes</option>
                                                                <option value="<$closed_date$>">Closed Date</option>
                                                                <option value="<$closed_reason$>">Closed Reason</option>
                                                                <option value="<$pending_date$>">Pending Date</option>
                                                                <option value="<$pending_reason$>">Pending Reason</option>                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-1 col-lg-2 col-12">
                                                    <div class='input-group'>
                                                        <label>&nbsp;</label>
                                                        <button type="button" onclick="add_get_select_value('close_incident_body_fields','incident_close_body')" class="btn btn-primary mr-1">
                                                            <i class="ft-plus"></i> Add Field
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12"><hr></div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-12">
                                                    <h3>Request Email Notification Formats</h3>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-8 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>
                                                            Create Request Email Subject
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <button type="button" onclick="test_email('request','request_create_subject','request_create_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                        </label>
                                                        <div class='input-group'>
                                                            <%
                                                            value = "";
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("request_created_by_create"))
                                                                {
                                                                    value = this_setting[3];
                                                                }
                                                            }
                                                            %>
                                                            <input type='text' class="form-control" id="request_create_subject" name="request_create_subject" value="<%=value%>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>Insert Subject Request Record Value</label>
                                                        <div class='input-group'>
                                                            <select class="form-control" name="create_request_subject_fields" id="create_request_subject_fields">
                                                                <option value="<$request_id$>">Request ID</option>
                                                                <option value="<$rev$>">Revision Number</option>
                                                                <option value="<$rev_date$>">Revision Date</option>
                                                                <option value="<$rev_by_user_id$>">Revision by</option>
                                                                <option value="<$service_catalog$>">Service Catalog</option>
                                                                <option value="<$service_catalog_item$>">Service Catalog Item</option>
                                                                <option value="<$create_date$>">Create Date</option>
                                                                <option value="<$request_date$>">Request Date</option>
                                                                <option value="<$due_date$>">Due Date</option>
                                                                <option value="<$resolve_date$>">Resolved Date</option>
                                                                <option value="<$closed_date$>">Closed Date</option>
                                                                <option value="<$assigned_group$>">Assigned Group</option>
                                                                <option value="<$assigned_to$>">Assigned To</option>
                                                                <option value="<$create_by$>">Created By</option>
                                                                <option value="<$requested_for_id$>">Requested For</option>
                                                                <option value="<$requested_for_group$>">Requested For Group</option>
                                                                <option value="<$location$>">Location</option>
                                                                <option value="<$department$>">Department</option>
                                                                <option value="<$site$>">Site</option>
                                                                <option value="<$company$>">Company</option>
                                                                <option value="<$impact$>">Impact</option>
                                                                <option value="<$urgency$>">Urgency</option>
                                                                <option value="<$priority$>">Priority</option>
                                                                <option value="<$state$>">State</option>
                                                                <option value="<$contact_method$>">Contact Method</option>
                                                                <option value="<$approval$>">Approval</option>
                                                                <option value="<$price$>">Price</option>
                                                                <option value="<$notes$>">Notes</option>
                                                                <option value="<$desk_notes$>">Desk Notes</option>
                                                                <option value="<$description$>">Description</option>
                                                                <option value="<$closed_reason$>">Closed Reason</option>
                                                                <option value="<$required_info$>">Required Info</option>
                                                                <option value="<$required_info_response$>">Required Info Response</option>
                                                                <option value="<$optional_info$>">Optional Info</option>
                                                                <option value="<$optional_info_response$>">Optional Info Response</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-1 col-lg-2 col-12">
                                                    <div class='input-group'>
                                                        <label>&nbsp;</label>
                                                        <button type="button" onclick="add_get_select_value('create_request_subject_fields','request_create_subject')" class="btn btn-primary mr-1">
                                                            <i class="ft-plus"></i> Add Field
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                                
                                            <div class="row">    
                                                <div class="col-xl-8 col-lg-6 col-12">
                                                    <div class='input-group'>
                                                        <label>Create Request Email Body</label>
                                                        <div class='input-group'>
                                                            <%
                                                            value = "";
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("request_created_by_create"))
                                                                {
                                                                    value = this_setting[4];
                                                                }
                                                            }
                                                            %>
                                                            <textarea rows="5" class="form-control" id="request_create_body" name="request_create_body" ><%=value%></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>Insert Body Request Record Value</label>
                                                        <div class='input-group'>
                                                            <select class="form-control" name="create_request_body_fields" id="create_request_body_fields">
                                                                <option value="<$request_id$>">Request ID</option>
                                                                <option value="<$rev$>">Revision Number</option>
                                                                <option value="<$rev_date$>">Revision Date</option>
                                                                <option value="<$rev_by_user_id$>">Revision by</option>
                                                                <option value="<$service_catalog$>">Service Catalog</option>
                                                                <option value="<$service_catalog_item$>">Service Catalog Item</option>
                                                                <option value="<$create_date$>">Create Date</option>
                                                                <option value="<$request_date$>">Request Date</option>
                                                                <option value="<$due_date$>">Due Date</option>
                                                                <option value="<$resolve_date$>">Resolved Date</option>
                                                                <option value="<$closed_date$>">Closed Date</option>
                                                                <option value="<$assigned_group$>">Assigned Group</option>
                                                                <option value="<$assigned_to$>">Assigned To</option>
                                                                <option value="<$create_by$>">Created By</option>
                                                                <option value="<$requested_for_id$>">Requested For</option>
                                                                <option value="<$requested_for_group$>">Requested For Group</option>
                                                                <option value="<$location$>">Location</option>
                                                                <option value="<$department$>">Department</option>
                                                                <option value="<$site$>">Site</option>
                                                                <option value="<$company$>">Company</option>
                                                                <option value="<$impact$>">Impact</option>
                                                                <option value="<$urgency$>">Urgency</option>
                                                                <option value="<$priority$>">Priority</option>
                                                                <option value="<$state$>">State</option>
                                                                <option value="<$contact_method$>">Contact Method</option>
                                                                <option value="<$approval$>">Approval</option>
                                                                <option value="<$price$>">Price</option>
                                                                <option value="<$notes$>">Notes</option>
                                                                <option value="<$desk_notes$>">Desk Notes</option>
                                                                <option value="<$description$>">Description</option>
                                                                <option value="<$closed_reason$>">Closed Reason</option>
                                                                <option value="<$required_info$>">Required Info</option>
                                                                <option value="<$required_info_response$>">Required Info Response</option>
                                                                <option value="<$optional_info$>">Optional Info</option>
                                                                <option value="<$optional_info_response$>">Optional Info Response</option>                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-1 col-lg-2 col-12">
                                                    <div class='input-group'>
                                                        <label>&nbsp;</label>
                                                        <button type="button" onclick="add_get_select_value('create_request_body_fields','request_create_body')" class="btn btn-primary mr-1">
                                                            <i class="ft-plus"></i> Add Field
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12"><hr></div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-xl-8 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>
                                                            Update Request Email Subject
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <button type="button" onclick="test_email('request','request_update_subject','request_update_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                        </label>
                                                        <div class='input-group'>
                                                            <%
                                                            value = "";
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("request_created_by_update"))
                                                                {
                                                                    value = this_setting[3];
                                                                }
                                                            }
                                                            %>
                                                            <input type='text' class="form-control" id="request_update_subject" name="request_update_subject" value="<%=value%>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>Insert Subject Request Record Value</label>
                                                        <div class='input-group'>
                                                            <select class="form-control" name="update_request_subject_fields" id="update_request_subject_fields">
                                                                <option value="<$request_id$>">Request ID</option>
                                                                <option value="<$rev$>">Revision Number</option>
                                                                <option value="<$rev_date$>">Revision Date</option>
                                                                <option value="<$rev_by_user_id$>">Revision by</option>
                                                                <option value="<$service_catalog$>">Service Catalog</option>
                                                                <option value="<$service_catalog_item$>">Service Catalog Item</option>
                                                                <option value="<$create_date$>">Create Date</option>
                                                                <option value="<$request_date$>">Request Date</option>
                                                                <option value="<$due_date$>">Due Date</option>
                                                                <option value="<$resolve_date$>">Resolved Date</option>
                                                                <option value="<$closed_date$>">Closed Date</option>
                                                                <option value="<$assigned_group$>">Assigned Group</option>
                                                                <option value="<$assigned_to$>">Assigned To</option>
                                                                <option value="<$create_by$>">Created By</option>
                                                                <option value="<$requested_for_id$>">Requested For</option>
                                                                <option value="<$requested_for_group$>">Requested For Group</option>
                                                                <option value="<$location$>">Location</option>
                                                                <option value="<$department$>">Department</option>
                                                                <option value="<$site$>">Site</option>
                                                                <option value="<$company$>">Company</option>
                                                                <option value="<$impact$>">Impact</option>
                                                                <option value="<$urgency$>">Urgency</option>
                                                                <option value="<$priority$>">Priority</option>
                                                                <option value="<$state$>">State</option>
                                                                <option value="<$contact_method$>">Contact Method</option>
                                                                <option value="<$approval$>">Approval</option>
                                                                <option value="<$price$>">Price</option>
                                                                <option value="<$notes$>">Notes</option>
                                                                <option value="<$desk_notes$>">Desk Notes</option>
                                                                <option value="<$description$>">Description</option>
                                                                <option value="<$closed_reason$>">Closed Reason</option>
                                                                <option value="<$required_info$>">Required Info</option>
                                                                <option value="<$required_info_response$>">Required Info Response</option>
                                                                <option value="<$optional_info$>">Optional Info</option>
                                                                <option value="<$optional_info_response$>">Optional Info Response</option>                                                               
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-1 col-lg-2 col-12">
                                                    <div class='input-group'>
                                                        <label>&nbsp;</label>
                                                        <button type="button" onclick="add_get_select_value('update_request_subject_fields','request_update_subject')" class="btn btn-primary mr-1">
                                                            <i class="ft-plus"></i> Add Field
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                                
                                            <div class="row">    
                                                <div class="col-xl-8 col-lg-6 col-12">
                                                    <div class='input-group'>
                                                        <label>Update Request Email Body</label>
                                                        <div class='input-group'>
                                                            <%
                                                            value = "";
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("request_created_by_update"))
                                                                {
                                                                    value = this_setting[4];
                                                                }
                                                            }
                                                            %>
                                                            <textarea rows="5" class="form-control" id="request_update_body" name="request_update_body" ><%=value%></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>Insert Body Request Record Value</label>
                                                        <div class='input-group'>
                                                            <select class="form-control" name="update_request_body_fields" id="update_request_body_fields">
                                                                <option value="<$request_id$>">Request ID</option>
                                                                <option value="<$rev$>">Revision Number</option>
                                                                <option value="<$rev_date$>">Revision Date</option>
                                                                <option value="<$rev_by_user_id$>">Revision by</option>
                                                                <option value="<$service_catalog$>">Service Catalog</option>
                                                                <option value="<$service_catalog_item$>">Service Catalog Item</option>
                                                                <option value="<$create_date$>">Create Date</option>
                                                                <option value="<$request_date$>">Request Date</option>
                                                                <option value="<$due_date$>">Due Date</option>
                                                                <option value="<$resolve_date$>">Resolved Date</option>
                                                                <option value="<$closed_date$>">Closed Date</option>
                                                                <option value="<$assigned_group$>">Assigned Group</option>
                                                                <option value="<$assigned_to$>">Assigned To</option>
                                                                <option value="<$create_by$>">Created By</option>
                                                                <option value="<$requested_for_id$>">Requested For</option>
                                                                <option value="<$requested_for_group$>">Requested For Group</option>
                                                                <option value="<$location$>">Location</option>
                                                                <option value="<$department$>">Department</option>
                                                                <option value="<$site$>">Site</option>
                                                                <option value="<$company$>">Company</option>
                                                                <option value="<$impact$>">Impact</option>
                                                                <option value="<$urgency$>">Urgency</option>
                                                                <option value="<$priority$>">Priority</option>
                                                                <option value="<$state$>">State</option>
                                                                <option value="<$contact_method$>">Contact Method</option>
                                                                <option value="<$approval$>">Approval</option>
                                                                <option value="<$price$>">Price</option>
                                                                <option value="<$notes$>">Notes</option>
                                                                <option value="<$desk_notes$>">Desk Notes</option>
                                                                <option value="<$description$>">Description</option>
                                                                <option value="<$closed_reason$>">Closed Reason</option>
                                                                <option value="<$required_info$>">Required Info</option>
                                                                <option value="<$required_info_response$>">Required Info Response</option>
                                                                <option value="<$optional_info$>">Optional Info</option>
                                                                <option value="<$optional_info_response$>">Optional Info Response</option>                                                               
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-1 col-lg-2 col-12">
                                                    <div class='input-group'>
                                                        <label>&nbsp;</label>
                                                        <button type="button" onclick="add_get_select_value('update_request_body_fields','request_update_body')" class="btn btn-primary mr-1">
                                                            <i class="ft-plus"></i> Add Field
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12"><hr></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-8 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>
                                                            Resolve Request Email Subject
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <button type="button" onclick="test_email('request','request_resolve_subject','request_resolve_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                        </label>
                                                        <div class='input-group'>
                                                            <%
                                                            value = "";
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("request_created_by_resolve"))
                                                                {
                                                                    value = this_setting[3];
                                                                }
                                                            }
                                                            %>
                                                            <input type='text' class="form-control" id="request_resolve_subject" name="request_resolve_subject" value="<%=value%>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>Insert Subject Request Record Value</label>
                                                        <div class='input-group'>
                                                            <select class="form-control" name="resolve_request_subject_fields" id="resolve_request_subject_fields">
                                                                <option value="<$request_id$>">Request ID</option>
                                                                <option value="<$rev$>">Revision Number</option>
                                                                <option value="<$rev_date$>">Revision Date</option>
                                                                <option value="<$rev_by_user_id$>">Revision by</option>
                                                                <option value="<$service_catalog$>">Service Catalog</option>
                                                                <option value="<$service_catalog_item$>">Service Catalog Item</option>
                                                                <option value="<$create_date$>">Create Date</option>
                                                                <option value="<$request_date$>">Request Date</option>
                                                                <option value="<$due_date$>">Due Date</option>
                                                                <option value="<$resolve_date$>">Resolved Date</option>
                                                                <option value="<$closed_date$>">Closed Date</option>
                                                                <option value="<$assigned_group$>">Assigned Group</option>
                                                                <option value="<$assigned_to$>">Assigned To</option>
                                                                <option value="<$create_by$>">Created By</option>
                                                                <option value="<$requested_for_id$>">Requested For</option>
                                                                <option value="<$requested_for_group$>">Requested For Group</option>
                                                                <option value="<$location$>">Location</option>
                                                                <option value="<$department$>">Department</option>
                                                                <option value="<$site$>">Site</option>
                                                                <option value="<$company$>">Company</option>
                                                                <option value="<$impact$>">Impact</option>
                                                                <option value="<$urgency$>">Urgency</option>
                                                                <option value="<$priority$>">Priority</option>
                                                                <option value="<$state$>">State</option>
                                                                <option value="<$contact_method$>">Contact Method</option>
                                                                <option value="<$approval$>">Approval</option>
                                                                <option value="<$price$>">Price</option>
                                                                <option value="<$notes$>">Notes</option>
                                                                <option value="<$desk_notes$>">Desk Notes</option>
                                                                <option value="<$description$>">Description</option>
                                                                <option value="<$closed_reason$>">Closed Reason</option>
                                                                <option value="<$required_info$>">Required Info</option>
                                                                <option value="<$required_info_response$>">Required Info Response</option>
                                                                <option value="<$optional_info$>">Optional Info</option>
                                                                <option value="<$optional_info_response$>">Optional Info Response</option>                                                               
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-1 col-lg-2 col-12">
                                                    <div class='input-group'>
                                                        <label>&nbsp;</label>
                                                        <button type="button" onclick="add_get_select_value('resolve_request_subject_fields','request_resolve_subject')" class="btn btn-primary mr-1">
                                                            <i class="ft-plus"></i> Add Field
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                                
                                            <div class="row">    
                                                <div class="col-xl-8 col-lg-6 col-12">
                                                    <div class='input-group'>
                                                        <label>Resolve Request Email Body</label>
                                                        <div class='input-group'>
                                                            <%
                                                            value = "";
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("request_created_by_resolve"))
                                                                {
                                                                    value = this_setting[4];
                                                                }
                                                            }
                                                            %>
                                                            <textarea rows="5" class="form-control" id="request_resolve_body" name="request_resolve_body" ><%=value%></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>Insert Body Request Record Value</label>
                                                        <div class='input-group'>
                                                            <select class="form-control" name="resolve_request_body_fields" id="resolve_request_body_fields">
                                                                <option value="<$request_id$>">Request ID</option>
                                                                <option value="<$rev$>">Revision Number</option>
                                                                <option value="<$rev_date$>">Revision Date</option>
                                                                <option value="<$rev_by_user_id$>">Revision by</option>
                                                                <option value="<$service_catalog$>">Service Catalog</option>
                                                                <option value="<$service_catalog_item$>">Service Catalog Item</option>
                                                                <option value="<$create_date$>">Create Date</option>
                                                                <option value="<$request_date$>">Request Date</option>
                                                                <option value="<$due_date$>">Due Date</option>
                                                                <option value="<$resolve_date$>">Resolved Date</option>
                                                                <option value="<$closed_date$>">Closed Date</option>
                                                                <option value="<$assigned_group$>">Assigned Group</option>
                                                                <option value="<$assigned_to$>">Assigned To</option>
                                                                <option value="<$create_by$>">Created By</option>
                                                                <option value="<$requested_for_id$>">Requested For</option>
                                                                <option value="<$requested_for_group$>">Requested For Group</option>
                                                                <option value="<$location$>">Location</option>
                                                                <option value="<$department$>">Department</option>
                                                                <option value="<$site$>">Site</option>
                                                                <option value="<$company$>">Company</option>
                                                                <option value="<$impact$>">Impact</option>
                                                                <option value="<$urgency$>">Urgency</option>
                                                                <option value="<$priority$>">Priority</option>
                                                                <option value="<$state$>">State</option>
                                                                <option value="<$contact_method$>">Contact Method</option>
                                                                <option value="<$approval$>">Approval</option>
                                                                <option value="<$price$>">Price</option>
                                                                <option value="<$notes$>">Notes</option>
                                                                <option value="<$desk_notes$>">Desk Notes</option>
                                                                <option value="<$description$>">Description</option>
                                                                <option value="<$closed_reason$>">Closed Reason</option>
                                                                <option value="<$required_info$>">Required Info</option>
                                                                <option value="<$required_info_response$>">Required Info Response</option>
                                                                <option value="<$optional_info$>">Optional Info</option>
                                                                <option value="<$optional_info_response$>">Optional Info Response</option>                                                               
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-1 col-lg-2 col-12">
                                                    <div class='input-group'>
                                                        <label>&nbsp;</label>
                                                        <button type="button" onclick="add_get_select_value('resolve_request_body_fields','request_resolve_body')" class="btn btn-primary mr-1">
                                                            <i class="ft-plus"></i> Add Field
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>    
                                                        
                                            <div class="row">
                                                <div class="col-12"><hr></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-8 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>
                                                            Close Request Email Subject
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <button type="button" onclick="test_email('request','request_close_subject','request_close_body')" class="btn btn-outline-primary btn-sm"><i class="ft-mail"></i>&nbsp;Send Test Email using this format</button>
                                                        </label>
                                                        <div class='input-group'>
                                                            <%
                                                            value = "";
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("request_created_by_close"))
                                                                {
                                                                    value = this_setting[3];
                                                                }
                                                            }
                                                            %>
                                                            <input type='text' class="form-control" id="request_close_subject" name="request_close_subject" value="<%=value%>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>Insert Subject Request Record Value</label>
                                                        <div class='input-group'>
                                                            <select class="form-control" name="close_request_subject_fields" id="close_request_subject_fields">
                                                                <option value="<$request_id$>">Request ID</option>
                                                                <option value="<$rev$>">Revision Number</option>
                                                                <option value="<$rev_date$>">Revision Date</option>
                                                                <option value="<$rev_by_user_id$>">Revision by</option>
                                                                <option value="<$service_catalog$>">Service Catalog</option>
                                                                <option value="<$service_catalog_item$>">Service Catalog Item</option>
                                                                <option value="<$create_date$>">Create Date</option>
                                                                <option value="<$request_date$>">Request Date</option>
                                                                <option value="<$due_date$>">Due Date</option>
                                                                <option value="<$resolve_date$>">Resolved Date</option>
                                                                <option value="<$closed_date$>">Closed Date</option>
                                                                <option value="<$assigned_group$>">Assigned Group</option>
                                                                <option value="<$assigned_to$>">Assigned To</option>
                                                                <option value="<$create_by$>">Created By</option>
                                                                <option value="<$requested_for_id$>">Requested For</option>
                                                                <option value="<$requested_for_group$>">Requested For Group</option>
                                                                <option value="<$location$>">Location</option>
                                                                <option value="<$department$>">Department</option>
                                                                <option value="<$site$>">Site</option>
                                                                <option value="<$company$>">Company</option>
                                                                <option value="<$impact$>">Impact</option>
                                                                <option value="<$urgency$>">Urgency</option>
                                                                <option value="<$priority$>">Priority</option>
                                                                <option value="<$state$>">State</option>
                                                                <option value="<$contact_method$>">Contact Method</option>
                                                                <option value="<$approval$>">Approval</option>
                                                                <option value="<$price$>">Price</option>
                                                                <option value="<$notes$>">Notes</option>
                                                                <option value="<$desk_notes$>">Desk Notes</option>
                                                                <option value="<$description$>">Description</option>
                                                                <option value="<$closed_reason$>">Closed Reason</option>
                                                                <option value="<$required_info$>">Required Info</option>
                                                                <option value="<$required_info_response$>">Required Info Response</option>
                                                                <option value="<$optional_info$>">Optional Info</option>
                                                                <option value="<$optional_info_response$>">Optional Info Response</option>                                                            
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-1 col-lg-2 col-12">
                                                    <div class='input-group'>
                                                        <label>&nbsp;</label>
                                                        <button type="button" onclick="add_get_select_value('close_request_subject_fields','request_close_subject')" class="btn btn-primary mr-1">
                                                            <i class="ft-plus"></i> Add Field
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                                
                                            <div class="row">    
                                                <div class="col-xl-8 col-lg-6 col-12">
                                                    <div class='input-group'>
                                                        <label>Close Request Email Body</label>
                                                        <div class='input-group'>
                                                            <%
                                                            value = "";
                                                            for(int a = 0; a < all_global_notifications.size();a++)
                                                            {
                                                                String this_setting[] = all_global_notifications.get(a);
                                                                if(this_setting[1].equalsIgnoreCase("request_created_by_close"))
                                                                {
                                                                    value = this_setting[4];
                                                                }
                                                            }
                                                            %>
                                                            <textarea rows="5" class="form-control" id="request_close_body" name="request_close_body" ><%=value%></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3 col-lg-5 col-12">
                                                    <div class='input-group'>
                                                        <label>Insert Body Request Record Value</label>
                                                        <div class='input-group'>
                                                            <select class="form-control" name="close_request_body_fields" id="close_request_body_fields">
                                                                <option value="<$request_id$>">Request ID</option>
                                                                <option value="<$rev$>">Revision Number</option>
                                                                <option value="<$rev_date$>">Revision Date</option>
                                                                <option value="<$rev_by_user_id$>">Revision by</option>
                                                                <option value="<$service_catalog$>">Service Catalog</option>
                                                                <option value="<$service_catalog_item$>">Service Catalog Item</option>
                                                                <option value="<$create_date$>">Create Date</option>
                                                                <option value="<$request_date$>">Request Date</option>
                                                                <option value="<$due_date$>">Due Date</option>
                                                                <option value="<$resolve_date$>">Resolved Date</option>
                                                                <option value="<$closed_date$>">Closed Date</option>
                                                                <option value="<$assigned_group$>">Assigned Group</option>
                                                                <option value="<$assigned_to$>">Assigned To</option>
                                                                <option value="<$create_by$>">Created By</option>
                                                                <option value="<$requested_for_id$>">Requested For</option>
                                                                <option value="<$requested_for_group$>">Requested For Group</option>
                                                                <option value="<$location$>">Location</option>
                                                                <option value="<$department$>">Department</option>
                                                                <option value="<$site$>">Site</option>
                                                                <option value="<$company$>">Company</option>
                                                                <option value="<$impact$>">Impact</option>
                                                                <option value="<$urgency$>">Urgency</option>
                                                                <option value="<$priority$>">Priority</option>
                                                                <option value="<$state$>">State</option>
                                                                <option value="<$contact_method$>">Contact Method</option>
                                                                <option value="<$approval$>">Approval</option>
                                                                <option value="<$price$>">Price</option>
                                                                <option value="<$notes$>">Notes</option>
                                                                <option value="<$desk_notes$>">Desk Notes</option>
                                                                <option value="<$description$>">Description</option>
                                                                <option value="<$closed_reason$>">Closed Reason</option>
                                                                <option value="<$required_info$>">Required Info</option>
                                                                <option value="<$required_info_response$>">Required Info Response</option>
                                                                <option value="<$optional_info$>">Optional Info</option>
                                                                <option value="<$optional_info_response$>">Optional Info Response</option>                                                              
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-1 col-lg-2 col-12">
                                                    <div class='input-group'>
                                                        <label>&nbsp;</label>
                                                        <button type="button" onclick="add_get_select_value('close_request_body_fields','request_close_body')" class="btn btn-primary mr-1">
                                                            <i class="ft-plus"></i> Add Field
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <button type="button" onclick="javascript:location.href='admin.jsp'" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </button>
                                                <button type="submit" class="btn btn-info">
                                                    &nbsp;&nbsp;<i class="ft-save"></i> Save&nbsp;&nbsp;
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>
    <script>
        function show_proper_protocol()
        {
            var e = document.getElementById("get_email_store_protocol");
            var proto = e.value;
            if(proto === "pop3")
            {
                pop3.style.display = "block";
                imap.style.display = "none";
            } 
            else if(proto === "pop3s")
            {
                pop3.style.display = "block";
                imap.style.display = "none";
            } 
            else if(proto === "imap")
            {
                pop3.style.display = "none";
                imap.style.display = "block";
            }
            else if(proto === "")
            {
                pop3.style.display = "none";
                imap.style.display = "none";
            }
        }
        window.onload=show_proper_protocol;
      </script>                                                    
    
    <script>
        function show(store_type)
        {
            //alert("store type = " + store_type);
            var pop3 = document.getElementById("pop3");
            var imap = document.getElementById("imap");
            if (store_type === "pop3") 
            {
                pop3.style.display = "block";
                imap.style.display = "none";
            } 
            else if (store_type === "pop3s") 
            {
                pop3.style.display = "block";
                imap.style.display = "none";
            } 
            else if(store_type === "imap") 
            {
                pop3.style.display = "none";
                imap.style.display = "block";
            }
            else if(store_type === "") 
            {
                pop3.style.display = "none";
                imap.style.display = "none";
            }
        }
    </script>
    <script>
        function test_email(type, subject, body)
        {
            var subject_element = document.getElementById(subject).value;
            var body_element = document.getElementById(body).value;
            window.open ("email_test_notification.jsp?type=" + type + "&subject=" + subject_element + "&body=" + body_element , '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
        }
    </script>
    <script>        
        function add_get_select_value(source_id, target_id) 
        {
            var source_value = document.getElementById(source_id).value;
            var current_target_value = document.getElementById(target_id).value;
            var new_target_value = current_target_value + source_value;
            document.getElementById(target_id).value = new_target_value;
        }
    </script>
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <!-- END PAGE LEVEL JS-->   
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_user.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_user.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

<!--Copyright 2019 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String FINANCIAL = session.getAttribute("financial").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String CONTACT = session.getAttribute("contact").toString();
        String CX = session.getAttribute("cx").toString();
        String SURVEY = session.getAttribute("survey").toString();
        String SLA = session.getAttribute("sla").toString();        
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        String METRIC = session.getAttribute("metric").toString();
        
        
        if(!ADMINISTRATION.equalsIgnoreCase("true") && !MANAGER.equalsIgnoreCase("true"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);  
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    
    <!-- BEGIN PAGE LEVEL CSS-->
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/selects/select2.min.css">
    <!-- END PAGE LEVEL CSS-->

    <div class="clr whtbg p-10 mb-15">
        <div class="float-right options">
            <ul>
                <li class="dInlineBlock dropdown ">
                    <div class="dInlineBlock clr menuHolder">
                        <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                        </a>
                        <div class="dropdown-menu dropdown-menu-right md p-10">
                            <h5 class="medium-font boldFont pt-5 mb-10">All Filters</h5>
                            <form>
                                <ul>
                                    <li class="mb-10 clr">
                                        <div class="formField md  mr-0 full-width">
                                            <select class="no-border">
                                                <option>Select Filters by Dates</option>
                                                <option>Select Filters by Dates</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Incident Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>
                                        
                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Create Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>

                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Pending Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>

                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">State Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>

                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Close Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>

                                    </li>
                                </ul>
                                <div class="text-center pt-5 pb-5">
                                    <button class="btn md2 btn-primary-new customBtn mb-5">Apply Filters</button>
                                    <div class="clr"></div>
                                    <a href="#" class="">Reset Filters</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </li>
                <li class="dInlineBlock">
                    <span class="btn btn-success customBtn lg waves-effect rightSidePanelOpener">
                        <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>Add Group
                    </span>    
                </li>
            </ul>
        </div>
        <h1 class="large-font boldFont">User Groups</h1>
    </div>

    <%
    if (session.getAttribute("alert") != null) 
    {
        String alert = session.getAttribute("alert").toString();
        session.removeAttribute("alert");

        String alert_class = "alert-info";
        if (session.getAttribute("alert-class") != null) 
        {
            alert_class = session.getAttribute("alert-class").toString();
            session.removeAttribute("alert-class");
        }

    %>
    <div id="session_alert" class="clr whtbg p-10 mb-15">
        <div class="alert <%=alert_class%>" role="alert">
            <%=alert%>
        </div>
    </div>    
    <script type="text/javascript">
        setTimeout(function(){ document.getElementById('session_alert').classList.add("d-none"); }, 3000);    
    </script>
    <%
    }
    %>
        
        <!-- start content here-->
       <div class="sort-tbl-holder">
        <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
            <%
                ArrayList <String[]> all_groups_with_poc_info =  db.get_groups.with_poc_info(con);


                /*user_group_record[0] = rs.getString("groups.id");
                user_group_record[1] = rs.getString("groups.name");
                user_group_record[2] = rs.getString("groups.description");
                user_group_record[3] = rs.getString("users.id");
                user_group_record[4] = rs.getString("users.username");
                user_group_record[5] = rs.getString("user.first");
                user_group_record[6] = rs.getString("users.last");
                user_group_record[7] = rs.getString("users.email");
                user_group_record[8] = rs.getString("users.phone_office");
                user_group_record[9] = rs.getString("users.phone_mobile");*/  

                for(int a = 0; a < all_groups_with_poc_info.size();a++)
                {
                    String value[] = all_groups_with_poc_info.get(a);

                    //id=0 name=1 description=2 created=3 created_by_id=4 administration=5 manager=6 financial=7 incident=8 request=9 contact=10 cx=11 survey=12 sla=13 project=14 job=15 
                    %>

                    
                <tr data-toggle="collapse" data-target="#group_data_<%=a%>" class="tableSortHolder accordion-toggle" aria-expanded="true">
                    <td width="10%">
                        <span class="expand-td">
                            <img src="assets/images/svg/collapse-minus-icon.svg" alt="">
                        </span>
                    </td>
                    <td>
                        <%=value[1]%>
                    </td>
                    <td>
                        <%=value[3]%>
                    </td>
                    <td>
                        <span class="user-img dInlineBlock mr-5 user-avatar"><img src="assets/images/users/avatar-3.jpg" alt=""></span>Admin admin (admin@demo)
                    </td>

                    <td>
                        <a href="javascript:void(0)" onclick="event.stopPropagation(); editGroup(<%=value[0]%>);" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>
                        <a href="javascript:void(0)" onclick="event.stopPropagation(); showDeleteModal(<%=value[0]%>)" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>
                    </td>           

                </tr>

            <tr class="expanded accordian-body -collapse <%=(a==0?"show":"collapse")%>" id="group_data_<%=a%>" aria-expanded="false" style="height: 0px;">
                    <td colspan="5" class="first-row hiddenRow">
                        <div class="content-holder p-15 mt-10 mb-10  ">
                            <h5 class="mb-15 boldFont">Group Members</h5>
                            <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="20%">Group Member</th>
                                        <th width="70%">User Role</th>
                                        <th width="10%" align="center" class="text-center">Actions</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <%
                                    String group_id = value[0];
                                    String users_in_group[][] = db.get_users.users_in_group(con, group_id);
                                    
                                    for(int b = 0; b < users_in_group.length; b++)
                                        {
                                            %>
                                            <tr>
                                                <td>
                                                    <span class="user-img dInlineBlock mr-5 user-avatar"><img src="assets/images/users/avatar-3.jpg" alt=""></span>
                                                    <%=users_in_group[b][3]+" "+users_in_group[b][5]%>
                                                </td>
                                                <td>
                                                    <%
                                                    String roles = db.get_roles.all_roles_for_user_id(con, users_in_group[b][0]); 
                                                    %>
                                                    <%=roles%>
                                                </td>
                                                <td align="center">
                                                    <a href="#" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>
                                                    <a href="#" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>
                                                </td>
                                            </tr>
                                            <%
                                        }
                                    
                                    %>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>

                <%
                }
                %>
            </tbody>
        </table>
    </div>
    <!-- end content here-->

            <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    
    <div class="rightPanelHolder">
        <div class="p-15 ticket-content">
            <span class="rightSidePanelCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
            <h4 id="right_panel_title" class="mediumFont large-font mb-20">Create a New Group</h4>

            <div class="clr whtbg p-15 ">
                <form action="admin_groups_add" method="POST" name="add_form" id="add_form" class="add-project-form needs-validation" novalidate>  
                    <input type="hidden" name="id" value=""/>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="field-title">
                                    <label class="formLabel md has-txt-only px-0">
                                        Name
                                    </label>
                                </div>
                                <div class="formField clr border-0 md">
                                    <input name="name" type="text" value="" class="form-control" placeholder="Group Name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="field-title">
                                    <label class="formLabel md has-txt-only px-0">
                                        Description
                                    </label>
                                </div>
                                <div class="formField clr border-0 md">
                                    <input name="description" type="text" value="" class="form-control" placeholder="Group Description">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="field-title">
                                    <label class="formLabel md has-txt-only px-0">
                                        Group POC
                                    </label>
                                </div>
                                <div class="formField clr border-0 md">
                                    <input type="hidden" name="poc_id" id="poc_id" value=""/>
                                    <input type="text" name="poc_username" id="poc_username" value="" class="form-control" placeholder="Enter the POC's username"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="field-title">
                                    <label class="formLabel md px-0">
                                        Select User(s) to add to Group
                                    </label>
                                </div>
                                <div class="formField clr border-0 md">
                                    <input type="text" size="60" name="username" id="username" value="" class="form-control ui-autocomplete-input" placeholder="Enter a character to start a db lookup" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="field-title">
                                    <label class="formLabel md has-txt-only px-0">
                                        Group Members
                                    </label>
                                </div>
                                <div class="formField clr border-0 md">
                                    <select class="select2 form-control" id="user_ids" name="user_ids" multiple="multiple" placeholder="Select Group members">
                                                
                                    </select>
                                </div>
                            </div>
                            
                        </div>
                        

                        <div class="row ">
                            <div class="col-md-12 text-center pt-30 pb-30">
                            <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                            <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                                    Submit
                                </button>
                            </div>
                        </div>

                </form>
            </div>
        </div>

    </div>
    // delete confirmation modal
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="deleteModalLabel">Role deletion</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            Are you sure you want to delete this role?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-danger" onclick="deleteRole()">Delete</button>
          </div>
        </div>
      </div>
    </div>         

            
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <script src="app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="app-assets/js/scripts/forms/select/form-select2.js"></script>

    <script>                
       $( function() 
       {                      
            //create_by
            $( "#poc_username" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#poc_username').val(ui.item.label); // display the selected text
                    $('#poc_id').val(ui.item.value); // save selected id to input
                    return false;
                }
            });            
        });
        
        $( function() 
        {
            //username
            $( "#username" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#username').val(ui.item.label); // display the selected text
                    $('#caller_id').val(ui.item.value); // save selected id to input
                    //if vip = true then check the vip checkbox
                    var user_id = ui.item.value;
                    var username = ui.item.label;                    
                    //add value to user_ids  user_id and username
                    var opt = document.createElement('option');
                    opt.value = user_id;
                    opt.innerHTML = username;
                    opt.selected = true;
                    var user_ids = document.getElementById("user_ids");
                    user_ids.appendChild(opt);
                    return false;
                }
            });
        });
        
    </script>    
    <!-- END PAGE LEVEL JS-->   
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_roles.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_roles.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

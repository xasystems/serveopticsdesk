<%@page import="org.apache.commons.lang3.StringEscapeUtils"%>
<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : home_self_service
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>

<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        System.out.println("start home_self_service.jsp auth = null");
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        //is self_service enabled?
        
        //are you logged in?
        
        if (!session.getAttribute("self_service").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("true")) 
        {
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            session.setAttribute("home_page", "home_service_catalog.jsp");
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020            
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);

            Connection con = db.db_util.get_contract_connection(context_dir, session); 
            boolean enabled = true;            
            if(db.get_configuration_properties.by_name(con, "self_service_enabled").equalsIgnoreCase("true"))
            {
                enabled = true;
            }
            else
            {
                enabled = false;
            }
            String user_id = session.getAttribute("user_id").toString();
            String user_info[] = db.get_users.by_id(con, user_id);
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            String result = "";
            try
            {
                result = request.getParameter("result");
                if(result == null)
                {
                    result = "";
                }
            }
            catch(Exception e)
            {
                result = "";
            }
            String user_tz_name = "US/Eastern";
            String user_tz_time = "-05:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "US/Eastern";
                    user_tz_time = "-05:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "US/Eastern";
                user_tz_time = "-05:00";
            }
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/chat_app.css">

            <%
            if(!enabled)
            {
                %>
                <div class="alert alert-icon-left alert-warning alert-dismissible mb-2" role="alert">
                    <span class="alert-icon"><i class="la la-asterisk"></i></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>Self Service has been disabled by the administrator!</strong> Please contact the Service Desk via another channel.
                </div>
                <%
            }
            else
            {
            %>
        <div id="wrapper" style="overflow-y: auto;">


            <div class="knowledgebase-search-page">
                <div class="page-content-wrapper clr">
                    <%
                    if(result.equalsIgnoreCase("success"))
                    {
                        %>
                        <div class="alert alert-icon-left alert-success alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="la la-thumbs-o-up"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Success!</strong> Your request has been sent to the Service Desk.
                        </div>
                        <%
                    }
                    else if(result.equalsIgnoreCase("fail"))
                    {
                        %>
                        <div class="alert alert-icon-left alert-danger alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="la la-question-circle"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Sorry, something went wrong!</strong> Please verify all the form data is complete and submit again.
                        </div>
                        <%
                    }
                    %>
                    <div class="banner-section clr">
                        <div class="container">
                            <div class="txt">
                                <h1>How can we help you?</h1>
                                <p>Lead volutpat nibh ligula gravida. Magna auctor eget venenatis phasellus luctus sodales pulvinar</p>
                                <div class="searchHolder clr">
                                    <input id="knowledge_search_terms" type="text" placeholder="Search here...">
                                    <span class="icon"><img src="assets/images/svg/searchIcon.svg" alt=""></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 col-lg-4">
                                <span data-target="get_help" class="rightSidePanelOpener">
                                    <div class="txtBlock text-center whtbg pb-15 mb-30">
                                        <div class="icon"><img src="assets/images/knowledgebase-icon/help.png" alt=""></div>
                                        <h5 class="pl-15 pr-15">Get Help</h5>
                                        <p class="pl-15 pr-15">Mattis nunc convallis turpis elementum. Enim mauris, sagittis auctor </p>
                                    </div>
                                </span>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <span data-target="make_request" class="rightSidePanelOpener">
                                    <div class="txtBlock text-center whtbg pb-15 mb-30">
                                        <div class="icon"><img src="assets/images/knowledgebase-icon/requests.png" alt=""></div>
                                        <h5 class="pl-15 pr-15">Make a Request</h5>
                                        <p class="pl-15 pr-15">Mattis nunc convallis turpis elementum. Enim mauris, sagittis auctor </p>
                                    </div>
                                </span>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <span data-target="my_open_tickets" class="rightSidePanelOpener">
                                    <div class="txtBlock text-center whtbg pb-15 mb-30">
                                        <div class="icon"><img src="assets/images/knowledgebase-icon/ticket.png" alt=""></div>
                                        <h5 class="pl-15 pr-15">My Open Tickets</h5>
                                        <p class="pl-15 pr-15">Mattis nunc convallis turpis elementum. Enim mauris, sagittis auctor </p>
                                    </div>
                                </span>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>

            <%
            }
            %>
            <!-- end content here-->
            
        </div>    
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <div class="rightPanelHolder">
        <div class="p-15 ticket-content">
            <span class="rightSidePanelCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
            <h4 id="rightPanelTitle" class="mediumFont large-font mb-20"></h4>
            <div id="rightPanelContent" class="clr whtbg p-15">
            </div>
        </div>
    </div>

    <div class="rightPanelHolder2">
        <div class="p-15 ticket-content">
            <span class="rightSidePanelCloser2"><img src="assets/images/svg/close-icon.svg" alt=""></span>
            <h4 id="rightPanelTitleChat" class="mediumFont large-font mb-20"></h4>
            <chat-app current-user-id="<%=user_id%>" show-chat="false" service-desk-endpoint="<%=props.get("servicedesk_url").toString()%>"></chat-app>
        </div>
    </div>

    <template id="new_issue">
        <form action="self_service_new_issue" onsubmit="return validate_new_issue()" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-xl-2 col-lg-6 col-12">
                                <label><br>Your Email&nbsp;</label>
                                <input type="text" name="creator_email" id="creator_email" value="<%=user_info[15]%>" class="form-control" placeholder="Enter your Email address"/>
                            </div>
                            <div class="col-xl-2 col-lg-6 col-12">
                                <label><br>Your Phone&nbsp;</label>
                                <input type="text" name="creator_phone" id="creator_phone" value="<%=user_info[16]%>" class="form-control" placeholder="Enter your phone number"/>
                            </div>
                        </div> 
                        <div class="row mt-1">
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-2 col-lg-6 col-12">
                                <label><br>Submitting for someone else?&nbsp;</label>
                                <br>
                                <input onchange="change_submitted_for_email();" class="big-checkbox" type="checkbox" name="for_someone_else" id="for_someone_else"value="ON"/>
                            </div>
                            <div class="col-xl-3 col-lg-6 col-12">
                                <label><br>Submitting for Email&nbsp;</label>
                                <input readonly="true" type="text" name="submitted_for_email" id="submitted_for_email" value="" class="form-control" placeholder="Enter the Submitted For Persons Email address"/>
                            </div>
                            <div class="col-xl-2 col-lg-6 col-12">
                                <label><br>Submitting for Phone&nbsp;</label>
                                <input type="text" name="submitting_for_phone" id="submitting_for_phone" value="" class="form-control" placeholder="Enter their phone number"/>
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <label><br>Brief Description of the Issue</label>
                                <br>
                                <input type="text" name="subject" id="subject" value="" class="form-control" placeholder="Enter brief description"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <label><br>Details</label>
                                <textarea rows="6" name="body" id="body" class="form-control" placeholder="Enter the details of the issue"/></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12"><hr></div>
                        </div>
                        <div class="row">
                            <div class="col-12"><input type="file" name="file" type="id" class="form-control-file" id="exampleInputFile"></div>
                        </div>
                        <div class="row">
                            <div class="col-12"><hr></div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <button type="submit" id="submit" class="btn btn-primary-new mr-1">
                                    <i class="fa-filter"></i> Submit Issue
                                </button>
                            </div>
                        </div>                                        
        </form>
    </template>

    <template id="my_open_tickets">
        <%
        ArrayList<String[]> my_items = db.my_data.my_self_service_list(con, user_id, user_tz_time);
        %>
        <div class="sort-tbl-holder">
            <table id="my_open_tickets_table" name="item_table" class="table table-striped table-striped-reverse custom-sort-table">
                <thead>
                    <tr>
                        <th style="width:1%"></th>
                        <th style="width:5%">ID</th>
                        <th style="width:5%">Name</th>
                        <th style="width:5%">Description</th>
                        <th style="width:5%">Priority</th>
                        <th style="width:5%">Time</th>
                        <th style="width:5%">State</th>
                    </tr>
                </thead>
                <tbody>
                    <% 
                    for(int a = 0; a < my_items.size(); a++)
                    {
                        String my_item[] = my_items.get(a);
                        String pri = my_item[4];
                        String Dpriority = "unknown";
                        String time = "";
                        String unEscapedHTML = StringEscapeUtils.unescapeHtml4(my_item[7]);
                        try
                        {
                            //System.out.println("time before date function=" + my_item[5]);
                            time = display_format.format(timestamp_format.parse(my_item[5]));
                            //System.out.println("time after date function=" + time);
                        }
                        catch (Exception e)
                        {
                            time = "";
                        }
                        if(pri.equalsIgnoreCase("critical"))
                        {
                            Dpriority = "critical";
                        }else if(pri.equalsIgnoreCase("high"))
                        {
                            Dpriority = "high";
                        }else if(pri.equalsIgnoreCase("medium"))
                        {
                            Dpriority = "medium";
                        }else if(pri.equalsIgnoreCase("low"))
                        {
                            Dpriority = "low";
                        } 
                        %>
                        <tr data-toggle="collapse" data-target="#<%=my_item[1]%>" class="accordion-toggle" aria-expanded="true">
                            <td><span class="expand-td"><img src="assets/images/svg/collapse-minus-icon.svg" alt=""></span></td>
                            <td><%=my_item[1]%></td>
                            <td><%=my_item[2]%></td>
                            <td><%=my_item[3]%></td>
                            <td><span class="indicator <%=Dpriority%>"></span>&nbsp;<%=(my_item[4].equals("") ? "Unknown" : my_item[4])%></td>  
                            <td nowrap><%=time%></td> 
                            <td><%=my_item[6]%></td>
                        </tr>   
                                <tr class="expanded accordian-body -collapse collapse" id="<%=my_item[1]%>" aria-expanded="false" style="height: 0px;">
                                    <td colspan="7" class="first-row hiddenRow">
                                        <div class="content-holder p-15 mt-10 mb-10 mr-2">
                                            <% if (!unEscapedHTML.equals("")) { %>
                                                <div class="row mb-10">
                                                    <div class="col-12">
                                                        <h4>Notes:</h4>
                                                        <% out.println(unEscapedHTML); %>
                                                    </div>
                                                </div>
                                            <% } %>
                                            <form action="self_service_follow" onsubmit="return validateFollowUpForm()"  method="post">
                                                <input type="hidden" name="follow_up_item" id="follow_up_item_<%=my_item[1]%>" value="<%=my_item[1]%>">
                                                <div class="row mb-10">
                                                    <div class="col-12">
                                                        <h3>Enter your question/comment below.</h3>  
                                                        <textarea class="form-control" rows="5" id="follow_up_question" name="follow_up_question" placeholder="Enter your qusetion here"></textarea>
                                                    </div>
                                                </div>            

                                                <div class="row">
                                                    <div class="col-12">
                                                        <button type="submit" id="submit" class="btn btn-primary-new mr-1">
                                                            <i class="fa-filter"></i> Submit Question
                                                        </button>
                                                    </div>
                                                </div>    
                                            </form>

                                        </div>
                                    </td>
                                </tr> 
                       <%
                    }  
                    if(my_items.size() == 0)
                    {
                        %>
                        <tr>
                            <td>No Items</td>
                            <td></td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                        </tr>
                        <%
                    }
                    %> 
                </tbody>
            </table>
        </div>
    </template>

    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script src="assets/js/axios.min.js"></script>
    <script src="assets/js/vue.min.js"></script>

    <script>
        jQuery.ui.autocomplete.prototype._resizeMenu = function () {
          var ul = this.menu.element;
          ul.outerWidth(this.element.outerWidth());
        }
        
    function addRightSidePanelListeners()
    {
        $('.rightSidePanelCloser2:not(.listens)').each(function(){
            $(this).addClass('listens');
            this.addEventListener("click", function()
            {
                document.querySelector('chat-app').showChat = false;
                $('.rightPanelHolder2').removeClass('open');
                $('.rightSidePanel2').removeClass('open');
                $('html').removeClass('panelOpen');
            }, false);
        });

        $('.rightSidePanelOpener2:not(.listens)').each(function(){
            $(this).addClass('listens');
            
            this.addEventListener("click", function()
            {
                $('.rightPanelHolder2').addClass('open');
                $('.rightSidePanel2').addClass('open');
                $('html').addClass('panelOpen');
                
                var chatApp = document.querySelector('chat-app');
                chatApp.objectType = $(this).data('target');
                chatApp.objectId = $(this).data('id');
                chatApp.parentObjectType = "";
                chatApp.parentObjectId = 0;
//                console.log($(this).data('projectId'));
//                chatApp.currentUserId = 0;
                chatApp.showChat = true;
//                console.log("panel2 opened");
            });
        });

        $('.rightSidePanelCloser:not(.listens)').each(function(){
            $(this).addClass('listens');
            this.addEventListener("click", function()
            {
                $('.rightPanelHolder').removeClass('open');
                $('.rightSidePanel').removeClass('open');
                $('html').removeClass('panelOpen');
            }, false);
        });
        
        $('.rightSidePanelOpener:not(.listens)').each(function(){
//            console.log("attaching to ", this);
            $(this).addClass('listens');
            
            this.addEventListener("click", function()
            {
                $('.rightPanelHolder').addClass('open');
                $('.rightSidePanel').addClass('open');
                $('html').addClass('panelOpen');
                var template_id = "";
                var panel_title = "";
                var clicked_node = $(event.target).closest(".rightSidePanelOpener");
                var target = clicked_node.data('target');

                var ajax = {};
                ajax.url = "";
                ajax.type = 'GET';
                ajax.dataType = "html";

                ajax.error = function(jqXHR, textStatus)
                {
                    switch (jqXHR.status) {
                        case 401:
                            window.location.assign("<%=props.get("login_url").toString()%>");
                            break;
                        case 403:
                            alert("No permission!");
                            break;
                        default:
                            alert("Error processing request!");
                    }
                };
                $("#rightPanelContent").removeClass("reversed");
                
                switch (target) 
                {

                    case "get_help":
                        template_id = "new_issue";
                        panel_title = "New Issue";
                        break;
                    case "my_open_tickets":
                        template_id = "my_open_tickets";
                        panel_title = "My Incidents and Requests";
                        $("#rightPanelContent").addClass("reversed");
                        break;
                    case "make_request":
                        ajax.url = "home_service_catalog.jsp?embedded=true";
                        panel_title = "Create a New Request";                        
                        break;
                    case "service-catalog":
                        ajax.url = "service_catalog.jsp";
                        var service_catalog_id = clicked_node.data('catalogId');
                        // todo urlSearchParameters for pass_parameters
                        ajax.data = {id: service_catalog_id, embedded: true};
                        panel_title = "Create a New Request";
                        break;
                    case "item":
                        ajax.url = "service_catalog_item.jsp";
                        var service_catalog_item_id = clicked_node.data('itemId');
                        // todo urlSearchParameters for pass_parameters
                        ajax.data = {id: service_catalog_item_id, embedded: true};
                        panel_title = "Create a New Request";
                        break;
                        
                }
                $('.rightPanelHolder').removeClass("service-catalog-page");
                $("#rightPanelContent").removeClass("catalog-items-listing");

                if (template_id != "") {
                    var temp = document.getElementById(template_id);
                    var clone = temp.content.cloneNode(true);
                    $("#rightPanelTitle").html(panel_title);
                    $("#rightPanelContent").html(clone);
                    addRightSidePanelListeners();
                    attachFieldListeners($("#rightPanelContent"));
                } else {
                    ajax.success = function (data) 
                    {                    
                        $('.rightPanelHolder').addClass('open');
                        $('.rightSidePanel').addClass('open');
                        $('html').addClass('panelOpen');

                        $("#rightPanelTitle").html(panel_title);

                        $("#rightPanelContent").html(data);
                        addRightSidePanelListeners();
                        attachFieldListeners($("#rightPanelContent"));        
                    }                        

                    $.ajax(ajax);
//                    console.log(target);
                    switch (target) 
                    {
                        case "make_request":
                            $('.rightPanelHolder').addClass("service-catalog-page");
                            break;
                        case "service-catalog":
                            $('.rightPanelHolder').addClass("service-catalog-page");
                            $("#rightPanelContent").addClass("catalog-items-listing");
                            break;
                    }

                }
                
                  

                return;

            }, false);
        });

    }
    
    window.addEventListener("load", (event) => 
    {
        addRightSidePanelListeners();
//                    autoWidth: false,
    });
    
        function attachFieldListeners(container)
        {
//            if (!$.fn.dataTable.isDataTable('#my_open_tickets_table')) {
//                $('#my_open_tickets_table').DataTable();
//            }
            
            $('#catalog_search').autocomplete({
                source: function( request, response ) 
                {
                    $.ajax({
                        url: "ajax_lookup_service_catalog",
                        type: 'post',
                        dataType: "json",
                        data: 
                        {
                            search: request.term,
                            full: 1
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                minLength: 2,
                select: function( event, ui ) 
                {
                    window.location.href = "service_catalog.jsp?id=" + ui.item.value
                    return false;
                }
            });
            
        }
        
        function validateFollowUpForm() 
        {
            var radios = document.getElementsByTagName('input');
            var error = "Please select an item from the list.";
            for (var i = 0; i < radios.length; i++) 
            {
                if (radios[i].type === 'radio' && radios[i].checked && radios[i].name === 'follow_up_item') 
                {
                     error = "";
                }
            }
            
            if(document.getElementById("follow_up_question").value === '')
            {
                error = error + " Please enter a question/comment."
            }
            if(error !== "")
            {
                var errorMsg = "<p>" + error + "</p>"
                document.getElementById("follow_up_modal_text").innerHTML = errorMsg
                $("#follow_up_error_modal").modal("show");
                //alert(error);
                return false;
            }
            else
            {
                return true;
            }
        }
    </script>
    <script>
        function validate_new_issue()
        {
            var error = "";
            if(document.getElementById('for_someone_else').checked)
            {
                if(document.getElementById('submitted_for_email').value === "")
                {
                    error = "If submitting for someone else, the someone else's email must be filled out.";
                }
            }
            if(error !== "")
            {
                var errorMsg = "<p>" + error + "</p>"
                document.getElementById("modal_text").innerHTML = errorMsg
                $("#error_modal").modal("show");
                return false;
            }
            else
            {
                return true;
            }
            
        }
    </script>
    
    <script>
        function change_submitted_for_email()
        {
            if(document.getElementById('for_someone_else').checked)
            {
                document.getElementById('submitted_for_email').readOnly = false;
            }
            else
            {
                document.getElementById('submitted_for_email').value = '';
                
                
                document.getElementById('submitted_for_email').readOnly = true;
                
            }
        }
        
    </script>
    <script>
        function toggleDiv(id) 
        {
            var div = document.getElementById(id);
            div.style.display = div.style.display == "none" ? "block" : "none";
        }
    </script>
    <script>
    //knowledge_search_terms
    $(document).ready(function() {
         $("#knowledge_search_terms").autocomplete({

             source: function( request, response ) 
             {
                 // Fetch data
                 $.ajax({
                     url: "ajax_lookup_self_service_knowledge_article_text",
                     type: 'post',
                     dataType: "json",
                     minLength: 2,
                     data: 
                     {
                         search: request.term
                     },
                     success: function( data ) 
                     {
                         response( data );
                     }
                 });
             },
             select: function( event, ui ) 
             { 
//                 $('#knowledge_search_terms').val(ui.item.article_description);
                 //window.location.href = ui.item.value;
//                 window.open("kb_view.jsp?id=" + ui.item.value, "Knowledge Base Article","directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=600,height=600,top=300,left=500");
                 //window.open("kb_view.jsp?id=" + ui.item.value, "Knowledge Base Article", "width=600,height=600,top=400,left=400");
                var ajax = {};
                ajax.url = "kb_view.jsp?id=" + ui.item.value;
                ajax.type = 'GET';
                ajax.dataType = "html";
                panel_title = "Knowledge Base Article";

                ajax.error = function(jqXHR, textStatus)
                {
                    switch (jqXHR.status) {
                        case 401:
                            window.location.assign("<%=props.get("login_url").toString()%>");
                            break;
                        case 403:
                            alert("No permission!");
                            break;
                        default:
                            alert("Error processing request!");
                    }
                };

                ajax.success = function (data) 
                {                    
                    $('.rightPanelHolder').addClass('open');
                    $('.rightSidePanel').addClass('open');
                    $('html').addClass('panelOpen');

                    $("#rightPanelTitle").html(panel_title);
                    $("#rightPanelContent").html(data);
                    addRightSidePanelListeners();
                }
                
                $.ajax(ajax);
                return false;
            }
         });
     });
    </script>
    <script src="assets/js/chat_app.js"></script>
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>
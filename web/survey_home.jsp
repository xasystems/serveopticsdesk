<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->

<%@page import="db.get_incidents"%>
<%@page import="db.get_chart_data"%>

<%-- 
    Document   : incident_home
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="java.text.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>

<!DOCTYPE html>
<%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        if (!session.getAttribute("survey").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            //System.out.println("line 29");
            String home_page = session.getAttribute("home_page").toString();
            //if you have the permission to read/modify or Admin then you can see this
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            
            //int number_of_active_sessions = db.get_user_sessions.active(con, session_timeout, user_id);
            DecimalFormat two_decimals = new DecimalFormat("0.00");
            DecimalFormat no_decimals = new DecimalFormat("#");
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
            //SimpleDateFormat filter_format2 = new SimpleDateFormat("yyyy"); //01/12/2019 12:00 AM
            DecimalFormat df2 = new DecimalFormat("#.##");
            java.util.Date filter_start = new java.util.Date();
            java.util.Date filter_end = new java.util.Date();
            java.util.Date previous_start_date = new java.util.Date();
            java.util.Date previous_end_date = new java.util.Date();
            String start = "";
            String end = "";
            
            String date_range = "";
            String selected = "";
            try 
            {
                date_range = request.getParameter("date_range");
                if (date_range.equalsIgnoreCase("null") || date_range == null) 
                {
                    date_range = support.filter_dates.past_30_days();
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                date_range = support.filter_dates.past_30_days();
            }
            
            try 
            {                
                //split the start and end date  //01/12/2019 12:00 AM - 01/12/2019 11:59 PM
                String temp[] = date_range.split("-");
                filter_start = filter_format.parse(temp[0]);
                filter_end = filter_format.parse(temp[1]);                
                start = timestamp.format(filter_start);
                end = timestamp.format(filter_end);
                //get pervious date frame                
                previous_start_date.setTime(filter_start.getTime());
                previous_end_date.setTime(filter_end.getTime());                
                long diff = filter_end.getTime() - filter_start.getTime();
                previous_start_date.setTime(filter_start.getTime() - diff);
                previous_end_date.setTime(filter_end.getTime() - diff);
                
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                System.out.println("survey_home.jsp exception=" + e);
            }
            //System.out.println("line 89");

%>
<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>	
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
<!-- END VENDOR CSS-->
<!-- BEGIN MODERN CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
<!-- END MODERN CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
<link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
<link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">

<!-- END Page Level CSS-->
<!-- BEGIN Custom CSS-->
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<!-- END Custom CSS-->
<jsp:include page='menu_service_desk.jsp'>
    <jsp:param name="active_menu" value=""/>
</jsp:include>	
<input type="hidden" name="start" id="start" value="<%=start%>"/>
<input type="hidden" name="end" id="end" value="<%=end%>"/>


<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title"><i class="la la-dollar"></i>&nbsp;CSAT</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a>
                            </li>
                            <li class="breadcrumb-item active">CSAT
                            </li>
                        </ol>
                    </div>
                </div>                    
            </div>
        </div>
        <!-- start content here-->
        <div class="row">
            <div class="col-xl-3 col-6">
                <div class="form-group">
                    <label>Date Range</label>
                    <div class='input-group'>
                        <input type='text' id="filter_date_range" name="filter_date_range" value="<%=date_range%>" class="form-control datetime" />
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <span class="la la-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-1 col-2">
                <label>&nbsp;</label>
                <div class="form-actions">
                    <button type="button" onclick="reload_page()" class="btn btn-blue mr-1">
                        <i class="fa-filter"></i> Apply
                    </button>
                </div>
            </div>
        </div>       
        <div class="row">    
            <div class="col-xl-2 col-lg-6 col-12">      
                <a href="csat_incident.jsp?date_range=<%=date_range%>">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <table width="100%">
                                    <tr>
                                        <td colspan="3"><h4 class="text-muted"><b>Incident CSAT<b></h4></td>
                                    </tr>
                                    <tr>
                                        <td width="10%">
                                            <%
                                            String color = "";
                                            String arrow = "";
                                            //get this periods score
                                            String this_period_survey_results[][] = db.get_survey_results.for_all_csat_results_for_date_range_and_trigger_object(con, "incident", filter_start, filter_end);
                                            double this_period_score[] = db.get_survey_results.csat_score_for_survey_results(this_period_survey_results);
                                            //get previous periods score 
                                            //System.out.println("previous_start_date=" + previous_start_date + " previous_end_date=" + previous_end_date);
                                            String previous_period_survey_results[][] = db.get_survey_results.for_all_csat_results_for_date_range_and_trigger_object(con, "incident", previous_start_date, previous_end_date);
                                            double previous_period_score[] = db.get_survey_results.csat_score_for_survey_results(previous_period_survey_results);
                                            
                                            //System.out.println("this_period_score[3]" + this_period_score[3] + "  - previous_period_score[3]" + previous_period_score[3] + " =" + (this_period_score[3] - previous_period_score[3]));
                                            if(this_period_score[3] > previous_period_score[3])
                                            {
                                                color = "danger";
                                                arrow = "ft-chevron-up";
                                            }
                                            else if(this_period_score[3] < previous_period_score[3])
                                            {
                                                color = "success";
                                                arrow = "ft-chevron-down";
                                            }
                                            else if(this_period_score[3] == previous_period_score[3])
                                            {
                                                color = "info";
                                                arrow = "la la-pause";
                                            }
                                            %>
                                            <i class="<%=arrow%> <%=color%> font-large-2" data-toggle="popover" data-content="The Change (+ or -) value is the difference from this period to the previous period." data-trigger="hover" data-original-title="Total Contacts"></i>
                                        </td>
                                        <td>
                                            <h5>
                                                
                                                <%=df2.format(this_period_score[3] - previous_period_score[3])%>   
                                            </h5>
                                        </td>
                                        <td align="right"><h2><%=df2.format(this_period_score[3])%></h2></td>
                                    </tr>
                                </table>                            
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-2 col-lg-6 col-12">
                <a href="csat_request.jsp?date_range=<%=date_range%>">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <table width="100%">
                                    <tr>
                                        <td colspan="3"><h4 class="text-muted"><b>Request CSAT</b></h4></td>
                                    </tr>
                                    <tr>
                                        <td width="10%">
                                            <%
                                            color = "";
                                            arrow = "";
                                            //get this periods score
                                            this_period_survey_results = db.get_survey_results.for_all_csat_results_for_date_range_and_trigger_object(con, "request", filter_start, filter_end);
                                            this_period_score = db.get_survey_results.csat_score_for_survey_results(this_period_survey_results);
                                            //get previous periods score 
                                            //System.out.println("previous_start_date=" + previous_start_date + " previous_end_date=" + previous_end_date);
                                            previous_period_survey_results = db.get_survey_results.for_all_csat_results_for_date_range_and_trigger_object(con, "request", previous_start_date, previous_end_date);
                                            previous_period_score = db.get_survey_results.csat_score_for_survey_results(previous_period_survey_results);
                                            
                                            //System.out.println("this_period_score[3]" + this_period_score[3] + "  - previous_period_score[3]" + previous_period_score[3] + " =" + (this_period_score[3] - previous_period_score[3]));
                                            if(this_period_score[3] > previous_period_score[3])
                                            {
                                                color = "danger";
                                                arrow = "ft-chevron-up";
                                            }
                                            else if(this_period_score[3] < previous_period_score[3])
                                            {
                                                color = "success";
                                                arrow = "ft-chevron-down";
                                            }
                                            else if(this_period_score[3] == previous_period_score[3])
                                            {
                                                color = "info";
                                                arrow = "la la-pause";
                                            }
                                            %>
                                            <i class="<%=arrow%> <%=color%> font-large-2" data-toggle="popover" data-content="The Change (+ or -) value is the difference from this period to the previous period." data-trigger="hover" data-original-title="Total Contacts"></i>
                                        </td>
                                        <td>
                                            <h5>
                                                
                                                <%=df2.format(this_period_score[3] - previous_period_score[3])%>   
                                            </h5>
                                        </td>
                                        <td align="right"><h2><%=df2.format(this_period_score[3])%></h2></td>
                                    </tr>
                                </table>                            
                            </div>
                        </div>
                    </div>
                </a>
            </div>
           <div class="col-xl-2 col-lg-6 col-12">
                <a href="csat_contact.jsp">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <table width="100%">
                                    <tr>
                                        <td colspan="3"><h4 class="text-muted"><b>Contact CSAT</b></h4></td>
                                    </tr>
                                    <tr>
                                        <%
                                        int this_period_self_help = db.get_self_help.sessions_per_timeframe(con, filter_start, filter_end);
                                        int previous_period_self_help = db.get_self_help.sessions_per_timeframe(con, previous_start_date, previous_end_date);
                                        if(this_period_self_help > previous_period_self_help)
                                        {
                                            color = "success";
                                            arrow = "ft-chevron-up";
                                        }
                                        else if(this_period_self_help < previous_period_self_help)
                                        {
                                            color = "danger";
                                            arrow = "ft-chevron-down";
                                        }
                                        else if(this_period_self_help == previous_period_self_help)
                                        {
                                            color = "info";
                                            arrow = "la la-pause";
                                        }
                                        int self_help_diff = this_period_self_help - previous_period_self_help;
                                        %>
                                        <td width="10%">
                                            <i class="<%=arrow%> <%=color%> font-large-2" data-toggle="popover" data-content="The number of Self-help sessions. " data-trigger="hover" data-original-title="Tier 0 Self Help"></i></td>
                                        <td><h5><%=self_help_diff%></h5></td>
                                        <td align="right"><h2><%=this_period_self_help%></h2></td>
                                    </tr>
                                </table>                            
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-2 col-lg-6 col-12">
                <a href="csat_manual.jsp">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <table width="100%">
                                    <tr>
                                        <td colspan="3"><h4 class="text-muted"><b>Adhoc CSAT</b></h4></td>
                                    </tr>
                                    <tr>
                                        <td width="10%">
                                            <%
                                            int this_period_fcr_count = db.get_incidents.incident_fcr_count_start_date_stop_date(con, filter_start, filter_end);
                                            int previous_period_fcr_count = db.get_incidents.incident_fcr_count_start_date_stop_date(con, previous_start_date, previous_end_date);
                                            color = "";
                                            arrow = "";
                                            if(this_period_fcr_count < previous_period_fcr_count)
                                            {
                                                color = "danger";
                                                arrow = "ft-chevron-down";
                                            }
                                            else if(this_period_fcr_count > previous_period_fcr_count)
                                            {
                                                color = "success";
                                                arrow = "ft-chevron-up";
                                            }
                                            else if(this_period_fcr_count == previous_period_fcr_count)
                                            {
                                                color = "info";
                                                arrow = "la la-pause";
                                            }
                                            %>
                                            <i class="<%=arrow%> <%=color%> font-large-2" data-toggle="popover" data-content="First Contact Resolution is a measure based on the number of calls that are resolved on first contact." data-trigger="hover" data-original-title="First Contact Resolution"></i>
                                        </td>
                                        <td><h5><%=df2.format(this_period_fcr_count - previous_period_fcr_count)%></h5></td>
                                        <td align="right"><h2><%=df2.format(this_period_fcr_count)%></h2></td>
                                    </tr>
                                </table>                            
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>            
        
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><b>Incident CSAT</b></h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body pt-0">
                            <table class="table table-striped table-bordered zero-configuration">
                                <thead>
                                    <tr>
                                        <th>Survey Name</th>
                                        <th>Sent</th>
                                        <th>Completed</th>
                                        <th>Completion Rate</th>
                                        <th>Avg. Score</th>
                                        <!--<th>Avg. Monthly Score <em>(Past 12 Months)</em></th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                    String all_csat_contact_surveys[][] = db.get_surveys.reportable_csat_by_trigger_object(con,"incident");
                                    for(int a = 0; a < all_csat_contact_surveys.length;a++) //each survey
                                    {
                                        String survey_id = all_csat_contact_surveys[a][0];
                                        //get survey results for this survey and filter_start and filter_stop and support_group_id and customer_group_id
                                        String survey_results[][] = db.get_survey_results.for_survey_id_start_date_end_date(con, survey_id, filter_start, filter_end);
                                        //System.out.println("survey_results length=" + survey_results.length);
                                        double survey_score[] = db.get_survey_results.csat_score_for_survey_results(survey_results);
                                        %>
                                        <tr onclick="location.href='csat_survey_detail.jsp?date_range=<%=date_range%>&support_group_id=11>&customer_group_id=22&survey_id=<%=survey_id%>';" style="cursor:pointer;">
                                        <th><%=all_csat_contact_surveys[a][2]%></th>                                            
                                        <td><%=no_decimals.format(survey_score[0])%></td>      
                                        <td><%=no_decimals.format(survey_score[1])%></td>
                                        <td><%=two_decimals.format(survey_score[2])%></td>
                                        <td><%=two_decimals.format(survey_score[3])%></td>
                                        <!--<td data-sparkline="3, 3.5, 4.2, 4.8, 4.0, 3.0, 2.8,3, 3.5, 4.2, 4.8, 4.0  ; column"/>-->
                                        </tr>

                                        <%
                                    }
                                    %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
       
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><b>Request CSAT</b></h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body pt-0">
                            <table class="table table-striped table-bordered zero-configuration">
                                <thead>
                                    <tr>
                                        <th>Survey Name</th>
                                        <th>Sent</th>
                                        <th>Completed</th>
                                        <th>Completion Rate</th>
                                        <th>Avg. Score</th>
                                        <!--<th>Avg. Monthly Score <em>(Past 12 Months)</em></th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                    all_csat_contact_surveys = db.get_surveys.reportable_csat_by_trigger_object(con,"request");
                                    for(int a = 0; a < all_csat_contact_surveys.length;a++) //each survey
                                    {
                                        String survey_id = all_csat_contact_surveys[a][0];
                                        //get survey results for this survey and filter_start and filter_stop and support_group_id and customer_group_id
                                        String survey_results[][] = db.get_survey_results.for_survey_id_start_date_end_date(con, survey_id, filter_start, filter_end);
                                        System.out.println("survey_results length=" + survey_results.length);
                                        double survey_score[] = db.get_survey_results.csat_score_for_survey_results(survey_results);
                                        %>
                                        <tr onclick="location.href='csat_survey_detail.jsp?date_range=<%=date_range%>&support_group_id=11>&customer_group_id=22&survey_id=<%=survey_id%>';" style="cursor:pointer;">
                                        <th><%=all_csat_contact_surveys[a][2]%></th>                                            
                                        <td><%=no_decimals.format(survey_score[0])%></td>      
                                        <td><%=no_decimals.format(survey_score[1])%></td>
                                        <td><%=two_decimals.format(survey_score[2])%></td>
                                        <td><%=two_decimals.format(survey_score[3])%></td>
                                        <!--<td data-sparkline="3, 3.5, 4.2, 4.8, 4.0, 3.0, 2.8,3, 3.5, 4.2, 4.8, 4.0  ; column"/>-->
                                        </tr>

                                        <%
                                    }
                                    %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><b>Contact CSAT</b></h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body pt-0">
                            <table class="table table-striped table-bordered zero-configuration">
                                <thead>
                                    <tr>
                                        <th>Survey Name</th>
                                        <th>Sent</th>
                                        <th>Completed</th>
                                        <th>Completion Rate</th>
                                        <th>Avg. Score</th>
                                        <!--<th>Avg. Monthly Score <em>(Past 12 Months)</em></th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                    all_csat_contact_surveys = db.get_surveys.reportable_csat_by_trigger_object(con,"contact");
                                    for(int a = 0; a < all_csat_contact_surveys.length;a++) //each survey
                                    {
                                        String survey_id = all_csat_contact_surveys[a][0];
                                        //get survey results for this survey and filter_start and filter_stop and support_group_id and customer_group_id
                                        String survey_results[][] = db.get_survey_results.for_survey_id_start_date_end_date(con, survey_id, filter_start, filter_end);
                                        System.out.println("survey_results length=" + survey_results.length);
                                        double survey_score[] = db.get_survey_results.csat_score_for_survey_results(survey_results);
                                        %>
                                        <tr onclick="location.href='csat_survey_detail.jsp?date_range=<%=date_range%>&support_group_id=11>&customer_group_id=22&survey_id=<%=survey_id%>';" style="cursor:pointer;">
                                        <th><%=all_csat_contact_surveys[a][2]%></th>                                            
                                        <td><%=no_decimals.format(survey_score[0])%></td>      
                                        <td><%=no_decimals.format(survey_score[1])%></td>
                                        <td><%=two_decimals.format(survey_score[2])%></td>
                                        <td><%=two_decimals.format(survey_score[3])%></td>
                                        <!--<td data-sparkline="3, 3.5, 4.2, 4.8, 4.0, 3.0, 2.8,3, 3.5, 4.2, 4.8, 4.0  ; column"/>-->
                                        </tr>

                                        <%
                                    }
                                    %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><b>Adhoc CSAT</b></h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body pt-0">
                            <table class="table table-striped table-bordered zero-configuration">
                                <thead>
                                    <tr>
                                        <th>Survey Name</th>
                                        <th>Sent</th>
                                        <th>Completed</th>
                                        <th>Completion Rate</th>
                                        <th>Avg. Score</th>
                                        <!--<th>Avg. Monthly Score <em>(Past 12 Months)</em></th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                    //String all_csat_contact_surveys[][] = db.get_surveys.reportable_csat_by_trigger_object(con,"contact");

                                    for(int a = 0; a < all_csat_contact_surveys.length;a++) //each survey
                                    {
                                        String survey_id = all_csat_contact_surveys[a][0];
                                        //get survey results for this survey and filter_start and filter_stop and support_group_id and customer_group_id
                                        String survey_results[][] = db.get_survey_results.for_survey_id_start_date_end_date_support_group_id_customer_group_id(con, survey_id, filter_start, filter_end, "1", "1");
                                        double survey_score[] = db.get_survey_results.csat_score_for_survey_results(survey_results);
                                        %>
                                        <tr onclick="location.href='csat_survey_detail.jsp?date_range=<%=date_range%>&support_group_id=11>&customer_group_id=22&survey_id=<%=survey_id%>';" style="cursor:pointer;">
                                        <th><%=all_csat_contact_surveys[a][2]%></th>                                            
                                        <td><%=no_decimals.format(survey_score[0])%></td>      
                                        <td><%=no_decimals.format(survey_score[1])%></td>
                                        <td><%=two_decimals.format(survey_score[2])%></td>
                                        <td><%=two_decimals.format(survey_score[3])%></td>
                                        <!--<td data-sparkline="3, 3.5, 4.2, 4.8, 4.0, 3.0, 2.8,3, 3.5, 4.2, 4.8, 4.0  ; column"/>-->
                                        </tr>

                                        <%
                                    }
                                    %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
        <!-- end content here-->
        
    </div> <!--end  content-wrapper -->       
</div><!--end  app-content content -->
<jsp:include page='footer.jsp'>
    <jsp:param name="parameter" value=""/>
</jsp:include>	
<!-- BEGIN VENDOR JS-->

<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN MODERN JS-->
<script src="app-assets/js/core/app-menu.js"></script>
<script src="app-assets/js/core/app.js"></script>
<!-- END MODERN JS-->
<!-- BEGIN PAGE LEVEL JS-->

<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
<script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
<script src="app-assets/js/scripts/popover/popover.js"></script>
<script>
    function reload_page()
    {
        var filter_date_range = document.getElementById("filter_date_range").value;
        var URL = "contact_home.jsp?date_range=" + filter_date_range;
        window.location.href = URL;
    }
</script>


<!-- END PAGE LEVEL JS-->

</body>
</html>
<%
            con.close();
        }//end if user has permission
        else {
            String RedirectURL = "no_permission.jsp";
            response.sendRedirect(RedirectURL);
        }
    }//end if user is logged in
%>

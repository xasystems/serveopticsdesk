<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : incident_home
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="db.get_incidents"%>
<%@page import="db.get_chart_data"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page import="java.time.ZonedDateTime"%>
<%@ page import="java.time.format.DateTimeFormatter"%>
<!DOCTYPE html>
<%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    } 
    else 
    {
        if (!session.getAttribute("incident").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            String user_tz_name = "UTC";
            String user_tz_time = "+00:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }                
            }
            catch(Exception e)
            {
                user_tz_name = "UTC";
                user_tz_time = "+00:00";
            }  
            String home_page = session.getAttribute("home_page").toString();
            //if you have the permission to read/modify or Admin then you can see this
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session); 
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            //SimpleDateFormat filter_format2 = new SimpleDateFormat("yyyy"); //01/12/2019 12:00 AM
            java.util.Date filter_start = new java.util.Date();
            java.util.Date filter_end = new java.util.Date();
            java.util.Date previous_start_date = new java.util.Date();
            java.util.Date previous_end_date = new java.util.Date();
            
            String start = "";
            String end = "";
            //System.out.println("format2=" + filter_format2.parse("2016"));
            //System.out.println("format" + filter_format.parse("01/12/2019 12:00 AM"));

            ArrayList<String[]> groups = db.get_groups.all(con);
            String date_range = "";
            String support_group_id = "";
            String customer_group_id = "all";
            String selected = "";
            String incident_dashboard_queue_by_priority[][] = {{"Critical", "0", "Critical", "#a781e4"}, {"High", "0", "High", "#da5656"}, {"Medium", "0", "Medium", "#e8aa4e"}, {"Low", "0", "Low", "#6dba95"}};
            String incident_dashboard_number_of_incidents[][] = new String[0][0];
            String incident_dashboard_number_of_incidents_categories = "";
            String incident_dashboard_number_of_incidents_data = "";
            String incident_dashboard_count_by_priority_data = "";
            String incident_dashboard_top_chart_data[][] = new String[0][0];
            String incident_dashboard_top_5_categories = "";
            String incident_dashboard_top_5_data = "";
            String incident_dashboard_closure_rate_data[][] = new String[0][0];
            String incident_dashboard_closure_rate_categories = "";
            String incident_dashboard_closure_rate_low_data = "";
            String incident_dashboard_closure_rate_medium_data = "";
            String incident_dashboard_closure_rate_high_data = "";
            String incident_dashboard_closure_rate_critical_data = "";
            int total_incident_count = 0;
            int open_incident_count = 0;
            int closed_incident_count = 0;
            int total_incident_count_previous_period = 0;            
            int open_incident_count_previous_period = 0;            
            int closed_incident_count_previous_period = 0;            
            double this_period_csat_survey_score[] = {0,0,0,0};
            double previous_period_csat_survey_score[] = {0,0,0,0};
            GregorianCalendar first_of_month = new GregorianCalendar();
            first_of_month.set(Calendar.DAY_OF_MONTH, 1);
            first_of_month.set(Calendar.HOUR_OF_DAY, 0);
            first_of_month.set(Calendar.MINUTE, 0);
            first_of_month.set(Calendar.SECOND, 0);     
            GregorianCalendar now = new GregorianCalendar();
            try 
            {
                date_range = request.getParameter("date_range");
                if (date_range.equalsIgnoreCase("null") || date_range == null) 
                {
                    date_range = support.filter_dates.past_30_days();
                }
            } 
            catch (Exception e) 
            {
                // if not set then default to past 30 days
                // date_range = support.filter_dates.past_30_days();
                filter_start = first_of_month.getTime();
                filter_end = now.getTime();
                date_range = filter_format.format(filter_start) + " - " + filter_format.format(filter_end);

            }
            try 
            {
                support_group_id = request.getParameter("support_group_id");
                if (support_group_id.equalsIgnoreCase("null") || support_group_id == null) 
                {
                    support_group_id = "all";
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                support_group_id = "all";
            }
            try 
            {
                //01/12/2019 12:00 AM - 01/12/2019 11:59 PM&group_id=all
                //System.out.println("date_range=" + date_range);
                String temp[] = date_range.split("-");
                filter_start = filter_format.parse(temp[0]);  //filter_start = filter_format.parse("01/12/2019 12:00 AM");
                ZonedDateTime zdt_filter_start = support.date_utils.user_tz_to_utc(user_tz_name, timestamp_format.format(filter_start));
                filter_start = java.util.Date.from(zdt_filter_start.toInstant());  
                
                filter_end = filter_format.parse(temp[1]);  //System.out.println("filter_end=" + temp[1].trim() + " ====" + filter_end);
                ZonedDateTime zdt_filter_end = support.date_utils.user_tz_to_utc(user_tz_name, timestamp_format.format(filter_end));
                filter_end = java.util.Date.from(zdt_filter_end.toInstant());  
            
                start = timestamp_format.format(filter_start);
                end = timestamp_format.format(filter_end);
                
                

                //System.out.println("group_id=" + group_id);
                incident_dashboard_queue_by_priority = get_chart_data.incident_dashboard_queue_by_priority(con, filter_start, filter_end, support_group_id);
                
                
                for (int a = 0; a < incident_dashboard_queue_by_priority.length; a++) 
                {
                    
                    if (a == 0) 
                    {
                        incident_dashboard_count_by_priority_data = "{name: \"" + incident_dashboard_queue_by_priority[a][0] + "\","
                                + " y:" + incident_dashboard_queue_by_priority[a][1] + ","
                                + "priority: \"" + incident_dashboard_queue_by_priority[a][2] + "\","
                                + "color: \"" + incident_dashboard_queue_by_priority[a][3] + "\"}";
                    } 
                    else 
                    {
                        incident_dashboard_count_by_priority_data = incident_dashboard_count_by_priority_data + ",{name: \"" + incident_dashboard_queue_by_priority[a][0] + "\","
                                + " y:" + incident_dashboard_queue_by_priority[a][1] + ","
                                + "priority: \"" + incident_dashboard_queue_by_priority[a][2] + "\","
                                + "color: \"" + incident_dashboard_queue_by_priority[a][3] + "\"}";
                    }
                }
                //send the users tz to adjust the db utc 
                incident_dashboard_number_of_incidents = get_chart_data.incident_dashboard_number_of_incidents(con, user_tz_name, filter_start, filter_end, support_group_id);
                //12-Dec,y_value,12/12/2018 
                //build categories string categories: ['12-Dec', '13-Dec', '14-Dec', '15-Dec']
                for(int b =0; b < incident_dashboard_number_of_incidents.length; b++)
                {
                    if(b == 0)
                    {
                        incident_dashboard_number_of_incidents_categories = "'" + incident_dashboard_number_of_incidents[b][0] + "'";
                        incident_dashboard_number_of_incidents_data = "{y:" + incident_dashboard_number_of_incidents[b][1] + ", date:'" + incident_dashboard_number_of_incidents[b][2] + "', date_range:'" + incident_dashboard_number_of_incidents[b][3] + "'}";
                    }
                    else
                    {
                        incident_dashboard_number_of_incidents_categories = incident_dashboard_number_of_incidents_categories + ",'" + incident_dashboard_number_of_incidents[b][0] + "'";
                        incident_dashboard_number_of_incidents_data = incident_dashboard_number_of_incidents_data + ",{y:" + incident_dashboard_number_of_incidents[b][1] + ", date:'" + incident_dashboard_number_of_incidents[b][2] + "', date_range:'" + incident_dashboard_number_of_incidents[b][3] + "'}";
                    }
                    
                }
                
                //do top 5
                incident_dashboard_top_chart_data = get_chart_data.incident_dashboard_top_5_categories(con, filter_start, filter_end, support_group_id);
                for(int c = 0; c < incident_dashboard_top_chart_data.length;c++)
                {
                    if(c == 0)
                    {
                        incident_dashboard_top_5_categories = "'" + incident_dashboard_top_chart_data[c][0] + "'";  //'1','2','3','4','5'
                        incident_dashboard_top_5_data = "{category: '" + incident_dashboard_top_chart_data[c][0] + "',y:" + incident_dashboard_top_chart_data[c][1] + "}"; //{y: 7,category: "Cat 1"},
                    }
                    else
                    {
                        incident_dashboard_top_5_categories = incident_dashboard_top_5_categories +  ",'" + incident_dashboard_top_chart_data[c][0] + "'";
                        incident_dashboard_top_5_data = incident_dashboard_top_5_data + ",{category: '" + incident_dashboard_top_chart_data[c][0] + "',y:" + incident_dashboard_top_chart_data[c][1] + "}";
                    }
                }
                //closure rate
                //12-Dec,12/12/2018,Low_Rate,Med_Rate,Hi_Rate,Critical_Rate,date_range   
                incident_dashboard_closure_rate_data = get_chart_data.incident_dashboard_closure_rate(con, filter_start, filter_end, support_group_id);
                for(int d = 0; d < incident_dashboard_closure_rate_data.length;d++)
                {
                    if(d ==0)
                    {
                        incident_dashboard_closure_rate_categories = "'" + incident_dashboard_closure_rate_data[d][0].replace(",",".") + "'";
                        incident_dashboard_closure_rate_low_data = "{priority:'Low',y:" + incident_dashboard_closure_rate_data[d][2].replace(",",".") + ",date:'" +  incident_dashboard_closure_rate_data[d][1] + "',date_range:'" + incident_dashboard_closure_rate_data[d][6] + "'}" ;
                        incident_dashboard_closure_rate_medium_data = "{priority:'Medium',y:" + incident_dashboard_closure_rate_data[d][3].replace(",",".") + ",date:'" +  incident_dashboard_closure_rate_data[d][1] + "',date_range:'" + incident_dashboard_closure_rate_data[d][6] + "'}" ;
                        incident_dashboard_closure_rate_high_data = "{priority:'High',y:" + incident_dashboard_closure_rate_data[d][4].replace(",",".") + ",date:'" +  incident_dashboard_closure_rate_data[d][1] + "',date_range:'" + incident_dashboard_closure_rate_data[d][6] + "'}" ;;
                        incident_dashboard_closure_rate_critical_data = "{priority:'Critical',y:" + incident_dashboard_closure_rate_data[d][5].replace(",",".") + ",date:'" +  incident_dashboard_closure_rate_data[d][1] + "',date_range:'" + incident_dashboard_closure_rate_data[d][6] + "'}" ;
                    }
                    else
                    {
                        incident_dashboard_closure_rate_categories = incident_dashboard_closure_rate_categories + ",'" + incident_dashboard_closure_rate_data[d][0].replace(",",".") + "'";
                        incident_dashboard_closure_rate_low_data = incident_dashboard_closure_rate_low_data + ",{priority:'Low',y:" + incident_dashboard_closure_rate_data[d][2].replace(",",".") + ",date:'" +  incident_dashboard_closure_rate_data[d][1] + "',date_range:'" + incident_dashboard_closure_rate_data[d][6] + "'}" ;
                        incident_dashboard_closure_rate_medium_data = incident_dashboard_closure_rate_medium_data + ",{priority:'Medium',y:" + incident_dashboard_closure_rate_data[d][3].replace(",",".") + ",date:'" +  incident_dashboard_closure_rate_data[d][1] + "',date_range:'" + incident_dashboard_closure_rate_data[d][6] + "'}" ;
                        incident_dashboard_closure_rate_high_data = incident_dashboard_closure_rate_high_data + ",{priority:'High',y:" + incident_dashboard_closure_rate_data[d][4].replace(",",".") + ",date:'" +  incident_dashboard_closure_rate_data[d][1] + "',date_range:'" + incident_dashboard_closure_rate_data[d][6] + "'}" ;
                        incident_dashboard_closure_rate_critical_data = incident_dashboard_closure_rate_critical_data + ",{priority:'Critical',y:" + incident_dashboard_closure_rate_data[d][5].replace(",",".") + ",date:'" +  incident_dashboard_closure_rate_data[d][1] + "',date_range:'" + incident_dashboard_closure_rate_data[d][6] + "'}" ;
                    }
                }
                total_incident_count = get_incidents.incident_count_start_date_stop_date_group_id(con, filter_start, filter_end, support_group_id);
                open_incident_count = get_incidents.incident_open_count_start_date_stop_date_group_id(con, filter_start, filter_end, support_group_id);
                closed_incident_count = get_incidents.incident_closed_count_start_date_stop_date_group_id(con, filter_start, filter_end, support_group_id);
                //get pervious date frame
                previous_start_date.setTime(filter_start.getTime());
                previous_end_date.setTime(filter_end.getTime());                
                long diff = filter_end.getTime() - filter_start.getTime();
                previous_start_date.setTime(filter_start.getTime() - diff);
                previous_end_date.setTime(filter_end.getTime() - diff);
                total_incident_count_previous_period = get_incidents.incident_count_start_date_stop_date_group_id(con, previous_start_date, previous_end_date, support_group_id);
                open_incident_count_previous_period = get_incidents.incident_open_count_start_date_stop_date_group_id(con, previous_start_date, previous_end_date, support_group_id);
                closed_incident_count_previous_period = get_incidents.incident_closed_count_start_date_stop_date_group_id(con, previous_start_date, previous_end_date, support_group_id);
                
                
                //CSAT survey score
                String this_period_survey_results[][] = db.get_survey_results.csat_incident_view_group_start_date_stop_date(con,"support", support_group_id, filter_start, filter_end);
                String previous_period_survey_results[][] = db.get_survey_results.csat_incident_view_group_start_date_stop_date(con,"support", support_group_id, previous_start_date, previous_end_date);
                
                this_period_csat_survey_score = db.get_survey_results.csat_score_for_survey_results(this_period_survey_results);
                previous_period_csat_survey_score = db.get_survey_results.csat_score_for_survey_results(previous_period_survey_results);
                
                //String this_period_survey_stats[] = db.get_survey_results.is_trending_positive(this_period_survey_results); //negative_count,  negative or neutral, positive_count
                //this_period_positive_surveys = Integer.parseInt(this_period_survey_stats[2]);
                
                //String previous_period_survey_stats[] = db.get_survey_results.is_trending_positive(previous_period_survey_results); //negative_count,  negative or neutral, positive_count
                //previous_period_positive_surveys = Integer.parseInt(previous_period_survey_stats[2]);
                
                
            } catch (Exception e) 
            {
                //if not set then default to past 30 days
                System.out.println("incident_home.jsp exception=" + e);
            }

%>
<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>	
<!-- BEGIN VENDOR CSS-->
<!--<link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">-->
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
<!-- END VENDOR CSS-->
<!-- BEGIN MODERN CSS-->
<!--<link rel="stylesheet" type="text/css" href="app-assets/css/app.css">-->
<!-- END MODERN CSS-->
<!-- BEGIN Page Level CSS-->
<!--<link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">-->
<!--<link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">-->
<link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">

<!-- END Page Level CSS-->
<!-- BEGIN Custom CSS-->
<!--<link rel="stylesheet" type="text/css" href="assets/css/style.css">-->
<!-- END Custom CSS-->
<jsp:include page='menu_service_desk.jsp'>
    <jsp:param name="active_menu" value=""/>
</jsp:include>	
<input type="hidden" name="start" id="start" value="<%=start%>"/>
<input type="hidden" name="end" id="end" value="<%=end%>"/>

    <div class="clr whtbg p-10 mb-15 position-relative">
        <div class="float-right options">
            <ul>
                <li id="filters_dropdown" class="dInlineBlock dropdown ">
                    <div class="dInlineBlock clr menuHolder">
                        <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                        </a>
                        <div class="dropdown-menu dropdown-menu-right md p-10" x-placement="bottom-end" style="position: absolute; transform: translate3d(100px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                            <h5 class="medium-font boldFont pt-5 mb-10">All Filters</h5>
                            <form id="filter_form">
                                <ul>
                                    <li class="mb-10 clr">
                                        <div class="formField md  mr-0 full-width">
                                            <input class="no-border full-width datetime" type="text" name="filter_date_range" id="filter_date_range" value="<%=date_range%>">
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="formField md  mr-0 full-width">
                                            <select class="no-border" name="support_group_id" id="support_group_id">
                                                <option value="all">All</option>
                                                <%
                                                for (int a = 0; a < groups.size(); a++) 
                                                {
                                                    String this_group[] = groups.get(a);
                                                    selected = "";
                                                    if (this_group[0].equalsIgnoreCase(support_group_id)) 
                                                    {
                                                        selected = "SELECTED";
                                                    }
                                                    %>
                                                    <option <%=selected%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                    <%
                                                }
                                                %>
                                            </select>
                                        </div>
                                    </li>

                                </ul>
                                <div class="text-center pt-5 pb-5">
                                    <button type="button" class="btn md2 btn-primary-new customBtn mb-5" onclick="reload_page()">Apply Filters</button>
                                    <div class="clr"></div>
                                    <a href="#" class="">Reset Filters</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </li>

            </ul>
        </div>
        <h1 class="large-font boldFont">Analytics</h1>
        <div class="custom-tabs clr ">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" href="javascript:void(0)" data-href="incident_home_support.jsp" onclick="switchTab()" aria-selected="true">Incidents </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)" data-href="request_home_support.jsp" onclick="switchTab()" aria-selected="false">Request</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)" data-href="contact_home.jsp" onclick="switchTab()" aria-selected="false">Contacts</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" aria-selected="false">CSAT Surveys</a>
                </li>
            </ul><!-- Tab panes -->
        </div>
    </div>

    <div class=" clr mb-15 text-right ">
        <% if (!date_range.equals(""))
        {
        %>
            <div class="clr tag dInlineBlock formField md whtbg ml-5" style="width:350px">
                <span class="no-border full-width"><%=date_range%></span>
                <img class="icon" src="assets/images/svg/cross-icon.svg" alt="" onclick="reset_date_range()">
            </div>
        <%
        }
        %>
        <% if (!support_group_id.equals("all"))
        {
        %>
            <div class="clr tag dInlineBlock formField md whtbg ml-5">
                <strong>Support Group:</strong> <%=groups.get(Integer.parseInt(support_group_id))[1]%>
                <img class="icon" src="assets/images/svg/cross-icon.svg" alt="" onclick="reset_support_group()">
            </div>
        <%
        }
        %>
        <!-- <div class="formField md full-width mr-0 float-right whtbg">
            <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015" />
        </div> -->
    </div>

    <div class="group-info-holder clr">
        <div class="holder">
            <div class=" box mb-20 clr">
                <h5 class="boldFont basic-font mb-50" onclick="javascript:location.href='incident_home_support.jsp?date_range=<%=date_range%>&support_group_id=<%=support_group_id%>'; return false;" style="cursor: pointer;">Total Incidents</h5>
                <div class="float-left boldFont basic-font graph-index">
                    <%
                    String arrow = "";
                    if((total_incident_count - total_incident_count_previous_period) > 0)
                    {
                        arrow = "high";
                    }
                    else
                    {
                        arrow = "down";
                    }
                    %>
                    <span class="icon mr-5"><img src="assets/images/svg/<%=arrow%>-arrow-icon.svg" alt="" data-toggle="popover" data-content="The Change (+ or -) value is the difference from this period to the previous period." data-trigger="hover" data-original-title="Total Incidents"></span>
                    <%=total_incident_count - total_incident_count_previous_period%>   
                </div>
                <div class="float-right xxl-font boldFont">
                    <span onclick="javascript:location.href='incident_list.jsp?date_range=<%=date_range%>&group_type=support&group_id=<%=support_group_id%>'; return false;" style="cursor: pointer;"><%=total_incident_count%></span>
                </div>
            </div>
        </div>
        <div class="holder">
            <div class=" box mb-20 clr">
                <h5 class="boldFont basic-font mb-50">Open Incidents</h5>
                <div class="float-left boldFont basic-font graph-index">
                <%
                    arrow = "";
                    if((open_incident_count - open_incident_count_previous_period) > 0)
                    {
                        arrow = "high";
                    }
                    else
                    {
                        arrow = "down";
                    }
                %>
                    <span class="icon mr-5"><img src="assets/images/svg/<%=arrow%>-arrow-icon.svg" alt="" data-toggle="popover" data-content="The Change (+ or -) value is the difference from this period to the previous period." data-trigger="hover" data-original-title="Open Incidents"></span>
                    <%=open_incident_count - open_incident_count_previous_period%> 
                </div>
                <div class="float-right xxl-font boldFont">
                    <span onclick="javascript:location.href='incident_list.jsp?predefined=open&date_range=<%=date_range%>&group_type=support&group_id=<%=support_group_id%>'; return false;" style="cursor: pointer;"><%=open_incident_count%></span>
                </div>
            </div>
        </div>
        <div class="holder">
            <div class=" box mb-20 clr">
                <h5 class="boldFont basic-font mb-50">Closed Incidents</h5>
                <div class="float-left boldFont basic-font graph-index">
                <%
                    arrow = "";
                    if((closed_incident_count - closed_incident_count_previous_period) > 0)
                    {
                        arrow = "high";
                    }
                    else
                    {
                        arrow = "down";
                    }
                %>
                    <span class="icon mr-5"><img src="assets/images/svg/<%=arrow%>-arrow-icon.svg" alt="" data-toggle="popover" data-content="The Change (+ or -) value is the difference from this period to the previous period." data-trigger="hover" data-original-title="closed Incidents"></span>
                    <%=closed_incident_count - closed_incident_count_previous_period%> 
                </div>
                <div class="float-right xxl-font boldFont">
                    <span onclick="javascript:location.href='incident_list.jsp?state=closed&date_range=<%=date_range%>&group_type=support&group_id=<%=support_group_id%>'; return false;" style="cursor: pointer;"><%=closed_incident_count%></span>
                </div>
            </div>
        </div>
        <div class="holder">
            <div class=" box mb-20 clr" onclick="location.href='incident_service_compliance.jsp?referer=incident_home_support.jsp&date_range=<%=date_range%>';" style="cursor:pointer">
                <h5 class="boldFont basic-font mb-50">Service Compliance</h5>
                <div class="float-left boldFont basic-font graph-index">
                    <%
                    double previous_period_score = 0;
                    double this_period_score = 0;

                    //get all active/reportable contact SLAs
                    String reportable_incident_sla[][] = db.get_sla.all_reportable_incident(con);
                    //get scores for previous period
                    for(int a = 0; a < reportable_incident_sla.length;a++)
                    {
                        previous_period_score = previous_period_score + support.sla_incident_calc.for_sla_id(con, reportable_incident_sla[a][0], previous_start_date, previous_end_date);
                    }
                    //get the avg score for all sla's
                    previous_period_score = previous_period_score / reportable_incident_sla.length;
                    //get scores for this period
                    for(int a = 0; a < reportable_incident_sla.length;a++)
                    {
                        this_period_score = this_period_score + support.sla_incident_calc.for_sla_id(con, reportable_incident_sla[a][0], filter_start, filter_end);
                    }
                    //get the avg score for all sla's
                    this_period_score = this_period_score / reportable_incident_sla.length;

                    if(this_period_score < previous_period_score)
                    {
                        arrow = "down";
                    }
                    else if(this_period_score > previous_period_score)
                    {
                        arrow = "high";
                    }
                    else if(this_period_score == previous_period_score)
                    {
                        arrow = "la la-pause";
                    }
                    double score_diff = this_period_score - previous_period_score;
                    %>
                    <span class="icon mr-5"><img src="assets/images/svg/<%=arrow%>-arrow-icon.svg" alt="" data-toggle="popover" data-content="Service Compliance is the result of meeting service agreement criteria between a service provider and the user community that define measurable targets to be achieved. The displayed 'Value' is the actual Month-To-Date % of target achieved compared to the previous Month-To-Date % achievement." data-trigger="hover" data-original-title="Service Compliance"></span>
                    <%=String.valueOf(String.format("%1$,.2f", score_diff))%>%
                </div>
                <div class="float-right xxl-font boldFont">
                    <%=String.valueOf(String.format("%1$,.2f", this_period_score))%>%
                </div>
            </div>
        </div>
        <div class="holder">
            <div class=" box mb-20 clr" onclick="javascript:location.href='incident_survey.jsp?date_range=<%=date_range%>&support_group_id=<%=support_group_id%>&customer_group_id=all'; return false;" style="cursor: pointer;">
                <h5 class="boldFont basic-font mb-50">CSAT Surveys</h5>

                <div class="float-left boldFont basic-font graph-index">
                    <%
                    double survey_diff = this_period_csat_survey_score[3] - previous_period_csat_survey_score[3];
                    String total_survey_arrow = "high";
                    if(survey_diff > 0)
                    {
                        total_survey_arrow = "high";
                    }
                    else
                    {
                        if(survey_diff < 0)
                        {
                            total_survey_arrow = "down";
                        }
                        else
                        {
                            if(survey_diff == 0)
                            {
                                total_survey_arrow = "pause";
                            }
                        }
                    }
                    %>
                    <span class="icon mr-5"><img src="assets/images/svg/<%=total_survey_arrow%>-arrow-icon.svg" alt="" data-toggle="popover" data-content="Customer Satisfaction is a measure based on the collection of returned survey responses. The 'Value' is this periods average compared to the last periods average. The maximum survey value is 5." data-trigger="hover" data-original-title="Customer Satisfaction"></span>
                    <%=String.format( "%.2f", survey_diff)%>
                </div>
                <div class="float-right xxl-font boldFont">
                    <%=String.format( "%.2f", this_period_csat_survey_score[3] )%>
                </div>
            </div>
        </div>
    </div>

    <!--<div class="col-xl-2 col-lg-6 col-12">
        <a href="incident_tier_n_utilization.jsp">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <table width="100%">
                            <tr>
                                <td colspan="3"><h4 class="text-muted"><b>Tier N Participation</b></h4></td>
                            </tr>
                            <tr>
                                <td width="10%"><i class="ft-chevron-down info font-large-2" data-toggle="popover" data-content="Tier N Utilization is a measure of the average number of Incidents handled by an analyst or technical support group in a month MULTIPLIED by the average Incident handle time in minutes DIVIDED by the average number of days worked by that analyst in a month MULTIPLIED by the number of hours worked in a day MULTIPLIED by 60 minutes." data-trigger="hover" data-original-title="Tier N Utilization"></i></td>
                                <td><h5>-1.2%</h5></td>
                                <td align="right"><h2>43.6%</h2></td>
                            </tr>
                        </table>                            
                    </div>
                </div>
            </div>
        </a>
    </div>-->

    <div class="row">
        <div class="col-md-6">
            <div class=" box mb-20">
                <h4 class="boldFont basic-font mb-30">Incident Queue by Priority</h4>
                <div class="graph-holder text-center">
                    <div id="incident_queue_container" class="chart" style="min-width: 250px; height: 180px; margin: 0 auto"></div>
                </div>

            </div>
        </div>
        <div class="col-md-6">
            <div class=" box mb-20">
                <h4 class="boldFont basic-font mb-30">Number of Daily Incidents</h4>
                <div class="graph-holder text-center">
                    <div id="incident_number_container" class="chart" style="min-width: 250px; height: 180px; margin: 0 auto"></div>
                </div>

            </div>
        </div>
        <div class="col-md-6">
            <div class=" box mb-20">
                <h4 class="boldFont basic-font mb-30">Top 5 Categories</h4>
                <div class="graph-holder text-center">
                    <div id="incident_top_5_categories_container" class="chart" style="min-width: 250px; height: 180px; margin: 0 auto"></div>
                </div>

            </div>
        </div>
        <div class="col-md-6">
            <div class=" box mb-20">
                <h4 class="boldFont basic-font mb-30">Closure Rate</h4>
                <div class="graph-holder text-center">
                    <div id="incident_closure_rate_container" class="chart" style="min-width: 250px; height: 180px; margin: 0 auto"></div>
                </div>

            </div>
        </div>


    </div>

    <!--
    <div class="row">
        <div class="col-12">
            <h4 class="card-title">Investigate</h4>    
            <hr>
            <a href="incident_investigate_drilldown.jsp?type=queue">Incident Queue</a>
            <br>
            <a href="incident_investigate_drilldown.jsp?type=closure">Incident Closure Rate</a>
            <br>
            <a href="incident_investigate_drilldown.jsp?type=backlog">Incident Backlog</a>
            <br>
            <a href="incident_investigate_drilldown.jsp?type=category">Incident Count By Category</a>
            <br>
        </div>
    </div>
    -->
    <!-- end content here-->
        
        
<jsp:include page='footer.jsp'>
    <jsp:param name="parameter" value=""/>
</jsp:include>	
<!-- BEGIN VENDOR JS-->

<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN MODERN JS-->
<!--<script src="app-assets/js/core/app-menu.js"></script>
<script src="app-assets/js/core/app.js"></script>-->
<!-- END MODERN JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
<script src="assets/js/highcharts/highcharts.js"></script>
<script src="assets/js/highcharts/data.js"></script>
<script src="assets/js/highcharts/drilldown.js"></script>
<script src="assets/js/highcharts/exporting.js"></script>
<script src="app-assets/js/scripts/popover/popover.js"></script>

<script>
    function switchTab() {
        window.location.href = $(event.target).data('href') + "?date_range=" + encodeURIComponent(getQueryStringParameter("date_range")).replaceAll("%20", "+");
    }
        $(function () {
            $('#filters_dropdown').on('hide.bs.dropdown', function (e) 
            { // prevent dropdown close on autocomplete
                console.log("dropdown closed ", e);
                //            console.log(e.clickEvent.target.className);
                if (e.clickEvent && e.clickEvent.target && (e.clickEvent.target.className == "ui-menu-item-wrapper" || e.clickEvent.target.className == "icon" || e.clickEvent.target.className.indexOf("applyBtn") !== -1)) {
                    e.preventDefault();
                }
            });
        });
        function reset_date_range()
        {
            var support_group_id = document.getElementById("support_group_id").value;
            var URL = "incident_home_support.jsp?support_group_id=" + support_group_id + "&view=support";
            window.location.href = URL;
        }
        function reset_support_group()
        {
            var filter_date_range = document.getElementById("filter_date_range").value;
            var URL = "incident_home_support.jsp?date_range=" + filter_date_range + "&support_group_id=all&view=support";
            window.location.href = URL;
        }
        function reload_page()
        {
            var support_group_id = document.getElementById("support_group_id").value;
            var filter_date_range = document.getElementById("filter_date_range").value;
            var URL = "incident_home_support.jsp?date_range=" + filter_date_range + "&support_group_id=" + support_group_id + "&view=support";
            window.location.href = URL;
        }
        function load_customer_page()
        {
            var support_group_id = document.getElementById("support_group_id").value;
            var filter_date_range = document.getElementById("filter_date_range").value;
            var URL = "incident_home_customer.jsp?date_range=" + filter_date_range + "&group_id=" + support_group_id;
            window.location.href = URL;
        }
        function load_support_page()
        {
            var support_group_id = document.getElementById("support_group_id").value;
            var filter_date_range = document.getElementById("filter_date_range").value;
            var URL = "incident_home_support.jsp?date_range=" + filter_date_range + "&support_group_id=" + support_group_id;
            window.location.href = URL;
        }
</script>

<script>
    $(function () {
        Highcharts.chart('incident_queue_container',
                {
                    chart: {
                        type: 'column',
                    },
                    title: {
                        text: null
                    },
                    exporting: {
                        enabled: false
                    },
                    credits: {
                        enabled: false
                    },
                    legend: {
                        enabled: false
                    },
                    xAxis: {
                        type: 'category',
                        minorTickLength: 0,
                        tickLength: 0
                    },
                    yAxis: {
                        title: {
                            x: -10,
                            text: 'Incident Count'
                        }
                    },
                    plotOptions: {
                        column: {
                            dataLabels: {
                                enabled: true
                            }
                        },
                        series: {
                            cursor: 'pointer',
                            point: {
                                events: {
                                    click: function (event) {
                                        window.location = "incident_list.jsp?predefined=open&priority=" + event.point.priority + "&date_range=" + document.getElementById("filter_date_range").value + "&assigned_group_id=" + document.getElementById("support_group_id").value;
                                    }
                                }
                            }
                        }
                    },
                    series: [
                        {
                            name: "Incident Count",
                            color: '#64b5f6',
                            data: [
                                <%=incident_dashboard_count_by_priority_data%>
                            ]
                        }
                    ]
                });

    });
</script>
<script>
    Highcharts.chart('incident_number_container', {
        chart: {
            type: 'spline'
        },
        title: {
            text: null
        },
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: [<%=incident_dashboard_number_of_incidents_categories%>]
        },
        yAxis: {
            title: {
                x: -10,
                text: 'Number'
            }
        },

        plotOptions: {
            spline: {
                marker: {
                    enabled: false
                }
            },
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (event) {
                            window.location = "incident_list.jsp?date_range=" + event.point.date_range + "&assigned_group_id=" + document.getElementById("support_group_id").value;
                        }
                    }
                }
            }
        },
        series: [{
                name: "Number of Daily Incidents",
                color: '#64b5f6',
                data: [
                    <%=incident_dashboard_number_of_incidents_data%>
                ]
            }]
    });
</script>
<script>
    Highcharts.chart('incident_top_5_categories_container', {
            chart: {
                type: 'bar'
            },
            title: {
                text: null
            },
            exporting: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            xAxis: {
                categories: [<%=incident_dashboard_top_5_categories%>],
                lineWidth: 0,
                minorGridLineWidth: 0,
                lineColor: 'transparent',
                minorTickLength: 0,
                tickLength: 0
            },
            yAxis: {
                visible: false
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    },
                    borderWidth: 11,
                    cursor: 'pointer',
                    point: {
                        events:{
                            click: function(event) {
                                window.location = "incident_list.jsp?category=" + event.point.category + "&date_range=" + document.getElementById("filter_date_range").value + "&assigned_group_id=" + document.getElementById("support_group_id").value;
                            }
                        }
                    }
                }
            },
            series: [{
                name: 'Count',
                color: '#64b5f6',
                data: [<%=incident_dashboard_top_5_data%>]
            }]
        });
</script>
<script>
    Highcharts.chart('incident_closure_rate_container', {
            chart: {
                type: 'spline'
            },
            title: {
                text: null
            },
            exporting: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: [<%=incident_dashboard_closure_rate_categories%>]
            },
            yAxis: {
                title: {
                    x: -10,
                    text: 'Closure Rate'
                }
            },
            plotOptions: {
                spline: {
                    marker: {
                        enabled: false
                    }
                },
                series: {
                    events: {
                        click: function(event) {
                            window.location = "incident_list.jsp?state=closed&priority=" + event.point.priority + "&date_range=" + event.point.date_range + "&assigned_group_id=" + document.getElementById("support_group_id").value;
                        }
                    }
                }
            },
            series: [
                {
                    name: "Critical",
                    color: "#a781e4",
                    data:[<%=incident_dashboard_closure_rate_critical_data%>]
                },
                {
                    name: "High",
                    color: "#da5656",
                    data:[<%=incident_dashboard_closure_rate_high_data%>]
                },
                {
                    name: "Medium",
                    color: "#e8aa4e",
                    data:[<%=incident_dashboard_closure_rate_medium_data%>]
                },
                {
                    name: "Low",
                    color: '#6dba95',
                    data:[<%=incident_dashboard_closure_rate_low_data%>]
                }
            ]
        });
</script>
<!-- END PAGE LEVEL JS-->

</body>
</html>
<%
            con.close();
        }//end if user has permission
        else {
            String RedirectURL = "no_permission.jsp";
            response.sendRedirect(RedirectURL);
        }
    }//end if user is logged in
%>

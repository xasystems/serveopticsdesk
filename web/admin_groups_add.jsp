<!--Copyright 2020 Real Data Technologies, Inc. , All rights reserved.-->
<%-- 
    Document   : incident_new.jsp
    Created on : 18-Jan-2020, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>

<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String FINANCIAL = session.getAttribute("financial").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String CONTACT = session.getAttribute("contact").toString();
        String CX = session.getAttribute("cx").toString();
        String SURVEY = session.getAttribute("survey").toString();
        String SLA = session.getAttribute("sla").toString();        
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        
        
        
        if (ADMINISTRATION.equalsIgnoreCase("true") || MANAGER.equalsIgnoreCase("true")) 
        {
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
            java.util.Date now = new java.util.Date();
            String now_display_format = display_format.format(now);
            String now_timestamp_format = timestamp_format.format(now);
            String now_incident_time = date_time_picker_format.format(now);
            Connection con = null;
            try
            {
                con = db.db_util.get_contract_connection(context_dir, session); 
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
            }
            catch(Exception e)
            {
                System.out.println("admin_groups_add.jsp exception=" + e);
            }
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <!-- Page specific CSS-->
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <!--End Page specific CSS-->
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/selects/select2.min.css">
    <!-- END Page Level CSS-->
    
    
    <div class="app-content content">
        <form action="admin_groups_add" method="post">
            <!--future fields-->            
            <div class="content-wrapper">
                <!-- start content here-->
                <div class="content-header row">
                    <div class="content-header-left col-md-6 col-12 mb-2">
                        <h3 class="content-header-title"><i class="la la-group"></i>&nbsp;New User Group</h3>
                        <div class="row breadcrumbs-top">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a></li>
                                    <li class="breadcrumb-item"><a href="admin.jsp">Administration</a></li>
                                    <li class="breadcrumb-item"><a href="admin_groups.jsp">User Groups</a></li>
                                    <li class="breadcrumb-item active">Add</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Create a New Group</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>                                    
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2 col-lg-4 col-4">
                                            <label><br>Name</label>
                                            <div class='input-group'>
                                                <input name="name" id="name" type="text" value="" class="form-control" placeholder="Group Name"/>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-4 col-4">
                                            <label><br>Description</label>
                                            <div class='input-group'>
                                                <input name="description" id="description" type="text" value="" class="form-control" placeholder="Group Description"/>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-4 col-4">
                                            <label><br>Group POC&nbsp;<i class="la la-search" title="Enter a character to start a username lookup" style="font-size: 14px;"></i></label>
                                            <input type="hidden" name="poc_id" id="poc_id" value=""/>
                                            <input type="text" name="poc_username" id="poc_username" value="" class="form-control" placeholder="Enter the POC's username"/>
                                        </div>
                                    </div>                                    
                                    <div class="row">
                                        <div class="col-12 mt-1">
                                            <hr>
                                        </div>
                                    </div>      
                                    <div class="row">
                                        <div class="col-12">                                            
                                            <label><br>Select User(s) to add to Group&nbsp;<i class="la la-search" title="Enter a character to start a username lookup" style="font-size: 14px;"></i>
                                            <input type="text" size="60" name="username" id="username" value="" class="form-control" placeholder="Enter a character to start a db lookup"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label><br>Group Members&nbsp;<i class="la la-search" style="font-size: 14px;"></i></label>
                                            <select class="select2 form-control" id="user_ids" name="user_ids" multiple="multiple" placeholder="Select Group members">
                                                
                                            </select>
                                        </div>
                                    </div>                                            
                                    <div class="row">
                                        <div class="col-12 mt-1">
                                            <input class="btn btn-info" type="submit" name="submit" value="Submit"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>
                <!-- end content here-->
            </div> 
        </form>
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN VENDOR JS-->    
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="app-assets/js/core/app-menu.js"></script>
    <script src="app-assets/js/core/app.js"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/popover/popover.js"></script>    
    <script src="app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="app-assets/js/scripts/forms/select/form-select2.js"></script>

    
    <script>                
       $( function() 
       {                      
            //create_by
            $( "#poc_username" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#poc_username').val(ui.item.label); // display the selected text
                    $('#poc_id').val(ui.item.value); // save selected id to input
                    return false;
                }
            });            
        });
        
        $( function() 
        {
            //username
            $( "#username" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#username').val(ui.item.label); // display the selected text
                    $('#caller_id').val(ui.item.value); // save selected id to input
                    //if vip = true then check the vip checkbox
                    var user_id = ui.item.value;
                    var username = ui.item.label;                    
                    //add value to user_ids  user_id and username
                    var opt = document.createElement('option');
                    opt.value = user_id;
                    opt.innerHTML = username;
                    opt.selected = true;
                    var user_ids = document.getElementById("user_ids");
                    user_ids.appendChild(opt);
                    return false;
                }
            });
        });
        
    </script>
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>
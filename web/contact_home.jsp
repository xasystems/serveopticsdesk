<%@page import="db.get_incidents"%>
<%@page import="db.get_chart_data"%>
<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : incident_home
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>

<!DOCTYPE html>
<%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        if (!session.getAttribute("contact").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            String home_page = session.getAttribute("home_page").toString();
            //if you have the permission to read/modify or Admin then you can see this
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
            //SimpleDateFormat filter_format2 = new SimpleDateFormat("yyyy"); //01/12/2019 12:00 AM
            java.util.Date filter_start = new java.util.Date();
            java.util.Date filter_end = new java.util.Date();
            java.util.Date previous_start_date = new java.util.Date();
            java.util.Date previous_end_date = new java.util.Date();
            String start = "";
            String end = "";
            
            String date_range = "";
            String selected = "";
            try 
            {
                date_range = request.getParameter("date_range");
                if (date_range.equalsIgnoreCase("null") || date_range == null) 
                {
                    date_range = support.filter_dates.past_30_days();
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                date_range = support.filter_dates.past_30_days();
            }
            
            try 
            {
                
                //split the start and end date  //01/12/2019 12:00 AM - 01/12/2019 11:59 PM
                String temp[] = date_range.split("-");
                filter_start = filter_format.parse(temp[0]);
                filter_end = filter_format.parse(temp[1]);                
                start = timestamp.format(filter_start);
                end = timestamp.format(filter_end);

                //get pervious date frame                
                previous_start_date.setTime(filter_start.getTime());
                previous_end_date.setTime(filter_end.getTime());                
                long diff = filter_end.getTime() - filter_start.getTime();
                previous_start_date.setTime(filter_start.getTime() - diff);
                previous_end_date.setTime(filter_end.getTime() - diff);
                
                
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                System.out.println("contact_home.jsp exception=" + e);
            }

%>
<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>	
<!-- BEGIN VENDOR CSS-->
<!--<link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">-->
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
<!-- END VENDOR CSS-->
<!-- BEGIN MODERN CSS-->
<!--<link rel="stylesheet" type="text/css" href="app-assets/css/app.css">-->
<!-- END MODERN CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
<link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
<link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">

<!-- END Page Level CSS-->
<!-- BEGIN Custom CSS-->
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<!-- END Custom CSS-->
<jsp:include page='menu_service_desk.jsp'>
    <jsp:param name="active_menu" value=""/>
</jsp:include>	
<input type="hidden" name="start" id="start" value="<%=start%>"/>
<input type="hidden" name="end" id="end" value="<%=end%>"/>

<div class="clr whtbg p-10 mb-15 position-relative">
    <div class="float-right options">
        <ul>
            <li id="filters_dropdown" class="dInlineBlock dropdown ">
                <div class="dInlineBlock clr menuHolder">
                    <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                    </a>
                    <div class="dropdown-menu dropdown-menu-right md p-10" x-placement="bottom-end" style="position: absolute; transform: translate3d(100px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <h5 class="medium-font boldFont pt-5 mb-10">All Filters</h5>
                        <form id="filter_form">
                            <ul>
                                <li class="mb-10 clr">
                                    <div class="formField md  mr-0 full-width">
                                        <input class="no-border full-width datetime" type="text" name="filter_date_range" id="filter_date_range" value="<%=date_range%>">
                                    </div>
                                </li>
                            </ul>
                            <div class="text-center pt-5 pb-5">
                                <button type="button" class="btn md2 btn-primary-new customBtn mb-5" onclick="reload_page()">Apply Filters</button>
                                <div class="clr"></div>
                                <a href="#" class="">Reset Filters</a>
                            </div>
                        </form>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <h1 class="large-font boldFont">Contact</h1>
    <div class="custom-tabs clr ">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link" href="javascript:void(0)" data-href="incident_home_support.jsp" onclick="switchTab()" aria-selected="true">Incidents </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:void(0)" data-href="request_home_support.jsp" onclick="switchTab()" aria-selected="false">Request</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active show" href="javascript:void(0)" data-href="contact_home.jsp" onclick="switchTab()" aria-selected="false">Contacts</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" aria-selected="false">CSAT Surveys</a>
            </li>
        </ul><!-- Tab panes -->
    </div>
</div>

<div class=" clr mb-15 text-right ">
    <% if (!date_range.equals(""))
    {
    %>
        <div class="clr tag dInlineBlock formField md whtbg ml-5" style="width:350px">
            <span class="no-border full-width"><%=date_range%></span>
            <img class="icon" src="assets/images/svg/cross-icon.svg" alt="" onclick="reset_date_range()">
        </div>
    <%
    }
    %>
    <!-- <div class="formField md full-width mr-0 float-right whtbg">
        <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015" />
    </div> -->
</div>

    <div class="group-info-holder clr">
        <div class="holder">
            <div class=" box mb-20 clr">
                <h5 class="boldFont basic-font mb-50">Total Contacts</h5>
                <div class="float-left boldFont basic-font graph-index">
                    <%
                    int this_period_contact_count = db.get_contact.all_contacts_for_timeframe(con, filter_start, filter_end);
                    int previous_period_contact_count = db.get_contact.all_contacts_for_timeframe(con, previous_start_date, previous_end_date);
                    String arrow = "";
                    if(this_period_contact_count > previous_period_contact_count)
                    {
                        arrow = "high";
                    }
                    else if(this_period_contact_count < previous_period_contact_count)
                    {
                        arrow = "down";
                    }
                    else if(this_period_contact_count == previous_period_contact_count)
                    {
                        arrow = "la la-pause";
                    }
                    %>
                    <span class="icon mr-5"><img src="assets/images/svg/<%=arrow%>-arrow-icon.svg" alt="" data-toggle="popover" data-content="The Change (+ or -) value is the difference from this period to the previous period." data-trigger="hover" data-original-title="Total Contacts"></span>
                    <%=this_period_contact_count - previous_period_contact_count%>   
                </div>
                <div class="float-right xxl-font boldFont">
                    <span><%=this_period_contact_count%></span>
                </div>
            </div>
        </div>
        <div class="holder">
            <div class=" box mb-20 clr" onclick="location.href='contact_service_compliance.jsp?date_range=<%=date_range%>';" style="cursor:pointer">
                <h5 class="boldFont basic-font mb-50">Service Compliance</h5>
                <div class="float-left boldFont basic-font graph-index">
                    <%
                    double previous_period_score = 0.0;
                    double this_period_score = 0.0;

                    //get all active/reportable contact SLAs
                    String reportable_contact_sla[][] = db.get_sla.all_reportable_contact(con);
                    System.out.println("number of reportable_contact_sla=" + reportable_contact_sla.length);
                    //get scores for previous period
                    for(int a = 0; a < reportable_contact_sla.length;a++)
                    {
                        System.out.println("sla_id=" + reportable_contact_sla[a][0]);
                        System.out.println("support.sla_contact_calc.for_sla_id(con, reportable_contact_sla[a][0], previous_start_date, previous_end_date=" + support.sla_contact_calc.for_sla_id(con, reportable_contact_sla[a][0], previous_start_date, previous_end_date));
                        double this_calc = support.sla_contact_calc.for_sla_id(con, reportable_contact_sla[a][0], previous_start_date, previous_end_date);

                        if(!Double.isNaN(this_calc))
                        {
                            previous_period_score = previous_period_score + this_calc;
                        }
                    }
                    //System.out.println("previous_period_score=" + previous_period_score);
                    //get the avg score for all sla's
                    previous_period_score = previous_period_score / reportable_contact_sla.length;
                    System.out.println("previous_period_score=" + previous_period_score);
                    //get scores for this period

                    for(int a = 0; a < reportable_contact_sla.length;a++)
                    {
                        double this_calc = support.sla_contact_calc.for_sla_id(con, reportable_contact_sla[a][0], previous_start_date, previous_end_date);
                        if(!Double.isNaN(this_calc))
                        {
                            this_period_score = this_period_score + this_calc;
                        }
                    }
                    //get the avg score for all sla's
                    this_period_score = this_period_score / reportable_contact_sla.length;
                    System.out.println("this_period_score=" + this_period_score);


                    if(this_period_score < previous_period_score)
                    {
                        System.out.println("this_period_score < previous_period_score");
                        arrow = "down";
                    }
                    else if(this_period_score > previous_period_score)
                    {
                        System.out.println("this_period_score > previous_period_score");
                        arrow = "high";
                    }
                    else if(this_period_score == previous_period_score)
                    {
                        System.out.println("this_period_score == previous_period_score");
                        arrow = "la la-pause";
                    }

                    double score_diff = this_period_score - previous_period_score;

                    %>
                    <span class="icon mr-5"><img src="assets/images/svg/<%=arrow%>-arrow-icon.svg" alt="" data-toggle="popover" data-content="Service Compliance is the result of meeting service agreement criteria between a service provider and the user community that define measurable targets to be achieved. The displayed 'Value' is the actual Month-To-Date % of target achieved compared to the previous Month-To-Date % achievement." data-trigger="hover" data-original-title="Service Compliance"></span>
                    <%=String.valueOf(String.format("%1$,.2f", score_diff))%>%
                </div>
                <div class="float-right xxl-font boldFont">
                    <%=String.valueOf(String.format("%1$,.2f", this_period_score))%>%
                </div>
            </div>
        </div>
        <div class="holder">
            <div class=" box mb-20 clr">
                <h5 class="boldFont basic-font mb-50" onclick="javascript:location.href='contact_tier0_self_help.jsp'; return false;" style="cursor: pointer;">Tier 0 (Self-Help)</h5>
                <div class="float-left boldFont basic-font graph-index">
                    <%
                    int this_period_self_help = db.get_self_help.sessions_per_timeframe(con, filter_start, filter_end);
                    int previous_period_self_help = db.get_self_help.sessions_per_timeframe(con, previous_start_date, previous_end_date);
                    if(this_period_self_help > previous_period_self_help)
                    {
                        arrow = "high";
                    }
                    else if(this_period_self_help < previous_period_self_help)
                    {
                        arrow = "down";
                    }
                    else if(this_period_self_help == previous_period_self_help)
                    {
                        arrow = "la la-pause";
                    }
                    int self_help_diff = this_period_self_help - previous_period_self_help;
                    %>
                    <span class="icon mr-5"><img src="assets/images/svg/<%=arrow%>-arrow-icon.svg" alt="" data-toggle="popover" data-content="The number of Self-help sessions. " data-trigger="hover" data-original-title="Tier 0 Self Help"></span>
                    <%=self_help_diff%> 
                </div>
                <div class="float-right xxl-font boldFont">
                    <span><%=this_period_self_help%></span>
                </div>
            </div>
        </div>
        <div class="holder">
            <div class=" box mb-20 clr">
                <h5 class="boldFont basic-font mb-50" onclick="javascript:location.href='contact_fcr.jsp'; return false;" style="cursor: pointer;">First Contact Resolution</h5>
                <div class="float-left boldFont basic-font graph-index">
                    <%
                    int this_period_fcr_count = db.get_incidents.incident_fcr_count_start_date_stop_date(con, filter_start, filter_end);
                    int previous_period_fcr_count = db.get_incidents.incident_fcr_count_start_date_stop_date(con, previous_start_date, previous_end_date);
                    arrow = "";
                    if(this_period_fcr_count < previous_period_fcr_count)
                    {
                        arrow = "down";
                    }
                    else if(this_period_fcr_count > previous_period_fcr_count)
                    {
                        arrow = "high";
                    }
                    else if(this_period_fcr_count == previous_period_fcr_count)
                    {
                        arrow = "la la-pause";
                    }
                    %>
                    <span class="icon mr-5"><img src="assets/images/svg/<%=arrow%>-arrow-icon.svg" alt="" data-toggle="popover" data-content="First Contact Resolution is a measure based on the number of calls that are resolved on first contact." data-trigger="hover" data-original-title="First Contact Resolution"></span>
                    <%=this_period_fcr_count - previous_period_fcr_count%>
                </div>
                <div class="float-right xxl-font boldFont">
                    <span><%=this_period_fcr_count%></span>
                </div>
            </div>
        </div>
        <div class="holder">
            <div class=" box mb-20 clr">
                <h5 class="boldFont basic-font mb-50" onclick="javascript:location.href='contact_survey.jsp'; return false;" style="cursor: pointer;">CSAT Survey</h5>
                <div class="float-left boldFont basic-font graph-index">
                    <%
                    //int this_period_fcr_count = db.get_incidents.incident_fcr_count_start_date_stop_date(con, filter_start, filter_end);
                    //int previous_period_fcr_count = db.get_incidents.incident_fcr_count_start_date_stop_date(con, previous_start_date, previous_end_date);
                    arrow = "";
                    if(this_period_fcr_count < previous_period_fcr_count)
                    {
                        arrow = "down";
                    }
                    else if(this_period_fcr_count > previous_period_fcr_count)
                    {
                        arrow = "high";
                    }
                    else if(this_period_fcr_count == previous_period_fcr_count)
                    {
                        arrow = "la la-pause";
                    }
                    %>
                    <span class="icon mr-5"><img src="assets/images/svg/<%=arrow%>-arrow-icon.svg" alt="" data-toggle="popover" data-content="CSAT Survey results for all Contact types." data-trigger="hover" data-original-title="CSAT Surveys"></span>
                    <%=this_period_fcr_count - previous_period_fcr_count%>
                </div>
                <div class="float-right xxl-font boldFont">
                    <span><%=this_period_fcr_count%></span>
                </div>
            </div>
        </div>

    </div>
    <div class="formField md dInlineBlock mb-30 whtbg">
        <select id="contact_type" class="no-border" onchange="draw_tables()">
            <option value="all">Show All</option>
            <option value="phone">Phone Contacts</option>
            <option value="email">Email Contacts</option>
            <option value="chat">Chat Contacts</option>
            <option value="voice_mail">Voice Mail Contacts</option>
        </select>
    </div>

    <div id="contact_phone_wrapper" class="clr mb-30 d-none">
        <div class="top clr mb-15">
            <div class="float-right">
                <div class="clr searchFieldHolder dInline formField whtbg">
                    <span class="icon"><img src="assets/images/svg/searchIcon.svg" alt=""></span>
                    <input id="" type="text" dir="ltr" placeholder="Find anything...">
                </div> 
            </div>
            <h4 class="boldFont large-font pt-15">Phone Contacts</h4>
        </div>
        <div class="sort-tbl-holder">
            <table id="data_table_phone" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Channel</th>
                        <th>Volume</th>
                        <th>Avg ASA (Sec)</th>
                        <th>Avg. Handle Time(Sec)</th>
                        <th>Avg. Duration(Sec)</th>
                        <th>Avg. Wrap Up Time(Sec) </th>
                        <th>Abandoned Rate</th>
                    </tr>
                </thead>
            </table>
        </div>          
    </div>
    <div id="contact_email_wrapper" class="clr mb-30 d-none">
        <div class="top clr mb-15">
            <div class="float-right">
                <div class="clr searchFieldHolder dInline formField whtbg">
                    <span class="icon"><img src="assets/images/svg/searchIcon.svg" alt=""></span>
                    <input id="" type="text" dir="ltr" placeholder="Find anything...">
                </div> 
            </div>
            <h4 class="boldFont large-font pt-15">Email Contacts</h4>
        </div>
        <div class="sort-tbl-holder">
            <table id="data_table_email" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Channel</th>
                        <th>Volume</th>
                        <th>Avg ASA (Sec)</th>
                        <th>Avg. Handle Time(Sec)</th>
                        <th>Avg. Duration(Sec)</th>
                        <th>Avg. Wrap Up Time(Sec) </th>
                        <th>Abandoned Rate</th>
                    </tr>
                </thead>
            </table>
        </div>          
    </div>
    <div id="contact_chat_wrapper" class="clr mb-30 d-none">
        <div class="top clr mb-15">
            <div class="float-right">
                <div class="clr searchFieldHolder dInline formField whtbg">
                    <span class="icon"><img src="assets/images/svg/searchIcon.svg" alt=""></span>
                    <input id="" type="text" dir="ltr" placeholder="Find anything...">
                </div> 
            </div>
            <h4 class="boldFont large-font pt-15">Chat Contacts</h4>
        </div>
        <div class="sort-tbl-holder">
            <table id="data_table_chat" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Channel</th>
                        <th>Volume</th>
                        <th>Avg ASA (Sec)</th>
                        <th>Avg. Handle Time(Sec)</th>
                        <th>Avg. Duration(Sec)</th>
                        <th>Avg. Wrap Up Time(Sec) </th>
                        <th>Abandoned Rate</th>
                    </tr>
                </thead>
            </table>
        </div>          
    </div>
    <div id="contact_voice_mail_wrapper" class="clr mb-30 d-none">
        <div class="top clr mb-15">
            <div class="float-right">
                <div class="clr searchFieldHolder dInline formField whtbg">
                    <span class="icon"><img src="assets/images/svg/searchIcon.svg" alt=""></span>
                    <input id="" type="text" dir="ltr" placeholder="Find anything...">
                </div> 
            </div>
            <h4 class="boldFont large-font pt-15">Voice Mail Contacts</h4>
        </div>
        <div class="sort-tbl-holder">
            <table id="data_table_voice_mail" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Channel</th>
                        <th>Volume</th>
                        <th>Avg ASA (Sec)</th>
                        <th>Avg. Handle Time(Sec)</th>
                        <th>Avg. Duration(Sec)</th>
                        <th>Avg. Wrap Up Time(Sec) </th>
                        <th>Abandoned Rate</th>
                    </tr>
                </thead>
            </table>
        </div>          
    </div>
        
<jsp:include page='footer.jsp'>
    <jsp:param name="parameter" value=""/>
</jsp:include>	
<!-- BEGIN VENDOR JS-->

<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN MODERN JS-->
<!--<script src="app-assets/js/core/app-menu.js"></script>
<script src="app-assets/js/core/app.js"></script>-->
<!-- END MODERN JS-->
<!-- BEGIN PAGE LEVEL JS-->

<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
<script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
<script src="app-assets/js/scripts/popover/popover.js"></script>
<script>
    var dt_tables = [];
    function switchTab() {
        window.location.href = $(event.target).data('href') + "?date_range=" + encodeURIComponent(getQueryStringParameter("date_range")).replaceAll("%20", "+");
    }

    function reload_page()
    {
        var filter_date_range = document.getElementById("filter_date_range").value;
        var URL = "contact_home.jsp?date_range=" + filter_date_range;
        window.location.href = URL;
    }

    function draw_tables()
    {
        var contact_type_select = $("#contact_type");
        contact_type_select.children("option").each(function() {
            $("#contact_" + this.value + "_wrapper").addClass("d-none");
        });
        if (contact_type_select.val() == "all") 
        {
            contact_type_select.children("option").each(function() {
                draw_table(this.value);
            });
        } else {
            draw_table(contact_type_select.val());
        }
    }

    function draw_table(contact_type)
    {
        if (dt_tables[contact_type] != undefined) 
        {
            dt_tables[contact_type].draw();
        } else {
            dt_tables[contact_type] = $('#data_table_' + contact_type).DataTable( {
                language: {
                    "info":           "Showing _START_ to _END_ of _TOTAL_ contacts",
                    "infoEmpty":      "",
                    "infoFiltered":   "",
                    "lengthMenu":     "",
                    "zeroRecords":    "No matching contacts found",
                },
                "processing": true,
                "serverSide": true,
                "ordering": false,
                'serverMethod': 'post',
                'searching': false,
                "lengthChange": false,
                dom: "<'row'<'col-sm-12'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                "ajax": {
                    "url": "ajax_lookup_contacts",
                    'data': function(data){
                        data['date_range'] = $("#filter_date_range").val();
                        data['contact_type'] = contact_type;
                    }
                },
                "pageLength": 50,
                "columnDefs": [

    //                { targets: 0, defaultContent: '<span class="expand-td"><img src="assets/images/svg/collapse-minus-icon.svg" alt=""></span>', orderable: false },
                    { targets: 0, "data": "channel" },
                    { targets: 1, "data": "volume" },
                    { targets: 2, "data": "avg_asa" },
                    { targets: 3, "data": "avg_handle_time" },
                    { targets: 4, "data": "avg_duration" },
                    { targets: 5, "data": "wrap_up_time" },
                    { targets: 6, "data": "abandoned_rate" },
                ],
                "order": [],
                createdRow: function( row, data, dataIndex ) {
                    $( row )
                        .attr('data-id', data.id)
                        .attr('style', "cursor: pointer")
                        .attr('onclick', "javascript:location.href='contact_home_drilldown.jsp?date_range=" + $("#filter_date_range").val() + "&media_type=" + contact_type + "&channel=" + data.channel + "'; return false;")
                },
                "stripeClasses":[]
            } );
            
            dt_tables[contact_type].on( 'draw', function () {
    //            $(dt.table().node()).removeClass("d-none");
    //            $(dt.table().node()).removeClass("d-none");
                $("#contact_" + contact_type + "_wrapper").removeClass("d-none");
            });
            
        }


    }    
    window.addEventListener("load", (event) => 
    {
        draw_tables();
    });
</script>


<!-- END PAGE LEVEL JS-->

</body>
</html>
<%
            con.close();
        }//end if user has permission
        else {
            String RedirectURL = "no_permission.jsp";
            response.sendRedirect(RedirectURL);
        }
    }//end if user is logged in
%>

<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_knowledge_base_add.jsp
    Created on : Oct 4 2020, 6:13:59 PM
    Author     : rcampbell
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        boolean page_authorized = support.role.authorized(session, "ADMINISTRATION","read"); 
        
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
        
    %>
    <!-- BEGIN PAGE LEVEL CSS-->
    <!-- END PAGE LEVEL CSS-->

    <form action="admin_knowledge_base_add" method="POST">
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Name
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input name="name" id="name" type="text" value="" class="form-control" placeholder=""/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Description
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input name="description" id="description" type="text" value="" class="form-control" placeholder=""/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        State
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="state" id="state" class="form-control">
                        <option value="Draft">Draft</option>
                        <option value="Waiting_Approval">Awaiting Approval</option>
                        <option value="Published">Published</option>
                        <option value="Superceded">Superceded</option>
                        <option value="Expired">Expired</option>
                        <option value="Invalid">Invalid</option>
                        <option value="Deleted">Deleted</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Type
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="type" id="type" class="form-control">
                        <option value="Service_Desk">Service Desk</option>
                        <option value="Self_Service">Self Service</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-12 text-center pt-30 pb-30">
                <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                    Save
                </button>
            </div>
        </div>
    </form>
    <!-- BEGIN PAGE LEVEL JS-->    
    <!-- END PAGE LEVEL JS-->   
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_knowledge_base_add.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_knowledge_base_add.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
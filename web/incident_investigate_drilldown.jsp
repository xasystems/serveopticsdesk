
<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : incident_total_incidents
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="db.get_incidents"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        if (!session.getAttribute("incident").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            //create the go back url
            if(request.getHeader("referer").contains("incident_home.jsp"))
            {
                session.setAttribute("incident_drilldown_referer", request.getHeader("referer"));
            }
            String home_page = session.getAttribute("home_page").toString();
            String chart_type = request.getParameter("chart_type");
            String date_range = "";
            String category = "";
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session); 
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            ArrayList<String[]> incident_info = new ArrayList();
            String priority = "";
            java.util.Date start_date = new java.util.Date();
            java.util.Date end_date = new java.util.Date();
            ArrayList<String[]> groups = db.get_groups.all(con);
            String selected = "";
            String group_id = "";
            String date = "";
            try {
                group_id = request.getParameter("group_id");
                if (group_id.equalsIgnoreCase("null") || group_id == null) {
                    group_id = "all";
                }
            } catch (Exception e) {
                //if not set then default to past 30 days
                group_id = "all";
            }
            //if date_range is set override start and end dates
            if(request.getParameter("date_range") != null) //filter has been updated and applied so override the dates
            {
                //if date_range is set override the start and end
                try {
                    date_range = request.getParameter("date_range");
                    if (date_range.equalsIgnoreCase("null") || date_range == null) {
                        date_range = support.filter_dates.past_30_days();
                    }
                } catch (Exception e) {
                    //if not set then default to past 30 days
                    date_range = support.filter_dates.past_30_days();
                }
                String temp[] = date_range.split("-");
                start_date = filter_format.parse(temp[0]);
                //filter_start = filter_format.parse("01/12/2019 12:00 AM");
                //System.out.println("filter_start=" + temp[0] + " ====" + filter_start);

                end_date = filter_format.parse(temp[1]);
                //System.out.println("filter_end=" + temp[1].trim() + " ====" + filter_end);
            }
            else
            {
                //use the passed start and end
                if(chart_type.equalsIgnoreCase("number") || chart_type.equalsIgnoreCase("closure")) //number
                {
                    SimpleDateFormat chart_date_format = new SimpleDateFormat("MM/dd/yyyy");
                    try
                    {
                        start_date = chart_date_format.parse(request.getParameter("date"));
                    }
                    catch(Exception e)
                    {
                        start_date = new java.util.Date();
                    }
                    GregorianCalendar cal = new GregorianCalendar();
                    cal.setTime(start_date);
                    cal.set(cal.HOUR, 0);
                    cal.set(cal.MINUTE, 0);
                    cal.set(cal.SECOND, 0);
                    start_date.setTime(cal.getTimeInMillis());
                    cal.set(cal.HOUR, 23);
                    cal.set(cal.MINUTE, 59);
                    cal.set(cal.SECOND, 59);
                    end_date.setTime(cal.getTimeInMillis());
                    date_range =  filter_format.format(start_date) + " - " + filter_format.format(end_date);//12/19/2018 10:15 AM - 01/19/2019 10:15 AM
                }
                else
                {
                
                    start_date = timestamp_format.parse(request.getParameter("start_date"));
                    end_date = timestamp_format.parse(request.getParameter("stop_date"));
                    date_range =  filter_format.format(start_date) + " - " + filter_format.format(end_date);//12/19/2018 10:15 AM - 01/19/2019 10:15 AM
                }
            }
            //what kind of page 
            if(chart_type.equalsIgnoreCase("queue"))
            {
                priority = request.getParameter("priority");    
                //get the incidents
                incident_info = get_incidents.incident_chart_drilldown_queue(con, priority, start_date, end_date, group_id); 
            }
            else
            {
                if(chart_type.equalsIgnoreCase("number")) //number
                {
                    date = request.getParameter("date");  //12/25/2018
                    incident_info = get_incidents.incident_chart_drilldown_number(con, timestamp_format.format(start_date), timestamp_format.format(end_date), group_id); 
                    //incident_chart_drilldown_number(Connection con, String priority, java.util.Date start_date, java.util.Date end_date, String group_id)
                }
                else
                {
                    if(chart_type.equalsIgnoreCase("category")) //category
                    {
                        //?chart_type=category&category=Desktop%20Software&start_date=20181219071500&stop_date=20190119071500&group_id=all
                        category = request.getParameter("category");
                        incident_info = get_incidents.incident_chart_drilldown_category(con, category, start_date, end_date, group_id); 
                    }
                    else
                    {
                        if(chart_type.equalsIgnoreCase("closure")) //closure
                        {
                            //?chart_type=closure&priority=Medium&date=12/22/2018&group_id=all
                            date = request.getParameter("date");  //12/25/2018
                            priority = request.getParameter("priority");
                            incident_info = get_incidents.incident_chart_drilldown_closure(con, priority, start_date, end_date, group_id); 
                        }
                    }
                }
            }
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END Custom CSS-->
    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <input type="hidden" name="chart_type" id="chart_type" value="<%=chart_type%>"/>
    <input type="hidden" name="priority" id="priority" value="<%=priority%>"/>
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title"><i class="la la-wrench"></i>&nbsp;Incidents</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="#" onClick="parent.location='<%=session.getAttribute("incident_drilldown_referer")%>'"><i class="la la-angle-left"></i>Back</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- start content here-->
            <div class="row">
                <div class="col-xl-3 col-6">
                    <div class="form-group">
                        <label>Date Range</label>
                        <div class='input-group'>
                            <input type='text' id="filter_date_range" name="filter_date_range" value="<%=date_range%>" class="form-control datetime" />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <span class="la la-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-4">
                    <div class="form-group">
                        <label>Group</label>
                        <div class='input-group'>
                            <select class="select2-placeholder form-control" name="group_id" id="group_id">
                                <option value="all">All</option>
                                <%
                                for (int a = 0; a < groups.size(); a++) 
                                {
                                    String this_group[] = groups.get(a);
                                    selected = "";
                                    if (this_group[0].equalsIgnoreCase(group_id)) 
                                    {
                                        selected = "SELECTED";
                                    }
                                %>
                                <option <%=selected%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                <%
                                    }
                                %>
                            </select>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <span class="la la-group"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-1 col-2">
                    <label>&nbsp;</label>
                    <div class="form-actions">
                        <button type="button" onclick="reload_page()" class="btn btn-blue mr-1">
                            <i class="fa-filter"></i> Apply
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <table class="table table-striped table-bordered zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>Incident ID</th>
                                            <th>Priority</th>
                                            <th>Incident Time</th>
                                            <th>State</th>
                                            <th>State Time</th>
                                            <th>Group</th>
                                            <th>Analyst</th>
                                            <th>Category</th>
                                            <th>Sub-Category</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                        for(int a = 0; a < incident_info.size(); a++)
                                        {
                                            String this_incident[] = incident_info.get(a);
                                            String incident_time = "";
                                            try
                                            {
                                                incident_time = display_format.format(timestamp_format.parse(this_incident[2]));
                                            }
                                            catch (Exception e)
                                            {
                                                incident_time = "";
                                            }
                                            String state_date = "";
                                            try
                                            {
                                                state_date = display_format.format(timestamp_format.parse(this_incident[16]));
                                            }
                                            catch (Exception e)
                                            {
                                                state_date = "";
                                            }
                                            %>
                                            <tr onclick="location.href='incident.jsp?id=<%=this_incident[0]%>';" style="cursor:pointer;">
                                            <td><%=this_incident[0]%></td>
                                            <td><%=this_incident[10]%></td>
                                            <td><%=incident_time%></td>
                                            <td><%=this_incident[15]%></td>
                                            <td><%=state_date%></td>
                                            <td><%=this_incident[33]%></td>
                                            <td><%=this_incident[35] + " " + this_incident[37]%></td>
                                            <td><%=this_incident[5]%></td>
                                            <td><%=this_incident[6]%></td>
                                            </tr>
                                            <%
                                        }                                        
                                        %>                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Incident ID</th>
                                            <th>Priority</th>
                                            <th>Incident Time</th>
                                            <th>State</th>
                                            <th>State Time</th>
                                            <th>Group</th>
                                            <th>Analyst</th>
                                            <th>Category</th>
                                            <th>Sub-Category</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <script>
        function reload_page()
        {
            var group_id = document.getElementById("group_id").value;
            var chart_type = document.getElementById("chart_type").value;
            var priority = document.getElementById("priority").value;
            var filter_date_range = document.getElementById("filter_date_range").value;
            var URL = "incident_chart_drilldown.jsp?chart_type=" + chart_type + "&priority=" + priority + "&date_range=" + filter_date_range + "&group_id=" + group_id;
            window.location.href = URL;            
        }
    </script>
    <script>
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>


    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%      con.close();
        }//end if not permission
    }//end if not logged in
%>
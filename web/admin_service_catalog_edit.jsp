<!--Copyright 2020 Real Data Technologies, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_service_catalog_edit.jsp
    Created on : 18-Jan-2020, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>

    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String FINANCIAL = session.getAttribute("financial").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String CONTACT = session.getAttribute("contact").toString();
        String CX = session.getAttribute("cx").toString();
        String SURVEY = session.getAttribute("survey").toString();
        String SLA = session.getAttribute("sla").toString();        
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        if (ADMINISTRATION.equalsIgnoreCase("true") || MANAGER.equalsIgnoreCase("true")) 
        {
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = null;
            String id = request.getParameter("id");
            String service_catalog_info[] = {"","","",""};
            try
            {
                con = db.db_util.get_contract_connection(context_dir, session); 
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                service_catalog_info = db.get_service_catalog.service_catalog_by_id(con, id);
            }
            catch(Exception e)
            {
                System.out.println("admin_service_catalog_add.jsp exception=" + e);
            }
    %>
    <!-- BEGIN VENDOR CSS-->
    <!-- END VENDOR CSS-->    
    
    <form action="admin_service_catalog_edit" method="post" enctype="multipart/form-data" >
        <input type="hidden" name="id" id="id" value="<%=id%>"/>
        <!--future fields-->            
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Name
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input name="name" id="name" type="text" class="form-control" value="<%=service_catalog_info[1]%>" placeholder="Catalog Name"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Description
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input name="description" id="description" type="text" class="form-control" value="<%=service_catalog_info[2]%>" placeholder="Catalog Description"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        State
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="state" id="status" class="form-control">
                        <%
                        String selected = "";
                        String[] approver_status_names = {"Draft","Awaiting Approval","Published","Superceded","Expired","Invalid","Deleted"};
                        String[] approver_status_values = {"Draft","Waiting_Approval","Published","Superceded","Expired","Invalid","Deleted"};
                        for(int a = 0; a < approver_status_values.length; a++)
                        {
                            selected = "";
                            if(approver_status_values[a].equalsIgnoreCase(service_catalog_info[3]))
                            {
                                selected = "SELECTED";
                            }
                            %>
                            <option <%=selected%> value="<%=approver_status_values[a]%>"><%=approver_status_names[a]%></option>
                            <%
                        }                                                
                        %>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Type
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="catalog_type" id="catalog_type" class="form-control">
                        <%
                        String[] catalog_names = {"Self Service","Service Desk"};
                        String[] catalog_values = {"self_service","service_desk"};
                        for(int a = 0; a < catalog_values.length; a++)
                        {
                            selected = "";
                            if(catalog_values[a].equalsIgnoreCase(service_catalog_info[4]))
                            {
                                selected = "SELECTED";
                            }
                            %>
                            <option <%=selected%> value="<%=catalog_values[a]%>"><%=catalog_names[a]%></option>
                            <%
                        }                                                
                        %>
                    </select>
                </div>
            </div>
        </div>  
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md">
                        Choose an Image for this Catalog
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="clipper_result" name="file" onchange="window.clipper_app.$children[0].upload(event)">
                        <label class="custom-file-label" for="file">Choose file(s)</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row d-none" id="clipper_preview_row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only">
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <div class="clr dInlineBlock formField md whtbg mr-5 mb-15 float-none w-auto">
                        <span>
                            <img width="100px" id="clipper_preview" src="" />
                            <a href="javascript:void(0)" onclick="$(this).closest('div.row').addClass('d-none');$('#clipper_result').val('');event.preventDefault" >
                                <img class="icon pl-2" src="assets/images/svg/cross-icon.svg" alt="">
                            </a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <% if (service_catalog_info[6] != null && !service_catalog_info[6].equals("null")) { %>

            <div class="row" id="image_saved">
                <div class="col-md-12">
                    <div class="field-title">
                        <label class="formLabel md has-txt-only">
                        </label>
                    </div>
                    <div class="formField clr md px-0 border-0">
                        <div class="clr dInlineBlock formField md whtbg mr-5 mb-15 float-none w-auto">
                            <span>
                                <img width="100px" src="get_attachment?uuid=<%=service_catalog_info[6]%>&name=image" />
                                <a data-uuid="<%=service_catalog_info[6]%>" href="javascript:void(0)" onclick="deleteImage();event.preventDefault" >
                                    <img class="icon pl-2" src="assets/images/svg/cross-icon.svg" alt="">
                                </a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        <% } %>

        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md mb-15">
                        Choose an Icon for this Catalog
                    </label>
                </div>
                <div class="formField clr md mb-15"> <!-- style="overflow-y:scroll; height:400px;" -->
                    <%
                    String checked = "";
                    int row = 1;
                    ArrayList <String> icons = db.get_icons.all_icons(con);
                    for(int z = 0; z < icons.size(); z++)
                    {
                        if(icons.get(z).equalsIgnoreCase(service_catalog_info[5]))
                        {
                            checked = "CHECKED";
                        }
                        else
                        {
                            checked = "";
                        }
                        if(z ==  0)
                        {
                            out.println("<div class='row'>");
                        }
                        else
                        {
                            if ( z % 4 == 0)
                            {
                                out.println("<div class='row'>");
                            }
                        }                                
                        %>
                        <div class="col-md-3">
                            <div class="form-check form-check-inline full-width">
                              <input <%=checked%> class="form-check-input w-auto" type="radio" name="icon" id="inlineRadio<%=z%>" value="<%=icons.get(z)%>">
                              <label class="form-check-label full-width" for="inlineRadio<%=z%>"><i class="la <%=icons.get(z)%>"></i>&nbsp;<%=icons.get(z)%></label>
                            </div>                            
                        </div>                    
                    <%
                        if ( z > 0 && (z == (icons.size()-1) || (z+1) % 4 == 0))
                        {
                            out.println("</div>");
                        }
                    }
                    %>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-12 text-center pt-30 pb-30">
                <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                    Save
                </button>
            </div>
        </div>
    </form>

    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>
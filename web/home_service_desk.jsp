<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.sql.*" %>
<!DOCTYPE html>
    <%
    String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        
    if(session.getAttribute("authenticated")==null)
    {
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        String inc_chart_values = "[0,0,0,0]";
        String req_chart_values = "[0,0,0,0]";
        String prj_chart_values = "[0,0,0,0]";
        String job_chart_values = "[0,0,0,0]";
        session.setAttribute("home_page", "home_service_desk.jsp");
        try
        {
            String user_tz_name = "UTC";
            String user_tz_time = "+00:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }                
            }
            catch(Exception e)
            {
                user_tz_name = "UTC";
                user_tz_time = "+00:00";
            } 
            
            
            //page authorization
            //session vars
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
            
            GregorianCalendar first_of_month = new GregorianCalendar();
            first_of_month.set(Calendar.DAY_OF_MONTH, 1);
            first_of_month.set(Calendar.HOUR_OF_DAY, 0);
            first_of_month.set(Calendar.MINUTE, 0);
            first_of_month.set(Calendar.SECOND, 0);     
            GregorianCalendar now = new GregorianCalendar();
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            boolean has_a_display = false;

            //get role
            String administration = session.getAttribute("administration").toString();
            String manager = session.getAttribute("manager").toString();
            
            String support_group_id = "all";
            String customer_group_id = "all";
            java.util.Date filter_start = new java.util.Date();
            java.util.Date filter_end = new java.util.Date();
            String incident_dashboard_number_of_incidents[][] = new String[0][0];
            String incident_dashboard_number_of_incidents_categories = "";
            String incident_dashboard_number_of_incidents_data = "";
            String date_range = "";
            String start = "";
            String end = "";
            try 
            {
                date_range = request.getParameter("date_range");
                if (date_range.equalsIgnoreCase("null") || date_range == null) 
                {
                    date_range = support.filter_dates.past_30_days();
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days                
                date_range = support.filter_dates.past_30_days();
            }
            String temp[] = date_range.split("-");
            filter_start = filter_format.parse(temp[0]);
            //filter_start = filter_format.parse("01/12/2019 12:00 AM");
            filter_end = filter_format.parse(temp[1]);
            start = timestamp.format(filter_start);
            end = timestamp.format(filter_end);
            incident_dashboard_number_of_incidents = db.get_chart_data.incident_dashboard_number_of_incidents(con, user_tz_name, filter_start, filter_end, support_group_id);
            //12-Dec,y_value,12/12/2018 
            //build categories string categories: ['12-Dec', '13-Dec', '14-Dec', '15-Dec']
            for(int b = 0; b < incident_dashboard_number_of_incidents.length; b++)
            {
                if(b == 0)
                {
                    incident_dashboard_number_of_incidents_categories = "'" + incident_dashboard_number_of_incidents[b][0] + "'";
                    incident_dashboard_number_of_incidents_data = "{y:" + incident_dashboard_number_of_incidents[b][1] + ", date:'" + incident_dashboard_number_of_incidents[b][2] + "', date_range:'" + incident_dashboard_number_of_incidents[b][3] + "'}";
                }
                else
                {
                    incident_dashboard_number_of_incidents_categories = incident_dashboard_number_of_incidents_categories + ",'" + incident_dashboard_number_of_incidents[b][0] + "'";
                    incident_dashboard_number_of_incidents_data = incident_dashboard_number_of_incidents_data + ",{y:" + incident_dashboard_number_of_incidents[b][1] + ", date:'" + incident_dashboard_number_of_incidents[b][2] + "', date_range:'" + incident_dashboard_number_of_incidents[b][3] + "'}";
                }
            }
            String request_dashboard_number_of_request_categories = "";
            String request_dashboard_number_of_requests_data = "";
            String request_dashboard_number_of_requests[][] = db.get_chart_data.request_dashboard_number_of_requests_support(con, filter_start, filter_end, support_group_id);
            //12-Dec,y_value,12/12/2018 
            //build categories string categories: ['12-Dec', '13-Dec', '14-Dec', '15-Dec']
            for(int b =0; b < request_dashboard_number_of_requests.length; b++)
            {
                if(b == 0)
                {
                    request_dashboard_number_of_request_categories = "'" + request_dashboard_number_of_requests[b][0] + "'";
                    request_dashboard_number_of_requests_data = "{y:" + request_dashboard_number_of_requests[b][1] + ", date:'" + request_dashboard_number_of_requests[b][2] + "', date_range:'" + request_dashboard_number_of_requests[b][3] + "'}";
                }
                else
                {
                    request_dashboard_number_of_request_categories = request_dashboard_number_of_request_categories + ",'" + request_dashboard_number_of_requests[b][0] + "'";
                    request_dashboard_number_of_requests_data = request_dashboard_number_of_requests_data + ",{y:" + request_dashboard_number_of_requests[b][1] + ", date:'" + request_dashboard_number_of_requests[b][2] + "', date_range:'" + request_dashboard_number_of_requests[b][3] + "'}";
                }
            }
            
   %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <!--Start Page level css-->
    <!--End Page level css-->

    <div class="clr whtbg p-10 mb-15 position-relative">
        <h1 class="large-font boldFont">Service Desk Home</h1>
    </div>
    
    <div class="row">
                    <%
                    //incident CRUD
                    boolean incident_authorized = support.role.authorized(session, "incident","read");
                    ArrayList <Integer> incident_values = new ArrayList();
                    String incident_display = "none";
                    if(incident_authorized)
                    {
                        incident_display = "inline";
                        has_a_display = true;
                        //get my incident counts
                        incident_values = db.get_incidents.dashboard_incidents_count(con);
                        inc_chart_values = "[" + incident_values.get(1) + "," + incident_values.get(2) + "," + incident_values.get(3) + "," + incident_values.get(4) + "]";
                    }
                    else
                    {
                        incident_display = "none";
                    } 
                    %>

        <div class="col-md-6">
            <div class=" box mb-20" style="min-height: 235px;">
                <a class="btn btn-success customBtn md waves-effect float-right" href="incident_list.jsp?action=new">
                    <span class="btn-label -icon"><img src="assets/images/svg/plus-icon.svg" alt=""></span>
                    New
                </a>
                <h4 class="boldFont medium-font mb-30"><span class="icon mr-10"><img src="assets/images/svg/wrench-icon.svg" alt=""></span>Incidents</h4>
                <div class="row align-items-center">
                    <div class="col-md-3 pr-0">
                        <div class="graph-holder text-center">
                            <canvas id="incident_doughnut" height="250" data-toggle="tooltip" data-placement="top"></canvas>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="border-bottom" style="border-bottom-style:dashed !important">
                            <%
                            int unassigned_count = db.get_incidents.unassigned_incident_count(con);
                            %>
                            <a class="float-right" style="text-decorations:none; color:#64b5f6;" href="incident_list.jsp?predefined=unassigned"><%=unassigned_count%></a>
                            Unassigned
                        </div>
                        <div class="border-bottom" style="border-bottom-style:dashed !important">
                            <%
                            int open_count = db.get_incidents.open_incident_count(con);
                            %>
                            <a class="float-right" style="text-decorations:none; color:#64b5f6;" href="incident_list.jsp?predefined=open"><%=open_count%></a>
                            Open
                        </div>
                        <div class="border-bottom" style="border-bottom-style:dashed !important">
                            <%
                            int create_today_count = db.get_incidents.created_today_incident_count(con);
                            %>
                            <a class="float-right" style="text-decorations:none; color:#64b5f6;" href="incident_list.jsp?predefined=created_today"><%=create_today_count%></a>
                            Created Today 
                        </div>
                        <div class="border-bottom" style="border-bottom-style:dashed !important">
                            <%
                            int closed_today_count = db.get_incidents.closed_today_incident_count(con);
                            %>
                            <a class="float-right" style="text-decorations:none; color:#64b5f6;" href="incident_list.jsp?predefined=closed_today"><%=closed_today_count%></a>
                            Closed Today 
                        </div>
                        <div class="border-bottom" style="border-bottom-style:dashed !important">
                            <%
                            int open_7_14_count = db.get_incidents.incidents_open_7_14_days_count(con);
                            %>
                            <a class="float-right" style="text-decorations:none; color:#64b5f6;" href="incident_list.jsp?predefined=open_7_14"><%=open_7_14_count%></a>
                            Open 7-14 Days
                        </div>
                        <div class="border-bottom" style="border-bottom-style:dashed !important">
                            <%
                            int open_14_30_count = db.get_incidents.incidents_open_14_30_days_count(con);
                            %>
                            <a class="float-right" style="text-decorations:none; color:#64b5f6;" href="incident_list.jsp?predefined=open_14_30"><%=open_14_30_count%></a>
                            Open 14-30 Days
                        </div>
                        <div class="border-bottom" style="border-bottom-style:dashed !important">
                            <%
                            int open_30_plus_days_count = db.get_incidents.incidents_open_30_plus_days_count(con);
                            %>
                            <a class="float-right" style="text-decorations:none; color:#64b5f6;" href="incident_list.jsp?predefined=open_30_plus"><%=open_30_plus_days_count%></a>
                            Open 30+ Days
                        </div>

                    </div>

                </div>
            </div>
        </div>

                 <%
                    //request CRUD
                    ArrayList <Integer> request_values = new ArrayList();
                    boolean request_authorized = support.role.authorized(session, "request","read");
                    String request_display = "none";
                    if(request_authorized)
                    {
                        request_display = "inline";
                        has_a_display = true;
                        //get my incident counts
                        request_values = db.get_requests.dashboard_requests_count(con);
                        req_chart_values = "[" + request_values.get(1) + "," + request_values.get(2) + "," + request_values.get(3) + "," + request_values.get(4) + "]";
                    }
                    else
                    {
                        request_display = "none";
                    }
                    %>
    
        <div class="col-md-6">
            <div class=" box mb-20 clr" style="min-height: 235px;">
                <a class="btn btn-success customBtn md waves-effect float-right" href="request_list.jsp?action=new">
                    <span class="btn-label -icon"><img src="assets/images/svg/plus-icon.svg" alt=""></span>
                    New
                </a>
                <h5 class="boldFont medium-font mb-30"><span class="icon mr-10"><img src="assets/images/svg/cart-icon.svg" alt=""></span>Requests</h5>
                <div class="row align-items-center">
                    <div class="col-md-3 pr-0">
                        <div class="graph-holder text-center">
                            <canvas id="request_doughnut" height="250" data-toggle="tooltip" data-placement="top"></canvas>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="border-bottom" style="border-bottom-style:dashed !important">
                            <%
                            int unassigned_requests_count = db.get_requests.requests_unassigned_count(con);
                            %>
                            <a class="float-right" style="text-decorations:none; color:#64b5f6;" href="request_list.jsp?predefined=unassigned"><%=unassigned_requests_count%></a>
                            Unassigned
                        </div>
                        <div class="border-bottom" style="border-bottom-style:dashed !important">
                            <%
                            int request_open_count = db.get_requests.requests_open_count(con);
                            %>
                            <a class="float-right" style="text-decorations:none; color:#64b5f6;" href="request_list.jsp?predefined=open"><%=request_open_count%></a>
                            Open
                        </div>
                        <div class="border-bottom" style="border-bottom-style:dashed !important">
                            <%
                            int requests_create_today_count = db.get_requests.requests_created_today_count(con);
                            %>
                            <a class="float-right" style="text-decorations:none; color:#64b5f6;" href="request_list.jsp?predefined=created_today"><%=requests_create_today_count%></a>
                            Created Today 
                        </div>
                        <div class="border-bottom" style="border-bottom-style:dashed !important">
                            <%
                            int requests_closed_today_count = db.get_requests.requests_closed_today_count(con);
                            %>
                            <a class="float-right" style="text-decorations:none; color:#64b5f6;" href="request_list.jsp?predefined=closed_today"><%=requests_closed_today_count%></a>
                            Closed Today 
                        </div>
                        <div class="border-bottom" style="border-bottom-style:dashed !important">
                            <%
                            int request_open_7_14_count = db.get_requests.requests_open_7_14_count(con);
                            %>
                            <a class="float-right" style="text-decorations:none; color:#64b5f6;" href="request_list.jsp?predefined=open_7_14"><%=request_open_7_14_count%></a>
                            Open 7-14 Days
                        </div>
                        <div class="border-bottom" style="border-bottom-style:dashed !important">
                            <%
                            int requests_open_14_30_count = db.get_requests.requests_open_14_30_count(con);
                            %>
                            <a class="float-right" style="text-decorations:none; color:#64b5f6;" href="request_list.jsp?predefined=open_14_30"><%=requests_open_14_30_count%></a>
                            Open 14-30 Days
                        </div>
                        <div class="border-bottom" style="border-bottom-style:dashed !important">
                            <%
                            int open_30_plus_count = db.get_requests.requests_open_30_plus_days_count(con);
                            %>
                            <a class="float-right" style="text-decorations:none; color:#64b5f6;" href="request_list.jsp?predefined=open_30_plus"><%=open_30_plus_count%></a>
                            Open 30+ Days
                        </div>

                    </div>
                </div>
            </div>
        </div>

                    <%
                    ArrayList <Integer> project_values = new ArrayList();
                    int task_count = 0;
                    //project CRUD
                    boolean project_authorized = support.role.authorized(session, "project","read");
                    String project_display = "none";
                    if(project_authorized)
                    {
                        project_display = "inline";
                        has_a_display = true;
                        //get my incident counts
                        project_values = db.get_projects.projects(con);
                        prj_chart_values = "[" + project_values.get(1) + "," + project_values.get(2) + "," + project_values.get(3) + "," + project_values.get(4) + "]";
                        task_count = db.get_projects.open_project_tasks_count(con);
                    }
                    else
                    {
                        project_display = "none";
                    }
                    %>        
        <div class="col-md-6">
            <div class=" box mb-20 clr" style="min-height: 235px;">
                <a class="btn btn-success customBtn md waves-effect float-right" href="project_list.jsp?action=new">
                    <span class="btn-label -icon"><img src="assets/images/svg/plus-icon.svg" alt=""></span>
                    New
                </a>
                <h5 class="boldFont medium-font mb-30"><span class="icon mr-10"><img src="assets/images/svg/tasks-icon.svg" alt=""></span>Projects</h5>
                <div class="row align-items-center">
                    <div class="col-md-3 pr-0">
                        <div class="graph-holder text-center">
                            <canvas id="project_doughnut" height="250" data-toggle="tooltip" data-placement="top"></canvas>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="border-bottom" style="border-bottom-style:dashed !important">
                            <%
                            int open_projects_count = db.get_projects.all_open_projects_count(con);
                            %>
                            <a class="float-right" style="text-decorations:none; color:#64b5f6;" href="project_list.jsp?predefined=open"><%=open_projects_count%></a>
                            Open Projects
                        </div>
                        <div class="border-bottom" style="border-bottom-style:dashed !important">
                            <%
                            int open_project_tasks_count = db.get_projects.open_project_tasks_count(con);
                            %>
                            <a class="float-right" style="text-decorations:none; color:#64b5f6;" href="project_task_list.jsp?filter=all_open_tasks"><%=open_project_tasks_count%></a>
                            Open Tasks
                        </div>
                        <div class="border-bottom" style="border-bottom-style:dashed !important">
                            <%
                            int delinquent_project_tasks_count = db.get_projects.delinquent_project_tasks_count(con);
                            %>
                            <a class="float-right" style="text-decorations:none; color:#64b5f6;" href="project_task_list.jsp?filter=all_delinquent"><%=delinquent_project_tasks_count%></a>
                            Delinquent Tasks
                        </div>
                    </div>
                </div>
            </div>
        </div>

                    <%
                    ArrayList <Integer> job_values = new ArrayList();
                    //request CRUD
                    boolean job_authorized = support.role.authorized(session, "job","read");
                    String job_display = "none";
                    if(job_authorized)
                    {
                        job_display = "inline";
                        has_a_display = true;
                        job_values = db.get_jobs.open_jobs_count_by_priority(con);
                        job_chart_values = "[" + job_values.get(1) + "," + job_values.get(2) + "," + job_values.get(3) + "," + job_values.get(4) + "]";
                    }
                    else
                    {
                        job_display = "none";
                    }
                    %>        
        <div class="col-md-6">
            <div class=" box mb-20 clr" style="min-height: 235px;">
                <a class="btn btn-success customBtn md waves-effect float-right" href="job_list.jsp?action=new">
                    <span class="btn-label -icon"><img src="assets/images/svg/plus-icon.svg" alt=""></span>
                    New
                </a>
                <h5 class="boldFont medium-font mb-30"><span class="icon mr-10"><img src="assets/images/svg/jobs-icon.svg" alt=""></span>Other Tasks</h5>
                <div class="row align-items-center">
                    <div class="col-md-3 pr-0">
                        <div class="graph-holder text-center">
                            <canvas id="job_doughnut" height="250" data-toggle="tooltip" data-placement="top"></canvas>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="border-bottom" style="border-bottom-style:dashed !important">
                            <%
                            int open_job_count = db.get_jobs.open_jobs_count(con);
                            %>
                            <a class="float-right" style="text-decorations:none; color:#64b5f6;" href="job_list.jsp?predefined=all_open_jobs"><%=open_job_count%></a>
                            Open Other Tasks
                        </div>
                        <div class="border-bottom" style="border-bottom-style:dashed !important">
                            <%
                            int delinquent_job_count = db.get_jobs.delinquent_jobs_count(con);
                            %>
                            <a class="float-right" style="text-decorations:none; color:#64b5f6;" href="job_list.jsp?predefined=delinquent"><%=delinquent_job_count%></a> 
                            Delinquent Other Tasks
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class=" box mb-20">
                <h4 class="boldFont medium-font mb-30">Number of Incidents and Requests per Day</h4>
                <div class="graph-holder text-center">
                    <div id="incident_number_container" class="chart" style="min-width: 250px; height: 240px; margin: 0 auto"></div>
                </div>

            </div>
        </div>
    </div>

    <!-- BEGIN PAGE LEVEL JS-->
    <script src="assets/js/highcharts/highcharts.js"></script>
    <script src="assets/js/highcharts/data.js"></script>
    <script src="assets/js/highcharts/drilldown.js"></script>
    <script src="assets/js/highcharts/exporting.js"></script>
    
    <script src="app-assets/vendors/js/charts/chart.min.js"></script>
    
    <script>
        var incident_div = document.getElementById("incident_doughnut");
        var ctxP = incident_div.getContext('2d');
        var incident_doughnut = new Chart(ctxP, {
           type: 'doughnut',
           data: {
              labels: ["Critical", "High", "Medium", "Low"],
              datasets: [{
                 data: <%=inc_chart_values%>,
                  backgroundColor: [
                        '#ef5350',    // color for data at index 0
                        '#ffa726',   // color for data at index 1
                        '#ffee58',  // color for data at index 2
                        '#64b5f6'  // color for data at index 3
                    ],
                 hoverBackgroundColor: ["#DF0101", "#FFBF00", "#FFFF00", "#00BFFF"]
              }]
           },
           options: {
              legend: {
                 display: false
              }
           },
        });

        incident_div.onclick = function(e) {
           var slice = incident_doughnut.getElementAtEvent(e);
           if (!slice.length) return; // return if not clicked on slice
           var label = slice[0]._model.label;
           switch (label) {
              // add case for each label/slice
              case 'Critical':
                 parent.location='incident_list.jsp?predefined=all_open_critical';
                 break;
              case 'High':
                 parent.location='incident_list.jsp?predefined=all_open_high';
                 break;
              case 'Medium':
                 parent.location='incident_list.jsp?predefined=all_open_medium';
                 break;
              case 'Low':
                 parent.location='incident_list.jsp?predefined=all_open_low';
                 break;
           }
        }
    </script>
    <script>
        var request_div = document.getElementById("request_doughnut");
        var ctxP = request_div.getContext('2d');
        var request_doughnut = new Chart(ctxP, {
           type: 'doughnut',
           data: {
              labels: ["Critical", "High", "Medium", "Low"],
              datasets: [{
                 data: <%=req_chart_values%>,
                  backgroundColor: [
                        '#ef5350',    // color for data at index 0
                        '#ffa726',   // color for data at index 1
                        '#ffee58',  // color for data at index 2
                        '#64b5f6'  // color for data at index 3
                    ],
                 hoverBackgroundColor: ["#DF0101", "#FFBF00", "#FFFF00", "#00BFFF"]
              }]
           },
           options: {
              legend: {
                 display: false
              }
           }
        });

        request_div.onclick = function(e) {
           var slice = request_doughnut.getElementAtEvent(e);
           if (!slice.length) return; // return if not clicked on slice
           var label = slice[0]._model.label;
           switch (label) {
              // add case for each label/slice
              case 'Critical':
                 parent.location='request_list.jsp?predefined=all_open_critical';
                 break;
              case 'High':
                 parent.location='request_list.jsp?predefined=all_open_high';
                 break;
              case 'Medium':
                 parent.location='request_list.jsp?predefined=all_open_medium';
                 break;
              case 'Low':
                 parent.location='request_list.jsp?predefined=all_open_low';
                 break;
           }
        }
    </script>
    <script>
        var project_div = document.getElementById("project_doughnut");
        var ctxP = project_div.getContext('2d');
        var project_doughnut = new Chart(ctxP, {
           type: 'doughnut',
           data: {
              labels: ["Critical", "High", "Medium", "Low"],
              datasets: [{
                 data: <%=prj_chart_values%>,
                  backgroundColor: [
                        '#ef5350',    // color for data at index 0
                        '#ffa726',   // color for data at index 1
                        '#ffee58',  // color for data at index 2
                        '#64b5f6'  // color for data at index 3
                    ],
                 hoverBackgroundColor: ["#DF0101", "#FFBF00", "#FFFF00", "#00BFFF"]
              }]
           },
           options: {
              legend: {
                 display: false
              }
           }
        });

        project_div.onclick = function(e) {
           var slice = project_doughnut.getElementAtEvent(e);
           if (!slice.length) return; // return if not clicked on slice
           var label = slice[0]._model.label;
           switch (label) {
              // add case for each label/slice
              case 'Critical':
                 parent.location='project_list.jsp?predefined=all_open_critical';
                 break;
              case 'High':
                 parent.location='project_list.jsp?predefined=all_open_high';
                 break;
              case 'Medium':
                 parent.location='project_list.jsp?predefined=all_open_medium';
                 break;
              case 'Low':
                 parent.location='project_list.jsp?predefined=all_open_low';
                 break;
           }
        }
    </script>
    <script>
        var job_div = document.getElementById("job_doughnut");
        var ctxP = job_div.getContext('2d');
        var job_doughnut = new Chart(ctxP, {
           type: 'doughnut',
           data: {
              labels: ["Critical", "High", "Medium", "Low"],
              datasets: [{
                 data: <%=job_chart_values%>,
                  backgroundColor: [
                        '#ef5350',    // color for data at index 0
                        '#ffa726',   // color for data at index 1
                        '#ffee58',  // color for data at index 2
                        '#64b5f6'  // color for data at index 3
                    ],
                 hoverBackgroundColor: ["#DF0101", "#FFBF00", "#FFFF00", "#00BFFF"]
              }]
           },
           options: {
              legend: {
                 display: false
              }
           }
        });

        job_div.onclick = function(e) {
           var slice = job_doughnut.getElementAtEvent(e);
           if (!slice.length) return; // return if not clicked on slice
           var label = slice[0]._model.label;
           switch (label) {
              // add case for each label/slice
              case 'Critical':
                 parent.location='job_list.jsp?predefined=all_open_critical';
                 break;
              case 'High':
                 parent.location='job_list.jsp?predefined=all_open_high';
                 break;
              case 'Medium':
                 parent.location='job_list.jsp?predefined=all_open_medium';
                 break;
              case 'Low':
                 parent.location='job_list.jsp?predefined=all_open_low';
                 break;
           }
        }
    </script>
    <script>
        Highcharts.chart('incident_number_container', {
            chart: {
                type: 'spline'
            },
            title: {
                text: null
            },
            exporting: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            legend: {
                enabled: true
            },
            xAxis: {
                categories: [<%=incident_dashboard_number_of_incidents_categories%>]
            },
            yAxis: {
                title: {
                    x: -10,
                    text: 'Number'
                }
            },
            plotOptions: {
                spline: {
                    marker: {
                        enabled: false
                    }
                }
            },
            series: [
                {
                    name: "Number of Incidents",
                    color: '#0084FF',
                    data: [<%=incident_dashboard_number_of_incidents_data%>]
                },
                {
                    name: "Number of Request",
                    color: '#44BEC7',
                    data: [<%=request_dashboard_number_of_requests_data%>]
                }
            ]
        });
    </script>
    <!-- END PAGE LEVEL JS-->
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
    <%
        }
        catch(Exception e)
        {
            System.out.println("Exception on home_service_desk.jsp=" + e);
        }
    }//end if not logged in
%>
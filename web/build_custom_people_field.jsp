<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->

        <%
        String field_name = request.getParameter("field_name");
        String field_label = request.getParameter("field_label");
        String number_cols = request.getParameter("number_cols");
        String field_values = request.getParameter("field_values");   
        String new_record = request.getParameter("new_record"); 
        String default_text = request.getParameter("default_text"); 
        String db_value = request.getParameter("db_value"); 
        String user_name = request.getParameter("user_name"); 
        String value = "";
        String lookup_field_name = "lookup_" + field_name;
        if(new_record.equalsIgnoreCase("true"))
        {
            value = "";
        }
        else
        {
            
            value = db_value;
        }
        
        %>
        <div class="col-<%=number_cols%>">
            <label><br><%=field_label%>&nbsp;<i class="la la-search" style="font-size: 14px;"></i></label>
            <input type="hidden" name="<%=field_name%>" id="<%=field_name%>" value="<%=db_value%>"/>
            <input type="text" name="<%=lookup_field_name%>" id="<%=lookup_field_name%>" value="<%=user_name%>" class="form-control" placeholder="Enter the username"/>
        </div>
        <script>    
            $(function() 
            {
                 //caller
                 $( "#<%=lookup_field_name%>" ).autocomplete(
                 {
                     source: function( request, response ) 
                     {
                         // Fetch data
                         $.ajax({
                             url: "ajax_lookup_user",
                             type: 'post',
                             dataType: "json",
                             minLength: 2,
                             data: 
                             {
                                 search: request.term
                             },
                             success: function( data ) 
                             {
                                 response( data );
                             }
                         });
                     },
                     select: function (event, ui) 
                     {
                         // Set selection
                         $('#<%=lookup_field_name%>').val(ui.item.label); // display the selected text
                         $('#<%=field_name%>').val(ui.item.value); // save selected id to input        
                         return false;
                     }
                 });
             });
         </script>
<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : message_new
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="org.apache.commons.text.StringEscapeUtils"%>
<%@ page import="java.time.ZonedDateTime"%>
<%@ page import="java.time.format.DateTimeFormatter"%>     
<%@page import="org.apache.commons.lang3.StringUtils"%>

<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {        
        //session vars
        boolean page_authorized = false;
        boolean incident_authorized = support.role.authorized(session, "incident","create");      
        boolean request_authorized = support.role.authorized(session, "request","create");            
        if(!incident_authorized || !request_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            String user_tz_name = "UTC";
            String user_tz_time = "+00:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }

            }
            catch(Exception e)
            {
                user_tz_name = "UTC";
                user_tz_time = "+00:00";
            }
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
            date_time_picker_format.setTimeZone(TimeZone.getTimeZone(user_tz_name));
            java.util.Date now = new java.util.Date();
            String now_picker_time = date_time_picker_format.format(now);            
            boolean is_admin = false;
            if (session.getAttribute("administration").toString().equalsIgnoreCase("true") || session.getAttribute("manager").toString().equalsIgnoreCase("true")) 
            {
                is_admin = true;
            }

            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String status = StringUtils.defaultString(request.getParameter("status"));
                if (status.equals("")) {
                    status = "new";
                }
                String timeframe = request.getParameter("timeframe");
                int number_of_new_messages;
                int number_of_all_messages;
                int number_of_open_messages;
                if(timeframe == null)
                {
                    timeframe = "all_time";
                }
                GregorianCalendar start_cal = new GregorianCalendar();
                GregorianCalendar end_cal = new GregorianCalendar();
                if(timeframe.equalsIgnoreCase("1Days"))
                {
                    start_cal.add(start_cal.DATE, -1);
                }
                else if(timeframe.equalsIgnoreCase("7Days"))
                {
                    start_cal.add(start_cal.DATE, -7);
                }
                else if(timeframe.equalsIgnoreCase("30Days"))
                {
                    start_cal.add(start_cal.DATE, -30);
                }
                else if(timeframe.equalsIgnoreCase("60Days"))
                {
                    start_cal.add(start_cal.DATE, -60);
                }
                else if(timeframe.equalsIgnoreCase("90Days"))
                {
                    start_cal.add(start_cal.DATE, -90);
                }
                else if(timeframe.equalsIgnoreCase("365Days"))
                {
                    start_cal.add(start_cal.DATE, -365);
                }
                if (timeframe.equalsIgnoreCase("all_time")) {
                    try
                    {
                        number_of_new_messages = db.get_message.message_count_for_status(con, "New");
                    } catch (Exception e)
                    {
                        number_of_new_messages = 0;
                    }
                    try
                    {
                        // "FROM `message` WHERE status='Open' OR status='INW' OR status='Assigned'";
                        number_of_open_messages = db.get_message.message_count_for_status(con, "Open");
                        number_of_open_messages += db.get_message.message_count_for_status(con, "INW");
                        number_of_open_messages += db.get_message.message_count_for_status(con, "Assigned");
                    } catch (Exception e)
                    {
                        number_of_open_messages = 0;
                    }
                    try
                    {
                        number_of_all_messages = db.get_message.all_message_count(con);
                    } catch (Exception e)
                    {
                        number_of_all_messages = 0;
                    }
                } else {
                    try
                    {
                        number_of_new_messages = db.get_message.message_count_for_timeframe_and_status(con, start_cal.getTime(), end_cal.getTime(), "New");
                    } catch (Exception e)
                    {
                        number_of_new_messages = 0;
                    }
                    try
                    {
                        // "FROM `message` WHERE status='Open' OR status='INW' OR status='Assigned'";
                        number_of_open_messages = db.get_message.message_count_for_timeframe_and_status(con, start_cal.getTime(), end_cal.getTime(), "Open");
                        number_of_open_messages += db.get_message.message_count_for_timeframe_and_status(con, start_cal.getTime(), end_cal.getTime(), "INW");
                        number_of_open_messages += db.get_message.message_count_for_timeframe_and_status(con, start_cal.getTime(), end_cal.getTime(), "Assigned");
                    } catch (Exception e)
                    {
                        number_of_open_messages = 0;
                    }
                    try
                    {
                        number_of_all_messages = db.get_message.message_count_for_timeframe(con, start_cal.getTime(), end_cal.getTime());
                    } catch (Exception e)
                    {
                        number_of_all_messages = 0;
                    }

                }
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">

    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <style>
        table.custom-sort-table td:last-child{
            float: right;
        }
        table.custom-sort-table th:last-child{
            text-align: right;
        }

    </style>
<div class="clr whtbg p-10 mb-15">
    <div class="float-right options">
        <ul>
            <li class="dInlineBlock valign-m pl-5">
                <button data-status="new" class="btn <%=(status.equals("new") ? "btn-primary-new" : "btn-wht")%> md customBtn dInlineBlock" style="min-width: 90px" onclick="filter_messages()">
                    <strong>New: <span class="status_counter" data-status="new"><%=number_of_new_messages%></span></strong>
                </button>
            </li>
            <li class="dInlineBlock valign-m pl-5">
                <button data-status="open" class="btn <%=(status.equals("open") ? "btn-primary-new" : "btn-wht")%> md customBtn dInlineBlock" style="min-width: 90px" onclick="filter_messages()">
                    <strong>Open: <span class="status_counter" data-status="open"><%=number_of_open_messages%></span></strong>
                </button>
            </li>
            <li class="dInlineBlock valign-m pl-5">
                <button data-status="all" class="btn <%=(status.equals("all") ? "btn-primary-new" : "btn-wht")%> md customBtn dInlineBlock" style="min-width: 90px" onclick="filter_messages()">
                    <strong>All: <span class="status_counter"data-status="all"><%=number_of_all_messages%></span></strong>
                </button>
            </li>
            <li id="filters_dropdown" class="dInlineBlock dropdown pl-15">
                <div class="dInlineBlock clr menuHolder">
                    <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                    </a>
                    <div class="dropdown-menu dropdown-menu-right md p-10" x-placement="bottom-end" style="position: absolute; transform: translate3d(100px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <form id="filter_form">
                            <ul>
                                <li class="mb-10 clr">
                                    <label class="field-label full-width">
                                        Date Range
                                    </label>
                                    <div class="formField md  mr-0 full-width">
                                        <%
                                        String values[] = {"1Days","7Days","30Days","60Days","90Days","365Days","all_time"};
                                        String names[] = {"Last Day","Last Week", "Last 30 Days","Last 60 Days","Last 90 Days","Last 365 Days","All Messages"};
                                        String selected = "";
                                        %>
                                        <select name="timeframe" id="timeframe" class="form-control border-0 h-auto px-0 mb-0">
                                            <%
                                            for(int a = 0; a < values.length; a++)
                                            {
                                                selected = "";
                                                if(values[a].equalsIgnoreCase(timeframe))
                                                {
                                                    selected = "SELECTED";
                                                }
                                                %>
                                                <option <%=selected%> value="<%=values[a]%>"><%=names[a]%></option>
                                                <%
                                            }
                                            %>
                                        </select>
                                    </div>
                                </li>
                            </ul>
                            <div class="text-center pt-5 pb-5">
                                <button type="button" class="btn md2 btn-primary-new customBtn mb-5" onclick="apply_filters()">Apply Filters</button>
                                <div class="clr"></div>
                                <a href="#" class="">Reset Filters</a>
                            </div>
                        </form>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <h1 class="large-font boldFont">Messages</h1>
</div>

<%
        String[] filter_fields = {
                 "timeframe",
        };
        List<Map<String, String>> filters = new ArrayList<>();
        for (int i = 0; i < filter_fields.length; i++)
        {
            String filter_field = filter_fields[i];
            String filter_value = request.getParameter(filter_field);
            if (filter_value != null && !filter_value.isEmpty()) 
            {
                Map<String, String> map = new HashMap<String, String>();
                map.put("key", filter_field);
                map.put("value", filter_value);
                filters.add(map);
            }
        }
        request.setAttribute("filter_fields", filter_fields);

    %>

<div id="active_filters">
    <%--<jsp:include page="active_filters_row.jsp" />--%>
</div>

<div class="sort-tbl-holder">
    <table id="data_table" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>Status</th>
                <th>Date Sent</th>
                <th>From</th>
                <th>Subject</th>
                <th>Description</th>
                <!--<th></th>-->
                <th></th>
            </tr>
        </thead>
    </table>
</div>
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteModalLabel">Delete Confirmation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="deleteModalBody">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger" onclick="delete_entity()" id="modalDeleteButton">Delete</button>
        </div>
      </div>
    </div>
</div>                     

<div class="rightPanelHolder">
    <div class="p-15 ticket-content">
        <span class="rightSidePanelCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
        <h4 id="rightPanelTitle" class="mediumFont large-font mb-20"></h4>
        <div id="rightPanelContent" class="clr whtbg p-15">
        </div>
    </div>
</div>

<div class="rightPanelHolderUser">
    <div class="p-15 ticket-content">
        <span class="rightSidePanelUserCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
        <h4 class="mediumFont large-font mb-20 panelTitle"></h4>
        <div class="clr whtbg p-15 panelContent">
        </div>
    </div>
</div>

<div class="modal fade text-left" id="error_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
    <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header bg-danger white">
                  <h4 class="modal-title white" id="myModalLabel10">Error on Form</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
            </div>
            <div class="modal-body">
                <div id="modal_text"></div>
            </div>
            <div class="modal-footer">
                  <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> 

            <!-- end content here-->
            <% 
            }
            catch(Exception e)
            {
                System.out.println("Exception on message_list.jsp=" + e);
            }
            %>
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
                                    
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>    
    <script src="app-assets/js/scripts/tables/datatables-extensions/dataRender/datetime.js"></script>    
    <script src="assets/js/tinymce/tinymce.min.js"></script>

    
    
    <script>
    var dt;
    
    function filter_messages()
    {
        var btn_clicked = $(event.target).closest("button");
        if (btn_clicked.hasClass("btn-wht")) {
            $("span.status_counter").closest("button").removeClass("btn-primary-new").addClass("btn-wht");
            btn_clicked.removeClass("btn-wht").addClass("btn-primary-new");
            setQueryStringParameter("status", btn_clicked.data("status"));
            dt.draw();        
        }
    }
//    function apply_filters()
//    {
//        
//        setQueryStringParameter("timeframe", $("select[name='timeframe']").val());
//        dt.draw();        
//    }
    function apply_filters(draw = true)
    {
        $("#filter_form input,#filter_form select").each(function() {

            var field = $(this);
//            console.log("filter_form_field", field);
            var field_name = field.attr('name');
            
            if (!field_name || field.attr('type') == "hidden" || (field.attr('type') == "radio" && !field.prop("checked")))
            {
                return;
            }
            if (field.val() && field.val() != "Any")
            {
                var label = "";
                var filter_value = "";
                if (field.attr("type") == "radio")
                {
                    label = "Predefined";
                    filter_value = field.siblings("label").html();
                } else {
                    label = field.closest("div").prev().html();
                    filter_value = field.val();
                }
                if ($("#active_filter_" + field_name).length > 0) {
                    var filter_block = $("#active_filter_" + field_name);
                    $(filter_block).children("span").html(filter_value);
                } else {
                    $("#active_filters").append(
                        $('' +
                            '<div class="clr tag dInlineBlock formField md whtbg mr-5 mb-15" id="active_filter_' + field_name + '">' +
                                '<h5 class="basic-font mediumFont">' + label + '</h5>' +
                                '<span>' + filter_value + '</span>' +
                                '<img class="icon" src="assets/images/svg/cross-icon.svg" alt="" onclick="resetFilterFieldNew(\'' + field_name + '\')">' +
                            '</div>'
                        )
                    );
                }
                setQueryStringParameter(field_name, field.val());
            } else {
                resetFilterFieldNew(field_name, false);
            }                
        });
        if(draw)
        {
            console.log("calling draw from apply_filters")
            dt.draw();
        }
    }
    function showDeleteModal(delete_entity_id, entity_type) 
    {
        event.stopPropagation();
        switch (entity_type) {
            case "message":
                $("#deleteModalBody").html("Delete this Message?");
                break;
            default:
                break;
        }
        $("#modalDeleteButton").prop("data-id", delete_entity_id);
        $("#modalDeleteButton").prop("data-entity", entity_type);
        $('#deleteModal').modal();
    }    
    function delete_entity() 
    {
        let entity_type = $("#modalDeleteButton").prop("data-entity");
        let url = null;
        switch (entity_type) {
            case "message":
                url = "message_delete";
                break;
            case "incident":
                url = "incident_delete";
                break;
            default:
                break;
        }
        let delete_entity_id = $("#modalDeleteButton").prop("data-id");
        if (url && delete_entity_id) {
            window.location.assign(url + "?id=" + delete_entity_id);
        }
    }    

    function resetFilterFieldNew(field_name, draw = true)
    {
        var filter_block = $("#active_filter_" + field_name);
        if (filter_block.length > 0)
        {
            filter_block.remove();
            var filter_field = $("#filter_form input[name='" + field_name + "'],#filter_form select[name='" + field_name + "']");
            if (filter_field.attr("type") != "radio")
            {
                filter_field.val("");
            }
            if (field_name == "owner_username" || field_name == "owner_group_name")
            {
                filter_field.trigger('change');
            }
            if (draw) {
                console.log("calling draw from resetfilter")
                dt.draw();
            }
            setQueryStringParameter(field_name, "");
        }
    }

    window.addEventListener("load", (event) => 
    {
//        addRightSidePanelListeners();
        apply_filters(false);
        dt = $('#data_table').DataTable( {
            language: {
                "info":           "Showing _START_ to _END_ of _TOTAL_ messages",
                "infoEmpty":      "Showing 0 to 0 of 0 messages",
                "infoFiltered":   "(filtered from _MAX_ total messages)",
                "lengthMenu":     "Show _MENU_ messages",
                "zeroRecords":    "No matching messages found",
            },
            autoWidth: false,
            "processing": true,
            "serverSide": true,
            "ordering": true,
            'serverMethod': 'post',
            'searching': false,
            "lengthChange": false,
            dom: "<'row'<'col-sm-3'i><'col-sm-3'f><'col-sm-6'p>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            "ajax": {
                "url": "ajax_lookup_messages",
                'data': function(data){
                    // Read values
                    $("#filter_form input,#filter_form select").each(function() {
                        if ($(this).attr("name") && (($(this).attr("type") == "radio" && $(this).is(':checked')) || ($(this).attr("type") != "radio" && this.value && this.value != "" && this.value.toLowerCase() != "any" && this.value.toLowerCase() != "all" && this.value != 0)))
                        {
                            data[$(this).attr('name')] = this.value;
                        }
                    });
                    data['status'] = getQueryStringParameter("status");
                }
            },
            "pageLength": 50,
            "columnDefs": [
//                <th></th>
//                <th>Status</th>
//                <th>Date Sent</th>
//                <th>From</th>
//                <th>Subject</th>
//                <th>Body</th>
//                <th>Has Attachment</th>

                { targets: 0, "width": "2em", "data": null, orderable: false, className: "text-center",
                    render: function ( data, type, row ) 
                    {
                        return '<input class="align-middle" name="message_id" id="mid_' + row.id + '" type="checkbox" title="Select" value=' + row.id + '>';
                    }
                },
                { targets: 1, "width": "5em", "data": null, orderable: false,
                    render: function ( data, type, row ) 
                    {
                        return '<span class="btn sm state-' + row.status.toLowerCase() + ' customBtn">' + row.status + '</span>';
                    }
                },
                { targets: 2, "width": "7em", "data": "date_sent", name: "date_sent" },
                { targets: 3, "width": "10em", "data": "from", name: "from" },
                { targets: 4, "data": "subject", name: "subject"},
                { targets: 5, name: "body", orderable: false, className: "body-column", 
                    render: function ( data, type, row ) 
                    {
                        return row.body + (row.has_attachment == "true" ? '<i class="la la-paperclip la-lg body-paperclip" title="Has Attachment"></i>' : "");
                    }
                },
//                { targets: 6, "width": "2em", "data": null, name: "has_attachment", orderable: false, 
//                    render: function ( data, type, row ) 
//                    {
//                        return (row.has_attachment == "true" ? '<i class="la la-paperclip la-lg" title="Has Attachment"></i>' : "");
//                    }
//
//                },
                { targets: 6, "width": "3em", orderable: false,
                    render: function (data, type, full, meta) 
                    {
                        return '' +
                            '<a href="javascript:void(0)" data-target="message" data-message-id="' + full.id + '" class="mr-5 rightSidePanelOpener"><img src="assets/images/svg/edit-icon.svg" alt=""></a>' +
                            '<a href="javascript:void(0)" onclick="showDeleteModal(' + full.id + ',\'message\')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>';
                    }
                }
                
            ],
            "order": [],
            createdRow: function( row, data, dataIndex ) {
                $( row )
                    .attr('data-message-id', data.id)
                    .attr('data-target', 'message')
                    // .addClass('tableSortHolder')
                    .addClass('message-row')
                    .addClass('rightSidePanelOpener');
            },
            drawCallback: function( settings ) {
                var api = this.api();
//                if ($(api.table().node()).hasClass("d-none"))
//                {
//                    var dt_child_response = api.ajax.json();
//                    $("span.status_counter_" + message_id).each(function() {
//                        var status = $(this).attr('data-status')
//                        if (dt_child_response.statusCounters[status])
//                        {
//                            $(this).html(dt_child_response.statusCounters[status]);
//                        } else {
//                            $(this).html(0);
//                        }
//                    });
//                    $(api.table().node()).removeClass("d-none");
//                }
//
//                addRightSidePanelListeners();
                // Output the data for the visible rows to the browser's console
                    console.log( "drawCallback fired", settings);
                    var ajax_response = api.ajax.json();
                    $("span.status_counter[data-status='new']").html(ajax_response.recordsFilteredNew);
                    $("span.status_counter[data-status='open']").html(ajax_response.recordsFilteredOpen);
                    $("span.status_counter[data-status='all']").html(ajax_response.recordsFilteredAll);
                    
//                $(api.table().node()).removeClass("d-none");
                if (getQueryStringParameter("message_id"))
                {
                    $(".message-row[data-id='" + getQueryStringParameter("message_id") + "']").trigger('click');
                }
                
//                $("tr.message-row").on("click", "td", function() {
//                    console.log($(this), $(this).closest("tr").find("a.rightSidePanelOpener"));
//                    event.stopPropagation();
////                    event.preventDefault();
//                    $(this).closest("tr").find("a.rightSidePanelOpener").trigger('click');
//                    
//                });

                addRightSidePanelListeners();
                $(api.table().node()).find("th:first").css("overflow", "visible").html('' +
                    '<div class="dInlineBlock clr menuHolder" title="Bulk Actions">' +
                        '<a class="btn btn-wht customBtn md dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">' + 
                            '<img src="assets/images/svg/menu-hamburger-icon.svg" alt="">' +
                        '</a>' +
                        '<div class="dropdown-menu dropdown-menu" x-placement="bottom-end" >' +
//                            '<input class="align-middle" name="bulk_select" type="checkbox" title="Select All" onchange="bulkSelect()" /> All' +
                            '<div class="dropdown-item">' + 
                                '<span class="icon mr-5"><input name="bulk_select" type="checkbox" title="Select All" onchange="bulkSelect()" /></span>' + 
                                '<span>All</span>' + 
                            '</div>' +
                            '<a class="dropdown-item" href="#" onclick="bulkAction(\'spam\')">' + 
//                                '<span class="icon mr-5"><img src="assets/images/svg/searchIcon.svg" alt=""></span>' + 
                                'Mark as Spam' + 
                            '</a>' +
                            '<a class="dropdown-item" href="#" onclick="bulkAction(\'close\')">' + 
//                                '<span class="icon mr-5"><img src="assets/images/svg/searchIcon.svg" alt=""></span>' + 
                                'Close' + 
                            '</a>' +
                        '</div>' +
                    '</div>');
            },
            "stripeClasses":[]
        } );
    });
    
    function bulkSelect() {
        $("input[name='message_id']").prop('checked', $(event.target).prop('checked'));
    }

    function bulkAction(action) {
        var message_ids = $("input[name='message_id']:checked").map(function(){return $(this).val();}).get();
        if (!message_ids.length) {
            alert("Select something");
            return false;
        }
        $.ajax({
            url: "message_process",
            type: "POST",
            data: {action: action, message_ids: message_ids},
            success: function(resText){
                var res = jQuery.parseJSON( resText );
                console.log(res);
                if (res.error == "") {
//                        $("input[name='requested_for_name']").autocomplete('search', res.user_name).data("ui-autocomplete").menu.element.children().first().click();;
//                    $("input[name='requested_for_name']").val(res.user_name);
//                    $("input[name='requested_for_id']").val(res.user_id);
//                    $(".rightSidePanelCloser").click();
                } else {
                    alert(res.error);
                }
                dt.draw();
//                window.location.assign("admin_users.jsp");
            },
            error: function(jqXHR, textStatus)
            {
                switch (jqXHR.status) {
                    case 401:
                        window.location.assign("<%=props.get("login_url").toString()%>");
                        break;
                    case 403:
                        alert("No permission!");
                        break;
                    default:
                        alert("Error processing request! " + textStatus);
                }
            }
        });
    }
    
    function addRightSidePanelListeners()
    {
        $('.rightSidePanelCloser:not(.listens)').each(function(){
            $(this).addClass('listens');
            this.addEventListener("click", function()
            {
                $('.rightPanelHolder').removeClass('open');
                $('.rightSidePanel').removeClass('open');
                $('html').removeClass('panelOpen');
            }, false);
        });

        $('.rightSidePanelUserCloser:not(.listens)').each(function(){
            $(this).addClass('listens');
            this.addEventListener("click", function()
            {
                $('.rightPanelHolderUser').removeClass('open');
                $('.rightSidePanelUser').removeClass('open');
            }, false);
        });
        
        $('.rightSidePanelOpener:not(.listens)').each(function(){
//            console.log("attaching to ", this);
            $(this).addClass('listens');
            
            this.addEventListener("click", function()
            {
                $("#rightPanelContent").html("");
//                console.log("rightSidePanelOpener clicked", clicked_node);

                var panel_title = "";
                var ajax = {};
                ajax.url = "";
                ajax.type = 'GET';
                ajax.dataType = "html";
                var clicked_node = $(event.target).closest(".rightSidePanelOpener");
//                console.log($(event.target));
                if ($(event.target).is("input[type='checkbox']") || ($(event.target).is("td") && $(event.target).find("input[type='checkbox']").length > 0)) {
                    return false;
                }
                var target = clicked_node.data("target");
                var message_id = clicked_node.data('messageId');
//                console.log(clicked_node, target, message_id);

                switch (target) 
                {
                    case "message":
                        if (message_id)
                        {
                            ajax.url = "message_process.jsp";
                            ajax.data = {id: message_id};
                            panel_title = "Process Message";
                        }
                        break;
                    default:
                        break;
                }
                
                if (ajax.url === "") return false;

                ajax.error = function(jqXHR, textStatus)
                {
                    switch (jqXHR.status) {
                        case 401:
                            window.location.assign("<%=props.get("login_url").toString()%>");
                            break;
                        case 403:
                            alert("No permission!");
                            break;
                        default:
                            alert("Error processing request!");
                    }
                };

                ajax.success = function (data) 
                {                    
                    $('.rightPanelHolder').addClass('open');
                    $('.rightSidePanel').addClass('open');
                    $('html').addClass('panelOpen');

                    $("#rightPanelTitle").html(panel_title);
                    $("#rightPanelContent").html(data);
                    addRightSidePanelListeners();
                    attachFieldListeners($("#rightPanelContent"));        


                }
                
                $.ajax(ajax);
            }, false);
        });
        
        $('.rightSidePanelUserOpener:not(.listens)').each(function(){
//            console.log("attaching to ", this);
//            console.log("rightSidePanelOpener attached to", this);
            $(this).addClass('listens');
            
            this.addEventListener("click", function()
            {
                $(".rightPanelHolderUser").find(".panelContent").html("");

                var panel_title = "";
                var ajax = {};
                ajax.url = "";
                ajax.type = 'GET';
                ajax.dataType = "html";
//                console.log("rightSidePanelOpener fired click ", event);

                ajax.url = "users_add_lite.jsp";
                ajax.data = {embedded: 'true', referer: 'message_process.jsp', message_id: $(event.target).closest(".rightSidePanelUserOpener").data('messageId')};

                panel_title = "Add New User";
                
                ajax.error = function(jqXHR, textStatus)
                {
                    switch (jqXHR.status) {
                        case 401:
                            window.location.assign("<%=props.get("login_url").toString()%>");
                            break;
                        case 403:
                            alert("No permission!");
                            break;
                        default:
                            alert("Error processing request!");
                    }
                };

                ajax.success = function (data) 
                {                    
                   
                    $('.rightPanelHolderUser').addClass('open');
                    $('.rightSidePanelUser').addClass('open');

                    $(".rightPanelHolderUser").find(".panelTitle").html(panel_title);
                    $(".rightPanelHolderUser").find(".panelContent").html(data);
                    
//                    addRightSidePanelListeners();
//                    attachFieldListeners($(".rightPanelHolderUser"));        
                    
                }
                
                $.ajax(ajax);
            }, false);
        });

    }

    function saveUser()
    {
        var form = $(event.target).closest("form").get(0);
        $.ajax({
            url: form.getAttribute("action"),
            type: "POST",
            data: new FormData(form),
            processData: false,
            contentType: false,
            success: function(resText){
                var res = jQuery.parseJSON( resText );
                console.log(res);
                if (res.error == "") {
//                        $("input[name='requested_for_name']").autocomplete('search', res.user_name).data("ui-autocomplete").menu.element.children().first().click();;
//                    $("input[name='requested_for_name']").val(res.user_name);
//                    $("input[name='requested_for_id']").val(res.user_id);
                    $(".rightSidePanelUserCloser").click();
                    $("input[name='user_exists']").val('true');
                    $(".rightSidePanelUserOpener").hide().after("<span class='alert alert-success'>User created</span>");
                } else {
                    alert(res.error);
                }
//                window.location.assign("admin_users.jsp");
            },
            error: function(jqXHR, textStatus)
            {
                switch (jqXHR.status) {
                    case 401:
                        window.location.assign("<%=props.get("login_url").toString()%>");
                        break;
                    case 403:
                        alert("No permission!");
                        break;
                    default:
                        alert("Error processing request! " + textStatus);
                }
            }
        });
    }
    
    function saveMessage()
    {
        var form = $(event.target).closest("form").get(0);
        $.ajax({
            url: form.getAttribute("action"),
            type: "POST",
            data: new FormData(form),
            processData: false,
            contentType: false,
            success: function(resText){
                var res = jQuery.parseJSON( resText );
                console.log(res);
                if (res.error == "") {
//                        $("input[name='requested_for_name']").autocomplete('search', res.user_name).data("ui-autocomplete").menu.element.children().first().click();;
//                    $("input[name='requested_for_name']").val(res.user_name);
//                    $("input[name='requested_for_id']").val(res.user_id);
                    $(".rightSidePanelCloser").click();
                } else {
                    alert(res.error);
                }
//                window.location.assign("admin_users.jsp");
            },
            error: function(jqXHR, textStatus)
            {
                switch (jqXHR.status) {
                    case 401:
                        window.location.assign("<%=props.get("login_url").toString()%>");
                        break;
                    case 403:
                        alert("No permission!");
                        break;
                    default:
                        alert("Error processing request! " + textStatus);
                }
            }
        });
    }
    
    function attachFieldListeners(container)
    {
        
        tinymce.remove('textarea');
        tinymce.init({
            selector: '.ckeditor',
            setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                });
            },
            height: 250,
            plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });


        //caller
        container.find('input[name="caller_username"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="caller_username"]').val(ui.item.label); // display the selected text
                container.find('input[name="caller_id"]').val(ui.item.value); // save selected id to input
                container.find('input[name="location"]').val(ui.item.location); // save selected id to input
                container.find('input[name="department"]').val(ui.item.department); // save selected id to input
                container.find('input[name="site"]').val(ui.item.site); // save selected id to input
                container.find('input[name="company"]').val(ui.item.company); // save selected id to input
                //if vip = true then check the vip checkbox
                var vip = ui.item.vip;
                if(vip.toUpperCase() === "TRUE")
                {
                    container.find('input[name="vip"]').prop('checked', true);
                }
                else
                {
                    container.find('input[name="vip"]').prop('checked', false);
                }
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });

        //assigned_to_username
        container.find('input[name="assigned_to_username"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="assigned_to_username"]').val(ui.item.label); // display the selected text
                container.find('input[name="assigned_to_id"]').val(ui.item.value); // save selected id to input
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });

        container.find('input[name="status_date"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": 
            {
                "format": "HH:mm MM/DD/YYYY",

                "applyLabel": "Apply",
                "cancelLabel": "Cancel",

                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            }
        });
        container.find('input[name="status_date"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="status_date"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        container.find('input[name="date_closed"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": 
            {
                "format": "HH:mm MM/DD/YYYY",

                "applyLabel": "Apply",
                "cancelLabel": "Cancel",

                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            }
        });
        container.find('input[name="date_closed"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="date_closed"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        
        container.find('input[name="date_assigned"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": 
            {
                "format": "HH:mm MM/DD/YYYY",

                "applyLabel": "Apply",
                "cancelLabel": "Cancel",

                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            }
        });
        container.find('input[name="date_assigned"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="date_assigned"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

    }    

        function clear_caller() 
    { 
        document.getElementById("caller_id").value = "";
        document.getElementById("caller_name").value = "";
    }
    function clear_assigned_to() 
    { 
        document.getElementById("assigned_to_id").value = "";
        document.getElementById("assigned_to_username").value = "";
    }
    function clear_create_by() 
    { 
        document.getElementById("create_by_id").value = "";
        document.getElementById("create_by_name").value = "";
    }
    function clear_caller_group() 
    { 
        document.getElementById("caller_group_id").value = "";
        document.getElementById("caller_group_name").value = "";
    }
    function clear_assigned_group() 
    { 
        document.getElementById("assigned_group_id").value = "";
        document.getElementById("assigned_group_name").value = "";
    }
    </script>
    <script>
        function hide_show_div() 
        {
            var x = document.getElementById("selectors");
            if (x.style.display === "none") 
            {
                x.style.display = "block";
            } 
            else 
            {
                x.style.display = "none";
            }
        }        
        
    </script>
    <script>
        function validateForm() 
        {
            var error = "";
            var user_exists = document.getElementById("user_exists").value;
            var action = document.getElementById("action").value;
            var link_to_item = "";
            var radio_elements = document.getElementsByName('link_to_item');
            for(i = 0; i < radio_elements.length; i++) 
            {
                if(radio_elements[i].checked)
                {
                    link_to_item = radio_elements[i].value
                }
            }            
            //alert("link_to_item=" + link_to_item);
            var requires_user = false;
            var radio_item_checked = false;
            //new_incident","new_request","follow-up_incident","follow-up_request"
            if(action === "new_incident")
            {
                requires_user = true;
            }
            else if(action === "new_request")
            {
                requires_user = true;
            }
            else if(action === "follow-up_incident")
            {
                requires_user = true;
                if(link_to_item.startsWith("INC"))
                {
                    radio_item_checked = true;
                }
                else
                {
                    error = "No Incident selected in the Related Items Table. Select an Incident and try again.";
                }
            }
            else if(action === "follow-up_request")
            {
                requires_user = true;
                if(link_to_item.startsWith("REQ"))
                {
                    radio_item_checked = true;
                }
                else
                {
                    error = "No Request selected in the Related Items Table. Select a Request and try again.";
                }
            }
            if (user_exists === "false" && requires_user) 
            {
                error = "Sender's email address not found or defined as a valid ServeOpticsDesk User. Click on 'Add User' button on the top of this page or contact your ServeOpticsDesk Administrator.";
            }
            //check radio
            if(error !== "")
            {
                var errorMsg = "<p>" + error + "</p>"
                document.getElementById("modal_text").innerHTML = errorMsg
                $("#error_modal").modal("show");
                return false;
            }
            else
            {
                return true;
            }
        }
    </script>    
    <script>
        function change_options(this_radio) 
        {
            var select_option = document.getElementById("action");  
            //clear out the select options
            while (select_option.options.length > 0) 
            {
                select_option.remove(select_option.options.length - 1);
            }            
            var this_value = this_radio.value;
            if(this_value.includes("INC"))
            {
                var select_options = ["Take No Action","Update Email Record","Create Incident / Close this Email","Update Existing Incident / Close this Email","Mark as Spam"];
                var values = ["none","update_email","create_incident","update_incident","spam"];
                var arrayLength = select_options.length;
                for (i = 0; i < arrayLength; i++)
                {
                    var opt = document.createElement('option');
                    opt.text = select_options[i];
                    opt.value = values[i];
                    select_option.add(opt, null);
                }
            }
            else if(this_value.includes("REQ"))
            {
                var select_options = ["Take No Action","Update Email Record","Create Request / Close this Email","Update Request / Close this Email","Mark as Spam"];
                var values = ["none","update_email","create_request","update_request","spam"];
                var arrayLength = select_options.length;
                for (i = 0; i < arrayLength; i++)
                {
                    var opt = document.createElement('option');
                    opt.text = select_options[i];
                    opt.value = values[i];
                    select_option.add(opt, null);
                }
            }
            
            //<option value="none" >Take No Action</option>
            //<option value="update_email" >Update Email Record</option>
            //<option value="create_incident">Create Incident / Close this Email</option>
            //<option value="update_incident">Update Existing Incident / Close this Email</option>
            //<option value="create_request">Create Request / Close this Email</option>
            //<option value="update_request">Update Request / Close this Email</option>
            //<option value="spam">Mark as Spam</option> 
        }
    </script>
    
    
    
    <script>
        $('#item_table').dataTable(
                {
                    /* No ordering applied by DataTables during initialisation */
                "order": []
                }
        );        
    </script>
    
    <script>     
       $( function() 
       {
            //assigned to
            $( "#assigned_to_username" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#assigned_to_username').val(ui.item.label); // display the selected text
                    $('#assigned_to_id').val(ui.item.value); // save selected id to input
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            });
        });
    </script>
    <script>
        $(function()
        {
          $('input[name="status_date"]').daterangepicker({
              "startDate": "<%=now_picker_time%>",
              "autoUpdateInput": false,
              "singleDatePicker": true,
                "showDropdowns": true,
                "timePicker": true,
                "timePicker24Hour": true,
                "locale": {
                "format": "HH:mm MM/DD/YYYY",

                "applyLabel": "Apply",
                "cancelLabel": "Cancel",

                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
              }
          });
          $('input[name="status_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });
          $('input[name="status_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });
        });
    </script>
    <script>
        $(function()
        {
          $('input[name="date_closed"]').daterangepicker({
              "startDate": "<%=now_picker_time%>",
              "autoUpdateInput": false,
              "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
          $('input[name="date_closed"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });
          $('input[name="date_closed"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });
        });
    </script>
    <script>
        $(function()
        {
          $('input[name="date_assigned"]').daterangepicker({
              "startDate": "<%=now_picker_time%>",
              "autoUpdateInput": false,
              "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
          $('input[name="date_assigned"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });
          $('input[name="date_assigned"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });
        });
    </script>
    <script>
        $(function()
        {
          $('input[name="date_closed"]').daterangepicker({
              "startDate": "<%=now_picker_time%>",
              "autoUpdateInput": false,
              "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
          $('input[name="date_closed"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });
          $('input[name="date_closed"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });
        });
    </script>
    
    
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%     
        }//end if not permission
    }//end if not logged in
%>
<!--Copyright 2019 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.HashMap"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        if(!session.getAttribute("administration").toString().equalsIgnoreCase("true"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                String to_address = request.getParameter("to_address");
                String subject = request.getParameter("subject");
                String body = request.getParameter("body");
                
                Connection con = db.db_util.get_contract_connection(context_dir, session);              
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String status = "";
                try
                {
                    HashMap results = support.send_mail.now(con, context_dir, to_address, subject, body);
                    status = results.get("status").toString();
                }
                catch(Exception e)
                {
                    status = "Exception thrown";
                }
                
                
                
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END PAGE LEVEL CSS-->
    
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title">Settings</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a></li>
                                <li class="breadcrumb-item"><a href="admin.jsp">Administration</a></li>
                                <li class="breadcrumb-item"><a href="admin_email.jsp">Email Settings</a></li>
                                <li class="breadcrumb-item"><a href="#">Send Test Email</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div> <!--End breadcrumbs -->
            <div class="row match-height">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form" action="admin_email" method="post">
                                    <p class="card-text">Test Email Settings</p>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="to_address">To:</label>
                                                <input readonly type="text" id="to_address" name="to_address" value="<%=to_address%>" class="form-control">
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="subject">Subject:</label>
                                                <input readonly type="text" id="subject" name="subject" value="<%=subject%>"  class="form-control">
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="body">Body:</label>
                                                <input readonly type="text" id="body" name="body" value="<%=body%>" class="form-control">
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="body">Results:</label>
                                                <fieldset class="form-group">
                                                    <textarea readonly class="form-control" id="results" rows="5"><%=status%></textarea>
                                                </fieldset>
                                            </div>
                                        </div> 
                                    </div>
                                    
                                    <div class="form-actions">
                                        <button type="button" onclick="javascript:location.href='admin_email.jsp'" class="btn btn-warning mr-1">
                                            <i class="ft-x"></i> Go Back to Email Settings
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <!-- END PAGE LEVEL JS-->   
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_user.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_user.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

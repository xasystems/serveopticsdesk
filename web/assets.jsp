<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_users_add.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        boolean authorized = support.role.authorized(session, "asset","read");
        
        
        if(!authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            String home_page = session.getAttribute("home_page").toString();
            Logger logger = LogManager.getLogger();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            int starting_record = 0;
            int previous_record = 0;
            int next_record = 0;
            int fetch_size = 10;
            try
            {
                starting_record  = Integer.parseInt(request.getParameter("start"));
                next_record = starting_record + fetch_size;
                previous_record = starting_record - fetch_size;
                if(previous_record <= 0)
                {
                    previous_record = 0;
                }
            }
            catch(Exception e)
            {
                starting_record = 0;
                previous_record = 0;
                next_record = starting_record + fetch_size;
            }
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                ArrayList <String[]> all_assets_info = db.get_assets.all_limited(con,starting_record,fetch_size);
%>
<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>	

<jsp:include page='menu_service_desk.jsp'>
    <jsp:param name="active_menu" value=""/>
</jsp:include>
<!-- BEGIN PAGE LEVEL CSS-->
<link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
<link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
<style>
    .dropdown-menu.md {
        width: 500px;
    }
</style>
<!-- END PAGE LEVEL CSS-->

<div class="clr whtbg p-10 mb-15">
    <div class="float-right options">
        <ul>
            <li class="dInlineBlock dropdown ">
                <div class="dInlineBlock clr menuHolder">
                    <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                    </a>
                    <div class="dropdown-menu dropdown-menu-right md p-10" x-placement="bottom-end" style="position: absolute; transform: translate3d(100px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <form id="filter_form" onsubmit="return false">
                            <ul>
                                <h4 class="mediumFont medium-font mb-20">Asset Attributes</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                Asset Tag
                                            </label>
                                            <div class="formField md full-width mr-0">
                                                <input type="text" id="asset_tag" name="asset_tag" class="border-0 full-width" placeholder=""/>
                                            </div>
                                        </li>
                                    </div>
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                Serial #
                                            </label>
                                            <div class="formField md full-width mr-0">
                                                <input type="text" id="serial_number" name="serial_number" class="border-0 full-width" placeholder="Enter Serial #"/>
                                            </div>
                                        </li>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                Manufacturer
                                            </label>
                                            <div class="formField md full-width mr-0">
                                                <input type="text" id="manufacturer" name="manufacturer" class="border-0 full-width" placeholder=""/>
                                            </div>
                                        </li>
                                    </div>
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                Model
                                            </label>
                                            <div class="formField md full-width mr-0">
                                                <input type="text" id="model" name="model" class="border-0 full-width" placeholder=""/>
                                            </div>
                                        </li>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                Type
                                            </label>
                                            <div class="formField md full-width mr-0">    
                                                <input type="hidden" name="asset_type" id="asset_type" value=""/>
                                                <select class="border-0 full-width" name="asset_type_id" id="asset_type_id" onchange="update_subtype(this)">
                                                    <option value=""></option>
                                                    <%
                                                    ArrayList<String[]> all_active_types = db.get_assets.all_active_types(con);
                                                    for (int a = 0; a < all_active_types.size(); a++) {
                                                    String type[] = all_active_types.get(a);
                                                    %>
                                                    <option value="<%=type[0]%>"><%=type[1]%></option>
                                                    <%
                                                }
                                                %>
                                                </select>
                                            </div>
                                        </li>
                                    </div>
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                Subtype
                                            </label>
                                            <div class="formField md full-width mr-0">                                                
                                                <select class="border-0 full-width" name="asset_subtype" id="asset_subtype">
                                                    <option value=""></option>
                                                </select>
                                            </div>
                                        </li>
                                    </div> 
                                </div>
                                <h4 class="mediumFont medium-font mb-20">Purchase/Warranty/State</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                Purchase Order
                                            </label>
                                            <div class="formField md full-width mr-0">
                                                <input type="text" id="purchase_order" name="purchase_order" class="border-0 full-width" placeholder=""/>
                                            </div>
                                        </li>
                                    </div>
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                Warranty Expire
                                            </label>
                                            <div class='formField md full-width mr-0'>                                                
                                                <input type='text' id="warranty_expire" name="warranty_expire" value="" class="border-0 full-width datetime" />
                                            </div>
                                        </li>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                State
                                            </label>
                                            <div class="formField md full-width mr-0">
                                                <select class="border-0 full-width" name="state" id="state">
                                                    <option value=""></option>
                                                    <option value="Ordered">Ordered</option>
                                                    <option value="New">New</option>
                                                    <option value="Delivered">Delivered</option>
                                                    <option value="Active">Active</option>
                                                    <option value="Spare">Spare</option>
                                                    <option value="Broken">Broken</option>
                                                    <option value="Retired">Retired</option>
                                                </select>
                                            </div>
                                        </li>
                                    </div>
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                State Date
                                            </label>
                                            <div class='formField md full-width mr-0'>
                                                <input type='text' id="state_date" name="state_date" value="" class="border-0 full-width datetime" />
                                            </div>
                                        </li>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                Assigned to Group&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                                            </label>
                                            <div class="formField md full-width mr-0">
                                                <input type="hidden" name="assigned_group_id" id="assigned_group_id" value="0"/>
                                                <input type="text" name="assigned_group_name" id="assigned_group_name" value="" class="border-0 full-width" />
                                            </div>
                                        </li>
                                    </div>
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">

                                                Assigned to&nbsp;
                                                <i class="la la-search" style="font-size: 14px;"></i>

                                            </label>
                                            <div class="formField md full-width mr-0">
                                                <input type="hidden" name="assigned_to_id" id="assigned_to_id" value="0"/>
                                                <input type="text" name="assigned_to_name" id="assigned_to_name" value="" class="border-0 full-width" placeholder="Enter the Assigned To username"/>
                                            </div>
                                        </li>
                                    </div>
                                </div>
                                <h4 class="mediumFont medium-font mb-20">Location</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                Department
                                            </label>
                                            <div class="formField md full-width mr-0">
                                                <input type="text" id="department" name="department" class="border-0 full-width" placeholder=""/>
                                            </div>
                                        </li>
                                    </div>
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                Location
                                            </label>
                                            <div class="formField md full-width mr-0">
                                                <input type="text" id="location" name="location" class="border-0 full-width" placeholder=""/>
                                            </div>
                                        </li>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                Site
                                            </label>
                                            <div class="formField md full-width mr-0">
                                                <input type="text" id="site" name="site" class="border-0 full-width" placeholder=""/>
                                            </div>
                                        </li>
                                    </div>
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                Company
                                            </label>
                                            <div class="formField md full-width mr-0">
                                                <input type="text" id="company" name="company" class="border-0 full-width" placeholder=""/>
                                            </div>
                                        </li>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                IP
                                            </label>
                                            <div class="formField md full-width mr-0">
                                                <input type="text" id="ip" name="ip" class="border-0 full-width" placeholder=""/>
                                            </div>
                                        </li>
                                    </div>
                                </div>
                                <h4 class="mediumFont medium-font mb-20">Notes/Log</h4>
                                <div class="row">
                                    <div class="col-md-12">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                Notes
                                            </label>
                                            <div class="formField md full-width mr-0">
                                                <input type="text" class="border-0 full-width" id="notes" name="notes"/>
                                            </div>
                                        </li>
                                    </div>
                                </div>
                            </ul>      
                            <div class="text-center pt-5 pb-5">
                                <button class="btn md2 btn-primary-new customBtn mb-5">Apply Filters</button>
                                <div class="clr"></div>
                                <a href="javascript:void(0)" onclick="resetFilters()" class="">Reset Filters</a>
                            </div>
                        </form>
                    </div>
                </div>
            </li>
            <li class="dInlineBlock">
                <span data-target="type" class="btn btn-success customBtn lg waves-effect" onclick="window.location.href = 'assets_type_subtype.jsp'">
                    Types/Subtypes
                </span>    
            </li>
            <li class="dInlineBlock">
                <span data-type="asset" class="btn btn-success customBtn lg waves-effect rightSidePanelOpener">
                    <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>New Asset
                </span>    
            </li>
        </ul>
    </div>
    <h1 class="large-font boldFont">Assets</h1>
</div>
<%
        String[] filter_fields = {
                 "serial_number",
                 "asset_tag",
                 "manufacturer",
                 "model",
                 "asset_type",
                 "asset_subtype",
                 "purchase_order",
                 "warranty_expire",
                 "state",
                 "state_date",
                 "assigned_to_id",
                 "assigned_group_id",
                 "department",
                 "location",
                 "site",
                 "company",
                 "ip",
                 "notes"
        };
        List<Map<String, String>> filters = new ArrayList<>();
        for (int i = 0; i < filter_fields.length; i++)
        {
            String filter_field = filter_fields[i];
            String filter_value = request.getParameter(filter_field);
            if (filter_value != null && !filter_value.isEmpty()) 
            {
                Map<String, String> map = new HashMap<String, String>();
                map.put("key", filter_field);
                map.put("value", filter_value);
                filters.add(map);
            }
        }
        request.setAttribute("filter_fields", filter_fields);
    %>
<div id="active_filters">
    <jsp:include page="active_filters_row.jsp" />
</div>

<div class="sort-tbl-holder">
    <table id="assets-table" class="table table-striped custom-sort-table d-none" style="width:100%">
        <thead>
            <tr>
                <th></th>
                <th>Serial#</th>
                <th>Asset Tag</th>
                <th>Vendor</th>
                <th>Model</th>
                <th>Type</th>
                <th>SubType</th>
                <th>Actions</th>
            </tr>
        </thead>
    </table>

</div>

<jsp:include page='footer.jsp'>
    <jsp:param name="parameter" value=""/>
</jsp:include>	

<div class="rightPanelHolder">
    <div class="p-15 ticket-content">
        <span class="rightSidePanelCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
        <h4 id="rightPanelTitle" class="mediumFont large-font mb-20"></h4>
        <div id="rightPanelContent" class="clr whtbg p-15">
        </div>
    </div>

</div>

<jsp:include page="delete_confirmation_modal.jsp" />

<!-- BEGIN PAGE LEVEL JS-->
<script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<!-- END PAGE VENDOR JS-->
<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>

<script>
    var ajax_id = "";
    var ajax_type = "";
    var dt;

    function validateForm() 
    {
        var password = document.forms["in_form"]["password"].value;
        if (document.getElementById('role_id_0').checked && password == "") 
        {
            alert("If Login Role is checked, then Password must be filled in.");
            return false;
        } 
        else 
        {
            return true;
        }
    }

    window.addEventListener("load", (event) => 
    {
        $('.rightSidePanelCloser').on('click', function(){
            $('.rightPanelHolder').removeClass('open');
            $('.rightSidePanel').removeClass('open');
            $('html').removeClass('panelOpen');
            sla_id = "";
            sla_type = "";
        });

        $('.dropdown').on('hide.bs.dropdown', function (e) 
        { // prevent dropdown close on autocomplete
//            console.log(e.clickEvent.target.className);
            if (e.clickEvent && e.clickEvent.target && (e.clickEvent.target.className == "ui-menu-item-wrapper" || e.clickEvent.target.className == "icon")) {
                e.preventDefault();
            }
        });

        $('.rightSidePanelOpener').on('click', function(e){
            $("#rightPanelContent").html("");

            var panel_title = "";
            var ajax = {};
            ajax.type = 'GET';
            ajax.dataType = "html";

            if(e.originalEvent && e.originalEvent.isTrusted)
            {
                // real click.
                var url_addon = "_new.jsp";
                ajax_type = $(e.target).data('type');

            } else {
                // programmatic click        
                ajax.data = {id: ajax_id};
                var url_addon = "_edit.jsp";
            }

            switch (ajax_type) 
            {
                case "asset":
                    ajax.url = "assets";
                    panel_title = "Create New Asset";
                    break;
                case "asset_type":
                    ajax.url = "admin_knowledge_base_article";
                    panel_title = "Create New Asset Type";
                    break;
                default:
                    ajax.url = "";
                    break;
            }
            if (ajax.url === "") return false;

            ajax.url += url_addon;

            ajax.error = function(jqXHR, textStatus)
            {
                switch (jqXHR.status) {
                    case 401:
                        window.location.assign("<%=props.get("login_url").toString()%>");
                        break;
                    case 403:
                        alert("No permission!");
                        break;
                    default:
                        alert("Error processing request!");
                }
            };

            ajax.success = function (data) 
            {                    
                $("#rightPanelTitle").html(panel_title);
                $("#rightPanelContent").html(data);
                $('.rightSidePanelCloser').on('click', function(){
                    $('.rightPanelHolder').removeClass('open');
                    $('.rightSidePanel').removeClass('open');
                    $('html').removeClass('panelOpen');
                    ajax_id = "";
                    ajax_type = "";
                });

                attachFieldListeners($("#rightPanelContent"));
                
            }
            
            $.ajax(ajax);
        });

        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
        
        dt = $('#assets-table').DataTable( {
            "processing": true,
            "serverSide": true,
            "ordering": false,
            'serverMethod': 'post',
            'searching': false,
            "lengthChange": false,
            dom: "<'row'<'col-sm-3'i><'col-sm-3'f><'col-sm-6'p>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            "ajax": {
                "url": "ajax_lookup_assets",
                'data': function(data){
                    // Read values
                    $("#filter_form input,select").each(function() {
                        if (this.value && this.value != "" && this.value != 0)
                        {
                            data[$(this).attr('name')] = this.value;
//                            console.log($(this).attr('name'), this.value);
                        }
//                        console.log(data,this);
                    });

//                    var asset_tag = $('#asset_tag').val();
//                    var serial_number = $('#serial_number').val();
//
//                    // Append to data
//                    data.searchByAssetTag = asset_tag;
//                    data.searchBySerialNumber = serial_number;
                }
            },
            "pageLength": 50,
            "columnDefs": [
                { targets: 0, defaultContent: '<span class="expand-td"><img src="assets/images/svg/collapse-minus-icon.svg" alt=""></span>', orderable: false },
                { targets: 1, "data": "serial_number" },
                { targets: 2, "data": "asset_tag" },
                { targets: 3, "data": "vendor" },
                { targets: 4, "data": "model" },
                { targets: 5, "data": "asset_type" },
                { targets: 6, "data": "asset_subtype" },
                { targets: 7, 
                    "render": function (data, type, full, meta) 
                    {
                        return '' +
                            '<a href="javascript:void(0)" onclick="expandRow(' + full.id + ');" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>' +
                            '<a href="javascript:void(0)" onclick="showDeleteModal(' + full.id + ')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>';
                    }
                }
            ],
            "order": [],
            createdRow: function( row, data, dataIndex ) {
                // Set the data-status attribute, and add a class
//                $( row ).find('td:eq(0)')
                $( row )
//                    .attr('data-toggle', 'collapse')
                    .attr('data-asset-id', data.id)
//                    .attr('data-target', '#asset_data_' + data.id)
//                    .attr('aria-expanded', 'false')
                    // .addClass('tableSortHolder');
            },
            "stripeClasses":[]
        } );

        $("#filter_form input,select").change(function() {
            var changed = $(this);
            var field_name = changed.attr('name');
            if ($("#active_filter_" + field_name).length == 0)
            {
                var label = changed.closest("div").prev();
                $("#active_filters").append(
                    $('' +
                        '<div class="clr tag dInlineBlock formField md whtbg mr-5 mb-15" id="active_filter_' + field_name + '">' +
                            '<h5 class="basic-font mediumFont">' + label.html() + '</h5>' +
                            '<span>' + changed.val() + '</span>' +
                            '<img class="icon" src="assets/images/svg/cross-icon.svg" alt="" onclick="resetFilterFieldNew(\'' + field_name + '\')">' +
                        '</div>'
                    )
                );
            } else {
                resetFilterFieldNew(field_name);
            }
            dt.draw();
        });
        

        var detailRows = [];
        
        $('#assets-table tbody').on( 'click', 'tr span.expand-td', function () {
            var tr = $(this).closest('tr');
//            console.log(tr);
            var row = dt.row( tr );
            var row_id = tr.data('assetId');
            var idx = $.inArray( row_id, detailRows );

            if ( row.child.isShown() ) {
                row.child.hide();

                // Remove from the 'open' array
                detailRows.splice( idx, 1 );
            }
            else {
                if(row.child() && row.child().length)
                {
                    row.child.show();
                } else {
                    row.child( 
                        $(
                            '<tr class="expanded" id="asset_details_row_' + row_id + '">' +
                                '<td colspan="8" class="first-row">' +
                                    '<div class="content-holder p-15 mt-10 mb-10"></div>' +
                                '</td>' +
                            '</tr>'
                        )                    
                    ).show();
                    editAsset(row_id);
                }
                // Add to the 'open' array
                if ( idx === -1 ) {
                    detailRows.push( row_id );
                }
            }
        } );
        
        dt.on( 'draw', function () {
            $.each( detailRows, function ( i, id ) {
                $('#'+id+' span.expand-td').trigger( 'click' );
            } );
            $(dt.table().node()).removeClass("d-none");
        } );
//        dt.on("preXhr.dt", function (e, settings, data) {
//            $("#assets-table").addClass("d-none");
//        });
        
        attachFieldListeners($("#filter_form"));
    });

    function resetFilterFieldNew(field_name)
    {
        var filter_block = $("#active_filter_" + field_name);
        if (filter_block.length > 0)
        {
            filter_block.remove();
            $("#filter_form input[name='" + field_name + "'],select[name='" + field_name + "']").val("");
            dt.draw();
        }
        
    }
    function expandRow(asset_id) {
        console.log(asset_id);
        $('tr[data-asset-id="' + asset_id + '"] span.expand-td').click();
    }
    function editAsset(id)
    {
        
        var content_holder = $('tr[id="asset_details_row_' + id+'"] div.content-holder');
        console.log(id, content_holder);
        if (content_holder.html() == "") 
        {

            var ajax = {};
            ajax.type = 'GET';
            ajax.dataType = "html";

            ajax.data = {id: id};
            ajax.url = "assets_edit.jsp";

            ajax.error = function(jqXHR, textStatus)
            {
                switch (jqXHR.status) {
                    case 401:
                        window.location.assign("<%=props.get("login_url").toString()%>");
                        break;
                    case 403:
                        alert("No permission!");
                        break;
                    default:
                        alert("Error processing request!");
                }
            };

            ajax.success = function (data) 
            {
                content_holder.html(data);
                attachFieldListeners(content_holder);

            };

            $.ajax(ajax);        
        }
        
    }
    
    function update_subtype(type_select)
    {
        var type_id = $(type_select).val();
        var subtype_select = $(type_select).closest("form").find("select[name='asset_subtype']").get(0);
        subtype_select.options.length = 0;
        
        $.get('ajax_lookup_subtype?type_id=' + type_id, function (data)
        {
            for (var i = 0; i < data.length; i++)
            {
                var obj = data[i];
                var name = obj.name;
                var opt = document.createElement('option');
                if (i == 0) {    
                    var empty_opt = document.createElement('option');
                    empty_opt.innerHTML = "";
                    empty_opt.value = 0;
                    subtype_select.appendChild(empty_opt);
                }
                opt.value = name;
                opt.innerHTML = name;
                subtype_select.appendChild(opt);
            }
        });
    }

    function attachFieldListeners(container)
    {
        container.find('input[name="state_date"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
                "format": "HH:mm MM/DD/YYYY",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            }
        });
        container.find('input[name="state_date"]').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="state_date"]').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

        //state date
        container.find('input[name="warranty_expire"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
                "format": "HH:mm MM/DD/YYYY",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            }
        });
        container.find('input[name="warranty_expire"]').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
        });

        container.find('input[name="warranty_expire"]').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

        container.find("input[name='manufacturer']").autocomplete(
        {
            source: function (request, response)
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_manufacturer",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data:
                            {
                                search: request.term
                            },
                    success: function (data)
                    {
                        response(data);
                    }
                });
            },
            select: function (event, ui)
            {
                // Set selection
                container.find('input[name="manufacturer"]').val(ui.item.label); // display the selected text
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });

        //model
        container.find("input[name='model']").autocomplete(
        {
            source: function (request, response)
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_model",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data:
                            {
                                search: request.term
                            },
                    success: function (data)
                    {
                        response(data);
                    }
                });
            },
            select: function (event, ui)
            {
                // Set selection
                container.find('input[name="model"]').val(ui.item.label); // display the selected text
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });
        //assigned_group
        container.find("input[name='assigned_group_name']").autocomplete(
        {
            source: function (request, response)
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_group",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data:
                            {
                                search: request.term
                            },
                    success: function (data)
                    {
                        response(data);
                    }
                });
            },
            select: function (event, ui)
            {
                // Set selection
                container.find('input[name="assigned_group_name"]').val(ui.item.label); // set name
                container.find('input[name="assigned_group_id"]').val(ui.item.value); // set id
                return false;
            }
        });
        //assigned_to_name
        container.find("input[name='assigned_to_name']").autocomplete(
        {
            source: function (request, response)
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data:
                            {
                                search: request.term
                            },
                    success: function (data)
                    {
                        response(data);
                    }
                });
            },
            select: function (event, ui)
            {
                // Set selection
                container.find('input[name="assigned_to_name"]').val(ui.item.label); // display the selected text
                container.find('input[name="assigned_to_id"]').val(ui.item.value); // save selected id to input
                //alert("assigned to setting is=" + ui.item.value);
                return false;
            }
        });

        $("input[name='company']" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incident_company",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $(this).val(ui.item.label); // display the selected text
                //alert("setting is=" + ui.item.value);
                return false;
            }
        }); 
        //department
        $( "#department" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incident_department",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="department"]').val(ui.item.label); // display the selected text
                //alert("setting is=" + ui.item.value);
                return false;
            }
        }); 
        //site
        $( "#site" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incident_site",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="site"]').val(ui.item.label); // display the selected text
                //alert("setting is=" + ui.item.value);
                return false;
            }
        }); 
        //location
        $( "#location" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incident_location",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="location"]').val(ui.item.label); // display the selected text
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });                
    }
</script>


<!-- END PAGE LEVEL JS-->   
<%                con.close();
    
            } catch (Exception e) {
                System.out.println("Exception in admin_user_add.jsp: " + e);
                logger.debug("ERROR: Exception in admin_user_add.jsp:" + e);
            }
        } //end if not admin permission
    } //end if not redirected
%>
</body>
</html>

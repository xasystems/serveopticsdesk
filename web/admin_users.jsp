<!DOCTYPE html>
<%@page import="org.apache.commons.codec.digest.HmacAlgorithms"%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.time.ZonedDateTime"%>
<%@ page import="java.time.format.DateTimeFormatter"%>
<%@ page import="org.apache.commons.codec.digest.HmacUtils"%>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        boolean page_authorized = support.role.authorized(session, "user","read");  
        String user_id = "0";
        try
        {
            user_id = session.getAttribute("user_id").toString(); 
        }
        catch(Exception e)
        {
            user_id = "0";
        }
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            String user_tz_name = "UTC";
            String user_tz_time = "+00:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "UTC";
                user_tz_time = "+00:00";
            }
            boolean user_delete = false;
            boolean user_update = false;
            String date_range = "";
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020            

            date_time_picker_format.setTimeZone(TimeZone.getTimeZone(user_tz_name));
            java.util.Date now = new java.util.Date();
            String now_picker_time = date_time_picker_format.format(now);
            String timestamp_string = timestamp_format.format(now);
            
            String status = request.getParameter("status");
            if(status == null || status.equals(""))
            {
                status = "all";
            }
            ArrayList<String[]> users = new ArrayList();

            if(session.getAttribute("user").toString().equalsIgnoreCase("delete") || session.getAttribute("administration").toString().equalsIgnoreCase("true") || session.getAttribute("manager").toString().equalsIgnoreCase("true"))
            {
                user_delete = true;
                user_update = true;
            }
            else if(session.getAttribute("user").toString().equalsIgnoreCase("update"))
            {
                user_delete = false;
                user_update = true;
            }

            String couchdb_username = timestamp_string;
//            String couchdb_token = HmacUtils(HmacAlgorithms.HMAC_SHA_1, couchdb_username).hmacHex(couchdb_username);
            String couchdb_secret = props.get("couchdb.secret").toString();
            String couchdb_token = HmacUtils.hmacSha1Hex(couchdb_secret, couchdb_username);
            ArrayList<String[]> status_select = db.get_system_select_field.active_for_table_column(con, "users", "status");
                                              
    %>
<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>


<jsp:include page='menu_service_desk.jsp'>
    <jsp:param name="active_menu" value=""/>
</jsp:include>	

<!-- BEGIN PAGE LEVEL CSS-->
<style type="text/css">
    .dropdown-menu.md {
        width: 500px;
    }
    .dropdown-menu input {
        width: auto !important;
    }
</style>
<link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
<!-- END VENDOR CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/selects/select2.min.css">

<!--<link rel="stylesheet" type="text/css" href="assets/css/dashboard.css">-->
<!--<link rel="stylesheet" type="text/css" href="assets/css/app.668ec775.css">-->

<!--End Page level css-->

<div class="clr whtbg p-10 mb-15">
    <div class="float-right options">
                <ul>
            <li id="filters_dropdown" class="dInlineBlock dropdown ">
                <div class="dInlineBlock clr menuHolder">
                    <a id="filters_dropdown_toggle" class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters</a>
                    <div id="filters_dropdown_menu" class="dropdown-menu dropdown-menu-right md p-10" x-placement="bottom-end" style="position: absolute; transform: translate3d(100px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <form id="filter_form" onsubmit="return false">
                            <ul>
                                <li class="mb-10 clr">
                                    <div class="">
                                        <label class="field-label full-width">Username</label>
                                        <div class="formField md full-width mr-0">
                                            <input class="no-border full-width autocomplete ui-autocomplete-input" type="text" id="search_username" name="username" value="" autocomplete="off">
                                        </div>
                                    </div>
                                </li>
                                <li class="mb-10 clr">
                                    <div class="">
                                        <label class="field-label full-width">First name</label>
                                        <div class="formField md full-width mr-0">
                                            <input class="no-border full-width autocomplete ui-autocomplete-input" type="text" name="first" value="" autocomplete="off">
                                        </div>
                                    </div>
                                </li>
                                <li class="mb-10 clr">
                                    <div class="">
                                        <label class="field-label full-width">Last name</label>
                                        <div class="formField md full-width mr-0">
                                            <input class="no-border full-width autocomplete ui-autocomplete-input" type="text" name="last" value="" autocomplete="off">
                                        </div>
                                    </div>
                                </li>
                                <li class="mb-10 clr">
                                    <div class="">
                                        <label class="field-label full-width">Role</label>
                                        <div class="formField md full-width mr-0">
                                            <select class="no-border" name="role">
                                                <option value="Any">Any</option>

                                                        <option value="1">Administrator</option>

                                                        <option value="2">Manager</option>

                                                        <option value="3">Self Service</option>

                                                        <option value="4">Shift Manager</option>

                                                        <option value="5">Service Desk Manager</option>

                                                        <option value="6">Agent</option>

                                            </select>
                                        </div>
                                    </div>
                                </li>
                                <li class="mb-10 clr">
                                    <div class="">
                                        <label class="field-label full-width">Company</label>
                                        <div class="formField md full-width mr-0">
                                            <input class="no-border full-width autocomplete ui-autocomplete-input" type="text" name="company" value="" autocomplete="off">
                                        </div>
                                    </div>
                                </li>
                                <li class="mb-10 clr">
                                    <div class="">
                                        <label class="field-label full-width">Department</label>
                                        <div class="formField md full-width mr-0">
                                            <input class="no-border full-width autocomplete ui-autocomplete-input" type="text" name="department" value="" autocomplete="off">
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="text-center pt-5 pb-5">
                                <button class="btn md2 btn-primary-new customBtn mb-5" onclick="apply_filters()">Apply Filters</button>
                                <div class="clr"></div>
                                <a href="javascript:void(0)" onclick="resetFilters()">Reset Filters</a>
                            </div>
                        </form>                        
                    </div>
                </div>
            </li>
            <li class="dInlineBlock">
                <a data-target="user" data-user-id="0" class="btn btn-success customBtn lg waves-effect rightSidePanelOpener" href="#">
                    <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>New User
                </a>    
            </li>
        </ul>
    </div>
    <h1 class="large-font boldFont">Users</h1>
</div>

<jsp:include page="alerts.jsp" />

<%
        String[] filter_fields = {
                 "serial_number",
                 "asset_tag",
                 "manufacturer",
                 "model",
                 "asset_type",
                 "asset_subtype",
                 "purchase_order",
                 "warranty_expire",
                 "state",
                 "state_date",
                 "assigned_to_id",
                 "assigned_group_id",
                 "department",
                 "location",
                 "site",
                 "company",
                 "ip",
                 "notes"
        };
        List<Map<String, String>> filters = new ArrayList<>();
        for (int i = 0; i < filter_fields.length; i++)
        {
            String filter_field = filter_fields[i];
            String filter_value = request.getParameter(filter_field);
            if (filter_value != null && !filter_value.isEmpty()) 
            {
                Map<String, String> map = new HashMap<String, String>();
                map.put("key", filter_field);
                map.put("value", filter_value);
                filters.add(map);
            }
        }
        request.setAttribute("filter_fields", filter_fields);

    %>
<div id="active_filters">
    <jsp:include page="active_filters_row.jsp" />
</div>

<div class="sort-tbl-holder">
    <table id="data_table" class="table table-striped custom-sort-table d-none" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>Username</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>User Roles</th>
                <th>Actions</th>
            </tr>
        </thead>
    </table>
</div>
<jsp:include page='footer.jsp'>
    <jsp:param name="parameter" value=""/>
</jsp:include>	

<div class="rightPanelHolder">
    <div class="p-15 ticket-content">
        <span class="rightSidePanelCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
        <h4 id="rightPanelTitle" class="mediumFont large-font mb-20"></h4>
        <div id="rightPanelContent" class="clr whtbg p-15">
        </div>
    </div>
</div>

<div class="rightPanelHolder2">
    <div class="p-15 ticket-content">
        <span class="rightSidePanelCloser2"><img src="assets/images/svg/close-icon.svg" alt=""></span>
        <h4 id="rightPanelTitleChat" class="mediumFont large-font mb-20"></h4>
        <div id="chat_app" class="clr whtbg p-15">
        </div>
    </div>
</div>

<jsp:include page="delete_confirmation_modal.jsp" />

<script type="text/javascript">
    function deleteAttachment()
    {
        var id = $(event.target).closest("a").data('id');
        var div = $(event.target).closest("div").replaceWith('<input type="hidden" name="attachment_delete_id" value="' + id +'"/>');
    }
</script>

<!-- BEGIN PAGE LEVEL JS-->
<!-- BEGIN VENDOR JS-->    
<script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN MODERN JS-->
<script src="app-assets/js/core/app-menu.js"></script>
<script src="app-assets/js/core/app.js"></script>
<!-- END MODERN JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="app-assets/js/scripts/popover/popover.js"></script>

<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>    
<script src="app-assets/js/scripts/tables/datatables-extensions/dataRender/datetime.js"></script>    
<script src="app-assets/vendors/js/forms/select/select2.full.min.js"></script>
<script src="app-assets/js/scripts/forms/select/form-select2.js"></script>
<script src="assets/js/bs-custom-file-input.min.js"></script>
<script src="assets/js/axios.min.js"></script>

<script>
    var dt;
    var currentUserId = <%=user_id%>;
    var serviceDeskEndpoint = "<%=props.get("servicedesk_url")%>";

    function deleteAttachment()
    {
        var id = $(event.target).closest("a").data('id');
        var div = $(event.target).closest("div").replaceWith('<input type="hidden" name="attachment_delete_id" value="' + id +'"/>');
    }

    function saveUser()
    {
        var form = $(event.target).closest("form").get(0);
        $.ajax({
            url: form.action,
            type: "POST",
            data: new FormData(form),
            processData: false,
            contentType: false,
            success: function(data){
//                console.log(data);
                window.location.assign("admin_users.jsp");
            },
            error: function(jqXHR, textStatus)
            {
                switch (jqXHR.status) {
                    case 401:
                        window.location.assign("<%=props.get("login_url").toString()%>");
                        break;
                    case 403:
                        alert("No permission!");
                        break;
                    default:
                        alert("Error processing request! " + textStatus);
                }
            }
        });
    }

    function apply_filters()
    {
        $("#filter_form input,#filter_form select").each(function() {

            var field = $(this);
//            console.log("filter_form_field", field);
            var field_name = field.attr('name');
            var field_type = (field.attr("type") ? field.attr("type") : field.get(0).tagName.toLowerCase());
            
            if (!field_name || field.attr('type') == "hidden" || (field.attr('type') == "radio" && !field.prop("checked")))
            {
                return;
            }
            if (field.val() && field.val() != "Any")
            {
                var label = "";
                var filter_value = "";
                if (field_type == "radio")
                {
                    label = "Predefined";
                    filter_value = field.siblings("label").html();
                } else if (field_type == "select") {
                    label = field.closest("div").prev("label").html();
                    filter_value = field.find("option:selected").text();
                } else {
                    label = field.closest("div").prev("label").html();
                    filter_value = field.val();
                }
                if ($("#active_filter_" + field_name).length > 0) {
                    var filter_block = $("#active_filter_" + field_name);
                    $(filter_block).children("span").html(filter_value);
                } else {
                    $("#active_filters").append(
                        $('' +
                            '<div class="clr tag dInlineBlock formField md whtbg mr-5 mb-15" id="active_filter_' + field_name + '">' +
                                '<h5 class="basic-font mediumFont">' + label + '</h5>' +
                                '<span>' + filter_value + '</span>' +
                                '<img class="icon" src="assets/images/svg/cross-icon.svg" alt="" onclick="resetFilterFieldNew(\'' + field_name + '\')">' +
                            '</div>'
                        )
                    );
                }
            } else {
                resetFilterFieldNew(field_name);
            }                
        });
        dt.draw();
    }

    function resetFilterFieldNew(field_name)
    {
        var filter_block = $("#active_filter_" + field_name);
        if (filter_block.length > 0)
        {
            filter_block.remove();
            var filter_field = $("#filter_form input[name='" + field_name + "'],#filter_form select[name='" + field_name + "']");
            if (filter_field.attr("type") != "radio")
            {
                filter_field.val("");
            }
            if (field_name == "assigned_to_username" || field_name == "assigned_group_name")
            {
                filter_field.trigger('change');
            }
            dt.draw();
        }
        
    }

    function addRightSidePanelListeners()
    {
        $('.rightSidePanelCloser:not(.listens)').each(function(){
            $(this).addClass('listens');
            this.addEventListener("click", function()
            {
                $('.rightPanelHolder').removeClass('open');
                $('.rightSidePanel').removeClass('open');
                $('html').removeClass('panelOpen');
            }, false);
        });

        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        
        var validation = Array.prototype.filter.call(forms, function(form) 
        {
            $(form).removeClass("needs-validation");
            form.addEventListener('submit', function(event) 
            {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                    $('.rightPanelHolder').animate({scrollTop: $('.form-control:invalid').first().offset().top - 50}, 'fast');
                }
                form.classList.add('was-validated');
            }, false);
        });            
        
                
        $('.rightSidePanelOpener:not(.listens)').each(function(){
//            console.log("attaching to ", this);
            $(this).addClass('listens');
            
            this.addEventListener("click", function()
            {
                $("#rightPanelContent").html("");

                var panel_title = "";
                var ajax = {};
                ajax.url = "";
                ajax.type = 'GET';
                ajax.dataType = "html";
                if (event.target.nodeName == "IMG")
                {
                    var clicked_node = $(event.target).closest("a");
                } else {
                    var clicked_node = $(event.target);
                }
                var target = clicked_node.data('target');
                var user_id = clicked_node.data('userId');
//                console.log(clicked_node, target, user_id);

                switch (target) 
                {
                    case "user":
                        if (user_id)
                        {
                            ajax.url = "admin_users_edit.jsp";
                            ajax.data = {id: user_id};
                            panel_title = "Edit User";
                        } else {
                            ajax.url = "admin_users_add.jsp";
                            panel_title = "Create a New User";
                        }
                        break;
                    default:
                        break;
                }
                
                if (ajax.url === "") return false;

                ajax.error = function(jqXHR, textStatus)
                {
                    switch (jqXHR.status) {
                        case 401:
                            window.location.assign("<%=props.get("login_url").toString()%>");
                            break;
                        case 403:
                            alert("No permission!");
                            break;
                        default:
                            alert("Error processing request!");
                    }
                };

                ajax.success = function (data) 
                {                    
                    $('.rightPanelHolder').addClass('open');
                    $('.rightSidePanel').addClass('open');
                    $('html').addClass('panelOpen');

                    $("#rightPanelTitle").html(panel_title);
                    $("#rightPanelContent").html(data);
                    addRightSidePanelListeners();
                    attachUserFieldListeners($("#rightPanelContent"));        


                }
                
                $.ajax(ajax);
            }, false);
        });

    }
    
    window.addEventListener("load", (event) => 
    {
        addRightSidePanelListeners();

        $('#filters_dropdown').on('hide.bs.dropdown', function (e) 
        { // prevent dropdown close on autocomplete
//            console.log("dropdown closed ", e);
            //            console.log(e.clickEvent.target.className);
            if (e.clickEvent && e.clickEvent.target && (e.clickEvent.target.className == "ui-menu-item-wrapper" || e.clickEvent.target.className == "icon" || e.clickEvent.target.className.indexOf("applyBtn") !== -1)) {
                e.preventDefault();
            }
        });

        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
        
        dt = $('#data_table').DataTable( {
            language: {
                "info":           "Showing _START_ to _END_ of _TOTAL_ users",
                "infoEmpty":      "Showing 0 to 0 of 0 users",
                "infoFiltered":   "(filtered from _MAX_ total users)",
                "lengthMenu":     "Show _MENU_ users",
                "zeroRecords":    "No matching users found",
            },
            autoWidth: false,
            "processing": true,
            "serverSide": true,
            "ordering": false,
            'serverMethod': 'post',
            'searching': false,
            "lengthChange": false,
            dom: "<'row'<'col-sm-3'i><'col-sm-3'f><'col-sm-6'p>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            "ajax": {
                "url": "ajax_lookup_users",
                'data': function(data){
                    // Read values
                    var filters = {};
                    $("#filter_form input,#filter_form select").each(function() {
                        if ($(this).attr("name") && $(this).attr("name") != "assigned_to_username" && $(this).attr("name") != "assigned_group_name" && this.value && this.value != "")
                        {
                            filters[$(this).attr('name')] = this.value;
//                            data[$(this).attr('name')] = this.value;
                        }

                    });
                    data['filters'] = JSON.stringify(filters);
                }
            },
            "pageLength": 50,
            "columnDefs": [

//                { targets: 0, defaultContent: '<span class="expand-td"><img src="assets/images/svg/collapse-minus-icon.svg" alt=""></span>', orderable: false },
                { targets: 0, "data": "id" },
                { targets: 1, "data": "username" },
                { targets: 2, "data": "first" },
                { targets: 3, "data": "last" },
                { targets: 4, "data": "email" },
                { targets: 5, "data": "roles" },
                { targets: 6, 
                    render: function (data, type, full, meta) 
                    {
                        return '' +
                            '<a href="javascript:void(0)" data-target="user" data-user-id="' + full.id + '" class="mr-5 rightSidePanelOpener"><img src="assets/images/svg/edit-icon.svg" alt=""></a>' +
                            '<a href="javascript:void(0)" onclick="showDeleteModal(' + full.id + ')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>';
                    }
                }
            ],
            "order": [],
            createdRow: function( row, data, dataIndex ) {
                $( row )
                    .attr('data-id', data.id)
                    // .addClass('tableSortHolder')
                    .addClass('user-row');
            },
            "stripeClasses":[]
        } );


        var detailRows = [];
        
        $('#data_table tbody').on( 'click', 'tr.user-row span.expand-td', function (e) {
            e.stopPropagation();
            var tr = $(this).closest('tr');
//            console.log(tr);
            var row = dt.row( tr );
            var data = row.data();
            var row_id = tr.data('id');
            var idx = $.inArray( row_id, detailRows );

            if ( row.child.isShown() ) {
                row.child.hide();

                // Remove from the 'open' array
                detailRows.splice( idx, 1 );
            }
            else {
                if(row.child() && row.child().length)
                {
                    row.child.show();
                } else {
                    row.child( 
                        $(
                            '<tr class="expanded" id="request_details_row_' + row_id + '">' +
                                '<td colspan="7" class="first-row">' +
                                    '<div class="content-holder p-15 mt-10 mb-10"></div>' +
                                '</td>' +
                            '</tr>'
                        )                    
                    ).show();
                    editUser(row_id);
                }
                // Add to the 'open' array
                if ( idx === -1 ) {
                    detailRows.push( row_id );
                }
            }
        } );

        apply_filters();
        
        dt.on( 'draw', function () {
            $.each( detailRows, function ( i, id ) {
                $('#'+id+' span.expand-td').trigger( 'click' );
            } );
            $(dt.table().node()).removeClass("d-none");
            
            addRightSidePanelListeners();

        } );
//        dt.on("preXhr.dt", function (e, settings, data) {
//            $("#data_table").addClass("d-none");
//        });
        
        attachUserFieldListeners($("#filter_form"));
    });

    function editUser(id)
    {
        
        var content_holder = $('tr[id="request_details_row_' + id+'"] div.content-holder');
//        console.log(id, content_holder);
        if (content_holder.html() == "") 
        {

            var ajax = {};
            ajax.type = 'GET';
            ajax.dataType = "html";

            ajax.data = {id: id};
            ajax.url = "admin_users_edit.jsp?preview=true";

            ajax.error = function(jqXHR, textStatus)
            {
                switch (jqXHR.status) {
                    case 401:
                        window.location.assign("<%=props.get("login_url").toString()%>");
                        break;
                    case 403:
                        alert("No permission!");
                        break;
                    default:
                        alert("Error processing request!");
                }
            };

            ajax.success = function (data) 
            {
                content_holder.html(data);
                attachUserFieldListeners(content_holder)
            };

            $.ajax(ajax);        
        }
        
    }
    const axiosInstance = axios.create();

    axiosInstance.interceptors.request.use(function (config) {
        // Do something before request is sent
        if (config.url != 'ajax_get_jwt')
        {
            config.responseType = 'arraybuffer'
        }
        return config;
    }, function (error) {
        // Do something with request error
        return Promise.reject(error);
    });
    
    axiosInstance.interceptors.response.use((response) => {
        return response
    },  function (error) {
            const originalRequest = error.config;
            if (error.response)
            {
                if (error.response.status === 401 && originalRequest.url.indexOf('ajax_get_jwt') !== -1 ) {
                    router.push('/login');
                    return Promise.reject(error);
                }

                if (error.response.status === 401 && !originalRequest._retry) {
                    originalRequest._retry = true;
                    return axiosInstance.get('ajax_get_jwt')
                        .then(res => {
                            if (res.status === 200) {
                                axiosInstance.defaults.headers.common['Authorization'] = 'Bearer ' + res.data.token;
                                return axiosInstance(originalRequest);
                            }
                        })
                }
            } else {
                console.log("axiosInstance error", error);
            }
            return Promise.reject(error);
    });
    
    function openAttachment(url, file_name)
    {
        let file = ''
        if (typeof url == 'undefined' || typeof file_name == 'undefined')
        {
            let name_on_disk = event.target.getAttribute('data-link');
            file_name = event.target.getAttribute('data-file-name');
            file = '<%=props.get("couchdb.attachments_host")%>/<%=props.get("couchdb.name")%>/' + name_on_disk;
        } else {
            file = url
        }        
        if (file != '')
        {            
            axiosInstance.get(file).then((response) => {
//                console.log(response)

                let blob = new Blob([response.data], { type: response.headers['content-type'] })
                let link = document.createElement('a')
                link.href = window.URL.createObjectURL(blob)
                link.download = file_name
                link.click()
                link.remove(); 
                window.URL.revokeObjectURL(link);

            });
        }
    }    

    function attachUserFieldListeners(container)
    {
        $('.select2').select2();
        bsCustomFileInput.init();
        container.find('input.autocomplete').each(function() 
        {
            var $el = $(this);
            var field = $el.prop('name');
//            console.log(field);

            $el.autocomplete({
                source: function( request, response ) 
                {
//                    console.log(request);
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user_by_filter",
                        type: 'post',
                        dataType: "json",
                        data: 
                        {
                            search: request.term,
                            field: field
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                minLength: 2,
                select: function( event, ui ) 
                {
//                    console.log( ui.item );    
                    // Set selection
                    container.find('input[name=' + field + ']').val(ui.item.label); // display the selected text
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            });
        });
        
        //owner
        container.find('input[name="assigned_to_username"]').on('change', function() {
            if (this.value == "") {
                container.find('input[name="assigned_to_id"]').val("");
            }
        });

        container.find('input[name="assigned_group_name"]').on('change', function() {
            if (this.value == "") {
                container.find('input[name="assigned_group_id"]').val("");
            }
        });
        
        container.find('input[name="assigned_to_username"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="assigned_to_username"]').val(ui.item.label); // display the selected text
                container.find('input[name="assigned_to_id"]').val(ui.item.value); // save selected id to input
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });

    }    

</script>
    
<script>
function clear_caller() 
{ 
    document.getElementById("caller_id").value = "";
    document.getElementById("caller_name").value = "";
}
function clear_assigned_to() 
{ 
    document.getElementById("assigned_to_id").value = "";
    document.getElementById("assigned_to_name").value = "";
}
function clear_create_by() 
{ 
    document.getElementById("create_by_id").value = "";
    document.getElementById("create_by_name").value = "";
}
function clear_caller_group() 
{ 
    document.getElementById("caller_group_id").value = "";
    document.getElementById("caller_group_name").value = "";
}
function clear_assigned_group() 
{ 
    document.getElementById("assigned_group_id").value = "";
    document.getElementById("assigned_group_name").value = "";
}
</script>

    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%      con.close();
        }//end if not permission
    }//end if not logged in
%>
<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_users_add.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        if (!session.getAttribute("administration").toString().equalsIgnoreCase("true") && !session.getAttribute("manager").toString().equalsIgnoreCase("true")) 
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
%>
<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>	

<jsp:include page='menu_service_desk.jsp'>
    <jsp:param name="active_menu" value=""/>
</jsp:include>
<!-- BEGIN PAGE LEVEL CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
<!-- END PAGE LEVEL CSS-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- start content here-->
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">Schedules</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a></li>
                            <li class="breadcrumb-item"><a href="admin_settings.jsp">Administration</a></li>
                            <li class="breadcrumb-item"><a href="admin_schedules.jsp">Schedule</a></li>
                            <li class="breadcrumb-item"><a href="#">Add</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div> <!--End breadcrumbs -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Add Schedule</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form class="form" action="admin_schedule_add" method="post">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input type="text" id="name" name="name" class="form-control" placeholder="Schedule Name" />
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <label for="description">Description</label>
                                                <input type="text" id="description" name="description" class="form-control" placeholder="Description" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <label for="is_all_day">All Day?</label>
                                            <br>
                                            <input type="checkbox" id="is_all_day" name="is_all_day">
                                            
                                        </div> 
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="start_time">Start Time</label>
                                                <input type="text" id="start_time" name="start_time" class="form-control" placeholder="HH:mm:ss  24 Hour Clock" />
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="end_time">End Time</label>
                                                <input type="text" id="end_time" name="end_time" class="form-control" placeholder="HH:mm:ss  24 Hour Clock" />
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="dow">Days of Week</label>
                                            <br>
                                            <input type="checkbox" id="sunday" name="sunday"><label for="sunday">&nbsp;Sun</label> 
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" id="monday" name="monday"><label for="monday">&nbsp;Mon</label> 
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" id="tuesday" name="tuesday"><label for="tuesday">&nbsp;Tue</label> 
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" id="wednesday" name="wednesday"><label for="wednesday">&nbsp;Wed</label> 
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" id="thursday" name="thursday"><label for="thursday">&nbsp;Thu</label> 
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" id="friday" name="friday"><label for="friday">&nbsp;Fri</label> 
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" id="saturday" name="saturday"><label for="saturday">&nbsp;Sat</label> 
                                        </div> 
                                        <div class="col-md-2">
                                            <label for="include_holidays">Include Holidays?</label>
                                            <br>
                                            <input type="checkbox" id="include_holidays" name="include_holidays">                                            
                                        </div>
                                    </div>
                                    <div class="row">
                                         
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="button" onclick="javascript:location.href='admin_schedules.jsp'" class="btn btn-warning mr-1">
                                        <i class="ft-x"></i> Cancel
                                    </button>
                                    <button type="submit" class="btn btn-info">
                                        &nbsp;&nbsp;<i class="ft-save"></i> Save&nbsp;&nbsp;
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end content here-->
    </div>        
</div>
<jsp:include page='footer.jsp'>
    <jsp:param name="parameter" value=""/>
</jsp:include>	
<!-- BEGIN PAGE LEVEL JS-->
<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<!-- END PAGE LEVEL JS-->   
<%                con.close();
    
            } catch (Exception e) {
                System.out.println("Exception in admin_schedule_add.jsp: " + e);
                logger.debug("ERROR: Exception in admin_schedule_add.jsp:" + e);
            }
        } //end if not admin permission
    } //end if not redirected
%>
</body>
</html>

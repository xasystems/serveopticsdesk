<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="javax.mail.*" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.util.Properties" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
     String context_dir = request.getServletContext().getRealPath("");
    LinkedHashMap sys_props = support.config.get_config(context_dir);
    Connection con = db.db_util.get_contract_connection(context_dir, session); 
    String user_id = session.getAttribute("user_id").toString();
    boolean update_session_table = db.get_user_sessions.update(con, user_id);
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <!-- Page specific CSS-->
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <!--End Page specific CSS-->
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END Page Level CSS-->
    
    
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title"><i class="la ft-users"></i>&nbsp;&nbsp;Page Title</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="home_analytics.jsp">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Page Layouts</a>
                                </li>
                                <li class="breadcrumb-item active">Fixed Layout
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <%
                
            try
            {
                String unencrypted_password = support.encrypt_utils.decrypt(context_dir, "8BJkUGF0x2eAmqxtl9ODuBZ53KtlAIcE");    //8BJkUGF0x2eAmqxtl9ODuBZ53KtlAIcE = 1qaz2wsx#EDC$RFV

                final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
                String receive_email_user = db.get_configuration_properties.by_name(con, "receive_email_user").trim();
                String receive_email_port = db.get_configuration_properties.by_name(con, "receive_email_port").trim();
                String receive_email_password = db.get_configuration_properties.by_name(con, "receive_email_password").trim();
                receive_email_password = support.encrypt_utils.decrypt(context_dir, receive_email_password);
                String receive_email_host = db.get_configuration_properties.by_name(con, "receive_email_host").trim();
                String receive_email_encryption = db.get_configuration_properties.by_name(con, "receive_email_encryption").trim();
                String receive_email_protocol = db.get_configuration_properties.by_name(con, "receive_email_protocol").trim().toLowerCase();

                Properties email_props = new Properties();

                /*if(receive_email_protocol.equalsIgnoreCase("imap"))
                {
                    email_props.setProperty("mail.imaps.socketFactory.class", SSL_FACTORY);
                    email_props.setProperty("mail.imaps.socketFactory.fallback", "false");
                    email_props.setProperty("mail.imaps.port", receive_email_port);
                    email_props.setProperty("mail.imaps.socketFactory.port", receive_email_port);
                    email_props.put("mail.imaps.host", receive_email_host);                
                }
                else if(receive_email_protocol.equalsIgnoreCase("pop3"))
                {
                    email_props.put("mail.host", receive_email_host);
                    email_props.put("mail.store.protocol", receive_email_protocol);
                    email_props.put("mail.pop3.starttls.enable", "true");
                    email_props.put("mail.pop3.auth", "true");
                    email_props.put("mail.pop3.port", receive_email_port);
                }*/


                System.out.println("password=" + unencrypted_password);
                final String username = "asbury_sd_survey@xasystems.com";
                final String passwd = "1qaz2wsx#EDC$RFV";




                Session mail_session = Session.getInstance(email_props, new javax.mail.Authenticator() 
                {
                    protected PasswordAuthentication getPasswordAuthentication() 
                    {
                        return new PasswordAuthentication(username, passwd);
                    }
                });

                System.out.println("receive_email_protocol=" + receive_email_protocol);
                Store store = mail_session.getStore(receive_email_protocol);
                store.connect();
                Folder emailFolder = store.getFolder("INBOX");

                emailFolder.open(Folder.READ_ONLY);

                // retrieve the messages from the folder in an array and print it
                Message[] messages = emailFolder.getMessages();
                System.out.println("messages.length---" + messages.length);

                for (int i = 0, n = messages.length; i < n; i++) {
                    Message message = messages[i];
                    System.out.println("---------------------------------");
                    System.out.println("Email Number " + (i + 1));
                    System.out.println("Subject: " + message.getSubject());
                    System.out.println("From: " + message.getFrom()[0]);
                }

                //close the store and folder objects
                emailFolder.close(false);
                store.close();



                try
                {
                    con.close();
                }
                catch(Exception e)
                {

                }
            }
            catch(Exception e)
            {
                System.out.println("Exception in test.jsp=" + e);
            }

            %>

            
            
            
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN VENDOR JS-->    
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="app-assets/js/core/app-menu.js"></script>
    <script src="app-assets/js/core/app.js"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/popover/popover.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    </body>
    
    <script>
       $(document).ready(function() {
            $("#autocomplete").autocomplete({
                
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_incident_knowledge_article_text",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function( event, ui ) 
                { 
                    $('#autocomplete').val(ui.item.article_description);
                    //window.location.href = ui.item.value;
                    window.open("kb_view.jsp?id=" + ui.item.value, "Knowledge Base Article", "width=600,height=600,top=400,left=400");
                    return false;
                }
            });
        });
    </script>
</html>
<%
%>
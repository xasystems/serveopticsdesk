<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        boolean page_authorized = support.role.authorized(session, "REQUEST","read"); 
        
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
    %>
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">-->
    <!-- END VENDOR CSS-->

    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <link rel="stylesheet" type="text/css" href="assets/css/vue_clip.css">
    
    <div class="clr whtbg p-10 mb-15 position-relative">
        <div class="float-right options">
            <ul>
                <li class="dInlineBlock dropdown ">
                    <div class="dInlineBlock clr menuHolder">
                        <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                        </a>
                        <div class="dropdown-menu dropdown-menu-right md p-10">
                            <h5 class="medium-font boldFont pt-5 mb-10">All Filters</h5>
                            <form>
                                <ul>
                                    <li class="mb-10 clr">
                                        <div class="formField md  mr-0 full-width">
                                            <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="formField md  mr-0 full-width">
                                            <select class="no-border">
                                                <option>Support Group</option>
                                                <option>Support Group2</option>
                                            </select>
                                        </div>
                                    </li>

                                </ul>
                                <div class="text-center pt-5 pb-5">
                                    <button class="btn md2 btn-primary-new customBtn mb-5">Apply Filters</button>
                                    <div class="clr"></div>
                                    <a href="#" class="">Reset Filters</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </li>

            </ul>
        </div>
        <h1 class="large-font boldFont">Service Catalog Administration</h1>
        <div class="custom-tabs clr ">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" data-toggle="tab" href="#catalogs-tab" role="tab" aria-selected="true">Catalogs</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#catalogs-item-tab" role="tab" aria-selected="false">Catalogs Items</a>
                </li>


            </ul><!-- Tab panes -->
        </div>
    </div>
    <div class="tab-content">
        <div class="tab-pane active show" id="catalogs-tab" role="tabpanel">
            <div class=" clr mb-15 text-right ">
                <span class="btn btn-success customBtn lg waves-effect rightSidePanelOpener" data-type="catalog">
                    <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>Create a Catalog
                </span>
            </div>
            <div class="sort-tbl-holder pb-100">
                <h2 class="medium-font boldFont">Catalogs</h2>
                <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th width="20%">Name</th>
                            <th width="40%">Description</th>
                            <th width="15%">State</th>
                            <th width="15%">Type</th>
                            <th width="10%" class="text-center">Actions</th>
                        </tr>
                    </thead>

                    <tbody>
                        <%
                        String[] catalog_names = {"Self Service","Service Desk"};
                        String[] catalog_values = {"self_service","service_desk"};
                        ArrayList <String[]> all_catalogs = db.get_service_catalog.service_catalog_all(con);
                        for(int a = 0; a < all_catalogs.size(); a++)
                        {
                            String catalog[] = all_catalogs.get(a);                                        
                        %>
                        <tr>
                            <td><%=catalog[1]%></td>
                            <td><%=catalog[2]%></td>
                            <%
                            String state = "";
                            String[] state_names = {"Draft","Awaiting Approval","Published","Superceded","Expired","Invalid","Deleted"};
                            String[] state_values = {"Draft","Waiting_Approval","Published","Superceded","Expired","Invalid","Deleted"};
                            for(int c = 0; c < state_values.length; c++)
                            {
                                if(catalog[3].equalsIgnoreCase(state_values[c]))
                                {
                                    state = state_names[c];
                                }
                            }
                            %>                                            
                            <td><%
                                String state_color_class = db.static_data.get_css_class_by_state(state);
                                if (!"".equals(state_color_class))
                                {
                                    %>
                                    <span class="btn sm btn-<%=state_color_class%> customBtn"><%=state%></span>
                                    <%
                                } else
                                {
                                    %>
                                    <%=state%>
                                    <%
                                } 
                                %>
                            </td>
                            <%
                            String type = "";
                            for(int b = 0; b < catalog_values.length; b++)
                            {
                                if(catalog[4].equalsIgnoreCase(catalog_values[b]))
                                {
                                    type = catalog_names[b];
                                }
                            }
                            %>
                            <td><%=type%></td>
                            <td class="text-center">
                                <a href="javascript:void(0)" onclick="editCatalog('<%=catalog[0]%>');" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>
                                <a href="javascript:void(0)" onclick="showDeleteModal('<%=catalog[0]%>', 'catalog')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>
                            </td>
                        </tr>
                        <%
                        }
                        if(all_catalogs.size() == 0)
                        {
                            %>
                            <tr>
                                <td colspan="3">No Catalogs Defined</td>
                            </tr>
                            <%
                        }
                        %>                                        

                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane" id="catalogs-item-tab" role="tabpanel">
            <div class=" clr mb-15 text-right ">
                <span class="btn btn-success customBtn lg waves-effect rightSidePanelOpener" data-type="catalog_item">
                    <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>Create a Catalog Item
                </span>
            </div>
            <div class="sort-tbl-holder pb-100">
                <h2 class="medium-font boldFont">Catalog Items</h2>
                <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th width="20%">Catalog</th>
                            <th width="20%">Name</th>
                            <th width="40%">Description</th>
                            <th width="10%">State</th>
                            <th width="10%" class="text-center">Actions</th>
                        </tr>
                    </thead>

                    <tbody>
                        <%
                        ArrayList <String[]> all_catalog_items = db.get_service_catalog.service_catalog_items_all(con);
                        for(int a = 0; a < all_catalog_items.size(); a++)
                        {
                            String catalog_item[] = all_catalog_items.get(a);                                        
                        %>
                        <tr>
                            <td><%=catalog_item[37]%></td>
                            <td><%=catalog_item[2]%></td>
                            <td><%=catalog_item[3]%></td>

                            <%
                            String  state = "";
                            String[] state_names = {"Draft","Awaiting Approval","Published","Superceded","Expired","Invalid","Deleted"};
                            String[] state_values = {"Draft","Waiting_Approval","Published","Superceded","Expired","Invalid","Deleted"};
                            for(int b = 0; b < state_values.length;b++)
                            {
                                if(catalog_item[5].equalsIgnoreCase(state_values[b]))
                                {
                                    state = state_names[b];
                                }
                            }
                            %>
                            <td><%
                                String state_color_class = db.static_data.get_css_class_by_state(state);
                                if (!"".equals(state_color_class))
                                {
                                    %>
                                    <span class="btn sm btn-<%=state_color_class%> customBtn"><%=state%></span>
                                    <%
                                } else
                                {
                                    %>
                                    <%=state%>
                                    <%
                                } 
                                %>
                            </td>
                            <td class="text-center">
                                <a href="javascript:void(0)" onclick="editCatalogItem('<%=catalog_item[0]%>');" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>
                                <a href="javascript:void(0)" onclick="showDeleteModal('<%=catalog_item[0]%>', 'catalog_item')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>
                            </td>
                        </tr>
                        <%
                        }
                        if(all_catalogs.size() == 0)
                        {
                            %>
                            <tr>
                                <td colspan="4">No Catalog Items Defined</td>
                            </tr>
                            <%
                        }
                        %>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>
                    
    <div class="rightPanelHolder">
        <div class="p-15 ticket-content">
            <span class="rightSidePanelCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
            <h4 id="rightPanelTitle" class="mediumFont large-font mb-20"></h4>
            <div id="rightPanelContent" class="clr whtbg p-15">
            </div>
        </div>
    </div>

    <div class="modal fade modal-xl" id="clipModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header whtbg">
                    <h5 class="modal-title boldFont large-font" id="exampleModalLabel">Clip the image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  <div id="clipper_app"></div>
                  <ul>
                    <li class="text-center pt-15 pb-15">
                        <button type="button" class="btn btn-outline-primary customBtn lg waves-effect " data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary-new customBtn lg waves-effect" role="button" onclick="window.clipper_app.$children[0].getResult()">Apply</button>
                    </li>
                </ul>
              </div>
          </div>        
      </div>
    </div>

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="deleteModalLabel">Delete Confirmation</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" id="deleteModalBody">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-danger" onclick="delete_entity()" id="modalDeleteButton">Delete</button>
            </div>
          </div>
        </div>
    </div>                     

    <script type="text/javascript">
        function showDeleteModal(delete_entity_id, entity_type) 
        {
            switch (entity_type) {
                case "catalog":
                    $("#deleteModalBody").html("Are you sure you want to delete this Catalog");
                    break;
                case "catalog_item":
                    $("#deleteModalBody").html("Are you sure you want to delete this Catalog Item");
                    break;
                default:
                    break;
            }
            $("#modalDeleteButton").prop("data-id", delete_entity_id);
            $("#modalDeleteButton").prop("data-entity", entity_type);
            $('#deleteModal').modal();
        }    
        function delete_entity() 
        {
            let entity_type = $("#modalDeleteButton").prop("data-entity");
            let url = null;
            switch (entity_type) {
                case "catalog":
                    url = "admin_service_catalog_delete";
                    break;
                case "catalog_item":
                    url = "admin_service_catalog_item_delete";
                    break;
                default:
                    break;
            }
            let delete_entity_id = $("#modalDeleteButton").prop("data-id");
            if (url && delete_entity_id) {
                window.location.assign(url + "?id=" + delete_entity_id);
            }
        }    
    </script>
                    
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <script src="assets/js/tinymce/tinymce.min.js"></script>
    <script src="assets/js/bs-custom-file-input.min.js"></script>
    <script src="assets/js/vue_clip.js"></script>

    <script type="text/javascript">
        var ajax_id = "";
        var ajax_type = "";

        function deleteImage()
        {
            var uuid = $(event.target).closest("a").data('uuid');
            var div = $(event.target).closest("div").replaceWith('<input type="hidden" name="image_delete" value="' + uuid +'"/>');
        }

        function editCatalog(id)
        {
            ajax_id = id;
            ajax_type = "catalog";
            $('.rightSidePanelOpener[data-type="catalog"').click();
        }

        function editCatalogItem(id)
        {
            ajax_id = id;
            ajax_type = "catalog_item";
            $('.rightSidePanelOpener[data-type="catalog_item"').click();
        }
        
        $(function() {
            $('.dropdown').on('hide.bs.dropdown', function (e) 
            { // prevent dropdown close on autocomplete // console.log(e.clickEvent.target.className);
                if (e.clickEvent && e.clickEvent.target && (e.clickEvent.target.className == "ui-menu-item-wrapper" || e.clickEvent.target.className.indexOf("applyBtn") !== -1 )) {
                    e.preventDefault();
                }
            });

            $('.rightSidePanelOpener').on('click', function(e){
                $("#rightPanelContent").html("");

                var panel_title = "";
                var ajax = {};
                ajax.type = 'GET';
                ajax.dataType = "html";

                if(e.originalEvent && e.originalEvent.isTrusted)
                {
                    // real click.
                    var url_addon = "_add.jsp";
//                    console.log(e, e.target);
                    ajax_type = $(e.target).data('type');
                    
                } else {
                    // programmatic click        
                    ajax.data = {id: ajax_id};
                    var url_addon = "_edit.jsp";
                    
                }

                switch (ajax_type) 
                {
                    case "catalog":
                        ajax.url = "admin_service_catalog";
                        panel_title = (url_addon.indexOf("add") ? "Create a New" : "Edit") + " Service Catalog";
                        break;
                    case "catalog_item":
                        ajax.url = "admin_service_catalog_item";
                        panel_title = (url_addon.indexOf("add") ? "Create a New" : "Edit") + " Service Catalog Item";
                        break;
                    default:
                        ajax.url = "";
                        break;
                }
                if (ajax.url === "") return false;

                ajax.url += url_addon;

                ajax.error = function(jqXHR, textStatus)
                {
                    switch (jqXHR.status) {
                        case 401:
                            window.location.assign("<%=props.get("login_url").toString()%>");
                            break;
                        case 403:
                            alert("No permission!");
                            break;
                        default:
                            alert("Error processing request!");
                    }
                };
                
                ajax.success = function (data) 
                {                    
                    $("#rightPanelTitle").html(panel_title);
                    $("#rightPanelContent").html(data);
                    bsCustomFileInput.init();
                    $('.rightSidePanelCloser').on('click', function(){
                        $('.rightPanelHolder').removeClass('open');
                        $('.rightSidePanel').removeClass('open');
                        $('html').removeClass('panelOpen');
                        ajax_id = "";
                        ajax_type = "";
                        tinymce.remove('textarea');
                    });

                    if (ajax_type === "catalog")
                    {
                        window.clipper_app.$children[0].wPixel=64;
                    } else if (ajax_type === "catalog_item")
                    {
                        window.clipper_app.$children[0].wPixel=300;
                        tinymce.remove('textarea');
                        $( "#category" ).autocomplete(
                        {
                            source: function( request, response ) 
                            {
                                // Fetch data
                                $.ajax({
                                    url: "ajax_lookup_service_catalog_category",
                                    type: 'post',
                                    dataType: "json",
                                    minLength: 1,
                                    data: 
                                    {
                                        search: request.term
                                    },
                                    success: function( data ) 
                                    {
                                        response( data );
                                    }
                                });
                            },
                            select: function (event, ui) 
                            {
                                // Set selection
                                $('#category').val(ui.item.label); // set category
                                return false;
                            }
                        });
                        
                        tinymce.init({
                            selector: 'textarea',
                            setup: function (editor) {
                                editor.on('change', function () {
                                    editor.save();
                                });
                            },
                            height: 250,
                            plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
                            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code',
                            image_advtab: true,
                            templates: [
                                { title: 'Test template 1', content: 'Test 1' },
                                { title: 'Test template 2', content: 'Test 2' }
                            ],
                            content_css: [
                                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                                '//www.tinymce.com/css/codepen.min.css'
                            ]
                        });    
                        
                        $( "#approver_name" ).autocomplete(
                        {
                            source: function( request, response ) 
                            {
                                // Fetch data
                                $.ajax({
                                    url: "ajax_lookup_user_last_first",
                                    type: 'post',
                                    dataType: "json",
                                    minLength: 1,
                                    data: 
                                    {
                                        search: request.term
                                    },
                                    success: function( data ) 
                                    {
                                        response( data );
                                    }
                                });
                            },
                            select: function (event, ui) 
                            {

                                // Set selection
                                $('#approver_name').val(ui.item.label); // set name
                                $('#approver_id').val(ui.item.value); // set id
                                return false;
                            }
                        });

                        const btnAdd = document.querySelector('#btnAdd');
                        const btnRemove = document.querySelector('#btnRemove');
                        const approver_list = document.getElementById("approver_list");
                        const approver_id = document.getElementById("approver_id");
                        const approver_name = document.getElementById("approver_name");


                        btnAdd.onclick = (e) => 
                        {
                            e.preventDefault();

                            // validate the option
                            if (approver_name.value == '' || approver_id.value == '') 
                            {
                                //alert('Please enter the name.');
                                return;
                            }
                            // create a new option
                            const option = new Option(approver_name.value, approver_id.value);
                            // add it to the list
                            approver_list.add(option, undefined);
                            // reset the value of the input
                            approver_name.value = '';
                            approver_name.focus();
                        };

                        // remove selected option
                        btnRemove.onclick = (e) => {
                            e.preventDefault();

                            // save the selected option
                            let selected = [];

                            for (let i = 0; i < approver_list.options.length; i++) {
                                selected[i] = approver_list.options[i].selected;
                            }

                            // remove all selected option
                            let index = approver_list.options.length;
                            while (index--) {
                                if (selected[index]) {
                                    approver_list.remove(index);
                                }
                            }
                        };

                        $( "#critical_delivery_group_name" ).autocomplete(
                        {
                            source: function( request, response ) 
                            {
                                // Fetch data
                                $.ajax({
                                    url: "ajax_lookup_group",
                                    type: 'post',
                                    dataType: "json",
                                    minLength: 2,
                                    data: 
                                    {
                                        search: request.term
                                    },
                                    success: function( data ) 
                                    {
                                        response( data );
                                    }
                                });
                            },
                            select: function (event, ui) 
                            {
                                // Set selection
                                $('#critical_delivery_group_name').val(ui.item.label); // set name
                                $('#critical_delivery_group_id').val(ui.item.value); // set id
                                return false;
                            }
                        });

                        $( "#high_delivery_group_name" ).autocomplete(
                        {
                            source: function( request, response ) 
                            {
                                // Fetch data
                                $.ajax({
                                    url: "ajax_lookup_group",
                                    type: 'post',
                                    dataType: "json",
                                    minLength: 2,
                                    data: 
                                    {
                                        search: request.term
                                    },
                                    success: function( data ) 
                                    {
                                        response( data );
                                    }
                                });
                            },
                            select: function (event, ui) 
                            {
                                // Set selection
                                $('#high_delivery_group_name').val(ui.item.label); // set name
                                $('#high_delivery_group_id').val(ui.item.value); // set id
                                return false;
                            }
                        });

                        $( "#medium_delivery_group_name" ).autocomplete(
                        {
                            source: function( request, response ) 
                            {
                                // Fetch data
                                $.ajax({
                                    url: "ajax_lookup_group",
                                    type: 'post',
                                    dataType: "json",
                                    minLength: 2,
                                    data: 
                                    {
                                        search: request.term
                                    },
                                    success: function( data ) 
                                    {
                                        response( data );
                                    }
                                });
                            },
                            select: function (event, ui) 
                            {
                                // Set selection
                                $('#medium_delivery_group_name').val(ui.item.label); // set name
                                $('#medium_delivery_group_id').val(ui.item.value); // set id
                                return false;
                            }
                        });

                        $( "#low_delivery_group_name" ).autocomplete(
                        {
                            source: function( request, response ) 
                            {
                                // Fetch data
                                $.ajax({
                                    url: "ajax_lookup_group",
                                    type: 'post',
                                    dataType: "json",
                                    minLength: 2,
                                    data: 
                                    {
                                        search: request.term
                                    },
                                    success: function( data ) 
                                    {
                                        response( data );
                                    }
                                });
                            },
                            select: function (event, ui) 
                            {
                                // Set selection
                                $('#low_delivery_group_name').val(ui.item.label); // set name
                                $('#low_delivery_group_id').val(ui.item.value); // set id
                                return false;
                            }
                        });
                        
                    } // catalog-item-case-end
                };
                
                console.log("calling ajax" + Date.now());
                $.ajax(ajax);
            });

        });

        function selectAll(selectBox,selectAll) 
        { 
            if (typeof selectBox == "string") 
            { 
                selectBox = document.getElementById(selectBox);
            } 
            if (selectBox.type == "select-multiple") 
            { 
                for (var i = 0; i < selectBox.options.length; i++) 
                { 
                     selectBox.options[i].selected = selectAll; 
                } 
            }
        }

        function validateForm() 
        {
            //select all users
            selectAll(approver_list, true);
            
            var error = "";
            var name = document.getElementById("name").value;
            var description = document.getElementById("description").value;
            
            if (name === "") 
            {
                error = "Name is required<br>";
            }
            if (description === "" )
            {
                if(error === "")
                {
                    error = "Description is required<br>";
                }
                else
                {
                    error = error + "Description is required<br>";
                }
            }     
            
            //check dollar values
            var regex  = /^\d+(?:\.\d{0,2})$/;
            
            
            //critical_fixed_cost
            if(regex.test(document.getElementById("critical_fixed_cost").value))
            {
                //good value
            }
            else
            {
                //bad value
                if(error === "")
                {
                    error = "Critical fixed cost is not valid. (N.NN) <br>";
                }
                else
                {
                    error = error + "Critical fixed cost is not valid. (N.NN) <br>";
                }
            }
            //critical_recurring_cost
            if(regex.test(document.getElementById("critical_recurring_cost").value))
            {
                //good value
            }
            else
            {
                //bad value
                if(error === "")
                {
                    error = "Critical recurring cost is not valid. (N.NN) <br>";
                }
                else
                {
                    error = error + "Critical recurring cost is not valid. (N.NN) <br>";
                }
            }
            //high_fixed_cost
            if(regex.test(document.getElementById("high_fixed_cost").value))
            {
                //good value
            }
            else
            {
                //bad value
                if(error === "")
                {
                    error = "High fixed cost is not valid. (N.NN) <br>";
                }
                else
                {
                    error = error + "High fixed cost is not valid. (N.NN) <br>";
                }
            }
            //high_recurring_cost
            if(regex.test(document.getElementById("high_recurring_cost").value))
            {
                //good value
            }
            else
            {
                //bad value
                if(error === "")
                {
                    error = "High recurring cost is not valid. (N.NN) <br>";
                }
                else
                {
                    error = error + "High recurring cost is not valid. (N.NN) <br>";
                }
            }
            //medium_fixed_cost
            if(regex.test(document.getElementById("medium_fixed_cost").value))
            {
                //good value
            }
            else
            {
                //bad value
                if(error === "")
                {
                    error = "Medium fixed cost is not valid. (N.NN) <br>";
                }
                else
                {
                    error = error + "Medium fixed cost is not valid. (N.NN) <br>";
                }
            }
            //medium_recurring_cost
            if(regex.test(document.getElementById("medium_recurring_cost").value))
            {
                //good value
            }
            else
            {
                //bad value
                if(error === "")
                {
                    error = "Medium recurring cost is not valid. (N.NN) <br>";
                }
                else
                {
                    error = error + "Medium recurring cost is not valid. (N.NN) <br>";
                }
            }
            //low_fixed_cost
            if(regex.test(document.getElementById("low_fixed_cost").value))
            {
                //good value
            }
            else
            {
                //bad value
                if(error === "")
                {
                    error = "Low fixed cost is not valid. (N.NN) <br>";
                }
                else
                {
                    error = error + "Low fixed cost is not valid. (N.NN) <br>";
                }
            }
            //low_recurring_cost
            if(regex.test(document.getElementById("low_recurring_cost").value))
            {
                //good value
            }
            else
            {
                //bad value
                if(error === "")
                {
                    error = "Low recurring cost is not valid. (N.NN) <br>";
                }
                else
                {
                    error = error + "Low recurring cost is not valid. (N.NN) <br>";
                }
            }
            
            var numbers = /^[0-9]+$/;
            //critical_delivery_time
            if(numbers.test(document.getElementById("critical_delivery_time").value))
            {
                //good value
            }
            else
            {
                //bad value
                if(error === "")
                {
                    error = "Critical delivery time must be a number. '0' = No delivery time/SLA<br>";
                }
                else
                {
                    error = error + "Critical delivery time must be a number. '0' = No delivery time/SLA<br>";
                }
            }
            //high_delivery_time
            if(numbers.test(document.getElementById("high_delivery_time").value))
            {
                //good value
            }
            else
            {
                //bad value
                if(error === "")
                {
                    error = "High delivery time must be a number. '0' = No delivery time/SLA<br>";
                }
                else
                {
                    error = error + "High delivery time must be a number. '0' = No delivery time/SLA<br>";
                }
            }
            //medium_delivery_time
            if(numbers.test(document.getElementById("medium_delivery_time").value))
            {
                //good value
            }
            else
            {
                //bad value
                if(error === "")
                {
                    error = "Medium delivery time must be a number. '0' = No delivery time/SLA<br>";
                }
                else
                {
                    error = error + "Medium delivery time must be a number. '0' = No delivery time/SLA<br>";
                }
            }
            //low_delivery_time
            if(numbers.test(document.getElementById("low_delivery_time").value))
            {
                //good value
            }
            else
            {
                //bad value
                if(error === "")
                {
                    error = "Low delivery time must be a number. '0' = No delivery time/SLA<br>";
                }
                else
                {
                    error = error + "Low delivery time must be a number. '0' = No delivery time/SLA<br>";
                }
            }
            
            if(error !== "")
            {
                var errorMsg = "<p>" + error + "</p>"
                document.getElementById("modal_text").innerHTML = errorMsg
                $("#error_modal").modal("show");
                //alert(error);
                return false;
            }
            else
            {
                return true;
            }
        }

    </script>
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_service_catalog.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_service_catalog.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
%>
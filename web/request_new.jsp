<!--Copyright 2020 Real Data Technologies, Inc. , All rights reserved.-->
<%-- 
    Document   : incident_new.jsp
    Created on : 18-Jan-2020, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>

<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        if (REQUEST.contains("create") || ADMINISTRATION.equalsIgnoreCase("true") || MANAGER.equalsIgnoreCase("true")) 
        {
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
            java.util.Date now = new java.util.Date();
            String now_display_format = display_format.format(now);
            String now_timestamp_format = timestamp_format.format(now);
            String now_incident_time = date_time_picker_format.format(now);
            Connection con = null;
            try
            {
                con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
            }
            catch(Exception e)
            {
                System.out.println("request_new.jsp exception=" + e);
            }
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <!-- Page specific CSS-->
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <!--End Page specific CSS-->
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END Page Level CSS-->
    
    
    <div class="app-content content">
        <form action="request_new" method="post">
            <!--future fields-->
            <input type="hidden" name="impact" id="impact" value=""/>
            <input type="hidden" name="urgency" id="urgency" value=""/>
            
            <div class="content-wrapper">
                <!-- start content here-->
                <div class="content-header row">
                    <div class="content-header-left col-md-6 col-12 mb-2">
                        <h3 class="content-header-title"><i class="la la-shopping-cart"></i>&nbsp;New Request</h3>
                        <div class="row breadcrumbs-top">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a></li>
                                    <li class="breadcrumb-item active">New Request</li>
                                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-info" type="submit" name="submit" value="Submit"/></li>
                                </ol>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Create a New Request</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>                                    
                                </div>
                            </div> 
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Request ID</label>
                                            <input name="id" id="id" type="text" readonly value="" class="form-control" disabled placeholder="Assigned on save"/>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Request Time</label>
                                            <div class='input-group'>
                                                <input type='text' id="request_date" name="request_date" value="" class="form-control datetime" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <span class="la la-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <!--<input type="text" id="incident_time" name="incident_time" value="<%=now_display_format%>" class="form-control"/>-->
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Contact Method</label>
                                            <%
                                            ArrayList<String[]> contact_method_select = db.get_system_select_field.active_for_table_column(con, "request", "contact_method");
                                            %>
                                            <select name="contact_method" id="contact_method" class="form-control">
                                                <option value=""></option>
                                                <%
                                                for(int a = 0; a < contact_method_select.size(); a++)
                                                {
                                                    String select_option[] = contact_method_select.get(a);
                                                    //select_option[3] = value    select_option[4] = label
                                                    %>
                                                    <option value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                                                    <%
                                                }
                                                %>
                                            </select>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>State</label>
                                            <%
                                            ArrayList<String[]> state_select = db.get_system_select_field.active_for_table_column(con, "request", "state");
                                            %>
                                            <select name="state" id="state" class="form-control">
                                                <%
                                                 
                                                for(int a = 0; a < state_select.size(); a++)
                                                {
                                                    String select_option[] = state_select.get(a);
                                                    //select_option[3] = value    select_option[4] = label
                                                    %>
                                                    <option value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                                                    <%
                                                }
                                                %>
                                            </select>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Approval</label>
                                            <%
                                            ArrayList<String[]> approval_select = db.get_system_select_field.active_for_table_column(con, "request", "approval");
                                            %>
                                            <select name="approval" id="approval" class="form-control">
                                                <%
                                                 
                                                for(int a = 0; a < approval_select.size(); a++)
                                                {
                                                    String select_option[] = approval_select.get(a);
                                                    //select_option[3] = value    select_option[4] = label
                                                    %>
                                                    <option value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                                                    <%
                                                }
                                                %>
                                            </select>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Priority</label>
                                            <%
                                            ArrayList<String[]> priority_select = db.get_system_select_field.active_for_table_column(con, "request", "priority");
                                            %>
                                            <select name="priority" id="priority" class="form-control">
                                                <option value=""></option>
                                                <%
                                                for(int a = 0; a < priority_select.size(); a++)
                                                {
                                                    String select_option[] = priority_select.get(a);
                                                    //select_option[3] = value    select_option[4] = label
                                                    %>
                                                    <option value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                                                    <%
                                                }
                                                %>
                                            </select>
                                        </div>
                                    </div>        
                                    <div class="row">        
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Requester&nbsp;<i class="la la-search" style="font-size: 14px;"></i></label>
                                            <input type="hidden" name="requested_for_id" id="requested_for_id" value=""/>
                                            <input type="text" name="requester_username" id="requester_username" value="" class="form-control" placeholder="Enter the requesters username"/>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Requester VIP?</label>
                                            <br>
                                            <input name="vip" id="vip" type="checkbox" disabled=""/>&nbsp;<em>Set by Requester's profile</em> 
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Requesters Group&nbsp;<i class="la la-search" style="font-size: 14px;"></i></label>
                                            <input type="hidden" name="requested_for_group_id" id="requested_for_group_id"/>
                                            <input type="text" name="requester_group_name" id="requester_group_name" value="" class="form-control" placeholder="Enter the requesters Group"/>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Request Create By&nbsp;<i class="la la-search" style="font-size: 14px;"></i></label>
                                            <input type="hidden" name="create_by_id" id="create_by_id"/>
                                            <input type="text" name="create_by_username" id="create_by_username" value="" class="form-control" placeholder="Enter the creaters username"/>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Assigned to Group&nbsp;<i class="la la-search" style="font-size: 14px;"></i></label>
                                            <input type="hidden" name="assigned_group_id" id="assigned_group_id" value="0"/>
                                            <input type="text" name="assigned_group_name" id="assigned_group_name" value="Group Unassigned" class="form-control" />
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label>
                                                <br>
                                                Assigned to&nbsp;
                                                <i class="la la-search" style="font-size: 14px;"></i>
                                                <i class="la la-question-circle" style="font-size: 14px;" title="This field is filtered by the Assigned to Group field. If Group Unassigned is selected, then all users will be available. If a specific group is selected, then only the members of that group will be available. If the Assigned to Group is changed this field will be cleared."></i>
                                            </label>
                                            <input type="hidden" name="assigned_to_id" id="assigned_to_id" value="0"/>
                                            <input type="text" name="assigned_to_name" id="assigned_to_name" value="" class="form-control" placeholder="Enter the Assigned To group's name"/>
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Location&nbsp;<i class="la la-search" style="font-size: 14px;"></i></label>
                                            <input type="text" id="location" name="location" value="" class="form-control">
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Department&nbsp;<i class="la la-search" style="font-size: 14px;"></i></label>
                                            <input type="text" id="department" name="department" value="" class="form-control">
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Site&nbsp;<i class="la la-search" style="font-size: 14px;"></i></label>
                                            <input type="text" id="site" name="site" value="" class="form-control">
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Company&nbsp;<i class="la la-search" style="font-size: 14px;"></i></label>
                                            <input type="text" id="company" name="company" value="" class="form-control">
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <label><br>Description</label>
                                            <input type="text" name="description" id="description" value="" class="form-control"/> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <br>
                                            <p class="text-muted">Notes <em>(Customer viewable)</em></p>
                                            <textarea class="form-control" name="notes" id="notes" rows="5"></textarea>  
                                        </div>
                                        <div class="col-6">
                                            <br>
                                            <p class="text-muted">Notes <em>(Service Desk)</em></p>
                                            <textarea class="form-control" name="desk_notes" id="desk_notes" rows="5"></textarea>  
                                        </div>
                                    </div>
                                            
                                            
                                    <div class="row">
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Resolved Date</label>
                                            <div class='input-group'>
                                                <input type='text' id="resolve_date" name="resolve_date" value="" class="form-control datetime" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <span class="la la-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Closure Date</label>
                                            <div class='input-group'>
                                                <input type='text' id="closed_date" name="closed_date" value="" class="form-control datetime" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <span class="la la-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Closure Reason</label>
                                            <%
                                            ArrayList<String[]> closed_reason_select = db.get_system_select_field.active_for_table_column(con, "request", "closed_reason");
                                            %>
                                            <select name="closed_reason" id="closed_reason" class="form-control">
                                                <option value=""></option>
                                                <%
                                                for(int a = 0; a < closed_reason_select.size(); a++)
                                                {
                                                    String select_option[] = closed_reason_select.get(a);
                                                    //select_option[3] = value    select_option[4] = label
                                                    %>
                                                    <option value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                                                    <%
                                                }
                                                %>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-12 mt-1">
                                            <hr>
                                        </div>
                                    </div>        
                                            
                                    <div class="row">
                                        <div class="col-12 mt-1">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-info" type="submit" name="submit" value="Submit"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 mt-1">
                                            &nbsp;
                                        </div>
                                    </div>  
                                    <div class="row">
                                        <div class="col-12 mt-1">
                                            &nbsp;
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>
                <!-- end content here-->
            </div> 
        </form>
    </div>
    <br>&nbsp;
    <br>&nbsp;
    <br>&nbsp;
    <br>&nbsp;
    
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN VENDOR JS-->    
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="app-assets/js/core/app-menu.js"></script>
    <script src="app-assets/js/core/app.js"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/popover/popover.js"></script>
    
    
    
    <script>     
       $( function() 
       {
            //caller
            $( "#requester_username" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#requester_username').val(ui.item.label); // display the selected text
                    $('#requested_for_id').val(ui.item.value); // save selected id to input
                    $('#location').val(ui.item.location); // save selected id to input
                    $('#department').val(ui.item.department); // save selected id to input
                    $('#site').val(ui.item.site); // save selected id to input
                    $('#company').val(ui.item.company); // save selected id to input
                    //if vip = true then check the vip checkbox
                    var vip = ui.item.vip;
                    if(vip.toLowerCase() === "true")
                    {
                        document.getElementById("vip").checked = true;
                    }
                    else
                    {
                        document.getElementById("vip").checked = false;
                    }
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            });
            //caller_group_name
            $( "#requester_group_name" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_group",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#requester_group_name').val(ui.item.label); // display the selected text
                    $('#requested_for_group_id').val(ui.item.value); // save selected id to input
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            });
            
            //create_by
            $( "#create_by_username" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#create_by_username').val(ui.item.label); // display the selected text
                    $('#create_by_id').val(ui.item.value); // save selected id to input
                    return false;
                }
            });
            //location
            $( "#location" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user_location",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#location').val(ui.item.label); // display the selected text
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            }); 
            //department
            $( "#department" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user_department",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#department').val(ui.item.label); // display the selected text
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            });
            //site
            $( "#site" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user_site",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#site').val(ui.item.label); // display the selected text
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            }); 
            //company
            $( "#company" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user_company",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#company').val(ui.item.label); // display the selected text
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            }); 
            //assigned_group
            $( "#assigned_group_name" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_group",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#assigned_to_name').val(""); // clear field
                    $('#assigned_to_id').val(""); // clear field
                    $('#assigned_group_name').val(ui.item.label); // set name
                    $('#assigned_group_id').val(ui.item.value); // set id
                    return false;
                }
            });
            //assigned_to_name
            $( "#assigned_to_name" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    var var_group_id = $('#assigned_group_id').val();
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_group_user",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term,
                            group_id: var_group_id                            
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#assigned_to_name').val(ui.item.label); // display the selected text
                    $('#assigned_to_id').val(ui.item.value); // save selected id to input
                    //alert("assigned to setting is=" + ui.item.value);
                    return false;
                }
            });
        });
    </script>
    
    <script>
        $(function() 
        {

          $('input[name="request_date"]').daterangepicker(
            {
                "startDate": new Date(),
                "singleDatePicker": true,
                "showDropdowns": true,
                "timePicker": true,
                "timePicker24Hour": true,
                "locale": {
                "format": "HH:mm MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            }

          }, 
          function(start, end, label) 
          {
            
          });
        });
    </script>
    
    <script type="text/javascript">
        $(function()
        {
          $('input[name="resolve_date"]').daterangepicker({
              "autoUpdateInput": false,
              "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
          $('input[name="resolve_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });

          $('input[name="resolve_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });
        });
    </script>
    
    <script>
        $(function()
        {
          $('input[name="closed_date"]').daterangepicker({
              "autoUpdateInput": false,
              "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
          $('input[name="closed_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });

          $('input[name="closed_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });
        });
    </script>
    
    
    
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>
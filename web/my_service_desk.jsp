<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : my_service_desk
    Created on : Aug 10, 2020, 6:13:59 PM
    Author     : Ralph
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.sql.*" %>
<!DOCTYPE html>    
    <%
    String context_dir = request.getServletContext().getRealPath("");
    LinkedHashMap props = support.config.get_config(context_dir);
        
    if(session.getAttribute("authenticated")==null)
    {
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        String inc_chart_values = "[0,0,0,0]";
        String req_chart_values = "[0,0,0,0]";
        String prj_chart_values = "[0,0,0,0]";
        String job_chart_values = "[0,0,0,0]";
        try
        {
            //update home in the users session to my_service_desk.jsp
            session.setAttribute("home_page", "my_service_desk.jsp");
            //page authorization
            //session vars
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");   
            java.util.Date object_date = new java.util.Date();
            GregorianCalendar first_of_month = new GregorianCalendar();
            first_of_month.set(Calendar.DAY_OF_MONTH, 1);
            first_of_month.set(Calendar.HOUR_OF_DAY, 0);
            first_of_month.set(Calendar.MINUTE, 0);
            first_of_month.set(Calendar.SECOND, 0);     
            GregorianCalendar now = new GregorianCalendar();
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            boolean has_a_display = false;
            //get role
            String administration = session.getAttribute("administration").toString();
            String manager = session.getAttribute("manager").toString();
   %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	
    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>

    <!--Start Page level css-->
    <link rel="stylesheet" type="text/css" href="assets/css/dashboard.css">
    <!--End Page level css-->
    
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title">
                        <i class="la ft-users"></i>
                        &nbsp;&nbsp;&nbsp;My View&nbsp;&nbsp;&nbsp;<input type="button" value="Switch to Service Desk View" class="btn btn-info" onclick="location.href='home_service_desk.jsp';"/>
                        
                    </h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="home_service_desk.jsp">Desk Home</a>
                                </li>
                                <li class="breadcrumb-item active">My Service Desk
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row match-height">
                    <%
                    //incident CRUD
                    boolean incident_authorized = support.role.authorized(session, "incident","read");
                    ArrayList <Integer> incident_values = new ArrayList();
                    String incident_display = "none";
                    if(incident_authorized)
                    {
                        incident_display = "inline";
                        has_a_display = true;
                        //get my incident counts
                        incident_values = db.get_incidents.my_incidents_count(con, user_id);
                        inc_chart_values = "[" + incident_values.get(1) + "," + incident_values.get(2) + "," + incident_values.get(3) + "," + incident_values.get(4) + "]";
                    }
                    else
                    {
                        incident_display = "none";
                    }
                    %>
                    <div class="col-xl-3 col-md-6 col-sm-12" style="display: <%=incident_display%>">
                        <div class="card">
                            <div class="card-header">
                                <h2 style="cursor: pointer;" onclick="location.href='incident_list.jsp?predefined=assigned_to_me';">
                                    <i class="la la-wrench font-large-1 "></i>
                                    Incidents&nbsp;<%=incident_values.get(0)%>
                                    &nbsp;&nbsp;&nbsp;
                                    <a href="incident_new.jsp" class="btn-sm btn-info"> New </a>
                                </h2>
                                <!--<h4 class="card-title"><i class="la la-wrench"></i>Incidents&nbsp;<%=incident_values.get(0)%></h4>-->
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                
                            </div> 
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <canvas id="incident_doughnut" height="120" data-toggle="tooltip" data-placement="top">
                                                <%
                                                if(incident_values.get(0).toString().equalsIgnoreCase("0"))
                                                {
                                                    out.print("You have 0 Incidents");
                                                }
                                                %>
                                            </canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%
                    //request CRUD
                    ArrayList <Integer> request_values = new ArrayList();
                    boolean request_authorized = support.role.authorized(session, "request","read");
                    String request_display = "none";
                    if(request_authorized)
                    {
                        request_display = "inline";
                        has_a_display = true;
                        //get my incident counts
                        request_values = db.get_requests.my_requests(con, user_id);
                        req_chart_values = "[\"" + request_values.get(1) + "\",\"" + request_values.get(2) + "\",\"" + request_values.get(3) + "\",\"" + request_values.get(4) + "\"]";
                    }
                    else
                    {
                        request_display = "none";
                    }
                    %>
                    <div class="col-xl-3 col-md-6 col-sm-12" style="display: <%=request_display%>">
                        <div class="card">
                            <div class="card-header">
                                <h2 style="cursor: pointer;" onclick="location.href='request_list.jsp?predefined=assigned_to_me';">
                                    <i class="la la-shopping-cart font-large-1"></i>
                                    Requests&nbsp;<%=request_values.get(0)%>
                                    &nbsp;&nbsp;&nbsp;<a href="home_service_catalog.jsp" class="btn-sm btn-info"> New </a>
                                </h2>
                                <!--<h4 class="card-title"><i class="la la-shopping-cart"></i>Requests&nbsp;<%=request_values.get(0)%></h4>-->
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            </div> 
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">                                            
                                            <canvas id="request_doughnut" height="120" data-toggle="tooltip" data-placement="top">
                                                <%
                                                if(request_values.get(0).toString().equalsIgnoreCase("0"))
                                                {
                                                    out.print("You have 0 Requests");
                                                }
                                                %>
                                            </canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%
                    ArrayList <Integer> project_values = new ArrayList();
                    int task_count = 0;
                    //project CRUD
                    boolean project_authorized = support.role.authorized(session, "project","read");
                    String project_display = "none";
                    if(project_authorized)
                    {
                        project_display = "inline";
                        has_a_display = true;
                        //get my incident counts
                        project_values = db.get_projects.my_projects(con, user_id);
                        prj_chart_values = "[" + project_values.get(1) + "," + project_values.get(2) + "," + project_values.get(3) + "," + project_values.get(4) + "]";
                        task_count = db.get_projects.open_project_tasks_count_for_user_id(con, user_id);
                    }
                    else
                    {
                        project_display = "none";
                    }
                    %>
                    <div class="col-xl-3 col-md-6 col-sm-12" style="display: <%=project_display%>">
                        <div class="card">
                            <div class="card-header" title="My Projects">
                                <h2 style="cursor: pointer;" onclick="location.href='my_projects.jsp';">
                                    <i class="la la-building font-large-1"></i>
                                    Projects&nbsp;<%=project_values.get(0)%>&nbsp;&nbsp;&nbsp;Tasks&nbsp;<%=task_count%>
                                    &nbsp;&nbsp;&nbsp;
                                    <a href="project_new.jsp" class="btn-sm btn-info"> New </a>
                                </h2>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>                                
                            </div> 
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <canvas id="project_doughnut" height="120" data-toggle="tooltip" data-placement="top">
                                                <%
                                                if(project_values.get(0).toString().equalsIgnoreCase("0"))
                                                {
                                                    out.print("You have 0 Projects/Tasks");
                                                }
                                                %>
                                            </canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%
                    ArrayList <Integer> job_values = new ArrayList();
                    //request CRUD
                    boolean job_authorized = support.role.authorized(session, "job","read");
                    String job_display = "none";
                    if(job_authorized)
                    {
                        job_display = "inline";
                        has_a_display = true;
                        job_values = db.get_jobs.my_open_jobs_count_by_priority(con, user_id);
                        job_chart_values = "[" + job_values.get(1) + "," + job_values.get(2) + "," + job_values.get(3) + "," + job_values.get(4) + "]";
                    }
                    else
                    {
                        job_display = "none";
                    }
                    %>
                    <div class="col-xl-3 col-md-6 col-sm-12" style="display: <%=job_display%>">
                        <div class="card">
                            <div class="card-header">
                                <h2 style="cursor: pointer;" onclick="location.href='job_list.jsp?predefined=my_open_jobs';">
                                    <i class="la la-industry"></i>
                                    Jobs&nbsp;<%=job_values.get(0)%>
                                    &nbsp;&nbsp;&nbsp;
                                    <a href="job_new.jsp" class="btn-sm btn-info"> New </a>
                                </h2>
                                <!--<h4 class="card-title"><i class="la la-industry"></i>Jobs&nbsp;<%=job_values.get(0)%></h4>-->
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            </div> 

                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <canvas id="job_doughnut" height="120" data-toggle="tooltip" data-placement="top">
                                                <%
                                                if(job_values.get(0).toString().equalsIgnoreCase("0"))
                                                {
                                                    out.print("You have 0 Jobs");
                                                }
                                                %>
                                            </canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">My List</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                </ul>                                    
                            </div>
                        </div> 

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="row">
                                            <table id="incident_table" class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                    <tr>
                                                        <th class="no-sort nowrap">ID</th>
                                                        <th>Description</th>
                                                        <th>Priority</th>
                                                        <th>Time</th>
                                                        <th>State</th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <%
                                                    ArrayList <String[]> table_content = db.my_data.my_list(con, user_id);
                                                    for(int a = 0; a < table_content.size();a++)
                                                    {
                                                        String date = "";
                                                        
                                                        String row[] = table_content.get(a);
                                                        try
                                                        {
                                                            object_date.setTime(Long.parseLong(row[5]));
                                                            date = filter_format.format(object_date);
                                                        }
                                                        catch(Exception e)
                                                        {
                                                            date = "--";
                                                        }
                                                        String icon = "";
                                                        String priority = "";
                                                        String type = row[1];
                                                        String pri = row[4];
                                                        if(type.startsWith("INC"))
                                                        {
                                                            icon = "la la-wrench";
                                                        }else if(type.startsWith("REQ"))
                                                        {
                                                            icon = "la la-shopping-cart";
                                                        }else if(type.startsWith("TSK"))
                                                        {
                                                            icon = "la la-building";
                                                        }else if(type.startsWith("JOB"))
                                                        {
                                                            icon = "la la-industry";
                                                        } 
                                                        if(pri.equalsIgnoreCase("critical"))
                                                        {
                                                            priority = "critical";
                                                        }else if(pri.equalsIgnoreCase("high"))
                                                        {
                                                            priority = "high";
                                                        }else if(pri.equalsIgnoreCase("medium"))
                                                        {
                                                            priority = "medium";
                                                        }else if(pri.equalsIgnoreCase("low"))
                                                        {
                                                            priority = "low";
                                                        } 
                                                        %>
                                                        <tr onclick="location.href='<%=row[0]%>';" style="cursor:pointer;">
                                                        <td nowrap><i class="<%=icon%> <%=priority%>"></i>&nbsp;<%=row[1]%></td>
                                                        <td><%=row[3]%></td>
                                                        <td><%=row[4]%></td>
                                                        <td><%=date%></td>
                                                        <td><%=row[6]%></td>
                                                        </tr>
                                                        <%
                                                    }
                                                    %>
                                                </tbody>
                                            </table>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
            <!-- end content here-->
        </div>        
    </div>
    
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/vendors/js/charts/chart.min.js"></script>
    <script>
        var incident_div = document.getElementById("incident_doughnut");
        var ctxP = incident_div.getContext('2d');
        var incident_doughnut = new Chart(ctxP, {
           type: 'doughnut',
           data: {
              labels: ["Critical", "High", "Medium", "Low"],
              datasets: [{
                 data: <%=inc_chart_values%>,
                  backgroundColor: [
                        '#ef5350',    // color for data at index 0
                        '#ffa726',   // color for data at index 1
                        '#ffee58',  // color for data at index 2
                        '#64b5f6'  // color for data at index 3
                    ],
                 hoverBackgroundColor: ["#DF0101", "#FFBF00", "#FFFF00", "#00BFFF"]
              }]
           },
           options: {
              legend: {
                 display: false
              }
           }
        });

        incident_div.onclick = function(e) {
           var slice = incident_doughnut.getElementAtEvent(e);
           if (!slice.length) return; // return if not clicked on slice
           var label = slice[0]._model.label;
           switch (label) {
              // add case for each label/slice
              case 'Critical':
                 parent.location='incident_list.jsp?predefined=my_critical';
                 break;
              case 'High':
                 parent.location='incident_list.jsp?predefined=my_high';
                 break;
              case 'Medium':
                 parent.location='incident_list.jsp?predefined=my_medium';
                 break;
              case 'Low':
                 parent.location='incident_list.jsp?predefined=my_low';
                 break;
           }
        }
    </script>
    <script>
        var request_div = document.getElementById("request_doughnut");
        var ctxP = request_div.getContext('2d');
        var request_doughnut = new Chart(ctxP, {
           type: 'doughnut',
           data: {
              labels: ["Critical", "High", "Medium", "Low"],
              datasets: [{
                 data: <%=req_chart_values%>,
                  backgroundColor: [
                        '#ef5350',    // color for data at index 0
                        '#ffa726',   // color for data at index 1
                        '#ffee58',  // color for data at index 2
                        '#64b5f6'  // color for data at index 3
                    ],
                 hoverBackgroundColor: ["#DF0101", "#FFBF00", "#FFFF00", "#00BFFF"]
              }]
           },
           options: {
              legend: {
                 display: false
              }
           }
        });

        request_div.onclick = function(e) {
           var slice = request_doughnut.getElementAtEvent(e);
           if (!slice.length) return; // return if not clicked on slice
           var label = slice[0]._model.label;
           switch (label) {
              // add case for each label/slice
              case 'Critical':
                 parent.location='request_list.jsp?predefined=my_critical';
                 break;
              case 'High':
                 parent.location='request_list.jsp?predefined=my_high';
                 break;
              case 'Medium':
                 parent.location='request_list.jsp?predefined=my_medium';
                 break;
              case 'Low':
                 parent.location='request_list.jsp?predefined=my_low';
                 break;
           }
        }
    </script>
    <script>
        var project_div = document.getElementById("project_doughnut");
        var ctxP = project_div.getContext('2d');
        var project_doughnut = new Chart(ctxP, {
           type: 'doughnut',
           data: {
              labels: ["Critical", "High", "Medium", "Low"],
              datasets: [{
                 data: <%=prj_chart_values%>,
                  backgroundColor: [
                        '#ef5350',    // color for data at index 0
                        '#ffa726',   // color for data at index 1
                        '#ffee58',  // color for data at index 2
                        '#64b5f6'  // color for data at index 3
                    ],
                 hoverBackgroundColor: ["#DF0101", "#FFBF00", "#FFFF00", "#00BFFF"]
              }]
           },
           options: {
              legend: {
                 display: false
              }
           }
        });

        project_div.onclick = function(e) {
           var slice = project_doughnut.getElementAtEvent(e);
           if (!slice.length) return; // return if not clicked on slice
           var label = slice[0]._model.label;
           switch (label) {
              // add case for each label/slice
              case 'Critical':
                 parent.location='project_list.jsp?predefined=my_critical';
                 break;
              case 'High':
                 parent.location='project_list.jsp?predefined=my_high';
                 break;
              case 'Medium':
                 parent.location='project_list.jsp?predefined=my_medium';
                 break;
              case 'Low':
                 parent.location='project_list.jsp?predefined=my_low';
                 break;
           }
        }
    </script>
    <script>
        var job_div = document.getElementById("job_doughnut");
        var ctxP = job_div.getContext('2d');
        var job_doughnut = new Chart(ctxP, {
           type: 'doughnut',
           data: {
              labels: ["Critical", "High", "Medium", "Low"],
              datasets: [{
                 data: <%=job_chart_values%>,
                  backgroundColor: [
                        '#ef5350',    // color for data at index 0
                        '#ffa726',   // color for data at index 1
                        '#ffee58',  // color for data at index 2
                        '#64b5f6'  // color for data at index 3
                    ],
                 hoverBackgroundColor: ["#DF0101", "#FFBF00", "#FFFF00", "#00BFFF"]
              }]
           },
           options: {
              legend: {
                 display: false
              }
           }
        });

        job_div.onclick = function(e) {
           var slice = job_doughnut.getElementAtEvent(e);
           if (!slice.length) return; // return if not clicked on slice
           var label = slice[0]._model.label;
           switch (label) {
              // add case for each label/slice
              case 'Critical':
                 parent.location='job_list.jsp?predefined=my_critical';
                 break;
              case 'High':
                 parent.location='job_list.jsp?predefined=my_high';
                 break;
              case 'Medium':
                 parent.location='job_list.jsp?predefined=my_medium';
                 break;
              case 'Low':
                 parent.location='job_list.jsp?predefined=my_low';
                 break;
           }
        }
    </script>
    <!-- END PAGE LEVEL JS-->
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    </body>
</html>
<%
        }
        catch(Exception e)
        {
            System.out.println("Exception on my_service_desk.jsp=" + e);
        }
    }//end if not logged in
%>
<%@page import="org.apache.commons.lang3.BooleanUtils"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : users_add_lite.jsp
    Created on : Dec 10, 2020, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        boolean page_authorized = false;
        boolean incident_authorized = support.role.authorized(session, "incident","create");      
        boolean request_authorized = support.role.authorized(session, "request","create");            
        if(!incident_authorized || !request_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            java.util.Date now = new java.util.Date();
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
            String now_date_time = date_time_picker_format.format(now);
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                String username = session.getAttribute("username").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String referer = StringUtils.defaultString(request.getParameter("referer"));
                Boolean embedded = request.getParameter("embedded") != null && BooleanUtils.toBoolean(request.getParameter("embedded"));
                String action = request.getParameter("action");
                String message_id = StringUtils.defaultString(request.getParameter("message_id"));
                String message_log = request.getParameter("message_log");
                String email = StringUtils.defaultString(request.getParameter("email"));
                if (email.equals("") && !message_id.equals("")) {
                    String message_info[] = db.get_message.message_by_id(con, message_id);
                    String from = message_info[4];
                    if(from.contains("<"))
                    {
                        String t1[] = from.split("<");
                        String t2[] = t1[1].split(">");
                        from = t2[0];
                    }
                    email = from;
                }
                System.out.println("email=" + email);
%>
<% if (!embedded) {%>

<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>	


<!-- BEGIN PAGE LEVEL CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/selects/select2.min.css">
<!-- END PAGE LEVEL CSS-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- start content here-->
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">Users</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">Add User/Customer</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div> <!--End breadcrumbs -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Add User</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
<% } %>

                            <form action="users_add_lite" name="in_form" id="in_form" onsubmit="return validateForm()">
                                <input type="hidden" name="referer" id="referer" value="<%=referer%>"/>
                                <input type="hidden" name="embedded" id="embedded" value="<%=embedded%>"/>
                                <input type="hidden" name="action" id="action" value="<%=action%>"/>
                                <input type="hidden" name="message_id" id="message_id" value="<%=message_id%>"/>
                                <input type="hidden" name="message_log" id="message_log" value="<%=message_log%>"/>
                                <input type="hidden" name="email" id="email" value="<%=email%>"/>
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="first_name">First Name</label>
                                                <input type="text" id="first" name="first" class="form-control" placeholder="Enter first name">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label for="mi">MI</label>
                                                <input type="text" id="mi" name="mi" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="last_name">Last Name</label>
                                                <input type="text" id="last" name="last" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="username">User Name</label>
                                                <input type="text" id="username" name="username" class="form-control" value="<%=email%>">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="password">Password</label>
                                                <input type="password" id="password" class="form-control" name="password" value="">
                                            </div>
                                        </div>
                                    </div><!--end row-->
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="address_1">Address <em>(1st Line)</em></label>
                                                <input type="text" id="address_1" name="address_1" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="address_2">Address <em>(2nd Line)</em></label>
                                                <input type="text" id="address_2" name="address_2" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="city">City</label>
                                                <input type="text" id="city" name="city" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="state">State</label>
                                                <input type="text" id="state" name="state" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="zip">Zip</label>
                                                <input type="text" id="zip" name="zip" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                    </div><!--end row-->
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="department">Department</label>
                                                <input type="text" id="department" name="department" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="location">Location</label>
                                                <input type="text" id="location" name="location" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="site">Site</label>
                                                <input type="text" id="site" name="site" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="company">Company</label>
                                                <input type="text" id="company" name="company" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="phone_office">Office Phone</label>
                                                <input type="text" id="phone_office" name="phone_office" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="phone_mobile">Mobile Phone</label>
                                                <input type="text" id="phone_mobile" name="phone_mobile" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="text" id="email" name="email" class="form-control" value="<%=email%>">
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div class="form-group">
                                                <label for="vip" class="center">VIP</label>
                                                <input type="checkbox" id="vip" name="vip" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="tz">Users Time Zone?</label>
                                                <select id="tz" name="tz" class="form-control">
                                                    <%
                                                    String default_tz = "US/Eastern";
                                                    String selected = "";
                                                    ArrayList<String[]> timezones = db.get_timezone.all(con);
                                                    for(int a = 0; a < timezones.size(); a++)
                                                    {
                                                        String timezone[] = timezones.get(a);
                                                        selected = "";
                                                        if(timezone[1].equalsIgnoreCase(default_tz))
                                                        {
                                                            selected = "SELECTED";
                                                        }
                                                        %>
                                                        <option <%=selected%> value="<%=timezone[1] + "," + timezone[2]%>">(<%=timezone[2] + ")   " + timezone[1]%></option>
                                                        <%
                                                    }
                                                    %>
                                                </select>
                                            </div>
                                        </div>
                                    </div><!--end row-->
                                    <div class="row">                                        
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="notes">Notes</label>
                                                <input type="text" id="notes" name="notes" class="form-control" value="This user was create by <%=username%> in response to a Message received on <%=now_date_time%>." placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12"><hr></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <%
                                            boolean assign_roles = false;
                                            String disabled = "";
                                            String message = "";
                                            if (!session.getAttribute("administration").toString().equalsIgnoreCase("true") && !session.getAttribute("manager").toString().equalsIgnoreCase("true")) 
                                            {
                                                assign_roles = false;
                                                message = "&nbsp;&nbsp;&nbsp;&nbsp;<small><em>Your assigned Role(s) do not allow you to assign Roles to Users.</em></small>";
                                                disabled = "disabled=\"disabled\"";
                                            }
                                            else
                                            {
                                                assign_roles = true;
                                            }
                                            %>
                                            <h3>Roles<%=message%></h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <%
                                        ArrayList<String[]> all_roles = db.get_roles.all(con);
                                        for(int a = 0; a < all_roles.size(); a++ )
                                        {
                                            String role[] = all_roles.get(a);
                                            if(role[0].equalsIgnoreCase("3"))
                                            {
                                                disabled = "";
                                            }
                                            else
                                            {
                                                disabled = "disabled=\"disabled\"";
                                            }
                                            %>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <input <%=disabled%> type="checkbox" name="role_id_<%=role[0]%>" id="role_id_<%=role[0]%>" value="<%=role[0]%>"/>&nbsp;<%=role[1]%>
                                                </div>
                                            </div>
                                            <%
                                        }
                                        %>
                                    </div><!--end row-->
                                    <div class="row">
                                        <div class="col-md-12"><hr></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3>Assign User to Group(s)</h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <select class="select2 form-control" id="group_ids" name="group_ids" multiple="multiple" placeholder="Select Group membership">
                                                <%
                                                //get all groups
                                                ArrayList<String[]> groups = db.get_groups.all(con);
                                                String group_id = "";
                                                String group_name = "";
                                                for(int a = 0; a < groups.size(); a++)
                                                {
                                                    String this_group[] = groups.get(a);
                                                    group_id = this_group[0];
                                                    group_name = this_group[1];
                                                    %>
                                                    <option value="<%=group_id%>"><%=group_name%></option>
                                                    <%
                                                }
                                                %>
                                            </select>
                                        </div>
                                    </div>
                                </div>
<% if (embedded) {%>

                                <div class="row ">
                                    <div class="col-md-12 text-center pt-30 pb-30">
                                        <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                                        <button type="button" class="btn btn-primary-new customBtn lg waves-effect" onclick="saveUser()">
                                            Save
                                        </button>
                                    </div>
                                </div>                                
<% }  else { %>

                                <div class="form-actions">
                                    <%
                                    if(referer.equalsIgnoreCase("message_process.jsp"))
                                    {
                                        referer = referer + "?id=" + message_id; //message_process.jsp?id=44 
                                    }
                                    %>
                                    
                                    
                                    <a href="<%=referer%>" class="btn btn-warning mr-1"><i class="ft-x"></i>&nbsp;Cancel</a>
                                    <button type="submit" class="btn btn-info">
                                        &nbsp;&nbsp;<i class="ft-save"></i> Save&nbsp;&nbsp;
                                    </button>
                                </div>
<% }%>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end content here-->
    </div>        
</div>
<% if (!embedded) {%>

<jsp:include page='footer.jsp'>
    <jsp:param name="parameter" value=""/>
</jsp:include>	

<!-- BEGIN PAGE LEVEL JS-->
<script>
    function validateForm() 
    {
        var password = document.forms["in_form"]["password"].value;
        if (document.getElementById('role_id_0').checked && password == "") 
        {
            alert("If Login Role is checked, then Password must be filled in.");
            return false;
        } 
        else 
        {
            return true;
        }
    }
</script>


<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="app-assets/vendors/js/forms/select/select2.full.min.js"></script>
<script src="app-assets/js/scripts/forms/select/form-select2.js"></script>
<!-- END PAGE LEVEL JS-->   
<% } %>

<%                con.close();
    
            } catch (Exception e) {
                System.out.println("Exception in admin_user_add.jsp: " + e);
                logger.debug("ERROR: Exception in admin_user_add.jsp:" + e);
            }
        } //end if not admin permission
    } //end if not redirected
%>
</body>
</html>


<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : incident_home
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="db.get_incidents"%>
<%@page import="db.get_chart_data"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="java.text.SimpleDateFormat"%>

<!DOCTYPE html>
<%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        if (!session.getAttribute("cx").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) {
            //if you have the permission to read/modify or Admin then you can see this
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
            //SimpleDateFormat filter_format2 = new SimpleDateFormat("yyyy"); //01/12/2019 12:00 AM
            java.util.Date filter_start = new java.util.Date();
            java.util.Date filter_end = new java.util.Date();
            String start = "";
            String end = "";
            //System.out.println("format2=" + filter_format2.parse("2016"));
            //System.out.println("format" + filter_format.parse("01/12/2019 12:00 AM"));

            ArrayList<String[]> groups= db.get_groups.all(con);
            String date_range = "";
            String group_id = "";
            String selected = "";
            
            try {
                date_range = request.getParameter("date_range");
                if (date_range.equalsIgnoreCase("null") || date_range == null) 
                {
                    date_range = support.filter_dates.past_30_days();
                }
            } catch (Exception e) {
                //if not set then default to past 30 days
                date_range = support.filter_dates.past_30_days();
            }
            try {
                group_id = request.getParameter("group_id");
                if (group_id.equalsIgnoreCase("null") || group_id == null) {
                    group_id = "all";
                }
            } catch (Exception e) {
                //if not set then default to past 30 days
                group_id = "all";
            }
            try {
                //01/12/2019 12:00 AM - 01/12/2019 11:59 PM&group_id=all
                //System.out.println("date_range=" + date_range);
                String temp[] = date_range.split("-");
                filter_start = filter_format.parse(temp[0]);
                //filter_start = filter_format.parse("01/12/2019 12:00 AM");
                
                filter_end = filter_format.parse(temp[1]);
                //System.out.println("filter_end=" + temp[1].trim() + " ====" + filter_end);

                start = timestamp.format(filter_start);
                end = timestamp.format(filter_end);

                
                
            } catch (Exception e) {
                //if not set then default to past 30 days
                System.out.println("incident_home.jsp exception=" + e);
            }

%>
<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>	
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">

<!-- END Page Level CSS-->
<!-- BEGIN Custom CSS-->
<!-- END Custom CSS-->
<jsp:include page='menu_service_desk.jsp'>
    <jsp:param name="active_menu" value=""/>
</jsp:include>	
<input type="hidden" name="start" id="start" value="<%=start%>"/>
<input type="hidden" name="end" id="end" value="<%=end%>"/>


<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title"><i class="la la-wrench"></i>&nbsp;Customer Experience</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Customer Experience
                            </li>
                        </ol>
                    </div>
                </div>                    
            </div>
        </div>
        <!-- start content here-->
        <div class="row">
            <div class="col-xl-3 col-6">
                <div class="form-group">
                    <label>Time</label>
                    <div class='input-group'>
                        <select class="select2-placeholder form-control" name="timeframe" id="timeframe">
                            <%
                                String timeframes[][] = {{"TODAY","Today"},{"THIS_WEEK","This Week"},{"LAST_WEEK","Last Week"},{"MTD","Month to Date"},{"LAST_MONTH","Last Month"},{"LAST_6_MONTHS","Past 6 Months"},{"YTD","Year to Date"},{"LAST_12_MONTHS","Past 12 Months"},{"LAST_YEAR","Last Year"}};
                                for (int a = 0; a < timeframes.length; a++) 
                                {
                                    selected = "";
                                    //if (groups[a][0].equalsIgnoreCase(group_id)) 
                                    //{
                                    //    selected = "SELECTED";
                                    //}
                                    %>
                                    <option <%=selected%> value="<%=timeframes[a][0]%>"><%=timeframes[a][1]%></option>
                                    <%
                                }
                            %>
                        </select>
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <span class="la la-group"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-4">
                <div class="form-group">
                    <label>Group</label>
                    <div class='input-group'>
                        <select class="select2-placeholder form-control" name="group_id" id="group_id">
                            <option value="all">All</option>
                            <%
                            for (int a = 0; a < groups.size(); a++) 
                            {
                                String this_group[] = groups.get(a);
                                selected = "";
                                if (this_group[0].equalsIgnoreCase(group_id)) 
                                {
                                    selected = "SELECTED";
                                }
                                %>
                                <option <%=selected%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                            <%
                                }
                            %>
                        </select>
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <span class="la la-group"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-1 col-2">
                <label>&nbsp;</label>
                <div class="form-actions">
                    <button type="button" onclick="reload_page()" class="btn btn-primary mr-1">
                        <i class="fa-filter"></i> Apply
                    </button>
                </div>
            </div>
        </div>
        <div class="row">    
            <div class="col-xl-2 col-lg-6 col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <table width="100%">
                                <tr>
                                    <td colspan="3"><h3 info onclick="javascript:location.href='cx_home.jsp?date_range=<%=date_range%>&group_id=<%=group_id%>'; return false;" style="cursor: pointer;"><b class="underlined">Net Promoter Score</b></h3></td>
                                </tr>
                                <tr>
                                    <td width="10%">
                                        <%
                                        
                                        %>
                                        <i class="ft-chevron-up success font-large-2" data-toggle="popover" data-content="The Net Promoter Score is an index ranging from -100 to 100 that measures the willingness of customers to recommend a company's products or services to others. It is used as a proxy for gauging the customer's overall satisfaction with a company's product or service and the customer's loyalty to the brand." data-trigger="hover" data-original-title="NPS"></i>
                                    </td>
                                    <td>
                                        <h5>
                                            +5   
                                        </h5>
                                    </td>
                                    <td align="right"><h2 onclick="javascript:location.href='cx_home.jsp?date_range=<%=date_range%>&group_id=<%=group_id%>'; return false;" style="cursor: pointer;">-1</h2></td>
                                </tr>
                            </table>                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-lg-6 col-12">
                <a href="cx_home.jsp">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <table width="100%">
                                    <tr>
                                        <td colspan="3"><h4 class="text-muted"><b>Customer Effort Score</b></h4></td>
                                    </tr>
                                    <tr>
                                        <td width="10%"><i class="ft-chevron-up success font-large-2" data-toggle="popover" data-content="Customer Effort Score (CES) is a type of customer satisfaction metric that measures the ease of an experience with a company by asking customers, on a five-point scale of 'Very Difficult' to 'Very Easy', how much effort was required on the part of the customer to use the product or service to evaluate how likely they are to continue using and paying for it." data-trigger="hover" data-original-title="Customer Effort Score"></i></td>
                                        <td><h5>+0.5</h5></td>
                                        <td align="right"><h2>5.6</h2></td>
                                    </tr>
                                </table>                            
                            </div>
                        </div>
                    </div>
                </a>
            </div>
           <div class="col-xl-2 col-lg-6 col-12">
                <a href="cx_home.jsp">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <table width="100%">
                                    <tr>
                                        <td colspan="3"><h4 class="text-muted"><b>Customer Satisfaction</b></h4></td>
                                    </tr>
                                    <tr>
                                        <td width="10%"><i class="la la-minus info font-large-2" data-toggle="popover" data-content="Customer Satisfaction is a measure based on the collection of returned survey responses. The 'Value' is the Average M-T-D result compared to the last months Average M-T-D result. The maximum survey value is 5." data-trigger="hover" data-original-title="Customer Satisfaction"></i></td>
                                        <td><h5>0.0</h5></td>
                                        <td align="right"><h2>4.6</h2></td>
                                    </tr>
                                </table>                             
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-2 col-lg-6 col-12">
                <a href="request_service_compliance.jsp">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <table width="100%">
                                    <tr>
                                        <td colspan="3"><h4 class="text-muted"><b>Service Compliance</b></h4></td>
                                    </tr>
                                    <tr>
                                        <td width="10%"><i class="ft-chevron-up success font-large-2" data-toggle="popover" data-content="Service Compliance is the result of meeting service agreement criteria between a service provider and the user community that define measurable targets to be achieved. The displayed 'Value' is the actual Month-To-Date % of target achieved compared to the previous Month-To-Date % achievement." data-trigger="hover" data-original-title="Service Compliance"></i></td>
                                        <td><h5>+0.5%</h5></td>
                                        <td align="right"><h2>87.5%</h2></td>
                                    </tr>
                                </table>                            
                            </div>
                        </div>
                    </div>
                </a>
            </div>
           <div class="col-xl-2 col-lg-6 col-12">
                <a href="cx_home.jsp">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <table width="100%">
                                    <tr>
                                        <td colspan="3"><h4 class="text-muted"><b>First Contact Resolution</b></h4></td>
                                    </tr>
                                    <tr>
                                        <td width="10%"><i class="la la-minus info font-large-2" data-toggle="popover" data-content="First call resolution is properly addressing the customer's need the first time they call, thereby eliminating the need for the customer to follow up with a second call." data-trigger="hover" data-original-title="First Contact Resolution(FCR)"></i></td>
                                        <td><h5>0.0%</h5></td>
                                        <td align="right"><h2>4.6%</h2></td>
                                    </tr>
                                </table>                            
                            </div>
                        </div>
                    </div>
                </a>
            </div>
           <div class="col-xl-2 col-lg-6 col-12">
               <a href="cx_home.jsp">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <table width="100%">
                                    <tr>
                                        <td colspan="3"><h4 class="text-muted"><b>Cost per Ticket</b></h4></td>
                                    </tr>
                                    <tr>
                                        <td width="10%"><i class="ft-chevron-down success font-large-2" data-toggle="popover" data-content="Cost per Ticket is the average cost of an ticket (Incident/Request). The cost is calculated by dividing the total support costs by the number of tickets." data-trigger="hover" data-original-title="Cost per Ticket"></i></td>
                                        <td><h5>$-1.30</h5></td>
                                        <td align="right"><h2>$45.65</h2></td>
                                    </tr>
                                </table>                            
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
            
        <div class="row">
            <div class="col-xl-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><b>Net Promoter Score (NPS)</b></h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body pt-0">
                            <div id="incident_queue_container" class="chart" style="min-width: 250px; height: 240px; margin: 0 auto">Imagine a fancy line chart here.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><b>Customer Effort Score (CES)</b></h4>                     
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body pt-0">
                            <div id="incident_number_container" class="chart" style="min-width: 250px; height: 240px; margin: 0 auto">Imagine a fancy line chart here.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>     
        <div class="row">
            <div class="col-xl-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><b>Customer Satisfaction Score (CSAT)</b></h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body pt-0">
                            <div id="incident_top_5_categories_container" class="chart" style="min-width: 250px; height: 240px; margin: 0 auto">Imagine a fancy line chart here.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><b>First Contact Resolution (FCR)</b></h4>                     
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body pt-0">
                            <div id="incident_closure_rate_container" class="chart" style="min-width: 250px; height: 240px; margin: 0 auto">Imagine a fancy line chart here.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
                                
        <!--
        <div class="row">
            <div class="col-12">
                <h4 class="card-title">Investigate</h4>    
                <hr>
                <a href="incident_investigate_drilldown.jsp?type=queue">Incident Queue</a>
                <br>
                <a href="incident_investigate_drilldown.jsp?type=closure">Incident Closure Rate</a>
                <br>
                <a href="incident_investigate_drilldown.jsp?type=backlog">Incident Backlog</a>
                <br>
                <a href="incident_investigate_drilldown.jsp?type=category">Incident Count By Category</a>
                <br>
            </div>
        </div>
        -->
        <!-- end content here-->
        
        
        
        
        
        
    </div> <!--end  content-wrapper -->       
</div><!--end  app-content content -->
<jsp:include page='footer.jsp'>
    <jsp:param name="parameter" value=""/>
</jsp:include>	
<!-- BEGIN VENDOR JS-->

<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
<script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
<script src="assets/js/highcharts/highcharts.js"></script>
<script src="assets/js/highcharts/data.js"></script>
<script src="assets/js/highcharts/drilldown.js"></script>
<script src="assets/js/highcharts/exporting.js"></script>
<script src="app-assets/js/scripts/popover/popover.js"></script>
<script>
    function reload_page()
    {
        var group_id = document.getElementById("group_id").value;
        var filter_date_range = document.getElementById("filter_date_range").value;
        var URL = "incident_home.jsp?date_range=" + filter_date_range + "&group_id=" + group_id;
        window.location.href = URL;
    }
</script>


<!-- END PAGE LEVEL JS-->

</body>
</html>
<%
            con.close();
        }//end if user has permission
        else {
            String RedirectURL = "no_permission.jsp";
            response.sendRedirect(RedirectURL);
        }
    }//end if user is logged in
%>

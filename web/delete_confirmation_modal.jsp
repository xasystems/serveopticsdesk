<!-- delete confirmation modal -->
<%  
try
    {
        String jsp_page = request.getServletPath();
        
        String delete_entity = "";
        String delete_entity_title = "";
        String delete_url = "";
        String delete_modal_body = "";

        switch (jsp_page)
        {
            case "/admin_users.jsp":
                delete_entity = "user";
                delete_entity_title = "User";
                delete_url = "admin_user_delete?id=";
                break;
            case "/admin_roles.jsp":
                delete_entity = "role";
                delete_entity_title = "Role";
                delete_url = "admin_role_delete?id=";
                break;
            case "/assets.jsp":
                delete_entity = "asset";
                delete_entity_title = "Asset";
                delete_url = "assets_delete?id=";

                break;
            case "/job_list.jsp":
                delete_entity = "job";
                delete_entity_title = "Job";
                delete_url = "job_delete?job_id=";

                break;
            case "/admin_survey.jsp":
                delete_entity = "survey";
                delete_entity_title = "Survey CSAT";
                delete_url = "admin_survey_delete?id=";
                break;
            default:
                delete_entity = "";
        }
                
        delete_modal_body = "Are you sure you want to delete this " + delete_entity_title + "?";
        
        if (!"".equals(delete_entity))
        {
            %>
                <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="deleteModalLabel"><%=delete_entity_title%> deletion</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <%=delete_modal_body%>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                          <button type="button" class="btn btn-danger" onclick="delete_<%=delete_entity%>()">Delete</button>
                        </div>
                      </div>
                    </div>
                </div>                     
                
                <script type="text/javascript">
                    var delete_<%=delete_entity%>_id = -1;

                    function showDeleteModal(<%=delete_entity%>_id) 
                    {
                        delete_<%=delete_entity%>_id = <%=delete_entity%>_id;
                        $('#deleteModal').modal();
                    }    
                    function delete_<%=delete_entity%>() 
                    {
                        window.location.assign("<%=delete_url%>"+delete_<%=delete_entity%>_id);
                    }    
                </script>
            <%
        }
    } catch (Exception e)
    {
        // do nothing here
    }
%>
<!-- End of delete confirmation modal -->

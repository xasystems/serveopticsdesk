<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_survey
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        if (!session.getAttribute("administration").toString().equalsIgnoreCase("true") && !session.getAttribute("manager").toString().equalsIgnoreCase("true")) 
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">

    <!-- END PAGE LEVEL CSS-->

    <div class="clr whtbg p-10 mb-15">
        <div class="float-right options">
            <ul>
                <li class="dInlineBlock dropdown ">
                    <div class="dInlineBlock clr menuHolder">
                        <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                        </a>
                        <div class="dropdown-menu dropdown-menu-right md p-10">
                            <h5 class="medium-font boldFont pt-5 mb-10">All Filters</h5>
                            <form>
                                <ul>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Name</label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width autocomplete" type="text" name="name" value="<%=(request.getParameter("name") != null?request.getParameter("name"):"")%>">
                                            </div>
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Status</label>
                                            <div class="formField md full-width mr-0">
                                                <select class="no-border full-width" name="status" >
                                                    <%=support.html_utils.generate_select_options(new String[]{"", "Draft", "Active", "Inactive", "Retired"}, request.getParameter("status"))%>
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Contact method</label>
                                            <div class="formField md full-width mr-0">
                                                <select class="no-border full-width" name="contact_method" >
                                                    <%=support.html_utils.generate_select_options(new String[]{"", "Chat", "Email", "Phone", "Self-Service", "Walk-in"}, request.getParameter("contact_method"))%>
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Ticket Type</label>
                                            <div class="formField md full-width mr-0">
                                                <select id="filter_trigger_object" class="no-border full-width" name="trigger_object" onchange="populate_filter_ticket_state()">
                                                    <%=support.html_utils.generate_select_options(new String[]{"", "Incident", "Request", "Contact", "Manual"}, request.getParameter("trigger_object"))%>
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Ticket State</label>
                                            <div class="formField md full-width mr-0">
                                                <select id="filter_trigger_state" class="no-border full-width" name="trigger_state" >
                                                    <%=support.html_utils.generate_select_options(new String[]{"", request.getParameter("ticket_state")}, request.getParameter("trigger_state"))%>
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="text-center pt-5 pb-5">
                                    <button class="btn md2 btn-primary-new customBtn mb-5">Apply Filters</button>
                                    <div class="clr"></div>
                                    <a href="javascript:void(0)" onclick="resetFilters()" class="">Reset Filters</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </li>
                <li class="dInlineBlock">
                    <span class="btn btn-success customBtn lg waves-effect rightSidePanelOpener">
                        <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>Add CSAT Survey
                    </span>    
                </li>
            </ul>
        </div>
        <h1 class="large-font boldFont">Customer Satisfaction Survey (CSAT)</h1>
    </div>

    <%
        String[] filter_fields = {
            "name",
            "status",
            "contact_method",
            "trigger_object",
            "trigger_state",
        };
        List<Map<String, String>> filters = new ArrayList<>();
        for (int i = 0; i < filter_fields.length; i++)
        {
            String filter_field = filter_fields[i];
            String filter_value = request.getParameter(filter_field);
            if (filter_value != null && !filter_value.isEmpty()) 
            {
                Map<String, String> map = new HashMap<String, String>();
                map.put("key", filter_field);
                map.put("value", filter_value);
                filters.add(map);
            }
        }

    %>    

    <%
        request.setAttribute("filter_fields", filter_fields);
    %>
    <jsp:include page="active_filters_row.jsp" />

    <%
        Connection con = db.db_util.get_contract_connection(context_dir, session);
        String user_id = session.getAttribute("user_id").toString();
        boolean update_session_table = db.get_user_sessions.update(con, user_id);
        String all_csat_surveys[][] = db.get_surveys.all_csat_by_filters(con, filters);
    %>    
    <div class="sort-tbl-holder pb-100">
        <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width=" ">Name</th>
                    <th width=" ">Description</th>
                    <th width=" ">Version</th>
                    <th width=" ">Status</th>
                    <th width=" " class="text-center">Actions</th>
                </tr>
            </thead>

            <tbody>
            <%
            for(int a = 0; a < all_csat_surveys.length;a++)
            {
            %>
                <tr>
                    <td><%=all_csat_surveys[a][2]%></td>
                    <td><%=all_csat_surveys[a][3]%></td>
                    <td><%=all_csat_surveys[a][4]%></td>
                    <td><%=all_csat_surveys[a][5]%></td>
                    <td class="text-center">
                        <a href="javascript:void(0)" onclick="editSurvey('<%=all_csat_surveys[a][0]%>');" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>
                        <a href="javascript:void(0)" onclick="showDeleteModal('<%=all_csat_surveys[a][0]%>')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>
                    </td>
                </tr>
            <%
            }
            if(all_csat_surveys.length == 0)
            {
                %>
                <tr>
                    <td colspan="5">No CSAT Surveys</td>
                </tr>
                <%
            }
            %>
            </tbody>
        </table>
    </div>
                                                            
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    
    <div class="rightPanelHolder">
    </div>
    
    <jsp:include page="delete_confirmation_modal.jsp" />
    
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <script type="text/javascript">
        var survey_id = "";

        function editSurvey(id)
        {
            survey_id = id;
            $('.rightSidePanelOpener').click();
        }

        window.addEventListener("load", (event) => 
        {
            $('.rightSidePanelCloser').on('click', function(){
                $('.rightPanelHolder').removeClass('open');
                $('.rightSidePanel').removeClass('open');
                $('html').removeClass('panelOpen');
            });

            $('.dropdown').on('hide.bs.dropdown', function (e) 
            { // prevent dropdown close on autocomplete
                if (e.clickEvent && e.clickEvent.target && e.clickEvent.target.className == "ui-menu-item-wrapper") {
                    e.preventDefault();
                }
            });

            $('.rightSidePanelOpener').on('click', function(e){
                var ajax = {};
                ajax.type = 'GET';
                ajax.dataType = "html";
                if(e.originalEvent && e.originalEvent.isTrusted)
                {
                    // real click.
                    ajax.url = "admin_survey_csat_add.jsp";            
                } else {
                    // programmatic click        
                    ajax.data = {id: survey_id};
                    ajax.url = "admin_survey_csat_edit.jsp";
                }

                ajax.error = function(jqXHR, textStatus)
                {
                    switch (jqXHR.status) {
                        case 401:
                            window.location.assign("<%=props.get("login_url").toString()%>");
                            break;
                        case 403:
                            alert("No permission!");
                            break;
                        default:
                            alert("Error processing request!");
                    }
                };
                
                ajax.success = function (data) {
                    $(".rightPanelHolder").html(data);

                    $('.rightSidePanelCloser').on('click', function(){
                        $(".rightPanelHolder").html("");
                        survey_id = "";
                    });
                };
                
                $.ajax(ajax);
            });

            $('input.autocomplete').each(function() 
            {
                var $el = $(this);
                var field = $el.prop('name');
                console.log(field);

                $el.autocomplete({
                    source: function( request, response ) 
                    {
                        console.log(request);
                        // Fetch data
                        $.ajax({
                            url: "ajax_lookup_survey_by_name",
                            type: 'post',
                            dataType: "json",
                            data: 
                            {
                                name: request.term,
                            },
                            success: function( data ) 
                            {
                                response( data );
                            }
                        });
                    },
                    minLength: 2,
                    select: function( event, ui ) 
                    {
                        console.log( ui.item );    
                        // Set selection
                        $('input[name=' + field + ']').val(ui.item.label); // display the selected text
                        //alert("setting is=" + ui.item.value);
                        return false;
                    }
                });
            });

        });
        
        function clear_filter_ticket_state()
        {
            var selectElement = document.getElementById("filter_trigger_state");
            while (selectElement.length > 0) 
            {
                selectElement.remove(0);
            }
        }

        function populate_filter_ticket_state()
        {
            //clear the original opts first
            clear_filter_ticket_state();

            var trigger_object = document.getElementById("filter_trigger_object").value;   

            if(trigger_object == "Incident")
            {
                let dropdown = document.getElementById('filter_trigger_state');
                let option;            
                option = document.createElement('option');
                option.text = "Create";
                option.value = "Create";
                dropdown.add(option);            
                option = document.createElement('option');
                option.text = "Resolved";
                option.value = "Resolved";
                dropdown.add(option);
                option = document.createElement('option');
                option.text = "Closed";
                option.value = "Closed";
                dropdown.add(option);
            }
            else
            {
                if(trigger_object == "Request")
                {
                    let dropdown = document.getElementById('filter_trigger_state');
                    let option;            
                    option = document.createElement('option');
                    option.text = "Create";
                    option.value = "Create";
                    dropdown.add(option);            
                    option = document.createElement('option');
                    option.text = "Resolved";
                    option.value = "Resolved";
                    dropdown.add(option);
                    option = document.createElement('option');
                    option.text = "Closed";
                    option.value = "Closed";
                    dropdown.add(option);
                }
                else
                {
                    if(trigger_object == "Contact")
                    {
                        let dropdown = document.getElementById('filter_trigger_state');
                        let option;            
                        option = document.createElement('option');
                        option.text = "Complete";
                        option.value = "Complete";
                        dropdown.add(option);  
                    }
                }
            }
        }
    
    </script>
    <!-- END PAGE LEVEL JS-->   
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_sla.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_sla.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

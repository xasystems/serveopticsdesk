<!--Copyright 2020 Real Data Technologies, Inc. , All rights reserved.-->
<%-- 
    Document   : job_to_task.jsp
    Created on : 18-Jan-2020, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>

<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        boolean page_authorized = support.role.authorized(session, "job","update");          
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            String job_id = "0";
            try
            {
                job_id = request.getParameter("job_id");
            }
            catch(Exception e)
            {
                job_id = "0";
            }
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
            java.util.Date now = new java.util.Date();
            Connection con = null;
            String job_info[] = new String[20];
            try
            {
                con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                job_info = db.get_jobs.job_by_id(con, job_id);
            }
            catch(Exception e)
            {
                System.out.println("job.jsp exception=" + e);
            }
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <!-- Page specific CSS-->
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <!--End Page specific CSS-->
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END Page Level CSS-->
    
    
    <div class="app-content content">
        <form action="job_to_task_complete_task.jsp" method="post">
            <input type="hidden" name="job_id" id="job_id" value="<%=job_id%>"/>
            <!--future fields-->
            <div class="content-wrapper">
                <!-- start content here-->
                <div class="content-header row">
                    <div class="content-header-left col-md-6 col-12 mb-2">
                        <h3 class="content-header-title"><i class="la la-building"></i>&nbsp;Job</h3>
                        <div class="row breadcrumbs-top">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a></li>
                                    <li class="breadcrumb-item"><a href="job_list.jsp">Job List</a></li>
                                    <li class="breadcrumb-item active">View Job</li>
                                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-info" type="submit" name="submit" value="Continue"/></li>
                                </ol>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Job Details</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>                                    
                                </div>
                            </div> 

                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Job Name</label>
                                            <input name="name" id="name" type="text" readonly value="<%=job_info[1]%>" class="form-control" />
                                        </div>
                                        <div class="col-xl-10 col-lg-6 col-12">
                                            <label><br>Description</label>
                                            <input type="text" readonly disabled  name="description" id="description" value="<%=job_info[2]%>" class="form-control"/> 
                                        </div>
                                    </div>                                    
                                    <div class="row">
                                        <div class="col-12 mt-1">
                                            <hr>
                                        </div>
                                    </div>   
                                    <div class="row">
                                        <div class="col-12">
                                            <h4>Select a Project</h4>
                                            <table id="incident_table" class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Description</th>
                                                    </tr>
                                                </thead>
                                                <tbody>                                                
                                            <%
                                            //get all projects
                                            ArrayList <String[]> projects = db.get_projects.all(con);
                                            for(int a = 0; a < projects.size(); a++)
                                            {
                                                String project[] = projects.get(a);
                                                %>
                                                <tr>
                                                    <td>
                                                        <input type="radio" id="project_id" name="project_id" value="<%=project[0]%>">&nbsp;<%=project[1]%>
                                                    </td>
                                                    <td>
                                                        <%=project[2]%>
                                                    </td>
                                                </tr>                                                
                                                <br>
                                                <%
                                            }
                                            %>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>   
                                        
                                        
                                        
                                        
                                    <div class="row">
                                        <div class="col-12 mt-1">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-info" type="submit" name="submit" value="Continue"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 mt-1">
                                            &nbsp;
                                        </div>
                                    </div>  
                                    <div class="row">
                                        <div class="col-12 mt-1">
                                            &nbsp;
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>
                <!-- end content here-->
            </div> 
        </form>
    </div>
    <br>&nbsp;
    <br>&nbsp;
    <br>&nbsp;
    <br>&nbsp;
    
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN VENDOR JS-->    
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="app-assets/js/core/app-menu.js"></script>
    <script src="app-assets/js/core/app.js"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/popover/popover.js"></script>
    
    
    
    <script>     
       $( function() 
       {
            //owner
            $( "#assigned_to_username" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#assigned_to_username').val(ui.item.label); // display the selected text
                    $('#assigned_to_id').val(ui.item.value); // save selected id to input
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            });
            //caller_group_name
            $( "#assigned_group_name" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_group",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#assigned_group_name').val(ui.item.label); // display the selected text
                    $('#assigned_group_id').val(ui.item.value); // save selected id to input
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            });
            //manager
            $( "#manager_username" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#manager_username').val(ui.item.label); // display the selected text
                    $('#manager_id').val(ui.item.value); // save selected id to input
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            });
            //manager
            $( "#tech_lead_username" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#tech_lead_username').val(ui.item.label); // display the selected text
                    $('#tech_lead_id').val(ui.item.value); // save selected id to input
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            });
        });
    </script>
    
    <script>
        $(function()
        {
          $('input[name="scheduled_start_date"]').daterangepicker({
              "autoUpdateInput": false,
              "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
          $('input[name="scheduled_start_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });
          $('input[name="scheduled_start_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });
        });
    </script>
    
    <script>
        $(function()
        {
          $('input[name="actual_start_date"]').daterangepicker({
              "autoUpdateInput": false,
              "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
          $('input[name="actual_start_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });
          $('input[name="actual_start_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });
        });
    </script>
    
    <script>
        $(function()
        {
          $('input[name="actual_end_date"]').daterangepicker({
              "autoUpdateInput": false,
              "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
          $('input[name="actual_end_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });
          $('input[name="actual_end_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });
        });
    </script>
    
    
    
    
    
    <script type="text/javascript">
        $(function()
        {
          $('input[name="scheduled_end_date"]').daterangepicker({
              "autoUpdateInput": false,
              "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
          $('input[name="scheduled_end_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });

          $('input[name="scheduled_end_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });
        });
    </script>
    
    
    
    
    
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%
        }//end if not permission
    }//end if not logged in
%>
<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="org.apache.commons.text.StringEscapeUtils"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String FINANCIAL = session.getAttribute("financial").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String CONTACT = session.getAttribute("contact").toString();
        String CX = session.getAttribute("cx").toString();
        String SURVEY = session.getAttribute("survey").toString();
        String SLA = session.getAttribute("sla").toString();        
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        
        String context_dir = request.getServletContext().getRealPath("");
        String home_page = session.getAttribute("home_page").toString();
        LinkedHashMap props = support.config.get_config(context_dir);
        SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
        SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
        java.util.Date now = new java.util.Date();
        String now_display_format = display_format.format(now);
        String now_timestamp_format = timestamp_format.format(now);
        String now_incident_time = date_time_picker_format.format(now);
        Connection con = null;
        try
        {
            con = db.db_util.get_contract_connection(context_dir, session); 
            String user_id = session.getAttribute("user_id").toString();
            String id = request.getParameter("id");
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            //get article text
            String article_info[] = db.get_kb.article_info_for_id(con, id);
            //unescape it 
            String unEscapedHTML = StringEscapeUtils.unescapeHtml4(article_info[5]);
            //get session kb feedbacks
            
            
    %>
    <jsp:include page='header_kb.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <div class="header-navbar navbar navbar-horizontal navbar-fixed navbar-dark  navbar-shadow" role="navigation" data-menu="menu-wrapper">
        <div class="navbar-container main-menu-content" data-menu="menu-container">
            <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">

            </ul>  
        </div>
    </div>	
    <form action="kb_feedback" method="post">
        <input type="hidden" name="id" id="id" value="<%=id%>"/>        
        <div class="app-content content">
            <div class="content-wrapper">
                <!-- start content here-->
                <div class="content-header row">
                    <div class="content-header-left col-12 mb-2">
                        <h3 class="content-header-title"><i class="la ft-users"></i>&nbsp;Knowledge Base Article Feedback</h3>   
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label>
                            Was this article useful?
                        </label>
                        <label>
                            &nbsp;<input type="radio" name="helpful" id="helpful" checked value="true">&nbsp;Yes
                        </label>
                        <label>
                            &nbsp;<input type="radio" name="helpful" id="helpful" value="false">&nbsp;No
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <fieldset class="form-group">
                            <textarea class="form-control" name="comment" id="comment" rows="5" placeholder="Enter your comments here."></textarea>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-info" type="submit" name="submit" value="Submit"/></div>
                </div>
                <div class="row">
                    <div class="col-12 mt-1">
                        <hr>
                        <h3><%=article_info[2]%></h3>
                        
                        <%out.println(unEscapedHTML);%>                    
                    </div>
                </div>              
                <!-- end content here-->
            </div>        
        </div>
    </form>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%
        }
        catch(Exception e)
        {
            System.out.println("incident_new.jsp exception=" + e);
        }
    }//end if not logged in
%>
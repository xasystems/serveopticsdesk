<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->

        <%
        String field_name = request.getParameter("field_name");
        String field_label = request.getParameter("field_label");
        String number_cols = request.getParameter("number_cols");
        String field_values = request.getParameter("field_values");   
        String new_record = request.getParameter("new_record"); 
        String default_text = request.getParameter("default_text"); 
        String db_value = request.getParameter("db_value"); 
        String checked = "";
        if(new_record.equalsIgnoreCase("true"))
        {
            checked = "";
        }
        else
        {
            if(db_value.equalsIgnoreCase("true"))
            {
                checked = "CHECKED";
            }
            else
            {
                checked = "";
            }
        }
        %>
        <div class="col-md-<%=number_cols%>">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    <%=field_label%>
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <div class="custom-control custom-checkbox dInlineBlock ">
                    <input type="checkbox" class="custom-control-input" <%=checked%> id="<%=field_name%>" name="<%=field_name%>" value="on">
                    <label class="custom-control-label small-font" for="<%=field_name%>"></label>
                </div>
            </div>
        </div>
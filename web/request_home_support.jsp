<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.time.ZonedDateTime"%>
<%@ page import="java.time.format.DateTimeFormatter"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        if (!session.getAttribute("request").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            String user_tz_name = "UTC";
            String user_tz_time = "+00:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }                
            }
            catch(Exception e)
            {
                user_tz_name = "UTC";
                user_tz_time = "+00:00";
            }            
            
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            java.util.Date filter_start = new java.util.Date();
            java.util.Date filter_end = new java.util.Date();
            String start = "";
            String end = "";
            String date_range = "";
            String selected = "";
            String support_group_id = "";
            String customer_group_id = "all";
            int total_request_count = 0;
            int total_request_count_previous_period = 0;

            int open_request_count = 0;
            int closed_request_count = 0;
            int open_request_count_previous_period = 0;            
            int closed_request_count_previous_period = 0;            

            double this_period_csat_survey_score[] = {0,0,0,0};
            double previous_period_csat_survey_score[] = {0,0,0,0};
            
            ArrayList<String[]> groups = db.get_groups.all(con);
            String view = request.getParameter("view");
            if (view == null || view.equalsIgnoreCase("null")) 
            {
                view = "support";
            }

            try 
            {
                date_range = request.getParameter("date_range");
                if (date_range.equalsIgnoreCase("null") || date_range == null) 
                {
                    date_range = support.filter_dates.past_30_days();
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                date_range = support.filter_dates.past_30_days();
            }
            try 
            {
                support_group_id = request.getParameter("support_group_id");
                if (support_group_id.equalsIgnoreCase("null") || support_group_id == null) 
                {
                    support_group_id = "all";
                }
            } 
            catch (Exception e) 
            {
                support_group_id = "all";
            }
            String temp[] = date_range.split("-");
            filter_start = filter_format.parse(temp[0]);
            //filter_start = filter_format.parse("01/12/2019 12:00 AM");
            filter_end = filter_format.parse(temp[1]);
            //convert filter dates to UTC dates
            ZonedDateTime zdt_filter_start = support.date_utils.user_tz_to_utc(user_tz_name, timestamp_format.format(filter_start));
            filter_start = java.util.Date.from(zdt_filter_start.toInstant());            
            ZonedDateTime zdt_filter_end = support.date_utils.user_tz_to_utc(user_tz_name, timestamp_format.format(filter_end));
            filter_end = java.util.Date.from(zdt_filter_end.toInstant());            
            total_request_count = db.get_requests.request_count_start_date_stop_date_group_id_support(con, filter_start, filter_end, support_group_id);
            open_request_count = db.get_requests.request_open_count_start_date_stop_date_group_id_support(con, filter_start, filter_end, support_group_id);
            closed_request_count = db.get_requests.request_closed_count_start_date_stop_date_group_id_support(con, filter_start, filter_end, support_group_id);
            
            //get pervious date frame
            long diff = filter_end.getTime() - filter_start.getTime(); //this is the previous timeframe            
            
            java.util.Date previous_start_date = new java.util.Date();
            previous_start_date.setTime(filter_start.getTime() - diff);
            ZonedDateTime zdt_previous_start_date = support.date_utils.user_tz_to_utc(user_tz_name, timestamp_format.format(previous_start_date));
            previous_start_date = java.util.Date.from(zdt_previous_start_date.toInstant());  
            
            java.util.Date previous_end_date = new java.util.Date();
            previous_end_date.setTime(filter_end.getTime() - diff);
            ZonedDateTime zdt_previous_end_date = support.date_utils.user_tz_to_utc(user_tz_name, timestamp_format.format(previous_end_date));
            previous_end_date = java.util.Date.from(zdt_previous_end_date.toInstant());             
            total_request_count_previous_period = db.get_requests.request_count_start_date_stop_date_group_id_support(con, previous_start_date, previous_end_date, support_group_id);
            open_request_count_previous_period = db.get_requests.request_open_count_start_date_stop_date_group_id_support(con, previous_start_date, previous_end_date, support_group_id);
            closed_request_count_previous_period = db.get_requests.request_closed_count_start_date_stop_date_group_id_support(con, previous_start_date, previous_end_date, support_group_id);

            //QUEUE/////////////////////////////////////////////////////////////////////////
            start = timestamp_format.format(filter_start);
            end = timestamp_format.format(filter_end);

            //System.out.println("group_id=" + group_id);
            String request_dashboard_queue_by_priority[][] = db.get_chart_data.request_support_dashboard_queue_by_priority_support(con, filter_start, filter_end, support_group_id);
            String request_dashboard_count_by_priority_data = "";

            for (int a = 0; a < request_dashboard_queue_by_priority.length; a++) 
            {
                if (a == 0) 
                {
                    request_dashboard_count_by_priority_data = "{name: \"" + request_dashboard_queue_by_priority[a][0] + "\","
                            + " y:" + request_dashboard_queue_by_priority[a][1] + ","
                            + "priority: \"" + request_dashboard_queue_by_priority[a][2] + "\","
                            + "color: \"" + request_dashboard_queue_by_priority[a][3] + "\"}";
                } 
                else 
                {
                    request_dashboard_count_by_priority_data = request_dashboard_count_by_priority_data + ",{name: \"" + request_dashboard_queue_by_priority[a][0] + "\","
                            + " y:" + request_dashboard_queue_by_priority[a][1] + ","
                            + "priority: \"" + request_dashboard_queue_by_priority[a][2] + "\","
                            + "color: \"" + request_dashboard_queue_by_priority[a][3] + "\"}";
                }
            }
            ///////////////////////////////////////////////////////////////////////////
            //Number chart data
            String request_dashboard_number_of_request_categories = "";
            String request_dashboard_number_of_requests_data = "";
            
            String request_dashboard_number_of_requests[][] = db.get_chart_data.request_dashboard_number_of_requests_support(con, filter_start, filter_end, support_group_id);
            //12-Dec,y_value,12/12/2018 
            //build categories string categories: ['12-Dec', '13-Dec', '14-Dec', '15-Dec']
            for(int b =0; b < request_dashboard_number_of_requests.length; b++)
            {
                if(b == 0)
                {
                    request_dashboard_number_of_request_categories = "'" + request_dashboard_number_of_requests[b][0] + "'";
                    request_dashboard_number_of_requests_data = "{y:" + request_dashboard_number_of_requests[b][1] + ", date:'" + request_dashboard_number_of_requests[b][2] + "', date_range:'" + request_dashboard_number_of_requests[b][3] + "'}";
                }
                else
                {
                    request_dashboard_number_of_request_categories = request_dashboard_number_of_request_categories + ",'" + request_dashboard_number_of_requests[b][0] + "'";
                    request_dashboard_number_of_requests_data = request_dashboard_number_of_requests_data + ",{y:" + request_dashboard_number_of_requests[b][1] + ", date:'" + request_dashboard_number_of_requests[b][2] + "', date_range:'" + request_dashboard_number_of_requests[b][3] + "'}";
                }
            }
            //////////////////////////////////////////////////////////////////////////////////
            //Top 5 groups
            String request_dashboard_top_5_categories = "";
            String request_dashboard_top_5_data = "";
            String request_dashboard_top_chart_data[][] = db.get_chart_data.request_dashboard_top_5_categories_support(con, filter_start, filter_end);
            for(int c = 0; c < request_dashboard_top_chart_data.length;c++)
            {
                if(c == 0)
                {
                    request_dashboard_top_5_categories = "'" + request_dashboard_top_chart_data[c][0] + "'";  //'1','2','3','4','5'
                    request_dashboard_top_5_data = "{category: '" + request_dashboard_top_chart_data[c][0] + "',group_id: '" + request_dashboard_top_chart_data[c][2] + "',y:" + request_dashboard_top_chart_data[c][1] + "}"; //{y: 7,category: "Cat 1"},
                }
                else
                {
                    request_dashboard_top_5_categories = request_dashboard_top_5_categories +  ",'" + request_dashboard_top_chart_data[c][0] + "'";
                    request_dashboard_top_5_data = request_dashboard_top_5_data + ",{category: '" + request_dashboard_top_chart_data[c][0] + "',group_id: '" + request_dashboard_top_chart_data[c][2] + "',y:" + request_dashboard_top_chart_data[c][1] + "}";
                }
            }
            ///////////////////////////////////////////////////////////////////////////////////
            //Closure Rate 
            String request_dashboard_closure_rate_categories = "";
            String request_dashboard_closure_rate_critical_data = "";
            String request_dashboard_closure_rate_high_data = "";
            String request_dashboard_closure_rate_medium_data = "";
            String request_dashboard_closure_rate_low_data = "";
            String request_dashboard_closure_rate_data[][] = db.get_chart_data.request_dashboard_closure_rate_support(con, filter_start, filter_end, support_group_id);
            for(int d = 0; d < request_dashboard_closure_rate_data.length;d++)
            {
                if(d ==0)
                {
                    request_dashboard_closure_rate_categories = "'" + request_dashboard_closure_rate_data[d][0] + "'";
                    request_dashboard_closure_rate_low_data = "{priority:'Low',y:" + request_dashboard_closure_rate_data[d][2].replace(",",".") + ",date:'" +  request_dashboard_closure_rate_data[d][1] + "',date_range:'" + request_dashboard_closure_rate_data[d][6] + "'}" ;
                    request_dashboard_closure_rate_medium_data = "{priority:'Medium',y:" + request_dashboard_closure_rate_data[d][3].replace(",",".") + ",date:'" +  request_dashboard_closure_rate_data[d][1] + "',date_range:'" + request_dashboard_closure_rate_data[d][6] + "'}" ;
                    request_dashboard_closure_rate_high_data = "{priority:'High',y:" + request_dashboard_closure_rate_data[d][4].replace(",",".") + ",date:'" +  request_dashboard_closure_rate_data[d][1] + "',date_range:'" + request_dashboard_closure_rate_data[d][6] + "'}" ;;
                    request_dashboard_closure_rate_critical_data = "{priority:'Critical',y:" + request_dashboard_closure_rate_data[d][5].replace(",",".") + ",date:'" +  request_dashboard_closure_rate_data[d][1] + "',date_range:'" + request_dashboard_closure_rate_data[d][6] + "'}" ;
                }
                else
                {
                    request_dashboard_closure_rate_categories = request_dashboard_closure_rate_categories + ",'" + request_dashboard_closure_rate_data[d][0]+ "'";
                    request_dashboard_closure_rate_low_data = request_dashboard_closure_rate_low_data + ",{priority:'Low',y:" + request_dashboard_closure_rate_data[d][2].replace(",",".") + ",date:'" +  request_dashboard_closure_rate_data[d][1] + "',date_range:'" + request_dashboard_closure_rate_data[d][6] + "'}" ;
                    request_dashboard_closure_rate_medium_data = request_dashboard_closure_rate_medium_data + ",{priority:'Medium',y:" + request_dashboard_closure_rate_data[d][3].replace(",",".") + ",date:'" +  request_dashboard_closure_rate_data[d][1] + "',date_range:'" + request_dashboard_closure_rate_data[d][6] + "'}" ;
                    request_dashboard_closure_rate_high_data = request_dashboard_closure_rate_high_data + ",{priority:'High',y:" + request_dashboard_closure_rate_data[d][4].replace(",",".") + ",date:'" +  request_dashboard_closure_rate_data[d][1] + "',date_range:'" + request_dashboard_closure_rate_data[d][6] + "'}" ;
                    request_dashboard_closure_rate_critical_data = request_dashboard_closure_rate_critical_data + ",{priority:'Critical',y:" + request_dashboard_closure_rate_data[d][5].replace(",",".") + ",date:'" +  request_dashboard_closure_rate_data[d][1] + "',date_range:'" + request_dashboard_closure_rate_data[d][6] + "'}" ;
                }
            }
            
            //survey trend
            String this_period_survey_results[][] = db.get_survey_results.by_trigger_object_date_range(con, "request","support", support_group_id, customer_group_id, filter_start, filter_end);
            String previous_period_survey_results[][] = db.get_survey_results.by_trigger_object_date_range(con, "request","support", support_group_id, customer_group_id, previous_start_date, previous_end_date);
            
            this_period_csat_survey_score = db.get_survey_results.csat_score_for_survey_results(this_period_survey_results);
            previous_period_csat_survey_score = db.get_survey_results.csat_score_for_survey_results(previous_period_survey_results);
            
            String color = "";
            String arrow = "";
                                            
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>
    
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <div class="clr whtbg p-10 mb-15 position-relative">
        <div class="float-right options">
            <ul>
                <li id="filters_dropdown" class="dInlineBlock dropdown ">
                    <div class="dInlineBlock clr menuHolder">
                        <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                        </a>
                        <div class="dropdown-menu dropdown-menu-right md p-10" x-placement="bottom-end" style="position: absolute; transform: translate3d(100px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                            <h5 class="medium-font boldFont pt-5 mb-10">All Filters</h5>
                            <form id="filter_form">
                                <ul>
                                    <li class="mb-10 clr">
                                        <div class="formField md  mr-0 full-width">
                                            <input class="no-border full-width datetime" type="text" name="filter_date_range" id="filter_date_range" value="<%=date_range%>">
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="formField md  mr-0 full-width">
                                            <select class="no-border" name="support_group_id" id="support_group_id">
                                                <option value="all">All</option>
                                                <%
                                                for (int a = 0; a < groups.size(); a++) 
                                                {
                                                    String this_group[] = groups.get(a);
                                                    selected = "";
                                                    if (this_group[0].equalsIgnoreCase(support_group_id)) 
                                                    {
                                                        selected = "SELECTED";
                                                    }
                                                    %>
                                                    <option <%=selected%> value="<%=this_group[0]%>"><%=this_group[1]%></option>
                                                    <%
                                                }
                                                %>
                                            </select>
                                        </div>
                                    </li>

                                </ul>
                                <div class="text-center pt-5 pb-5">
                                    <button type="button" class="btn md2 btn-primary-new customBtn mb-5" onclick="reload_page()">Apply Filters</button>
                                    <div class="clr"></div>
                                    <a href="#" class="">Reset Filters</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </li>

            </ul>
        </div>
        <h1 class="large-font boldFont">Analytics</h1>
        <div class="custom-tabs clr ">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)" data-href="incident_home_support.jsp" onclick="switchTab()" aria-selected="true">Incidents </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active show" href="javascript:void(0)" data-href="request_home_support.jsp" onclick="switchTab()" aria-selected="false">Request</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)" data-href="contact_home.jsp" onclick="switchTab()" aria-selected="false">Contacts</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" aria-selected="false">CSAT Surveys</a>
                </li>
            </ul><!-- Tab panes -->
        </div>
    </div>

    <div class=" clr mb-15 text-right ">
        <% if (!date_range.equals(""))
        {
        %>
            <div class="clr tag dInlineBlock formField md whtbg ml-5" style="width:350px">
                <input class="no-border full-width" type="text" value="<%=date_range%>" placeholder="Select Date Range">
                <img class="icon" src="assets/images/svg/cross-icon.svg" alt="" onclick="reset_date_range()">
            </div>
        <%
        }
        %>
        <% if (!support_group_id.equals("all"))
        {
        %>
            <div class="clr tag dInlineBlock formField md whtbg ml-5">
                <strong>Support Group:</strong> <%=groups.get(Integer.parseInt(support_group_id))[1]%>
                <img class="icon" src="assets/images/svg/cross-icon.svg" alt="" onclick="reset_support_group()">
            </div>
        <%
        }
        %>
        <!-- <div class="formField md full-width mr-0 float-right whtbg">
            <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015" />
        </div> -->
    </div>

    <div class="group-info-holder clr">
        <div class="holder">
            <div class=" box mb-20 clr">
                <h5 class="boldFont basic-font mb-50" onclick="javascript:location.href='request_home_support.jsp?date_range=<%=date_range%>&support_group_id=<%=support_group_id%>'; return false;" style="cursor: pointer;">Total Requests</h5>
                <div class="float-left boldFont basic-font graph-index">
                    <%
                    arrow = "";
                    if((total_request_count - total_request_count_previous_period) > 0)
                    {
                        arrow = "high";
                    }
                    else
                    {
                        arrow = "down";
                    }
                    %>
                    <span class="icon mr-5"><img src="assets/images/svg/<%=arrow%>-arrow-icon.svg" alt="" data-toggle="popover" data-content="The Change (+ or -) value is the difference from this period to the previous period." data-trigger="hover" data-original-title="Total Requests"></span>
                    <%=total_request_count - total_request_count_previous_period%>   
                </div>
                <div class="float-right xxl-font boldFont">
                    <span onclick="javascript:location.href='request_list.jsp?date_range=<%=date_range%>&group_type=support&group_id=<%=support_group_id%>'; return false;" style="cursor: pointer;"><%=total_request_count%></span>
                </div>
            </div>
        </div>
        <div class="holder">
            <div class=" box mb-20 clr">
                <h5 class="boldFont basic-font mb-50">Open Requests</h5>
                <div class="float-left boldFont basic-font graph-index">
                <%
                    arrow = "";
                    if((open_request_count - open_request_count_previous_period) > 0)
                    {
                        arrow = "high";
                    }
                    else
                    {
                        arrow = "down";
                    }
                %>
                    <span class="icon mr-5"><img src="assets/images/svg/<%=arrow%>-arrow-icon.svg" alt="" data-toggle="popover" data-content="The Change (+ or -) value is the difference from this period to the previous period." data-trigger="hover" data-original-title="Open Requests"></span>
                    <%=open_request_count - open_request_count_previous_period%> 
                </div>
                <div class="float-right xxl-font boldFont">
                    <span onclick="javascript:location.href='request_list.jsp?predefined=open&date_range=<%=date_range%>&group_type=support&group_id=<%=support_group_id%>'; return false;" style="cursor: pointer;"><%=open_request_count%></span>
                </div>
            </div>
        </div>
        <div class="holder">
            <div class=" box mb-20 clr">
                <h5 class="boldFont basic-font mb-50">Closed Requests</h5>
                <div class="float-left boldFont basic-font graph-index">
                <%
                    arrow = "";
                    if((closed_request_count - closed_request_count_previous_period) > 0)
                    {
                        arrow = "high";
                    }
                    else
                    {
                        arrow = "down";
                    }
                %>
                    <span class="icon mr-5"><img src="assets/images/svg/<%=arrow%>-arrow-icon.svg" alt="" data-toggle="popover" data-content="The Change (+ or -) value is the difference from this period to the previous period." data-trigger="hover" data-original-title="closed Requests"></span>
                    <%=closed_request_count - closed_request_count_previous_period%> 
                </div>
                <div class="float-right xxl-font boldFont">
                    <span onclick="javascript:location.href='request_list.jsp?state=closed&date_range=<%=date_range%>&group_type=support&group_id=<%=support_group_id%>'; return false;" style="cursor: pointer;"><%=closed_request_count%></span>
                </div>
            </div>
        </div>
        <div class="holder">
            <div class=" box mb-20 clr" onclick="location.href='request_service_compliance.jsp?referer=request_home_support.jsp&date_range=<%=date_range%>';" style="cursor:pointer">
                <h5 class="boldFont basic-font mb-50">Service Compliance</h5>
                <div class="float-left boldFont basic-font graph-index">
                    <%
                    double previous_period_score = 0;
                    double this_period_score = 0;

                    //get all active/reportable contact SLAs
                    String reportable_request_sla[][] = db.get_sla.all_reportable_request(con);
                    //get scores for previous period
                    for(int a = 0; a < reportable_request_sla.length;a++)
                    {
                        previous_period_score = previous_period_score + support.sla_request_calc.for_sla_id(con, reportable_request_sla[a][0], previous_start_date, previous_end_date);
                    }
                    //get the avg score for all sla's
                    previous_period_score = previous_period_score / reportable_request_sla.length;
                    //get scores for this period
                    for(int a = 0; a < reportable_request_sla.length;a++)
                    {
                        this_period_score = this_period_score + support.sla_request_calc.for_sla_id(con, reportable_request_sla[a][0], filter_start, filter_end);
                    }
                    //get the avg score for all sla's
                    this_period_score = this_period_score / reportable_request_sla.length;

                    if(this_period_score < previous_period_score)
                    {
                        arrow = "down";
                    }
                    else if(this_period_score > previous_period_score)
                    {
                        arrow = "high";
                    }
                    else if(this_period_score == previous_period_score)
                    {
                        arrow = "la la-pause";
                    }
                    double score_diff = this_period_score - previous_period_score;
                    %>
                    <span class="icon mr-5"><img src="assets/images/svg/<%=arrow%>-arrow-icon.svg" alt="" data-toggle="popover" data-content="Service Compliance is the result of meeting service agreement criteria between a service provider and the user community that define measurable targets to be achieved. The displayed 'Value' is the actual Month-To-Date % of target achieved compared to the previous Month-To-Date % achievement." data-trigger="hover" data-original-title="Service Compliance"></span>
                    <%=String.valueOf(String.format("%1$,.2f", score_diff))%>%
                </div>
                <div class="float-right xxl-font boldFont">
                    <%=String.valueOf(String.format("%1$,.2f", this_period_score))%>%
                </div>
            </div>
        </div>
        <div class="holder">
            <div class=" box mb-20 clr" onclick="javascript:location.href='request_survey.jsp?date_range=<%=date_range%>&support_group_id=<%=support_group_id%>&customer_group_id=all'; return false;" style="cursor: pointer;">
                <h5 class="boldFont basic-font mb-50">CSAT Surveys</h5>

                <div class="float-left boldFont basic-font graph-index">
                    <%
                    double survey_diff = this_period_csat_survey_score[3] - previous_period_csat_survey_score[3];
                    String total_survey_arrow = "high";
                    if(survey_diff > 0)
                    {
                        total_survey_arrow = "high";
                    }
                    else
                    {
                        if(survey_diff < 0)
                        {
                            total_survey_arrow = "down";
                        }
                        else
                        {
                            if(survey_diff == 0)
                            {
                                total_survey_arrow = "pause";
                            }
                        }
                    }
                    %>
                    <span class="icon mr-5"><img src="assets/images/svg/<%=total_survey_arrow%>-arrow-icon.svg" alt="" data-toggle="popover" data-content="Customer Satisfaction is a measure based on the collection of returned survey responses. The 'Value' is this periods average compared to the last periods average. The maximum survey value is 5." data-trigger="hover" data-original-title="Customer Satisfaction"></span>
                    <%=String.format( "%.2f", survey_diff)%>
                </div>
                <div class="float-right xxl-font boldFont">
                    <%=String.format( "%.2f", this_period_csat_survey_score[3] )%>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class=" box mb-20">
                <h4 class="boldFont basic-font mb-30">Request Queue by Priority</h4>
                <div class="graph-holder text-center">
                    <div id="request_queue_container" class="chart" style="min-width: 250px; height: 180px; margin: 0 auto"></div>
                </div>

            </div>
        </div>
        <div class="col-md-6">
            <div class=" box mb-20">
                <h4 class="boldFont basic-font mb-30">Number of Daily Requests</h4>
                <div class="graph-holder text-center">
                    <div id="request_number_container" class="chart" style="min-width: 250px; height: 180px; margin: 0 auto"></div>
                </div>

            </div>
        </div>
        <div class="col-md-6">
            <div class=" box mb-20">
                <h4 class="boldFont basic-font mb-30">Top Request Support Groups</h4>
                <div class="graph-holder text-center">
                    <div id="request_top_5_categories_container" class="chart" style="min-width: 250px; height: 180px; margin: 0 auto"></div>
                </div>

            </div>
        </div>
        <div class="col-md-6">
            <div class=" box mb-20">
                <h4 class="boldFont basic-font mb-30">Closure Rate</h4>
                <div class="graph-holder text-center">
                    <div id="request_closure_rate_container" class="chart" style="min-width: 250px; height: 180px; margin: 0 auto"></div>
                </div>

            </div>
        </div>


    </div>

    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <script>

        function switchTab() {
            window.location.href = $(event.target).data('href') + "?date_range=" + encodeURIComponent(getQueryStringParameter("date_range")).replaceAll("%20", "+");
        }

        function reload_page()
        {
            var support_group_id = document.getElementById("support_group_id").value;
            var filter_date_range = document.getElementById("filter_date_range").value;
            var URL = "request_home_support.jsp?date_range=" + filter_date_range + "&support_group_id=" + support_group_id + "&view=support";
            window.location.href = URL;
        }
        function load_customer_page()
        {
            var support_group_id = document.getElementById("support_group_id").value;
            var filter_date_range = document.getElementById("filter_date_range").value;
            var URL = "request_home_customer.jsp?date_range=" + filter_date_range + "&customer_group_id=" + support_group_id;
            window.location.href = URL;
        }
        function load_support_page()
        {
            var group_id = document.getElementById("group_id").value;
            var filter_date_range = document.getElementById("filter_date_range").value;
            var URL = "request_home_support.jsp?date_range=" + filter_date_range + "&support_group_id=" + support_group_id;
            window.location.href = URL;
        }
    </script>
    
    <script>
        $(function () {
            Highcharts.chart('request_queue_container',
                    {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: null
                        },
                        exporting: {
                            enabled: false
                        },
                        credits: {
                            enabled: false
                        },
                        legend: {
                            enabled: false
                        },
                        xAxis: {
                            type: 'category',
                            minorTickLength: 0,
                            tickLength: 0
                        },
                        yAxis: {
                            title: {
                                x: -10,
                                text: 'Request Count'
                            }
                        },
                        plotOptions: {
                            column: {
                                dataLabels: {
                                    enabled: true
                                }
                            },
                            series: {
                                cursor: 'pointer',
                                point: {
                                    events: {
                                        click: function (event) {
                                            window.location = "request_list.jsp?priority=" + event.point.priority + "&date_range=" + document.getElementById("filter_date_range").value + "&assigned_group_id=" + document.getElementById("support_group_id").value;
                                        }
                                    }
                                }
                            }
                        },
                        series: [
                            {
                                name: "Request Count",
                                color: '#64b5f6',
                                data: [
                                    <%=request_dashboard_count_by_priority_data%>
                                ]
                            }
                        ]
                    });
        });
    </script>
    <script>
        $(function () {
            Highcharts.chart('request_number_container', {
            chart: {
                type: 'spline'
            },
            title: {
                text: null
            },
            exporting: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            xAxis: {
                categories: [<%=request_dashboard_number_of_request_categories%>]
            },
            yAxis: {
                title: {
                    x: -10,
                    text: 'Number'
                }
            },

            plotOptions: {
                spline: {
                    marker: {
                        enabled: false
                    }
                },
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (event) {
                                window.location = "request_list.jsp?date_range=" + event.point.date_range + "&assigned_group_id=" + document.getElementById("support_group_id").value;
                            }
                        }
                    }
                }
            },
            series: [{
                    name: "Number of Daily Requests",
                    color: '#64b5f6',
                    data: [
                        <%=request_dashboard_number_of_requests_data%>
                    ]
                }]
        });
    });
    </script>
    <script>
        $(function () {
        Highcharts.chart('request_top_5_categories_container', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: null
                },
                exporting: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                xAxis: {
                    categories: [<%=request_dashboard_top_5_categories%>],
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0
                },
                yAxis: {
                    visible: false
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        },
                        borderWidth: 11,
                        cursor: 'pointer',
                        point: {
                            events:{
                                click: function(event) {
                                    window.location = "request_list.jsp?category=" + event.point.category + "&date_range=" + document.getElementById("filter_date_range").value + "&assigned_group_id=" + document.getElementById("support_group_id").value;
                                }
                            }
                        }
                    }
                },
                series: [{
                    name: 'Count',
                    color: '#64b5f6',
                    data: [<%=request_dashboard_top_5_data%>]
                }]
            });
        });
    </script>   
    <script>
        $(function () {
            Highcharts.chart('request_closure_rate_container', {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: null
                },
                exporting: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: [<%=request_dashboard_closure_rate_categories%>]
                },
                yAxis: {
                    title: {
                        x: -10,
                        text: 'Closure Rate'
                    }
                },
                plotOptions: {
                    spline: {
                        marker: {
                            enabled: false
                        }
                    },
                    series: {
                        events: {
                            click: function(event) {
                                window.location = "request_list.jsp?state=closed&priority=" + event.point.priority + "&date_range=" + event.point.date_range + "&assigned_group_id=" + document.getElementById("support_group_id").value;
                            }
                        }
                    }
                },
                series: [
                    {
                        name: "Critical",
                        color: "#ef5350",
                        data:[<%=request_dashboard_closure_rate_critical_data%>]
                    },
                    {
                        name: "High",
                        color: "#ffa726",
                        data:[<%=request_dashboard_closure_rate_high_data%>]
                    },
                    {
                        name: "Medium",
                        color: "#ffee58",
                        data:[<%=request_dashboard_closure_rate_medium_data%>]
                    },
                    {
                        name: "Low",
                        color: '#64b5f6',
                        data:[<%=request_dashboard_closure_rate_low_data%>]
                    }
                ]
            });
        });
    </script>
    
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
    <script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
    <script src="assets/js/highcharts/highcharts.js"></script>
    <script src="assets/js/highcharts/data.js"></script>
    <script src="assets/js/highcharts/drilldown.js"></script>
    <script src="assets/js/highcharts/exporting.js"></script>
    <script src="app-assets/js/scripts/popover/popover.js"></script>
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%
        }//end if not permission
    }//end if not logged in
%>
<!--Copyright 2020 Real Data Technologies, Inc. , All rights reserved.-->
<%-- 
    Document   : incident_new.jsp
    Created on : 18-Jan-2020, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>

    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String FINANCIAL = session.getAttribute("financial").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String CONTACT = session.getAttribute("contact").toString();
        String CX = session.getAttribute("cx").toString();
        String SURVEY = session.getAttribute("survey").toString();
        String SLA = session.getAttribute("sla").toString();        
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        
        
        
        if (ADMINISTRATION.equalsIgnoreCase("true") || MANAGER.equalsIgnoreCase("true")) 
        {
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
            java.util.Date now = new java.util.Date();
            String now_display_format = display_format.format(now);
            String now_timestamp_format = timestamp_format.format(now);
            String now_incident_time = date_time_picker_format.format(now);
            Connection con = null;
            try
            {
                con = db.db_util.get_contract_connection(context_dir, session); 
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
            }
            catch(Exception e)
            {
                System.out.println("admin_service_catalog_add.jsp exception=" + e);
            }
    %>
    <!-- BEGIN Page Level CSS-->
    <!-- END Page Level CSS-->
    
    
    <form action="admin_service_catalog_add" method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Name
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input name="name" id="name" type="text" value="" class="form-control" placeholder="Catalog Name"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Description
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input name="description" id="description" type="text" value="" class="form-control" placeholder="Catalog Description"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        State
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="state" id="status" class="form-control">
                        <%
                        String[] state_names = {"Draft","Awaiting Approval","Published","Superceded","Expired","Invalid","Deleted"};
                        String[] state_values = {"Draft","Waiting_Approval","Published","Superceded","Expired","Invalid","Deleted"};
                        for(int a = 0; a < state_values.length; a++)
                        {
                            %>
                            <option value="<%=state_values[a]%>"><%=state_names[a]%></option>
                            <%
                        }                                                
                        %>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Type
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select name="catalog_type" id="catalog_type" class="form-control">
                        <%
                        String[] catalog_names = {"Self Service","Service Desk"};
                        String[] catalog_values = {"self_service","service_desk"};
                        for(int a = 0; a < catalog_names.length; a++)
                        {
                            %>
                            <option value="<%=catalog_values[a]%>"><%=catalog_names[a]%></option>
                            <%
                        }                                                
                        %>
                    </select>
                </div>
            </div>
        </div> 
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md">
                        Choose an Image for this Catalog
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="clipper_result" name="file" onchange="window.clipper_app.$children[0].upload(event)">
                        <label class="custom-file-label" for="file">Choose file(s)</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row d-none" id="clipper_preview_row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only">
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <div class="clr dInlineBlock formField md whtbg mr-5 mb-15 float-none w-auto">
                        <span>
                            <img width="100px" id="clipper_preview" src="" />
                            <a href="javascript:void(0)" onclick="$(this).closest('div.row').addClass('d-none');$('#clipper_result').val('');event.preventDefault" >
                                <img class="icon pl-2" src="assets/images/svg/cross-icon.svg" alt="">
                            </a>
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md mb-15">
                        Choose an Icon for this Catalog
                    </label>
                </div>
                <div class="formField clr md mb-15"> <!-- style="overflow-y:scroll; height:400px;" -->
                    <%
                    String checked = "";
                    int row = 1;
                    ArrayList <String> icons = db.get_icons.all_icons(con);
                    for(int z = 0; z < icons.size(); z++)
                    {
                        if(z ==  0)
                        {
                            checked = "CHECKED";
                            out.println("<div class='row'>");
                        }
                        else
                        {
                            checked = "";
                            if ( z % 4 == 0)
                            {
                                out.println("<div class='row'>");
                            }

                        }
                        
                        %>
                        <div class="col-md-3">
                            <div class="form-check form-check-inline full-width">
                              <input <%=checked%> class="form-check-input w-auto" type="radio" name="icon" id="inlineRadio<%=z%>" value="<%=icons.get(z)%>">
                              <label class="form-check-label full-width" for="inlineRadio<%=z%>"><i class="la <%=icons.get(z)%>"></i>&nbsp;<%=icons.get(z)%></label>
                            </div>                            
                        </div>
                    <%
                        if ( z > 0 && (z == (icons.size()-1) || (z+1) % 4 == 0))
                        {
                            out.println("</div>");
                        }
                    }
                    %>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-12 text-center pt-30 pb-30">
                <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                    Save
                </button>
            </div>
        </div>
    </form>

    <!-- BEGIN PAGE LEVEL JS-->

    <!-- END PAGE LEVEL JS-->
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>
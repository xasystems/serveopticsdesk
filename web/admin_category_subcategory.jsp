<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String FINANCIAL = session.getAttribute("financial").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String CONTACT = session.getAttribute("contact").toString();
        String CX = session.getAttribute("cx").toString();
        String SURVEY = session.getAttribute("survey").toString();
        String SLA = session.getAttribute("sla").toString();        
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        String METRIC = session.getAttribute("metric").toString();
        
        
        if(!ADMINISTRATION.equalsIgnoreCase("true"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            String home_page = session.getAttribute("home_page").toString();
            Logger logger = LogManager.getLogger();        
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);   
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <!-- END PAGE LEVEL CSS-->
    <div class="clr whtbg p-10 mb-15">
        <div class="float-right options">
            <ul>
                <li class="dInlineBlock dropdown ">
                    <div class="dInlineBlock clr menuHolder">
                        <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                        </a>
                        <div class="dropdown-menu dropdown-menu-right md p-10">
                            <h5 class="medium-font boldFont pt-5 mb-10">All Filters</h5>
                            <form>
                                <ul>
                                    <li class="mb-10 clr">
                                        <div class="formField md  mr-0 full-width">
                                            <select class="no-border">
                                                <option>Select Filters by Dates</option>
                                                <option>Select Filters by Dates</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Incident Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>
                                        <!-- <div class="row">
                                            <div class="col-md-12">
                                                <label class="field-label full-width">Incident Date (Start)</label>
                                                <div class="formField md full-width mr-0">

                                                    <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="field-label full-width">Incident Date (End)</label>
                                                <div class="formField md full-width mr-0">
                                                    <input class="no-border full-width" type="date" value="">
                                                </div>
                                            </div>
                                        </div> -->
                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Create Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>

                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Pending Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>

                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">State Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>

                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Close Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>

                                    </li>
                                </ul>
                                <div class="text-center pt-5 pb-5">
                                    <button class="btn md2 btn-primary-new customBtn mb-5">Apply Filters</button>
                                    <div class="clr"></div>
                                    <a href="#" class="">Reset Filters</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </li>
                <li class="dInlineBlock">
                    <span class="btn btn-success customBtn lg waves-effect rightSidePanelOpener">
                        <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>Add Category
                    </span>    
                </li>

            </ul>
        </div>
        <h1 class="large-font boldFont">Category/Subcategory</h1>
    </div>    

    <div class="sort-tbl-holder">
        <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Category</th>
                    <th>Subcategory(s)</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                <%
                ArrayList<String[]> categories = db.get_category.all(con);
                for(int a = 0; a < categories.size(); a++)
                {
                    String[] category = categories.get(a);
                    %>
                    <tr>
                        <td>
                            <%=category[1]%>
                        </td>
                        <td>
                            <%
                            ArrayList<String[]> subcategories = db.get_subcategory.all_for_category_id(con, category[0]);
                            for(int b = 0; b < subcategories.size(); b++)
                            {
                                String subcategory[] = subcategories.get(b);
                                %>
                                <%=subcategory[2] + (b<subcategories.size()-1?" | ":"")%>
                                <%
                            }
                            %>
                        </td>
                        <td>
                            <a href="javascript:void(0)" onclick="editCategory(<%=category[0]%>)" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>
                            <a href="javascript:void(0)" onclick="showDeleteModal(<%=category[0]%>)" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>
                        </td>
                    </tr>
                    <%
                }
                %>                                                  
            </tbody>
        </table>
    </div>    
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>

    <div class="rightPanelHolder">
        <div class="p-15 ticket-content">
            <span class="rightSidePanelCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
            <h4 id="right_panel_title" class="mediumFont large-font mb-20">Add Category</h4>

            <div class="clr whtbg p-15 ">
                <form id="add_form" action="admin_category_subcategory_process_category_with_subs" class="add-project-form needs-validation" novalidate>  
                    <input type="hidden" name="category_id" value=""/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Active
                                </label>
                            </div>
                            <div class="formField clr md mb-15 no-border">
                                <div class="custom-control custom-checkbox dInlineBlock mr-15">
                                    <input name="category_active" type="checkbox" class="custom-control-input" id="listingCheck1">
                                    <label class="custom-control-label small-font" for="listingCheck1"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Category Name
                                </label>
                            </div>
                            <div class="formField clr md mb-15">
                                <input name="category_name" type="text" value="" class="no-border" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row mb-15">
                        <div class="col-md-12">
                            <span onclick="add_row()" class="btn btn-primary-new customBtn lg waves-effect">
                                <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>Add Subcategory
                            </span>
                        </div>
                    </div>
                    
                    <div class="row d-none" id="option_table_div">        
                        <div class="col-md-12" id="additional_parameter_list" >
                            <table class="table" id="option_table" border="0" cellspacing="2" cellpadding="10">
                                <tr>
                                    <td class="text-center"><b>Active</b></td>
                                    <td><b>Name</b></td>
                                    <td><b></b></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row ">
                        <div class="col-md-12 text-center pt-30 pb-30">
                            <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                            <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                                Submit
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>            

    // delete confirmation modal
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="deleteModalLabel">Role deletion</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            Are you sure you want to delete this category with all subcategories?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-danger" onclick="deleteCategory()">Delete</button>
          </div>
        </div>
      </div>
    </div>         

            
    <!-- BEGIN PAGE LEVEL JS-->
    <script type="text/javascript">
    
    var delete_category_id = -1;
    
    function editCategory(category_id) 
    {
        $("form#add_form").get(0).classList.remove('was-validated');
        document.getElementById('add_form').reset();
        document.getElementsByName("category_id")[0].value = "";
        $("#option_table > tbody > tr").slice(1).remove();

        $.ajax({
            url: "ajax_lookup_category",
            type: 'post',
            dataType: "json",
            data: 
            {
                category_id: category_id
            },
            error: function(jqXHR, textStatus) 
            {
                switch (jqXHR.status) {
                    case 401:
                        window.location.assign("<%=props.get("login_url").toString()%>");
                        break;
                    case 403:
                        alert("No permission!");
                        break;
                    default:
                        alert("Error processing request!");
                }
            },
            success: function( data ) 
            {
                console.log(data);
                if (Object.keys(data).length > 0) {
                    $('.rightPanelHolder').addClass('open');
                    $('.rightSidePanel').addClass('open');
                    $('html').addClass('panelOpen');
                    $('#right_panel_title').html('Edit category');

                    document.getElementsByName("category_id")[0].value=data[0];
                    document.querySelectorAll('input[name="category_name"]')[0].value = data[1];
                    document.getElementsByName("category_active")[0].checked = (data[2] === "true");

                    $.ajax({
                        url: "ajax_lookup_subcategory_data_by_category",
                        type: 'post',
                        dataType: "json",
                        data: 
                        {
                            category_id: category_id
                        },
                        error: function(jqXHR, textStatus) 
                        {
                            switch (jqXHR.status) {
                                case 401:
                                    window.location.assign("<%=props.get("login_url").toString()%>");
                                    break;
                                case 403:
                                    alert("You don't have rights enough!");
                                    break;
                                default:
                                    alert("Error processing request!");
                            }
                        },
                        success: function( data ) 
                        {
                            console.log(data);
                            if (Object.keys(data).length > 0) {
                                for (key in data['items'])
                                {
                                    console.log(data['items'][key]);
                                    add_row(data['items'][key]);
                                }
//                                document.getElementsByName("category_id")[0].value=data[0];
//                                document.querySelectorAll('input[name="category_name"]')[0].value = data[1];
//                                document.getElementsByName("category_active")[0].checked = (data[2] === "true");
                            } else {
                                alert("Error processing request!");
                            }
                        }
                    });  
                } else {
                    alert("Error processing request!");
                }
            }
        });            
    }
    
    function showDeleteModal(category_id) 
    {
        delete_category_id = category_id;
        $('#deleteModal').modal();
    }    

    function deleteCategory() 
    {
        window.location.assign("admin_category_subcategory_delete?category_id="+delete_category_id);
    }    

        
    window.addEventListener("load", (event) => 
    {
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        
        var validation = Array.prototype.filter.call(forms, function(form) 
        {
            
            form.addEventListener('submit', function(event) 
            {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                    $('.rightPanelHolder').animate({scrollTop: $('.form-control:invalid').first().offset().top - 50}, 'fast');
                }
                form.classList.add('was-validated');
                var checkboxes = document.getElementsByName("subcategory_active[]");
                for (var i=0; i<checkboxes.length; i++) {
                    // And stick the checked ones onto an array...
                    if (!checkboxes[i].checked) {
                        checkboxes[i].checked = "checked";
                        checkboxes[i].value = "false";
                    }
                }
                var category_id = document.getElementsByName("category_id")[0].value;
                if (category_id !== "") 
                {
                    form.action = "admin_category_subcategory_process_category_with_subs";
                    form.method = "POST";
                } else {
                    form.action = "admin_category_subcategory_process_category_with_subs";
                    form.method = "POST";
                }
            }, false);
        });            
        
        $('.rightSidePanelOpener').on('click', function(){
            $('#right_panel_title').html('Add category');
            $("form#add_form").get(0).classList.remove('was-validated');
            document.getElementsByName("category_id")[0].value = "";
            document.getElementById('add_form').reset();
            $("#option_table > tbody > tr").slice(1).remove();
            document.getElementById("option_table_div").classList.add("d-none");
        });

    });
    
        function delete_row(cell) 
        {
            $(cell).closest('tr').remove();
        }
    
        function add_row(data) 
        {
            document.getElementById("option_table_div").classList.remove("d-none");
            var object_table = document.getElementById("option_table");
            var row_number = object_table.rows.length;            
            /*** We insert the row by specifying the current rows length ***/
            var object_row = object_table.insertRow(object_table.rows.length);

            /*** We insert the first row cell ***/
            var checkbox_cell = object_row.insertCell(0);

            var subcategory_active = "";
            if (data !== undefined && data.hasOwnProperty("active") && data.active == "true") 
            {
                subcategory_active = "checked";
            }    
            var subcategory_id = "";
            if (data !== undefined && data.hasOwnProperty("id")) 
            {
                subcategory_id = data.id;
            }    
            checkbox_cell.classList.add("text-center");
            checkbox_cell.innerHTML = "<div class='custom-control custom-checkbox dInlineBlock'>" +
                    "<input type='hidden' name='subcategory_id[]' value='" + subcategory_id + "'>" +
                    "<input name='subcategory_active[]' id='subcatrow" + row_number + "' class='custom-control-input' type='checkbox' value='true' " + subcategory_active + ">" +
                    "<label class='custom-control-label small-font' for='subcatrow" + row_number + "'></label>" +
                    "</div";

            /*** field ***/
            var field_value_cell = object_row.insertCell(1);
            /*** We  add some text inside the celll ***/
            var field_value = "";
            if (data !== undefined && data.hasOwnProperty("name")) 
            {
                field_value = data.name;
            }    
            field_value_cell.innerHTML = "<input class='form-control' type='text' name='subcategory_name[]' value='" + field_value + "'/>";

            var delete_icon_cell = object_row.insertCell(2);
            delete_icon_cell.innerHTML = "<a href='javascript:void(0)' onclick='delete_row(this)' class='mr-5'><img src='assets/images/svg/trash-icon-blk.svg' alt=''></a>";
            
        }

    </script>

    <!-- END PAGE LEVEL JS-->  
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_systen_select_fields.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_systen_select_fields.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

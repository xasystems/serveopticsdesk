<%@page import="java.nio.charset.StandardCharsets"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.time.ZonedDateTime"%>
<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.sql.*" %>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        if (!session.getAttribute("incident").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("true")) 
        {
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            session.setAttribute("home_page", "home_analytics.jsp");
            GregorianCalendar first_of_month = new GregorianCalendar();
            first_of_month.set(Calendar.DAY_OF_MONTH, 1);
            first_of_month.set(Calendar.HOUR_OF_DAY, 0);
            first_of_month.set(Calendar.MINUTE, 0);
            first_of_month.set(Calendar.SECOND, 0);     
            GregorianCalendar now = new GregorianCalendar();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap sys_props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session); 
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            String date_range = StringUtils.defaultString(request.getParameter("date_range"));
            String user_tz_name = "UTC";
            String user_tz_time = "+00:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }                
            }
            catch(Exception e)
            {
                user_tz_name = "UTC";
                user_tz_time = "+00:00";
            }  

            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            //SimpleDateFormat filter_format2 = new SimpleDateFormat("yyyy"); //01/12/2019 12:00 AM
            java.util.Date filter_start = new java.util.Date();
            java.util.Date filter_end = new java.util.Date();
            String start = "";
            String end = "";
            //01/12/2019 12:00 AM - 01/12/2019 11:59 PM&group_id=all
            //System.out.println("date_range=" + date_range);
            String temp[] = date_range.split("-");
            try 
            {

                filter_start = filter_format.parse(temp[0]);  //filter_start = filter_format.parse("01/12/2019 12:00 AM");
                ZonedDateTime zdt_filter_start = support.date_utils.user_tz_to_utc(user_tz_name, timestamp_format.format(filter_start));
                filter_start = java.util.Date.from(zdt_filter_start.toInstant());  

                filter_end = filter_format.parse(temp[1]);  //System.out.println("filter_end=" + temp[1].trim() + " ====" + filter_end);
                ZonedDateTime zdt_filter_end = support.date_utils.user_tz_to_utc(user_tz_name, timestamp_format.format(filter_end));
                filter_end = java.util.Date.from(zdt_filter_end.toInstant());  

                start = timestamp_format.format(filter_start);
                end = timestamp_format.format(filter_end);
            } 
            catch (Exception e) 
            {
                filter_start = first_of_month.getTime();
                filter_end = now.getTime();
                date_range = filter_format.format(filter_start) + " - " + filter_format.format(filter_end);
            }


    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
<link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    
    <div class="clr whtbg p-10 mb-15">
        <h1 class="large-font boldFont">Service Desk Analytics</h1>
    </div>

    <div class="clr whtbg p-10 mb-15">
        <div class="col-md-2" style="max-width:350px">
                <label class="field-label full-width">
                    Date Range
                </label>
                <div class="formField md full-width mr-0">
                    <input type="text" id="date_range" name="date_range" value="<%=date_range%>" placeholder="" class="border-0 full-width datetime">
                </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class=" box mb-20 clr" style="min-height: 240px; cursor: pointer" onclick="javascript:location.href='incident_home_support.jsp?date_range=<%=URLEncoder.encode(date_range, StandardCharsets.UTF_8.toString())%>'">
                <h5 class="boldFont medium-font mb-30"><span class="icon mr-10"><img src="assets/images/svg/wrench-icon.svg" alt=""></span>Incidents</h5>
                <ul>
                    <li class="clr mb-15">
                        <div class="row">
                            <div class="col-md-6 text-right">
                                Incidents created
                            </div>
                            <div class="col-md-6 mediumFont medium-font">
                                <%
                                int incidents_created = db.get_incidents.incident_count_start_date_stop_date_group_id(con, filter_start, filter_end, "all");
                                %>
                                <%=incidents_created%>
                            </div>
                        </div>
                    </li>
                    <li class="clr mb-15">
                        <div class="row">
                            <div class="col-md-6 text-right">
                                Incidents Closed
                            </div>
                            <div class="col-md-6 mediumFont medium-font">
                                <%
                                int incidents_closed = db.get_incidents.incident_closed_count_start_date_stop_date_group_id(con, filter_start, filter_end, "all");
                                %>
                                <%=incidents_closed%>
                            </div>
                        </div>
                    </li>
                    <li class="clr mb-15">
                        <div class="row">
                            <div class="col-md-6 text-right">
                                Open Incidents
                            </div>
                            <div class="col-md-6 mediumFont medium-font">
                                <%
                                int incidents_open = db.get_incidents.incident_open_count(con);
                                %>                                                
                                <%=incidents_open%>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-3">
            <div class=" box mb-20 clr" style="min-height:240px; cursor: pointer;" onclick="javascript:location.href='request_home_support.jsp?date_range=<%=URLEncoder.encode(date_range, StandardCharsets.UTF_8.toString())%>'; return false;">
                <h5 class="boldFont medium-font mb-30"><span class="icon mr-10"><img src="assets/images/svg/cart-icon.svg" alt=""></span>Requests</h5>
                <ul>
                    <li class="clr mb-15">
                        <div class="row">
                            <div class="col-md-6 text-right">
                                Requests created
                            </div>
                            <div class="col-md-6 mediumFont medium-font">
                                <%
                                int requests_created = db.get_requests.request_created_count_start_date_stop_date(con, filter_start, filter_end);     
                                %>
                                <%=requests_created%>
                            </div>
                        </div>
                    </li>
                    <li class="clr mb-15">
                        <div class="row">
                            <div class="col-md-6 text-right">
                                Requests Closed
                            </div>
                            <div class="col-md-6 mediumFont medium-font">
                                <%
                                int requests_closed = db.get_requests.request_closed_count_start_date_stop_date(con, filter_start, filter_end);     
                                %>
                                <%=requests_closed%>
                            </div>
                        </div>
                    </li>
                    <li class="clr mb-15">
                        <div class="row">
                            <div class="col-md-6 text-right">
                                Open Requests
                            </div>
                            <div class="col-md-6 mediumFont medium-font">
                                <%
                                int requests_open = db.get_requests.request_open_count(con);     
                                %>
                                <%=requests_open%>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-3">
            <div class=" box mb-20 clr" style="min-height:240px; cursor: pointer;" onclick="javascript:location.href='contact_home.jsp?date_range=<%=URLEncoder.encode(date_range, StandardCharsets.UTF_8.toString())%>'; return false;">
                <h5 class="boldFont medium-font mb-30"><span class="icon mr-10"><img src="assets/images/svg/call-icon.svg" alt=""></span>Contacts</h5>
                <ul>
                    <li class="clr mb-15">
                        <div class="row">
                            <div class="col-md-6 text-right">
                                Calls Received
                            </div>
                            <div class="col-md-6 mediumFont medium-font">
                                <%
                                int phone = db.get_contact.contact_count_for_media_type_timeframe(con, "Phone", filter_start, filter_end);     
                                %>
                                <%=phone%>
                            </div>
                        </div>
                    </li>
                    <li class="clr mb-15">
                        <div class="row">
                            <div class="col-md-6 text-right">
                                Emails Received
                            </div>
                            <div class="col-md-6 mediumFont medium-font">
                                <%
                                int messages = db.get_message.message_count_for_timeframe(con, filter_start, filter_end);
                                %>
                                <%=messages%>
                            </div>
                        </div>
                    </li>
                    <li class="clr mb-15">
                        <div class="row">
                            <div class="col-md-6 text-right">
                                Chats Received
                            </div>
                            <div class="col-md-6 mediumFont medium-font">
                                <%
                                int ss_messages = db.get_contact.self_service_contacts_for_timeframe(con, filter_start, filter_end);
                                %>
                                <%=ss_messages%>
                            </div>
                        </div>
                    </li>
                    <li class="clr mb-15">
                        <div class="row">
                            <div class="col-md-6 text-right">
                                Voice Mail
                            </div>
                            <div class="col-md-6 mediumFont medium-font">
                                <%
                                int voice_mails = db.get_contact.contact_count_for_media_type_timeframe(con, "Voice Mail", filter_start, filter_end);     
                                %>
                                <%=voice_mails%>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-3">
            <div class=" box mb-20 clr" style="min-height: 240px;">
                <h5 class="boldFont medium-font mb-30"><span class="icon mr-10"><img src="assets/images/svg/dollar-icon.svg" alt=""></span>CSAT Surveys</h5>
                <ul>
                    <li class="clr mb-15">
                        <div class="row">
                            <div class="col-md-6 text-right">
                                Surveys Sent
                            </div>
                            <div class="col-md-6 mediumFont medium-font">
                                <%
                                int surveys_sent = db.get_survey_results.csat_by_trigger_object_timeframe(con, "all", filter_start, filter_end);     
                                %>
                                <%=surveys_sent%>
                            </div>
                        </div>
                    </li>
                    <li class="clr mb-15">
                        <div class="row">
                            <div class="col-md-6 text-right">
                                Surveys Completed
                            </div>
                            <div class="col-md-6 mediumFont medium-font">
                                <%
                                int surveys_completed = db.get_survey_results.completed_csat_by_trigger_object_timeframe(con, "all", filter_start, filter_end);     
                                %>
                                <%=surveys_completed%>
                            </div>
                        </div>
                    </li>
                    <li class="clr mb-15">
                        <div class="row">
                            <div class="col-md-6 text-right">
                                Average Score
                            </div>
                            <div class="col-md-6 mediumFont medium-font">
                                <%
                                //get all survey results
                                String survey_results[][] = db.get_survey_results.for_all_csat_results_for_date_range(con, filter_start, filter_end);
                                double score[] = db.get_survey_results.csat_score_for_survey_results(survey_results);
                                DecimalFormat df2 = new DecimalFormat("#.##");
                                %>
                                <%=df2.format(score[3])%>
                            </div>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
<script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
<script>
    $('input[name="date_range"]').on('apply.daterangepicker', function(ev, picker)
    {
        setQueryStringParameter("date_range", $(this).val());
        window.location.reload();
//        $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
    });


</script>

    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>
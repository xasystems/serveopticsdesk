<%-- 
    Document   : project.jsp
    Created on : 18-Jan-2020, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Calendar"%> 
<%@ page import="java.util.GregorianCalendar"%>
<%@ page import="java.time.ZonedDateTime"%>
<%@ page import="java.time.format.DateTimeFormatter"%>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        if (PROJECT.contains("update") || ADMINISTRATION.equalsIgnoreCase("true") || MANAGER.equalsIgnoreCase("true")) 
        {
            String user_tz_name = "UTC";
            String user_tz_time = "+00:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "UTC";
                user_tz_time = "+00:00";
            }
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
            java.util.Date now = new java.util.Date();
            String now_display_format = display_format.format(now);
            String now_timestamp_format = timestamp_format.format(now);
            String now_project_time = date_time_picker_format.format(now);
            Connection con = null;
            String selected = "";
            try
            {
                con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
            }
            catch(Exception e)
            {
                System.out.println("project.jsp exception=" + e);
            }
            //get project info
            boolean project_not_found = false;
            String project_info[] = {"","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};
            String project_id = "0";
            try
            {
                project_id = request.getParameter("project_id");
                project_info = db.get_projects.project_by_id(con, project_id);
                if(project_info[0].equalsIgnoreCase("--"))
                {
                    project_not_found = true;
                }
            }
            catch (Exception e)
            {
                project_not_found = true;
                project_id = "0";
            }
    %>
    <!-- Page specific CSS-->
    <!-- END Page Level CSS-->
    
    
    <form action="project_update" method="post" enctype="multipart/form-data">
        <input type="hidden" name="project_id" id="project_id" value="<%=project_id%>"/>
        <!--future fields-->
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Project ID
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" name="id" id="id" readonly value="<%=project_info[0]%>" class="form-control" disabled/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Project Name
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" name="name" id="name" value="<%=project_info[1]%>" class="form-control" placeholder="Enter Project Name"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Status
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <%
                    ArrayList<String[]> status_select = db.get_system_select_field.active_for_table_column(con, "projects", "status");
                    %>
                    <select name="status" id="status" class="form-control">
                        <option value=""></option>
                        <%
                        for(int a = 0; a < status_select.size(); a++)
                        {
                            String select_option[] = status_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            if(project_info[11].equalsIgnoreCase(select_option[3]))
                            {
                                selected = "SELECTED";
                            }
                            else
                            {
                                selected = "";
                            }
                            %>
                            <option <%=selected%> value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Project Owner&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="hidden" name="owner_id" id="owner_id" value="<%=project_info[4]%>"/>
                    <input type="text" name="owner_username" id="owner_username" value="<%=project_info[15]%>" class="form-control" placeholder="Enter the Owners username"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Project Owners Group&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <%
                    String owner_group_id = project_info[3];
                    String owner_group_id_info[] = db.get_groups.by_id(con, owner_group_id);
                    %>
                    <input type="hidden" name="owner_group_id" id="owner_group_id" value="<%=owner_group_id%>"/>
                    <input type="text" name="owner_group_name" id="owner_group_name" value="<%=owner_group_id_info[1]%>" class="form-control" placeholder="Enter the Owners Group"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Project Manager&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="hidden" name="manager_id" id="manager_id" value="<%=project_info[5]%>"/>
                    <input type="text" name="manager_username" id="manager_username" value="<%=project_info[20]%>" class="form-control" placeholder="Enter the Managers username"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Tech Lead&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="hidden" name="tech_lead_id" id="tech_lead_id" value="<%=project_info[6]%>"/>
                    <input type="text" name="tech_lead_username" id="tech_lead_username" value="<%=project_info[25]%>" class="form-control" placeholder="Enter the Tech Leads username"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Priority
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <%
                    ArrayList<String[]> priority_select = db.get_system_select_field.active_for_table_column(con, "projects", "priority");
                    %>
                    <select name="priority" id="priority" class="form-control">
                        <option value=""></option>
                        <%
                        for(int a = 0; a < priority_select.size(); a++)
                        {
                            String select_option[] = priority_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            if(project_info[12].equalsIgnoreCase(select_option[3]))
                            {
                                selected = "SELECTED";
                            }
                            else
                            {
                                selected = "";
                            }
                            %>
                            <option <%=selected%> value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Scheduled Start Date
                    </label>
                </div>
                <div class="formField clr md border-0 input-group d-flex">
                    <%
                    String scheduled_start_date = "";
                    try
                    {
                        ZonedDateTime zdt_temp_date = support.date_utils.utc_to_user_tz(user_tz_name, project_info[7]);
                        scheduled_start_date = DateTimeFormatter.ofPattern("HH:mm MM/dd/yyyy").format(zdt_temp_date);
                    }
                    catch(Exception e)
                    {
                        scheduled_start_date = "";
                    }                                                
                    %>
                    <input type='text' id="scheduled_start_date" name="scheduled_start_date" value="<%=scheduled_start_date%>" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Scheduled End Date
                    </label>
                </div>
                <div class="formField clr md border-0 input-group d-flex">
                    <%
                    String scheduled_end_date = "";
                    try
                    {
                        //java.util.Date rev_date = timestamp_format.parse(request_info[2]);
                        //rev_time = date_time_picker_format.format(rev_date);
                        ZonedDateTime zdt_temp_date = support.date_utils.utc_to_user_tz(user_tz_name, project_info[9]);
                        scheduled_end_date = DateTimeFormatter.ofPattern("HH:mm MM/dd/yyyy").format(zdt_temp_date);
                    }
                    catch(Exception e)
                    {
                        scheduled_end_date = "";
                    }  
                    %>
                    <input type='text' id="scheduled_end_date" name="scheduled_end_date" value="<%=scheduled_end_date%>" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Actual Start Date
                    </label>
                </div>
                <div class="formField clr md border-0 input-group d-flex">
                    <%
                    String actual_start_date = "";
                    try
                    {
                        //java.util.Date rev_date = timestamp_format.parse(request_info[2]);
                        //rev_time = date_time_picker_format.format(rev_date);
                        ZonedDateTime zdt_temp_date = support.date_utils.utc_to_user_tz(user_tz_name, project_info[8]);
                        actual_start_date = DateTimeFormatter.ofPattern("HH:mm MM/dd/yyyy").format(zdt_temp_date);
                    }
                    catch(Exception e)
                    {
                        actual_start_date = "";
                    }  
                    %>
                    <input type='text' id="actual_start_date" name="actual_start_date" value="<%=actual_start_date%>" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Actual End Date
                    </label>
                </div>
                <div class="formField clr md border-0 input-group d-flex">
                    <%
                    String actual_end_date = "";
                    try
                    {
                        //java.util.Date rev_date = timestamp_format.parse(request_info[2]);
                        //rev_time = date_time_picker_format.format(rev_date);
                        ZonedDateTime zdt_temp_date = support.date_utils.utc_to_user_tz(user_tz_name, project_info[10]);
                        actual_end_date = DateTimeFormatter.ofPattern("HH:mm MM/dd/yyyy").format(zdt_temp_date);
                    }
                    catch(Exception e)
                    {
                        actual_end_date = "";
                    } 
                    %>
                    <input type='text' id="actual_end_date" name="actual_end_date" value="<%=actual_end_date%>" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Notes
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <textarea class="form-control" name="notes" id="notes" rows="5"><%=project_info[13]%></textarea>  
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Description
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" name="description" id="description" value="<%=project_info[2]%>" class="form-control"/> 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Attachments
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="file" name="file" multiple>
                        <label class="custom-file-label" for="file">Choose file(s)</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="formField clr md border-0">
                    <% 
                    ArrayList<String[]> attachments = new ArrayList();
                    attachments = db.get_projects.get_attachments_by_project_id(con, project_id);
                    for (int i = 0; i < attachments.size(); i++)
                    {
                        String[] attachment = attachments.get(i);
                    %>
                    <div class="clr tag dInlineBlock formField md whtbg mr-5 mb-15 w-auto float-none">
                        <span><%=attachment[4]%></span>
                        <a data-id="<%=attachment[0]%>" href="javascript:void(0)" onclick="deleteAttachment()"><img class="icon" src="assets/images/svg/cross-icon.svg" alt=""></a>
                    </div>                    
                    <%
                    }
                    %>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-12 text-center pt-30 pb-30">
                <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                <button type="button" class="btn btn-primary-new customBtn lg waves-effect rightSidePanelCloser" role="button" onclick="saveProject()">
                    Save
                </button>
            </div>
        </div>                                

    </form>
    
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>
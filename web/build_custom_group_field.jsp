<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->

        <%
        String field_name = request.getParameter("field_name");
        String field_label = request.getParameter("field_label");
        String number_cols = request.getParameter("number_cols");
        String field_values = request.getParameter("field_values");   
        String new_record = request.getParameter("new_record"); 
        String default_text = request.getParameter("default_text"); 
        String db_value = request.getParameter("db_value"); 
        String group_name = request.getParameter("group_name"); 
        String value = "";
        String lookup_field_name = "lookup_" + field_name;
        if(new_record.equalsIgnoreCase("true"))
        {
            value = "0";
            group_name = "Group Unassigned";
        }
        else
        {
            value = db_value;
        }    
            
        %>
        <div class="col-xl-2 col-lg-6 col-12">
            <label><br><%=field_label%>&nbsp;<i class="la la-search" style="font-size: 14px;"></i></label>
            <input type="hidden" name="<%=field_name%>" id="<%=field_name%>" value="<%=value%>"/>
            <input type="text" name="<%=lookup_field_name%>" id="<%=lookup_field_name%>" value="<%=group_name%>" class="form-control" />
        </div>
        <script>
        //caller_group_name
        $( "#<%=lookup_field_name%>" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_group",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#<%=lookup_field_name%>').val(ui.item.label); // display the selected text
                $('#<%=field_name%>').val(ui.item.value); // save selected id to input
                //alert("setting is=" + ui.item.value);
                return false;
            }
        }); 
    </script>
<%
if (session.getAttribute("alert") != null) 
{
    String alert = session.getAttribute("alert").toString();
    session.removeAttribute("alert");

    String alert_class = "alert-info";
    if (session.getAttribute("alert-class") != null) 
    {
        alert_class = session.getAttribute("alert-class").toString();
        session.removeAttribute("alert-class");
    }

%>
<div class="alert <%=alert_class%> alert-dismissible fade show mb-0" role="alert">
    <%=alert%>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<%
}
%>
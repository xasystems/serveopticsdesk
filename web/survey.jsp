<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.text.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.io.StringReader"%>
<%@ page import="javax.json.Json"%>
<%@ page import="javax.json.JsonArray"%>
<%@ page import="javax.json.JsonArrayBuilder"%>
<%@ page import="javax.json.JsonObjectBuilder"%>
<%@ page import="javax.json.JsonObject"%>
<%@ page import="javax.json.JsonReader"%>
<%@ page import="org.apache.logging.log4j.LogManager"%>
<%@ page import="org.apache.logging.log4j.Logger"%>
<!DOCTYPE html>
    <%
    Logger logger = LogManager.getLogger();

    String survey_key = request.getParameter("survey");
    String contract_id = request.getParameter("id");
    
    if(survey_key == null || contract_id == null)
    {
       response.sendRedirect("survey_error.jsp?error=Survey request is missing its key");
    }
    else 
    {
        java.util.Date now = new java.util.Date();
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        
        session.setAttribute("db",contract_id);
        session.setAttribute("db_host",props.get("db_host").toString());
        session.setAttribute("db_port",props.get("db_port").toString());
        session.setAttribute("db_user",props.get("db_user").toString());
        session.setAttribute("db_password",props.get("db_password").toString());
        session.setAttribute("db_password_encrypted",props.get("db_password_encrypted").toString());
        session.setAttribute("debug",props.get("debug").toString());
        session.setAttribute("log_level",props.get("log_level").toString());
        session.setAttribute("timezone",props.get("timezone").toString());
        
        Connection con = db.db_util.get_contract_connection(context_dir, session); 
        
        SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
        SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
        String survey_results[] = db.get_survey_results.by_survey_key(con, survey_key);
        String incident_info[] = new String[0];
        String request_info[] = new String[0];
        String object_date = "";
        String object_description = "";
        if(survey_results.length == 0)
        {
            response.sendRedirect("survey_error.jsp?error=not_found");
        }
        else
        {
            java.util.Date expire_date = timestamp.parse(survey_results[6]);
            if(expire_date.getTime() <= now.getTime())
            {
                response.sendRedirect("survey_error.jsp?error=expired");
            }
            else
            {
                boolean is_completed = true;
                try
                {
                    java.util.Date date_completed = timestamp.parse(survey_results[8]);
                }
                catch(Exception e)
                {
                    is_completed = false;
                }
                if(is_completed)
                {
                    response.sendRedirect("survey_error.jsp?error=complete");
                }
                else
                {
                    if(survey_results[3].equalsIgnoreCase("incident"))
                    {
                        incident_info = db.get_incidents.incident_by_id(con, survey_results[4]);
                    }
                    else if(survey_results[3].equalsIgnoreCase("request"))
                    {
                        request_info = db.get_requests.request_by_id(con,survey_results[4]);
                        object_date = filter_format.format(timestamp.parse(request_info[7]));
                        object_description = request_info[29];
                    }
                    String survey_info[] = db.get_surveys.by_id(con, survey_results[1]);


                    %>
                    <jsp:include page='header_survey.jsp'>
                        <jsp:param name="page_type" value=""/>
                    </jsp:include>	
                    <link rel="icon" type="image/x-icon" href="assets/images/favicon.ico">
                    <div class="app-content content">
                        <div class="content-wrapper">
                            <form action="survey_submit">
                                <input type="hidden" name="survey_key" id="survey_key" value="<%=survey_key%>"/>
                                <input type="hidden" name="id" id="id" value="<%=survey_results[0]%>"/>
                                <!-- start content here-->
                                <div class="content-header row">
                                    <div class="content-header-left col-md-6 col-12 mb-2">
                                        <h3 class="content-header-title"><i class="la la-question"></i>&nbsp;Survey</h3>
                                        <div class="row breadcrumbs-top">
                                            <div class="breadcrumb-wrapper col-12">
                                                <ol class="breadcrumb">

                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">Details</h4>
                                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                                <div class="heading-elements">
                                                    <ul class="list-inline mb-0">
                                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="card-content collapse show">
                                                <div class="card-body">
                                                    <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                   <%
                                                                   if(survey_results[3].equalsIgnoreCase("incident"))
                                                                   {
                                                                       %>
                                                                       <b>Incident:</b> <%=survey_results[4]%>
                                                                       <%
                                                                   }
                                                                   else if(survey_results[3].equalsIgnoreCase("request"))
                                                                   {
                                                                       %>
                                                                       <b>Request:</b> <%=survey_results[4]%>
                                                                       &nbsp;&nbsp;&nbsp;
                                                                       <b>Date of Request:</b> <%=object_date%>
                                                                       &nbsp;&nbsp;&nbsp;
                                                                       <b>Request Description:</b> <%=object_description%>
                                                                       <%
                                                                   }%>
                                                                   <hr>
                                                                   <%
                                                                   String survey_questions_json = survey_info[19];
                                                                   //out.println(survey_questions_json + "<br>");
                                                                   try 
                                                                   {
                                                                        JsonReader reader = Json.createReader(new StringReader(survey_questions_json));
                                                                        JsonObject survey_object = reader.readObject();
                                                                        JsonObject survey = survey_object.getJsonObject("survey");                                                                    
                                                                        JsonArray questions = null;
                                                                        questions = (JsonArray) survey.getJsonArray("questions");
                                                                        String questions_array[][] = new String[questions.size()][8];
                                                                        //out.println("<br>questions.size=" + questions.size() + "<br>");
                                                                        //number , type , question , 1 , 2, 3, 4, 5
                                                                        for(int a = 0; a < questions.size();a++)
                                                                        {
                                                                            JsonObject this_question = questions.getJsonObject(a); //a single question record
                                                                            //int question_number = this_question.getInt("question_number");
                                                                            
                                                                            
                                                                            //out.println("question_number=" + question_number + "  ");

                                                                            int question_number_index = a;
                                                                            //out.println("question_number_index=" + question_number_index + "  ");
                                                                            
                                                                            int question_number = this_question.getInt("question_number");
                                                                            
                                                                            String question_type = this_question.getString("question_type");
                                                                            //out.println("question_type=" + question_type + "  ");

                                                                            String question_text = this_question.getString("question_text");  
                                                                            //out.println("question_text=" + question_text + " <br> ");

                                                                            questions_array[question_number_index][0] = String.valueOf(question_number);
                                                                            questions_array[question_number_index][1] = question_type;
                                                                            questions_array[question_number_index][2] = question_text;


                                                                            //out.println(question_text + "<br>");
                                                                            if(question_type.equalsIgnoreCase("true_false"))
                                                                            {
                                                                                questions_array[question_number_index][3] = this_question.getString("answer_text_1"); 
                                                                                questions_array[question_number_index][4] = ""; 
                                                                                questions_array[question_number_index][5] = ""; 
                                                                                questions_array[question_number_index][6] = ""; 
                                                                                questions_array[question_number_index][7] = this_question.getString("answer_text_5"); 
                                                                            }
                                                                            else if (question_type.equalsIgnoreCase("rating_scale_5"))
                                                                            {
                                                                                questions_array[question_number_index][3] = this_question.getString("answer_text_1"); 
                                                                                questions_array[question_number_index][4] = this_question.getString("answer_text_2"); 
                                                                                questions_array[question_number_index][5] = this_question.getString("answer_text_3"); 
                                                                                questions_array[question_number_index][6] = this_question.getString("answer_text_4"); 
                                                                                questions_array[question_number_index][7] = this_question.getString("answer_text_5");
                                                                            }
                                                                            else if (question_type.equalsIgnoreCase("opinion"))
                                                                            {
                                                                                questions_array[question_number_index][3] = ""; 
                                                                                questions_array[question_number_index][4] = ""; 
                                                                                questions_array[question_number_index][5] = ""; 
                                                                                questions_array[question_number_index][6] = ""; 
                                                                                questions_array[question_number_index][7] = ""; 
                                                                                
                                                                            }
                                                                        }
                                                                        //output a pretty formated table
                                                                        try
                                                                        {
                                                                        %>
                                                                        <table cellspacing="5" cellpadding="5" > 
                                                                        <%
                                                                        for(int a = 0; a < questions_array.length;a++)
                                                                        {
                                                                            //System.out.println("questions_array[a][1]=" + questions_array[a][1]);
                                                                            if(questions_array[a][1].equalsIgnoreCase("true_false"))
                                                                            {
                                                                                //System.out.println("a=" + a + " " + questions_array[a][1]);
                                                                                %>
                                                                                <%=a + 1%>.&nbsp;<%=questions_array[a][2]%>
                                                                                <br>
                                                                                &nbsp;&nbsp;&nbsp;
                                                                                <input type="radio" name="question-<%=questions_array[a][0]%>-<%=questions_array[a][1]%>" id="question-<%=questions_array[a][0]%>-<%=questions_array[a][1]%>" value="5">&nbsp;<%=questions_array[a][7]%>
                                                                                &nbsp;&nbsp;&nbsp;
                                                                                <input type="radio" name="question-<%=questions_array[a][0]%>-<%=questions_array[a][1]%>" id="question-<%=questions_array[a][0]%>-<%=questions_array[a][1]%>" value="1">&nbsp;<%=questions_array[a][3]%>
                                                                                <br>
                                                                                <br>
                                                                                <%
                                                                            }
                                                                            else if(questions_array[a][1].equalsIgnoreCase("rating_scale_5"))
                                                                            {
                                                                                //System.out.println("a=" + a + " " + questions_array[a][1]);
                                                                                %>
                                                                                <%=a + 1%>.&nbsp;<%=questions_array[a][2]%>
                                                                                <br>
                                                                                &nbsp;&nbsp;&nbsp;
                                                                                <input type="radio" name="question-<%=questions_array[a][0]%>-<%=questions_array[a][1]%>" id="question-<%=questions_array[a][0]%>-<%=questions_array[a][1]%>" value="5">&nbsp;<%=questions_array[a][7]%>
                                                                                &nbsp;&nbsp;&nbsp;
                                                                                <input type="radio" name="question-<%=questions_array[a][0]%>-<%=questions_array[a][1]%>" id="question-<%=questions_array[a][0]%>-<%=questions_array[a][1]%>" value="4">&nbsp;<%=questions_array[a][6]%>
                                                                                &nbsp;&nbsp;&nbsp;
                                                                                <input type="radio" name="question-<%=questions_array[a][0]%>-<%=questions_array[a][1]%>" id="question-<%=questions_array[a][0]%>-<%=questions_array[a][1]%>" value="3">&nbsp;<%=questions_array[a][5]%>
                                                                                &nbsp;&nbsp;&nbsp;
                                                                                <input type="radio" name="question-<%=questions_array[a][0]%>-<%=questions_array[a][1]%>" id="question-<%=questions_array[a][0]%>-<%=questions_array[a][1]%>" value="2">&nbsp;<%=questions_array[a][4]%>
                                                                                &nbsp;&nbsp;&nbsp;
                                                                                <input type="radio" name="question-<%=questions_array[a][0]%>-<%=questions_array[a][1]%>" id="question-<%=questions_array[a][0]%>-<%=questions_array[a][1]%>" value="1">&nbsp;<%=questions_array[a][3]%>
                                                                                <br>
                                                                                <br>
                                                                                <%
                                                                            }
                                                                            else if(questions_array[a][1].equalsIgnoreCase("opinion"))
                                                                            {
                                                                                //System.out.println("a=" + a + " " + questions_array[a][1]);
                                                                                %>
                                                                                <%=a + 1%>.&nbsp;<%=questions_array[a][2]%></td>
                                                                                <br>
                                                                                &nbsp;&nbsp;&nbsp;
                                                                                <textarea name="question-<%=questions_array[a][0]%>-<%=questions_array[a][1]%>" id="question-<%=questions_array[a][0]%>-<%=questions_array[a][1]%>" cols="90" rows="3"></textarea><%=questions_array[a][7]%>
                                                                                <br>
                                                                                <br>
                                                                                <%
                                                                            }
                                                                        }
                                                                        %>
                                                                        </table> 
                                                                        <%
                                                                        }
                                                                        catch(Exception m)
                                                                        {
                                                                            System.out.println("Exception on survey.jsp question table build:=" + m);
                                                                        }

                                                                        //out.println("questions length=" + questions.size());
                                                                   }
                                                                   catch (Exception e) 
                                                                   {
                                                                       logger.error("ERROR Exception surveys.jsp:=" + e);
                                                                   }
                                                                   %>


                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <button type="submit" class="btn btn-info">
                                                                        &nbsp;&nbsp;<i class="ft-save"></i> Complete&nbsp;&nbsp;
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>                                   
                            <!-- end content here-->
                        </div>        
                    </div>
                    
                    <jsp:include page='footer_survey.jsp'>
                        <jsp:param name="parameter" value=""/>
                    </jsp:include>	
                    <!-- BEGIN PAGE LEVEL JS-->
                    <!-- END PAGE LEVEL JS-->
                    </body>
                </html>
                <%
                }
            }
        }
    }//end if not key
%>
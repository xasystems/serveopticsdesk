<!--Copyright 2019 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_cost
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.*"%>
<%@page import="java.text.DecimalFormat"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        if (!session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            response.sendRedirect("no_permission.jsp");
        } 
        else 
        {
            int requested_year = 0;
            try
            {
                String s_year = request.getParameter("requested_year");
                if(s_year == null || s_year.equalsIgnoreCase("null") || s_year.equalsIgnoreCase(""))
                {
                    java.util.Date now = new java.util.Date();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(now);
                    requested_year = cal.get(Calendar.YEAR);
                }
                else
                {
                    requested_year = Integer.parseInt(request.getParameter("requested_year"));
                }
            }
            catch(Exception e)
            {
                java.util.Date now = new java.util.Date();
                Calendar cal = Calendar.getInstance();
                cal.setTime(now);
                requested_year = cal.get(Calendar.YEAR);
            }
            String home_page = session.getAttribute("home_page").toString();
            Logger logger = LogManager.getLogger();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session); 
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                ArrayList<String[]> all_groups = db.get_groups.all(con);
                String pattern="###,##0.00";
		DecimalFormat df = new DecimalFormat(pattern);
%>
<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>	

<jsp:include page='menu_service_desk.jsp'>
    <jsp:param name="active_menu" value=""/>
</jsp:include>
<!-- BEGIN PAGE LEVEL CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
<!-- END PAGE LEVEL CSS-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- start content here-->
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">Cost</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Administration</a></li>
                            <li class="breadcrumb-item"><a href="#">Cost</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div> <!--End breadcrumbs -->
        <form action="admin_cost">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4"></div>
                                        <div class="col-4">
                                            <label for="year"><h3>Year</h3></label>
                                            <br>
                                            <select onchange="update_year()" class="form-control" name="year" id="year">
                                                <%
                                                GregorianCalendar cal = new GregorianCalendar();
                                                cal.add(cal.YEAR, 1);
                                                int c_year = cal.get(cal.YEAR);
                                                for(int a = c_year; a > c_year - 6; a--)
                                                {
                                                    if(a == requested_year)
                                                    {
                                                        %>
                                                        <option SELECTED value="<%=a%>"><%=a%></option>
                                                        <%
                                                    }
                                                    else
                                                    {
                                                    %>
                                                        <option value="<%=a%>"><%=a%></option>
                                                    <%
                                                    }
                                                }
                                                %>
                                            </select>
                                        </div>
                                        <div class="col-4"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">                                    
                                            <div class="form form-horizontal row-separator">
                                                <div class="form-body">
                                                    <h4 class="form-section"><i class="la la-wrench"></i> Cost of Support by Group</h4>
                                                    <div class="table-responsive">
                                                        <style>
                                                            td {
                                                                border-bottom: 1px solid #e0e0e0;
                                                            } 
                                                            th {
                                                                border-bottom: 1px solid #a0a0a0;
                                                            } 
                                                        </style>
                                                        <table width="100%" border='0' cellspacing='5' cellpadding='5' id="incident_cost_table" name="incident_cost_table">
                                                            <thead>
                                                                <tr>
                                                                    <th width="30">Group</th>
                                                                    <th width="30">Jan</th>
                                                                    <th width="30">Feb</th>
                                                                    <th width="30">Mar</th>
                                                                    <th width="30">Apr</th>
                                                                    <th width="30">May</th>
                                                                    <th width="30">Jun</th>
                                                                    <th width="30">Jul</th>
                                                                    <th width="30">Aug</th>
                                                                    <th width="30">Sep</th>
                                                                    <th width="30">Oct</th>
                                                                    <th width="30">Nov</th>
                                                                    <th width="30">Dec</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <%
                                                                //for each group get values
                                                                for (int c = 0; c < all_groups.size(); c++) 
                                                                {
                                                                    String this_group[] = all_groups.get(c);
                                                                    //Double monthly_data[] = db.get_cost.for_year_and_type_and_group_id(con, requested_year, all_groups[c][0]);
                                                                    Double monthly_data[] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
                                                                    %>
                                                                    <tr>
                                                                        <td><%=this_group[1]%></td>
                                                                        <td><input size="7" type="text" name="inc_jan_<%=this_group[0]%>" value="$<%=df.format(monthly_data[0])%>"/></td>
                                                                        <td><input size="7" type="text" name="inc_feb_<%=this_group[0]%>" value="$<%=df.format(monthly_data[1])%>"/></td>
                                                                        <td><input size="7" type="text" name="inc_mar_<%=this_group[0]%>" value="$<%=df.format(monthly_data[2])%>"/></td>
                                                                        <td><input size="7" type="text" name="inc_apr_<%=this_group[0]%>" value="$<%=df.format(monthly_data[3])%>"/></td>
                                                                        <td><input size="7" type="text" name="inc_may_<%=this_group[0]%>" value="$<%=df.format(monthly_data[4])%>"/></td>
                                                                        <td><input size="7" type="text" name="inc_jun_<%=this_group[0]%>" value="$<%=df.format(monthly_data[5])%>"/></td>
                                                                        <td><input size="7" type="text" name="inc_jul_<%=this_group[0]%>" value="$<%=df.format(monthly_data[6])%>"/></td>
                                                                        <td><input size="7" type="text" name="inc_aug_<%=this_group[0]%>" value="$<%=df.format(monthly_data[7])%>"/></td>
                                                                        <td><input size="7" type="text" name="inc_sep_<%=this_group[0]%>" value="$<%=df.format(monthly_data[8])%>"/></td>
                                                                        <td><input size="7" type="text" name="inc_oct_<%=this_group[0]%>" value="$<%=df.format(monthly_data[9])%>"/></td>
                                                                        <td><input size="7" type="text" name="inc_nov_<%=this_group[0]%>" value="$<%=df.format(monthly_data[10])%>"/></td>
                                                                        <td><input size="7" type="text" name="inc_dec_<%=this_group[0]%>" value="$<%=df.format(monthly_data[11])%>"/></td>
                                                                        
                                                                    </tr>
                                                                    <%
                                                                }
                                                                %>

                                                            </tbody>
                                                        </table>
                                                        <br/>
                                                        <table width="100%" cellspacing='5' cellpadding='5'  border="0">
                                                            <tr>
                                                                <td width="45%" align="left">
                                                                    <button type="sumbit" class="btn mr-1 mb-1 btn-info">&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;</button>
                                                                </td>
                                                            </tr>

                                                        </table>
                                                        <br/><br/><br/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!--end row-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- end content here-->
    </div>        
</div>
<jsp:include page='footer.jsp'>
    <jsp:param name="parameter" value=""/>
</jsp:include>	
<!-- BEGIN PAGE LEVEL JS-->
<script>
    function update_year() {
        var sel = document.getElementById("year");
        var year = sel.value;
        var url = "admin_cost.jsp?requested_year=" + year;        
        window.location = url;
    }
</script>

<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<!-- END PAGE LEVEL JS-->   
<%
                con.close();
            } catch (Exception e) {
                System.out.println("Exception in admin_sla.jsp: " + e);
                logger.debug("ERROR: Exception in admin_sla.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
%>
</body>
</html>

<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : contact_service_compliance
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="javax.json.Json"%>
<%@ page import="javax.json.JsonArray"%>
<%@ page import="javax.json.JsonObject"%>
<%@ page import="javax.json.JsonReader"%>
<%@ page import="javax.json.JsonStructure"%>
<%@page import="java.io.StringReader"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        if (!session.getAttribute("incident").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            //if you have the permission to read/modify or Admin then you can see this
            String context_dir = request.getServletContext().getRealPath("");
            String home_page = session.getAttribute("home_page").toString();
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
            //SimpleDateFormat filter_format2 = new SimpleDateFormat("yyyy"); //01/12/2019 12:00 AM
            java.util.Date filter_start = new java.util.Date();
            java.util.Date filter_end = new java.util.Date();
            java.util.Date previous_start_date = new java.util.Date();
            java.util.Date previous_end_date = new java.util.Date();
            String start = "";
            String end = "";
            String date_range = "";
            String referer = "";
            
            try 
            {
                date_range = request.getParameter("date_range");
                if (date_range.equalsIgnoreCase("null") || date_range == null) 
                {
                    date_range = support.filter_dates.past_30_days();
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                date_range = support.filter_dates.past_30_days();
            }
            try 
            {
                referer = request.getParameter("referer");
                if (referer.equalsIgnoreCase("null") || referer == null) 
                {
                    referer = "incident_home_support.jsp?date_range=" + date_range;
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                referer = "incident_home_support.jsp?date_range=" + date_range;
            }
            try 
            {                
                //split the start and end date  //01/12/2019 12:00 AM - 01/12/2019 11:59 PM
                String temp[] = date_range.split("-");
                filter_start = filter_format.parse(temp[0]);
                filter_end = filter_format.parse(temp[1]);                
                start = timestamp.format(filter_start);
                end = timestamp.format(filter_end);

                //get pervious date frame                
                previous_start_date.setTime(filter_start.getTime());
                previous_end_date.setTime(filter_end.getTime());                
                long diff = filter_end.getTime() - filter_start.getTime();
                previous_start_date.setTime(filter_start.getTime() - diff);
                previous_end_date.setTime(filter_end.getTime() - diff);
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                System.out.println("incident_service_compliance.jsp exception=" + e);
            }
            ArrayList<String[]> all_groups = db.get_groups.all(con);
            
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	
    <input type="hidden" name="start" id="start" value="<%=start%>"/>
    <input type="hidden" name="end" id="end" value="<%=end%>"/>
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">

    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/app.css">-->
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
<!--    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <!--<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">-->
    
    <script src="assets/js/highcharts/highcharts.js"></script>
    <script src="assets/js/highcharts/data.js"></script>
    <script src="assets/js/highcharts/drilldown.js"></script>
    <script src="assets/js/highcharts/exporting.js"></script>
    
    
    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    
    <div class="clr whtbg p-10 mb-15 position-relative">
        <div class="float-right options">
            <ul>
                <li id="filters_dropdown" class="dInlineBlock dropdown ">
                    <div class="dInlineBlock clr menuHolder">
                        <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                        </a>
                        <div class="dropdown-menu dropdown-menu-right md p-10" x-placement="bottom-end" style="position: absolute; transform: translate3d(100px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                            <form id="filter_form">
                                <ul>
                                    <li class="mb-10 clr">
                                        <label class="field-label full-width">
                                            Service Type
                                        </label>
                                        <div class="formField md  mr-0 full-width">
                                            <select name="service_type" class="form-control border-0 h-auto px-0">
                                                <option value="Incident" selected>Incident</option>
                                                <option value="Request">Request</option>
                                                <option value="Contact">Contact</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <label class="field-label full-width">
                                            Date Range
                                        </label>
                                        <div class="formField md  mr-0 full-width">
                                            <input class="no-border full-width datetime" type="text" name="date_range" id="filter_date_range" value="<%=date_range%>">
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <label class="field-label full-width">
                                            Company
                                        </label>
                                        <div class="formField md full-width mr-0">
                                            <input type="text" id="company" name="company" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <label class="field-label full-width">
                                            Callers Group
                                        </label>
                                        <div class="formField md full-width mr-0">
                                            <input type="hidden" name="caller_group_id" id="caller_group_id" value="">
                                            <input type="text" id="caller_group_name" name="caller_group_name" value="" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                        </div>
                                    </li>                                </ul>
                                    <li class="mb-10 clr">
                                        <label class="field-label full-width">
                                            Compliance
                                        </label>
                                        <div class="formField md  mr-0 full-width">
                                            <select name="compliance" class="form-control border-0 h-auto px-0">
                                                <option value="Compliant" selected>Compliant</option>
                                                <option value="Non-Compliant">Non-Compliant</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <label class="field-label full-width">
                                            Reporting
                                        </label>
                                        <div class="formField md  mr-0 full-width">
                                            <select name="reporting" class="form-control border-0 h-auto px-0">
                                                <option value="Reportable" selected>Reportable</option>
                                                <option value="Non-Reportable">Non-Reportable</option>
                                            </select>
                                        </div>
                                    </li>
                                <div class="text-center pt-5 pb-5">
                                    <button type="button" class="btn md2 btn-primary-new customBtn mb-5" onclick="apply_filters()">Apply Filters</button>
                                    <div class="clr"></div>
                                    <a href="#" class="">Reset Filters</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </li>

            </ul>
        </div>

        <h1 class="large-font boldFont" id="page_title"></h1>
<!--        <div class="custom-tabs clr ">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" data-toggle="tab" href="#reportable" role="tab">Reportable</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#non-reportable" role="tab">Non-Reportable</a>
                </li>
            </ul> Tab panes 
        </div>-->
    </div>

<div id="active_filters">
</div>

<div  id="content_container">
</div>

    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN VENDOR JS-->
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>

    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <!--<script src="app-assets/js/core/app-menu.js"></script>-->
    <!--<script src="app-assets/js/core/app.js"></script>-->
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->

    <!--<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>-->
    <!--<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>-->
    <script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
    <script src="app-assets/js/scripts/popover/popover.js"></script>
    
<script>

    function getCharts()
    {
        var filters = {};
        $("#filter_form input,#filter_form select").each(function() {
            if ($(this).attr("name") && $(this).attr("name") != "assigned_to_username" && $(this).attr("name") != "assigned_group_name" && this.value && this.value != "")
            {
                filters[$(this).attr('name')] = this.value;
//                            data[$(this).attr('name')] = this.value;
            }

        });
        $("#content_container").html('<div class="loader loader-top"></div>');

        var url = "404.jsp";
        var service_type = $("#filter_form select[name='service_type']").val();

        switch (service_type)
        {
            case "Incident":
                $("#page_title").html("Incident SLAs");
                url = "incident_service_compliance.jsp";
                break;
            case "Request":
                $("#page_title").html("Request SLAs");
                url = "request_service_compliance.jsp";
                break;
            case "Contact":
                $("#page_title").html("Contact SLAs");
                url = "contact_service_compliance.jsp";
                break;
        }
        
        $.ajax({
            url: url,
            type: "POST",
            data: {
                embedded: true, 
                date_range: $("#filter_form input[name='date_range']").val(), 
                compliance: $("#filter_form select[name='compliance']").val(), 
                reporting: $("#filter_form select[name='reporting']").val(), 
            },
            success: function(response){
//                console.log(response);
                $("#content_container").html(response);
            },
            error: function(jqXHR, textStatus)
            {
                $("#content_container").html("");
                switch (jqXHR.status) {
                    case 401:
                        window.location.assign("<%=props.get("login_url").toString()%>");
                        break;
                    case 403:
                        alert("No permission!");
                        break;
                    default:
                        alert("Error processing request! " + textStatus);
                }
            }
        });
                
    }

    function apply_filters()
    {
        $("#filter_form input,#filter_form select").each(function() {

            var field = $(this);
//            console.log("filter_form_field", field);
            var field_name = field.attr('name');
            
            if (!field_name || field_name == "service_type" || field.attr('type') == "hidden" || (field.attr('type') == "radio" && !field.prop("checked")))
            {
                return;
            }
            if (field.val() && field.val() != "Any")
            {
                var label = "";
                var filter_label = "";
                if (field.attr("type") == "radio")
                {
                    label = "Predefined";
                    filter_label = field.siblings("label").html();
                } else {
                    label = field.closest("div").prev().html();
                    filter_label = field.val();
                }
                if ($("#active_filter_" + field_name).length > 0) {
                    var filter_block = $("#active_filter_" + field_name);
                    $(filter_block).children("span").html(filter_label);
                } else {
                    $("#active_filters").append(
                        $('' +
                            '<div class="clr tag dInlineBlock formField md whtbg mr-5 mb-15" id="active_filter_' + field_name + '">' +
                                '<h5 class="basic-font mediumFont">' + label + '</h5>' +
                                '<span>' + filter_label + '</span>' +
                                '<img class="icon" src="assets/images/svg/cross-icon.svg" alt="" onclick="resetFilterFieldNew(\'' + field_name + '\')">' +
                            '</div>'
                        )
                    );
                }
                setQueryStringParameter(field_name, field.val());
            } else {
                resetFilterFieldNew(field_name);
            }                
        });
        getCharts();
    }
    
    function resetFilterFieldNew(field_name)
    {
        var filter_block = $("#active_filter_" + field_name);
        if (filter_block.length > 0)
        {
            filter_block.remove();
            var filter_field = $("#filter_form input[name='" + field_name + "'],#filter_form select[name='" + field_name + "']");
            if (filter_field.attr("type") != "radio")
            {
                filter_field.val("");
                if (field_name.indexOf("_name") !== false)
                {
                    var filter_id_fields = $("#filter_form input[name='" + field_name.replace("_name","_id") + "'],#filter_form select[name='" + field_name.replace("_name","_id") + "']");
                    if (filter_id_fields.length > 0)
                    {
                        filter_id_fields.val("");
                    }
                }
            } else {
                filter_field.prop('checked', false);
            }
            setQueryStringParameter(field_name, "");
        }
    }

    function attachFieldListeners(container)
    {
        container.find('input[name="company"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incident_company",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="company"]').val(ui.item.label); // display the selected text
                //alert("setting is=" + ui.item.value);
                return false;
            }
        }); 

        container.find('input[name="caller_name"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user_last_first",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {

                // Set selection
                container.find('input[name="caller_name"]').val(ui.item.label); // set name
                container.find('input[name="caller_id"]').val(ui.item.value); // set id
                return false;
            }
        });

        //caller_group_name
        container.find( 'input[name="caller_group_name"]' ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_group",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="caller_group_name"]').val(ui.item.label); // display the selected text
                container.find('input[name="caller_group_id"]').val(ui.item.value); // save selected id to input
                //alert("setting is=" + ui.item.value);
                return false;
            }
        }); 
    }

    window.addEventListener("load", (event) => 
    {
        attachFieldListeners($("#filter_form"));
        $('#filters_dropdown').on('hide.bs.dropdown', function (e) 
        { // prevent dropdown close on autocomplete
            console.log("dropdown closed ", e);
            //            console.log(e.clickEvent.target.className);
            if (e.clickEvent && e.clickEvent.target && (e.clickEvent.target.className == "ui-menu-item-wrapper" || e.clickEvent.target.className == "icon" || e.clickEvent.target.className.indexOf("applyBtn") !== -1)) {
                e.preventDefault();
            }
        });
        apply_filters();
    });
    </script>

    </body>
</html>
<%
        }//end if not permission
    }//end if not logged in
%>
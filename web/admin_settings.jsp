<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    String context_dir = request.getServletContext().getRealPath("");
    LinkedHashMap props = support.config.get_config(context_dir);
    if(session.getAttribute("authenticated")==null)
    {
        
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        if(!session.getAttribute("administration").toString().equalsIgnoreCase("true"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session); 
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_incident_sla_delete.jsp=" + e);
            }
            
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <!-- END PAGE LEVEL CSS-->

    <div class="clr whtbg p-10 mb-15 position-relative">
        <h1 class="large-font boldFont">Settings Home</h1>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class=" box mb-20">
                            <h4 class="card-title">SLA</h4>
                            <p class="card-text">View/Manage the ServeOptics SLA(s).</p>
                            <a href="admin_sla.jsp">Manage SLA</a>
            </div>
        </div>
        <div class="col-md-3">
            <div class=" box mb-20">
                                <h4 class="card-title">Schedules</h4>
                                <p class="card-text">View/Manage Schedules for SLA/Reporting/Notifications</p>
                                <a href="admin_schedules.jsp">Manage Schedules</a>
            </div>
        </div>
        <div class="col-md-3">
            <div class=" box mb-20">
                                <h4 class="card-title">Holidays</h4>
                                <p class="card-text">View/Manage Holidays</p>
                                <a href="admin_holidays.jsp">Manage Holidays</a>
            </div>
        </div>
        <div class="col-md-3">
            <div class=" box mb-20">
                                <h4 class="card-title">Surveys Settings</h4>
                                <p class="card-text">View/Manage Survey Settings.</p>
                                <a href="admin_survey.jsp">Manage Surveys</a>
            </div>
        </div>
        <div class="col-md-3">
            <div class=" box mb-20">
                                <h4 class="card-title">ACD Abandoned Thresholds</h4>
                                <p class="card-text">Enter ACD Abandoned Thresholds</p>
                                <a href="admin_abandoned_thresholds.jsp">Enter Thresholds</a>
            </div>
        </div>
        <div class="col-md-3">
            <div class=" box mb-20">
                                <h4 class="card-title">Knowledge Base</h4>
                                <p class="card-text">Manage Knowledge Bases/Articles</p>
                                <a href="admin_knowledge_base.jsp">Manage Knowledge</a>
            </div>
        </div>
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->   
    <%
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

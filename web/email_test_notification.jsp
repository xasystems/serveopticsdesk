<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.io.UnsupportedEncodingException"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="java.net.URLEncoder"%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        Logger logger = LogManager.getLogger();
        String home_page = session.getAttribute("home_page").toString();
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        try
        {
            Connection con = db.db_util.get_contract_connection(context_dir, session); 
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            String email_host = db.get_configuration_properties.by_name(con, "send_email_host").trim();
            //options SSL,TLS,PLAIN
            String send_email_encryption = db.get_configuration_properties.by_name(con, "send_email_encryption").trim();
            String send_email_smtp_port = db.get_configuration_properties.by_name(con, "send_email_smtp_port").trim();
            String send_email_user = db.get_configuration_properties.by_name(con, "send_email_user").trim();
            String send_email_password = db.get_configuration_properties.by_name(con, "send_email_password").trim();    
            send_email_password = support.encrypt_utils.decrypt(context_dir, send_email_password);

            String type = request.getParameter("type");
            String subject = request.getParameter("subject");
            String body = request.getParameter("body");

            System.out.println("in type=" + type );
            type = URLEncoder.encode(type, "UTF-8");
            System.out.println("out type=" + type );

            System.out.println("in subject=" + subject );
            subject = URLEncoder.encode(subject, "UTF-8");
            System.out.println("out subject=" + subject );

            System.out.println("in body=" + body);
            body = URLEncoder.encode(body, "UTF-8");
            System.out.println("out body=" + body);

    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

   
    <!-- BEGIN PAGE LEVEL CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END PAGE LEVEL CSS-->
    
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title">Test Email</h3>
                    
                </div>
            </div> <!--End breadcrumbs -->
            <div class="row match-height">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form" action="email_test_notification_results.jsp" method="post">
                                    <input type="hidden" id="type" name="type" value="<%=type%>"/>
                                    <input type="hidden" id="subject" name="subject" value="<%=subject%>"/>
                                    <input type="hidden" id="body" name="body" value="<%=body%>"/>
                                    
                                    <p class="card-text">Test Notification Email Settings</p>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="to_address">To:</label>
                                                <input type="text" id="to_address" name="to_address" class="form-control" placeholder="Enter an email address">
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="type"><%=type%> Number to use for Notification testing:</label>
                                                <input type="text" id="number" name="number" class="form-control">
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-primary mr-1">
                                            <i class="ft-mail"></i> Send a Test Email
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <!-- END PAGE LEVEL JS-->   
    <%
        con.close();
        }
        catch(Exception e)
        {
            System.out.println("Exception in email_test_notification.jsp: " + e); 
            logger.debug("ERROR: Exception in email_test_notification.jsp:" + e);
        }
    }//end if not redirected
    %>
    </body>
</html>

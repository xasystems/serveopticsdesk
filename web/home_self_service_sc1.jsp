<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : home_self_service
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>

<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        System.out.println("start home_self_service_sc1.jsp");
        //is self_service enabled?
        
        //are you logged in?
        
        if (!session.getAttribute("self_service").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("true")) 
        {
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            session.setAttribute("home_page", "home_service_catalog.jsp");
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020            
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap sys_props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session); 
            boolean enabled = true;
            
            if(db.get_configuration_properties.by_name(con, "self_service_enabled").equalsIgnoreCase("true"))
            {
                enabled = true;
            }
            else
            {
                enabled = false;
            }
            
            String user_id = session.getAttribute("user_id").toString();
            String user_info[] = db.get_users.by_id(con, user_id);
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            String catalog_id = request.getParameter("id");
            String catalog_info[] = db.get_service_catalog.service_catalog_by_id(con, catalog_id);
            ArrayList <String[]> catalog_items = db.get_service_catalog.service_catalog_items_in_catalog_by_state(con, catalog_id, "Published");
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/dashboard.css">
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <div class="app-content content">
        <div class="app-content content">
            <div class="content-wrapper">
                <!-- start content here-->
                <div class="content-header row">
                    <div class="content-header-left col-md-6 col-12 mb-2">
                        <h3 class="content-header-title"><a href="home_self_service.jsp"><i class="la ft-home"></i>&nbsp;&nbsp;&nbsp;Self Service</h3></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <hr>
                    </div>
                </div>
                <%
                //get all published items in this catalog
                for(int a = 0; a < catalog_items.size();a++)
                {
                    String catalog_item[] = catalog_items.get(a);
                    %>
                    <div class="card" style="cursor: pointer;" onclick="javascript:location.href='home_self_service_sc2.jsp?id=<%=catalog_item[0]%>'; return false;" >
                        <div class="card-header">
                            <h1><i class="la  font-large-2 float-left"></i>&nbsp;&nbsp;<%=catalog_item[2]%></h1>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <%=catalog_item[3]%>
                            </div>
                        </div>
                    </div>    
                    <%
                }
                if(catalog_items.size() == 0)
                {
                    %>
                    <div class="row">
                        <div class="col-12">
                            This Catalog seems to be empty.
                        </div>
                    </div>
                    <%
                }
                %>
                <!-- end content here-->
            </div>        
        </div>      
    </div>
    
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>  
    <script>
        function change_submitted_for_email()
        {
            if(document.getElementById('for_someone_else').checked)
            {
                document.getElementById('submitted_for_email').readOnly = false;
            }
            else
            {
                document.getElementById('submitted_for_email').value = '';
                
                
                document.getElementById('submitted_for_email').readOnly = true;
                
            }
        }
        
    </script>
    <script>
        function toggleDiv(id) 
        {
            var div = document.getElementById(id);
            div.style.display = div.style.display == "none" ? "block" : "none";
        }
    </script>
    <script>
    //knowledge_search_terms
    $(document).ready(function() {
         $("#knowledge_search_terms").autocomplete({

             source: function( request, response ) 
             {
                 // Fetch data
                 $.ajax({
                     url: "ajax_lookup_self_service_knowledge_article_text",
                     type: 'post',
                     dataType: "json",
                     minLength: 2,
                     data: 
                     {
                         search: request.term
                     },
                     success: function( data ) 
                     {
                         response( data );
                     }
                 });
             },
             select: function( event, ui ) 
             { 
                 $('#knowledge_search_terms').val(ui.item.article_description);
                 //window.location.href = ui.item.value;
                 window.open("kb_view.jsp?id=" + ui.item.value, "Knowledge Base Article","directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=600,height=600,top=300,left=500");
                 //window.open("kb_view.jsp?id=" + ui.item.value, "Knowledge Base Article", "width=600,height=600,top=400,left=400");
                 return false;
             }
         });
     });
    </script>
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>
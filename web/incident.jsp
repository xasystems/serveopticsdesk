<%@page import="org.apache.commons.lang3.math.NumberUtils"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<!--Copyright 2021 Real Data Technologies, Inc. , All rights reserved.-->
<%-- 
    Document   : incident.jsp
    Created on : 27-Jan-2020, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Calendar"%> 
<%@ page import="java.util.GregorianCalendar"%>
<%@ page import="java.time.LocalDateTime"%>
<%@ page import="java.time.ZoneId"%>
<%@ page import="java.time.ZonedDateTime"%>
<%@ page import="java.time.format.DateTimeFormatter"%>
<%@ page import="java.util.TimeZone"%>

<!DOCTYPE html>
    <%
    String context_dir = request.getServletContext().getRealPath("");
    LinkedHashMap props = support.config.get_config(context_dir);
    SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
    //SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / MM/dd/yyyy");//18:31 / 14-Jan-19
    SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020    
    String force2col = StringUtils.defaultString(request.getParameter("force2col"));

    if(session.getAttribute("authenticated")==null)
    {
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        if (!session.getAttribute("incident").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            String user_tz_name = "UTC";
            String user_tz_time = "+00:00";
            int ci_id = 0;
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }                
            }
            catch(Exception e)
            {
                user_tz_name = "UTC";
                user_tz_time = "+00:00";
            }            
            
            String home_page = session.getAttribute("home_page").toString();
            boolean incident_not_found = false;
            
            java.util.Date now = new java.util.Date();
            //String now_display_format = display_format.format(now);
            //String now_timestamp_format = timestamp_format.format(now);
            //String now_incident_time = date_time_picker_format.format(now);
            Connection con = null;
            String selected = "";
            String category_id = "";
            String create_date = "";
            String incident_info[] = {"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};
            String asset_info[] = {"","","","","","","","","","","","","","","","","","",""};
            try
            {
                con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
            }
            catch(Exception e)
            {
                System.out.println("incident.jsp exception=" + e);
            }
            //get incident info
            String id = "0";
            try
            {
                id = request.getParameter("id");
                incident_info = db.get_incidents.incident_by_id(con, id);
                
                if(incident_info[0].equalsIgnoreCase("--"))
                {
                    incident_not_found = true;
                }
                create_date = incident_info[16];
                ci_id = NumberUtils.toInt(incident_info[11], 0);
                if (ci_id > 0) {
                    asset_info = db.get_assets.for_id(con, String.valueOf(ci_id));
                }
            }
            catch (Exception e)
            {
                System.out.println("incident.jsp incident_id " + id + " not found. exception=" + e);
                incident_not_found = true;
                id = "0";
            }
            boolean is_admin = false;
            if (session.getAttribute("administration").toString().equalsIgnoreCase("true") || session.getAttribute("manager").toString().equalsIgnoreCase("true")) 
            {
                is_admin = true;
            }
    %>
    <!-- BEGIN PAGE LEVEL CSS-->
    <!-- BEGIN PAGE LEVEL CSS-->
    
    <form id="edit_form_<%=id%>" action="incident_update" method="post" enctype="multipart/form-data" onsubmit="return validateForm()">
        <input type="hidden" name="id" id="id" value="<%=id%>"/>
        <!--Field that do no change-->
        <input type="hidden" name="create_date" id="create_date" value="<%=create_date%>"/>
        
        <!--future fields-->
        <input type="hidden" name="ci_id" id="ci_id" value="<%=ci_id%>"/>
        <input type="hidden" name="impact" id="impact" value=""/>
        <input type="hidden" name="urgency" id="urgency" value=""/>

        <div class="row row-cols-1 row-cols-md-2<%=(force2col.equals("") ? " row-cols-xl-3 row-cols-xxl-4" : "")%>">
                <%
                if(incident_not_found)
                {
                    %>
                <div class="col">
                    <font color="red">INCIDENT NOT FOUND</font>
                </div>
                    <%
                }
                %>
                <%
                if(is_admin)
                {
                    %>
                <div class="col">
				<div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Administrator::
                    </label>
				</div>
				<div class="formField clr md border-0 px-0">
                    <select name="admin_function" id="admin_function" onchange="goto_delete(<%=id%>)" class="selectBox form-control">
                        <option value=""></option>
                        <option value="delete">Delete this Incident</option>
                    </select>
                	</div>
			</div>
                    <%
                }
                %>
            <div class="w-100"></div>
            <div class="col">
				<div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    Incident ID
                </label>
				</div>
				<div class="formField clr md border-0 px-0">
                <input name="id" id="id" type="text" readonly value="" class="form-control" disabled placeholder="<%=incident_info[0]%>"/>
            	</div>
			</div>
            <div class="col">
				<div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                        Department&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                </label>
				</div>
				<div class="formField clr md border-0 px-0">
                    <input type="text" id="department" name="department" value="<%=incident_info[6]%>" class="form-control">
            	</div>
			</div>
            <div class="col">
				<div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Assigned to Group&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
				</div>
				<div class="formField clr md border-0 px-0">
                    <%
                    String assigned_group_id = incident_info[21];
                    String assigned_group_id_info[] = db.get_groups.by_id(con, assigned_group_id);
                    %>
                    <input type="hidden" name="assigned_group_id" id="assigned_group_id" value="<%=assigned_group_id%>"/>
                    <input type="text" name="assigned_group_name" id="assigned_group_name" value="<%=assigned_group_id_info[1]%>" class="form-control" />
            	</div>
			</div>
            <div class="col">
				<div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Incident Time
                    </label>
				</div>
				<div class="formField clr md border-0 px-0 input-group d-flex">
                    <%
                    ZonedDateTime zdt_incident_time = support.date_utils.utc_to_user_tz(user_tz_name, incident_info[2]);
                    DateTimeFormatter date_picker_formatter = DateTimeFormatter.ofPattern("HH:mm MM/dd/yyyy");
                    String incident_time = zdt_incident_time.format(date_picker_formatter);
                    %>                                                
                    <input type='text' id="incident_time" name="incident_time" value="<%=incident_time%>" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
            	   </div>
			     </div>
            </div>
            <div class="col">
				<div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Site&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
				</div>
				<div class="formField clr md border-0 px-0">
                    <input type="text" id="site" name="site" value="<%=incident_info[7]%>" class="form-control">
            	</div>
			</div>
            <div class="col">
				<div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                    Assigned to&nbsp;
                    <i class="la la-search" style="font-size: 14px;"></i>
                    <i class="la la-question-circle" style="font-size: 14px;" title="This field is filtered by the Assigned to Group field. If Group Unassigned is selected, then all users will be available. If a specific group is selected, then only the members of that group will be available. If the Assigned to Group is changed this field will be cleared."></i>
                    </label>
				</div>
				<div class="formField clr md border-0 px-0">
                    <%
                    String assigned_to_id = incident_info[22];
                    String assigned_to_id_info[] = db.get_users.by_id(con, assigned_to_id);
                    %>
                    <input type="hidden" name="assigned_to_id" id="assigned_to_id" value="<%=assigned_to_id%>"/>
                    <input type="text" name="assigned_to_name" id="assigned_to_name" value="<%=assigned_to_id_info[1]%>" class="form-control" placeholder="Enter the creaters username"/>
            	</div>
			</div>
            <div class="col">
				<div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        State
                    </label>
				</div>
				<div class="formField clr md border-0 px-0">
                    <select name="state" id="state" class="form-control">
                        <%
                        ArrayList<String[]> state_select = db.get_system_select_field.active_for_table_column(con, "incidents", "state");
                        for(int a = 0; a < state_select.size(); a++)
                        {
                            String select_option[] = state_select.get(a);
                            if(incident_info[19].equalsIgnoreCase(select_option[3]))
                            {
                                selected = "SELECTED";
                            }
                            else
                            {
                                selected = "";
                            }
                            //select_option[3] = value    select_option[4] = label
                            %>
                            <option <%=selected%> value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
            	</div>
			</div>
            <div class="col">
				<div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Company&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
				</div>
				<div class="formField clr md border-0 px-0">
                    <input type="text" id="company" name="company" value="<%=incident_info[8]%>" class="form-control">
            	</div>
			</div>
            <div class="col">
				<div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Caller&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
				</div>
				<div class="formField clr md border-0 px-0">
                    <%
                    String caller_id = incident_info[3];
                    String caller_id_info[] = db.get_users.by_id(con, caller_id);
                    %>
                    <input type="hidden" name="caller_id" id="caller_id" value="<%=caller_id_info[0]%>"/>
                    <input type="text" name="caller_username" id="caller_username" value="<%=caller_id_info[1]%>" class="form-control" placeholder="Enter the callers username"/>
            	</div>
			</div>
            <div class="col">
				<div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Priority
                    </label>
				</div>
				<div class="formField clr md border-0 px-0">
                    <%
                    ArrayList<String[]> priority_select = db.get_system_select_field.active_for_table_column(con, "incidents", "priority");
                    %>
                    <select name="priority" id="priority" class="form-control">
                        <option value=""></option>
                        <%
                        for(int a = 0; a < priority_select.size(); a++)
                        {
                            String select_option[] = priority_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            if(incident_info[14].equalsIgnoreCase(select_option[3]))
                            {
                                selected = "SELECTED";
                            }
                            else
                            {
                                selected = "";
                            }
                            //select_option[3] = value    select_option[4] = label
                            %>
                            <option <%=selected%> value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
            	</div>
			</div>
            <div class="col">
				<div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Caller Group&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
				</div>
				<div class="formField clr md border-0 px-0">
                    <%
                    String caller_group_id = incident_info[4];
                    String caller_group_id_info[] = db.get_groups.by_id(con, caller_group_id);
                    %>
                    <input type="hidden" name="caller_group_id" id="caller_group_id" value="<%=caller_group_id%>"/>
                    <input type="text" name="caller_group_name" id="caller_group_name" value="<%=caller_group_id_info[1]%>" class="form-control" placeholder="Enter the callers Group"/>
            	</div>
			</div>
            <div class="col">
				<div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Category
                    </label>
				</div>
				<div class="formField clr md border-0 px-0">
                    <select class="form-control" id="category_id" name="category_id" onchange="update_subcategory()">
                        <option value=""></option>
                        <%
                        ArrayList<String[]> categories = db.get_category.all_active(con);
                        for(int a = 0; a < categories.size();a++)
                        {
                            String category[] = categories.get(a);
                            if(incident_info[9] == null)
                            {
                                incident_info[9] = "";
                            }
                            if(incident_info[9].equalsIgnoreCase(category[1]))
                            {
                                selected = "SELECTED";
                                category_id = category[0];
                            }
                            else
                            {
                                selected = "";
                            }
                            %>
                            <option <%=selected%> value="<%=category[0]%>"><%=category[1]%></option>
                            <%
                        }
                        %>
                    </select>
            	</div>
			</div>
            <div class="col">
				<div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Pending Reason
                    </label>
				</div>
				<div class="formField clr md border-0 px-0">
                    <%
                    ArrayList<String[]> pending_reason_select = db.get_system_select_field.active_for_table_column(con, "incidents", "pending_reason");
                    %>
                    <select name="pending_reason" id="pending_reason" class="form-control">
                        <option value=""></option>
                        <%
                        for(int a = 0; a < pending_reason_select.size(); a++)
                        {
                            String select_option[] = pending_reason_select.get(a);  
                            if(incident_info[30].equalsIgnoreCase(select_option[3]))
                            {
                                selected = "SELECTED";
                            }
                            else
                            {
                                selected = "";
                            }
                            
                            //select_option[3] = value    select_option[4] = label
                            %>
                            <option <%=selected%> value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
            	</div>
			</div>
            <div class="col">
				<div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Incident Create By&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
				</div>
				<div class="formField clr md border-0 px-0">
                    <%
                    String create_by_id = incident_info[17];
                    String create_by_id_info[] = db.get_users.by_id(con, create_by_id);
                    %>
                    <input type="hidden" name="create_by_id" id="create_by_id" value="<%=create_by_id%>"/>
                    <input type="text" name="create_by_username" id="create_by_username" value="<%=create_by_id_info[1]%>" class="form-control" placeholder="Enter the creaters username"/>
            	</div>
			</div>
            <div class="col">
				<div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Subcategory
                    </label>
				</div>
				<div class="formField clr md border-0 px-0">
                    <select class="form-control" id="subcategory" name="subcategory">
                        <%
                        ArrayList<String[]> subcategories = db.get_subcategory.for_category_id(con, category_id);
                        for(int a = 0; a < subcategories.size();a++)
                        {
                            String subcategory[] = subcategories.get(a);
                            if(incident_info[10] == null)
                            {
                                incident_info[10] = "";
                            }
                            if(incident_info[10].equalsIgnoreCase(subcategory[2]))
                            {
                                selected = "SELECTED";
                            }
                            else
                            {
                                selected = "";
                            }
                            %>
                            <option <%=selected%> value="<%=subcategory[2]%>"><%=subcategory[2]%></option>
                            <%
                        }
                        %>
                        <option value=""></option>
                    </select>
            	</div>
			</div>
            <div class="col">
				<div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Closure Reason
                    </label>
				</div>
				<div class="formField clr md border-0 px-0">
                    <%
                    ArrayList<String[]> closed_reason_select = db.get_system_select_field.active_for_table_column(con, "incidents", "closed_reason");
                    %>
                    <select name="closed_reason" id="closed_reason" class="form-control">
                        <option value=""></option>
                        <%
                        for(int a = 0; a < closed_reason_select.size(); a++)
                        {
                            String select_option[] = closed_reason_select.get(a);  
                            if(incident_info[28].equalsIgnoreCase(select_option[3]))
                            {
                                selected = "SELECTED";
                            }
                            else
                            {
                                selected = "";
                            }
                            
                            //select_option[3] = value    select_option[4] = label
                            %>
                            <option <%=selected%> value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
            	</div>
			</div>
            <div class="col">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-0 pb-0">
                        Caller VIP?
                        <div class="blockquote-footer">
                            <em>Set by Caller's profile</em>
                        </div>
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <span class="icon mr-5"><img src="assets/images/svg/crown-icon.svg" alt=""></span>
                    <div class="custom-control custom-checkbox dInlineBlock ">
                        <input type="checkbox" class="custom-control-input" name="vip" id="vip" disabled="">

                    <%
                    if(caller_id_info[17].equalsIgnoreCase("true"))
                    {
                        %>
                        <input type="checkbox" checked class="custom-control-input" name="vip" id="vip" disabled="">
                        <%
                    }
                    else
                    {
                        %>
                        <input type="checkbox" class="custom-control-input" name="vip" id="vip" disabled="">
                        <%
                    }
                    %>
                        <label class="custom-control-label small-font" for="vip">Yes</label>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-0 pb-0">
                        First Contact Resolution?
                        <div class="blockquote-footer">
                            <em>Set by Caller's profile</em>
                        </div>
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">    
                    <div class="form-check form-check-inline full-width">
                        <input <%=(incident_info[26].equalsIgnoreCase("true")?"checked":"")%> class="form-check-input w-auto" type="radio" name="first_contact_resolution" id="first_contact_resolution_yes" value="on">
                        <label class="form-check-label" for="first_contact_resolution_yes">Yes</label>
                        <input <%=(incident_info[26].equalsIgnoreCase("true")?"":"checked")%> class="form-check-input w-auto mx-2" type="radio" name="first_contact_resolution" id="first_contact_resolution_no" value="null">
                        <label class="form-check-label" for="first_contact_resolution_no">No</label>
                    </div>
                </div>
            </div>
            <div class="col">
				<div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Location&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
				</div>
				<div class="formField clr md border-0 px-0">
                    <input type="text" id="location" name="location" value="<%=incident_info[5]%>" class="form-control">
            	</div>
			</div>
            <div class="col">
				<div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Contact Method
                    </label>
				</div>
				<div class="formField clr md border-0 px-0">
                    <select name="contact_method" id="contact_method" class="form-control">
                        <option value=""></option>
                        <%
                        ArrayList<String[]> contact_method_select = db.get_system_select_field.active_for_table_column(con, "incidents", "contact_method");
                        for(int a = 0; a < contact_method_select.size(); a++)
                        {                                                    
                            String select_option[] = contact_method_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            if(incident_info[18].equalsIgnoreCase(select_option[3]))
                            {
                                selected = "SELECTED";
                            }
                            else
                            {
                                selected = "";
                            }
                            //select_option[3] = value    select_option[4] = label
                            %>
                            <option <%=selected%> value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
            	</div>
			</div>
            <div class="col">
				<div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Pending Date
                    </label>
				</div>
                <div class="formField clr md border-0 px-0 input-group d-flex">
                    <%
                    String pending_date_value = "";
                    try
                    {
                        if(incident_info[29] == null || incident_info[29].equalsIgnoreCase("null") || incident_info[29].equalsIgnoreCase(""))
                        {
                            pending_date_value = "";
                        }
                        else
                        {
                            ZonedDateTime zdt_pending_date = support.date_utils.utc_to_user_tz(user_tz_name, incident_info[29]);
                            pending_date_value = zdt_pending_date.format(date_picker_formatter);   
                        }
                    }
                    catch(Exception e)
                    {
                        pending_date_value = "";
                    }
                    %>
                    <input type='text' id="pending_date" name="pending_date" value="<%=pending_date_value%>" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                	</div>
			     </div>
            </div>
            <div class="col">
				<div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Closure Date
                    </label>
				</div>
                <div class="formField clr md border-0 px-0 input-group d-flex">
                    <%
                    String closed_date_value = "";
                    try
                    {
                        if(incident_info[27] == null || incident_info[27].equalsIgnoreCase("null") || incident_info[27].equalsIgnoreCase(""))
                        {
                            closed_date_value = "";
                        }
                        else
                        {
                            ZonedDateTime zdt_closed_date = support.date_utils.utc_to_user_tz(user_tz_name, incident_info[27]);
                            closed_date_value = zdt_closed_date.format(date_picker_formatter);   
                        }
                    }
                    catch(Exception e)
                    {
                        closed_date_value = "";
                    }
                    //String incident_time = date_time_picker_format.format(timestamp_format.parse(incident_info[2]).getTime());
                    %>
                    <input type='text' id="closed_date" name="closed_date" value="<%=closed_date_value%>" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                	</div>
			     </div>
            </div>
        </div>

        <div class="row mb-15">
            <div class="col-12">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="attachments-tab" data-toggle="tab" href="#attachments" role="tab" aria-controls="attachments" aria-selected="true">Attachments</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="problems-tab" data-toggle="tab" href="#problems" role="tab" aria-controls="problems" aria-selected="false">Associated Problems</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="changes-tab" data-toggle="tab" href="#changes" role="tab" aria-controls="changes" aria-selected="false">Associated Changes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="asset-tab" data-toggle="tab" href="#asset" role="tab" aria-controls="asset" aria-selected="false">Associated Asset</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="attachments" role="tabpanel" aria-labelledby="attachments-tab">

                <div class="row">
                    <div class="col-md-12">

                        <div class="field-title">
                            <label class="formLabel md has-txt-only mb-15">
                                Attachments
                            </label>
                        </div>
                        <div class="formField clr md border-0">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="file" name="file" multiple>
                                <label class="custom-file-label" for="file">Choose file(s)</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="formField clr md border-0">
                            <% 
                            ArrayList<String[]> attachments = new ArrayList();
                            attachments = db.get_incidents.get_attachments_by_incident_id(con, id);
                            for (int i = 0; i < attachments.size(); i++)
                            {
                                String[] attachment = attachments.get(i);
                            %>
                            <div class="clr tag dInlineBlock formField md whtbg mr-5 mb-15 w-auto float-none">
                                <span>
                                    <a title="Uploaded by <%=attachment[9]%> on <%=attachment[5]%>" href="javascript:void(0)" onclick="openAttachment();event.preventDefault" data-file-name="<%=attachment[4]%>" data-link="<%=attachment[3]%>"><%=attachment[4]%></a>
                                </span>
                                <a data-id="<%=attachment[0]%>" href="javascript:void(0)" onclick="deleteAttachment()"><img class="icon" src="assets/images/svg/cross-icon.svg" alt=""></a>
                            </div>                    
                            <%
                            }
                            %>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="problems" role="tabpanel" aria-labelledby="problems-tab">
                <div class="row">
                    <div class="col-12">
                        <div class="field-title">
                            <label class="formLabel md has-txt-only mb-15">
                                Associated Problems
                                <a class="btn btn-success customBtn lg waves-effect" href="problem_list.jsp?action=new&incident_id=<%=id%>">
                                    <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>New Problem
                                </a>
                            </label>
                        </div>
                        <div class="formField clr md px-0 border-0">
                            <input type="text" name="problem_lookup" id="problem_lookup" value="" class="form-control" placeholder="Type an ID or Description to search"/> 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="container-fluid">
                            <div class="sort-tbl-holder container-fluid">
                                <table class="table table-striped custom-sort-table rightPanelDataTable" cellspacing="0" width="100%" data-ordering='false' data-searching='false' data-length-change="false">
                                    <thead>
                                        <tr>
                                            <th data-data="id">Problem ID</th>
                                            <th data-data="priority">Priority</th>
                                            <th data-data="status">Status</th>
                                            <th data-data="final_status">Final Status</th>
                                            <th data-data="problem_time">Problem Time</th>
                                            <th data-data="name">Name</th>
                                            <th data-data="description">Description</th>
                                            <th data-data="incidents">Incidents</th>
                                            <th data-data="assigned_to_username">Assigned To</th>
                                            <th data-data="assigned_group_name">Assigned Group</th>
                                            <th data-data="actions">Unlink</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <%  ArrayList<String[]> problems = db.get_problems.problems_for_incident(con, id, null);
                                        ArrayList<String> problem_ids = new ArrayList<>();
                                        for(int a = 0; a < problems.size();a++)
                                        {
                                            String problem_record[] = problems.get(a);
                                            problem_ids.add("\"" + problem_record[0] + "\""); 
                                    %>
                                        <tr data-id="<%=problem_record[0]%>">
                                            <td><%=problem_record[0]%></td>                                            
                                            <td><%=problem_record[1]%></td>                                            
                                            <td><%=problem_record[6]%></td>                                            
                                            <td><%=problem_record[22]%></td>                                            
                                            <td><%=problem_record[4]%></td>                                            
                                            <td><%=problem_record[2]%></td>                                            
                                            <td><%=problem_record[3]%></td>                                            
                                            <td><%=problem_record[23]%></td>                                            
                                            <td><%=problem_record[12]%></td>                                            
                                            <td><%=problem_record[11]%></td>                                            
                                            <td><a href="javascript:void(0)" onclick="unlinkProblem('<%=problem_record[0]%>')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a></td>                                            
                                        </tr>
                                    <%  } %>
                                    </tbody>
                                </table>
                                    <input type="hidden" name="problem_ids" value='[<%=(problems != null ? StringUtils.join(problem_ids, ",") : "")%>]'>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="tab-pane fade show active" id="changes" role="tabpanel" aria-labelledby="changes-tab">

            </div>
            <div class="tab-pane fade show active" id="asset" role="tabpanel" aria-labelledby="asset-tab">
                <div class="row">
                    <div class="col-md-6">
                        <div class="field-title">
                            <label class="formLabel md has-txt-only mb-15">
                                Asset Tag
                            </label>
                        </div>
                        <div class="formField clr md px-0 border-0">
                            <input type="text" id="asset_tag" name="asset_tag" class="form-control" placeholder="" value="<%=asset_info[2]%>"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="field-title">
                            <label class="formLabel md has-txt-only mb-15">
                                Serial #
                            </label>
                        </div>
                        <div class="formField clr md px-0 border-0">
                            <input type="text" id="serial_number" name="serial_number" class="form-control" placeholder="Enter Serial #" value="<%=asset_info[1]%>"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="field-title">
                            <label class="formLabel md has-txt-only mb-15">
                                Manufacturer
                            </label>
                        </div>
                        <div class="formField clr md px-0 border-0">
                            <input type="text" id="manufacturer" name="manufacturer" class="form-control" placeholder="" value="<%=asset_info[3]%>"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="field-title">
                            <label class="formLabel md has-txt-only mb-15">
                                Model
                            </label>
                        </div>
                        <div class="formField clr md px-0 border-0">
                            <input type="text" id="model" name="model" class="form-control" placeholder="" value="<%=asset_info[4]%>"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="separator-new mb-30 mt-30"></div>                
              
        <div class="row"><!--Start Custom Fields-->  
            <%
            //get active custom incident fields
            ArrayList <String[]> custom_fields = db.get_custom_fields.active_for_form(con, "incident");
            ArrayList<String[]> db_values = db.get_custom_field_data.active_by_form(con, "incident",id);

            for(int a = 0; a < custom_fields.size(); a++)
            {
                String custom_field[] = custom_fields.get(a);
                String field_name = custom_field[3];
                String db_value = "";
                //find the saved value if any
                for(int b = 0; b < db_values.size(); b++)
                {
                    String temp[] = db_values.get(b);
                    if(temp[2].equalsIgnoreCase(field_name))
                    {
                        db_value = temp[3];
                    }
                }

                //String db_value = "Bill";
                if(custom_field[7].equalsIgnoreCase("select"))
                {
                    %>
                    <jsp:include page='build_custom_select_field.jsp'>
                        <jsp:param name="field_name" value="<%=field_name%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="new_record" value="false"/>
                        <jsp:param name="db_value" value="<%=db_value%>"/>                                                
                    </jsp:include>
                    <%   
                }
                else if(custom_field[7].equalsIgnoreCase("text"))
                {
                    %>
                    <jsp:include page='build_custom_text_field.jsp'>
                        <jsp:param name="field_name" value="<%=field_name%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="false"/>
                        <jsp:param name="db_value" value="<%=db_value%>"/>         
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("textarea"))
                {
                    %>
                    <jsp:include page='build_custom_textarea_field.jsp'>
                        <jsp:param name="field_name" value="<%=field_name%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="false"/>
                        <jsp:param name="db_value" value="<%=db_value%>"/>    
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("checkbox"))
                {
                    %>
                    <jsp:include page='build_custom_checkbox_field.jsp'>
                        <jsp:param name="field_name" value="<%=field_name%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="false"/>
                        <jsp:param name="db_value" value="<%=db_value%>"/>    
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("people-lookup"))
                {
                    String user_info[] = db.get_users.by_id(con, db_value);
                    String user_name = user_info[1];
                    %>
                    <jsp:include page='build_custom_people_field.jsp'>
                        <jsp:param name="field_name" value="<%=field_name%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="false"/>
                        <jsp:param name="db_value" value="<%=db_value%>"/>   
                        <jsp:param name="user_name" value="<%=user_name%>"/>   
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("group-lookup"))
                {
                    String group_info[] = db.get_groups.by_id(con, db_value);
                    String group_name = group_info[1];
                    %>
                    <jsp:include page='build_custom_group_field.jsp'>
                        <jsp:param name="field_name" value="<%=field_name%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="false"/>
                        <jsp:param name="db_value" value="<%=db_value%>"/>   
                        <jsp:param name="group_name" value="<%=group_name%>"/>   
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("date"))
                {
                    %>
                    <jsp:include page='build_custom_date_field.jsp'>
                        <jsp:param name="field_name" value="<%=field_name%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="false"/>
                        <jsp:param name="db_value" value="<%=db_value%>"/>   
                    </jsp:include>
                    <%
                }
            }
            %>    
        </div><!--End Custom Fields-->      

        <div class="separator-new mb-30 mt-30"></div>                
            
        <div class="row">
            <div class="col-md-9">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Description
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" name="description" id="description" value="<%=incident_info[15]%>" class="form-control"/> 
                </div>
            </div>
            <div class="col-md-3">
                <label class="formLabel md has-txt-only mb-15">
                    <a id="knowledge_button" data-target="KB" class="btn btn-success customBtn lg waves-effect rightSidePanelOpenerKB" href="javascript:void(0)">
                        Knowledge Base Search
                    </a>    
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Notes <em>(Customer viewable)</em>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <textarea name="notes" id="notes" cols="30" rows="5" class="ckeditor"><%=incident_info[23]%></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Notes <em>(Service Desk viewable)</em>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <textarea name="desk_notes" id="desk_notes" cols="30" rows="5" class="ckeditor"><%=incident_info[24]%></textarea>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-12 text-center pt-30 pb-30">
                <button type="button" class="btn btn-outline-primary customBtn lg waves-effect">Cancel</button>
                <button type="button" class="btn btn-primary-new customBtn lg waves-effect save-button" role="button">
                    Save
                </button>
            </div>
        </div>                                

    </form>

    <script>
        function validateForm() 
        {
            var error = "";
            var state = document.getElementById("state").value;
            var closed_date = document.getElementById("closed_date").value;
            var closed_reason = document.getElementById("closed_reason").value;
            var pending_date = document.getElementById("pending_date").value;
            var pending_reason = document.getElementById("pending_reason").value;
            
            if (state === "Closed" && closed_date === "") 
            {
                error = "If the Incident State is \"Closed\" then Closed Date and Closed Reason must be filled in";
            }
            else if (state !== "Closed" && closed_date !== "" )
            {
                error = "When setting the Closed Date the State must also be closed";
            }            
            if (state === "Pending" && pending_date === "") 
            {
                error = "If the Incident State is \"Pending\" then Pending Date and Pending Reason must be filled in";
            }    
            if(error !== "")
            {
                var errorMsg = "<p>" + error + "</p>"
                document.getElementById("modal_text").innerHTML = errorMsg
                $("#error_modal").modal("show");
                //alert(error);
                return false;
            }
            else
            {
                return true;
            }
        }
    </script>
    
    
<%
        }//end if not permission
    }//end if not logged in
%>
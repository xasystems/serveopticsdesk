<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_users_add.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@ page import="java.text.SimpleDateFormat"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        boolean authorized = support.role.authorized(session, "asset","read");   
        if(!authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //00:00 09/12/2020
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
            try
            {
                String home_page = session.getAttribute("home_page").toString();
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                ArrayList <String[]> assets = new ArrayList();
                String serial_number = null;
                String asset_tag = null;
                String manufacturer = null;
                String model = null;
                String asset_type = null;
                String asset_subtype = null;
                String purchase_order = null;
                String warranty_expire = null;
                String state = null;
                String state_date = null;
                String assigned_group_name = null;
                String assigned_group_id = null;    
                String assigned_to_id = null;
                String assigned_to_name = null;
                String department = null;
                String location = null;
                String site = null;
                String company = null;
                String ip = null;
                String notes = null;
                
                
                boolean a_parameter_was_send = false;
                boolean use_serial_number = false;
                boolean use_asset_tag = false;
                boolean use_manufacturer = false;
                boolean use_model = false;
                boolean use_asset_type = false;
                boolean use_asset_subtype = false;
                boolean use_purchase_order = false;
                boolean use_warranty_expire = false;
                boolean use_state = false;
                boolean use_state_date = false;
                boolean use_assigned_group_id = false;
                boolean use_assigned_to_id = false;
                boolean use_department = false;
                boolean use_location = false;
                boolean use_site = false;
                boolean use_company = false;
                boolean use_ip = false;
                boolean use_notes = false;
                
                //get parameters
                try
                {
                    serial_number = request.getParameter("serial_number");
                    if(serial_number != null && !serial_number.equalsIgnoreCase("Any"))
                    {
                        a_parameter_was_send = true;
                        use_serial_number = true;
                    }
                    else
                    {
                        serial_number = "Any";
                    }
                }
                catch(Exception e)
                {
                    serial_number = "Any";
                }
                try
                {
                    asset_tag = request.getParameter("asset_tag");
                    if(asset_tag != null && !asset_tag.equalsIgnoreCase("Any"))
                    {
                        a_parameter_was_send = true;
                        use_asset_tag = true;
                    }
                    else
                    {
                        asset_tag = "Any";
                    }
                }
                catch(Exception e)
                {
                    asset_tag = "Any";
                }
                try
                {
                    manufacturer = request.getParameter("manufacturer");
                    if(manufacturer != null && !manufacturer.equalsIgnoreCase("Any"))
                    {
                        a_parameter_was_send = true;
                        use_manufacturer = true;
                    }
                    else
                    {
                        manufacturer = "Any";
                    }
                }
                catch(Exception e)
                {
                    manufacturer = "Any";
                }    
                try
                {
                    model = request.getParameter("model");
                    if(model != null && !model.equalsIgnoreCase("Any"))
                    {
                        a_parameter_was_send = true;
                        use_model = true;
                    }
                    else
                    {
                        model = "Any";
                    }
                }
                catch(Exception e)
                {
                    model = "Any";
                }   
                try
                {
                    asset_type = request.getParameter("asset_type");
                    if(asset_type != null && !asset_type.equalsIgnoreCase("Any"))
                    {
                        a_parameter_was_send = true;
                        use_asset_type = true;
                    }
                    else
                    {
                        asset_type = "Any";
                    }
                }
                catch(Exception e)
                {
                    asset_type = "Any";
                }  
                try
                {
                    asset_subtype = request.getParameter("asset_subtype");
                    if(asset_subtype != null && !asset_subtype.equalsIgnoreCase("Any"))
                    {
                        a_parameter_was_send = true;
                        use_asset_subtype = true;
                    }
                    else
                    {
                        asset_subtype = "Any";
                    }
                }
                catch(Exception e)
                {
                    asset_subtype = "Any";
                } 
                try
                {
                    purchase_order = request.getParameter("purchase_order");
                    if(purchase_order != null && !purchase_order.equalsIgnoreCase("Any"))
                    {
                        a_parameter_was_send = true;
                        use_purchase_order = true;
                    }
                    else
                    {
                        purchase_order = "Any";
                    }
                }
                catch(Exception e)
                {
                    purchase_order = "Any";
                } 
                
                try 
                {
                    warranty_expire = request.getParameter("warranty_expire").trim();
                    if(warranty_expire != null && !warranty_expire.equalsIgnoreCase("Any"))
                    {
                        a_parameter_was_send = true;
                        use_warranty_expire = true;
                    }
                    else
                    {
                        warranty_expire = "Any";
                        use_warranty_expire = false;
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to ""
                    warranty_expire = "Any";
                }
                try 
                {
                    state = request.getParameter("state");
                    if(state != null && !state.equalsIgnoreCase("Any"))
                    {
                        a_parameter_was_send = true;
                        use_state = true;
                    }
                    else
                    {
                        state = "Any";
                        use_state = false;
                    }                    
                } 
                catch (Exception e) 
                {
                    //if not set then default to ""
                    state = "Any";
                }
                try 
                {
                    state_date = request.getParameter("state_date");
                    if(state_date != null && !state_date.equalsIgnoreCase("Any"))
                    {
                        a_parameter_was_send = true;
                        use_state_date = true;
                    }
                    else
                    {
                        state_date = "Any";
                        use_state_date = false;
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to ""
                    state_date = "Any";
                }
                try 
                {
                    assigned_group_id = request.getParameter("assigned_group_id");
                    if (assigned_group_id.equalsIgnoreCase("null") || assigned_group_id == null || assigned_group_id.equalsIgnoreCase("Any")) 
                    {
                        assigned_group_id = "Any";
                        assigned_group_name = "Any";
                        use_assigned_group_id = false;
                    }
                    else
                    {
                        use_assigned_group_id = true;
                        //get group info
                        String assigned_group_id_info[] = db.get_groups.by_id(con, assigned_group_id);
                        assigned_group_id = assigned_group_id;
                        assigned_group_name = assigned_group_id_info[1];
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to ""
                    assigned_group_id = "Any";
                    assigned_group_name = "Any";
                }
                try 
                {
                    assigned_to_id = request.getParameter("assigned_to_id");
                    if (assigned_to_id.equalsIgnoreCase("null") || assigned_to_id == null || assigned_to_id.equalsIgnoreCase("Any")) 
                    {
                        assigned_to_id = "Any";
                        assigned_to_name = "Any";
                        use_assigned_to_id = false;
                    }
                    else
                    {
                        use_assigned_to_id = true;
                        //get user info
                        String assigned_to_id_info[] = db.get_users.by_id(con, assigned_to_id);
                        assigned_to_id = assigned_to_id;
                        assigned_to_name = assigned_to_id_info[1];
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to ""
                    assigned_to_id = "Any";
                    assigned_to_name = "Any";
                }                            
                try 
                {
                    department = request.getParameter("department");
                    if(department != null && !department.equalsIgnoreCase("Any"))
                    {
                        a_parameter_was_send = true;
                        use_department = true;
                    }
                    else
                    {
                        department = "Any";
                        use_department = false;
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to ""
                    department = "Any";
                }
                try 
                {
                    location = request.getParameter("location");
                    if(location != null && !location.equalsIgnoreCase("Any"))
                    {
                        a_parameter_was_send = true;
                        use_location = true;
                    }
                    else
                    {
                        location = "Any";
                        use_location = false;
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to ""
                    location = "Any";
                }
                try 
                {
                    site = request.getParameter("site");
                    if(site != null && !site.equalsIgnoreCase("Any"))
                    {
                        a_parameter_was_send = true;
                        use_site = true;
                    }
                    else
                    {
                        site = "Any";
                        use_site = false;
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to ""
                    site = "Any";
                }
                try 
                {
                    company = request.getParameter("company");
                    if(company != null && !company.equalsIgnoreCase("Any"))
                    {
                        a_parameter_was_send = true;
                        use_company = true;
                    }
                    else
                    {
                        company = "Any";
                        use_company = false;
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to ""
                    company = "Any";
                }
                try 
                {
                    ip = request.getParameter("ip");
                    if(ip != null && !ip.equalsIgnoreCase("Any"))
                    {
                        a_parameter_was_send = true;
                        use_ip = true;
                    }
                    else
                    {
                        ip = "Any";
                        use_ip = false;
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to ""
                    ip = "Any";
                }
                try 
                {
                    notes = request.getParameter("notes");
                    if(notes != null && !notes.equalsIgnoreCase("Any"))
                    {
                        a_parameter_was_send = true;
                        use_notes = true;
                    }
                    else
                    {
                        notes = "Any";
                        use_notes = false;
                    }
                } 
                catch (Exception e) 
                {
                    //if not set then default to ""
                    notes = "Any";
                }
                
                
                //build the query
                String query = "";
                if(use_serial_number)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "serial_number LIKE '%" + serial_number + "%'";
                    }
                    else
                    {
                        query = query + " AND serial_number LIKE '%" + serial_number + "%'";
                    }
                }
                if(use_asset_tag)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "asset_tag LIKE '%" + asset_tag + "%'";
                    }
                    else
                    {
                        query = query + " AND asset_tag LIKE '%" + asset_tag + "%'";
                    }
                }
                if(use_manufacturer)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "manufacturer LIKE '%" + manufacturer + "%'";
                    }
                    else
                    {
                        query = query + " AND manufacturer LIKE '%" + manufacturer + "%'";
                    }
                }
                if(use_model)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "model LIKE '%" + model + "%'";
                    }
                    else
                    {
                        query = query + " AND model LIKE '%" + model + "%'";
                    }
                }
                if(use_asset_type)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "asset_type LIKE '%" + asset_type + "%'";
                    }
                    else
                    {
                        query = query + " AND asset_type LIKE '%" + asset_type + "%'";
                    }
                }
                if(use_asset_subtype)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "asset_subtype LIKE '%" + asset_subtype + "%'";
                    }
                    else
                    {
                        query = query + " AND asset_subtype LIKE '%" + asset_subtype + "%'";
                    }
                }
                if(use_purchase_order)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "purchase_order LIKE '%" + purchase_order + "%'";
                    }
                    else
                    {
                        query = query + " AND purchase_order LIKE '%" + purchase_order + "%'";
                    }
                }
                if(use_warranty_expire)
                {
                    try
                    {
                        String temp[] = warranty_expire.split("-");
                        java.util.Date start = date_time_picker_format.parse(temp[0].trim());
                        java.util.Date end = date_time_picker_format.parse(temp[1].trim());
                        String start_string = timestamp_format.format(start);
                        String end_string = timestamp_format.format(end);
                        if(query.equalsIgnoreCase(""))
                        {
                            query = "(warranty_expire >= '" + start_string + "' AND warranty_expire <= '" + end_string + "')";
                        }
                        else
                        {
                            query = query + " AND (warranty_expire >= '" + start_string + "' AND warranty_expire <= '" + end_string + "')";
                        }
                    }
                    catch(Exception e)
                    {
                        System.out.println("Exception on assets_search parse warranty_expire=" + e);
                    }
                }
                if(use_state)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "state LIKE '%" + state + "%'";
                    }
                    else
                    {
                        query = query + " AND state LIKE '%" + state + "%'";
                    }
                }   
                if(use_state_date)
                {
                    try
                    {
                        String temp[] = state_date.split("-");
                        java.util.Date start = date_time_picker_format.parse(temp[0].trim());
                        java.util.Date end = date_time_picker_format.parse(temp[1].trim());
                        String start_string = timestamp_format.format(start);
                        String end_string = timestamp_format.format(end);
                        if(query.equalsIgnoreCase(""))
                        {
                            query = "(state_date >= '" + start_string + "' AND state_date <= '" + end_string + "')";
                        }
                        else
                        {
                            query = query + " AND (state_date >= '" + start_string + "' AND state_date <= '" + end_string + "')";
                        }
                    }
                    catch(Exception e)
                    {
                        System.out.println("Exception on assets_search parse state_date=" + e);
                    }
                }
                if(use_assigned_group_id)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "assigned_group_id LIKE '%" + assigned_group_id + "%'";
                    }
                    else
                    {
                        query = query + " AND assigned_group_id LIKE '%" + assigned_group_id + "%'";
                    }
                }
                if(use_assigned_to_id)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "assigned_to_id LIKE '%" + assigned_to_id + "%'";
                    }
                    else
                    {
                        query = query + " AND assigned_to_id LIKE '%" + assigned_to_id + "%'";
                    }
                }  
                if(use_department)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "department LIKE '%" + department + "%'";
                    }
                    else
                    {
                        query = query + " AND department LIKE '%" + department + "%'";
                    }
                } 
                if(use_location)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "location LIKE '%" + location + "%'";
                    }
                    else
                    {
                        query = query + " AND location LIKE '%" + location + "%'";
                    }
                }         
                if(use_site)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "site LIKE '%" + site + "%'";
                    }
                    else
                    {
                        query = query + " AND site LIKE '%" + site + "%'";
                    }
                }       
                if(use_company)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "company LIKE '%" + company + "%'";
                    }
                    else
                    {
                        query = query + " AND company LIKE '%" + company + "%'";
                    }
                }
                if(use_ip)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "ip LIKE '%" + ip + "%'";
                    }
                    else
                    {
                        query = query + " AND ip LIKE '%" + ip + "%'";
                    }
                }
                if(use_notes)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "notes LIKE '%" + notes + "%'";
                    }
                    else
                    {
                        query = query + " AND notes LIKE '%" + notes + "%'";
                    }
                }      
                
                //assemble the query
                String q_string = "";
                if(a_parameter_was_send)
                {
                    q_string = "SELECT * FROM assets WHERE " + query;
                }
                else
                {
                    q_string = "SELECT * FROM assets";
                }
                PreparedStatement stmt;
                stmt = con.prepareStatement(q_string);
                ResultSet rs = stmt.executeQuery();
                while(rs.next())
                {
                    String this_record[] = new String[19];
                    this_record[0] = rs.getString("id");
                    this_record[1] = rs.getString("serial_number");
                    this_record[2] = rs.getString("asset_tag");
                    this_record[3] = rs.getString("manufacturer");
                    this_record[4] = rs.getString("model");
                    this_record[5] = rs.getString("asset_type");
                    this_record[6] = rs.getString("asset_subtype");
                    this_record[7] = rs.getString("purchase_order");
                    this_record[8] = rs.getString("warranty_expire");
                    this_record[9] = rs.getString("state");
                    this_record[10] = rs.getString("state_date");
                    this_record[11] = rs.getString("assigned_to_id");
                    this_record[12] = rs.getString("assigned_group_id");
                    this_record[13] = rs.getString("department");
                    this_record[14] = rs.getString("location");
                    this_record[15] = rs.getString("site");
                    this_record[16] = rs.getString("company");
                    this_record[17] = rs.getString("ip");
                    this_record[18] = rs.getString("notes");                    
                    assets.add(this_record);
                }
                
%>
<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>	

<jsp:include page='menu_service_desk.jsp'>
    <jsp:param name="active_menu" value=""/>
</jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!--Start Page level css-->
    <link rel="stylesheet" type="text/css" href="assets/css/dashboard.css">
    <!--End Page level css--><!-- END Custom CSS-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- start content here-->
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">Assets</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a></li>
                            <li class="breadcrumb-item active"><a href="assets.jsp">Assets</a></li>
                            <li class="breadcrumb-item active"><a href="#">Asset Search</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div> <!--End breadcrumbs -->
        
        <form action="assets_search.jsp" method="post"> 
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">
                                Filter Selector
                                <button type="submit" class="btn btn-blue mr-1">
                                    <i class="fa-filter"></i> Apply Filters
                                </button>
                                <button type="button" id="t_button" onclick="hide_show_div('selectors');" class="btn btn-blue mr-1">
                                    <i class="fa-filter"></i> Show/Hide Filters
                                </button>
                            </h4>
                        </div>
                        <div class="card-content" id="selectors">
                            <div class="card-body">
                                <ul class="nav nav-tabs nav-underline nav-justified">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="attributes_tab" data-toggle="tab" href="#tab1" aria-controls="activeIcon12" aria-expanded="true"><i class="ft-calendar"></i>Filter by Asset Attributes</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="purchase_warranty_state_tab" data-toggle="tab" href="#tab2" aria-controls="activeIcon12" aria-expanded="false"><i class="ft-users"></i>Filter by Purchase/Warranty/State</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="location_tab" data-toggle="tab" href="#tab3" aria-controls="linkIcon12" aria-expanded="false"><i class="ft-book"></i>Filter by Location</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="notes_log_tab" data-toggle="tab" href="#tab4" aria-controls="linkIconOpt11"><i class="ft-filter"></i>Filter by Notes/Log</a>
                                    </li>
                                </ul>


                                <div class="tab-content px-1 pt-1">
                                    <div role="tabpanel" class="tab-pane active" id="tab1" aria-labelledby="attributes_tab" aria-expanded="true">
                                        <div class="row">                                            
                                           <div class="col-xl-2 col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label for="serial_number">Serial #</label>
                                                    <input type="text" id="serial_number" name="serial_number" class="form-control" value="<%=serial_number%>"/>
                                                </div>
                                            </div>
                                            <div class="col-xl-2 col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label for="asset_tag">Asset Tag</label>
                                                    <input type="text" id="asset_tag" name="asset_tag" class="form-control" value="<%=asset_tag%>"/>
                                                </div>
                                            </div>
                                            <div class="col-xl-2 col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label for="vendor">Manufacturer</label>
                                                    <input type="text" id="manufacturer" name="manufacturer" class="form-control" value="<%=manufacturer%>"/>
                                                </div>
                                            </div>
                                            <div class="col-xl-2 col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label for="model">Model</label>
                                                    <input type="text" id="model" name="model" class="form-control" value="<%=model%>"/>
                                                </div>
                                            </div>
                                            <div class="col-xl-2 col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label for="asset_type">Asset Type</label>
                                                    <input type="text" id="asset_type" name="asset_type" class="form-control" value="<%=asset_type%>"/>
                                                </div>
                                            </div>
                                            <div class="col-xl-2 col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label for="asset_subtype">Asset Subtype</label>
                                                    <input type="text" id="asset_subtype" name="asset_subtype" class="form-control" value="<%=asset_subtype%>"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab2" role="tabpanel" aria-labelledby="purchase_warranty_state_tab" aria-expanded="false">
                                        <div class="row">
                                            <div class="col-xl-1 col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label for="purchase_order">P.O.</label>
                                                    <input type="text" id="purchase_order" name="purchase_order" value="<%=purchase_order%>" class="form-control"/>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-6 col-12">
                                                <label for="warranty_expire">Warranty Expire</label>
                                                <div class='input-group'>                                                
                                                    <input type='text' id="warranty_expire" name="warranty_expire" value="<%=warranty_expire%>" class="form-control datetime" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <span class="la la-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-1 col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label for="state">State</label>
                                                    <select class="form-control" name="state" id="state">
                                                        <%
                                                        String values[] = {"Any","Ordered","New","Delivered","Active","Spare","Broken","Retired"};
                                                        String selected = "";
                                                        for(int a = 0; a < values.length; a++)
                                                        {
                                                            selected = "";
                                                            if(state.equalsIgnoreCase(values[a]))
                                                            {
                                                                selected = "SELECTED";
                                                            }
                                                            %>
                                                            <option <%=selected%> value="<%=values[a]%>"><%=values[a]%></option>
                                                            <%
                                                        }
                                                        %>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-6 col-12">
                                                <label>State Date</label>
                                                <div class='input-group'>
                                                    <input type='text' id="state_date" name="state_date" value="<%=state_date%>" class="form-control datetime" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <span class="la la-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-xl-2 col-lg-6 col-12">
                                                <label>Assigned to Group&nbsp;<i class="la la-search" style="font-size: 14px;"></i></label>
                                                <input type="hidden" name="assigned_group_id" id="assigned_group_id" value="<%=assigned_group_id%>"/>
                                                <input type="text" name="assigned_group_name" id="assigned_group_name" value="<%=assigned_group_name%>" class="form-control" />
                                            </div>
                                            <div class="col-xl-2 col-lg-6 col-12">
                                                <label>
                                                    Assigned to&nbsp;
                                                    <i class="la la-search" style="font-size: 14px;"></i>
                                                </label>
                                                <input type="hidden" name="assigned_to_id" id="assigned_to_id" value="<%=assigned_to_id%>"/>
                                                <input type="text" name="assigned_to_name" id="assigned_to_name" value="<%=assigned_to_name%>" class="form-control" placeholder="Enter the Assigned To username"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab3" role="tabpanel" aria-labelledby="location_tab" aria-expanded="false">
                                        <div class="row">
                                            <div class="col-xl-2 col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label for="department">Department</label>
                                                    <input type="text" id="department" name="department" class="form-control" value="<%=department%>"/>
                                                </div>
                                            </div>
                                            <div class="col-xl-2 col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label for="location">Location</label>
                                                    <input type="text" id="location" name="location" class="form-control" value="<%=location%>"/>
                                                </div>
                                            </div>
                                            <div class="col-xl-2 col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label for="site">Site</label>
                                                    <input type="text" id="site" name="site" class="form-control" value="<%=site%>"/>
                                                </div>
                                            </div>
                                            <div class="col-xl-2 col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label for="company">Company</label>
                                                    <input type="text" id="company" name="company" class="form-control" value="<%=company%>"/>
                                                </div>
                                            </div>
                                            <div class="col-xl-2 col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label for="ip">IP</label>
                                                    <input type="text" id="ip" name="ip" class="form-control" value="<%=ip%>"/>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    &nbsp;
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab4" role="tabpanel" aria-labelledby="notes_log_tab" aria-expanded="false">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="notes">Notes</label>
                                                    <textarea rows="1" class="form-control" id="notes" name="notes"><%=notes%></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </form>
        
        
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>                        
                    </div>
                    <form action="assets_search.jsp" method="post"> 
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">                                
                                <h4>
                                    Search Results
                                </h4>
                                <table class="table table-striped table-bordered zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>Serial#</th>
                                            <th>Asset Tag</th>
                                            <th>Manufacturer</th>
                                            <th>Model</th>
                                            <th>Type</th>
                                            <th>SubType</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                        for(int a = 0; a < assets.size();a++)
                                        {
                                            String asset_record[] = assets.get(a);
                                            //id=0 name=1 description=2 created=3 created_by_id=4 administration=5 manager=6 financial=7 incident=8 request=9 contact=10 cx=11 survey=12 sla=13 project=14 job=15 
                                            %>
                                            <tr>
                                                <td><i class="la la-edit info" onclick="javascript:location.href='assets_edit.jsp?id=<%=asset_record[0]%>'" style="cursor: pointer;"></i><i class="la la-remove danger" onclick="javascript:location.href='assets_delete.jsp?id=<%=asset_record[0]%>'" style="cursor: pointer;"></i><%=asset_record[1]%></td>
                                                <td><%=support.data_check.null_string(asset_record[2])%></td>
                                                <td><%=support.data_check.null_string(asset_record[3])%></td>
                                                <td><%=support.data_check.null_string(asset_record[4])%></td>
                                                <td><%=support.data_check.null_string(asset_record[5])%></td>
                                                <td><%=support.data_check.null_string(asset_record[6])%></td>
                                            </tr>
                                            <%
                                        }
                                        %>
                                    </tbody>
                                    <%
                                    if(assets.size() >= 10)
                                    {
                                    %>
                                    <tfoot>
                                        <tr>
                                            <th>Serial#</th>
                                            <th>Asset Tag</th>
                                            <th>Vendor</th>
                                            <th>Model</th>
                                            <th>Type</th>
                                            <th>SubType</th>
                                        </tr>
                                    </tfoot>
                                    <%
                                    }                                    
                                    %>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end content here-->
    </div>        
</div>
<jsp:include page='footer.jsp'>
    <jsp:param name="parameter" value=""/>
</jsp:include>	
<!-- BEGIN PAGE LEVEL JS-->
<script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN MODERN JS-->
<script src="app-assets/js/core/app-menu.js"></script>
<script src="app-assets/js/core/app.js"></script>
<!-- END MODERN JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="app-assets/js/scripts/popover/popover.js"></script>
<!--This is for date range picker-->

<script>
    function hide_show_div() 
    {
        var x = document.getElementById("selectors");
        if (x.style.display === "none") 
        {
            x.style.display = "block";
        } 
        else 
        {
            x.style.display = "none";
        }
    }        

</script>          
<script>
    $(function ()
    {     
    //serial_number
    $("#serial_number").autocomplete(
    {
            source: function (request, response)
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_serial_number",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data:
                            {
                                search: request.term
                            },
                    success: function (data)
                    {
                        response(data);
                    }
                });
            },
            select: function (event, ui)
            {
                // Set selection
                $('#serial_number').val(ui.item.label); // display the selected text
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });
    //asset_tag
    $("#asset_tag").autocomplete(
    {
        source: function (request, response)
        {
            // Fetch data
            $.ajax({
                url: "ajax_lookup_asset_tag",
                type: 'post',
                dataType: "json",
                minLength: 2,
                data:
                        {
                            search: request.term
                        },
                success: function (data)
                {
                    response(data);
                }
            });
        },
        select: function (event, ui)
        {
            // Set selection
            $('#asset_tag').val(ui.item.label); // display the selected text
            //alert("setting is=" + ui.item.value);
            return false;
        }
    });
    //manufacturer
    $("#manufacturer").autocomplete(
    {
        source: function (request, response)
        {
            // Fetch data
            $.ajax({
                url: "ajax_lookup_manufacturer",
                type: 'post',
                dataType: "json",
                minLength: 2,
                data:
                {
                    search: request.term
                },
                success: function (data)
                {
                    response(data);
                }
            });
        },
        select: function (event, ui)
        {
            // Set selection
            $('#manufacturer').val(ui.item.label); // set name
            return false;
        }
    });
    //model
    $("#model").autocomplete(
    {
        source: function (request, response)
        {
            // Fetch data
            $.ajax({
                url: "ajax_lookup_model",
                type: 'post',
                dataType: "json",
                minLength: 2,
                data:
                {
                    search: request.term
                },
                success: function (data)
                {
                    response(data);
                }
            });
        },
        select: function (event, ui)
        {
            // Set selection
            $('#model').val(ui.item.label); // display the selected text
            return false;
        }
    });
    //asset_type
    $("#asset_type").autocomplete(
    {
        source: function (request, response)
        {
            // Fetch data
            $.ajax({
                url: "ajax_lookup_asset_type",
                type: 'post',
                dataType: "json",
                minLength: 2,
                data:
                {
                    search: request.term
                },
                success: function (data)
                {
                    response(data);
                }
            });
        },
        select: function (event, ui)
        {
            // Set selection
            $('#asset_type').val(ui.item.label); // display the selected text
            return false;
        }
    });
    //asset_subtype
    $("#asset_subtype").autocomplete(
    {
        source: function (request, response)
        {
            // Fetch data
            $.ajax({
                url: "ajax_lookup_asset_subtype",
                type: 'post',
                dataType: "json",
                minLength: 2,
                data:
                {
                    search: request.term
                },
                success: function (data)
                {
                    response(data);
                }
            });
        },
        select: function (event, ui)
        {
            // Set selection
            $('#asset_subtype').val(ui.item.label); // display the selected text
            return false;
        }
    });
    //purchase_order
    $("#purchase_order").autocomplete(
    {
        source: function (request, response)
        {
            // Fetch data
            $.ajax({
                url: "ajax_lookup_asset_purchase_order",
                type: 'post',
                dataType: "json",
                minLength: 2,
                data:
                {
                    search: request.term
                },
                success: function (data)
                {
                    response(data);
                }
            });
        },
        select: function (event, ui)
        {
            // Set selection
            $('#purchase_order').val(ui.item.label); // display the selected text
            return false;
        }
    });
    //assigned_group
    $( "#assigned_group_name" ).autocomplete(
    {
        source: function( request, response ) 
        {
            // Fetch data
            $.ajax({
                url: "ajax_lookup_group",
                type: 'post',
                dataType: "json",
                minLength: 2,
                data: 
                {
                    search: request.term
                },
                success: function( data ) 
                {
                    response( data );
                }
            });
        },
        select: function (event, ui) 
        {
            // Set selection
            $('#assigned_group_name').val(ui.item.label); // set name
            $('#assigned_group_id').val(ui.item.value); // set id
            return false;
        }
    });
    //department
    $("#department").autocomplete(
    {
        source: function (request, response)
        {
            // Fetch data
            $.ajax({
                url: "ajax_lookup_asset_department",
                type: 'post',
                dataType: "json",
                minLength: 2,
                data:
                {
                    search: request.term
                },
                success: function (data)
                {
                    response(data);
                }
            });
        },
        select: function (event, ui)
        {
            // Set selection
            $('#department').val(ui.item.label); // display the selected text
            return false;
        }
    });
    //location
    $("#location").autocomplete(
    {
        source: function (request, response)
        {
            // Fetch data
            $.ajax({
                url: "ajax_lookup_asset_location",
                type: 'post',
                dataType: "json",
                minLength: 2,
                data:
                {
                    search: request.term
                },
                success: function (data)
                {
                    response(data);
                }
            });
        },
        select: function (event, ui)
        {
            // Set selection
            $('#location').val(ui.item.label); // display the selected text
            return false;
        }
    });
    //site
    $("#site").autocomplete(
    {
        source: function (request, response)
        {
            // Fetch data
            $.ajax({
                url: "ajax_lookup_asset_site",
                type: 'post',
                dataType: "json",
                minLength: 2,
                data:
                {
                    search: request.term
                },
                success: function (data)
                {
                    response(data);
                }
            });
        },
        select: function (event, ui)
        {
            // Set selection
            $('#site').val(ui.item.label); // display the selected text
            return false;
        }
    });
    //company
    $("#company").autocomplete(
    {
        source: function (request, response)
        {
            // Fetch data
            $.ajax({
                url: "ajax_lookup_asset_company",
                type: 'post',
                dataType: "json",
                minLength: 2,
                data:
                {
                    search: request.term
                },
                success: function (data)
                {
                    response(data);
                }
            });
        },
        select: function (event, ui)
        {
            // Set selection
            $('#company').val(ui.item.label); // display the selected text
            return false;
        }
    });
});
</script>
<script>
        $(function() 
        {
          $('input[name="warranty_expire"]').daterangepicker({
                "autoUpdateInput": false,
                "singleDatePicker": false,
                "showDropdowns": true,
                "timePicker": true,
                "timePicker24Hour": true,
                "locale": {
                    "format": "HH:mm MM/DD/YYYY",
                    "applyLabel": "Apply",
                    "cancelLabel": "Cancel",

                    "weekLabel": "W",
                    "daysOfWeek": [
                        "Su",
                        "Mo",
                        "Tu",
                        "We",
                        "Th",
                        "Fr",
                        "Sa"
                    ],
                    "monthNames": [
                        "January",
                        "February",
                        "March",
                        "April",
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",
                        "December"
                    ],
                    "firstDay": 1
              }
          });

          $('input[name="warranty_expire"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY') + ' - ' + picker.endDate.format('HH:mm MM/DD/YYYY'));
          });

          $('input[name="warranty_expire"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });
          //state_date
          $('input[name="state_date"]').daterangepicker({
                "autoUpdateInput": false,
                "singleDatePicker": false,
                "showDropdowns": true,
                "timePicker": true,
                "timePicker24Hour": true,
                "locale": {
                    "format": "HH:mm MM/DD/YYYY",
                    "applyLabel": "Apply",
                    "cancelLabel": "Cancel",

                    "weekLabel": "W",
                    "daysOfWeek": [
                        "Su",
                        "Mo",
                        "Tu",
                        "We",
                        "Th",
                        "Fr",
                        "Sa"
                    ],
                    "monthNames": [
                        "January",
                        "February",
                        "March",
                        "April",
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",
                        "December"
                    ],
                    "firstDay": 1
              }
          });

          $('input[name="state_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY') + ' - ' + picker.endDate.format('HH:mm MM/DD/YYYY'));
          });

          $('input[name="state_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });
        });
    </script>

<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="app-assets/vendors/js/forms/select/select2.full.min.js"></script>
<script src="app-assets/js/scripts/forms/select/form-select2.js"></script>
<!-- END PAGE LEVEL JS-->   
<%                con.close();
    
            } catch (Exception e) {
                System.out.println("Exception in admin_user_add.jsp: " + e);
                logger.debug("ERROR: Exception in admin_user_add.jsp:" + e);
            }
        } //end if not admin permission
    } //end if not redirected
%>
</body>
</html>

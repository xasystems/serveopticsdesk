<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        if (!session.getAttribute("administration").toString().equalsIgnoreCase("true") && !session.getAttribute("manager").toString().equalsIgnoreCase("true")) 
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String all_schedules_info[][] = db.get_schedules.all(con);
        
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <!-- END PAGE LEVEL CSS-->

    
    <div class="clr whtbg p-10 mb-15">
        <div class="float-right options">
            <ul>
                <li class="dInlineBlock dropdown ">
                    <div class="dInlineBlock clr menuHolder">
                        <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                        </a>
                        <div class="dropdown-menu dropdown-menu-right md p-10">
                            <h5 class="medium-font boldFont pt-5 mb-10">All Filters</h5>
                            <form>
                                <ul>
                                    <li class="mb-10 clr">
                                        <div class="formField md  mr-0 full-width">
                                            <select class="no-border">
                                                <option>Select Filters by Dates</option>
                                                <option>Select Filters by Dates</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Incident Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Create Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>

                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Pending Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>

                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">State Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>

                                    </li>
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Close Start and End Date </label>
                                            <div class="formField md full-width mr-0">
                                                <input class="no-border full-width" type="text" name="daterange" value="01/01/2015 - 01/31/2015">
                                            </div>
                                        </div>

                                    </li>
                                </ul>
                                <div class="text-center pt-5 pb-5">
                                    <button class="btn md2 btn-primary-new customBtn mb-5">Apply Filters</button>
                                    <div class="clr"></div>
                                    <a href="#" class="">Reset Filters</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </li>
                <li class="dInlineBlock">
                    <span class="btn btn-success customBtn lg waves-effect rightSidePanelOpener">
                        <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>Add Schedule
                    </span>    
                </li>
            </ul>
        </div>
        <h1 class="large-font boldFont">Schedules</h1>
    </div>    

    <div class="sort-tbl-holder pb-100">
        <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width=" ">Name</th>
                    <th width=" ">Description</th>
                    <th width=" " align="center">Days of the Week</th>
                    <th width=" ">Start Time</th>
                    <th width=" ">End Time</th>
                    <th width=" ">All Day</th>
                    <th width=" ">Include Holidays</th>
                    <th width=" " class="text-center">Actions</th>
                </tr>
            </thead>

            <tbody>
                    <%
                    String dow = "";
                    //7=sun
                    for(int a = 0; a < all_schedules_info.length;a++)
                    {
                        dow = "";
                        if(all_schedules_info[a][7].equalsIgnoreCase("1"))
                        {
                            dow = dow + "Sun ";
                        }
                        if(all_schedules_info[a][8].equalsIgnoreCase("1"))
                        {
                            dow = dow + "Mon ";
                        }
                        if(all_schedules_info[a][9].equalsIgnoreCase("1"))
                        {
                            dow = dow + "Tue ";
                        }
                        if(all_schedules_info[a][10].equalsIgnoreCase("1"))
                        {
                            dow = dow + "Wed ";
                        }
                        if(all_schedules_info[a][11].equalsIgnoreCase("1"))
                        {
                            dow = dow + "Thu ";
                        }
                        if(all_schedules_info[a][12].equalsIgnoreCase("1"))
                        {
                            dow = dow + "Fri ";
                        }
                        if(all_schedules_info[a][13].equalsIgnoreCase("1"))
                        {
                            dow = dow + "Sat ";
                        }
                        %>
                        <tr>
                            <td><%=all_schedules_info[a][1]%></td>
                            <td><%=all_schedules_info[a][2]%></td>
                            <td align="center"><%=dow%></td>
                            <td><%=all_schedules_info[a][3]%></td>
                            <td><%=all_schedules_info[a][4]%></td>
                            <td>
                                <%
                                if(all_schedules_info[a][5].equalsIgnoreCase("1"))
                                {
                                    out.print("True");
                                }
                                else
                                {
                                    out.print("False");
                                }
                                %>
                            </td>
                            <td>
                                <%
                                if(all_schedules_info[a][6].equalsIgnoreCase("1"))
                                {
                                    out.print("True");
                                }
                                else
                                {
                                    out.print("False");
                                }
                                %>
                            </td>
                            <td class="text-center">
                                <a href="javascript:void(0)" onclick="editSchedule(<%=all_schedules_info[a][0]%>);" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>
                                <a href="javascript:void(0)" onclick="showDeleteModal(<%=all_schedules_info[a][0]%>)" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>

                            </td>
                        </tr>
                        <%
                    }
                    %>
                </tbody>
                <%
                if(all_schedules_info.length > 10)
                {
                    %>
                    <tfoot>
                        <tr>
                            <th width=" ">Name</th>
                            <th width=" ">Description</th>
                            <th width=" " align="center">Days of the Week</th>
                            <th width=" ">Start Time</th>
                            <th width=" ">End Time</th>
                            <th width=" ">All Day</th>
                            <th width=" ">Include Holidays</th>
                            <th width=" " class="text-center">Actions</th>
                        </tr>
                    </tfoot>
                    <%
                }
                %>
            </tbody>
        </table>
    </div>
         
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>

    <div class="rightPanelHolder">
        <div class="p-15 ticket-content">
            <span class="rightSidePanelCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
            <h4 id="right_panel_title" class="mediumFont large-font mb-20">Add Schedule</h4>

            <div class="clr whtbg p-15 ">
                <form name="add_form" id="add_form" action="admin_schedules_add" class="add-project-form needs-validation" novalidate>  
                    <input type="hidden" name="schedule_id" value=""/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Name
                                </label>
                            </div>
                            <div class="formField clr md px-0 border-0 ">
                                <input type="text" name="name" value="" class="form-control" placeholder="Schedule Name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md mb-15">
                                    Description
                                </label>
                            </div>
                            <div class="formField clr md px-0 border-0">
                                <input type="text" name="description" value="" class="form-control" placeholder="Description">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    All Day?
                                </label>
                            </div>
                            <div class="formField clr md px-0 border-0  no-border">
                                <div class="custom-control custom-checkbox dInlineBlock mr-15">
                                    <input name="is_all_day" type="checkbox" class="custom-control-input" id="listingCheck1">
                                    <label class="custom-control-label small-font" for="listingCheck1"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Include Holidays?
                                </label>
                            </div>
                            <div class="formField clr md px-0 border-0  no-border">
                                <div class="custom-control custom-checkbox dInlineBlock mr-15">
                                    <input type="checkbox" name="include_holidays" class="custom-control-input" id="listingCheck2">
                                    <label class="custom-control-label small-font" for="listingCheck2"></label>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Start Time
                                </label>
                            </div>
                            <div class="formField clr md px-0 border-0 ">
                                <input type="text" name="start_time" value="" class="form-control" placeholder="HH:mm:ss  24 Hour Clock">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    End Time
                                </label>
                            </div>
                            <div class="formField clr md px-0 border-0 ">
                                <input type="text" name="end_time" value="" class="form-control" placeholder="HH:mm:ss  24 Hour Clock">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="field-title">
                                <label class="formLabel md has-txt-only mb-15">
                                    Days of Week
                                </label>
                            </div>
                            <div class="formField clr md px-0 border-0 no-border">
                                <div class="custom-control custom-checkbox dInlineBlock mr-15">
                                    <input type="checkbox" name="sunday" class="custom-control-input" id="listingCheck3">
                                    <label class="custom-control-label small-font" for="listingCheck3">Sun</label>
                                </div>
                                <div class="custom-control custom-checkbox dInlineBlock mr-15">
                                    <input type="checkbox" name="monday" class="custom-control-input" id="listingCheck4">
                                    <label class="custom-control-label small-font" for="listingCheck4">Mon</label>
                                </div>
                                <div class="custom-control custom-checkbox dInlineBlock mr-15">
                                    <input type="checkbox" name="tuesday" class="custom-control-input" id="listingCheck5">
                                    <label class="custom-control-label small-font" for="listingCheck5">Tue</label>
                                </div>
                                <div class="custom-control custom-checkbox dInlineBlock mr-15">
                                    <input type="checkbox" name="wednesday" class="custom-control-input" id="listingCheck6">
                                    <label class="custom-control-label small-font" for="listingCheck6">Wed</label>
                                </div>
                                <div class="custom-control custom-checkbox dInlineBlock mr-15">
                                    <input type="checkbox" name="thursday" class="custom-control-input" id="listingCheck7">
                                    <label class="custom-control-label small-font" for="listingCheck7">Thr</label>
                                </div>
                                <div class="custom-control custom-checkbox dInlineBlock mr-15">
                                    <input type="checkbox" name="friday" class="custom-control-input" id="listingCheck8">
                                    <label class="custom-control-label small-font" for="listingCheck8">Fri</label>
                                </div>
                                <div class="custom-control custom-checkbox dInlineBlock mr-15">
                                    <input type="checkbox" name="saturday" class="custom-control-input" id="listingCheck9">
                                    <label class="custom-control-label small-font" for="listingCheck9">Sun</label>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="row ">
                        <div class="col-md-12 text-center pt-30 pb-30">
                            <button type="button" class="btn btn-outline-primary customBtn lg waves-effect  rightSidePanelCloser">Cancel</button>
                            <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                                Submit
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>

    </div>
    // delete confirmation modal
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="deleteModalLabel">Schedule deletion</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            Are you sure you want to delete this schedule?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-danger" onclick="deleteSchedule()">Delete</button>
          </div>
        </div>
      </div>
    </div>         
            
    <!-- BEGIN PAGE LEVEL JS-->
    <script type="text/javascript">
    
    var delete_schedule_id = -1;
    
    function editSchedule(schedule_id) 
    {
        $("form#add_form").get(0).classList.remove('was-validated');
        document.getElementById('add_form').reset();

        $.ajax({
            url: "ajax_lookup_schedule_data",
            type: 'post',
            dataType: "json",
            data: 
            {
                id: schedule_id
            },
            error: function(jqXHR, textStatus) 
            {
                switch (jqXHR.status) {
                    case 401:
                        window.location.assign("<%=props.get("login_url").toString()%>");
                        break;
                    case 403:
                        alert("You don't have rights enough!");
                        break;
                    default:
                        alert("Error processing request!");
                }
            },
            success: function( data ) 
            {
                console.log(data);
                if (Object.keys(data).length > 0) {
                    $('.rightPanelHolder').addClass('open');
                    $('.rightSidePanel').addClass('open');
                    $('html').addClass('panelOpen');
                    $('#right_panel_title').html('Edit Schedule');

                    document.getElementsByName("schedule_id")[0].value=data[0];
                    document.querySelectorAll('input[name="name"]')[0].value = data[1];
                    document.querySelectorAll('input[name="description"]')[0].value = data[2];
                    document.getElementsByName("start_time")[0].value = data[3];
                    document.getElementsByName("end_time")[0].value = data[4];
                    document.getElementsByName("is_all_day")[0].checked = (data[5] === "1");
                    document.getElementsByName("include_holidays")[0].checked = (data[6] === "1");
                    document.getElementsByName("sunday")[0].checked = (data[7] === "1");
                    document.getElementsByName("monday")[0].checked = (data[8] === "1");
                    document.getElementsByName("tuesday")[0].checked = (data[9] === "1");
                    document.getElementsByName("wednesday")[0].checked = (data[10] === "1");
                    document.getElementsByName("thursday")[0].checked = (data[11] === "1");
                    document.getElementsByName("friday")[0].checked = (data[12] === "1");
                    document.getElementsByName("saturday")[0].checked = (data[13] === "1");

//                        console.log(data[0]);

                } else {
                    alert("Error processing request!");
                }
            }
        });            
    }
    
    function showDeleteModal(schedule_id) 
    {
        delete_schedule_id = schedule_id;
        $('#deleteModal').modal();
    }    

    function deleteSchedule() 
    {
        window.location.assign("admin_schedules_delete?id="+delete_schedule_id);
    }    
    
    window.addEventListener("load", (event) => 
    {
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        
        var validation = Array.prototype.filter.call(forms, function(form) 
        {
            
            form.addEventListener('submit', function(event) 
            {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                    $('.rightPanelHolder').animate({scrollTop: $('.form-control:invalid').first().offset().top - 50}, 'fast');
                }
                form.classList.add('was-validated');
                var schedule_id = document.getElementsByName("schedule_id")[0].value;
                if (schedule_id !== "") 
                {
                    form.action = "admin_schedule_edit";
                    form.method = "POST";
                } else {
                    form.action = "admin_schedule_add";
                    form.method = "POST";
                }
            }, false);
        });            
        
        $('.rightSidePanelOpener').on('click', function(){
            $('#right_panel_title').html('Add schedule');
            $("form#add_form").get(0).classList.remove('was-validated');
            document.getElementById('add_form').reset();
        });

    });
    </script>
    <!-- END PAGE LEVEL JS-->   
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_schedules.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_schedules.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

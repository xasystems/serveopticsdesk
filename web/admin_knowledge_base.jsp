<%@page import="java.util.List"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_knowledge_base.jsp
    Created on : Oct 4 2020, 6:13:59 PM
    Author     : rcampbell
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        boolean page_authorized = false;
        if(ADMINISTRATION.equalsIgnoreCase("true") || MANAGER.equalsIgnoreCase("true"))
        {
            page_authorized = true;
        }
        
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
                SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / MM/dd/yyyy");//18:31 / 14-Jan-19
                SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
                java.util.Date now = new java.util.Date();
                    
            String knowledge_base_id =  null; 
            String article_name = null;
            String article_description = null;
            String status = null;
            String article_text = null;
            String keywords = null;
            String created_by_id = null;
            String created_by_name= null;
            String created_date_start = null; 
            String created_date_end = null; 
            String approved_by_id = null;
            String approved_by_name = null;
            String approved_date_start = null; 
            String approved_date_end = null; 
            String valid_till_start = null;
            String valid_till_end = null;
            
            boolean a_parameter_was_send = false;
            boolean use_knowledge_base_id = false;
            boolean use_article_name = false;
            boolean use_article_description = false;
            boolean use_status = false;
            boolean use_article_text = false;
            boolean use_keywords = false;
            boolean use_created_by_id = false;
            boolean use_created_date = false;
            boolean use_approved_by_id = false;
            boolean use_approved_date = false;
            boolean use_valid_till = false;
            
            //get parameters
            try
            {
                knowledge_base_id = request.getParameter("knowledge_base_id");
                if(knowledge_base_id == null || knowledge_base_id.equalsIgnoreCase("any"))
                {
                    use_knowledge_base_id = false;
                    knowledge_base_id = "Any";
                }
                else
                {
                    use_knowledge_base_id = true;
                    a_parameter_was_send = true;
                }   
            }
            catch(Exception e)
            {
                knowledge_base_id = "Any";
            }
            try
            {
                article_name = request.getParameter("article_name");
                if(article_name == null || article_name.equalsIgnoreCase("Any"))
                {
                    article_name = "Any";
                    use_article_name = false;
                }
                else
                {
                    a_parameter_was_send = true;                    
                    use_article_name = true;
                }
            }
            catch(Exception e)
            {
                article_name = "Any";
            }
            try
            {
                article_description = request.getParameter("article_description");
                if(article_description == null || article_description.equalsIgnoreCase("Any"))
                {
                    article_description = "Any";
                    use_article_description = false;
                }
                else
                {
                    a_parameter_was_send = true;                    
                    use_article_description = true;
                }
            }
            catch(Exception e)
            {
                article_description = "Any";
            }
            try
            {
                status = request.getParameter("status");
                if(status != null && !status.equalsIgnoreCase("Any"))
                {
                    a_parameter_was_send = true;
                    use_status = true;
                }
                else
                {
                    status = "Any";
                    use_status = false;
                }
            }
            catch(Exception e)
            {
                status = "Any";
            }
            try
            {
                article_text = request.getParameter("article_text");
                if(article_text != null && !article_text.equalsIgnoreCase("Any"))
                {
                    a_parameter_was_send = true;
                    use_article_text = true;
                }
                else
                {
                    article_text = "Any";
                    use_article_text = false;
                }
            }
            catch(Exception e)
            {
                article_text = "Any";
            }
            try
            {
                keywords = request.getParameter("keywords");
                if(keywords != null && !keywords.equalsIgnoreCase("Any"))
                {
                    a_parameter_was_send = true;
                    use_keywords = true;
                }
                else
                {
                    keywords = "Any";
                    use_keywords = false;
                }
            }
            catch(Exception e)
            {
                keywords = "Any";
            }
            try 
            {
                created_by_id = request.getParameter("created_by_id");
                if (created_by_id.equalsIgnoreCase("null") || created_by_id == null || created_by_id.equalsIgnoreCase("Any")) 
                {
                    created_by_id = "Any";
                    created_by_name = "Any";
                    use_created_by_id = false;
                }
                else
                {
                    a_parameter_was_send = true;
                    use_created_by_id = true;
                    //get user info
                    String created_by_id_info[] = db.get_users.by_id(con, created_by_id);
                    created_by_id = created_by_id;
                    created_by_name = created_by_id_info[1];
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to ""
                created_by_id = "Any";
                created_by_name = "Any";
            }
            try
            {
                created_date_start = request.getParameter("created_date_start");
                created_date_end = request.getParameter("created_date_end");
                if(created_date_start != null && !created_date_start.equalsIgnoreCase("") && created_date_end != null && !created_date_end.equalsIgnoreCase(""))
                {
                    a_parameter_was_send = true;
                    use_created_date = true;
                }
                else
                {
                    created_date_start = "";
                    created_date_end = "";
                }
            }
            catch(Exception e)
            {
                created_date_start = "";
                created_date_end = "";
            }   
            
            try 
            {
                approved_by_id = request.getParameter("approved_by_id");
                if (approved_by_id.equalsIgnoreCase("null") || approved_by_id == null || approved_by_id.equalsIgnoreCase("Any")) 
                {
                    approved_by_id = "Any";
                    approved_by_name = "Any";
                    use_approved_by_id = false;
                }
                else
                {
                    a_parameter_was_send = true;
                    use_approved_by_id = true;
                    //get user info
                    String approved_by_id_info[] = db.get_users.by_id(con, approved_by_id);
                    approved_by_id = approved_by_id;
                    approved_by_name = approved_by_id_info[1];
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to ""
                approved_by_id = "Any";
                approved_by_name = "Any";
            }
            try
            {
                approved_date_start = request.getParameter("approved_date_start");
                System.out.println();
                approved_date_end = request.getParameter("approved_date_end");
                if(approved_date_start != null && !approved_date_start.equalsIgnoreCase("") && approved_date_end != null && !approved_date_end.equalsIgnoreCase(""))
                {
                    a_parameter_was_send = true;
                    use_approved_date = true;
                }
                else
                {
                    approved_date_start = "";
                    approved_date_end = "";
                }
            }
            catch(Exception e)
            {
                approved_date_start = "";
                approved_date_end = "";
            }   
            try
            {
                valid_till_start = request.getParameter("valid_till_start");
                valid_till_end = request.getParameter("valid_till_end");
                if(valid_till_start != null && !valid_till_start.equalsIgnoreCase("") && valid_till_end != null && !valid_till_end.equalsIgnoreCase(""))
                {
                    a_parameter_was_send = true;
                    use_valid_till = true;
                }
                else
                {
                    valid_till_start = "";
                    valid_till_end = "";
                }
            }
            catch(Exception e)
            {
                valid_till_start = "";
                valid_till_end = "";
            }   
//////////////////////////////////////////////////////////////////////////////////////////////            
            //build the query
            String query = "";
            if(use_knowledge_base_id)
            {
                if(query.equalsIgnoreCase(""))
                {
                    query = "knowledge_base_id LIKE '%" + knowledge_base_id + "%'";
                }
                else
                {
                    query = query + " AND knowledge_base_id LIKE '%" + knowledge_base_id + "%'";
                }
            }
            if(use_article_name)
            {
                String temp[] = article_name.split(" ");
                for(int a = 0; a < temp.length; a++)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "article_name LIKE '%" + temp[a] + "%'";
                    }
                    else
                    {
                        query = query + " AND article_name LIKE '%" + temp[a] + "%'";
                    }
                }
            }
            if(use_article_description)
            {
                String temp[] = article_description.split(" ");
                for(int a = 0; a < temp.length; a++)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "article_description LIKE '%" + temp[a] + "%'";
                    }
                    else
                    {
                        query = query + " AND article_description LIKE '%" + temp[a] + "%'";
                    }
                }
            }
            if(use_status)
            {
                if(query.equalsIgnoreCase(""))
                {
                    query = "status LIKE '%" + status + "%'";
                }
                else
                {
                    query = query + " AND status LIKE '%" + status + "%'";
                }
            }
            if(use_article_text)
            {
                String temp[] = article_text.split(" ");
                for(int a = 0; a < temp.length; a++)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "article_text LIKE '%" + temp[a] + "%'";
                    }
                    else
                    {
                        query = query + " AND article_text LIKE '%" + temp[a] + "%'";
                    }
                }
            }
            if(use_keywords)
            {
                String temp[] = keywords.split(" ");
                for(int a = 0; a < temp.length; a++)
                {
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "keywords LIKE '%" + temp[a] + "%'";
                    }
                    else
                    {
                        query = query + " AND keywords LIKE '%" + temp[a] + "%'";
                    }
                } 
            }
            if(use_created_by_id)
            {
                if(query.equalsIgnoreCase(""))
                {
                    query = "created_by_id LIKE '%" + created_by_id + "%'";
                }
                else
                {
                    query = query + " AND created_by_id LIKE '%" + created_by_id + "%'";
                }
            }
            if(use_created_date)
            {
                try
                {
                    java.util.Date Qstart = date_time_picker_format.parse(created_date_start.trim());
                    java.util.Date Qend = date_time_picker_format.parse(created_date_end.trim());
                    String start_string = timestamp_format.format(Qstart);
                    String end_string = timestamp_format.format(Qend);                            
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "(created_date >= '" + start_string + "' AND created_date <= '" + end_string + "')";
                    }
                    else
                    {
                        query = query + " AND (created_date >= '" + start_string + "' AND created_date <= '" + end_string + "')";
                    }
                }
                catch(Exception e)
                {
                    System.out.println("Exception on kb_search.jsp parse created_date=" + e);
                }
            }
            if(use_approved_by_id)
            {
                if(query.equalsIgnoreCase(""))
                {
                    query = "approved_by_id LIKE '%" + approved_by_id + "%'";
                }
                else
                {
                    query = query + " AND approved_by_id LIKE '%" + approved_by_id + "%'";
                }
            }
            if(use_approved_date)
            {
                try
                {
                    java.util.Date Qstart = date_time_picker_format.parse(approved_date_start.trim());
                    java.util.Date Qend = date_time_picker_format.parse(approved_date_end.trim());
                    String start_string = timestamp_format.format(Qstart);
                    String end_string = timestamp_format.format(Qend);                            
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "(approved_date >= '" + start_string + "' AND approved_date <= '" + end_string + "')";
                    }
                    else
                    {
                        query = query + " AND (approved_date >= '" + start_string + "' AND approved_date <= '" + end_string + "')";
                    }
                }
                catch(Exception e)
                {
                    System.out.println("Exception on kb_search.jsp parse approved_date=" + e);
                }
            }
            if(use_valid_till)
            {
                try
                {
                    java.util.Date Qstart = date_time_picker_format.parse(valid_till_start.trim());
                    java.util.Date Qend = date_time_picker_format.parse(valid_till_end.trim());
                    String start_string = timestamp_format.format(Qstart);
                    String end_string = timestamp_format.format(Qend);                            
                    if(query.equalsIgnoreCase(""))
                    {
                        query = "(valid_till >= '" + start_string + "' AND valid_till <= '" + end_string + "')";
                    }
                    else
                    {
                        query = query + " AND (valid_till >= '" + start_string + "' AND valid_till <= '" + end_string + "')";
                    }
                }
                catch(Exception e)
                {
                    System.out.println("Exception on kb_search.jsp parse valid_till=" + e);
                }
            }
    %>
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">

    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <link rel="stylesheet" type="text/css" href="assets/css/chat_app.css">
    <style>
    .dropdown-menu.md {
        width: 500px;
    }
    </style>
    <!-- END PAGE LEVEL CSS-->

<div class="clr whtbg p-10 mb-15 position-relative">
    <div class="float-right options">
        <ul>
            <li class="dInlineBlock dropdown ">
                <div class="dInlineBlock clr menuHolder">
                    <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                    </a>
                    <div class="dropdown-menu dropdown-menu-right md p-10">
                        <!--<h5 class="medium-font boldFont pt-5 mb-10">All Filters</h5>-->
                            <form id="filters_form">
                                <ul>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="medium-font boldFont pt-5 mb-10">Attributes</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <div class="">
                                                <label class="field-label full-width">Knowledge Base</label>
                                                <div class="formField md full-width mr-0 px-0 border-0">
                                                    <select class="form-control" id="knowledge_base_id" name="knowledge_base_id">
                                                        <option value="any">Any</option>
                                                        <%
                                                        ArrayList<String[]> all_kbs = db.get_kb.all_kb(con);
                                                        for(int a = 0; a < all_kbs.size(); a++)
                                                        {
                                                            String selected = "";
                                                            String kb[] = all_kbs.get(a);
                                                            if(knowledge_base_id.equalsIgnoreCase(kb[0]))
                                                            {
                                                                selected = "SELECTED";
                                                            }
                                                            %>
                                                            <option <%=selected%> value="<%=kb[0]%>"><%=kb[1]%></option>
                                                            <%
                                                        }
                                                        %>
                                                    </select>                                                    
                                                </div>
                                            </div>
                                        </li>
                                    </div>
                                    <div class="col-md-6">
                                        <li class="mb-10 clr">
                                            <div class="">
                                                <label class="field-label full-width">Name</label>
                                                <div class="formField md full-width mr-0 px-0 border-0">
                                                    <input type='text' id="article_name" name="article_name" value="<%=article_name%>" placeholder="Any" class="form-control full-width" />
                                                </div>
                                            </div>
                                        </li>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Description</label>
                                            <div class="formField md full-width mr-0 px-0 border-0">
                                                <input type='text' id="article_description" name="article_description" value="<%=article_description%>" placeholder="Any" class="form-control full-width" />
                                            </div>
                                        </div>
                                    </li>
                                </div>
                                    <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Status</label>
                                            <div class="formField md full-width mr-0 px-0 border-0">
                                                <select name="status" id="status" class="form-control">
                                                    <%
                                                    String[] status_names = {"Any","Draft","Awaiting Approval","Published","Superceded","Expired","Invalid","Deleted"};
                                                    String[] status_values = {"Any","Draft","Waiting_Approval","Published","Superceded","Expired","Invalid","Deleted"};
                                                    for(int a = 0; a < status_values.length; a++)
                                                    {
                                                        String selected = "";
                                                        if(status.equalsIgnoreCase(status_values[a]))
                                                        {
                                                            selected = "SELECTED";
                                                        }
                                                        %>
                                                        <option <%=selected%> value="<%=status_values[a]%>"><%=status_names[a]%></option>
                                                        <%
                                                    }
                                                    %>
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>
                                <div class="row">
                                    <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Keywords</label>
                                            <div class="formField md full-width mr-0 px-0 border-0">
                                                <input type='text' id="keywords" name="keywords" value="<%=keywords%>" placeholder="Any" class="form-control full-width" />
                                            </div>
                                        </div>
                                    </li>
                                </div>
                                    <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Text</label>
                                            <div class="formField md full-width mr-0 px-0 border-0">
                                                <input type='text' id="article_text" name="article_text" value="<%=article_text%>" placeholder="Any" class="form-control full-width" />
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <h5 class="medium-font boldFont pt-5 mb-10">People/Dates</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Created By (Last First)&nbsp;&nbsp;<a href="javascript:clear_created_by(0)"><small>(clear)</small></a></label>
                                            <div class="formField md full-width mr-0 px-0 border-0 input-group d-flex">
                                                <input type="hidden" name="created_by_id" id="created_by_id" value="<%=created_by_id%>"/>
                                                <input type='text' id="created_by_name" name="created_by_name" value="<%=created_by_name%>" placeholder="Any" class="form-control full-width" />
                                            </div>
                                        </div>
                                    </li>
                                </div>
                                <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Approved By (Last First)&nbsp;&nbsp;<a href="javascript:clear_approved_by(0)"><small>(clear)</small></a></label>
                                            <div class="formField md full-width mr-0 px-0 border-0">
                                                <input type="hidden" name="approved_by_id" id="approved_by_id" value="<%=approved_by_id%>"/>
                                                <input type='text' id="approved_by_name" name="approved_by_name" value="<%=approved_by_name%>" placeholder="Any" class="form-control full-width" />
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Created Date (Start)</label>
                                            <div class="formField md full-width mr-0 px-0 border-0">
                                                <input type='text' id="created_date_start" name="created_date_start" value="<%=created_date_start%>" placeholder="Any" class="form-control datetime full-width" />
                                            </div>
                                        </div>
                                    </li>
                                </div>
                                <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Created Date (End)</label>
                                            <div class="formField md full-width mr-0 px-0 border-0">
                                                <input type='text' id="created_date_end" name="created_date_end" value="<%=created_date_end%>" placeholder="Any" class="form-control datetime full-width" />
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Valid Till (Start)</label>
                                            <div class="formField md full-width mr-0 px-0 border-0">
                                                <input type='text' id="valid_till_start" name="valid_till_start" value="<%=valid_till_start%>" placeholder="Any" class="form-control datetime full-width" />
                                            </div>
                                        </div>
                                    </li>
                                </div>
                                <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Valid Till (End)</label>
                                            <div class="formField md full-width mr-0 px-0 border-0">
                                                <input type='text' id="valid_till_end" name="valid_till_end" value="<%=valid_till_end%>" placeholder="Any" class="form-control datetime full-width" />
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Approved Date (Start)</label>
                                            <div class="formField md full-width mr-0 px-0 border-0">
                                                <input type='text' id="approved_date_start" name="approved_date_start" value="<%=approved_date_start%>" placeholder="Any" class="form-control datetime full-width" />
                                            </div>
                                        </div>
                                    </li>
                                </div>
                                <div class="col-md-6">
                                    <li class="mb-10 clr">
                                        <div class="">
                                            <label class="field-label full-width">Approved Date (End)</label>
                                            <div class="formField md full-width mr-0 px-0 border-0">
                                                <input type='text' id="approved_date_end" name="approved_date_end" value="<%=approved_date_end%>" placeholder="Any" class="form-control datetime full-width" />
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>


                                </ul>
                                <div class="text-center pt-5 pb-5">
                                    <button class="btn md2 btn-primary-new customBtn mb-5">Apply Filters</button>
                                    <div class="clr"></div>
                                    <a href="javascript:void(0)" onclick="resetFilters()" class="">Reset Filters</a>
                                </div>
                            </form>
                    </div>
                </div>
            </li>

        </ul>
    </div>
    <h1 class="large-font boldFont">Knowledge Bases</h1>
    <div class="custom-tabs clr ">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link <%=(query.isEmpty()?"active show":"")%>" data-toggle="tab" href="#knowledge-base-tab" role="tab" aria-selected="true">Knowledge Base</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <%=(query.isEmpty()?"":"active show")%>" data-toggle="tab" href="#knowledge-base-article-tab" role="tab" aria-selected="false">Knowledge Base Articles</a>
            </li>


        </ul><!-- Tab panes -->
    </div>
</div>
<%
        String[] filter_fields = {
            "knowledge_base_id",
            "article_name",
            "article_description",
            "status",
            "keywords",
            "article_text",
            "created_by_id",
            "created_by_name",
            "approved_by_id",
            "approved_by_name",
            "created_date_start",
            "created_date_end",
            "valid_till_start",
            "valid_till_end",
            "approved_date_start",
            "approved_date_end",
        };
        List<Map<String, String>> filters = new ArrayList<>();
        for (int i = 0; i < filter_fields.length; i++)
        {
            String filter_field = filter_fields[i];
            String filter_value = request.getParameter(filter_field);
            if (filter_value != null && !filter_value.isEmpty() && !filter_value.equalsIgnoreCase("any")) 
            {
                Map<String, String> map = new HashMap<String, String>();
                map.put("key", filter_field);
                map.put("value", filter_value);
                filters.add(map);
            }
        }

    %>    

    <%
        request.setAttribute("filter_fields", filter_fields);
    %>
    <jsp:include page="active_filters_row.jsp" />
    
<div class="tab-content">
    <div class="tab-pane <%=(query.isEmpty()?"active show":"")%>" id="knowledge-base-tab" role="tabpanel">
        <div class=" clr mb-15 text-right ">
            <span data-type="kb" class="btn btn-success customBtn lg waves-effect rightSidePanelOpener">
                <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>Add Knowledge Base
            </span>
        </div>
        <div class="sort-tbl-holder pb-100">
            <h2 class="medium-font boldFont">Knowledge Bases</h2>
            <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="20%">Name</th>
                        <th width="40%">Description</th>
                        <th width="15%">State</th>
                        <th width="15%">Type</th>
                        <th width="10%" class="text-center">Actions</th>
                    </tr>
                </thead>

                <tbody>
                    <%
                    ArrayList<String[]> all_kb = db.get_kb.all_kb(con);
                    for(int a = 0; a < all_kb.size();a++)
                    {
                        String kb[] = all_kb.get(a);

                        %>
                        <tr>
                            <td><%=kb[1]%></td>
                            <td><%=kb[2]%></td>
                            <td><%
                                String state_color_class = db.static_data.get_css_class_by_state(kb[3]);
                                if (!"".equals(state_color_class))
                                {
                                    %>
                                    <span class="btn sm btn-<%=state_color_class%> customBtn"><%=kb[3]%></span>
                                    <%
                                } else
                                {
                                    %>
                                    <%=kb[3]%>
                                    <%
                                } 
                                %>
                            </td>
                            <td><%=kb[4]%></td>
                            <td class="text-center">
                                <a href="javascript:void(0)" onclick="editKB('<%=kb[0]%>');" class="mr-5"><img src="assets/images/svg/edit-icon.svg" alt=""></a>
                                <a href="javascript:void(0)" onclick="showDeleteModal('<%=kb[0]%>', 'KB')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>
                            </td>
                        </tr>
                        <%
                    }
                    %>
                </tbody>
                <%
                if(all_kb.size() > 10)
                {
                    %>
                    <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>State</th>
                            <th>Type</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>
                    <%
                }
                %>                                    
            </table>
        </div>
    </div>
    <div class="tab-pane <%=(query.isEmpty()?"":"active show")%>" id="knowledge-base-article-tab" role="tabpanel">
        <div class=" clr mb-15 text-right ">
            <span data-type="kba" class="btn btn-success customBtn lg waves-effect rightSidePanelOpener">
                <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>Add Knowledge Base Article
            </span>
        </div>
        <div class="sort-tbl-holder">
            <table id="dataTable_articles" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="5%"></th>
                        <th width="15%">Name</th>
                        <th width="25%">Description</th>
                        <th width="10%">Status</th>
                        <th width="10%">Knowledge Base</th>
                        <th width="10%" class="text-center">Date Created</th>
                        <th width="7.5%" class="text-center">Views</th>
                        <th width="10%" class="text-center">Helpful</th>
                        <th width="7.5%" class="text-center">Category</th>
                        <th width="10%" class="text-center">Actions</th>
                    </tr>
                </thead>

                <tbody>
                    <%
                    if (!"".equalsIgnoreCase(query))
                    {
                        query = " WHERE " + query;
                    }
                    ArrayList<String[]> all_articles = db.get_kb.all_articles_filtered(con, query);
                    for(int b = 0; b < all_articles.size();b++)
                    {
                        String article[] = all_articles.get(b);
                        String date = "";
                        try
                        {
                            date = date_time_picker_format.format(timestamp_format.parse(article[8]));
                        }
                        catch(Exception e)
                        {
                            date = "";
                        }
                    %>

                    <tr>
                        <td>
                            <span class="expand-td">
                                <img src="assets/images/svg/collapse-minus-icon.svg" alt="">
                            </span>
                        </td>
                        <td>
                            <div class="truncate" id="article_name_<%=article[0]%>">
                            <%=article[2]%>
                            </div>
                        </td>
                        <td>
                            <%=article[3]%>
                        </td>
                        <td><%
                            String state_color_class = db.static_data.get_css_class_by_state(article[4]);
                            if (!"".equals(state_color_class))
                            {
                                %>
                                <span class="btn sm btn-<%=state_color_class%> customBtn"><%=article[4]%></span>
                                <%
                            } else
                            {
                                %>
                                <%=article[4]%>
                                <%
                            } 
                            %>                        </td>
                        <td><%=article[12]%></td>
                        <td><%=article[8]%></td>
                        <td class="text-center"><%=article[13]%></td>
                        <td class="text-center">
                            <div class="dInlineBlock mr-5">
                                <span class="icon mr-5 <%=(article[14].isEmpty()?"d-none":"")%>"><img style="height:14px;" src="assets/images/svg/up-arrow.svg" alt=""></span><%=article[14]%>
                            </div>
                            <div class="dInlineBlock">
                                <span class="icon mr-5 <%=(article[15].isEmpty()?"d-none":"")%>"><img style="height:14px;" src="assets/images/svg/down-arrow.svg" alt=""></span><%=article[15]%>
                            </div>
                        </td>
                        <td class="text-center">Management</td>
                        <td class="text-center">
                            <a href="javascript:void(0)" onclick="event.stopPropagation(); previewKBA('<%=article[0]%>');" class="mr-5"><img src="assets/images/svg/view-icon.svg" alt=""></a>
                            <a href="javascript:void(0)" onclick="showDeleteModal('<%=article[0]%>', 'article')" class="mr-5"><img src="assets/images/svg/trash-icon-blk.svg" alt=""></a>
                            <% if (article[16] != null && !article[16].equals(""))
                            {
                            %>
                                <a href="javascript:void(0)" data-target="article" data-id="<%=article[0]%>" class="mr-5 rightSidePanelOpener2"><img src="assets/images/svg/team-icon-blink.svg" alt=""></a>
                            <%
                            }
                            %>
                        </td>
                    </tr>
                    <tr class="expanded accordian-body collapse" id="article-edit-<%=article[0]%>" data-article-id="<%=article[0]%>" aria-expanded="false" style="height: 0px;">
                        <td colspan="10" class="first-row hiddenRow">
                            <div class="content-holder p-15 mt-10 mb-10"></div>
                        </td>
                    </tr>
                    <%
                    }
                    %>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="area">
</div>
                
<div class="modalPreview">
    <div class="modal fade preview" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog preview" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"><!--modal_title--></h4>
            <button class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <!--modal_body-->
          </div>
          <div class="modal-footer">
            <button class="btn btn-primary-new" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
</div>


    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	

    <div class="rightPanelHolder">
        <div class="p-15 ticket-content">
            <span class="rightSidePanelCloser"><img src="assets/images/svg/close-icon.svg" alt=""></span>
            <h4 id="rightPanelTitle" class="mediumFont large-font mb-20"></h4>
            <div id="rightPanelContent" class="clr whtbg p-15">
            </div>
        </div>

    </div>
    <div class="rightPanelHolder2">
        <div class="p-15 ticket-content">
            <span class="rightSidePanelCloser2"><img src="assets/images/svg/close-icon.svg" alt=""></span>
            <h4 id="rightPanelTitleChat" class="mediumFont large-font mb-20"></h4>
            <div id="chat_app" class="clr whtbg p-15">
            </div>
        </div>
    </div>

    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>    
    <script src="app-assets/js/scripts/tables/datatables-extensions/dataRender/datetime.js"></script>    
    <script src="assets/js/vue.min.js"></script>
    <script src="assets/js/axios.min.js"></script>
    <script src="assets/js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        var showChat = false;
        var currentUserId = <%=user_id%>;
        var objectType = "";
        var objectId = 0;
        var parentObjectType = "";
        var parentObjectId = 0;
        var serviceDeskEndpoint = "<%=props.get("servicedesk_url")%>";

    </script>
    <script src="assets/js/chat_app.js"></script>

    <script type="text/javascript">
        var ajax_id = "";
        var ajax_type = "";

        function clear_created_by(article_id) 
        { 
            var form;
            var val;
            if (article_id === 0)
            {
                form = document.forms["filters_form"];
                val = "Any";
            } else {
                form = document.forms["article_edit_form_" + article_id];
                val = "";
            }
            form.elements["created_by_name"].value = val;
            form.elements["created_by_id"].value = val;
        }
        function clear_approved_by(article_id) 
        { 
            var form;
            var val;
            if (article_id === 0)
            {
                form = document.forms["filters_form"];
                val = "Any";
            } else {
                form = document.forms["article_edit_form_" + article_id];
                val = "";
            }
            form.elements["approved_by_name"].value = val;
            form.elements["approved_by_id"].value = val;
        }
        
        function editKB(id)
        {
            ajax_id = id;
            ajax_type = "kb";
            $('.rightSidePanelOpener').click();
        }
        
        $(function() {
            $('.dropdown').on('hide.bs.dropdown', function (e) 
            { // prevent dropdown close on autocomplete // console.log(e.clickEvent.target.className);
                if (e.clickEvent && e.clickEvent.target && (e.clickEvent.target.className == "ui-menu-item-wrapper" || e.clickEvent.target.className.indexOf("applyBtn") !== -1 )) {
                    e.preventDefault();
                }
            });
            
        $('#dataTable_articles tbody').on( 'click', 'tr span.expand-td', function () {
            var tr = $(this).closest('tr');
//            console.log(tr);
//            var row = $("#dataTable_articles").row( tr );
            var row_id = tr.data('id');
            var idx = $.inArray( row_id, detailRows );

            if ( row.child.isShown() ) {
                row.child.hide();

                // Remove from the 'open' array
                detailRows.splice( idx, 1 );
            }
            else {
                if(row.child() && row.child().length)
                {
                    row.child.show();
                } else {
                    row.child( 
                        $(
                            '<tr class="expanded" id="article_details_row_' + row_id + '">' +
                                '<td colspan="9" class="first-row">' +
                                    '<div class="content-holder p-15 mt-10 mb-10"></div>' +
                                '</td>' +
                            '</tr>'
                        )                    
                    ).show();
                    editArticle(row_id);
                }
                // Add to the 'open' array
                if ( idx === -1 ) {
                    detailRows.push( row_id );
                }
            }
        } );
            
            $( "#approved_by_name" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user_last_first",
                        type: 'post',
                        dataType: "json",
                        minLength: 1,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {                    
                    // Set selection
                    $(this).val(ui.item.label); // display the selected text
                    this.form.elements['approved_by_id'].value = ui.item.value; // save selected id to input
                    return false;
                }
            });
            
            $( "#created_by_name" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user_last_first",
                        type: 'post',
                        dataType: "json",
                        minLength: 1,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    $(this).val(ui.item.label); // display the selected text
                    this.form.elements['created_by_id'].value = ui.item.value; // save selected id to input
                    return false;
                }
            });

            $('input[name="created_date_start"]').daterangepicker(
            {
                "autoUpdateInput": false,
                "singleDatePicker": true,
                "showDropdowns": true,
                "timePicker": true,
                "timePicker24Hour": true,
                "locale": {
                    "format": "HH:mm MM/DD/YYYY",
                    "separator": " - ",
                    "applyLabel": "Apply",
                    "cancelLabel": "Cancel",
                    "fromLabel": "From",
                    "toLabel": "To",
                    "customRangeLabel": "Custom",
                    "weekLabel": "W",
                    "daysOfWeek": [
                        "Su",
                        "Mo",
                        "Tu",
                        "We",
                        "Th",
                        "Fr",
                        "Sa"
                    ],
                    "monthNames": [
                        "January",
                        "February",
                        "March",
                        "April",
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",
                        "December"
                    ],
                    "firstDay": 1
                }
            });
            $('input[name="created_date_start"]').on('apply.daterangepicker', function(ev, picker)
            {
                $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
            });
            $('input[name="created_date_start"]').on('cancel.daterangepicker', function(ev, picker) 
            {
                $(this).val('');
            });
          $('input[name="created_date_end"]').daterangepicker(
            {
                "autoUpdateInput": false,
                "singleDatePicker": true,
                "showDropdowns": true,
                "timePicker": true,
                "timePicker24Hour": true,
                "locale": {
                "format": "HH:mm MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            }
          });
          $('input[name="created_date_end"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });
         $('input[name="created_date_end"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
          $('input[name="approved_date_start"]').daterangepicker(
            {
                "autoUpdateInput": false,
                "singleDatePicker": true,
                "showDropdowns": true,
                "timePicker": true,
                "timePicker24Hour": true,
                "locale": {
                "format": "HH:mm MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            }
          });
          $('input[name="approved_date_start"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });
         $('input[name="approved_date_start"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
          $('input[name="approved_date_end"]').daterangepicker(
            {
                "autoUpdateInput": false,
                "singleDatePicker": true,
                "showDropdowns": true,
                "timePicker": true,
                "timePicker24Hour": true,
                "locale": {
                "format": "HH:mm MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            }
          });
          $('input[name="approved_date_end"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });
         $('input[name="approved_date_end"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
          $('input[name="valid_till_start"]').daterangepicker(
            {
                "autoUpdateInput": false,
                "singleDatePicker": true,
                "showDropdowns": true,
                "timePicker": true,
                "timePicker24Hour": true,
                "locale": {
                "format": "HH:mm MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            }
          });
          $('input[name="valid_till_start"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });
         $('input[name="valid_till_start"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
          $('input[name="valid_till_end"]').daterangepicker(
            {
                "autoUpdateInput": false,
                "singleDatePicker": true,
                "showDropdowns": true,
                "timePicker": true,
                "timePicker24Hour": true,
                "locale": {
                "format": "HH:mm MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            }
          });
          $('input[name="valid_till_end"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });
         $('input[name="valid_till_end"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
         
            $('tr.collapse').on('show.bs.collapse', function (e) {
                editArticle($(e.target).data('article-id'));
            });

            $('.rightSidePanelOpener').on('click', function(e){
                $("#rightPanelContent").html("");

                var panel_title = "";
                var ajax = {};
                ajax.type = 'GET';
                ajax.dataType = "html";

                if(e.originalEvent && e.originalEvent.isTrusted)
                {
                    // real click.
                    var url_addon = "_add.jsp";
//                    console.log(e, e.target);
                    ajax_type = $(e.target).data('type');
                    
                } else {
                    // programmatic click        
                    ajax.data = {id: ajax_id};
                    var url_addon = "_edit.jsp";
                }

                switch (ajax_type) 
                {
                    case "kb":
                        ajax.url = "admin_knowledge_base";
                        panel_title = "Knowledge base parameters";
                        break;
                    case "kba":
                        ajax.url = "admin_knowledge_base_article";
                        panel_title = "Knowledge base article parameters";
                        break;
                    default:
                        ajax.url = "";
                        break;
                }
                if (ajax.url === "") return false;

                ajax.url += url_addon;

                ajax.error = function(jqXHR, textStatus)
                {
                    switch (jqXHR.status) {
                        case 401:
                            window.location.assign("<%=props.get("login_url").toString()%>");
                            break;
                        case 403:
                            alert("No permission!");
                            break;
                        default:
                            alert("Error processing request!");
                    }
                };
                
                ajax.success = function (data) 
                {                    
                    $("#rightPanelTitle").html(panel_title);
                    $("#rightPanelContent").html(data);
                    $('.rightSidePanelCloser').on('click', function(){
                        $('.rightPanelHolder').removeClass('open');
                        $('.rightSidePanel').removeClass('open');
                        $('html').removeClass('panelOpen');
                        ajax_id = "";
                        ajax_type = "";
                    });

                    tinymce.remove('textarea');
                    tinymce.init({
                        selector: '.ckeditor',
                        setup: function (editor) {
                            editor.on('change', function () {
                                editor.save();
                            });
                        },
                        height: 250,
                        plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
                        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code',
                        image_advtab: true,
                        templates: [
                            { title: 'Test template 1', content: 'Test 1' },
                            { title: 'Test template 2', content: 'Test 2' }
                        ],
                        content_css: [
                            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                            '//www.tinymce.com/css/codepen.min.css'
                        ]
                    });

                    $('input[name="valid_till"]').daterangepicker({
                          "autoUpdateInput": false,
                          "singleDatePicker": true,
                          "showDropdowns": true,
                          "timePicker": true,
                          "timePicker24Hour": true,
                          "locale": {
                              "format": "HH:mm MM/DD/YYYY",

                              "applyLabel": "Apply",
                              "cancelLabel": "Cancel",

                              "weekLabel": "W",
                              "daysOfWeek": [
                                  "Su",
                                  "Mo",
                                  "Tu",
                                  "We",
                                  "Th",
                                  "Fr",
                                  "Sa"
                              ],
                              "monthNames": [
                                  "January",
                                  "February",
                                  "March",
                                  "April",
                                  "May",
                                  "June",
                                  "July",
                                  "August",
                                  "September",
                                  "October",
                                  "November",
                                  "December"
                              ],
                              "firstDay": 1
                        }
                    });

                    $('input[name="valid_till"]').on('apply.daterangepicker', function(ev, picker) {
                        $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
                    });

                    $('input[name="valid_till"]').on('cancel.daterangepicker', function(ev, picker) {
                        $(this).val('');
                    });

                };
                
                $.ajax(ajax);
            });

            $('.rightSidePanelCloser2:not(.listens)').each(function(){
                $(this).addClass('listens');
                this.addEventListener("click", function()
                {
                    window.chat_app.showChat = false;
                    $('.rightPanelHolder2').removeClass('open');
                    $('.rightSidePanel2').removeClass('open');
                    $('html').removeClass('panelOpen');
                }, false);
            });

            $('.rightSidePanelOpener2:not(.listens)').each(function(){
                $(this).addClass('listens');

                this.addEventListener("click", function()
                {
                    $('.rightPanelHolder2').addClass('open');
                    $('.rightSidePanel2').addClass('open');
                    $('html').addClass('panelOpen');
    //                console.log($(this).data('articleId'));
                    window.chat_app.objectType = $(this).data('target');
                    window.chat_app.objectId = $(this).data('id');
                    window.chat_app.parentObjectType = "";
                    window.chat_app.parentObjectId = 0;
                    window.chat_app.currentUserId = <%=user_id%>;
                    window.chat_app.showChat = true;
                    console.log("panel2 opened");
                });
            });

        });
        
        function previewKBA(article_id)
        {
            console.log(article_id);
            $.ajax({
                url: "admin_knowledge_base_article_preview.jsp?id=" + article_id,
                dataType: "html",
                error: function(jqXHR, textStatus) 
                {
                    switch (jqXHR.status) {
                        case 401:
                            window.location.assign("<%=props.get("login_url").toString()%>");
                            break;
                        case 403:
                            alert("No permission!");
                            break;
                        default:
                            alert("Error processing request!");
                    }
                },
                success: function( data ) 
                {
                    if (data.indexOf('action="login"') !== -1)
                    {
                        window.location.assign("<%=props.get("login_url").toString()%>");
                        return false;
                    }
                    modaldiv = $('.modalPreview').html();
                    modaldiv = modaldiv.replace("myModal", "myModal_" + article_id);
                    modaldiv = modaldiv.replace("<!--modal_body-->", data);
                    modaldiv = modaldiv.replace("<!--modal_title-->", $("#article_name_" + article_id).html());
                    $('.area').append(modaldiv);

                    // reset modal if it isn't visible
                    if (!($('.modal.in').length)) {
                      $('#myModal_' + article_id + ' .modal-dialog').css({
                        top: '50%',
                        left: '40%',

                      });
                    }


                    $('#myModal_' + article_id).modal({
                      backdrop: false,
                      show: true
                    });

                    $('.modal-dialog').draggable({
                      handle: ".modal-header"
                    });

                }
            });            
        }
        
        function editArticle(article_id)
        {
            var content_holder = $('tr[data-article-id="' + article_id + '"] div.content-holder');
            if (content_holder.html() == "") 
            {

                var ajax = {};
                ajax.type = 'GET';
                ajax.dataType = "html";

                ajax.data = {id: article_id};
                ajax.url = "admin_knowledge_base_article_edit.jsp";

                ajax.error = function(jqXHR, textStatus)
                {
                    switch (jqXHR.status) {
                        case 401:
                            window.location.assign("<%=props.get("login_url").toString()%>");
                            break;
                        case 403:
                            alert("No permission!");
                            break;
                        default:
                            alert("Error processing request!");
                    }
                };

                ajax.success = function (data) 
                {
                    content_holder.html(data);
                    
                    tinymce.init({
                        selector: '#article_text_' + article_id,
                        height: 250,
                        plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
                        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code',
                        image_advtab: true,
                        templates: [
                            { title: 'Test template 1', content: 'Test 1' },
                            { title: 'Test template 2', content: 'Test 2' }
                        ],
                        content_css: [
                            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                            '//www.tinymce.com/css/codepen.min.css'
                        ]
                    });

                    $('input[name="valid_till"]').daterangepicker({
                          "autoUpdateInput": false,
                          "singleDatePicker": true,
                          "showDropdowns": true,
                          "timePicker": true,
                          "timePicker24Hour": true,
                          "locale": {
                              "format": "HH:mm MM/DD/YYYY",

                              "applyLabel": "Apply",
                              "cancelLabel": "Cancel",

                              "weekLabel": "W",
                              "daysOfWeek": [
                                  "Su",
                                  "Mo",
                                  "Tu",
                                  "We",
                                  "Th",
                                  "Fr",
                                  "Sa"
                              ],
                              "monthNames": [
                                  "January",
                                  "February",
                                  "March",
                                  "April",
                                  "May",
                                  "June",
                                  "July",
                                  "August",
                                  "September",
                                  "October",
                                  "November",
                                  "December"
                              ],
                              "firstDay": 1
                        }
                    });

                    $('input[name="valid_till"]').on('apply.daterangepicker', function(ev, picker) {
                        $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
                    });

                    $('input[name="valid_till"]').on('cancel.daterangepicker', function(ev, picker) {
                        $(this).val('');
                    });


                    $( 'input[name="created_by_name"]' ).autocomplete(
                    {
                        source: function( request, response ) 
                        {
                            // Fetch data
                            $.ajax({
                                url: "ajax_lookup_user",
                                type: 'post',
                                dataType: "json",
                                minLength: 2,
                                data: 
                                {
                                    search: request.term
                                },
                                success: function( data ) 
                                {
                                    response( data );
                                }
                            });
                        },
                        select: function (event, ui) 
                        {
                            // Set selection
                            $(this).val(ui.item.label); // display the selected text
                            this.form.elements['created_by_id'].value = ui.item.value; // save selected id to input
                            //alert("created_by_id is=" + ui.item.value);
                            return false;
                        }
                    });
                    $('input[name="created_date"]').daterangepicker({
                            "autoUpdateInput": false,
                            "singleDatePicker": true,
                            "showDropdowns": true,
                            "timePicker": true,
                            "timePicker24Hour": true,
                            "locale": {
                                "format": "HH:mm MM/DD/YYYY",

                                "applyLabel": "Apply",
                                "cancelLabel": "Cancel",

                                "weekLabel": "W",
                                "daysOfWeek": [
                                    "Su",
                                    "Mo",
                                    "Tu",
                                    "We",
                                    "Th",
                                    "Fr",
                                    "Sa"
                                ],
                                "monthNames": [
                                    "January",
                                    "February",
                                    "March",
                                    "April",
                                    "May",
                                    "June",
                                    "July",
                                    "August",
                                    "September",
                                    "October",
                                    "November",
                                    "December"
                                ],
                                "firstDay": 1
                          }
                      });

                      $('input[name="created_date"]').on('apply.daterangepicker', function(ev, picker) {
                          $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
                      });

                      $('input[name="created_date"]').on('cancel.daterangepicker', function(ev, picker) {
                          $(this).val('');
                      });

                      $('input[name="approved_date"]').daterangepicker({
                            "autoUpdateInput": false,
                            "singleDatePicker": true,
                            "showDropdowns": true,
                            "timePicker": true,
                            "timePicker24Hour": true,
                            "locale": {
                                "format": "HH:mm MM/DD/YYYY",

                                "applyLabel": "Apply",
                                "cancelLabel": "Cancel",

                                "weekLabel": "W",
                                "daysOfWeek": [
                                    "Su",
                                    "Mo",
                                    "Tu",
                                    "We",
                                    "Th",
                                    "Fr",
                                    "Sa"
                                ],
                                "monthNames": [
                                    "January",
                                    "February",
                                    "March",
                                    "April",
                                    "May",
                                    "June",
                                    "July",
                                    "August",
                                    "September",
                                    "October",
                                    "November",
                                    "December"
                                ],
                                "firstDay": 1
                          }
                      });

                      $('input[name="approved_date"]').on('apply.daterangepicker', function(ev, picker) {
                          $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
                      });

                      $('input[name="approved_date"]').on('cancel.daterangepicker', function(ev, picker) {
                          $(this).val('');
                      });



                      $( 'input[name="approved_by_name"]' ).autocomplete(
                        {
                        source: function( request, response ) 
                        {
                            // Fetch data
                            $.ajax({
                                url: "ajax_lookup_user",
                                type: 'post',
                                dataType: "json",
                                minLength: 2,
                                data: 
                                {
                                    search: request.term
                                },
                                success: function( data ) 
                                {
                                    response( data );
                                }
                            });
                        },
                        select: function (event, ui) 
                        {
                            // Set selection
                            $(this).val(ui.item.label); // display the selected text
                            this.form.elements['approved_by_id'].value = ui.item.value; // save selected id to input
                            //alert("setting is=" + ui.item.value);
                            return false;
                        }
                    });
                    
                };

                $.ajax(ajax);        
            }
        }

    </script>
    <!-- END PAGE LEVEL JS-->   
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_knowledge_base.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_knowledge_base.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

<%@page import="org.apache.commons.lang3.StringUtils"%>
<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : contact_service_compliance
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="javax.json.Json"%>
<%@ page import="javax.json.JsonArray"%>
<%@ page import="javax.json.JsonObject"%>
<%@ page import="javax.json.JsonReader"%>
<%@ page import="javax.json.JsonStructure"%>
<%@ page import="java.io.StringReader"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else 
    {
        if (!session.getAttribute("contact").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            String home_page = session.getAttribute("home_page").toString();
            //if you have the permission to read/modify or Admin then you can see this
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
            //SimpleDateFormat filter_format2 = new SimpleDateFormat("yyyy"); //01/12/2019 12:00 AM
            java.util.Date filter_start = new java.util.Date();
            java.util.Date filter_end = new java.util.Date();
            java.util.Date previous_start_date = new java.util.Date();
            java.util.Date previous_end_date = new java.util.Date();
            String start = "";
            String end = "";
            String date_range = "";
            String selected = "";
            String embedded = StringUtils.defaultString(request.getParameter("embedded"));
            String company = StringUtils.defaultString(request.getParameter("company"));
            String caller_group_id = StringUtils.defaultString(request.getParameter("caller_group_id"));
            String caller_group_name = StringUtils.defaultString(request.getParameter("caller_group_name"));
            String compliance = StringUtils.defaultString(request.getParameter("compliance"));

            try 
            {
                date_range = request.getParameter("date_range");
                if (date_range.equalsIgnoreCase("null") || date_range == null) 
                {
                    date_range = support.filter_dates.past_30_days();
                }
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                date_range = support.filter_dates.past_30_days();
            }
            
            try 
            {
                
                //split the start and end date  //01/12/2019 12:00 AM - 01/12/2019 11:59 PM
                String temp[] = date_range.split("-");
                filter_start = filter_format.parse(temp[0]);
                filter_end = filter_format.parse(temp[1]);                
                start = timestamp.format(filter_start);
                end = timestamp.format(filter_end);

                //get pervious date frame                
                previous_start_date.setTime(filter_start.getTime());
                previous_end_date.setTime(filter_end.getTime());                
                long diff = filter_end.getTime() - filter_start.getTime();
                previous_start_date.setTime(filter_start.getTime() - diff);
                previous_end_date.setTime(filter_end.getTime() - diff);
            } 
            catch (Exception e) 
            {
                //if not set then default to past 30 days
                System.out.println("contact_home.jsp exception=" + e);
            }
    %>

<% if (embedded.equals("")) 
{
%>

    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	
    <input type="hidden" name="start" id="start" value="<%=start%>"/>
    <input type="hidden" name="end" id="end" value="<%=end%>"/>
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/app.css">-->
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
<!--    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <!--<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">-->
    
    <script src="assets/js/highcharts/highcharts.js"></script>
    <script src="assets/js/highcharts/data.js"></script>
    <script src="assets/js/highcharts/drilldown.js"></script>
    <script src="assets/js/highcharts/exporting.js"></script>
    
    
    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
<%
}
%>    
    <div class="clr whtbg p-10 mb-15 position-relative">
        <div class="float-right options">
            <ul>
<% if (embedded.equals("")) 
{
%>
                <li id="filters_dropdown" class="dInlineBlock dropdown ">
                    <div class="dInlineBlock clr menuHolder">
                        <a class="btn btn-wht customBtn lg dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <span class="icon mr-5"><img src="assets/images/svg/filter-icon.svg" alt=""></span>Filters
                        </a>
                        <div class="dropdown-menu dropdown-menu-right md p-10" x-placement="bottom-end" style="position: absolute; transform: translate3d(100px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                            <h5 class="medium-font boldFont pt-5 mb-10">All Filters</h5>
                            <form id="filter_form">
                                <ul>
                                    <li class="mb-10 clr">
                                        <label class="field-label full-width">
                                            Date Range
                                        </label>
                                        <div class="formField md  mr-0 full-width">
                                            <input class="no-border full-width datetime" type="text" name="date_range" id="date_range" value="<%=date_range%>">
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <label class="field-label full-width">
                                            Company
                                        </label>
                                        <div class="formField md full-width mr-0">
                                            <input type="text" id="company" name="company" value="<%=company%>" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                        </div>
                                    </li>
                                    <li class="mb-10 clr">
                                        <label class="field-label full-width">
                                            Callers Group
                                        </label>
                                        <div class="formField md full-width mr-0">
                                            <input type="hidden" name="caller_group_id" id="caller_group_id" value="<%=caller_group_id%>">
                                            <input type="text" id="caller_group_name" name="caller_group_name" value="<%=caller_group_name%>" placeholder="Any" class="border-0 full-width ui-autocomplete-input" autocomplete="off">
                                        </div>
                                    </li>                                </ul>
                                    <li class="mb-10 clr">
                                        <label class="field-label full-width">
                                            Compliance
                                        </label>
                                        <div class="formField md  mr-0 full-width">
                                            <select name="compliance" class="form-control border-0 h-auto px-0">
                                                <option value="Compliant" <%=(compliance.equals("Compliant") ? "selected" : "")%>>Compliant</option>
                                                <option value="Non-Compliant" <%=(compliance.equals("Non-Compliant") ? "selected" : "")%>>Non-Compliant</option>
                                            </select>
                                        </div>
                                    </li>
                                <div class="text-center pt-5 pb-5">
                                    <button type="submit" class="btn md2 btn-primary-new customBtn mb-5">Apply Filters</button>
                                    <div class="clr"></div>
                                    <a href="#" class="">Reset Filters</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </li>

<%
}
%>

            </ul>
        </div>
        <h1 class="large-font boldFont">Contact SLAs</h1>


        <div class="custom-tabs clr ">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" data-toggle="tab" href="#reportable" role="tab">Reportable</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#non-reportable" role="tab">Non-Reportable</a>
                </li>
            </ul><!-- Tab panes -->
        </div>
    </div>
    <div id="active_filters">
    </div>

<div class="tab-content">
    <div class="tab-pane active" id="reportable" role="tabpanel">
            <div class="row">
                <%
                //get all reportable contact sla's
                //get all active/reportable contact SLAs
                double previous_period_score = 0;
                double this_period_score = 0;
                String sla_id = "";
                String sla_timeframe = ""; //13 //String timeframes[] = {"DAILY", "WEEKLY", "MONTHLY", "QUARTERLY", "ANNUAL", "CUSTOM"}; String timeframes_names[] = {"Daily", "Weekly", "Monthly", "Quarterly", "Annual", "Custom"};
                String sla_name = "";
                String sla_type = "";
                String sla_type_raw = "";
                String sla_target = "";
                String sla_threshold_operator = "";
                String sla_threshold_value = "";
                String sla_threshold_unit = "";
                String popover_text = "";
                String channel_string = "";
                String compliance_color = "";
                double compliance_value = 0;
                double noncompliance_value = 0;
                //get all active/reportable contact SLAs
                String reportable_contact_sla[][] = db.get_sla.all_reportable_contact(con);
                //get scores for previous period
                for(int a = 0; a < reportable_contact_sla.length;a++)
                {
                    popover_text = "";
                    channel_string = "";
                    previous_period_score = support.sla_contact_calc.for_sla_id(con, reportable_contact_sla[a][0], previous_start_date, previous_end_date);
                    System.out.println("previous_period_score=" + previous_period_score);
                    this_period_score = support.sla_contact_calc.for_sla_id(con, reportable_contact_sla[a][0], filter_start, filter_end);
                    System.out.println("this_period_score=" + this_period_score);
                    sla_id = reportable_contact_sla[a][0];
                    sla_name = reportable_contact_sla[a][1];
                    sla_timeframe = support.string_case_changer.to_camel_case(reportable_contact_sla[a][13]);
                    sla_target = reportable_contact_sla[a][5];
                    sla_type_raw = reportable_contact_sla[a][4];
                    sla_type = support.string_case_changer.contact_sla_type(reportable_contact_sla[a][4]);
                    System.out.println("sla_type_raw=" + sla_type_raw + "  sla_type=" + sla_type);
                    sla_threshold_operator = support.string_case_changer.sla_threshold_operator(reportable_contact_sla[a][15]);
                    sla_threshold_value = reportable_contact_sla[a][16];
                    sla_threshold_unit = support.string_case_changer.sla_threshold_units(reportable_contact_sla[a][17]);
                    String channel_list[] = new String[0];
                    try 
                    {
                        JsonReader reader = Json.createReader(new StringReader(reportable_contact_sla[a][24]));
                        JsonObject resultObject = reader.readObject();
                        JsonArray parameters = (JsonArray) resultObject.getJsonArray("parameters");
                        JsonObject channel_array_object = parameters.getJsonObject(0); //get the first object
                        JsonArray channels = (JsonArray) channel_array_object.getJsonArray("channels");
                        channel_list = new String[channels.size()];
                        System.out.println("channels.size()=" + channels.size());
                        for(int b = 0; b < channels.size(); b++) //for each channel
                        {
                            JsonObject this_channel = channels.getJsonObject(b); //a single question
                            channel_list[b] = this_channel.getString("name").replace("\"", "");
                            System.out.println("channel_string=" + channel_string);
                            channel_string = channel_string + "&nbsp;&nbsp;&nbsp;" + channel_list[b] + "<br/>";
                        }
                    } 
                    catch (Exception e) 
                    {
                        System.out.println("Exception in record=" + e);
                    }
                    //System.out.println("sla_type=" + sla_type);
                    if(sla_type_raw.equalsIgnoreCase("ABANDONED_CONTACT_RATE"))
                    {
                        popover_text = "<strong>Threshold:</strong><br/>&nbsp;&nbsp;&nbsp; Abandoned Contact Rate " + sla_threshold_operator + "&nbsp;" + sla_threshold_value + "<br/><strong>Channels:</strong><br/>" + channel_string;
                    }
                    else if (sla_type_raw.equalsIgnoreCase("ABANDONED_CONTACT_COUNT"))
                    {
                        popover_text = "<strong>Threshold:</strong><br/>&nbsp;&nbsp;&nbsp; Abandoned Contact Count " + sla_threshold_operator + "&nbsp;" + sla_threshold_value + "<br/><strong>Channels:</strong><br/>" + channel_string;
                    }
                    else
                    {
                        popover_text = "<strong>Threshold:</strong><br/>&nbsp;&nbsp;&nbsp;" + sla_type +  " " + sla_threshold_operator + "&nbsp;" + sla_threshold_value + "<br/><strong>Channels:</strong><br/>" + channel_string;
                    }
                    
                    if(this_period_score >= Double.parseDouble(sla_target))
                    {
                        compliance_color = "#8bc34a";
                        compliance_value = this_period_score;
                        noncompliance_value = 100 - this_period_score;
                    }
                    else
                    {
                        compliance_color = "#ef5350";
                        compliance_value = this_period_score;
                        noncompliance_value = 100 - this_period_score;
                    }
                    %>
                    <div class="col-md-3">
                        <div class=" box mb-20">
                            <h4 class="boldFont basic-font mb-20"><%=sla_name%></h4>
                            <div class="graph-holder text-center mb-20" id="sla_container_<%=sla_id%>" style="min-width: 250px; height: 150px; margin: 0 auto">

                            </div>
                            <div class="stats clr">
                                <ul>
                                    <li class="clr">
                                        <div class="stat mb-5">
                                            <h5 class="boldFont small-font">Period</h5>
                                            <p class="regulat-font small-font m-0"><%=sla_timeframe%></p>
                                        </div>
                                    </li>
                                    <li class="clr">
                                        <div class="stat mb-5">
                                            <h5 class="boldFont small-font">Type</h5>
                                            <p class="regulat-font small-font m-0"><%=sla_type%></p>
                                        </div>
                                    </li>
                                    <li class="clr">
                                        <div class="stat mb-5">
                                            <h5 class="boldFont small-font">Target</h5>
                                            <p class="regulat-font small-font m-0"><%=sla_target%>%</p>
                                        </div>
                                    </li>
                                    <li class="clr">
                                        <div class="stat mb-5">
                                            <h5 class="boldFont small-font truncate">Previous Period</h5>
                                            <p class="regulat-font small-font m-0"><%=String.format("%1$,.2f",previous_period_score)%>%</p>
                                        </div>
                                    </li>
                                    <li class="clr">
                                        <div class="stat mb-5">
                                            <h5 class="boldFont small-font">Threshold</h5>
                                            <p class="regulat-font small-font m-0">
                                                <i class="la la-question-circle info" data-toggle="popover" data-html="true" data-content="<%=popover_text%>" data-trigger="hover" data-original-title="Threshold Info"></i>
                                            </p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <script>
                        var date_range = document.getElementById("date_range").value;
                        var events = {
                            click: function(event) 
                            {
                                window.location = "contact_sla_drilldown.jsp?sla_id=<%=sla_id%>&date_range=" + date_range;
                            }
                        };
                        Highcharts.chart('sla_container_<%=sla_id%>', {
                            chart: {
                                plotBackgroundColor: null,
                                plotBorderWidth: 0,
                                plotShadow: false
                            },
                            title: {
                                text: '<%=String.format("%1$,.2f",this_period_score)%>%',
                                align: 'center',
                                verticalAlign: 'middle',
                                y: 20
                            },
                            exporting: {
                                enabled: false
                            },
                            credits: {
                                enabled: false
                            },
                            legend: {
                                enabled: false
                            },
                            plotOptions: {
                                pie: {
                                    dataLabels: {
                                        enabled: false
                                    },
                                    startAngle: -90,
                                    endAngle: 90,
                                    center: ['50%', '100%'],
                                    size: 180,
                                    cursor: 'pointer'
                                },
                                series: {
                                    point : {
                                        events: events
                                    }
                                }
                            },
                            series: [
                            {
                                type: 'pie',
                                name: 'Value',
                                innerSize: '75%',
                                colors: ['<%=compliance_color%>', '#e9e9e9'],
                                data: [
                                    {
                                        name: 'Compliant', 
                                        y: <%=String.format("%1$,.2f",compliance_value).replace(",",".")%>,
                                        color: "<%=compliance_color%>",
                                    },
                                    {
                                        name: 'Non-Compliant',
                                        y: <%=String.format("%1$,.2f",noncompliance_value).replace(",",".")%>,
                                        dataLabels: {
                                            enabled: false
                                        }
                                    }
                                ]
                            }
                            ]
                        });
                    </script>
                    <%
                }
                %>
            </div>
    </div>
    <div class="tab-pane" id="non-reportable" role="tabpanel">

            <div class="row">
                <%
                //get all reportable contact sla's
                //get all active/reportable contact SLAs
                previous_period_score = 0;
                this_period_score = 0;
                sla_id = "";
                sla_timeframe = ""; //13 //String timeframes[] = {"DAILY", "WEEKLY", "MONTHLY", "QUARTERLY", "ANNUAL", "CUSTOM"}; String timeframes_names[] = {"Daily", "Weekly", "Monthly", "Quarterly", "Annual", "Custom"};
                sla_name = "";
                sla_type = "";
                sla_target = "";
                sla_threshold_operator = "";
                sla_threshold_value = "";
                sla_threshold_unit = "";
                popover_text = "";
                channel_string = "";
                compliance_color = "";
                compliance_value = 0;
                noncompliance_value = 0;
                //get all active/reportable contact SLAs
                String nonreportable_contact_sla[][] = db.get_sla.all_nonreportable_contact(con);
                //get scores for previous period
                for(int a = 0; a < nonreportable_contact_sla.length;a++)
                {
                    previous_period_score = support.sla_contact_calc.for_sla_id(con, nonreportable_contact_sla[a][0], previous_start_date, previous_end_date);
                    this_period_score = support.sla_contact_calc.for_sla_id(con, nonreportable_contact_sla[a][0], filter_start, filter_end);
                    sla_id = nonreportable_contact_sla[a][0];
                    sla_name = nonreportable_contact_sla[a][1];
                    sla_timeframe = support.string_case_changer.to_camel_case(nonreportable_contact_sla[a][13]);
                    sla_target = nonreportable_contact_sla[a][5];
                    sla_type = support.string_case_changer.contact_sla_type(nonreportable_contact_sla[a][4]);
                    sla_threshold_operator = support.string_case_changer.sla_threshold_operator(nonreportable_contact_sla[a][15]);
                    sla_threshold_value = nonreportable_contact_sla[a][16];
                    sla_threshold_unit = support.string_case_changer.sla_threshold_units(nonreportable_contact_sla[a][17]);
                    String channel_list[] = new String[0];
                    try 
                    {
                        JsonReader reader = Json.createReader(new StringReader(nonreportable_contact_sla[a][24]));
                        JsonObject resultObject = reader.readObject();
                        JsonArray parameters = (JsonArray) resultObject.getJsonArray("parameters");
                        JsonObject channel_array_object = parameters.getJsonObject(0); //get the first object
                        JsonArray channels = (JsonArray) channel_array_object.getJsonArray("channels");
                        channel_list = new String[channels.size()];
                        for(int b = 0; b < channels.size(); b++) //for each channel
                        {
                            JsonObject this_channel = channels.getJsonObject(b); //a single question
                            channel_list[b] = this_channel.getString("name").replace("\"", "");
                            channel_string = channel_string + "&nbsp;&nbsp;&nbsp;" + channel_list[b] + "<br/>";
                        }
                    } 
                    catch (Exception e) 
                    {
                        out.println("Exception in record=" + e);
                    }
                    popover_text = "<strong>Threshold:</strong><br/>&nbsp;&nbsp;&nbsp;" + sla_threshold_operator + "&nbsp;" + sla_threshold_value + "&nbsp;" + sla_threshold_unit + "<br/><strong>Channels:</strong><br/>" + channel_string;
                    if(this_period_score >= Double.parseDouble(sla_target))
                    {
                        compliance_color = "#8bc34a";
                        compliance_value = this_period_score;
                        noncompliance_value = 100 - this_period_score;
                    }
                    else
                    {
                        compliance_color = "#ef5350";
                        compliance_value = this_period_score;
                        noncompliance_value = 100 - this_period_score;
                    }
                    
                    %>
                    <div class="col-md-3">
                        <div class=" box mb-20">
                            <h4 class="boldFont basic-font mb-20"><%=sla_name%></h4>
                            <div class="graph-holder text-center mb-20" id="sla_container_<%=sla_id%>" style="min-width: 250px; height: 150px; margin: 0 auto">

                            </div>
                            <div class="stats clr">
                                <ul>
                                    <li class="clr">
                                        <div class="stat mb-5">
                                            <h5 class="boldFont small-font">Period</h5>
                                            <p class="regulat-font small-font m-0"><%=sla_timeframe%></p>
                                        </div>
                                    </li>
                                    <li class="clr">
                                        <div class="stat mb-5">
                                            <h5 class="boldFont small-font">Type</h5>
                                            <p class="regulat-font small-font m-0"><%=sla_type%></p>
                                        </div>
                                    </li>
                                    <li class="clr">
                                        <div class="stat mb-5">
                                            <h5 class="boldFont small-font">Target</h5>
                                            <p class="regulat-font small-font m-0"><%=sla_target%>%</p>
                                        </div>
                                    </li>
                                    <li class="clr">
                                        <div class="stat mb-5">
                                            <h5 class="boldFont small-font truncate">Previous Period</h5>
                                            <p class="regulat-font small-font m-0"><%=String.format("%1$,.2f",previous_period_score)%>%</p>
                                        </div>
                                    </li>
                                    <li class="clr">
                                        <div class="stat mb-5">
                                            <h5 class="boldFont small-font">Threshold</h5>
                                            <p class="regulat-font small-font m-0">
                                                <i class="la la-question-circle info" data-toggle="popover" data-html="true" data-content="<%=popover_text%>" data-trigger="hover" data-original-title="Threshold Info"></i>
                                            </p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <script>
                        var date_range = document.getElementById("date_range").value;
                        var events = {
                            click: function(event) 
                            {
                                window.location = "contact_sla_drilldown.jsp?sla_id=<%=sla_id%>&date_range=" + date_range;
                            }
                        };
                        Highcharts.chart('sla_container_<%=sla_id%>', {
                            chart: {
                                plotBackgroundColor: null,
                                plotBorderWidth: 0,
                                plotShadow: false
                            },
                            title: {
                                text: '<%=String.format("%1$,.2f",this_period_score)%>%',
                                align: 'center',
                                verticalAlign: 'middle',
                                y: 20
                            },
                            exporting: {
                                enabled: false
                            },
                            credits: {
                                enabled: false
                            },
                            legend: {
                                enabled: false
                            },
                            plotOptions: {
                                pie: {
                                    dataLabels: {
                                        enabled: false
                                    },
                                    startAngle: -90,
                                    endAngle: 90,
                                    center: ['50%', '100%'],
                                    size: 180,
                                    cursor: 'pointer'
                                },
                                series: {
                                    point : {
                                        events: events
                                    }
                                }
                            },
                            series: [
                            {
                                type: 'pie',
                                name: 'Value',
                                innerSize: '75%',
                                colors: ['<%=compliance_color%>', '#e9e9e9'],
                                data: [
                                    {
                                        name: 'Compliant', 
                                        y: <%=String.format("%1$,.2f",compliance_value).replace(",",".")%>,
                                        color: "<%=compliance_color%>",
                                    },
                                    {
                                        name: 'Non-Compliant',
                                        y: <%=String.format("%1$,.2f",noncompliance_value).replace(",",".")%>,
                                        dataLabels: {
                                            enabled: false
                                        }
                                    }
                                ]
                            }
                            ]
                        });
                    </script>
                    <%
                }
                %>             
            </div>
            <%
            if(nonreportable_contact_sla.length == 0)
            {
                %>
                <div class="row">
                    <div class="col-md-6 col-lg-3">
                        <h5>No Non-Reportable SLA's</h5>
                    </div>
                </div>  
                <%
            }
            %>
            <!-- end content here-->
    </div>        
</div>

<% if (embedded.equals("")) 
{
%>

    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN VENDOR JS-->

    <!-- BEGIN VENDOR JS-->
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
<!--    <script src="app-assets/js/core/app-menu.js"></script>
    <script src="app-assets/js/core/app.js"></script>-->
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->

<!--    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>-->
    <script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
    <script src="app-assets/js/scripts/popover/popover.js"></script>
    
    <script>
        function reload_page()
        {
            var date_range = document.getElementById("date_range").value;
            var URL = "contact_service_compliance.jsp?date_range=" + date_range;
            window.location.href = URL;
        }
    function apply_filters()
    {
        $("#filter_form input,#filter_form select").each(function() {

            var field = $(this);
//            console.log("filter_form_field", field);
            var field_name = field.attr('name');
            
            if (!field_name || field_name == "service_type" || field.attr('type') == "hidden" || (field.attr('type') == "radio" && !field.prop("checked")))
            {
                return;
            }
            if (field.val() && field.val() != "Any")
            {
                var label = "";
                var filter_label = "";
                if (field.attr("type") == "radio")
                {
                    label = "Predefined";
                    filter_label = field.siblings("label").html();
                } else {
                    label = field.closest("div").prev().html();
                    filter_label = field.val();
                }
                if ($("#active_filter_" + field_name).length > 0) {
                    var filter_block = $("#active_filter_" + field_name);
                    $(filter_block).children("span").html(filter_label);
                } else {
                    $("#active_filters").append(
                        $('' +
                            '<div class="clr tag dInlineBlock formField md whtbg mr-5 mb-15" id="active_filter_' + field_name + '">' +
                                '<h5 class="basic-font mediumFont">' + label + '</h5>' +
                                '<span>' + filter_label + '</span>' +
                                '<img class="icon" src="assets/images/svg/cross-icon.svg" alt="" onclick="resetFilterFieldNew(\'' + field_name + '\')">' +
                            '</div>'
                        )
                    );
                }
                setQueryStringParameter(field_name, field.val());
            } else {
                resetFilterFieldNew(field_name);
            }                
        });
    }
    
    function resetFilterFieldNew(field_name)
    {
        var filter_block = $("#active_filter_" + field_name);
        if (filter_block.length > 0)
        {
            filter_block.remove();
            var filter_field = $("#filter_form input[name='" + field_name + "'],#filter_form select[name='" + field_name + "']");
            if (filter_field.attr("type") != "radio")
            {
                filter_field.val("");
                if (field_name.indexOf("_name") !== false)
                {
                    var filter_id_fields = $("#filter_form input[name='" + field_name.replace("_name","_id") + "'],#filter_form select[name='" + field_name.replace("_name","_id") + "']");
                    if (filter_id_fields.length > 0)
                    {
                        filter_id_fields.val("");
                    }
                }
            } else {
                filter_field.prop('checked', false);
            }
            setQueryStringParameter(field_name, "");
            window.location.reload();
        }
    }
        function attachFieldListeners(container)
    {
        container.find('input[name="company"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incident_company",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="company"]').val(ui.item.label); // display the selected text
                //alert("setting is=" + ui.item.value);
                return false;
            }
        }); 

        container.find('input[name="caller_name"]').autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_user_last_first",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {

                // Set selection
                container.find('input[name="caller_name"]').val(ui.item.label); // set name
                container.find('input[name="caller_id"]').val(ui.item.value); // set id
                return false;
            }
        });

        //caller_group_name
        container.find( 'input[name="caller_group_name"]' ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_group",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                container.find('input[name="caller_group_name"]').val(ui.item.label); // display the selected text
                container.find('input[name="caller_group_id"]').val(ui.item.value); // save selected id to input
                //alert("setting is=" + ui.item.value);
                return false;
            }
        }); 
    }

    window.addEventListener("load", (event) => 
    {
        attachFieldListeners($("#filter_form"));
        apply_filters();
        $('#filters_dropdown').on('hide.bs.dropdown', function (e) 
        { // prevent dropdown close on autocomplete
            console.log("dropdown closed ", e);
            //            console.log(e.clickEvent.target.className);
            if (e.clickEvent && e.clickEvent.target && (e.clickEvent.target.className == "ui-menu-item-wrapper" || e.clickEvent.target.className == "icon" || e.clickEvent.target.className.indexOf("applyBtn") !== -1)) {
                e.preventDefault();
            }
        });

    });
    </script>

    </body>
</html>
<%
}
%>


<%
        }//end if not permission
    }//end if not logged in
%>
<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_users_add.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        boolean page_authorized = support.role.authorized(session, "USER","create");         
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String error = "";
                String username = "";
                String error_text = "";
                try
                {
                    error = request.getParameter("error").toString();
                    if(error != null && error.equalsIgnoreCase("user_exists"))
                    {
                        username = request.getParameter("username");
                        error_text = "User not added. Reason: User " + username + " already exists";
                    }
                }
                catch(Exception e)
                {
                    error = "";
                    username = "";
                }
%>
<!-- BEGIN PAGE LEVEL CSS-->
<!-- END PAGE LEVEL CSS-->

    <form action="admin_users_add" name="in_form" id="in_form" method="POST" enctype='multipart/form-data' class="needs-validation" novalidate>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        First Name
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" id="first" name="first" class="form-control" placeholder="Enter first name" required="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        MI
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" id="mi" name="mi" class="form-control" placeholder="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Last Name
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" id="last" name="last" class="form-control" placeholder="" required="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        User Name
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" id="username" name="username" class="form-control" placeholder="" required="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Password
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="password" id="password" class="form-control" name="password" placeholder="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Address <em>(1st Line)</em>
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" id="address_1" name="address_1" class="form-control" placeholder="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Address <em>(2nd Line)</em>
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" id="address_2" name="address_2" class="form-control" placeholder="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        City
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" id="city" name="city" class="form-control" placeholder="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        State
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" id="state" name="state" class="form-control" placeholder="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Zip
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" id="zip" name="zip" class="form-control" placeholder="">
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Department
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" id="department" name="department" class="form-control" placeholder="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Location
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" id="location" name="location" class="form-control" placeholder="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Site
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" id="site" name="site" class="form-control" placeholder="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Company
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" id="company" name="company" class="form-control" placeholder="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Office Phone
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" id="phone_office" name="phone_office" class="form-control" placeholder="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Mobile Phone
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" id="phone_mobile" name="phone_mobile" class="form-control" placeholder="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Email
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" id="email" name="email" class="form-control" placeholder="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        VIP
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="checkbox" id="vip" name="vip" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Users Time Zone?
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <select id="tz" name="tz" class="form-control">
                        <%
                        String default_tz = "US/Eastern";
                        String selected = "";
                        ArrayList<String[]> timezones = db.get_timezone.all(con);
                        for(int a = 0; a < timezones.size(); a++)
                        {
                            String timezone[] = timezones.get(a);
                            selected = "";
                            if(timezone[1].equalsIgnoreCase(default_tz))
                            {
                                selected = "SELECTED";
                            }
                            %>
                            <option <%=selected%> value="<%=timezone[1] + "," + timezone[2]%>">(<%=timezone[2] + ")   " + timezone[1]%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        External ID
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" id="external_id" name="external_id" class="form-control" placeholder="">
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Notes
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <input type="text" id="notes" name="notes" class="form-control" placeholder="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%
                boolean assign_roles = false;
                String disabled = "";
                String message = "";
                if (!session.getAttribute("administration").toString().equalsIgnoreCase("true") && !session.getAttribute("manager").toString().equalsIgnoreCase("true")) 
                {
                    assign_roles = false;
                    message = "&nbsp;<small><em>Your assigned Role(s) do not allow you to assign Roles to Users.</em></small>";
                    disabled = "disabled=\"disabled\"";
                }
                else
                {
                    assign_roles = true;
                }
                %>
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Roles<%=message%>
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <%
                    ArrayList<String[]> all_roles = db.get_roles.all(con);
                    for(int a = 0; a < all_roles.size(); a++ )
                    {
                        String role[] = all_roles.get(a);
                        %>
                        <div class="form-check form-check-inline">
                          <input <%=disabled%> class="form-check-input w-auto" type="checkbox" name="role_id_<%=role[0]%>" id="role_id_<%=role[0]%>" value="<%=role[0]%>">
                          <label class="form-check-label" for="role_id_<%=role[0]%>"><%=role[1]%></label>
                        </div>
                        <%
                    }
                    %>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Groups
                    </label>
                </div>
                <div class="formField clr border-0">
                    <select class="select2 form-control" id="group_ids" name="group_ids" multiple="multiple" data-placeholder="Select Group membership">
                        <%
                        //get all groups
                        ArrayList<String[]> groups = db.get_groups.all(con);
                        String group_id = "";
                        String group_name = "";
                        for(int a = 0; a < groups.size(); a++)
                        {
                            String this_group[] = groups.get(a);
                            group_id = this_group[0];
                            group_name = this_group[1];
                            %>
                            <option value="<%=group_id%>"><%=group_name%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Avatar
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="file" name="file" accept=".gif,.jpg,.jpeg,.png">
                        <label class="custom-file-label" for="file">Choose file</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-12 text-center pt-30 pb-30">
                <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                <button type="button" class="btn btn-primary-new customBtn lg waves-effect" onclick="saveUser()">
                    Save
                </button>
            </div>
        </div>                                
    </form>
<!-- BEGIN PAGE LEVEL JS-->
<!-- END PAGE LEVEL JS-->   
<%                con.close();
    
            } catch (Exception e) {
                System.out.println("Exception in admin_user_add.jsp: " + e);
                logger.debug("ERROR: Exception in admin_user_add.jsp:" + e);
            }
        } //end if not admin permission
    } //end if not redirected
%>
</body>
</html>

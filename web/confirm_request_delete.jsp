<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : incident_total_incidents
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        
        //session vars
        boolean page_authorized = support.role.authorized(session, "request","delete");          
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            String home_page = session.getAttribute("home_page").toString();
            String id = request.getParameter("id");
            
            
            
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!--Start Page level css-->
    <link rel="stylesheet" type="text/css" href="assets/css/dashboard.css">
    <!--End Page level css--><!-- END Custom CSS-->
    
    
    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <input type="hidden" name="id" id="id" value="<%=id%>"/>
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title"><i class="la la-wrench"></i>&nbsp;Request</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"> 
                                    <a href="<%=home_page%>"><i class="la la-home"></i>&nbsp;Home</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- start content here-->
            <form action="request_delete" method="POST">
                <input type="hidden" name="id" id="id" value="<%=id%>"/>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <h3>
                                        Request Delete
                                    </h3>
                                    Deleting this Request will permanently delete this Request and any data associated with it. This action can not be undone!
                                    &nbsp;&nbsp;<input class="btn btn-info" type="submit" name="submit" value="Delete"/>
                                    &nbsp;&nbsp;
                                    <button type="button" onclick="javascript:location.href='request.jsp?id=<%=id%>'" class="btn btn-blue-grey mr-1">
                                        <i class="la la-backward"></i> Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
                                    
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- BEGIN VENDOR JS-->    
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="app-assets/js/core/app-menu.js"></script>
    <script src="app-assets/js/core/app.js"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/popover/popover.js"></script>

    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>    
    
    <script>
    function clear_caller() 
    { 
        document.getElementById("caller_id").value = "";
        document.getElementById("caller_name").value = "";
    }
    function clear_assigned_to() 
    { 
        document.getElementById("assigned_to_id").value = "";
        document.getElementById("assigned_to_name").value = "";
    }
    function clear_create_by() 
    { 
        document.getElementById("create_by_id").value = "";
        document.getElementById("create_by_name").value = "";
    }
    function clear_caller_group() 
    { 
        document.getElementById("caller_group_id").value = "";
        document.getElementById("caller_group_name").value = "";
    }
    function clear_assigned_group() 
    { 
        document.getElementById("assigned_group_id").value = "";
        document.getElementById("assigned_group_name").value = "";
    }
    </script>
    <script>
        function hide_show_div() 
        {
            var x = document.getElementById("selectors");
            if (x.style.display === "none") 
            {
                x.style.display = "block";
            } 
            else 
            {
                x.style.display = "none";
            }
        }        
        
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="incident_time_start"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="incident_time_start"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="incident_time_start"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="create_date_start"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="create_date_start"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="create_date_start"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="pending_date_start"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="pending_date_start"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="pending_date_start"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="state_date_start"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="state_date_start"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="state_date_start"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="closed_date_start"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="closed_date_start"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="closed_date_start"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="incident_time_stop"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="incident_time_stop"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="incident_time_stop"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="create_date_stop"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="create_date_stop"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="create_date_stop"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="pending_date_stop"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="pending_date_stop"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="pending_date_stop"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="state_date_stop"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="state_date_stop"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="state_date_stop"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    <script type="text/javascript">
       $(function()
       {
         $('input[name="closed_date_stop"]').daterangepicker({
            "autoUpdateInput": false,
            "singleDatePicker": true,
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "locale": {
            "format": "HH:mm MM/DD/YYYY",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
             }
         });
         $('input[name="closed_date_stop"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
         });

         $('input[name="closed_date_stop"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });
       });
    </script>
    
    
    <script>
        //assigned_group
        $( function() 
        {
            $( "#caller_name" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user_last_first",
                        type: 'post',
                        dataType: "json",
                        minLength: 1,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    
                    // Set selection
                    $('#caller_name').val(ui.item.label); // set name
                    $('#caller_id').val(ui.item.value); // set id
                    return false;
                }
            });
     });   
    </script>
    <script>
        //assigned_to
        $( function() 
        {
            $( "#assigned_to_name" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user_last_first",
                        type: 'post',
                        dataType: "json",
                        minLength: 1,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#assigned_to_name').val(ui.item.label); // set name
                    $('#assigned_to_id').val(ui.item.value); // set id
                    return false;
                }
            });
     });   
    </script>
    <script>
        //created_by
        $( function() 
        {
            $( "#create_by_name" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user_last_first",
                        type: 'post',
                        dataType: "json",
                        minLength: 1,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#create_by_name').val(ui.item.label); // set name
                    $('#create_by_id').val(ui.item.value); // set id
                    return false;
                }
            });
     });   
    </script>
    <script>
        $( "#caller_group_name" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_group",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#caller_group_name').val(ui.item.label); // display the selected text
                $('#caller_group_id').val(ui.item.value); // save selected id to input
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });
    
    </script>
    <script>
        $( "#assigned_group_name" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_group",
                    type: 'post',
                    dataType: "json",
                    minLength: 2,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#assigned_group_name').val(ui.item.label); // display the selected text
                $('#assigned_group_id').val(ui.item.value); // save selected id to input
                //alert("setting is=" + ui.item.value);
                return false;
            }
        });
    
    </script>
    <script>
        $( "#priority" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_priority",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#priority').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        $( "#urgency" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_urgency",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#urgency').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        $( "#impact" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_impact",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#impact').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        $( "#category" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_category_search",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#category').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        $( "#subcategory" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_subcategory_search",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#subcategory').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        $( "#state" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incident_state_search",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#state').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        $( "#location" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incident_location",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#location').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        $( "#department" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incident_department",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#department').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        $( "#site" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incident_site",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#site').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        $( "#company" ).autocomplete(
        {
            source: function( request, response ) 
            {
                // Fetch data
                $.ajax({
                    url: "ajax_lookup_incident_company",
                    type: 'post',
                    dataType: "json",
                    minLength: 1,
                    data: 
                    {
                        search: request.term
                    },
                    success: function( data ) 
                    {
                        response( data );
                    }
                });
            },
            select: function (event, ui) 
            {
                // Set selection
                $('#company').val(ui.item.label); // display the selected text
                return false;
            }
        });
    
    </script>
    <script>
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>
    
    <!-- END PAGE LEVEL JS-->
    <script>
        $('#incident_table').dataTable( {
            "order": []
        } );
    </script>
    </body>
</html>
<%     
        }//end if not permission
    }//end if not logged in
%>
<!--Copyright 2019 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : menu
    Created on : Dec 13, 2019, 5:55:09 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.*" %>

<div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-dark navbar-without-dd-arrow navbar-shadow" role="navigation" data-menu="menu-wrapper">
    <div class="navbar-container main-menu-content" data-menu="menu-container">
        <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
            
            <li class="dropdown nav-item" data-menu="dropdown">
                <a class="dropdown-toggle nav-link" href="index.html" data-toggle="dropdown">
                    <i class="la la-home"></i><span>Home</span>
                </a>
                <ul class="dropdown-menu">
                    <li data-menu="">
                        <a class="dropdown-item" href="home_analytics.jsp" data-toggle="dropdown">Analytics Home</a>
                    </li>
                    <li data-menu="">
                        <a class="dropdown-item" href="home_service_desk.jsp" data-toggle="dropdown">Service Desk Home</a>
                    </li>
                </ul>
            </li>
            <!--<li class="dropdown nav-item" data-menu="dropdown">
                <a class="dropdown-toggle nav-link" href="index.html" data-toggle="dropdown">
                    <i class="la la-group"></i><span>Customer Experience</span>
                </a>
                <ul class="dropdown-menu">
                    <li data-menu="">
                        <a class="dropdown-item" href="cx_home.jsp" data-toggle="dropdown">CX Dashboard</a>
                    </li>
                </ul>
            </li>-->
            <li class="dropdown nav-item" data-menu="dropdown">
                <a class="dropdown-toggle nav-link" href="incident_home_support.jsp" data-toggle="dropdown">
                    <i class="la la-wrench"></i><span>Incident</span>
                </a>
                <ul class="dropdown-menu">
                    <li data-menu="">
                        <a class="dropdown-item" href="incident_home_support.jsp" data-toggle="dropdown">Incident Dashboard</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown nav-item" data-menu="dropdown">
                <a class="dropdown-toggle nav-link" href="request_home_support.jsp" data-toggle="dropdown">
                    <i class="la la-shopping-cart"></i><span>Request</span>
                </a>
                <ul class="dropdown-menu">
                    <li data-menu="">
                        <a class="dropdown-item" href="request_home_support.jsp" data-toggle="dropdown"><i class="la la-shopping-cart"></i>Request Dashboard</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown nav-item" data-menu="dropdown">
                <a class="dropdown-toggle nav-link" href="contact_home.jsp" data-toggle="dropdown">
                    <i class="la la-group"></i><span>Contact</span>
                </a>
                <ul class="dropdown-menu">
                    <li data-menu="">
                        <a class="dropdown-item" href="contact_home.jsp" data-toggle="dropdown">Contact Dashboard</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown nav-item" data-menu="dropdown">
                <a class="dropdown-toggle nav-link" href="home_service_desk.jsp" data-toggle="dropdown">
                    <i class="la la-group"></i><span>Service Desk</span>
                </a>
                <ul class="dropdown-menu">
                    <li data-menu="">
                        <a class="dropdown-item" href="home_service_desk.jsp" data-toggle="dropdown">Service Desk Dashboard</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown nav-item" data-menu="dropdown">
                <a class="dropdown-toggle nav-link" href="admin.jsp" data-toggle="dropdown">
                    <i class="la la-cog"></i><span data-i18n="nav.category.admin-panels">Administration</span>
                </a>
                <ul class="dropdown-menu">
                    <li data-menu=""><a class="dropdown-item" href="admin.jsp" data-toggle="dropdown"><i class="la la-user"></i>Administration</a></li>
                    <li data-menu=""><a class="dropdown-item" href="admin_settings.jsp" data-toggle="dropdown"><i class="la la-cog"></i>Settings</a></li>
                </ul>
            </li>
            <li class="dropdown nav-item" data-menu="dropdown">
                <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                    <i class="la la-question"></i><span>Help</span>
                </a>  
                <ul class="dropdown-menu">
                    <li data-menu=""><a class="dropdown-item" href="#" data-toggle="dropdown"><i class="la la-question"></i>Help</a></li>
                    <li data-menu=""><a class="dropdown-item" href="https://www.rdtmetrics.com/support/" target="_blank" data-toggle="dropdown"><i class="la la-life-saver"></i>Help on the Web</a></li>
                    <li data-menu=""><a class="dropdown-item" href="about.jsp" data-toggle="dropdown"><i class="la la-question"></i>About</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>

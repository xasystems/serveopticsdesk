<!--Copyright 2019 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String FINANCIAL = session.getAttribute("financial").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String CONTACT = session.getAttribute("contact").toString();
        String CX = session.getAttribute("cx").toString();
        String SURVEY = session.getAttribute("survey").toString();
        String SLA = session.getAttribute("sla").toString();        
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        String METRIC = session.getAttribute("metric").toString();
        
        
        if(!ADMINISTRATION.equalsIgnoreCase("true"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);   
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String table_column = request.getParameter("table_column");
                String temp[] = table_column.split("&seperator");
                String table = temp[0];
                String column = temp[1];
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END PAGE LEVEL CSS-->
    
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title">Edit Form Select Options</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a></li>
                                <li class="breadcrumb-item"><a href="admin.jsp">Administration</a></li>
                                <li class="breadcrumb-item"><a href="admin_system_select_fields.jsp">Select Fields</a></li>
                                <li class="breadcrumb-item"><a href="#">Select Fields</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div> <!--End breadcrumbs -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Select Field for Table <b><em><%=table%></em></b> Column <b><em><%=column%></em></b> </h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <%
                                ArrayList<String[]> fields = db.get_system_select_field.for_table_column(con, table, column);
                                /*temp[0] = rs.getString("id");
                                temp[1] = rs.getString("table");
                                temp[2] = rs.getString("column");
                                temp[3] = rs.getString("value");
                                temp[4] = rs.getString("label");
                                temp[5] = rs.getString("order");
                                temp[6] = rs.getString("active");*/
                                %>
                                <form class="form" action="admin_system_select_field_edit" method="post">
                                    <input type="hidden" name="table" id="table" value="<%=table%>"/>
                                    <input type="hidden" name="column" id="column" value="<%=column%>"/>
                                    <div class="form-body">
                                        <p><input type="button" class="btn btn-primary" id="tesss" value="Add New Row" onclick="addRow();" /></p>
                                        <table id="field_table" name="field_table" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Display Order</th>
                                                    <th>Display Value</th>
                                                    <th>Database Value</th>
                                                    <th>Active</th>
                                                    <th>&nbsp;</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%
                                                ArrayList<String[]> current_values = db.get_system_select_field.for_table_column(con, table, column);
                                                for(int a = 0; a < current_values.size(); a++)
                                                {
                                                    String current_value[] = current_values.get(a);
                                                    /*temp[0] = rs.getString("id");
                                                    temp[1] = rs.getString("table");
                                                    temp[2] = rs.getString("column");
                                                    temp[3] = rs.getString("value");
                                                    temp[4] = rs.getString("label");
                                                    temp[5] = rs.getString("order");
                                                    temp[6] = rs.getString("active");*/
                                                    String checked = "";
                                                    if(current_value[6].equalsIgnoreCase("true"))
                                                    {
                                                        checked = "CHECKED";
                                                    }
                                                    %>
                                                    <tr>
                                                        <td width="20"><input type="text" size="5" class="form-control" name="index_row_<%=current_value[5]%>" id="index_row_<%=current_value[5]%>" value="<%=current_value[5]%>"/></td>
                                                        <td><input type="text" class="form-control" name="display_row_<%=current_value[5]%>" id="display_row_<%=current_value[5]%>" value="<%=current_value[4]%>"/></td>
                                                        <td><input type="text" class="form-control" name="db_row_<%=current_value[5]%>" id="db_row_<%=current_value[5]%>" value="<%=current_value[3]%>"/></td>
                                                        <td><input type="checkbox" <%=checked%> name="active_row_<%=current_value[5]%>" id="active_row_<%=current_value[5]%>" value="ON"/></td>
                                                        <td width="30"><input type='button' class="btn btn-primary" value='Remove' onclick='removeRow(this)'></td>
                                                    </tr>
                                                    <%
                                                }
                                                %>
                                            </tbody>
                                        </table>
                                        <div class="form-actions">
                                            <button type="button" onclick="javascript:location.href='admin_system_select_fields.jsp'" class="btn btn-warning mr-1">
                                                <i class="ft-x"></i> Cancel
                                            </button>
                                            <button type="submit" class="btn btn-info">
                                                &nbsp;&nbsp;<i class="ft-edit"></i> Apply Changes&nbsp;&nbsp;
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>
    
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <!-- END PAGE LEVEL JS-->   
    <script type="text/javascript">
        var arrHead = new Array();    // array for header.
        arrHead = ['Display Order', 'Display Value', 'Database Value','Active', ''];
        
        function submit() 
        {
            var myTab = document.getElementById('field_table');
            var arrValues = new Array();
            // loop through each row of the table.
            for (row = 1; row < myTab.rows.length - 1; row++) 
            {
                // loop through each cell in a row.
                for (c = 0; c < myTab.rows[row].cells.length; c++) 
                {
                    var element = myTab.rows.item(row).cells[c];
                    if (element.childNodes[0].getAttribute('type')) 
                    {
                        arrValues.push("'" + element.childNodes[0].value + "'");
                    }
                }
            }
            // The final output.
            document.getElementById('output').innerHTML = arrValues;
            //console.log (arrValues);   // you can see the array values in your browsers console window. Thanks :-) 
        }
        
        function showMessage()
        {
            alert("Hello friends, this is message.");
        }
        // delete TABLE row function.
        function removeRow(oButton) 
        {
            var field_table = document.getElementById('field_table');
            field_table.deleteRow(oButton.parentNode.parentNode.rowIndex); // button -> td -> tr.
        }
        // now, add a new to the TABLE.
        function addRow() 
        {
            var field_table = document.getElementById('field_table');
            
            var totalRowCount = 0;
            var rowCount = 0;
            var rows = field_table.getElementsByTagName("tr");
            for (var i = 0; i < rows.length; i++) 
            {
                totalRowCount++;
                if (rows[i].getElementsByTagName("td").length > 0) 
                {
                    rowCount++;
                }
            }
            rowCount++;
            var next_row_number = rowCount;
            var rowCnt = field_table.rows.length;   // table row count.
            var tr = field_table.insertRow(rowCnt); // the table row.
            tr = field_table.insertRow(rowCnt);
            for (var c = 0; c < arrHead.length; c++) 
            {
                var td = document.createElement('td'); // table definition.
                td.setAttribute('width', '20');
                td = tr.insertCell(c);
                if (c === 0) 
                {      // the last column.
                    var ele = document.createElement('input');
                    ele.setAttribute('size', '5');
                    ele.setAttribute('type', 'text');
                    ele.setAttribute('class', 'form-control');
                    ele.setAttribute('name', 'index_row_' + next_row_number);
                    ele.setAttribute('id', 'index_row_' + next_row_number);
                    ele.setAttribute('value', next_row_number);
                    td.appendChild(ele);
                }
                else if(c === 1) 
                {      // the last column.
                    var ele = document.createElement('input');
                    ele.setAttribute('type', 'text');
                    ele.setAttribute('class', 'form-control');
                    ele.setAttribute('name', 'display_row_' + next_row_number);
                    ele.setAttribute('id', 'display_row_' + next_row_number);
                    ele.setAttribute('value', '');
                    td.appendChild(ele);
                }
                else if(c === 2) 
                {      // the last column.
                    var ele = document.createElement('input');
                    ele.setAttribute('type', 'text');
                    ele.setAttribute('class', 'form-control');
                    ele.setAttribute('name', 'db_row_' + next_row_number);
                    ele.setAttribute('id', 'db_row_' + next_row_number);
                    ele.setAttribute('value', '');
                    td.appendChild(ele);
                }
                else if(c === 3) 
                {      // the last column.
                    var ele = document.createElement('input');
                    ele.setAttribute('type', 'checkbox');
                    ele.setAttribute('width', '20');
                    ele.setAttribute('checked', 'true');
                    ele.setAttribute('name', 'active_row_' + next_row_number);
                    ele.setAttribute('id', 'active_row_' + next_row_number);
                    ele.setAttribute('value', 'checked');
                    td.appendChild(ele);
                }
                else if (c === 4) 
                {      
                    // the last column.
                    // add a button in every new row in the first column.
                    var button = document.createElement('input');
                    // set input attributes.
                    button.setAttribute('type', 'button');
                    button.setAttribute('class','btn btn-primary');
                    button.setAttribute('value', 'Remove');                    
                    // add button's 'onclick' event.
                    button.setAttribute('onclick', 'removeRow(this)');
                    td.appendChild(button);
                }
            }
        }
    </script>
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_systen_select_fields.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_systen_select_fields.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

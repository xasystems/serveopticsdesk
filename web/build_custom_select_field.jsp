<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%@ page import="org.apache.commons.text.StringEscapeUtils"%>
<%@ page import="javax.json.Json"%>
<%@ page import="javax.json.JsonArray"%>
<%@ page import="javax.json.JsonObject"%>
<%@ page import="javax.json.JsonReader"%>
<%@ page import="javax.json.JsonStructure"%>
<%@ page import="java.io.StringReader"%>

        <%
        String field_name = request.getParameter("field_name");
        String field_label = request.getParameter("field_label");
        String number_cols = request.getParameter("number_cols");
        String field_values = request.getParameter("field_values");   
        String new_record = request.getParameter("new_record"); 
        String db_value = request.getParameter("db_value"); 
        
        field_values = StringEscapeUtils.unescapeHtml4(field_values);
        %>

        <div class="col-<%=number_cols%>">
            <div class="field-title">
                <label class="formLabel md has-txt-only mb-15">
                    <%=field_label%>
                </label>
            </div>
            <div class="formField clr md px-0 border-0">
                <select class="form-control" id="<%=field_name%>" name="<%=field_name%>">
                <%
                    try
                    {
                        
                        /*{
                        "elements": {
                          "element": [
                            {
                              "id": "1",
                              "value": "Tom",
                              "label": "Tom",
                              "selected": "false"
                            },
                            {
                              "id": "2",
                              "value": "Frank",
                              "label": "Frank",
                              "selected": "false"
                            },
                            {

                              "id": "3",
                              "value": "Mary",
                              "label": "Mary",
                              "selected": "false"
                            }
                          ]
                        }
                      }*/
                        JsonReader reader = Json.createReader(new StringReader(field_values));
                        //System.out.println("service_catalog_item_info[5]=" + service_catalog_item_info[5]);
                        JsonObject jsonObject = reader.readObject();
                        //System.out.println("jsonObject=" + jsonObject);                                            
                        JsonObject elementsObject = jsonObject.getJsonObject("elements");
                        //System.out.println("approversObject=" + approversObject);   
                        JsonArray element_array = (JsonArray) elementsObject.getJsonArray("element");
                        //System.out.println("approver_array=" + approver_array); 
                        for(int a = 0; a < element_array.size(); a++)
                        {
                            JsonObject this_record = element_array.getJsonObject(a); //a single record
                            String this_record_id = this_record.getString("id");
                            String this_record_value = this_record.getString("value");
                            String this_record_label = this_record.getString("label");
                            String this_record_selected = this_record.getString("selected");
                            if(new_record.equalsIgnoreCase("true"))
                            {
                                if(this_record_selected.equalsIgnoreCase("true"))
                                {
                                    this_record_selected = "SELECTED";
                                }
                                else
                                {
                                    this_record_selected = "";
                                }
                            }
                            else
                            {
                                if(db_value.equalsIgnoreCase(this_record_value))
                                {
                                    this_record_selected = "SELECTED";
                                }
                                else
                                {
                                    this_record_selected = "";
                                }
                            }
                            out.println("<option " + this_record_selected + " value=\"" + this_record_value + "\">" + this_record_label + "</option>");
                        }
                    }
                    catch(Exception e)
                    {
                        out.println("<option value=\"ERROR\">CHECK YOUR CUSTOM FIELD DEFINITION FOR ERRORS</option>");
                    }
                    %>
                </select>
            </div>
        </div>
    
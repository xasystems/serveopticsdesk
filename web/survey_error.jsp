<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="org.apache.logging.log4j.LogManager"%>
<%@ page import="org.apache.logging.log4j.Logger"%>
<!DOCTYPE html>
    <%
    Logger logger = LogManager.getLogger();

    String error = request.getParameter("error");
    if(error==null)
    {
       error = "Unknown error has occurred";
    }
    else if(error.equalsIgnoreCase("expired"))
    {
        error = "Survey has expired";
    }
    else if(error.equalsIgnoreCase("not_found"))
    {
        error = "Survey not found";
    }
    else if(error.equalsIgnoreCase("complete"))
    {
        error = "Survey has been completed/submitted";
    }
    %>
    <jsp:include page='header_survey.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	
    <div class="app-content content">
        <div class="content-wrapper">                            
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title"><i class="la la-question"></i>&nbsp;Survey</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Details</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                 <%=error%>   
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                                
            <!-- end content here-->
        </div>        
    </div>
                    
    <jsp:include page='footer_survey.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<!--Copyright 2021 Real Data Technologies, Inc. , All rights reserved.-->
<%-- 
    Document   : request.jsp
    Created on : 18-Jan-2020, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Calendar"%> 
<%@ page import="java.util.GregorianCalendar"%>
<%@ page import="java.time.ZonedDateTime"%>
<%@ page import="java.time.format.DateTimeFormatter"%>
<%@ page import="org.apache.commons.text.StringEscapeUtils"%>
<%@ page import="java.util.*" %>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        if (!REQUEST.equalsIgnoreCase("none") || MANAGER.equalsIgnoreCase("true") || ADMINISTRATION.equalsIgnoreCase("true") ) 
        {
            String user_tz_name = "UTC";
            String user_tz_time = "+00:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "UTC";
                user_tz_time = "+00:00";
            }
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
            //filter_format.setTimeZone(TimeZone.getTimeZone(user_tz_name));
            
            //SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");            
            //timestamp_format.setTimeZone(TimeZone.getTimeZone("UTC"));
            
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            display_format.setTimeZone(TimeZone.getTimeZone(user_tz_name));
            
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020  
            date_time_picker_format.setTimeZone(TimeZone.getTimeZone(user_tz_name));
            
            java.util.Date now = new java.util.Date();
            //String now_display_format = display_format.format(now);
            //String now_timestamp_format = timestamp_format.format(now);
            //String now_incident_time = date_time_picker_format.format(now);
            Connection con = null;
            String request_info[] = new String[0];
            String selected = "";
            boolean request_not_found = false;
            String user_id = session.getAttribute("user_id").toString();
            
            try
            {
                con = db.db_util.get_contract_connection(context_dir, session);                
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
            }
            catch(Exception e)
            {
                System.out.println("request.jsp exception=" + e);
            }
            String id = request.getParameter("id");
            try
            {
                id = request.getParameter("id");
                request_info = db.get_requests.request_by_id(con, id);
                if(request_info[0].equalsIgnoreCase(""))
                {
                    request_not_found = true;
                }
            }
            catch (Exception e)
            {
                request_not_found = true;
                id = "0";
            }
            //set can edit
            boolean can_edit = false;
            if(REQUEST.equalsIgnoreCase("update") || MANAGER.equalsIgnoreCase("true") || ADMINISTRATION.equalsIgnoreCase("true") ) 
            {
                can_edit = true;
            }
            boolean is_admin = false;
            if(MANAGER.equalsIgnoreCase("true") || ADMINISTRATION.equalsIgnoreCase("true") ) 
            {
                is_admin = true;
            }
            String action = request.getParameter("action");
            String message_id = request.getParameter("message_id");
            
    %>
    <!-- BEGIN Page Level CSS-->
    <!-- END Page Level CSS-->

    <form action="request_update" method="post">
        <!--future fields-->            
        <input type="hidden" name="impact" id="impact" value=""/>
        <input type="hidden" name="urgency" id="urgency" value=""/>
        <input type="hidden" name="id" id="id" value="<%=id%>"/>
        <input type="hidden" name="service_catalog_item_id" id="service_catalog_item_id" value="<%=request_info[5]%>"/>
        <input type="hidden" name="service_catalog_id" id="catalog_item_id" value="<%=request_info[4]%>"/>
            
        <div class="row">
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Request ID
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <input name="id" id="id" type="text" readonly value="<%=request_info[0]%>" class="form-control" disabled/>
                </div>
            </div>
            <div class="col-md-8">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Description
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <input readonly="true" type="text" name="description" id="description" value="<%=request_info[29]%>" class="form-control"/> 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Request Date
                    </label>
                </div>
                <div class="formField clr md border-0 px-0 input-group d-flex px-0">
                    <%
                    String request_date = "";
                    try
                    {
                        ZonedDateTime zdt_temp_date = support.date_utils.utc_to_user_tz(user_tz_name, request_info[7]);
                        request_date = DateTimeFormatter.ofPattern("HH:mm MM/dd/yyyy").format(zdt_temp_date);
                        
                        
                        
                        System.out.println("raw=" + request_info[7]);
                        
                        
                        //ZonedDateTime zdt_temp_date = support.date_utils.utc_to_user_tz(user_tz_name, request_info[7]);
                        
                        //System.out.println("fut=" + DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(zdt_temp_date));
                        //System.out.println("tsp=" + timestamp_format.parse(request_info[7]));
                        //System.out.println("dpp=" + date_time_picker_format.format(timestamp_format.parse(request_info[7])));
                        //request_date = date_time_picker_format.format(timestamp_format.parse(request_info[7]));
                    }
                    catch (Exception e)
                    {
                        request_date = "";
                        System.out.println("Exception on request_date=" + e);
                    }
                    //String incident_time = date_time_picker_format.format(timestamp_format.parse(incident_info[2]).getTime());
                    %>
                    <input type='text' id="request_date" name="request_date" value="<%=request_date%>" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Department&nbsp;<i class="la la-search" title="This is a lookup field. Type in at least two characters for popup options. It should be populated when the 'Requested for' user is selected. If not, then it can be set manually. Any text value will be accepted." style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <input type="text" id="department" name="department" value="<%=request_info[17]%>" class="form-control">
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Assigned to Group&nbsp;<i class="la la-search" title="This is a lookup field. Manually enter values will be ignored, use the lookup feature to select a valid option. Enter at least two characters of the Group and click on one of the popup options. If the Group is not available, use the 'Administration' 'Groups' function to add the Group." style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <input type="hidden" name="assigned_group_id" id="assigned_group_id" value="<%=request_info[11]%>"/>
                    <input type="text" name="assigned_group_name" id="assigned_group_name" value="<%=request_info[40]%>" class="form-control" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Priority
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <%
                    ArrayList<String[]> priority_select = db.get_system_select_field.active_for_table_column(con, "request", "priority");
                    %>
                    <select name="priority" id="priority" class="form-control">
                        <option value=""></option>
                        <%
                        for(int a = 0; a < priority_select.size(); a++)
                        {
                            String select_option[] = priority_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            if(request_info[22].equalsIgnoreCase(select_option[3]))
                            {
                                selected = "SELECTED";
                            }
                            else
                            {
                                selected = "";
                            }
                            %>
                            <option <%=selected%> value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>                                           
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Site&nbsp;<i class="la la-search" title="This is a lookup field. Type in at least two characters for popup options. It should be populated when the 'Requested for' user is selected. If not, then it can be set manually. Any text value will be accepted." style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <input type="text" id="site" name="site" value="<%=request_info[18]%>" class="form-control">
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        
                    
                    Assigned to&nbsp;
                    <i class="la la-search" style="font-size: 14px;"></i>
                    <i class="la la-question-circle" style="font-size: 14px;" title="This field is filtered by the Assigned to Group field. If Group Unassigned is selected, then all users will be available. If a specific group is selected, then only the members of that group will be available. If the Assigned to Group is changed this field will be cleared."></i>
                
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <input type="hidden" name="assigned_to_id" id="assigned_to_id" value="<%=request_info[12]%>"/>
                    <input type="text" name="assigned_to_name" id="assigned_to_name" value="<%=request_info[41]%>" class="form-control" placeholder="Enter the Assigned To group's name"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        State
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <%
                    ArrayList<String[]> state_select = db.get_system_select_field.active_for_table_column(con, "request", "state");
                    %>
                    <select name="state" id="state" class="form-control">
                        <%
                         
                        for(int a = 0; a < state_select.size(); a++)
                        {
                            String select_option[] = state_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            if(request_info[23].equalsIgnoreCase(select_option[3]))
                            {
                                selected = "SELECTED";
                            }
                            else
                            {
                                selected = "";
                            }
                            %>
                            <option <%=selected%> value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Company&nbsp;<i class="la la-search" title="This is a lookup field. Type in at least two characters for popup options. It should be populated when the 'Requested for' user is selected. If not, then it can be set manually. Any text value will be accepted." style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <input type="text" id="company" name="company" value="<%=request_info[19]%>" class="form-control">
                </div>
            </div> 
<!--            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Requires Follow Up</i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="rfu" name="rfu" value="" class="form-control">
                </div>
            </div>-->
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Caller&nbsp;<i class="la la-search" title="This is a lookup field. To start the lookup enter a least two characters of the users 'User Name' then click on one of the available options that popup. Manually entered values will be ignored. If the user is not available in the list, then add the user via the 'Administration' 'Users' function." style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <input type="hidden" name="requested_for_id" id="requested_for_id" value="<%=request_info[14]%>"/>
                    <input type="text" name="requester_username" id="requester_username" value="<%=request_info[51]%>" class="form-control" placeholder="Enter the requesters username"/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Location&nbsp;<i class="la la-search" title="This is a lookup field. Type in at least two characters for popup options. It should be populated when the 'Requested for' user is selected. If not, then it can be set manually. Any text value will be accepted."  style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">                                            
                    <input type="text" id="location" name="location" value="<%=request_info[16]%>" class="form-control">
                </div>
            </div>
<!--            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Bills Product Serial No.</i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="bpsn" name="bpsn" value="" class="form-control">
                </div>
            </div>-->
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Caller Group&nbsp;<i class="la la-search" title="This is a lookup field. Manually enter values will be ignored, use the lookup feature to select a valid option. Enter at least two characters of the Group and click on one of the popup options. If the Group is not available, use the 'Administration' 'Groups' function to add the Group." style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <input type="hidden" name="requested_for_group_id" id="requested_for_group_id" value="<%=request_info[15]%>"/>
                    <input type="text" name="requester_group_name" id="requester_group_name" value="<%=request_info[56]%>" class="form-control" placeholder="Enter the requesters Group"/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Category
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select class="form-control" id="category" name="category">
                        <option value=""></option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Resolved Date
                    </label>
                </div>
                <div class="formField clr md border-0 px-0 input-group d-flex px-0">
                    <%
                    String resolve_date = "";
                    try
                    {
                        //resolve_date = date_time_picker_format.format(timestamp_format.parse(request_info[9]));
                        ZonedDateTime zdt_temp_date = support.date_utils.utc_to_user_tz(user_tz_name, request_info[9]);
                        resolve_date = DateTimeFormatter.ofPattern("HH:mm MM/dd/yyyy").format(zdt_temp_date);
                    }
                    catch (Exception e)
                    {
                        resolve_date = "";
                        System.out.println("Exception on resolve_date=" + e);
                    }
                    //String incident_time = date_time_picker_format.format(timestamp_format.parse(incident_info[2]).getTime());
                    %>
                    <input type='text' id="resolve_date" name="resolve_date" value="<%=resolve_date%>" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Request Create By&nbsp;<i class="la la-search" title="This is a lookup field. To start the lookup enter a least two characters of the users 'User Name' then click on one of the available options that popup. Manually entered values will be ignored. If the user is not available in the list, then add the user via the 'Administration' 'Users' function." style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <input type="hidden" name="create_by_id" id="create_by_id" value="<%=request_info[13]%>"/>
                    <input type="text" name="create_by_username" id="create_by_username" value="<%=request_info[46]%>" class="form-control" placeholder="Enter the creaters username"/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Subcategory
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select class="form-control" id="subcategory" name="subcategory">
                        <option value=""></option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Due Date <i class="la la-question" title="This field is calculated by the system based on 'Priority','Request Date' and the Delivery values set in the Catalog Item definition. Due Date will be updated after clicking on 'Update' button" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md border-0 px-0 input-group d-flex px-0">
                    <%
                    String due_date = "";
                    try
                    {
                        //due_date = date_time_picker_format.format(timestamp_format.parse(request_info[8]));
                        ZonedDateTime zdt_temp_date = support.date_utils.utc_to_user_tz(user_tz_name, request_info[8]);
                        due_date = DateTimeFormatter.ofPattern("HH:mm MM/dd/yyyy").format(zdt_temp_date);
                    }
                    catch (Exception e)
                    {
                        due_date = "";
                    }
                    //String incident_time = date_time_picker_format.format(timestamp_format.parse(incident_info[2]).getTime());
                    %>
                    <input readonly="true" type='text' id="due_date" name="due_date" value="<%=due_date%>" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Requester VIP?
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <%
                    if(request_info[55].equalsIgnoreCase("true"))
                    {
                        %>
                        <input name="vip" id="vip" CHECKED type="checkbox" disabled=""/>&nbsp;<em>Set by Requester's profile</em>
                        <%
                    }
                    else
                    {
                        %>
                        <input name="vip" id="vip" type="checkbox" disabled=""/>&nbsp;<em>Set by Requester's profile</em>
                        <%
                    }
                    %>
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Contact Method
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <%
                    ArrayList<String[]> contact_method_select = db.get_system_select_field.active_for_table_column(con, "request", "contact_method");
                    %>
                    <select name="contact_method" id="contact_method" class="form-control">
                        <option value=""></option>
                        <%
                        for(int a = 0; a < contact_method_select.size(); a++)
                        {
                            String select_option[] = contact_method_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            if(request_info[24].equalsIgnoreCase(select_option[3]))
                            {
                                selected = "SELECTED";
                            }
                            else
                            {
                                selected = "";
                            }
                            %>
                            <option <%=selected%> value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Approval
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <%
                    ArrayList<String[]> approval_select = db.get_system_select_field.active_for_table_column(con, "request", "approval");
                    %>
                    <select name="approval" id="approval" class="form-control">
                        <%
                         
                        for(int a = 0; a < approval_select.size(); a++)
                        {
                            String select_option[] = approval_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            if(request_info[25].equalsIgnoreCase(select_option[3]))
                            {
                                selected = "SELECTED";
                            }
                            else
                            {
                                selected = "";
                            }
                            %>
                            <option <%=selected%> value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Closure Date
                    </label>
                </div>
                <div class="formField clr md border-0 px-0 input-group d-flex px-0">
                    <%
                    String closed_date = "";
                    try
                    {
                        //closed_date = date_time_picker_format.format(timestamp_format.parse(request_info[10]));
                        ZonedDateTime zdt_temp_date = support.date_utils.utc_to_user_tz(user_tz_name, request_info[10]);
                        closed_date = DateTimeFormatter.ofPattern("HH:mm MM/dd/yyyy").format(zdt_temp_date);
                    }
                    catch (Exception e)
                    {
                        closed_date = "";
                        System.out.println("Exception on closed_date=" + e);
                    }
                    //String incident_time = date_time_picker_format.format(timestamp_format.parse(incident_info[2]).getTime());
                    %>
                    <input type='text' id="closed_date" name="closed_date" value="<%=closed_date%>" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Closure Reason
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <%
                    ArrayList<String[]> closed_reason_select = db.get_system_select_field.active_for_table_column(con, "request", "closed_reason");
                    %>
                    <select name="closed_reason" id="closed_reason" class="form-control">
                        <%
                        String closed_reason = "";
                        try
                        {
                            if(request_info[30] == null)
                            {
                                closed_reason = "";
                                %>
                                <option selected="true" value=""></option>
                                <%
                            }
                            else
                            {
                                closed_reason = request_info[30];
                                %>
                                <option value=""></option>
                                <%
                            }
                        }
                        catch(Exception e)
                        {
                            closed_reason = "";
                        }
                        
                        for(int a = 0; a < closed_reason_select.size(); a++)
                        {
                            String select_option[] = closed_reason_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            if(closed_reason.equalsIgnoreCase(select_option[3]))
                            {
                                selected = "SELECTED";
                            }
                            else
                            {
                                selected = "";
                            }
                            %>
                            <option <%=selected%> value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Notes <em>(Customer viewable)</em>
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <%
                    String new_notes = request_info[27];
                    if(action != null && message_id != null && action.equalsIgnoreCase("follow-up_request"))
                    {
                        String message_info[] = db.get_message.message_by_id(con, message_id);
                        new_notes = request_info[27] 
                                + "<br>---------------------------------------------------------<br>"
                                + "<br>Subject:" + message_info[5]
                                + "<br>Body:" + message_info[7]
                                + "<br>Activity Log:" + message_info[16];
                    }
                    %>
                    <textarea name="notes" id="notes" cols="30" rows="5" class="ckeditor"><%=new_notes%></textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Notes <em>(Service Desk)</em>
                    </label>
                </div>
                <div class="formField clr md border-0 px-0">
                    <textarea name="desk_notes" id="desk_notes" cols="30" rows="5" class="ckeditor"><%=request_info[28]%></textarea>
                </div>
            </div>
        </div> 
        <div class="row ">
            <div class="col-md-12 text-center pt-30 pb-30">
                <button <%=((request_not_found || !can_edit) ? "disabled" : "")%> type="button" class="btn btn-outline-primary customBtn lg waves-effect">Cancel</button>
                <button type="button" class="btn btn-primary-new customBtn lg waves-effect save-button" role="button">
                    Save
                </button>
            </div>
        </div>                                
    </form>
    <!-- END PAGE LEVEL JS-->
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>
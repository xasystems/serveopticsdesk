<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : service_catalog_item
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="org.apache.commons.text.StringEscapeUtils"%>
<%@ page import="org.apache.commons.lang3.BooleanUtils" %>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        if (!session.getAttribute("self_service").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("true") || session.getAttribute("manager").toString().equalsIgnoreCase("true")) 
        {
            
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            filter_format.setTimeZone(TimeZone.getTimeZone("UTC"));
            session.setAttribute("home_page", "home_service_catalog.jsp");
            String home_page = session.getAttribute("home_page").toString();
            GregorianCalendar first_of_month = new GregorianCalendar();
            first_of_month.set(Calendar.DAY_OF_MONTH, 1);
            first_of_month.set(Calendar.HOUR_OF_DAY, 0);
            first_of_month.set(Calendar.MINUTE, 0);
            first_of_month.set(Calendar.SECOND, 0);     
            GregorianCalendar now = new GregorianCalendar();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session); 
            String user_id = session.getAttribute("user_id").toString();
            String user_info[] = db.get_users.by_id(con, user_id);
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            String catalog_item_id = request.getParameter("id");
            String catalog_item_info[] = db.get_service_catalog.service_catalog_items_by_id(con, catalog_item_id);
            String message_id = request.getParameter("message_id");
            String request_user_id = request.getParameter("user_id");
            String requested_for_user_info[] = {"","","","","","","","","","","","","","","","","","","","","","","",""};
            Boolean embedded = request.getParameter("embedded") != null && BooleanUtils.toBoolean(request.getParameter("embedded"));
            try 
            {
                requested_for_user_info = db.get_users.by_id(con, request_user_id);
                
            }
            catch(Exception e)
            {
                
            }
    %>
    <% if (!embedded) {%>
        <jsp:include page='header.jsp'>
            <jsp:param name="page_type" value=""/>
        </jsp:include>	

        <jsp:include page='menu_service_desk.jsp'>
            <jsp:param name="active_menu" value=""/>
        </jsp:include>	
        <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
        <link rel="stylesheet" href="app-assets/css/colors.css">

    <% } %>
        <h4 class="title xl-font boldFont mb-15 pt-15">        
            <span class="txt"><%=catalog_item_info[2]%></span>
        </h4>
        <p><%=catalog_item_info[3]%></p>
        <div class="row mb-30">
            <div class="col-md-5 col-lg-5">
                <div class="imgHolder dFlex">
                <% if (catalog_item_info[38] != null && !catalog_item_info[38].equals("null")) { %>
                        <img class="img-responsive" src="get_attachment?uuid=<%=catalog_item_info[38]%>&name=image" alt="">
                <% } %>
                </div>
            </div>
            <div class="col-md-7 col-lg-7">
            </div>
        </div>
                <form action="service_catalog_item" method="post">
                    <input type="hidden" name="catalog_item_id" id="catalog_item_id" value="<%=catalog_item_id%>"/>
                                <div class="row">
                                    <div class="col-6">
                                        <label>
                                            Requesting this Item for&nbsp;&nbsp;<a onclick="clear_requested_for()" href="javascript:void(0)"><small>(clear)</small></a>
                                            <a data-target="user" data-user-id="0" class="btn btn-success customBtn lg waves-effect rightSidePanelUserOpener" href="javascript:void(0)">
                                                <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>New User
                                            </a>
                                        </label>
                                        <input type="hidden" name="requested_for_id" id="requested_for_id" value="<%=requested_for_user_info[0]%>"/>
                                        <input type="text" name="requested_for_name" id="requested_for_name" value="<%=requested_for_user_info[1]%>" class="form-control" placeholder="Requested For username"/>

                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        Delivery Option/Cost
                                        <table id="cost_table" class="table table-striped table-bordered zero-configuration">
                                            <thead>
                                                <tr>
                                                    <th class="no-sort">Priority</th>
                                                    <th class="no-sort">Fixed Cost</th>
                                                    <th class="no-sort">Recurring Cost</th>
                                                    <th class="no-sort">Delivery Time</th>
                                                    <th class="no-sort">Delivered By</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <input type="radio" name="priority" value="Critical"/>
                                                        &nbsp;Critical 
                                                    </td>
                                                    <td>
                                                        $<%=catalog_item_info[7]%>
                                                    </td>
                                                    <td>
                                                        $<%=catalog_item_info[8]%>&nbsp;<%=catalog_item_info[9]%>
                                                    </td>
                                                    <td>
                                                        <%=catalog_item_info[10]%>&nbsp;<%=catalog_item_info[11]%>
                                                    </td>
                                                    <td>
                                                        <%
                                                        String group_info[] = db.get_groups.by_id(con, catalog_item_info[13]);
                                                        out.print(group_info[1]);
                                                        %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="radio" name="priority" value="High"/>
                                                        &nbsp;High 
                                                    </td>
                                                    <td>
                                                        $<%=catalog_item_info[14]%>
                                                    </td>
                                                    <td>
                                                        $<%=catalog_item_info[15]%>&nbsp;<%=catalog_item_info[16]%>
                                                    </td>
                                                    <td>
                                                        <%=catalog_item_info[17]%>&nbsp;<%=catalog_item_info[18]%>
                                                    </td>
                                                    <td>
                                                        <%
                                                        group_info = db.get_groups.by_id(con, catalog_item_info[20]);
                                                        out.print(group_info[1]);
                                                        %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="radio" name="priority" value="Medium"/>
                                                        &nbsp;Medium 
                                                    </td>
                                                    <td>
                                                        $<%=catalog_item_info[21]%>
                                                    </td>
                                                    <td>
                                                        $<%=catalog_item_info[22]%>&nbsp;<%=catalog_item_info[23]%>
                                                    </td>
                                                    <td>
                                                        <%=catalog_item_info[24]%>&nbsp;<%=catalog_item_info[25]%>
                                                    </td>
                                                    <td>
                                                        <%
                                                        group_info = db.get_groups.by_id(con, catalog_item_info[27]);
                                                        out.print(group_info[1]);
                                                        %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input checked="true" type="radio" name="priority" value="Low"/>
                                                        &nbsp;Low 
                                                    </td>
                                                    <td>
                                                        $<%=catalog_item_info[28]%>
                                                    </td>
                                                    <td>
                                                        $<%=catalog_item_info[29]%>&nbsp;<%=catalog_item_info[30]%>
                                                    </td>
                                                    <td>
                                                        <%=catalog_item_info[31]%>&nbsp;<%=catalog_item_info[32]%>
                                                    </td>
                                                    <td>
                                                        <%
                                                        group_info = db.get_groups.by_id(con, catalog_item_info[34]);
                                                        out.print(group_info[1]);
                                                        %>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>             
                                <div class="row">
                                    <div class="col-md-12">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width danger required-question">
                                                <%
                                                String unEscaped_required_user_input_text = StringEscapeUtils.unescapeHtml4(catalog_item_info[35]);
                                                out.print(unEscaped_required_user_input_text);
                                                %>
                                            </label>
                                            <div class="formField md full-width mr-0 px-0 border-0">
                                                <input type="hidden" name="required_info" id="required_info" value="<%=catalog_item_info[35]%>"/>
                                                <textarea rows="3" name="required_info_response" id="required_info_response" class="form-control ckeditor"></textarea>
                                            </div>
                                        </li>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <li class="mb-10 clr">
                                            <label class="field-label full-width">
                                                Optional Information: 
                                                <%
                                                String unEscaped_optional_user_input_text = StringEscapeUtils.unescapeHtml4(catalog_item_info[36]);
                                                out.print(unEscaped_optional_user_input_text);
                                                %>
                                            </label>
                                            <div class="formField md full-width mr-0 px-0 border-0">
                                                <input type="hidden" name="optional_info" id="optional_info" value="<%=catalog_item_info[36]%>"/>
                                                <textarea rows="3" name="optional_info_response" id="optional_info_response" class="form-control ckeditor" placeholder="Enter any Optional Information Here"></textarea>
                                            </div>
                                        </li>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 mt-1 text-center">
                                        <input class="btn btn-info" type="submit" name="submit" value="Submit Request"/>
                                    </div>
                                </div>                
                </form>
    <% if (!embedded) {%>
                            </div>
                        </div>
                    </div>  
                <!-- end content here-->
            </div>        
        </div>
        <jsp:include page='footer.jsp'>
            <jsp:param name="parameter" value=""/>
        </jsp:include>	
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
        <script src="app-assets/vendors/js/editors/ckeditor/ckeditor.js"></script>
        <script>    
           $( function() 
           {
                //requested_for_id
                $( "#requested_for_name" ).autocomplete(
                {
                    source: function( request, response ) 
                    {
                        // Fetch data
                        $.ajax({
                            url: "ajax_lookup_user",
                            type: 'post',
                            dataType: "json",
                            minLength: 2,
                            data: 
                            {
                                search: request.term
                            },
                            success: function( data ) 
                            {
                                response( data );
                            }
                        });
                    },
                    select: function (event, ui) 
                    {
                        // Set selection
                        $('#requested_for_name').val(ui.item.label); // display the selected text
                        $('#requested_for_id').val(ui.item.value); // save selected id to input
                        return false;
                    }
                });
            });
        </script>
        <!-- END PAGE LEVEL JS-->
    <% } %>

<script>
if (typeof clear_requested_for == "undefined") { 
    function clear_requested_for() 
    { 
        document.getElementById("requested_for_id").value = "";
        document.getElementById("requested_for_name").value = "";
    }
}
</script>


    </body>
</html>
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>

<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="rtl">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
    <title>Chartjs Pie &amp; Doughnut Chart - Modern Admin - Clean Bootstrap 4 Dashboard HTML Template + Bitcoin Dashboard</title>
    <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/vendors.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/app.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/custom-rtl.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/colors/palette-gradient.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style-rtl.css">
    <!-- END Custom CSS-->
  </head>
  <body class="horizontal-layout horizontal-menu horizontal-menu-padding 2-columns   menu-expanded" data-open="click" data-menu="horizontal-menu" data-col="2-columns">

    <!-- fixed-top-->
    

    
    
    <script src="app-assets/vendors/js/charts/chart.min.js"></script>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


    

    <div class="app-content container center-layout mt-2">
      <div class="content-wrapper">
        
        <div class="content-body"><!-- Pie charts section start -->
<section id="chartjs-pie-charts">
    
    <div class="row">

        <!-- Simple Doughnut Chart -->
        <div class="col-md-6 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Simple Doughnut Chart</h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <h3>Title</h3>
                        
                        <canvas id="myChart" height="100"></canvas>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6 container-fixed">
            <center><h3>Incidents</h3></center>
            <canvas id="pieChart" height="80" data-toggle="tooltip" data-placement="top"></canvas>
            </div>
    </div>

    
</section>
<!-- // Pie charts section end -->
        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
     
  

<script>
var canvasP = document.getElementById("pieChart");
var ctxP = canvasP.getContext('2d');
var myPieChart = new Chart(ctxP, {
   type: 'doughnut',
   data: {
      labels: ["Critical", "High", "Medium", "Low"],
      datasets: [{
         data: [1, 5, 10, 20],
          backgroundColor: [
                '#ef5350',    // color for data at index 0
                '#ffa726',   // color for data at index 1
                '#ffee58',  // color for data at index 2
                '#64b5f6'  // color for data at index 3
            ],
         hoverBackgroundColor: ["#DF0101", "#FFBF00", "#FFFF00", "#00BFFF"]
      }]
   },
   options: {
      legend: {
         display: false
      }
   }
});

canvasP.onclick = function(e) {
   var slice = myPieChart.getElementAtEvent(e);
   if (!slice.length) return; // return if not clicked on slice
   var label = slice[0]._model.label;
   switch (label) {
      // add case for each label/slice
      case 'Critical':
         parent.location='index.html';
         break;
      case 'High':
         parent.location='index.html';
         break;
      case 'Medium':
         parent.location='index.html';
         break;
      case 'Low':
         parent.location='index.html';
         break;
   }
}
  </script>
  
    
    

    <!-- BEGIN VENDOR JS-->
    <script src="app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/charts/chart.min.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="app-assets/js/core/app-menu.js"></script>
    <script src="app-assets/js/core/app.js"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    
    <!-- END PAGE LEVEL JS-->
  </body>
</html>
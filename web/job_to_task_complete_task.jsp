<!--Copyright 2020 Real Data Technologies, Inc. , All rights reserved.-->
<%-- 
    Document   : project_task_complete_task.jsp
    Created on : 18-Jan-2020, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>

<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        if (PROJECT.contains("create") || ADMINISTRATION.equalsIgnoreCase("true") || MANAGER.equalsIgnoreCase("true")) 
        {
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
            java.util.Date now = new java.util.Date();
            String now_display_format = display_format.format(now);
            String now_timestamp_format = timestamp_format.format(now);
            String now_project_time = date_time_picker_format.format(now);
            Connection con = null;
            try
            {
                con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
            }
            catch(Exception e)
            {
                System.out.println("project_new.jsp exception=" + e);
            }
            String project_id = request.getParameter("project_id");
            String project_info[] = db.get_projects.project_by_id(con, project_id);
            String job_id = request.getParameter("job_id");
            String job_info[] = db.get_jobs.job_by_id(con, job_id);
            
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <!-- Page specific CSS-->
    <link rel="stylesheet" href="app-assets/css/plugins/ui/jqueryui.css">
    <!--End Page specific CSS-->
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END Page Level CSS-->
    
    
    <div class="app-content content">
        <form action="project_task_convert" method="post">
            <input type="hidden" id="project_id" name="project_id" value="<%=project_id%>"/>
            <input type="hidden" id="job_id" name="job_id" value="<%=job_id%>"/>
            <!--future fields-->
            <div class="content-wrapper">
                <!-- start content here-->
                <div class="content-header row">
                    <div class="content-header-left col-md-6 col-12 mb-2">
                        <h3 class="content-header-title"><i class="la la-building"></i>&nbsp;New Task for Project <em><%=project_info[1]%></em> </h3>
                        <div class="row breadcrumbs-top">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<%=home_page%>">Home</a></li>
                                    <li class="breadcrumb-item"><a href="project.jsp?id=<%=project_id%>">Project</a></li>
                                    <li class="breadcrumb-item active">New Project Task</li>
                                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-info" type="submit" name="submit" value="Submit"/></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Create a New Project Task</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>                                    
                                </div>
                            </div> 
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    
                                    <div class="row">
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Task Name</label>
                                            <input name="name" id="name" type="text" value="<%=job_info[1]%>" class="form-control"/>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Description</label>
                                            <input type="text" name="description" id="description" value="<%=job_info[2]%>" class="form-control"/>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Assigned To Group&nbsp;<i class="la la-search" style="font-size: 14px;"></i></label>
                                            <input type="hidden" name="assigned_group_id" id="assigned_group_id" value="<%=job_info[3]%>"/>
                                            <input type="text" name="assigned_group_name" id="assigned_group_name" value="<%=job_info[14]%>" class="form-control" />
                                        </div>                                        
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Assigned To&nbsp;<i class="la la-search" style="font-size: 14px;"></i></label>
                                            <input type="hidden" name="assigned_to_id" id="assigned_to_id" value="<%=job_info[4]%>"/>
                                            <input type="text" name="assigned_to_id_username" id="assigned_to_id_username" value="<%=job_info[15]%>" class="form-control"/>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Priority</label>
                                            <%
                                            ArrayList<String[]> priority_select = db.get_system_select_field.active_for_table_column(con, "projects", "priority");
                                            %>
                                            <select name="priority" id="priority" class="form-control">
                                                <option value=""></option>
                                                <%
                                                for(int a = 0; a < priority_select.size(); a++)
                                                {
                                                    String select_option[] = priority_select.get(a);
                                                    //select_option[3] = value    select_option[4] = label
                                                    %>
                                                    <option value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                                                    <%
                                                }
                                                %>
                                            </select>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Status</label>
                                            <%
                                            ArrayList<String[]> status_select = db.get_system_select_field.active_for_table_column(con, "projects", "status");
                                            %>
                                            <select name="status" id="status" class="form-control">
                                                <option value=""></option>
                                                <%                                                 
                                                for(int a = 0; a < status_select.size(); a++)
                                                {
                                                    String select_option[] = status_select.get(a);
                                                    //select_option[3] = value    select_option[4] = label
                                                    %>
                                                    <option value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                                                    <%
                                                }
                                                %>
                                            </select>
                                        </div>
                                    </div>    
                                    <div class="row">
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Scheduled Start Date</label>
                                            <div class='input-group'>
                                                <%
                                                String scheduled_start_date = "";
                                                try
                                                {
                                                    java.util.Date now2 = new java.util.Date();
                                                    now2 = timestamp_format.parse(job_info[7]);
                                                    scheduled_start_date = date_time_picker_format.format(now2);
                                                }
                                                catch(Exception e)
                                                {
                                                    scheduled_start_date = "";
                                                }
                                                %>
                                                <input type='text' id="scheduled_start_date" name="scheduled_start_date" value="<%=scheduled_start_date%>" class="form-control datetime" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <span class="la la-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Actual Start Date</label>
                                            <div class='input-group'>
                                                <%
                                                String actual_start_date = "";
                                                try
                                                {
                                                    java.util.Date now2 = new java.util.Date();
                                                    now2 = timestamp_format.parse(job_info[8]);
                                                    actual_start_date = date_time_picker_format.format(now2);
                                                }
                                                catch(Exception e)
                                                {
                                                    actual_start_date = "";
                                                }
                                                %>
                                                <input type='text' id="actual_start_date" name="actual_start_date" value="<%=actual_start_date%>" class="form-control datetime" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <span class="la la-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Scheduled End Date</label>
                                            <div class='input-group'>
                                                <%
                                                String scheduled_end_date = "";
                                                try
                                                {
                                                    java.util.Date now2 = new java.util.Date();
                                                    now2 = timestamp_format.parse(job_info[9]);
                                                    scheduled_end_date = date_time_picker_format.format(now2);
                                                }
                                                catch(Exception e)
                                                {
                                                    scheduled_end_date = "";
                                                }
                                                %>
                                                <input type='text' id="scheduled_end_date" name="scheduled_end_date" value="<%=scheduled_end_date%>" class="form-control datetime" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <span class="la la-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Actual End Date</label>
                                            <div class='input-group'>
                                                <%
                                                String actual_end_date = "";
                                                try
                                                {
                                                    java.util.Date now2 = new java.util.Date();
                                                    now2 = timestamp_format.parse(job_info[10]);
                                                    actual_end_date = date_time_picker_format.format(now2);
                                                }
                                                catch(Exception e)
                                                {
                                                    actual_end_date = "";
                                                }
                                                %>
                                                <input type='text' id="actual_end_date" name="actual_end_date" value="<%=actual_end_date%>" class="form-control datetime" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <span class="la la-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Estimated Duration <em>(hours)</em></label>
                                            <input name="estimated_duration" id="estimated_duration" type="text" value="<%=job_info[11]%>" class="form-control"/>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-12">
                                            <label><br>Actual Duration <em>(hours)</em></label>
                                            <input name="actual_duration" id="actual_duration" type="text" value="<%=job_info[12]%>" class="form-control"/>
                                        </div>
                                    </div> 
                                    <div class="row">
                                        <div class="col-12">
                                            <br>
                                            <p class="text-muted">Notes</p>
                                            <textarea class="form-control" name="notes" id="notes" rows="5"><%=job_info[13]%></textarea>  
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-12 mt-1">
                                            <hr>
                                        </div>
                                    </div>        
                                            
                                    <div class="row">
                                        <div class="col-12 mt-1">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-info" type="submit" name="submit" value="Submit"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 mt-1">
                                            &nbsp;
                                        </div>
                                    </div>  
                                    <div class="row">
                                        <div class="col-12 mt-1">
                                            &nbsp;
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>
                <!-- end content here-->
            </div> 
        </form>
    </div>
    <br>&nbsp;
    <br>&nbsp;
    <br>&nbsp;
    <br>&nbsp;
    
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN VENDOR JS-->    
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="app-assets/js/core/app-menu.js"></script>
    <script src="app-assets/js/core/app.js"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/popover/popover.js"></script>
    
    
    
    <script>     
       $( function() 
       {
            //owner
            $( "#assigned_to_id_username" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#assigned_to_id_username').val(ui.item.label); // display the selected text
                    $('#assigned_to_id').val(ui.item.value); // save selected id to input
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            });
            //caller_group_name
            $( "#assigned_group_name" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_group",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#assigned_group_name').val(ui.item.label); // display the selected text
                    $('#assigned_group_id').val(ui.item.value); // save selected id to input
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            });
            //manager
            $( "#manager_username" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#manager_username').val(ui.item.label); // display the selected text
                    $('#manager_id').val(ui.item.value); // save selected id to input
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            });
            //manager
            $( "#tech_lead_username" ).autocomplete(
            {
                source: function( request, response ) 
                {
                    // Fetch data
                    $.ajax({
                        url: "ajax_lookup_user",
                        type: 'post',
                        dataType: "json",
                        minLength: 2,
                        data: 
                        {
                            search: request.term
                        },
                        success: function( data ) 
                        {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) 
                {
                    // Set selection
                    $('#tech_lead_username').val(ui.item.label); // display the selected text
                    $('#tech_lead_id').val(ui.item.value); // save selected id to input
                    //alert("setting is=" + ui.item.value);
                    return false;
                }
            });
        });
    </script>
    
    <script>
        $(function()
        {
          $('input[name="scheduled_start_date"]').daterangepicker({
              "autoUpdateInput": false,
              "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
          $('input[name="scheduled_start_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });
          $('input[name="scheduled_start_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });
        });
    </script>
    
    <script>
        $(function()
        {
          $('input[name="actual_start_date"]').daterangepicker({
              "autoUpdateInput": false,
              "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
          $('input[name="actual_start_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });
          $('input[name="actual_start_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });
        });
    </script>
    
    <script>
        $(function()
        {
          $('input[name="actual_end_date"]').daterangepicker({
              "autoUpdateInput": false,
              "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
          $('input[name="actual_end_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });
          $('input[name="actual_end_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });
        });
    </script>
    
    
    
    
    
    <script type="text/javascript">
        $(function()
        {
          $('input[name="scheduled_end_date"]').daterangepicker({
              "autoUpdateInput": false,
              "singleDatePicker": true,
                        "showDropdowns": true,
                        "timePicker": true,
                        "timePicker24Hour": true,
                        "locale": {
                        "format": "HH:mm MM/DD/YYYY",

                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",

                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
              }
          });
          $('input[name="scheduled_end_date"]').on('apply.daterangepicker', function(ev, picker) {
              $(this).val(picker.startDate.format('HH:mm MM/DD/YYYY'));
          });

          $('input[name="scheduled_end_date"]').on('cancel.daterangepicker', function(ev, picker) {
              $(this).val('');
          });
        });
    </script>
    
    
    
    
    
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>

<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="db.get_incidents"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="java.text.SimpleDateFormat"%>

<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        if (!session.getAttribute("incident").toString().equalsIgnoreCase("none") || session.getAttribute("administration").toString().equalsIgnoreCase("yes")) 
        {
            String home_page = session.getAttribute("home_page").toString();
            session.setAttribute("incident_detail_referer", request.getHeader("referer"));
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session); 
            String user_id = session.getAttribute("user_id").toString();
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            String id = request.getParameter("id");            
            String incident_info[] = get_incidents.incident_by_id(con, id);
            ArrayList<String[]> incident_state_info = get_incidents.incident_state_log(con, id);
            %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>	
    <input type="hidden" name="incident_view_referer" id="incident_view_referer" value="<%=session.getAttribute("incident_view_referer")%>"/>
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title"><i class="la la-wrench"></i>&nbsp;Incident View</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="#" onClick="parent.location='<%=session.getAttribute("incident_detail_referer")%>'"><i class="la la-angle-left"></i>Back</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div> <!--end header-->
            <div class="row">
		<div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="heading-toggle">INCIDENT <%=id%></h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xl-2 col-lg-6 col-12">
                                        <label><br>Incident ID</label>
                                        <input type="text" value="<%=incident_info[0]%>" class="form-control" disabled />
                                    </div>
                                    <div class="col-xl-2 col-lg-6 col-12">
                                        <label><br>Incident Time</label>
                                        <input type="text" value="<%=display_format.format(timestamp_format.parse(incident_info[2]))%>" class="form-control" disabled />
                                    </div>
                                    <div class="col-xl-2 col-lg-6 col-12">
                                        <label><br>State</label>
                                        <input type="text" value="<%=incident_info[19]%>" class="form-control" disabled >
                                    </div>
                                    <div class="col-xl-2 col-lg-6 col-12">
                                        <label><br>State Time</label>
                                        <input type="text" value="<%=display_format.format(timestamp_format.parse(incident_info[20]))%>" class="form-control" disabled />
                                    </div>
                                    <div class="col-xl-2 col-lg-6 col-12">
                                        <label><br>Caller</label>
                                        <input type="text" value="<%=incident_info[32] + "(" + incident_info[33] + " " + incident_info[35] + ")"%>" class="form-control" disabled />
                                    </div>
                                    <div class="col-xl-2 col-lg-6 col-12">
                                        <label><br>Incident Create By</label>
                                        <input type="text" value="<%=incident_info[45] + "(" + incident_info[45] + " " + incident_info[47] + ")"%>" class="form-control" disabled />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xl-2 col-lg-6 col-12">
                                        <label><br>Caller VIP?</label>
                                        <br>
                                        <%
                                        if(incident_info[36].equalsIgnoreCase("true"))
                                        {
                                            %>
                                            <input type="checkbox" checked disabled=""/> 
                                            <%
                                        }
                                        else
                                        {
                                            %>
                                            <input type="checkbox"  disabled=""/> 
                                            <%
                                        }
                                        %>                                       
                                    </div>
                                    <div class="col-xl-2 col-lg-6 col-12">
                                        <label><br>Contact Method</label>
                                        <input type="text" value="<%=incident_info[18]%>" class="form-control" disabled >
                                    </div>
                                    <div class="col-xl-2 col-lg-6 col-12">
                                        <label><br>Location</label>
                                        <input type="text" value="<%=incident_info[5]%>" class="form-control" disabled >
                                    </div>
                                    <div class="col-xl-2 col-lg-6 col-12">
                                        <label><br>Priority</label>
                                        <input type="text" value="<%=incident_info[14]%>" class="form-control" disabled >
                                    </div>
                                    <div class="col-xl-2 col-lg-6 col-12">
                                        <label><br>Category</label>
                                        <input type="text" value="<%=incident_info[9]%>" class="form-control" disabled >
                                    </div>
                                    <div class="col-xl-2 col-lg-6 col-12">
                                        <label><br>Subcategory</label>
                                        <input type="text" value="<%=incident_info[10]%>" class="form-control" disabled >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xl-2 col-lg-6 col-12">
                                        <label><br>Assigned to Group</label>
                                        <input type="text" value="<%=incident_info[38]%>" class="form-control" disabled >
                                    </div>
                                    <div class="col-xl-2 col-lg-6 col-12">
                                        <label><br>Assigned to</label>
                                        <input type="text" value="<%=incident_info[39] + "(" + incident_info[40] + " " + incident_info[42] + ")"%>" class="form-control" disabled />
                                    </div>
                                    <div class="col-xl-2 col-lg-6 col-12">
                                        <label><br>First Contact Resolution?</label>
                                        <br>
                                        <%
                                        if(incident_info[26].equalsIgnoreCase("true"))
                                        {
                                            %>
                                            <input type="checkbox" checked disabled=""/> 
                                            <%
                                        }
                                        else
                                        {
                                            %>
                                            <input type="checkbox"  disabled=""/> 
                                            <%
                                        }
                                        %>                                       
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <label><br>Description</label>
                                        <input type="text" value="<%=incident_info[15]%>" class="form-control" disabled /> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <br>
                                        <p class="text-muted">Notes <em>(Customer viewable)</em></p>
                                        <textarea class="form-control" id="descTextarea" rows="3" disabled><%=incident_info[23]%></textarea>  
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <br>
                                        <p class="text-muted">Notes <em>(Service Desk)</em></p>
                                        <textarea class="form-control" id="descTextarea" rows="3" disabled><%=incident_info[24]%></textarea>  
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <hr>
                                        <h5><b>State Change Log</b></h5>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>State</th>
                                                        <th>Start</th>
                                                        <th>Stop</th>
                                                        <th>Total Time</th>
                                                        <th>Assigned to Group</th>
                                                        <th>Assigned Analyst</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <%
                                                    String row[][] = new String[incident_state_info.size()][6];
                                                    java.util.Date start = new java.util.Date();
                                                    java.util.Date end = new java.util.Date();
                                                    
                                                    
                                                    for(int a = 0; a < incident_state_info.size(); a++)
                                                    {
                                                        String this_row[] = incident_state_info.get(a);
                                                        row[a][0] = this_row[21]; //state
                                                        row[a][1] = display_format.format(timestamp_format.parse(this_row[22])); //start
                                                        start.setTime(timestamp_format.parse(this_row[22]).getTime());
                                                        if((a + 1) < incident_state_info.size()) //next row state date
                                                        {
                                                            row[a][2] = display_format.format(timestamp_format.parse(this_row[22])); //stop
                                                            end.setTime(timestamp_format.parse(this_row[22]).getTime());
                                                        }
                                                        else
                                                        {
                                                            row[a][2] = ""; //no next row
                                                        }
                                                        long different = end.getTime() - start.getTime();
		                                        long secondsInMilli = 1000;
                                                        long minutesInMilli = secondsInMilli * 60;
                                                        long hoursInMilli = minutesInMilli * 60;
                                                        long daysInMilli = hoursInMilli * 24;
                                                        long elapsedDays = different / daysInMilli;
                                                        different = different % daysInMilli;
                                                        long elapsedHours = different / hoursInMilli;
                                                        different = different % hoursInMilli;
                                                        long elapsedMinutes = different / minutesInMilli;
                                                        different = different % minutesInMilli;

                                                        long elapsedSeconds = different / secondsInMilli;

                                                        row[a][3] = elapsedDays + ":Days , " + elapsedHours + ":Hours , " + elapsedMinutes + ":Minutes , " + elapsedSeconds + ":Seconds"; //total
                                                        row[a][4] = this_row[40]; //group
                                                        row[a][5] = this_row[42] + " " + this_row[44]; //analyst
                                                        
                                                    }
                                                    %>
                                                    <%
                                                    for(int b = 0; b < row.length; b++)
                                                    {
                                                        %>
                                                        <tr>
                                                        <td><%=row[b][0]%></th>
                                                        <td><%=row[b][1]%></td>
                                                        <td><%=row[b][2]%></td>
                                                        <td><%=row[b][3]%></td>
                                                        <td><%=row[b][4]%></td>
                                                        <td><%=row[b][5]%></td>
                                                        </tr>
                                                        <%
                                                    }
                                                    %>
                                                    
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
		</div>
            </div>
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <script>
        function reload_page()
        {
            var URL = document.getElementById("incident_view_referer").value;
            window.location.href = URL;            
        }
    </script>
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%      con.close();
        }//end if not permission
    }//end if not logged in
%>
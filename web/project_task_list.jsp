<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : project_list.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        boolean page_authorized = support.role.authorized(session, "project","read");  
        String user_id = "0";
        try
        {
            user_id = session.getAttribute("user_id").toString(); 
        }
        catch(Exception e)
        {
            user_id = "0";
        }
        if(!page_authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            String home_page = session.getAttribute("home_page").toString();
            String date_range = "";
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_contract_connection(context_dir, session);
            boolean update_session_table = db.get_user_sessions.update(con, user_id);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/12/2019 12:00 AM yyyy-MM-dd%20HH:mm:ss
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19    
            ArrayList<String[]> project_tasks = new ArrayList();      
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	
    <!-- BEGIN VENDOR CSS-->
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">-->
<!--    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">-->
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/app.css">-->
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/horizontal-menu.css">-->
    <!--<link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">-->
<!--    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <!--<link rel="stylesheet" type="text/css" href="assets/css/style.css">-->
    <!-- END Custom CSS-->
    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>

    <div class="clr whtbg p-10 mb-15">

        <h1 class="large-font boldFont">Project tasks</h1>
    </div>    

            <!-- start content here-->
            <%
            String filter = "";
            try
            {
                filter = request.getParameter("filter"); //assigned_to_me,unassigned, created_today, closed_today, open, open_7_14, open_14_30, open_30_plus 
                if(filter == null || filter.equalsIgnoreCase("null"))
                {
                    filter = "all";
                }
            }
            catch(Exception e)
            {
                filter = "all";
            }
            //get the projects
            if(filter.equalsIgnoreCase("all"))
            {
                project_tasks = db.get_projects.all_open(con);
            }
            else if(filter.equalsIgnoreCase("my_tasks"))
            {
                project_tasks = db.get_projects.open_project_tasks_for_user_id(con, user_id);
            }
            else if(filter.equalsIgnoreCase("delinquent"))
            {
                project_tasks = db.get_projects.delinquent_project_tasks_for_user_id(con, user_id);
            }
            else if(filter.equalsIgnoreCase("open"))
            {
                project_tasks = db.get_projects.open_project_tasks(con);
            }
            else if(filter.equalsIgnoreCase("all_delinquent"))
            {
                project_tasks = db.get_projects.delinquent_project_tasks(con);
            }
            else if(filter.equalsIgnoreCase("all_open_tasks"))
            {
                project_tasks = db.get_projects.open_project_tasks(con);
            }
            %>
            
        <div class="sort-tbl-holder">
            <form action="project_list.jsp">
                        <table id="request_table" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Project Name</th>
                                                <th>Task Name</th>
                                                <th>Priority</th>
                                                <th>Assigned To</th>
                                                <th>Status</th>
                                                <th>Due Date</th>
                                                <th>Assigned Group</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                            for(int a = 0; a < project_tasks.size(); a++)
                                            {
                                                String task_info[] = project_tasks.get(a);

                                                String scheduled_start = "";
                                                try
                                                {
                                                    scheduled_start = filter_format.format(timestamp_format.parse(task_info[8]));
                                                }
                                                catch (Exception e)
                                                {
                                                    scheduled_start = "";
                                                }

                                                String scheduled_end = "";
                                                try
                                                {
                                                    scheduled_end = filter_format.format(timestamp_format.parse(task_info[10]));
                                                }
                                                catch (Exception e)
                                                {
                                                    scheduled_end = "";
                                                }
                                                String priority_class = "";
                                                switch (task_info[6]) {
                                                    case "Low":
                                                        priority_class = "low";
                                                        break;
                                                    case "Medium":
                                                        priority_class = "medium";
                                                        break;
                                                    case "High":
                                                        priority_class = "high";
                                                        break;
                                                    case "Critical":
                                                        priority_class = "critical";
                                                        break;
                                                    default:
                                                        priority_class = "unknown";
                                                        break;
                                                }
                                                String priority = "<span class=\"indicator " + priority_class + "\"></span>&nbsp;" + (task_info[6].equals("") ? "Unknown" : task_info[6]);

                                                %>
                                                <tr onclick="location.href='project_list.jsp?project_id=<%=task_info[1]%>&task_id=<%=task_info[0]%>';" style="cursor:pointer;">
                                                <td><%=task_info[21]%></td>
                                                <td><%=task_info[2]%></td>
                                                <td><%=priority%></td>
                                                <td><span class="user-img dInlineBlock mr-5 user-avatar"><img src="assets/images/users/avatar-2.jpg" alt=""></span><%=task_info[17] + " " + task_info[19]%></td>
                                                <td><span class="btn sm state-<%=(task_info[7].equals("") ? "unknown" : task_info[7].toLowerCase()) %> customBtn"><%=(task_info[7].equals("") ? "Unknown" : task_info[7])%></span></td>
                                                <!--<td><%=scheduled_start%></td>-->
                                                <td><%=scheduled_end%></td>
                                                <td><%=task_info[15]%></td>

                                                </tr>
                                                <%
                                            }                                        
                                            %>                                        
                                        </tbody>
                                    </table>
            </form>
        </div>
            <!-- end content here-->
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <script>
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>
<!--    <script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
    <script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
    <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
    <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>-->


    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script>
        $('#request_table').dataTable( {
            "order": []
        } );
    </script>
    <!-- END PAGE LEVEL JS-->
    </body>
</html>
<%      con.close();
        }//end if not permission
    }//end if not logged in
%>
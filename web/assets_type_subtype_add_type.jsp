<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        boolean authorized = support.role.authorized(session, "asset","update"); 
        if(!authorized)
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);  
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
    %>
    <!-- BEGIN PAGE LEVEL CSS-->
    <!-- END PAGE LEVEL CSS-->
    
    <form class="form" action="assets_type_subtype_add_type" method="post">
        <div class="sort-tbl-holder">
            <table class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="10%" class="text-center">Active</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center">
                            <input type="checkbox" name="type_active" id="type_active"/></div>
                        </td>
                        <td>                                                        
                            <input class="form-control"  type="text" name="assets_type_name" id="assets_type_name" value=""/>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="row mb-15">
                <div class="col-md-12">
                    <span onclick="add_row()" class="btn btn-primary-new customBtn lg waves-effect">
                        <span class="icon mr-5"><img src="assets/images/svg/plus-icon.svg" alt=""></span>Add Subtype
                    </span>
                </div>
            </div>

            <div class="row" id="option_table_div">        
                <div class="col-md-12" id="additional_parameter_list" >
                    <table class="table" id="option_table" border="0" cellspacing="2" cellpadding="10">
                        <thead>
                            <tr>
                                <th class="text-center" width="10%"><b>Active</b></th>
                                <th><b>Subtype</b></th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <div class="row ">
                <div class="col-md-12 text-center pt-30 pb-30">
                    <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                    <button type="submit" class="btn btn-primary-new customBtn lg waves-effect">
                        Save
                    </button>
                </div>
            </div>                                

        </div>
    </form>
    
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->  
    <%
            con.close();
            }
            catch(Exception e)
            {
                System.out.println("Exception in admin_category_subcategory_add_category.jsp: " + e); 
                logger.debug("ERROR: Exception in admin_category_subcategory_add_category.jsp:" + e);
            }
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>

<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_users_add.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String FINANCIAL = session.getAttribute("financial").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String CONTACT = session.getAttribute("contact").toString();
        String CX = session.getAttribute("cx").toString();
        String SURVEY = session.getAttribute("survey").toString();
        String SLA = session.getAttribute("sla").toString();        
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        String METRIC = session.getAttribute("metric").toString();
        String ASSET = session.getAttribute("asset").toString();
        String USER = session.getAttribute("user").toString();
        
        boolean locked = false;
        
        if(!ADMINISTRATION.equalsIgnoreCase("true") && !MANAGER.equalsIgnoreCase("true"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session);
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String role_id = request.getParameter("role_id");

                if(role_id.equalsIgnoreCase("0") || role_id.equalsIgnoreCase("1"))
                {
                    locked = true;
                }
                String role_info[] = db.get_roles.by_role_id(con, role_id);
                /*returnString[0] = rs.getString("id");
                returnString[1] = rs.getString("name");
                returnString[2] = rs.getString("description");
                returnString[3] = rs.getString("created");
                returnString[4] = rs.getString("created_by_id");
                returnString[5] = rs.getString("administration");  //true or false
                returnString[6] = rs.getString("manager");   //true or false
                returnString[7] = rs.getString("financial");//create,read,update,delete  
                returnString[8] = rs.getString("incident"); //create,read,update,delete
                returnString[9] = rs.getString("request"); //create,read,update,delete
                returnString[10] = rs.getString("contact"); //create,read,update,delete
                returnString[11] = rs.getString("cx"); //create,read,update,delete
                returnString[12] = rs.getString("survey");//create,read,update,delete  
                returnString[13] = rs.getString("sla");//create,read,update,delete  
                returnString[14] = rs.getString("project"); //create,read,update,delete                        
                returnString[15] = rs.getString("jobs"); //create,read,update,delete
                returnString[16] = rs.getString("metric"); //create,read,update,delete
                returnString[17] = rs.getString("asset"); //create,read,update,delete
                returnString[18] = rs.getString("user"); //create,read,update,delete
                returnString[19] = rs.getString("self_service"); //create,read,update,delete*/
                String selected = "";
                String option_values[] = {"none","read","create","update","delete"};
                String option_labels[] = {"None","Read","Create","Update", "Delete"};
                

%>
<jsp:include page='header.jsp'>
    <jsp:param name="page_type" value=""/>
</jsp:include>	

<jsp:include page='menu_service_desk.jsp'>
    <jsp:param name="active_menu" value=""/>
</jsp:include>
<!-- BEGIN PAGE LEVEL CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/icheck/icheck.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/icheck/custom.css">
<link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/checkboxes-radios.css">
<!-- END PAGE LEVEL CSS-->

<div class="app-content content">
    <div class="content-wrapper">
        <!-- start content here-->
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">Roles</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<%=home_page%>.jsp">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Administration</a></li>
                            <li class="breadcrumb-item"><a href="admin_roles.jsp">User Roles</a></li>
                            <li class="breadcrumb-item"><a href="#">Edit</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div> <!--End breadcrumbs -->
        
        <%
        if(locked)
        {
            %>
            <div class="row">
                <div class="col-12">
                    <h4>Can not edit this Role</h4>
                </div>
            </div>
            <%
        }
        else
        {        
        %>
        
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit Role&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em><small>*See footnotes</small></em></h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form class="form" action="admin_roles_edit" method="post">
                                <input type="hidden" name="id" id="id" value="<%=role_id%>"/>
                                <div class="form-body">
                                    <h4 class="form-section"><i class="la la-cogs"></i> Enter User Role Details</h4>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="name">User Role Name</label>
                                                <input type="text" id="name" name="name" class="form-control" value="<%=role_info[1]%>" >
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <label for="description">Description</label>
                                                <input type="text" id="description" name="description" class="form-control" value="<%=role_info[2]%>" >
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <h4 class="form-section"><i class="la la-cogs"></i> Select Permissions for this User Role</h4>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="administration" >Administration</label>
                                                <select id="administration" name="administration" class="form-control">
                                                    <%
                                                    if(role_info[5].equalsIgnoreCase("true"))
                                                    {
                                                        %>
                                                        <option selected value="true">True</option>
                                                        <option value="false">False</option>
                                                        <%
                                                    }
                                                    else
                                                    {
                                                        %>
                                                        <option value="true">True</option>
                                                        <option selected value="false">False</option>
                                                        <%
                                                    }
                                                    %>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="manager" >Manager</label>
                                                <select id="manager" name="manager" class="form-control">
                                                    <%
                                                    if(role_info[6].equalsIgnoreCase("true"))
                                                    {
                                                        %>
                                                        <option selected value="true">True</option>
                                                        <option value="false">False</option>
                                                        <%
                                                    }
                                                    else
                                                    {
                                                        %>
                                                        <option value="true">True</option>
                                                        <option selected value="false">False</option>
                                                        <%
                                                    }
                                                    %>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="financial">Financial</label>
                                                <select id="financial" name="financial" class="form-control">
                                                    <%
                                                    selected = "";
                                                    for(int a = 0; a < option_values.length;a++)
                                                    {
                                                        if(role_info[7].equalsIgnoreCase(option_values[a]))
                                                        {
                                                            selected = "SELECTED";
                                                        }
                                                        else
                                                        {
                                                            selected = "";
                                                        }
                                                        %>
                                                        <option <%=selected%> value="<%=option_values[a]%>"><%=option_labels[a]%></option>
                                                        <%
                                                    }
                                                    %>                                                   
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="incident">Incident</label>
                                                <select id="incident" name="incident" class="form-control">
                                                    <%
                                                    selected = "";
                                                    for(int a = 0; a < option_values.length;a++)
                                                    {
                                                        if(role_info[8].equalsIgnoreCase(option_values[a]))
                                                        {
                                                            selected = "SELECTED";
                                                        }
                                                        else
                                                        {
                                                            selected = "";
                                                        }
                                                        %>
                                                        <option <%=selected%> value="<%=option_values[a]%>"><%=option_labels[a]%></option>
                                                        <%
                                                    }
                                                    %>        
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="param_request">Request</label>
                                                <select id="param_request" name="param_request" class="form-control">
                                                    <%
                                                    selected = "";
                                                    for(int a = 0; a < option_values.length;a++)
                                                    {
                                                        if(role_info[9].equalsIgnoreCase(option_values[a]))
                                                        {
                                                            selected = "SELECTED";
                                                        }
                                                        else
                                                        {
                                                            selected = "";
                                                        }
                                                        %>
                                                        <option <%=selected%> value="<%=option_values[a]%>"><%=option_labels[a]%></option>
                                                        <%
                                                    }
                                                    %>       
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="contact">Contact</label>
                                                <select id="contact" name="contact" class="form-control">
                                                    <%
                                                    selected = "";
                                                    for(int a = 0; a < option_values.length;a++)
                                                    {
                                                        if(role_info[10].equalsIgnoreCase(option_values[a]))
                                                        {
                                                            selected = "SELECTED";
                                                        }
                                                        else
                                                        {
                                                            selected = "";
                                                        }
                                                        %>
                                                        <option <%=selected%> value="<%=option_values[a]%>"><%=option_labels[a]%></option>
                                                        <%
                                                    }
                                                    %>      
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">                                        
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="cx">CX</label>
                                                <select id="cx" name="cx" class="form-control">
                                                    <%
                                                    selected = "";
                                                    for(int a = 0; a < option_values.length;a++)
                                                    {
                                                        if(role_info[11].equalsIgnoreCase(option_values[a]))
                                                        {
                                                            selected = "SELECTED";
                                                        }
                                                        else
                                                        {
                                                            selected = "";
                                                        }
                                                        %>
                                                        <option <%=selected%> value="<%=option_values[a]%>"><%=option_labels[a]%></option>
                                                        <%
                                                    }
                                                    %>     
                                                </select>
                                            </div>
                                        </div> 
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="survey">Survey</label>
                                                <select id="survey" name="survey" class="form-control">
                                                    <%
                                                    selected = "";
                                                    for(int a = 0; a < option_values.length;a++)
                                                    {
                                                        if(role_info[12].equalsIgnoreCase(option_values[a]))
                                                        {
                                                            selected = "SELECTED";
                                                        }
                                                        else
                                                        {
                                                            selected = "";
                                                        }
                                                        %>
                                                        <option <%=selected%> value="<%=option_values[a]%>"><%=option_labels[a]%></option>
                                                        <%
                                                    }
                                                    %>     
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="sla">SLA</label>
                                                <select id="sla" name="sla" class="form-control">
                                                    <%
                                                    selected = "";
                                                    for(int a = 0; a < option_values.length;a++)
                                                    {
                                                        if(role_info[13].equalsIgnoreCase(option_values[a]))
                                                        {
                                                            selected = "SELECTED";
                                                        }
                                                        else
                                                        {
                                                            selected = "";
                                                        }
                                                        %>
                                                        <option <%=selected%> value="<%=option_values[a]%>"><%=option_labels[a]%></option>
                                                        <%
                                                    }
                                                    %>       
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="project">Project</label>
                                                <select id="project" name="project" class="form-control">
                                                    <%
                                                    selected = "";
                                                    for(int a = 0; a < option_values.length;a++)
                                                    {
                                                        if(role_info[14].equalsIgnoreCase(option_values[a]))
                                                        {
                                                            selected = "SELECTED";
                                                        }
                                                        else
                                                        {
                                                            selected = "";
                                                        }
                                                        %>
                                                        <option <%=selected%> value="<%=option_values[a]%>"><%=option_labels[a]%></option>
                                                        <%
                                                    }
                                                    %>        
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="job">Job</label>
                                                <select id="job" name="job" class="form-control">
                                                    <%
                                                    selected = "";
                                                    for(int a = 0; a < option_values.length;a++)
                                                    {
                                                        if(role_info[15].equalsIgnoreCase(option_values[a]))
                                                        {
                                                            selected = "SELECTED";
                                                        }
                                                        else
                                                        {
                                                            selected = "";
                                                        }
                                                        %>
                                                        <option <%=selected%> value="<%=option_values[a]%>"><%=option_labels[a]%></option>
                                                        <%
                                                    }
                                                    %>      
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="job">Metrics</label>
                                                <select id="metric" name="metric" class="form-control">
                                                    <%
                                                    selected = "";
                                                    for(int a = 0; a < option_values.length;a++)
                                                    {
                                                        if(role_info[16].equalsIgnoreCase(option_values[a]))
                                                        {
                                                            selected = "SELECTED";
                                                        }
                                                        else
                                                        {
                                                            selected = "";
                                                        }
                                                        %>
                                                        <option <%=selected%> value="<%=option_values[a]%>"><%=option_labels[a]%></option>
                                                        <%
                                                    }
                                                    %>      
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="job">Asset</label>
                                                <select id="asset" name="asset" class="form-control">
                                                    <%
                                                    selected = "";
                                                    for(int a = 0; a < option_values.length;a++)
                                                    {
                                                        if(role_info[17].equalsIgnoreCase(option_values[a]))
                                                        {
                                                            selected = "SELECTED";
                                                        }
                                                        else
                                                        {
                                                            selected = "";
                                                        }
                                                        %>
                                                        <option <%=selected%> value="<%=option_values[a]%>"><%=option_labels[a]%></option>
                                                        <%
                                                    }
                                                    %>      
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="user">Users</label>
                                                <select id="user" name="user" class="form-control">
                                                    <%
                                                    selected = "";
                                                    for(int a = 0; a < option_values.length;a++)
                                                    {
                                                        if(role_info[18].equalsIgnoreCase(option_values[a]))
                                                        {
                                                            selected = "SELECTED";
                                                        }
                                                        else
                                                        {
                                                            selected = "";
                                                        }
                                                        %>
                                                        <option <%=selected%> value="<%=option_values[a]%>"><%=option_labels[a]%></option>
                                                        <%
                                                    }
                                                    %>      
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="user">Self Service</label>
                                                <select id="self_service" name="self_service" class="form-control">
                                                    <%
                                                    selected = "";
                                                    for(int a = 0; a < option_values.length;a++)
                                                    {
                                                        if(role_info[19].equalsIgnoreCase(option_values[a]))
                                                        {
                                                            selected = "SELECTED";
                                                        }
                                                        else
                                                        {
                                                            selected = "";
                                                        }
                                                        %>
                                                        <option <%=selected%> value="<%=option_values[a]%>"><%=option_labels[a]%></option>
                                                        <%
                                                    }
                                                    %>      
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="button" onclick="javascript:location.href='admin_roles.jsp'" class="btn btn-warning mr-1">
                                        <i class="ft-x"></i> Cancel
                                    </button>
                                    <button type="submit" class="btn btn-info">
                                        &nbsp;&nbsp;<i class="ft-save"></i> Save&nbsp;&nbsp;
                                    </button>
                                </div>
                            </form>
                            <hr>
                            Role permissions: 
                            <br>
                            Administration permission grants full access to everything. Overrides all other permissions.
                            <br>
                            Manager permission has full access to everything Except Administration functions.   
                            <br>
                            <br>
                            None = No Access to the data
                            <br>
                            Read = Able to view the data but can not control any data functions
                            <br>
                            Create = Able to create new records, inherits Read permission
                            <br>
                            Update = Able to update records, inherits Read and Create permission
                            <br>
                            Delete = Able to delete records, inherits Read, Create, and Update permission
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%
        }//end of can edit
        %>
        <!-- end content here-->
    </div>        
</div>
<jsp:include page='footer.jsp'>
    <jsp:param name="parameter" value=""/>
</jsp:include>	
<!-- BEGIN PAGE LEVEL JS-->
<script src="app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
<script src="app-assets/js/scripts/forms/checkbox-radio.js"></script>
<!-- END PAGE LEVEL JS-->   
<%                con.close();
    
            } catch (Exception e) {
                System.out.println("Exception in admin_role_edit.jsp: " + e);
                logger.debug("ERROR: Exception in admin_role_edit.jsp:" + e);
            }
        } //end if not admin permission
    } //end if not redirected
%>
</body>
</html>

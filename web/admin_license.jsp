<!--Copyright 2021 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")== null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    }
    else
    {
        if(!session.getAttribute("administration").toString().equalsIgnoreCase("true"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            String installation_type = props.get("installation_type").toString().trim();
            String license = "";
            String number_of_users = "";
            String expire_date = "";
            try
            {                
                if(installation_type.equalsIgnoreCase("shared"))
                {
                    license = session.getAttribute("license").toString();
                }
                else
                {
                    //db connection from the serveoptics.properties file
                    Connection con = db.db_util.get_contract_connection(context_dir, session);    
                    String user_id = session.getAttribute("user_id").toString();
                    boolean update_session_table = db.get_user_sessions.update(con, user_id);
                    con.close();
                    license = db.get_license.all(con);
                }
                String values[] = db.get_license.decoded(context_dir, license);
                number_of_users = values[1];
                java.util.Date temp = new java.util.Date();
                temp = timestamp_format.parse(values[2]);
                expire_date = filter_format.format(temp);
                
            }
            catch(Exception e)
            {
                System.out.println("Exception getting license=" + e);
            }
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_service_desk.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <!-- END PAGE LEVEL CSS-->
    
                            <div class="clr whtbg p-10 mb-15">
                                <h1 class="large-font boldFont">License</h1>
                            </div>
                            <div class="sort-tbl-holder">

                                <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>                                                                <%
                                String readonly = "";
                                if(installation_type.equalsIgnoreCase("shared"))
                                {
                                    readonly = " READONLY ";
                                    %>
                                    <h4 class="card-title">License <em><small>License managed by RDT support</small></em></h4>
                                    <%
                                }
                                else
                                {
                                    readonly = " READONLY ";
                                    %>
                                    <h4 class="card-title">License &nbsp;&nbsp;&nbsp;&nbsp;<a href="admin_license_edit.jsp" class="btn btn-outline-blue">Edit License</a></h4>
                                    <% 
                                }
                                %>
                                            </th>
                                        </tr>
                                    </thead>
                            
                                    <tbody>
                                        <tr>
                                            <td>
                                                <p class="card-text">Current License</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                
                                <table id="dataTable" class="table table-striped custom-sort-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>License</th>
                                            <th>Number of Users</th>
                                            <th>Expiration Date</th>
                                        </tr>
                                    </thead>
                            
                                    <tbody>
                                        <tr>
                                            <td>
                                               <input <%=readonly%> type="text" id="license" name="license" class="no-border" value="<%=license%>">
                                            </td>

                                            <td>
                                                <div class="formField clr md dInlineBlock">
                                                    <input <%=readonly%> type="text" id="number_of_users" name="number_of_users" class="no-border" value="<%=number_of_users%>">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="formField clr md dInlineBlock">
                                                    <input <%=readonly%> type="text" id="expire_date" name="expire_date" class="no-border" value="<%=expire_date%>" >
                                                </div>
                                            </td>
                                            
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
    
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->   
    <%
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : admin_request_sla_add.jsp
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="javax.json.Json"%>
<%@page import="javax.json.JsonArray"%>
<%@page import="javax.json.JsonObject"%>
<%@page import="javax.json.JsonReader"%>
<%@page import="javax.json.JsonStructure"%>
<%@page import="java.io.StringReader"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
    <%
    if (session.getAttribute("authenticated") == null) 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url);
    } 
    else 
    {
        boolean authorized = support.role.authorized(session, "sla","update");
        if(!authorized)
        {
            response.sendRedirect("no_permission.jsp");
        } 
        else 
        {
            Logger logger = LogManager.getLogger();
            String home_page = session.getAttribute("home_page").toString();
            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            try 
            {
                Connection con = db.db_util.get_contract_connection(context_dir, session); 
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
                String id = request.getParameter("id");
                String sla_info[] = db.get_sla.request_by_id(con, id);
%>
    <!-- BEGIN PAGE LEVEL CSS-->
    <!-- END PAGE LEVEL CSS-->
        <form class="form" action="admin_request_sla_edit" method="post">
            <input type="hidden" name="id" id="id" value="<%=id%>"/>
            <!-- start content here-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only mb-15">
                                                        Catalog Item
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0 ">
                                                    <select name="service_catalog_item_id" id="service_catalog_item_id" class="form-control">
                                                    <%
                                                    ArrayList <String[]> catalog_items = db.get_service_catalog.service_catalog_items_by_state(con, "Published");
                                                    Map<String, String> item_names = new HashMap<String, String>();

                                                    Map<String, List<String>> catalog = new HashMap<String, List<String>>();
                                                    for(int a = 0; a < catalog_items.size(); a++)
                                                    {
                                                        String catalog_item[] = catalog_items.get(a);
                                                        item_names.put(catalog_item[0], catalog_item[2]);
                                                        
                                                        if (catalog.containsKey(catalog_item[37])) {
                                                            catalog.get(catalog_item[37]).add(catalog_item[0]);
                                                        } else {
                                                            List<String> valueList = new ArrayList<>();
                                                            valueList.add(catalog_item[0]);
                                                            // and put both into the Map
                                                            catalog.put(catalog_item[37], valueList);
                                                        }
                                                    }
                                                    for (String key : catalog.keySet()) {
                                                        %>
                                                            <optgroup label="<%=key%>">
                                                        <%
                                                        List<String> value = catalog.get(key);
                                                        for (String val : value) {
                                                            %>
                                                                <option <%=(sla_info[28].equals(val) ? "selected" : "")%> value="<%=val%>"><%=item_names.get(val)%></option>
                                                            <%
                                                        }
                                                    }
                                                    %>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only mb-15">
                                                        Request Priority
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">                                                
                                                    <select id="request_priority" name="request_priority" class="form-control">
                                                        <%
                                                        String priorities[] = {"Low","Medium","High","Critical"};
                                                        for(int a = 0; a < priorities.length;a++)
                                                        {
                                                            if(sla_info[5].equalsIgnoreCase(priorities[a]))
                                                            {
                                                                %>
                                                                <option SELECTED value="<%=priorities[a]%>"><%=priorities[a]%></option>
                                                                <%
                                                            }
                                                            else
                                                            {
                                                                %>
                                                                <option value="<%=priorities[a]%>"><%=priorities[a]%></option>
                                                                <%
                                                            }
                                                        }
                                                        %>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only px-0">
                                                        SLA Display Name
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0 ">
                                                    <input type="text" id="name" name="name" class="form-control" value="<%=sla_info[1]%>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only px-0">
                                                        Short Description (Optional)
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0 ">
                                                    <input type="text" id="description" name="description" class="form-control" value="<%=sla_info[2]%>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only mb-15">
                                                        SLA Target
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">
                                                    <input type="text" id="target" name="target" class="form-control" value="<%=sla_info[6]%>" />                                    
                                                </div>
                                            </div> 
                                            <div class="col-md-6 pt-10">
                                                <div class="custom-control custom-checkbox dInlineBlock mr-15">
                                                    <%
                                                    if (sla_info[12].equalsIgnoreCase("0")) {
                                                    %>
                                                    <input type="checkbox" class="custom-control-input" id="active" name="active">  
                                                    <%
                                                    } else {
                                                    %>
                                                    <input type="checkbox" class="custom-control-input" CHECKED id="active" name="active">  
                                                    <%
                                                        }
                                                    %>                                          
                                                    <label class="custom-control-label small-font" for="active">Active?</label>
                                                </div>
                                                <div class="custom-control custom-checkbox dInlineBlock mr-15">
                                                    <%
                                                        //12
                                                        if (sla_info[13].equalsIgnoreCase("0")) {
                                                    %>
                                                    <input type="checkbox" class="custom-control-input" id="reportable" name="reportable">  
                                                    <%
                                                    } else {
                                                    %>
                                                    <input type="checkbox" class="custom-control-input" CHECKED id="reportable" name="reportable">  
                                                    <%
                                                        }
                                                    %>                                            
                                                    <label class="custom-control-label small-font" for="reportable">Do you want this SLA included on the Incident Dashboard?</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only mb-15">
                                                        SLA Text Detail (Optional)
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">
                                                    <textarea class="form-control" name="sla_text" id="sla_text" rows="3"><%=sla_info[4]%></textarea>  
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only mb-15">
                                                        SLA Type
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">                                                
                                                    <select id="sla_type" name="sla_type" class="form-control">
                                                        <option SELECTED value="TIME">Time</option>
                                                        <!--<option value="COUNT_CONCURRENT">Count (Concurrent)</option>
                                                        <option value="COUNT_CUMULATIVE">Count (Cumulative)</option>
                                                        <option value="RATIO">Ratio</option>-->
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="field-title">
                                                    <label class="formLabel md has-txt-only mb-15">
                                                        SLA Timeframe
                                                    </label>
                                                </div>
                                                <div class="formField clr md px-0 border-0">                                                
                                                    <select id="sla_timeframe" name="sla_timeframe" class="form-control">
                                                        <%
                                                            System.out.println("191");
                                                        String timeframes[] = {"DAILY","WEEKLY","MONTHLY","QUARTERLY","ANNUAL","CUSTOM"};
                                                        String timeframes_names[] = {"Daily","Weekly","Monthly","Quarterly","Annual","Custom"};
                                                        for(int a = 0; a < timeframes.length;a++)
                                                        {
                                                            if(sla_info[15].equalsIgnoreCase(timeframes[a]))
                                                            {
                                                                %>
                                                                <option SELECTED value="<%=timeframes[a]%>"><%=timeframes_names[a]%></option>
                                                                <%
                                                            }
                                                            else
                                                            {
                                                                %>
                                                                <option value="<%=timeframes[a]%>"><%=timeframes_names[a]%></option>
                                                                <%
                                                            }
                                                        }
                                                        %>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="row ">
                                        <div class="col-md-12 pt-15">
                                            <button type="button" onclick="show_select()" class="btn mr-1 mb-1 btn-primary">Add Additional Parameters</button>
                                            <p>
                                                <em>
                                                    Do you need to add additional parameters to further define records that will be part of this SLA?
                                                </em>
                                            </p>
                                            <div class="row  col-md-12" id="additional_parameters">
                                            </div>
                                        </div>                                        
                                    </div>

                                    <div class="row" id="parameter_selector" style="display:none">
                                        <div class="col-md-12">
                                            <div class="field-title">
                                                <label class="formLabel md has-txt-only mb-15">
                                                    Select a Field
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <select onchange="add_measure()" id="field_name" name="field_name" class="form-control">
                                                    <option value="--"></option>
                                                    <option value="description">Description</option>
                                                    <option value="contact_method">Contact Method</option>
                                                    <option value="assigned_group_id">Assigned to Group</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row"  id="field_measure_div" style="display:none">
                                        <div class="col-md-12">
                                            <div class="field-title">
                                                <label class="formLabel md has-txt-only mb-15">
                                                    Select a Measure
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <select onchange="add_request_parameter_choices()" id="field_measure" name="field_measure" class="form-control">
                                                    <option value="--"></option>
                                                    <option value="equal">Equal</option>
                                                    <option value="not_equal">Not Equal</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" id="field_choices_div" style="display:none">
                                            <div id="contact_method_choices" style="display:none">
                                                <label for="field_select_choices">Select all the values that apply</label>
                                                <select onchange="show_add_button()" class="select2 form-control" multiple="multiple" id="field_values" name="field_values">

                                                </select>
                                            </div>
                                            <div id="description_choices" style="display:none">
                                                <label for="field_text_choices">Enter text that will match the text in the Request Description</label>
                                                <input onfocus="show_add_button()" type="text" id="field_text_choices" name="field_text_choices" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-3" id="add_button_div" style="display:none">
                                        <div class="col-md-6">
                                            <button type="button" onclick="add_row()" class="btn mr-1 mb-1 btn-primary">Add >></button>
                                        </div>
                                    </div>
                                    <div class="row" id="top_hr" style="display:none">
                                        <div class="col-md-12">
                                            <hr/>
                                        </div>
                                    </div>
                                        <div class="row" id="additional_parameter" <%=(sla_info[27].equalsIgnoreCase("{\"parameters\": []}") ? "style='display:none'" : "")%>> 
                                            <div class="col-md-12" id="additional_parameter_list" >
                                                <h4> Additional Parameters&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" onclick="delete_row()" class="btn mr-1 mb-1 btn-info">Delete Selected Parameter(s)</button></h4>

                                                <table class="table" id="parameter_table" border="0" cellspacing="2" cellpadding="10">
                                                    <tr>
                                                        <td>#</td>
                                                        <td><b>Field</b></td>
                                                        <td><b>Operator</b></td>
                                                        <td><b>Value(s)</b></td>
                                                    </tr>
                                                    <%
                                                    //{"parameters": [{"value": ["Chat", "Email", "Phone"], "operator": "equal", "field_name": "contact_method"}, {"value": ["Security"], "operator": "equal", "field_name": "category"}]}
                                                    try 
                                                    {
                                                        String values = "";
                                                        String transfer_values = "";
                                                        JsonReader reader = Json.createReader(new StringReader(sla_info[27]));
                                                        JsonObject resultObject = reader.readObject();
                                                        JsonArray parameters = null;
                                                        parameters = (JsonArray) resultObject.getJsonArray("parameters");
                                                        for(int a = 0; a < parameters.size(); a++)
                                                        {
                                                            JsonObject this_record = parameters.getJsonObject(a); //a single incident record

                                                            String field_name = this_record.getString("field_name");
                                                            String operator = this_record.getString("operator");
                                                            JsonArray vals = this_record.getJsonArray("value");
                                                            transfer_values = "";
                                                            for(int b = 0; b < vals.size();b++)
                                                            {
                                                                transfer_values += vals.getJsonString(b).getString().replace("\"", "") + "~";
                                                                if(b == 0)
                                                                {
                                                                    values = vals.getJsonString(b).getString().replace("\"", "");
                                                                }
                                                                else
                                                                {
                                                                    values = values + " , " + vals.getJsonString(b).toString().replace("\"", "");
                                                                }                                                            
                                                            }
                                                            %>
                                                            <tr>
                                                                <td><input type="checkbox"/></td>
                                                                <td><%="<input type='hidden' name='field_name" + "~" + a + "' value='" + field_name + "'/>" + field_name%></td>
                                                                <td><%="<input type='hidden' name='field_measure" + "~" + a + "' value='" + operator + "'/>" +  operator%></td>
                                                                <td><%="<input type='hidden' name='field_values" + "~" + a + "' value='" + transfer_values + "'/>" + values%></td>
                                                            </tr>
                                                            <%
                                                            values="";
                                                        }
                                                    }
                                                    catch(Exception e)
                                                    {
                                                        System.out.println("Exception in Request SLA parameters=" + e);
                                                    }
                                                    %>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row" id="bottom_hr" style="display:none">
                                            <div class="col-md-12">
                                                <hr/>
                                            </div>
                                        </div>     

                                        <div class="row">
                                            <div class="col-md-12">
                                                <h4 class="mediumFont large-font mb-20">Compliance Parameters</h4>
                                            </div>
                                        </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="field-title">
                                                <label class="formLabel md has-txt-only mb-15">
                                                    Which State defines the starting point of the Incident record?
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <select id="start_condition" name="start_condition" class="form-control">
                                                    <%
                                                    String start_condition = sla_info[7];
                                                    String start_conditions[] = {"--","create_date","request_date","resolve_date","closed_date"};
                                                    String start_conditions_names[] = {"","Create Date","Request Date","Resolved Date","Closed Date"};
                                                    for(int a = 0; a < start_conditions.length;a++)
                                                    {
                                                        if(start_condition.equalsIgnoreCase(start_conditions[a]))
                                                        {
                                                            %>
                                                            <option SELECTED value="<%=start_conditions[a]%>"><%=start_conditions_names[a]%></option>
                                                            <%
                                                        }
                                                        else
                                                        {
                                                            %>
                                                            <option value="<%=start_conditions[a]%>"><%=start_conditions_names[a]%></option>
                                                            <%
                                                        }
                                                    }
                                                    %>                                           
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="field-title">
                                                <label class="formLabel md has-txt-only mb-15">
                                                    Which State defines the stopping point of the Incident record?
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <select id="stop_condition" name="stop_condition" class="form-control">
                                                    <%
                                                    String stop_condition = sla_info[9];
                                                    String stop_conditions[] = {"--","create_date","request_date","resolve_date","closed_date"};
                                                    String stop_conditions_names[] = {"","Create Date","Request Date","Resolved Date","Closed Date"};
                                                    for(int a = 0; a < stop_conditions.length;a++)
                                                    {
                                                        if(stop_condition.equalsIgnoreCase(start_conditions[a]))
                                                        {
                                                            %>
                                                            <option SELECTED value="<%=stop_conditions[a]%>"><%=stop_conditions_names[a]%></option>
                                                            <%
                                                        }
                                                        else
                                                        {
                                                            %>
                                                            <option value="<%=stop_conditions[a]%>"><%=stop_conditions_names[a]%></option>
                                                            <%
                                                        }
                                                    }
                                                    %>  
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="field-title">
                                                <label class="formLabel md has-txt-only mb-15">
                                                    Stopping state
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <select id="stop_state" name="stop_state" class="form-control">
                                                    <%
                                                    String stop_state = sla_info[10];
                                                    String db_values[] = {"pending_approval","approved","closed_complete","closed_incomplete","closed_cancelled","closed_Rejected"};
                                                    String display_values[] = {"Pending Approval","Approved","Closed Complete","Closed Incomplete","Closed Cancelled","Closed Rejected"};
                                                    String result  = "";
                                                    for(int a = 0; a < db_values.length;a++)
                                                    {
                                                        if(stop_state.equalsIgnoreCase(db_values[a]))
                                                        {
                                                            %>
                                                            <option SELECTED value="<%=db_values[a]%>"><%=display_values[a]%></option>
                                                            <%
                                                        }
                                                        else
                                                        {
                                                            %>
                                                            <option value="<%=db_values[a]%>"><%=display_values[a]%></option>
                                                            <%
                                                        }
                                                    }        
                                                    %>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="field-title">
                                                <label class="formLabel md has-txt-only mb-15">
                                                    Operator
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <select id="sla_success_threshold_operator" name="sla_success_threshold_operator" class="form-control">
                                                    <%
                                                    String sla_success_threshold_operator[] = {"greater_than","greater_than_or_equal","equal_to","not_equal_to","less_than_or_equal","less_than"};
                                                    String sla_success_threshold_operator_names[] = {"Greater than","Greater than or equal to","Equal to","Not equal to","Less than or equal","Less than"};
                                                    for(int a = 0; a < sla_success_threshold_operator.length;a++)
                                                    {
                                                        if(sla_info[18].equalsIgnoreCase(sla_success_threshold_operator[a]))
                                                        {
                                                            %>
                                                            <option SELECTED value="<%=sla_success_threshold_operator[a]%>"><%=sla_success_threshold_operator_names[a]%></option>
                                                            <%
                                                        }
                                                        else
                                                        {
                                                            %>
                                                            <option value="<%=sla_success_threshold_operator[a]%>"><%=sla_success_threshold_operator_names[a]%></option>
                                                            <%
                                                        }
                                                    }
                                                    %>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="field-title">
                                                <label class="formLabel md has-txt-only mb-15">
                                                    Value
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <input type="text" id="sla_success_threshold_value" name="sla_success_threshold_value" class="form-control" value="<%=sla_info[19]%>" />     
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="field-title">
                                                <label class="formLabel md has-txt-only mb-15">
                                                    Unit
                                                </label>
                                            </div>
                                            <div class="formField clr md px-0 border-0">
                                                <select id="sla_success_threshold_unit" name="sla_success_threshold_unit" class="form-control">
                                                    <%
                                                    String sla_success_threshold_unit[] = {"seconds","minutes","hours","days"};
                                                    String sla_success_threshold_unit_names[] = {"Seconds","Minutes","Hours","Days"};
                                                    for(int a = 0; a < sla_success_threshold_unit.length;a++)
                                                    {
                                                        if(sla_info[20].equalsIgnoreCase(sla_success_threshold_unit[a]))
                                                        {
                                                            %>
                                                            <option SELECTED value="<%=sla_success_threshold_unit[a]%>"><%=sla_success_threshold_unit_names[a]%></option>
                                                            <%
                                                        }
                                                        else
                                                        {
                                                            %>
                                                            <option value="<%=sla_success_threshold_unit[a]%>"><%=sla_success_threshold_unit_names[a]%></option>
                                                            <%
                                                        }
                                                    }
                                                    %>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-md-12 text-center pt-30 pb-30">
                                            <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                                            <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                                                Save
                                            </button>
                                        </div>
                                    </div>

            <input type="hidden" id="report_name" name="report_name" value="" />
            <input type="hidden" id="report_description" name="report_description" value="" />
            <input type="hidden" id="report_header" name="report_header" value="" />
            <!--
            <div class="row">
                <div class="col-12">   
                    <div class="card">    
                        <div class="card-header">
                            <h4 class="card-title">Report Parameters </h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name">Report Name</label>                                        
                                            <input type="text" id="report_name" name="report_name" class="form-control" value="<%=sla_info[11]%>" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name">Report Description</label>                                        
                                            <input type="text" id="report_description" name="report_description" class="form-control" value="<%=sla_info[12]%>" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name">Report Header</label>                                        
                                            <input type="text" id="report_header" name="report_header" class="form-control" value="<%=sla_info[15]%>" />
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>   
                    </div>
                </div>
            </div>-->
            <!-- end content here-->
        </form>   

<!-- BEGIN PAGE LEVEL JS-->
<!-- END PAGE LEVEL JS-->   
<%                con.close();

            } catch (Exception e) {
                System.out.println("Exception in admin_request_sla_edit.jsp: " + e);
                logger.debug("ERROR: Exception in admin_request_sla_edit.jsp:" + e);
            }
        } //end if not admin permission
    } //end if not redirected
%>
<!--Copyright 2021 Real Data Technologies, Inc. , All rights reserved.-->
<%-- 
    Document   : incident_new.jsp
    Created on : 18-Jan-2020, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>

<%@ page import="java.time.Instant"%>
<%@ page import="java.time.format.DateTimeFormatter"%>
<%@ page import="java.time.LocalDateTime"%>
<%@ page import="java.time.ZoneId"%>
<%@ page import="java.time.ZonedDateTime"%>
<%@ page import="java.util.TimeZone"%>


    <%
    if(session.getAttribute("authenticated")==null)
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String login_url = props.get("login_url").toString();
        response.sendRedirect(login_url + "?error=Session Timed Out");
    }
    else 
    {
        //session vars
        String ADMINISTRATION = session.getAttribute("administration").toString();
        String MANAGER = session.getAttribute("manager").toString();
        String FINANCIAL = session.getAttribute("financial").toString();
        String INCIDENT = session.getAttribute("incident").toString();
        String REQUEST = session.getAttribute("request").toString();
        String CONTACT = session.getAttribute("contact").toString();
        String CX = session.getAttribute("cx").toString();
        String SURVEY = session.getAttribute("survey").toString();
        String SLA = session.getAttribute("sla").toString();        
        String PROJECT = session.getAttribute("project").toString();
        String JOB = session.getAttribute("job").toString();
        
        
        
        if (INCIDENT.contains("create") || ADMINISTRATION.equalsIgnoreCase("true") || MANAGER.equalsIgnoreCase("true")) 
        {
            java.util.Date now = new java.util.Date();
            String context_dir = request.getServletContext().getRealPath("");
            String home_page = session.getAttribute("home_page").toString();
            LinkedHashMap props = support.config.get_config(context_dir);
            SimpleDateFormat timestamp_format = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat display_format = new SimpleDateFormat("HH:mm / dd-MMM-yy");//18:31 / 14-Jan-19
            SimpleDateFormat date_time_picker_format = new SimpleDateFormat("HH:mm MM/dd/yyyy");//01:00 01/27/2020
            
            String user_tz_name = "UTC";
            String user_tz_time = "+00:00";
            try
            {
                user_tz_name = session.getAttribute("tz_name").toString();
                user_tz_time = session.getAttribute("tz_time").toString();
                if(user_tz_name == null || user_tz_time == null )
                {
                    user_tz_name = "UTC";
                    user_tz_time = "+00:00";
                }
            }
            catch(Exception e)
            {
                user_tz_name = "UTC";
                user_tz_time = "+00:00";
            }
            date_time_picker_format.setTimeZone(TimeZone.getTimeZone(user_tz_name));
            String now_picker_time = date_time_picker_format.format(now);
            String now_display_format = display_format.format(now);
            String now_timestamp_format = timestamp_format.format(now);
            String now_incident_time = date_time_picker_format.format(now);            
            Connection con = null;
            try
            {
                con = db.db_util.get_contract_connection(context_dir, session); 
                String user_id = session.getAttribute("user_id").toString();
                boolean update_session_table = db.get_user_sessions.update(con, user_id);
            }
            catch(Exception e)
            {
                System.out.println("incident_new.jsp exception=" + e);
            }
    %>
    <!-- BEGIN Page Level CSS-->
    <!-- END Page Level CSS-->
    
    <form action="incident_new" method="post">
        <!--future fields-->
        <input type="hidden" name="ci_id" id="ci_id" value=""/>
        <input type="hidden" name="impact" id="impact" value=""/>
        <input type="hidden" name="urgency" id="urgency" value=""/>
        
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Incident ID
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input name="id" id="id" type="text" readonly value="" class="form-control" disabled placeholder="Assigned on save"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Incident Time
                    </label>
                </div>
                <div class="formField clr md px-0 border-0 input-group d-flex">
                    <input type='text' id="incident_time" name="incident_time" value="" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        State
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <%
                    ArrayList<String[]> state_select = db.get_system_select_field.active_for_table_column(con, "incidents", "state");
                    %>
                    <select name="state" id="state" class="form-control">
                        <%
                         
                        for(int a = 0; a < state_select.size(); a++)
                        {
                            String select_option[] = state_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            %>
                            <option value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Caller&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="hidden" name="caller_id" id="caller_id" value=""/>
                    <input type="text" name="caller_username" id="caller_username" value="" class="form-control" placeholder="Enter the callers username"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Callers Group&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="hidden" name="caller_group_id" id="caller_group_id"/>
                    <input type="text" name="caller_group_name" id="caller_group_name" value="" class="form-control" placeholder="Enter the callers Group"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Incident Create By&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="hidden" name="create_by_id" id="create_by_id"/>
                    <input type="text" name="create_by_username" id="create_by_username" value="" class="form-control" placeholder="Enter the creaters username"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Caller VIP?
                        <div>
                            <em>Set by Caller's profile</em>
                        </div>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <span class="icon mr-5"><img src="assets/images/svg/crown-icon.svg" alt=""></span>
                    <div class="custom-control custom-checkbox dInlineBlock ">
                        <input type="checkbox" class="custom-control-input" name="vip" id="vip" disabled="">
                        <label class="custom-control-label small-font" for="vip">Yes</label>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Contact Method
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <%
                    ArrayList<String[]> contact_method_select = db.get_system_select_field.active_for_table_column(con, "incidents", "contact_method");
                    %>
                    <select name="contact_method" id="contact_method" class="form-control">
                        <option value=""></option>
                        <%
                        for(int a = 0; a < contact_method_select.size(); a++)
                        {
                            String select_option[] = contact_method_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            %>
                            <option value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Location&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="location" name="location" value="" class="form-control">
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Department&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="department" name="department" value="" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Site&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="site" name="site" value="" class="form-control">
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Company&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" id="company" name="company" value="" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Priority
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <%
                    ArrayList<String[]> priority_select = db.get_system_select_field.active_for_table_column(con, "incidents", "priority");
                    %>
                    <select name="priority" id="priority" class="form-control">
                        <option value=""></option>
                        <%
                        for(int a = 0; a < priority_select.size(); a++)
                        {
                            String select_option[] = priority_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            %>
                            <option value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Category
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select class="form-control" id="category_id" name="category_id" onchange="update_subcategory()">
                        <option value=""></option>
                        <%
                        ArrayList<String[]> categories = db.get_category.all_active(con);
                        for(int a = 0; a < categories.size();a++)
                        {
                            String category[] = categories.get(a);
                            %>
                            <option value="<%=category[0]%>"><%=category[1]%></option>
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Subcategory
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <select class="form-control" id="subcategory" name="subcategory">
                        <option value=""></option>
                    </select>
                </div>
            </div>
        
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Assigned to Group&nbsp;<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="hidden" name="assigned_group_id" id="assigned_group_id" value="0"/>
                    <input type="text" name="assigned_group_name" id="assigned_group_name" value="Group Unassigned" class="form-control" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        
                    
                    Assigned to&nbsp;
                    <i class="la la-search" style="font-size: 14px;"></i>
                    <i class="la la-question-circle" style="font-size: 14px;" title="This field is filtered by the Assigned to Group field. If Group Unassigned is selected, then all users will be available. If a specific group is selected, then only the members of that group will be available. If the Assigned to Group is changed this field will be cleared."></i>
                
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="hidden" name="assigned_to_id" id="assigned_to_id" value="0"/>
                    <input type="text" name="assigned_to_name" id="assigned_to_name" value="" class="form-control" placeholder="Enter the Assigned To group's name"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        First Contact Resolution?
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <div class="form-check form-check-inline full-width">
                        <input class="form-check-input w-auto" type="radio" name="first_contact_resolution" id="first_contact_resolution_yes" value="on">
                        <label class="form-check-label" for="first_contact_resolution_yes">Yes</label>
                        <input checked class="form-check-input w-auto mx-2" type="radio" name="first_contact_resolution" id="first_contact_resolution_no" value="">
                        <label class="form-check-label" for="first_contact_resolution_no">No</label>
                    </div>
                </div>                            
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-0 pb-0">
                        <a data-toggle="collapse" data-target="#associated_asset" aria-expanded="true" href="javascript:void(0)">Associated Asset</a>
                    </label>
                </div>
            </div>
        </div>
        <div class="collapse" id="associated_asset" aria-expanded="false"">
            <input type="hidden" name="ci_id" id="ci_id" value="0"/>
            <div class="row">
                <div class="col-md-6">
                    <div class="field-title">
                        <label class="formLabel md has-txt-only mb-15">
                            Asset Tag
                        </label>
                    </div>
                    <div class="formField clr md px-0 border-0">
                        <input type="text" id="asset_tag" name="asset_tag" class="form-control" placeholder=""/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="field-title">
                        <label class="formLabel md has-txt-only mb-15">
                            Serial #
                        </label>
                    </div>
                    <div class="formField clr md px-0 border-0">
                        <input type="text" id="serial_number" name="serial_number" class="form-control" placeholder="Enter Serial #"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="field-title">
                        <label class="formLabel md has-txt-only mb-15">
                            Manufacturer
                        </label>
                    </div>
                    <div class="formField clr md px-0 border-0">
                        <input type="text" id="manufacturer" name="manufacturer" class="form-control" placeholder=""/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="field-title">
                        <label class="formLabel md has-txt-only mb-15">
                            Model
                        </label>
                    </div>
                    <div class="formField clr md px-0 border-0">
                        <input type="text" id="model" name="model" class="form-control" placeholder=""/>
                    </div>
                </div>
            </div>
        </div>
        <div class="separator-new mb-30 mt-15"></div>                
              
        <div class="row"><!--Start Custom Fields-->  
            <%
            //get active custom incident fields
            ArrayList <String[]> custom_fields = db.get_custom_fields.active_for_form(con, "incident");
            /*temp[0] = rs.getString("id");
            temp[1] = rs.getString("form");
            temp[2] = rs.getString("index");
            temp[3] = rs.getString("field_name");
            temp[4] = rs.getString("field_label");
            temp[5] = rs.getString("default_text");
            temp[6] = rs.getString("number_cols");
            temp[7] = rs.getString("field_type");
            temp[8] = rs.getString("field_values");
            temp[9] = rs.getString("required");
            temp[10] = rs.getString("active");*/
            for(int a = 0; a < custom_fields.size(); a++)
            {
                String custom_field[] = custom_fields.get(a);
                if(custom_field[7].equalsIgnoreCase("select"))
                {
                    %>
                    <jsp:include page='build_custom_select_field.jsp'>
                        <jsp:param name="field_name" value="<%=custom_field[3]%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="6"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="new_record" value="true"/>
                        <jsp:param name="db_value" value=""/>
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("text"))
                {
                    %>
                    <jsp:include page='build_custom_text_field.jsp'>
                        <jsp:param name="field_name" value="<%=custom_field[3]%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="true"/>
                        <jsp:param name="db_value" value=""/>      
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("textarea"))
                {
                    %>
                    <jsp:include page='build_custom_textarea_field.jsp'>
                        <jsp:param name="field_name" value="<%=custom_field[3]%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="true"/>
                        <jsp:param name="db_value" value=""/>      
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("checkbox"))
                {
                    %>
                    <jsp:include page='build_custom_checkbox_field.jsp'>
                        <jsp:param name="field_name" value="<%=custom_field[3]%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="true"/>
                        <jsp:param name="db_value" value=""/>     
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("people-lookup"))
                {
                    %>
                    <jsp:include page='build_custom_people_field.jsp'>
                        <jsp:param name="field_name" value="<%=custom_field[3]%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="true"/>
                        <jsp:param name="db_value" value=""/>     
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("group-lookup"))
                {
                    %>
                    <jsp:include page='build_custom_group_field.jsp'>
                        <jsp:param name="field_name" value="<%=custom_field[3]%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="field_values" value="<%=custom_field[8]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="true"/>
                        <jsp:param name="db_value" value=""/> 
                    </jsp:include>
                    <%
                }
                else if(custom_field[7].equalsIgnoreCase("date"))
                {
                    %>
                    <jsp:include page='build_custom_date_field.jsp'>
                        <jsp:param name="field_name" value="<%=custom_field[3]%>"/>
                        <jsp:param name="field_label" value="<%=custom_field[4]%>"/>
                        <jsp:param name="number_cols" value="<%=custom_field[6]%>"/>
                        <jsp:param name="default_text" value="<%=custom_field[5]%>"/>
                        <jsp:param name="new_record" value="true"/>
                        <jsp:param name="db_value" value=""/>
                    </jsp:include>
                    <%
                }
            }
            %>    
        </div><!--End Custom Fields-->      
        <div class="separator-new mb-30 mt-30"></div>                
                
        <div class="row">
            <div class="col-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Description
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <input type="text" name="description" id="description" value="" class="form-control"/> 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Notes <em>(Customer viewable)</em>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <textarea name="notes" id="notes" cols="30" rows="5" class="ckeditor">
                    </textarea>
                </div>
                <!--
                <p class="text-muted">Notes <em>(Customer viewable)</em></p>
                <textarea class="form-control" name="notes" id="notes" rows="5"></textarea>  
                -->
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Notes <em>(Service Desk viewable)</em>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <textarea name="desk_notes" id="desk_notes" cols="30" rows="5" class="ckeditor">
                    </textarea>
                </div>
                <!--
                <p class="text-muted">Notes <em>(Service Desk)</em></p>
                <textarea class="form-control" name="desk_notes" id="desk_notes" rows="5"></textarea>  -->
            </div>
        </div>
        <div class="separator-new mb-30 mt-30"></div>                
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Pending Date
                    </label>
                </div>
                <div class="formField clr md px-0 border-0 input-group d-flex">
                    <input type='text' id="pending_date" name="pending_date" value="" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Pending Reason
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <%
                    ArrayList<String[]> pending_reason_select = db.get_system_select_field.active_for_table_column(con, "incidents", "pending_reason");
                    %>
                    <select name="pending_reason" id="pending_reason" class="form-control">
                        <option value=""></option>
                        <%
                        for(int a = 0; a < pending_reason_select.size(); a++)
                        {
                            String select_option[] = pending_reason_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            %>
                            <option value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Closure Date
                    </label>
                </div>
                <div class="formField clr md px-0 border-0 input-group d-flex">
                    <input type='text' id="closed_date" name="closed_date" value="" class="form-control datetime" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Closure Reason
                    </label>
                </div>
                <div class="formField clr md px-0 border-0">
                    <%
                    ArrayList<String[]> closed_reason_select = db.get_system_select_field.active_for_table_column(con, "incidents", "closed_reason");
                    %>
                    <select name="closed_reason" id="closed_reason" class="form-control">
                        <option value=""></option>
                        <%
                        for(int a = 0; a < closed_reason_select.size(); a++)
                        {
                            String select_option[] = closed_reason_select.get(a);
                            //select_option[3] = value    select_option[4] = label
                            %>
                            <option value="<%=select_option[3]%>"><%=select_option[4]%></option>        
                            <%
                        }
                        %>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Key Word Search Terms<i class="la la-search" style="font-size: 14px;"></i>
                    </label>
                </div>
                <div class="formField clr md px-0 border-0 input-group d-flex">
                    <input type='text' id="knowledge_search_terms" name="knowledge_search_terms" placeholder="Enter a comma separated list of word to search for" value="" class="form-control" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span class="la la-question"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>            
        <div class="row">
            <div class="col-md-12">
                <div class="field-title">
                    <label class="formLabel md has-txt-only mb-15">
                        Attachments
                    </label>
                </div>
                <div class="formField clr md border-0">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="file" name="file" multiple>
                        <label class="custom-file-label" for="file">Choose file(s)</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-12 text-center pt-30 pb-30">
                <button type="button" class="btn btn-outline-primary customBtn lg waves-effect rightSidePanelCloser">Cancel</button>
                <button type="submit" class="btn btn-primary-new customBtn lg waves-effect" role="button">
                    Save
                </button>
            </div>
        </div>                                
    </form>
<%
        }//end if not permission
        else
        {
            response.sendRedirect("no_permission.jsp");
        }
    }//end if not logged in
%>
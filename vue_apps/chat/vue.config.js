var path = require('path');
const FileManagerPlugin = require('filemanager-webpack-plugin');
var JSESSIONID = "B9C3D2832AB25134E9582EEB7053061E";

module.exports = {
	lintOnSave: false,
	publicPath:
		process.env.NODE_ENV === 'production' ? '/vue-advanced-chat/' : '/',
	devServer: {
		port: 9999,
		open: true,
		before: function(app, server, compiler) {
	      app.get('/JSESSIONID/*', function(req, res) {
	      	// console.log(req.params[0]);
	      	JSESSIONID = req.params[0];
	        res.json({ custom: 'OK' });
	      });
	    },
	    proxy: {
	    	'get_attachment': {
	    		target: 'http://localhost:8084/2019DeskSultan/',
	    		changeOrigin: true,
				onProxyReq: function(proxyReq) {
	                proxyReq.setHeader("Cookie", "JSESSIONID=" + JSESSIONID);
	            }
	    	},
	    	'get_avatar': {
	    		target: 'http://localhost:8084/2019DeskSultan/',
	    		changeOrigin: true,
				onProxyReq: function(proxyReq) {
	                proxyReq.setHeader("Cookie", "JSESSIONID=" + JSESSIONID);
	            }
	    	},
	    }
	},
	configureWebpack: {
		externals: {
	      "vue": "Vue",
	      "core-js": "core-js",
	    },
        output: {
            filename: "js/chat_app.js",
        },
		plugins: [
	    	new FileManagerPlugin({
	    		events:{
		            onEnd: [{
		                copy: [
		                    {
		                        // source: 'dist/js/chat_app.js',
		                        // destination: path.resolve(__dirname, '../../SourceCode/ServeOpticsDesk/web/assets/js/test'),
		                        source: path.join(__dirname, "dist/js"),
		                        destination: path.join(__dirname, '../../web/assets/js')
		                    },
		                    {
		                        // source: 'dist/js/chat_app.js',
		                        // destination: path.resolve(__dirname, '../../SourceCode/ServeOpticsDesk/web/assets/js/test'),
		                        source: path.join(__dirname, "dist/css"),
		                        destination: path.join(__dirname, '../../web/assets/css')
		                    }
		                ]
		            }]	    			
	    		}
	        })
	    ]
    },
	css: {
	    	extract: { 
	      	ignoreOrder: true,
	      	filename: 'css/chat_app.css'
    	},
    },
  	filenameHashing: false,

  	chainWebpack: config => {
    	config.optimization.delete('splitChunks');
  	}
}

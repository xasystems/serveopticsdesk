import Vue from 'vue'
import vueCustomElement from 'vue-custom-element';
import chatApp from './App.vue'

// Vue.config.productionTip = false
Vue.config.ignoredElements = [
  'chat-app'
];

Vue.use(vueCustomElement);

Vue.customElement('chat-app', chatApp, {
  // Additional Options: https://github.com/karol-f/vue-custom-element#options
});

// window.chat_app = new Vue({
// 	data: {
// 		showChat
// 	},
// 	render: h => h(App)
// }).$mount('#chat_app').$children[0]

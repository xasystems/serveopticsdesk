import app from 'firebase/app'
import 'firebase/firestore'
import 'firebase/database'
import 'firebase/storage'

const config = {
  apiKey: "AIzaSyChMYH9jQzj_j9QlZTKAUQbMz03etpjwqU",
  authDomain: "dsultanr-98886.firebaseapp.com",
  databaseURL: "https://dsultanr-98886.firebaseio.com",
  projectId: "dsultanr-98886",
  storageBucket: "dsultanr-98886.appspot.com",
  messagingSenderId: "252141222850",
  appId: "1:252141222850:web:cf16c6d64388f8039db45b",
  measurementId: "G-EKB4QB401B"
};

app.initializeApp(config)

export const firebase = app
export const db = app.firestore()
export const storageRef = app.storage().ref()

export const usersRef = db.collection('users')
export const roomsRef = db.collection('chatRooms')
export const messagesRef = roomId => roomsRef.doc(roomId).collection('messages')

export const filesRef = storageRef.child('files')

export const dbTimestamp = firebase.firestore.FieldValue.serverTimestamp()
export const deleteDbField = firebase.firestore.FieldValue.delete()

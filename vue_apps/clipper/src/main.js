import Vue from 'vue'
import VueRx from 'vue-rx'
import VuejsClipper from 'vuejs-clipper'
import App from './App.vue'

Vue.use(VueRx)
Vue.use(VuejsClipper)

Vue.config.productionTip = false;

/* eslint-disable no-new */
window.clipper_app = new Vue({
	components: {App},
  	template: '<App/>',
  	el: '#clipper_app',
})
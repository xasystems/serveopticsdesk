var path = require('path');
const FileManagerPlugin = require('filemanager-webpack-plugin');

module.exports = {
  	runtimeCompiler: true,
	lintOnSave: false,
	publicPath:
		process.env.NODE_ENV === 'production' ? '/clipper/' : '/',
	devServer: {
		port: 9999,
		open: true,
	    // proxy: {
	    // 	'^': {
	    // 		target: 'http://localhost:8084',
	    // 		changeOrigin: true
	    // 	},
	    // }
	},
	configureWebpack: {
        output: {
            filename: "js/vue_clip.js",
        },
		plugins: [
	    	new FileManagerPlugin({
	    		events:{
		            onEnd: [{
		                copy: [
		                    {
		                        // source: 'dist/js/chat_app.js',
		                        // destination: path.resolve(__dirname, '../../SourceCode/ServeOpticsDesk/web/assets/js/test'),
		                        source: path.join(__dirname, "dist/js"),
		                        destination: path.join(__dirname, '../../web/assets/js')
		                    },
		                    {
		                        // source: 'dist/js/chat_app.js',
		                        // destination: path.resolve(__dirname, '../../SourceCode/ServeOpticsDesk/web/assets/js/test'),
		                        source: path.join(__dirname, "dist/css"),
		                        destination: path.join(__dirname, '../../web/assets/css')
		                    }
		                ]
		            }]	    			
	    		}
	        })
	    ]
    },
	filenameHashing: false,
	css: {
	    extract: { 
	    	ignoreOrder: true,
	      	filename: 'css/vue_clip.css'
	    },
  	},
  	chainWebpack: config => {
    	config.optimization.delete('splitChunks');
  	}
}
